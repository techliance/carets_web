<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ad_views")) {
        Schema::create('ad_views', function (Blueprint $table) {
            $table->id(); // id as an unsigned INT with AUTO_INCREMENT
            $table->foreignId('ad_id')->constrained('ads')->onDelete('cascade')->onUpdate('restrict');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('restrict');
            $table->foreignId('campaign_id')->nullable()->constrained('campaigns')->onDelete('restrict')->onUpdate('restrict');
            $table->string('position', 255)->nullable()->default(null);
            $table->string('age', 255)->nullable()->default(null);
            $table->string('gender', 255)->nullable()->default(null);
            $table->string('location', 255)->nullable()->default(null);
            $table->string('interests', 255)->nullable()->default(null);
            $table->tinyInteger('complete')->unsigned()->nullable()->default(null);
            $table->tinyInteger('clicked')->unsigned()->nullable()->default(null);
            $table->timestamps(0); // Timestamps for created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes

          
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_views');
    }
}
