<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_keywords")) {
        Schema::create('clm_keywords', function (Blueprint $table) {
            $table->increments('id'); // id as INT(11) UNSIGNED AUTO_INCREMENT
            $table->string('title', 255)->nullable(); // title as VARCHAR(255) NULL DEFAULT NULL
            $table->string('type', 50)->nullable(); // type as VARCHAR(50) NULL DEFAULT NULL
            $table->unsignedTinyInteger('is_active')->nullable()->default(null); // is_active as TINYINT(1) UNSIGNED ZEROFILL NULL DEFAULT NULL
            $table->timestamps(); // created_at and updated_at (TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP and ON UPDATE CURRENT_TIMESTAMP)
            $table->softDeletes(); // deleted_at as TIMESTAMP NULL DEFAULT NULL
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_keywords');
    }
}
