<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ages")) {
        Schema::create('ages', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned(); // id as an unsigned INT with AUTO_INCREMENT
            $table->integer('min')->nullable()->default(null); // minimum age
            $table->integer('max')->nullable()->default(null); // maximum age
            $table->string('title', 255)->nullable()->default(null); // title of the age range
            $table->tinyInteger('sortorder')->nullable()->default(null); // sort order for display
            $table->timestamps(0); // Timestamps for created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ages');
    }
}
