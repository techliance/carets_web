<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoDisplayToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_display_to")) {
        Schema::create('video_display_to', function (Blueprint $table) {
            $table->increments('id'); // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->string('title', 255)->nullable(); // title (VARCHAR, NULLABLE)
            $table->boolean('is_active')->default(0); // is_active (TINYINT)
            $table->timestamps(); // created_at, updated_at
            $table->softDeletes(); // deleted_at (TIMESTAMP, NULLABLE)
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_display_to');
    }
}
