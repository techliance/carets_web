<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_profiles")) {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
            $table->unsignedInteger('industry_id')->default(0);  // industry_id (INT, UNSIGNED, DEFAULT 0)
            $table->string('business_name', 250)->nullable()->default('0');  // business_name (VARCHAR, COLLATE 'utf8mb4_unicode_ci')
            $table->string('phone_number', 250)->nullable()->default('0');  // phone_number (VARCHAR, COLLATE 'utf8mb4_unicode_ci')
            $table->string('first_name', 250)->nullable()->default(null);  // first_name (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('middle_name', 250)->nullable()->default(null);  // middle_name (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('last_name', 250)->nullable()->default(null);  // last_name (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('user_title', 250)->nullable()->default(null);  // user_title (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->text('user_bio')->nullable()->default(null);  // user_bio (TEXT, COLLATE 'utf8mb4_unicode_ci')
            $table->string('user_photo', 250)->nullable()->default(null);  // user_photo (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('user_video', 250)->nullable()->default(null);  // user_video (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('gender', 250)->nullable()->default(null);  // gender (VARCHAR, COLLATE 'utf8mb4_unicode_ci')
            $table->dateTime('dob')->nullable()->default(null);  // dob (DATETIME, DEFAULT NULL)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign key
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // user_id (Foreign Key)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
