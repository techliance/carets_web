<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaretSearchDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("caret_search_data")) {
        Schema::create('caret_search_data', function (Blueprint $table) {
            $table->id(); // id as BIGINT(20) with AUTO_INCREMENT
            $table->string('keyword')->nullable()->collation('utf8mb4_general_ci'); // keyword as VARCHAR(255)
            $table->string('classification')->nullable()->collation('utf8mb4_general_ci');
            $table->string('net_worth')->nullable()->collation('utf8mb4_general_ci');
            $table->string('google_search_volume')->nullable()->collation('utf8mb4_general_ci');
            $table->string('instagram')->nullable()->collation('utf8mb4_general_ci');
            $table->string('twitter')->nullable()->collation('utf8mb4_general_ci');
            $table->timestamps(0); // created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes

        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caret_search_data');
    }
}
