<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ad_status")) {
        Schema::create('ad_status', function (Blueprint $table) {
            $table->id();// id as an unsigned INT with AUTO_INCREMENT
            $table->string('title', 250)->nullable()->default(null);
            $table->timestamps(0); // Timestamps for created_at and updated_at, with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes

        
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_status');
    }
}
