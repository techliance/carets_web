<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtpToPasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('password_resets', function (Blueprint $table) {
        //     $table->string('otp', 6)->nullable()->after('email');
        //     $table->timestamp('expires_at')->nullable()->after('otp');
        // });
    }

    public function down()
    {
        Schema::table('password_resets', function (Blueprint $table) {
            $table->dropColumn(['otp', 'expires_at']);
        });
    }
}
