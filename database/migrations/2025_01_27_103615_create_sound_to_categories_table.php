<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoundToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("sound_to_categories")) {
        Schema::create('sound_to_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('sound_id');  // sound_id (BIGINT, UNSIGNED)
            $table->unsignedInteger('cat_id');  // cat_id (INT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->softDeletes();  // deleted_at (Soft delete)
            
            // Foreign keys
            $table->foreign('sound_id')->references('id')->on('sounds')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('cat_id')->references('id')->on('sound_categories')->onUpdate('restrict')->onDelete('restrict');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sound_to_categories');
    }
}
