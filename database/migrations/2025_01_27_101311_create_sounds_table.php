<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("sounds")) {
        Schema::create('sounds', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->bigInteger('user_id')->nullable();  // user_id (BIGINT, can be null)
            $table->integer('license_id')->unsigned()->nullable();  // license_id (INT, UNSIGNED, can be null)
            $table->string('sound_title', 50)->nullable();  // sound_title (VARCHAR(50), can be null)
            $table->string('sound_url', 50)->nullable();  // sound_url (VARCHAR(50), can be null)
            $table->string('image_url', 50)->nullable();  // image_url (VARCHAR(50), can be null)
            $table->string('sound_description', 50)->nullable();  // sound_description (VARCHAR(50), can be null)
            $table->tinyInteger('is_active')->unsigned()->default(0);  // is_active (TINYINT, UNSIGNED, DEFAULT 0)
            $table->tinyInteger('video_count')->unsigned()->default(0);  // video_count (TINYINT, UNSIGNED, DEFAULT 0)
            $table->timestamps();  // created_at, updated_at
            $table->softDeletes();  // deleted_at (Soft delete)
      
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sounds');
    }
}
