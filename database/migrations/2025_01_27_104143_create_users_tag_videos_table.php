<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTagVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("users_tag_videos")) {
        Schema::create('users_tag_videos', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('video_id');  // video_id (BIGINT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // user_id (Foreign Key)
            $table->foreign('video_id')->references('id')->on('videos')->onUpdate('RESTRICT')->onDelete('CASCADE');  // video_id (Foreign Key)

            // Indexes
            $table->index('user_id', 'FK_users_m_videos_users');  // user_id (INDEX)
            $table->index('video_id', 'FK_users_m_videos_videos');  // video_id (INDEX)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_tag_videos');
    }
}
