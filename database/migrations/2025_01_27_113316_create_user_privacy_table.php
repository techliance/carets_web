<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPrivacyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_privacy")) {
        Schema::create('user_privacy', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
            $table->tinyInteger('private_account')->unsigned()->default(0);  // private_account (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('sugest_others')->unsigned()->default(0);  // sugest_others (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('ad_auth')->unsigned()->default(0);  // ad_auth (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('download_video')->unsigned()->default(0);  // download_video (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('likes')->unsigned()->default(0);  // likes (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('comments')->unsigned()->default(0);  // comments (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('followers')->unsigned()->default(0);  // followers (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('mentions')->unsigned()->default(0);  // mentions (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('direct_messages')->unsigned()->default(0);  // direct_messages (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('video_from')->unsigned()->default(0);  // video_from (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->tinyInteger('video_suggestion')->unsigned()->default(0);  // video_suggestion (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign key
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // user_id (Foreign Key)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_privacy');
    }
}
