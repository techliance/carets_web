<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_likes")) {
        Schema::create('video_likes', function (Blueprint $table) {
            $table->increments('id'); // Primary key (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // Foreign key to 'videos'
            $table->unsignedBigInteger('user_id'); // Foreign key to 'users'
            $table->timestamps(); // Adds 'created_at' and 'updated_at'
            $table->softDeletes(); // Adds 'deleted_at' for soft deletes

            // Foreign key constraints
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');

            // Indexes
            $table->index('video_id', 'FK_video_likes_videos');
            $table->index('user_id', 'FK_video_likes_users');
        });
    }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_likes');
    }
}
