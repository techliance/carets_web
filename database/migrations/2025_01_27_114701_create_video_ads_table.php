<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_ads")) {
        Schema::create('video_ads', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id');  // video_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('ad_id');  // ad_id (BIGINT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('video_id')->references('id')->on('videos')->onUpdate('RESTRICT')->onDelete('CASCADE');  // video_id (Foreign Key to videos)
            $table->foreign('ad_id')->references('id')->on('ads')->onUpdate('RESTRICT')->onDelete('CASCADE');  // ad_id (Foreign Key to ads)
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_ads');
    }
}
