<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_requests")) {
        Schema::create('clm_requests', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->unsignedBigInteger('user_id')->nullable();
            $table->integer('order_id');
            $table->integer('order_item_id')->nullable();
            $table->integer('caret_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('caret_title', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->integer('subscription_plan')->nullable();
            $table->decimal('subscription_amount', 20, 2)->nullable();
            $table->string('name', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->string('designation', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->string('email', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->string('phone', 15)->nullable()->collation('utf8mb4_general_ci');
            $table->string('company_name', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->string('company_address', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->string('company_description', 255)->nullable()->collation('utf8mb4_general_ci');
            $table->tinyInteger('status')->nullable()->default(0);
            $table->timestamp('approval_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_requests');
    }
}
