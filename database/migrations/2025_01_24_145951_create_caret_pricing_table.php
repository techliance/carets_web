<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaretPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("caret_pricing")) {
        Schema::create('caret_pricing', function (Blueprint $table) {
            $table->id(); // id as INT(11) with AUTO_INCREMENT
            $table->unsignedBigInteger('plan_id')->default(0); // plan_id as BIGINT(20) with a default value of 0
            $table->string('stripe_id')->nullable()->collation('latin1_swedish_ci'); // stripe_id as VARCHAR(255)
            $table->string('stripe_id_one_year')->nullable()->collation('latin1_swedish_ci');
            $table->string('stripe_id_two_year')->nullable()->collation('latin1_swedish_ci');
            $table->string('stripe_id_three_year')->nullable()->collation('latin1_swedish_ci');
            $table->string('stripe_subscription_id')->nullable()->collation('latin1_swedish_ci');
            $table->string('title')->collation('latin1_swedish_ci'); // title as VARCHAR(255) NOT NULL
            $table->string('networth')->nullable()->collation('latin1_swedish_ci');
            $table->string('instagram_followers')->nullable()->collation('latin1_swedish_ci');
            $table->string('twitter_followers')->nullable()->collation('latin1_swedish_ci');
            $table->string('google_searches')->nullable()->collation('latin1_swedish_ci');
            $table->decimal('one_year_license', 10, 2)->nullable();
            $table->decimal('two_year_license', 10, 2)->nullable();
            $table->decimal('three_year_license', 10, 2)->nullable();
            $table->text('special_instructions')->nullable()->collation('latin1_swedish_ci');
            $table->tinyInteger('contact_sales')->unsigned()->nullable();
            $table->decimal('contact_amount', 10, 2)->unsigned()->nullable();
            $table->tinyInteger('is_active')->unsigned()->nullable();
            $table->decimal('sales_price', 10, 2)->nullable();
            $table->timestamps(0); // created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes

           
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caret_pricing');
    }
}
