<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("campaigns_ads")) {
            Schema::create('campaigns_ads', function (Blueprint $table) {
                $table->bigIncrements('id'); // BIGINT UNSIGNED AUTO_INCREMENT
                $table->unsignedBigInteger('campaign_id')->default(0)->nullable();
                $table->unsignedBigInteger('ad_id')->default(0)->nullable();
                $table->unsignedBigInteger('plan_id')->nullable();
                
                // Define status_id only once
                $table->unsignedBigInteger('status_id')->nullable(); // Match ad_status.id type
                
                $table->unsignedInteger('watch_count')->default(0);
                $table->tinyInteger('recurring')->default(1)->nullable();
                $table->string('subscription_status', 255)->nullable();
                $table->string('cancel_at_period_end', 255)->nullable();
                $table->string('current_period_end', 255)->nullable();
                $table->timestamps();
                $table->softDeletes();
            
                // Foreign key constraints
                $table->foreign('campaign_id')->references('id')->on('campaigns')
                    ->onUpdate('restrict')->onDelete('restrict');
                    
                $table->foreign('ad_id')->references('id')->on('ads')
                    ->onUpdate('restrict')->onDelete('restrict');
                    
                $table->foreign('plan_id')->references('id')->on('ad_plans')
                    ->onUpdate('restrict')->onDelete('restrict');
                    
                $table->foreign('status_id')->references('id')->on('ad_status')
                    ->onUpdate('restrict')->onDelete('restrict');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_ads');
    }
}
