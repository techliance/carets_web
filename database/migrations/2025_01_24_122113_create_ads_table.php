<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ads')) {
            Schema::create('ads', function (Blueprint $table) {
                $table->id(); // Primary key
                $table->unsignedBigInteger('user_id')->nullable()->default(0);
                $table->unsignedInteger('license_id')->nullable();
                $table->string('ad_title', 255)->nullable();
                $table->string('video_url', 255)->nullable();
                $table->string('image_url', 255)->nullable();
                $table->string('ad_share_link', 255)->nullable();
                $table->string('ad_button_text', 255)->nullable();
                $table->string('ad_button_link', 255)->nullable();
                $table->text('ad_description')->nullable();
                $table->unsignedTinyInteger('is_active')->default(0)->comment('0: Inactive, 1: Active');
                $table->unsignedTinyInteger('is_blocked')->default(0)->comment('0: Not Blocked, 1: Blocked');
                $table->unsignedInteger('like_count')->default(0);
                $table->unsignedInteger('watch_count')->nullable()->default(0);
                $table->unsignedInteger('duration')->nullable()->default(0);
                $table->timestamps(); // Includes created_at and updated_at
                $table->softDeletes(); // Adds deleted_at column for soft deletes

                // Foreign Key Constraints
                $table->foreign('user_id')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('NO ACTION');
                
                // Indexes
                $table->index('user_id', 'FK_ads_users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
