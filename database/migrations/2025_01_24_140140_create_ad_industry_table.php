<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdIndustryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ad_industry")) {
            Schema::create('ad_industry', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->string('title', 255)->nullable()->default(null);
                $table->timestamps(0);
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_industry');
    }
}
