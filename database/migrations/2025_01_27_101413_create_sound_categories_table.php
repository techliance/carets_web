<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoundCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("sound_categories")) {
        Schema::create('sound_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->string('title', 255)->nullable();  // title (VARCHAR(255), can be null)
            $table->tinyInteger('is_active')->unsigned()->default(0);  // is_active (TINYINT, UNSIGNED, DEFAULT 0)
            $table->timestamps();  // created_at, updated_at
            $table->softDeletes();  // deleted_at (Soft delete)
            
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sound_categories');
    }
}
