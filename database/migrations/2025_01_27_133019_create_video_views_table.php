<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_views")) {
        Schema::create('video_views', function (Blueprint $table) {
            $table->increments('id')->unsigned(); // Primary key (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // Foreign key to 'videos'
            $table->unsignedBigInteger('user_id'); // Foreign key to 'users'
            $table->timestamps(); // Adds 'created_at' and 'updated_at'
            $table->softDeletes(); // Adds 'deleted_at' for soft deletes

            // Foreign key constraints
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_views');
    }
}
