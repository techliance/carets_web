<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("system_settings")) {
        Schema::create('system_settings', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->string('name', 255)->default('0');  // name (VARCHAR, DEFAULT '0')
            $table->tinyInteger('is_active')->unsigned()->default(0);  // is_active (TINYINT, UNSIGNED)
            $table->string('setting_type', 255)->default('0');  // setting_type (VARCHAR, DEFAULT '0')
            $table->string('value', 255)->default('0');  // value (VARCHAR, DEFAULT '0')
            $table->timestamps();  // created_at, updated_at
            
            
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_settings');
    }
}
