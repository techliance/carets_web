<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_order_items")) {
        Schema::create('clm_order_items', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->unsignedBigInteger('order_id')->nullable()->index();
            $table->string('caret_title', 255)->nullable();
            $table->string('subscription_stripe_id', 255)->nullable();
            $table->unsignedBigInteger('subscription_pricing_id')->nullable();
            $table->integer('interval_count')->nullable();
            $table->decimal('subscription_amount', 20, 2)->nullable();
            $table->string('subscription_status', 255)->nullable();
            $table->string('cancel_at_period_end', 255)->nullable();
            $table->string('current_period_end', 255)->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_order_items');
    }
}
