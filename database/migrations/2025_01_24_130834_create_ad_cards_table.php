<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ad_cards")) {
            Schema::create('ad_cards', function (Blueprint $table) {
                $table->id(); // Primary key
                $table->unsignedBigInteger('user_id'); // Foreign key to users table
                $table->string('stripe_id', 255)->nullable(); // Stripe ID
                $table->string('card_number', 255)->nullable(); // Card Number
                $table->string('card_expiry', 255)->nullable(); // Card Expiry
                $table->unsignedTinyInteger('set_default')->nullable(); // Set Default (Unsigned TinyInt)
                $table->timestamps(); // created_at and updated_at
                $table->softDeletes(); // deleted_at
                // Indexes
                $table->index('user_id', 'FK_ad_cards_users');
                // Foreign Key Constraint
                $table->foreign('user_id', 'FK_ad_cards_users')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_cards');
    }
}
