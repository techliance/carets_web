<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsIndexingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        if (!Schema::hasTable("ads_indexing")) {
            Schema::create('ads_indexing', function (Blueprint $table) {
                $table->id(); // Primary key
                $table->unsignedBigInteger('ad_id'); // Ad ID
                $table->unsignedBigInteger('campaign_ad_id'); // Campaign Ad ID
                $table->text('description')->nullable(); // Description
                $table->string('video_url', 255); // Video URL
                $table->string('image_url', 255)->nullable(); // Image URL
                $table->unsignedInteger('age_range')->nullable(); // Age Range
                $table->unsignedInteger('gender')->nullable(); // Gender
                $table->string('position', 255)->nullable(); // Position
                $table->string('location', 255)->nullable(); // Location
                $table->timestamps(); // Includes created_at and updated_at

                // Indexes
                $table->index('ad_id');
                $table->index('campaign_ad_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_indexing');
    }
}
