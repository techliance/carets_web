<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_to_categories")) {
        Schema::create('video_to_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned(); // Primary key (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // Foreign key to 'videos'
            $table->unsignedInteger('cat_id'); // Foreign key to 'video_categories'
            $table->timestamps(); // Adds 'created_at' and 'updated_at'
            $table->softDeletes(); // Adds 'deleted_at' for soft deletes

            // Foreign key constraints
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onUpdate('restrict')
                  ->onDelete('restrict');
            $table->foreign('cat_id')->references('id')->on('video_categories')
                  ->onUpdate('restrict')
                  ->onDelete('restrict');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_to_categories');
    }
}
