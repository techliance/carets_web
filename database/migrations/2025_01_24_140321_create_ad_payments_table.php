<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable("ad_payments")){
            Schema::create('ad_payments', function (Blueprint $table) {
                $table->id();// id as an unsigned INT with AUTO_INCREMENT
                $table->foreignId('user_id')->nullable()->constrained('users')->onDelete('set null')->onUpdate('restrict');
                $table->foreignId('campaign_ad_id')->nullable()->constrained('campaigns_ads')->onDelete('set null')->onUpdate('restrict');
                $table->bigInteger('card_id')->nullable();
                $table->decimal('amount', 10, 2)->default(0.00);
                $table->string('transaction_id', 255)->nullable();
                $table->string('latest_invoice', 255)->nullable();
                $table->string('transaction_type', 255)->nullable();
                $table->string('customer', 255)->nullable();
                $table->string('cancel_at', 255)->nullable();
                $table->string('cancel_at_period_end', 255)->nullable();
                $table->string('canceled_at', 255)->nullable();
                $table->string('collection_method', 255)->nullable();
                $table->string('created', 255)->nullable();
                $table->string('current_period_end', 255)->nullable();
                $table->string('current_period_start', 255)->nullable();
                $table->string('days_until_due', 255)->nullable();
                $table->string('default_payment_method', 255)->nullable();
                $table->string('plan_id', 255)->nullable();
                $table->string('plan_currency', 255)->nullable();
                $table->string('plan_interval', 255)->nullable();
                $table->string('plan_interval_count', 255)->nullable();
                $table->string('plan_product', 255)->nullable();
                $table->string('start_date', 255)->nullable();
                $table->string('status', 255)->nullable();
                $table->timestamps(0); // Created at & Updated at timestamps
                $table->softDeletes();

                // Indexes
            
                $table->index('user_id', 'FK_ad_payments_users');
                $table->index('campaign_ad_id', 'FK_ad_payments_campaigns_ads');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_payments');
    }
}
