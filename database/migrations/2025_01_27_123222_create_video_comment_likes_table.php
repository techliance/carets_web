<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoCommentLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_comment_likes")) {
        Schema::create('video_comment_likes', function (Blueprint $table) {
            $table->increments('id'); // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('comment_id'); // comment_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('user_id'); // user_id (BIGINT, UNSIGNED)
            $table->timestamps(); // created_at, updated_at
            $table->softDeletes(); // deleted_at (TIMESTAMP, NULLABLE)

            // Foreign Key Constraints
            $table->foreign('comment_id')
                ->references('id')
                ->on('video_comments')
                ->onUpdate('restrict')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('restrict')
                ->onDelete('cascade');
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_comment_likes');
    }
}
