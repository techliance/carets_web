<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_licenses")) {
        Schema::create('clm_licenses', function (Blueprint $table) {
            $table->bigIncrements('id'); // Primary Key
            $table->unsignedBigInteger('user_id')->nullable();
            $table->integer('caret_id')->nullable();
            $table->tinyInteger('is_active')->nullable()->default(0);
            $table->tinyInteger('status_id')->nullable()->default(0);
            $table->integer('default_ad')->nullable();
            $table->integer('default_intro')->nullable();
            $table->integer('default_finish')->nullable();
            $table->integer('default_sound')->nullable();
            $table->smallInteger('subscription_plan')->nullable();
            $table->decimal('subscription_amount', 10, 2)->nullable();
            $table->string('subscription_stripe_id', 255)->nullable();
            $table->string('company_name', 255)->nullable();
            $table->string('caret_title', 255)->nullable();
            $table->string('caret_logo', 255)->nullable();
            $table->string('caret_certificate', 255)->nullable();
            $table->string('subscription_status', 255)->nullable();
            $table->string('cancel_at_period_end', 255)->nullable();
            $table->string('current_period_end', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }   
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_licenses');
    }
}
