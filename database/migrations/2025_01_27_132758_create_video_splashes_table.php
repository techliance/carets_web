<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoSplashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_splashes")) {
        Schema::create('video_splashes', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned(); // Primary key (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // Foreign key to 'videos'
            $table->unsignedBigInteger('splash_id'); // Foreign key to 'splash' or similar table
            $table->tinyInteger('finish')->unsigned()->default(0); // Finish flag (default: 0)
            $table->timestamps(); // Adds 'created_at' and 'updated_at'
            $table->softDeletes(); // Adds 'deleted_at' for soft deletes

            // Foreign key constraints (Adjust according to your schema)
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');
            $table->foreign('splash_id')->references('id')->on('clm_splash')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_splashes');
    }
}
