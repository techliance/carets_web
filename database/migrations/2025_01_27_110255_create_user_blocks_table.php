<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_blocks")) {
        Schema::create('user_blocks', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('block_to');  // block_to (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('block_by');  // block_by (BIGINT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('block_to')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // block_to (Foreign Key)
            $table->foreign('block_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // block_by (Foreign Key)

            // Indexes
            $table->index('block_to', 'FK_user_blocks_users');  // block_to (INDEX)
            $table->index('block_by', 'FK_user_blocks_users_2');  // block_by (INDEX)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_blocks');
    }
}
