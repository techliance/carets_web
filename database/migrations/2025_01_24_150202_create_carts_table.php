<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("carts")) {
        Schema::create('carts', function (Blueprint $table) {
            $table->id(); // id as INT(10) with AUTO_INCREMENT
            $table->integer('user_id')->nullable(); // user_id as INT(10) nullable
            $table->string('guest_token')->nullable()->collation('latin1_swedish_ci'); // guest_token as VARCHAR(255)
            $table->tinyInteger('total_items')->nullable(); // total_items as TINYINT(3)
            $table->decimal('total_amount', 10, 2)->nullable(); // total_amount as DECIMAL(10,2)
            $table->timestamps(0); // created_at and updated_at with no fractional seconds

            // Define unique index for guest_token
            $table->unique('guest_token');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
