<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("sessions")) {
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id', 191);  // id (VARCHAR)
            $table->bigInteger('user_id')->unsigned()->nullable();  // user_id (BIGINT, can be null)
            $table->string('ip_address', 45)->nullable();  // ip_address (VARCHAR, can be null)
            $table->text('user_agent')->nullable();  // user_agent (TEXT, can be null)
            $table->text('payload');  // payload (TEXT, NOT NULL)
            $table->integer('last_activity');  // last_activity (INT, NOT NULL)
            
            $table->unique('id');  // Unique index for id
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
