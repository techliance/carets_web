<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("file_uploads")) {
        Schema::create('file_uploads', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->string('unique_id', 255)->collation('utf8mb4_unicode_ci');
            $table->string('file_name', 255)->collation('utf8mb4_unicode_ci');
            $table->integer('total_chunks');
            $table->integer('received_chunks')->default(0);
            $table->enum('status', ['in_progress', 'completed', 'failed'])->default('in_progress')->collation('utf8mb4_unicode_ci');
            $table->timestamps();
                        $table->unique('unique_id'); // Unique index for unique_id
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_uploads');
    }
}
