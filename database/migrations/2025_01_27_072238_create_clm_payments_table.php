<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_payments")) {
        Schema::create('clm_payments', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('license_id')->nullable();
            $table->unsignedInteger('card_id')->nullable();
            $table->decimal('amount', 10, 2)->default(0.00);
            $table->string('transaction_id', 255)->nullable();
            $table->string('latest_invoice', 255)->nullable();
            $table->string('transaction_type', 255)->nullable();
            $table->string('customer', 255)->nullable();
            $table->string('cancel_at', 255)->nullable();
            $table->string('cancel_at_period_end', 255)->nullable();
            $table->string('canceled_at', 255)->nullable();
            $table->string('collection_method', 255)->nullable();
            $table->string('created', 255)->nullable();
            $table->string('current_period_end', 255)->nullable();
            $table->string('current_period_start', 255)->nullable();
            $table->string('days_until_due', 255)->nullable();
            $table->string('default_payment_method', 255)->nullable();
            $table->string('plan_id', 255)->nullable();
            $table->string('plan_currency', 255)->nullable();
            $table->string('plan_interval', 255)->nullable();
            $table->string('plan_interval_count', 255)->nullable();
            $table->string('plan_product', 255)->nullable();
            $table->string('start_date', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->index('user_id', 'FK_clm_payments_users');

            // Foreign key constraints
            $table->foreign('user_id', 'FK_clm_payments_users')
                ->references('id')->on('users')
                ->onUpdate('restrict')
                ->onDelete('restrict');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_payments');
    }
}
