<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("resource_types")) {
        Schema::create('resource_types', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('account_id')->unsigned()->nullable();  // account_id, can be null
            $table->string('title', 191)->nullable();  // title of resource type
            $table->string('guidelines', 191)->nullable();  // guidelines for the resource type
            $table->string('model_type', 191)->nullable();  // model type for the resource
            $table->tinyInteger('status')->nullable();  // status of the resource
            $table->timestamps();  // created_at and updated_at timestamps
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_types');
    }
}
