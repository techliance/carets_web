<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmSplashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_splash")) {
        Schema::create('clm_splash', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->unsignedBigInteger('user_id')->default(0);
            $table->unsignedInteger('license_id')->nullable();
            $table->string('splash_title', 255)->nullable()->collation('latin1_swedish_ci');
            $table->string('video_url', 255)->nullable()->collation('latin1_swedish_ci');
            $table->string('image_url', 255)->nullable()->collation('latin1_swedish_ci');
            $table->tinyInteger('is_active')->unsigned()->default(0)->zerofill();
            $table->tinyInteger('start_default')->unsigned()->default(0);
            $table->tinyInteger('end_default')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();
            
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_splash');
    }
}
