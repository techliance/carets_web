<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFavHashtagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_fav_hashtags")) {
            Schema::create('user_fav_hashtags', function (Blueprint $table) {
                $table->increments('id');  // id (INT, UNSIGNED, AUTO_INCREMENT)
                $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
                $table->unsignedInteger('tag_id');  // tag_id (INT, UNSIGNED)
                $table->timestamps();  // created_at, updated_at
                $table->softDeletes();  // deleted_at (TIMESTAMP)
            
                // Foreign keys
                $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');  
            
                // Verify if the correct table is 'users_tag_videos' or 'video_hashtags'
                $table->foreign('tag_id')->references('id')->on('users_tag_videos')->onUpdate('RESTRICT')->onDelete('RESTRICT');  
            
                // Indexes
                $table->index('user_id', 'FK_user_fav_hashtags_users');  
                $table->index('tag_id', 'FK_user_fav_hashtags_users_tag_videos');  // Updated to match the correct table
            });
        }
        // Schema::create('user_fav_hashtags', function (Blueprint $table) {
        //     $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
        //     $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
        //     $table->unsignedInteger('tag_id');  // tag_id (INT, UNSIGNED)
        //     $table->timestamps();  // created_at, updated_at
        //     $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

        //     // Foreign keys
        //     $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');  // user_id (Foreign Key)
        //     $table->foreign('tag_id')->references('id')->on('users_tag_videos')->onUpdate('RESTRICT')->onDelete('RESTRICT');  // tag_id (Foreign Key)

        //     // Indexes
        //     $table->index('user_id', 'FK_user_fav_hashtags_users');  // user_id (INDEX)
        //     $table->index('tag_id', 'FK_user_fav_hashtags_video_hashtags');  // tag_id (INDEX)
        // });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_fav_hashtags');
    }
}
