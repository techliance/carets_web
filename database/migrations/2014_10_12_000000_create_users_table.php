<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('users', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('name');
        //     $table->string('email')->unique();
        //     $table->timestamp('email_verified_at')->nullable();
        //     $table->string('password');
        //     $table->rememberToken();
        //     $table->timestamps();
        // });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->string('stripe_id', 191)->nullable()->default(null);  // stripe_id (VARCHAR, NULLABLE)
            $table->tinyInteger('is_guest')->nullable()->default(0);  // is_guest (TINYINT, DEFAULT 0)
            $table->string('name', 191)->nullable()->default(null);  // name (VARCHAR, NULLABLE)
            $table->string('email', 191)->nullable()->default(null);  // email (VARCHAR, NULLABLE)
            $table->string('phone', 191)->nullable()->default(null);  // phone (VARCHAR, NULLABLE)
            $table->string('username', 191)->nullable()->default(null);  // username (VARCHAR, NULLABLE)
            $table->timestamp('email_verified_at')->nullable();  // email_verified_at (TIMESTAMP)
            $table->timestamp('phone_verified_at')->nullable();  // phone_verified_at (TIMESTAMP)
            $table->string('password', 191)->nullable()->default(null);  // password (VARCHAR, NULLABLE)
            $table->tinyInteger('is_active')->nullable()->default(0);  // is_active (TINYINT, DEFAULT 0)
            $table->tinyInteger('is_blocked')->nullable()->default(0);  // is_blocked (TINYINT, DEFAULT 0)
            $table->integer('follow_to_count')->default(0);  // follow_to_count (INT, DEFAULT 0)
            $table->integer('follow_by_count')->default(0);  // follow_by_count (INT, DEFAULT 0)
            $table->integer('video_like_count')->default(0);  // video_like_count (INT, DEFAULT 0)
            $table->integer('video_count')->default(0);  // video_count (INT, DEFAULT 0)
            $table->integer('caret_count')->default(0);  // caret_count (INT, DEFAULT 0)
            $table->string('remember_token', 100)->nullable();  // remember_token (VARCHAR, NULLABLE)
            $table->string('fb_token', 255)->nullable();  // fb_token (VARCHAR, NULLABLE)
            $table->string('google_token', 255)->nullable();  // google_token (VARCHAR, NULLABLE)
            $table->string('ios_token', 255)->nullable();  // ios_token (VARCHAR, NULLABLE)
            $table->string('device_id', 255)->nullable();  // device_id (VARCHAR, NULLABLE)
            $table->string('device_type', 255)->nullable();  // device_type (VARCHAR, NULLABLE)
            $table->string('device_model', 255)->nullable();  // device_model (VARCHAR, NULLABLE)
            $table->string('device_token', 255)->nullable();  // device_token (VARCHAR, NULLABLE)
            $table->string('device_os', 255)->nullable();  // device_os (VARCHAR, NULLABLE)
            $table->string('firestore_reference_id', 255)->nullable();  // firestore_reference_id (VARCHAR, NULLABLE)
            $table->string('activation_token', 255)->nullable();  // activation_token (VARCHAR, NULLABLE)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('last_visit')->nullable();  // last_visit (TIMESTAMP)
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
