<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("cart_items")) {
        Schema::create('cart_items', function (Blueprint $table) {
            $table->id(); // id as INT(10) with AUTO_INCREMENT
            $table->integer('cart_id'); // cart_id as INT(10)
            $table->string('caret_title')->nullable()->collation('latin1_swedish_ci'); // caret_title as VARCHAR(255)
            $table->string('subscription_stripe_id')->nullable()->collation('latin1_swedish_ci'); // subscription_stripe_id as VARCHAR(255)
            $table->integer('subscription_pricing_id')->nullable(); // subscription_pricing_id as INT(10)
            $table->integer('interval_count')->nullable(); // interval_count as INT(10)
            $table->decimal('subscription_amount', 20, 2)->nullable(); // subscription_amount as DECIMAL(20,2)
            $table->timestamps(0); // created_at and updated_at with no fractional seconds

        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
    }
}
