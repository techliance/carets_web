<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchCaretCreatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("search_caret_created")) {
        Schema::create('search_caret_created', function (Blueprint $table) {
            $table->bigIncrements('id');  // id (BIGINT, AUTO_INCREMENT)
            $table->bigInteger('video_id')->nullable();  // video_id (BIGINT, can be null)
            $table->integer('like_count')->nullable();  // like_count (INT, can be null)
            $table->integer('age')->nullable();  // age (INT, can be null)
            $table->timestamps();  // created_at and updated_at
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_caret_created');
    }
}
