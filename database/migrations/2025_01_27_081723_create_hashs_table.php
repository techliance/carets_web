<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHashsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        if (!Schema::hasTable("hashs")) {
            Schema::create('hashs', function (Blueprint $table) {
                $table->id(); // Auto-increment primary key
                $table->string('title', 255)->nullable()->collation('latin1_swedish_ci');
                $table->integer('category_id')->nullable();
                
                // Corrected 'is_active' definition
                $table->tinyInteger('is_active')->unsigned()->default(0);
            
                $table->timestamps(); // created_at, updated_at
                $table->softDeletes(); // deleted_at for soft deletes
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashs');
    }
}
