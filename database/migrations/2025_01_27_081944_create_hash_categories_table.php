<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHashCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("hash_categories")) {
        Schema::create('hash_categories', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->string('title')->nullable(); // title column
            $table->boolean('is_active')->default(0); // is_active column
            $table->timestamps(); // created_at, updated_at
            $table->softDeletes(); // deleted_at for soft deletes

        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hash_categories');
    }
}
