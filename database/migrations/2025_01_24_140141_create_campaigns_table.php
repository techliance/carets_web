<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("campaigns")) {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned(); // id as a BIGINT(20) UNSIGNED with AUTO_INCREMENT
            $table->unsignedBigInteger('user_id')->default(0)->nullable(); // user_id as BIGINT(20) UNSIGNED, nullable
            $table->string('campaign_title', 255)->nullable()->default(null); // campaign_title as VARCHAR(255)
            $table->tinyInteger('is_active')->default(0)->nullable(); // is_active as TINYINT(1), default 0
            $table->tinyInteger('is_blocked')->default(0)->nullable(); // is_blocked as TINYINT(1), default 0
            $table->timestamps(0); // Timestamps for created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes
            
            // Add the foreign key constraint on user_id
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('restrict')
                ->onDelete('restrict');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
