<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaretsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("carets")) {
        Schema::create('carets', function (Blueprint $table) {
            $table->id(); // id as INT(11) UNSIGNED with AUTO_INCREMENT
            $table->string('title', 255)->nullable()->default(null); // title as VARCHAR(255)
            $table->tinyInteger('is_active')->unsigned()->nullable()->default(0); // is_active as TINYINT(1) UNSIGNED
            $table->timestamps(0); // Timestamps for created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes

        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carets');
    }
}
