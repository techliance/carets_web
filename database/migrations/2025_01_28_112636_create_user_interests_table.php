<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_interests")) {
            Schema::create('user_interests', function (Blueprint $table) {
                $table->increments('id')->unsigned(); // Auto-increment primary key
                $table->unsignedBigInteger('user_id'); // User ID (Not FK)
                $table->unsignedInteger('cat_id'); // Category ID (Not FK)
                $table->timestamps(); // created_at & updated_at
                $table->softDeletes(); // deleted_at for soft deletes
            
            });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_interests');
    }
}
