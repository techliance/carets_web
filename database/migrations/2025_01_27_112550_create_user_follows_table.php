<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFollowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_follows")) {
        Schema::create('user_follows', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('follow_to');  // follow_to (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('follow_by');  // follow_by (BIGINT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('follow_to')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // follow_to (Foreign Key)
            $table->foreign('follow_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // follow_by (Foreign Key)

            // Indexes
            $table->index('follow_to', 'FK_user_follows_users');  // follow_to (INDEX)
            $table->index('follow_by', 'FK_user_follows_users_2');  // follow_by (INDEX)
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_follows');
    }
}
