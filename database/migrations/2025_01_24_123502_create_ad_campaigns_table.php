<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ad_campaigns")) {
            Schema::create('ad_campaigns', function (Blueprint $table) {
                $table->id(); // Primary key
                $table->unsignedBigInteger('user_id')->nullable()->default(0); // User ID
                $table->unsignedBigInteger('ad_id')->nullable()->default(0); // Ad ID
                $table->unsignedBigInteger('plan_id')->nullable(); // Plan ID (changed to unsignedBigInteger)
                $table->unsignedBigInteger('ages')->nullable(); // Age ID as unsignedBigInteger
                $table->unsignedInteger('gender')->default(0); // Gender ID
                $table->string('location', 255)->nullable(); // Location
                $table->string('campaign_title', 255)->nullable(); // Campaign Title
                $table->boolean('is_active')->nullable()->default(0); // Is Active
                $table->boolean('is_blocked')->nullable()->default(0); // Is Blocked
                $table->unsignedInteger('watch_count')->nullable()->default(0); // Watch Count
                $table->boolean('recurring')->nullable()->default(1); // Recurring
                $table->string('subscription_status', 255)->nullable(); // Subscription Status
                $table->string('cancel_at_period_end', 255)->nullable(); // Cancel at Period End
                $table->string('current_period_end', 255)->nullable(); // Current Period End
                $table->timestamps(); // created_at and updated_at
                $table->softDeletes(); // deleted_at
            
                // Indexes
                $table->index('user_id', 'FK_ad_campaigns_users');
                $table->index('ad_id', 'FK_ad_campaigns_ads');
                $table->index('plan_id', 'FK_ad_campaigns_ad_subscription_plans');
                $table->index('ages', 'FK_ad_campaigns_ages');
                $table->index('gender', 'FK_ad_campaigns_genders');
            
                // Foreign Key Constraints
                $table->foreign('user_id', 'FK_ad_campaigns_users')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');
                $table->foreign('ad_id', 'FK_ad_campaigns_ads')->references('id')->on('ads')->onUpdate('restrict')->onDelete('restrict');
                $table->foreign('plan_id', 'FK_ad_campaigns_ad_subscription_plans')->references('id')->on('ad_plans')->onUpdate('restrict')->onDelete('restrict');
                $table->foreign('ages', 'FK_ad_campaigns_ages')->references('id')->on('ages')->onUpdate('restrict')->onDelete('restrict');
                $table->foreign('gender', 'FK_ad_campaigns_genders')->references('id')->on('genders')->onUpdate('restrict')->onDelete('restrict');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_campaigns');
    }
}
