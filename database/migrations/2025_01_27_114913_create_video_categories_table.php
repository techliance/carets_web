<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_categories")) {
        Schema::create('video_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned(); // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->string('title', 255)->nullable(); // title (VARCHAR, NULLABLE)
            $table->unsignedTinyInteger('is_active')->default(0); // is_active (TINYINT, UNSIGNED, ZEROFILL, DEFAULT 0)
            $table->timestamps(); // created_at, updated_at
            $table->timestamp('deleted_at')->nullable(); // deleted_at (TIMESTAMP, NULLABLE)
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_categories');
    }
}
