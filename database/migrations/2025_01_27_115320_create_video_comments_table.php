<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_comments")) {
        Schema::create('video_comments', function (Blueprint $table) {
            $table->bigIncrements('id'); // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // video_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('user_id'); // user_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('parent_id')->nullable(); // parent_id (BIGINT, UNSIGNED, NULLABLE)
            $table->text('comments'); // comments (TINYTEXT in SQL)
            $table->integer('like_count')->default(0); // like_count (INT, DEFAULT 0)
            $table->timestamps(); // created_at, updated_at
            $table->softDeletes(); // deleted_at (TIMESTAMP, NULLABLE)

            // Foreign Key Constraints
            $table->foreign('video_id')
                ->references('id')
                ->on('videos')
                ->onUpdate('restrict')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('restrict')
                ->onDelete('cascade');
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_comments');
    }
}
