<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_reports")) {
        Schema::create('user_reports', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('report_to');  // report_to (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('report_by');  // report_by (BIGINT, UNSIGNED)
            $table->text('comments')->nullable()->default(null);  // comments (TEXT, COLLATE 'latin1_swedish_ci')
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('report_to')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // report_to (Foreign Key)
            $table->foreign('report_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // report_by (Foreign Key)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_reports');
    }
}
