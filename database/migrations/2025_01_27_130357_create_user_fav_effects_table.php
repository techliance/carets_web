<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFavEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_fav_effects")) {
        Schema::create('user_fav_effects', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
            $table->unsignedInteger('effect_id');  // effect_id (INT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');  // user_id (Foreign Key)
            $table->foreign('effect_id')->references('id')->on('video_effects')->onUpdate('RESTRICT')->onDelete('CASCADE');  // effect_id (Foreign Key)

            // Indexes
            $table->index('user_id', 'FK_user_fav_effects_users');  // user_id (INDEX)
            $table->index('effect_id', 'FK_user_fav_effects_video_effects');  // effect_id (INDEX)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_fav_effects');
    }
}
