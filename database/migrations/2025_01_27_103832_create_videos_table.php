<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("videos")) {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('sound_id')->nullable();  // sound_id (BIGINT, UNSIGNED, NULL)
            $table->boolean('is_private')->default(0);  // is_private (TINYINT, UNSIGNED)
            $table->string('video_title', 255)->nullable();  // video_title (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->unsignedBigInteger('age')->nullable();  // age (INT, UNSIGNED)
            $table->unsignedBigInteger('license_id')->nullable();  // license_id (INT, UNSIGNED)
            $table->boolean('is_manual')->default(0);  // is_manual (TINYINT, UNSIGNED)
            $table->boolean('caret_atempts')->default(0);  // caret_atempts (TINYINT, UNSIGNED)
            $table->boolean('caret_processing')->default(0);  // caret_processing (TINYINT, UNSIGNED)
            $table->boolean('is_caret')->default(0)->comment('video / collage');  // is_caret (TINYINT, UNSIGNED)
            $table->boolean('is_marked')->default(0);  // is_marked (TINYINT, UNSIGNED)
            $table->boolean('is_marked_distance')->default(0);  // is_marked_distance (TINYINT, UNSIGNED)
            $table->boolean('is_processing_caret')->default(0);  // is_processing_caret (TINYINT, UNSIGNED)
            $table->boolean('tag_video_process')->default(0);  // tag_video_process (TINYINT, UNSIGNED)
            $table->boolean('distance_video_process')->default(0);  // distance_video_process (TINYINT, UNSIGNED)
            $table->unsignedTinyInteger('tag_retry_count')->default(0);  // tag_retry_count (TINYINT, UNSIGNED)
            $table->unsignedTinyInteger('distance_retry_count')->default(0);  // distance_retry_count (TINYINT, UNSIGNED)
            $table->string('video_raw_url', 255)->nullable();  // video_raw_url (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('video_url', 255)->nullable();  // video_url (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('video_export_url', 255)->nullable();  // video_export_url (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('image_url', 255)->nullable();  // image_url (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('video_share_link', 255)->nullable()->unique();  // video_share_link (VARCHAR, UNIQUE, COLLATE 'latin1_swedish_ci')
            $table->text('video_description')->nullable();  // video_description (TEXT, COLLATE 'utf8mb4_general_ci')
            $table->boolean('is_active')->default(0);  // is_active (TINYINT)
            $table->boolean('is_blocked')->default(0);  // is_blocked (TINYINT)
            $table->unsignedInteger('like_count')->default(0);  // like_count (INT, UNSIGNED, ZEROFILL)
            $table->unsignedInteger('watch_count')->default(0);  // watch_count (INT, UNSIGNED)
            $table->unsignedInteger('share_count')->default(0);  // share_count (INT, UNSIGNED, ZEROFILL)
            $table->unsignedInteger('comments_count')->default(0);  // comments_count (INT, UNSIGNED)
            $table->string('loc_lat', 255)->nullable();  // loc_lat (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('loc_long', 255)->nullable();  // loc_long (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->string('hashtags', 255)->nullable();  // hashtags (VARCHAR, COLLATE 'latin1_swedish_ci')
            $table->boolean('is_allow_comments')->default(0);  // is_allow_comments (TINYINT, UNSIGNED)
            $table->boolean('is_allow_caretCreation')->default(0);  // is_allow_caretCreation (TINYINT, UNSIGNED)
            $table->unsignedInteger('duration')->default(0);  // duration (INT, UNSIGNED)
            $table->timestamp('start_time')->nullable();  // start_time (TIMESTAMP)
            $table->timestamp('end_time')->nullable();  // end_time (TIMESTAMP)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('sound_id')->references('id')->on('sounds')->onUpdate('RESTRICT')->onDelete('RESTRICT');  // sound_id (Foreign Key)
            $table->foreign('age')->references('id')->on('ages')->onUpdate('RESTRICT')->onDelete('RESTRICT');  // age (Foreign Key)
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
