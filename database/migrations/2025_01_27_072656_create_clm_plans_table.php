<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_plans")) {
        Schema::create('clm_plans', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->string('stripe_id', 255)->default('0');
            $table->string('title', 255)->nullable();
            $table->string('duration', 50)->nullable();
            $table->integer('interval_count')->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->tinyInteger('is_active')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_plans');
    }
}
