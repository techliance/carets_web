<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_reports")) {
        Schema::create('video_reports', function (Blueprint $table) {
            $table->increments('id'); // Primary key (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // Foreign key to 'videos'
            $table->unsignedBigInteger('report_by'); // Foreign key to 'users'
            $table->text('remarks')->nullable(); // Allows longer remarks
            $table->tinyInteger('is_active')->unsigned()->default(1); // Active flag (default: 1)
            $table->timestamps(); // Adds 'created_at' and 'updated_at'
            $table->softDeletes(); // Adds 'deleted_at' for soft deletes

            // Foreign key constraints
            $table->foreign('video_id')->references('id')->on('videos')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');
            $table->foreign('report_by')->references('id')->on('users')
                  ->onUpdate('restrict')
                  ->onDelete('cascade');

            // Indexes
            $table->index('video_id', 'FK_video_reports_videos');
            $table->index('report_by', 'FK_video_reports_users');
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_reports');
    }
}
