<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoCollagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("video_collages")) {
        Schema::create('video_collages', function (Blueprint $table) {
            $table->bigIncrements('id'); // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('video_id'); // video_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('video_added'); // video_added (BIGINT, UNSIGNED)
            $table->tinyInteger('start_order')->nullable(); // start_order (TINYINT, NULLABLE)
            $table->integer('duration')->nullable(); // duration (INT, NULLABLE)
            $table->timestamps(); // created_at, updated_at
            $table->softDeletes(); // deleted_at (TIMESTAMP, NULLABLE)

            // Foreign Keys
            $table->foreign('video_id')
                ->references('id')
                ->on('videos')
                ->onUpdate('restrict')
                ->onDelete('cascade');

            $table->foreign('video_added')
                ->references('id')
                ->on('videos')
                ->onUpdate('restrict')
                ->onDelete('cascade');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_collages');
    }
}
