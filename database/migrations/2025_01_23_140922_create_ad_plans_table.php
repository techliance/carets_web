<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("ad_plans")) {
            Schema::create('ad_plans', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();  // id (BIGINT, UNSIGNED, AUTO_INCREMENT)
                $table->string('stripe_id', 255)->default('0');
                $table->string('title', 255)->nullable(); // Nullable title
                $table->string('position', 50)->nullable(); // Nullable position
                $table->string('duration', 50)->nullable(); // Nullable duration
                $table->decimal('amount', 10, 2)->nullable(); // Nullable amount with decimal precision
                $table->tinyInteger('is_active')->nullable()->unsigned(); // Nullable unsigned tinyInteger for 'is_active'
                $table->timestamps(0); // Timestamps with no fractional seconds
                $table->softDeletes(); // Adds deleted_at for soft deletes
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_plans');
    }
}
