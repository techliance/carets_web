<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("countries")) {
        Schema::create('countries', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->string('name', 200)->collation('utf8_general_ci');
            $table->char('code2', 10)->nullable()->collation('utf8_general_ci');
            $table->char('code3', 10)->collation('utf8_general_ci');
            $table->tinyInteger('status')->nullable()->default(0);
            $table->integer('sortOrder', false, true)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
