<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHashsVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("hashs_videos")) {
        Schema::create('hashs_videos', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->foreignId('hash_id')->constrained('hashs')->onUpdate('restrict')->onDelete('cascade');
            $table->foreignId('video_id')->constrained('videos')->onUpdate('restrict')->onDelete('cascade');
            $table->timestamps(); // created_at, updated_at
            $table->softDeletes(); // deleted_at for soft deletes

        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashs_videos');
    }
}
