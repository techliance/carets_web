<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("cms_pages")) {
            Schema::create('cms_pages', function (Blueprint $table) {
                $table->id(); // Auto-increment primary key
                $table->unsignedBigInteger('created_by')->default(0);
                $table->unsignedBigInteger('updated_by')->default(0);
                $table->string('page_title', 255)->nullable()->collation('latin1_swedish_ci');
                $table->string('page_slug', 255)->nullable()->collation('latin1_swedish_ci');
                $table->text('page_description')->nullable()->collation('latin1_swedish_ci');
                $table->string('meta_title', 255)->nullable()->collation('latin1_swedish_ci');
            
                // Fix tinyText() issue
                $table->string('meta_description', 255)->nullable()->collation('latin1_swedish_ci');
                $table->string('meta_tags', 255)->nullable()->collation('latin1_swedish_ci');
            
                $table->tinyInteger('is_active')->unsigned()->default(0)->zerofill();
                $table->timestamps();
                $table->softDeletes();
                
                $table->unique('page_slug'); // Unique index on page_slug
                $table->index('created_by'); // Foreign key index
                $table->index('updated_by'); // Foreign key index
                
                $table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pages');
    }
}
