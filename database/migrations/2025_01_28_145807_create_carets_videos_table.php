<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaretsVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("carets_videos")) {
        Schema::create('carets_videos', function (Blueprint $table) {
            $table->id(); // id as INT(10) UNSIGNED with AUTO_INCREMENT
            $table->unsignedBigInteger('caret_id'); // caret_id as INT(11) UNSIGNED
            $table->unsignedBigInteger('video_id')->default(0); // video_id as BIGINT(20) UNSIGNED with a default value of 0
            $table->timestamps(0); // Timestamps for created_at and updated_at with no fractional seconds
            $table->softDeletes(); // Adds deleted_at for soft deletes

            // Foreign key constraints
            $table->foreign('caret_id')->references('id')->on('carets')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('video_id')->references('id')->on('videos')->onUpdate('RESTRICT')->onDelete('CASCADE');

            // Indexes for better query performance
            $table->index('caret_id');
            $table->index('video_id');

        
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carets_videos');
    }
}
