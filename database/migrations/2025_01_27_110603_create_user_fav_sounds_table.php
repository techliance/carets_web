<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFavSoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("user_fav_sounds")) {
        Schema::create('user_fav_sounds', function (Blueprint $table) {
            $table->increments('id')->unsigned();  // id (INT, UNSIGNED, AUTO_INCREMENT)
            $table->unsignedBigInteger('user_id');  // user_id (BIGINT, UNSIGNED)
            $table->unsignedBigInteger('sound_id');  // sound_id (BIGINT, UNSIGNED)
            $table->timestamps();  // created_at, updated_at
            $table->timestamp('deleted_at')->nullable();  // deleted_at (TIMESTAMP)

            // Foreign keys
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');  // user_id (Foreign Key)
            $table->foreign('sound_id')->references('id')->on('sounds')->onUpdate('RESTRICT')->onDelete('RESTRICT');  // sound_id (Foreign Key)

            // Indexes
            $table->index('user_id', 'FK_user_fav_sounds_users');  // user_id (INDEX)
            $table->index('sound_id', 'FK_user_fav_sounds_video_sounds');  // sound_id (INDEX)
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_fav_sounds');
    }
}
