<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClmOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("clm_order_payments")) {
        Schema::create('clm_order_payments', function (Blueprint $table) {
            $table->id(); // Auto-increment primary key
            $table->unsignedBigInteger('order_id'); // Foreign key to orders
            $table->enum('payment_method', ['stripe', 'paypal', 'bank_transfer'])->nullable();
            $table->string('transaction_id', 255); // Transaction identifier
            $table->decimal('amount', 20, 2); // Payment amount
            $table->enum('status', ['pending', 'completed', 'failed', 'refunded'])->default('pending'); // Payment status
            $table->timestamps(); // created_at and updated_at fields
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clm_order_payments');
    }
}
