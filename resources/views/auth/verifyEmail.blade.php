@extends('layouts.email2')
@section('heading')

    @if($activation_token)
        Hi,
        Please click the link to verify your email address.
    @endif
@endsection

@section('content')

    @if($activation_token)
        Token: <br /><br />{{ $activation_token }}

    @endif
    <p>Regards,</p>
    <p>Carets.tv</p>
@endsection



