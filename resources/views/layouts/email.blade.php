<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700" rel="stylesheet">
    <title>Email</title>


</head>
<body style="font-family: 'PT Sans', sans-serif; font-weight: 500; margin: 0; padding: 0; background: #fff;">
<div style="width: 800px; margin: 0 auto; padding: 0 25px;">
@yield('content')
</div>
</body>
</html>