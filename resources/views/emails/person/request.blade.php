@extends('layouts.email')
@section('content')

    @if($data)
        Hi,
        <br /><br />{!! $msg !!}

        <br /><br />Reported By:<br /><br />

        @foreach ($data as $key => $value)
            <p><b>{!! $key !!}: </b> {!! $value !!}</p>
        @endforeach

    @endif
    <p>Regards,</p>
    <p>Carets.tv</p>
@endsection

