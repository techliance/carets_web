@extends('layouts.email2')
@section('heading')

    @if($data)
        Hi {{ $data['name'] }},

    @endif
@endsection

@section('content')

    @if($data)
        <br /><br />{{ $data['message'] }}

    @endif
    <p>Regards,</p>
    <p>Carets.tv</p>
@endsection



