import React, { Component } from 'react'
import { connect } from 'react-redux'
import SideNav from 'react-simple-sidenav';
import { IoMdClose } from 'react-icons/io';
import FrontEndRoutes from '../../../routes/FrontEndRoutes';
import User from '../../../helper/User';
import { Link } from 'react-router-dom'
import { orderActions } from '../../../redux/actions/order-actions';
import { Roles } from '../../../constants/Roles';
class SideNavDashboard extends Component {

    render() {
        const navLinks = [];
        FrontEndRoutes.forEach((item, index) => {
            if ('role' in item && !item.showMenu && item.showInSidebar !== false) {
                if (User.hasRole(item.role)) {
                    if ('render' in item) {
                        if (User.hasRole(Roles.PHOTO_ADMIN) && 'needCameraShipment' in item) {
                            if ((User.getProperty('cameraShipped') && item.needCameraShipment) || ((!User.getProperty('cameraShipped') && !item.needCameraShipment) && ('needUploadApproved' in item && item.needUploadApproved === true && User.getProperty('upload_approved')))) {
                                navLinks.push(<item.render key={index}  {...item.props}>
                                    {item.navIcon ? <i className={item.navIcon}></i> : ""}
                                </item.render>);
                            }
                        } else {
                            navLinks.push(<item.render key={index}  {...item.props}>
                                {item.navIcon ? <i className={item.navIcon}></i> : ""}
                            </item.render>);
                        }
                    } else {
                        if (item.name === 'Logout') {
                            navLinks.push(<a href='/logout' onClick={this.props.logout} key={index} >{item.navIcon ? <i className={item.navIcon}></i> : ""}{item.name}</a>);
                        } else {
                            if ((User.hasRole([Roles.PHOTO_ADMIN, Roles.TRAVELER]) && 'needCameraShipment' in item) && ('needUploadApproved' in item && item.needUploadApproved && User.getProperty('upload_approved'))) {
                                if ((User.getProperty('cameraShipped') && item.needCameraShipment) || (!User.getProperty('cameraShipped') && !item.needCameraShipment)) {
                                    navLinks.push(<Link onClick={this.props.handleNav} key={index} to={item.path.replace(/(\/:[\w]*\??)+/, '')}>
                                        {item.navIcon ? <i className={item.navIcon}></i> : ""}{item.name}
                                    </Link>);
                                }
                            } else {
                                if ('needUploadApproved' in item && item.needUploadApproved === true && User.getProperty('upload_approved')) {
                                    navLinks.push(<Link onClick={this.props.handleNav} key={index} to={item.path.replace(/(\/:[\w]*\??)+/, '')}>
                                        {item.navIcon ? <i className={item.navIcon}></i> : ""}{item.name}
                                    </Link>);
                                } else {
                                    navLinks.push(<Link onClick={this.props.handleNav} key={index} to={item.path.replace(/(\/:[\w]*\??)+/, '')}>
                                        {item.navIcon ? <i className={item.navIcon}></i> : ""}{item.name}
                                    </Link>);
                                }
                            }
                        }
                    }
                }
            }
        })
        return (
            <SideNav
                openFromRight={true}
                className="sideNav"
                showNav={this.props.showNav}
                onHideNav={this.props.handleNav}
                title={
                    <button className="sideNavBtn dark" onClick={this.props.handleNav}><IoMdClose /></button>
                }
                titleStyle={{ backgroundColor: '#2196F3' }}
                items={
                    navLinks
                } />
        )
    }
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        getOrderCall: (orderId) => { dispatch(orderActions.getOrder(orderId)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideNavDashboard)
