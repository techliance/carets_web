
import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  OverlayTrigger,
  Popover
} from "react-bootstrap";

// import CorporatePlanForm from "./CorporatePlanForm";

import { objectsConstants as OC } from 'redux/constant/objects-constant';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { Card } from "components/Card/Card.jsx";
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';
import { FaRegImage } from "react-icons/fa";
import { RiSpam2Fill, RiVideoLine } from 'react-icons/ri';
import { FaCreditCard } from "react-icons/fa";

// import LicenseForm from "./LicenseForm";
import { hasPermission } from 'helper/hasPermission';
import Confirmalertfordelete from '../../ThemeComponents/confirmAlertForDelete';
import { adminLabels } from '../../../redux/constant/admin-label-constant';
import OpenModalButton from '../../ThemeComponents/openModelButton';
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import { ThemeFunctions as TF } from 'helper/ThemeFunctions';
import Pagination from '../../ThemeComponents/Pagination';
import DatePicker from "react-datepicker";
import { splashService } from '../../../services/splash';
import { licenseService } from '../../../services/license';
import { corporateService } from '../../../services/corporate';
import { clmPlanService } from '../../../services/CLMplan';

import ReactValidator from "simple-react-validator";
import "react-datepicker/dist/react-datepicker.css";
import Switch from "react-switch";
import Eye from "../../../assets/img/eye.svg"


var cloneDeep = require('lodash.clonedeep');
var Modal = require('react-bootstrap-modal');
const moment = window.moment;

class CorporateListingPage extends Component {
  constructor(props) {
    super(props);
    const { id } = this.props.match.params;
    const user = JSON.parse(localStorage.getItem('user'));
    // ====================================
    this.dateValidator = new ReactValidator();
    var today = new Date();
    today.setDate(today.getDate() + 1);
    var lastmonth = new Date();
    lastmonth.setMonth(lastmonth.getMonth() - 1);
    var startDate = lastmonth.getFullYear() + '-' + (lastmonth.getMonth() + 1) + '-' + lastmonth.getDate();
    var endDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    // ====================================
    this.state = {
      addModalForm: false,
      editModalForm: false,
      openVideoModal: false,
      campaignModal: false,
      campaignData: OC.CAMPAIGN,
      video_url: '',
      selectedVideo: [],
      sorted: '',
      filtered: '',
      pageSize: 10,
      userID: id,
      statusList: [],
      
      
      // search: { a: 1 },
      search:{
        fromDate : null,
        toDate: null,
        // status_id:false,
        // is_reported:false
      },
      subscription_plan:'',
      subscription_amount:'',
      value:"",
      caret_id:'',
      caret_title:'',
      company_name:'',
      user_id: '',
    };

    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
    this.deleteConfirmModal = this.deleteConfirmModal.bind(this);
    this.getPaginationData = this.getPaginationData.bind(this);
    this.filterSearchHandle = this.filterSearchHandle.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.getUsersCall = this.getUsersCall.bind(this);
    this.videoModal = this.videoModal.bind(this);
    this.getDurationsCall= this.getDurationsCall.bind(this);
    this.closeModal = HF.closeModal.bind(this);
    this.callFunction = this.callFunction.bind(this);
    this.handleSubmitPlan = this.handleSubmitPlan.bind(this);

  };

  callFunction(formData, formType) {
    this.setState({ campaignData: false });
  }

  videoModal(url) {
    this.setState({video_url:url}, ()=>{
        console.log('video_url',this.state.video_url);
        this.setState({openVideoModal:true});
        console.log('uopenVideoModalrl', this.state.openVideoModal);
        console.log('url',this.state.video_url);
    });

  }
  
  submitForm(formData, formType) {
    if (formType === 'add') {
      licenseService.storeLicense(formData).then(res => {
        this.setState({ addModalForm: false });
        this.pagination.dataCall();
      });
    } else if (formType === 'edit') {
      licenseService.editLicense(formData, this.state.editRec.id).then(res => {
        this.setState({ editModalForm: false });
        this.pagination.dataCall();
      });
    }
  }

  filterSearchHandle(e) {
    this.setState({
      'filtered': e.target.value
    });
  }

  editRecord(id) {
    licenseService.getLicense(id).then(res => {
      console.log(res);
      this.setState({ editRec: res.data }, () => {
        this.setState({ editModalForm: true });
      });
    });
  }

  // showCampaignModal = (id, user_id) => {

  //   let cmpdata = { ...this.state.campaignData };
  //   cmpdata['user_id'] = user_id;
  //   cmpdata['ad_id'] = id;
  //   this.setState({ campaignData: cmpdata });
  //   this.setState({ campaignModal: true });
  // }


  deleteRecord(recID) {
    licenseService.deleteLicense(recID).then(res => {
      this.pagination.dataCall();
    });
  }

  deleteConfirmModal(recID) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <Confirmalertfordelete typeName="Record" description="Are you sure to delete the Record?" onClosePro={onClose} deleteType={() => this.deleteRecord(recID)} />
        )
      }
    })
  }


  getPaginationData(page, filter, sort, pageSize, status) {
    this.setState({ pageSize: pageSize }, function () {
      corporateService.paginationData(this.state.userID, page, filter, sort, pageSize, status, this.state.search);
    });
  }

  updateStatus = recID => (e) => {
    // return false;
    licenseService.updateLicenseStatus(recID, { is_active: e.target.checked ? 1 : 0 }).then(
      response => {
        corporateService.paginationData(this.state.userID, this.props.current_page, '', '', this.state.pageSize, this.state.rec_status, this.state.search);
        this.pagination.dataCall();
      }
    );
  }

  updateBlocked = recID => (e) => {
    // return false;
    splashService.blockRecord(recID).then(
      response => {
        corporateService.paginationData(this.state.userID, this.props.current_page, '', '', this.state.pageSize, this.state.rec_status, this.state.search);
        this.pagination.dataCall();
      }
    );
  }



  

  componentDidUpdate(prevProps, prevState) {
    if (this.state.rec_status !== prevState.rec_status) {
      this.table.state.onFetchData(this.table.state, this.table.instance)
    }

  }

  paginationCall = (data) => {
    return corporateService.paginationData(this.state.userID, data.page, data.filter, data.sort, data.pageSize, this.state.rec_status, this.state.search);
  }


  getUsersCall() {
    licenseService.getUsers().then(res => {
      console.log('Users', res);
      this.setState({ users: res.data });
    });
  }

  componentDidMount() {
    this.getUsersCall();
    this.getStatusCall();
  }


  getStatusCall = (val) => {
    licenseService.getStatus(val).then(res => {
        console.log('Status', res);
        this.setState({statusList:res.data});
        });
    }


  handleUpdateLicenseStatus = (license_id, status_id) => {
    const data = {
      license_id: license_id,
      status_id: status_id
  
    };
    // console.log("data", data)
    licenseService.licenseStatus(data).then((res) => {
      // console.log(res)
      this.pagination.dataCall();
    });
  }

  DateChangeHandler = (value, field) => {
    const temp = cloneDeep(this.state.search);
    temp[field] = value === null ? '' : moment(value).format("YYYY-MM-DD");

    this.setState({ search : temp  }, () => {

        if (this.dateValidator.allValid()) {
          corporateService.paginationData(this.state.userID, this.props.current_page, '', '', this.state.pageSize, this.state.rec_status, this.state.search);
          this.pagination.dataCall();
        } else {
            this.dateValidator.showMessages();
            this.forceUpdate();
        }
    })
  }
  dateElement(label, name, value, rules) {
    var selectedDate =  this.state.search[name] !== '' ? new Date(this.state.search[name]) :'';
    const { fromDate, toDate } = this.state.search;
    console.log(value);
    return (
        <div className='form-group fullCalendar' id={name}>
              <label>{label}</label>
              <DatePicker className="form-control calender_icon"  selected={  selectedDate } name={name} onChange={ (event) => this.DateChangeHandler(event, name) } />
              {
                  ( fromDate !== '' && toDate !== '' ) &&
                  this.dateValidator.message(name, value, rules)
              }
        </div>
    );
  }

  handleCheckPending = (checked) => {
    const temp = cloneDeep(this.state.search);
    temp['status_id'] = checked;
    this.setState({ search : temp  }, () => {
      corporateService.paginationData(this.state.userID, this.props.current_page, '', '', this.state.pageSize, this.state.rec_status, this.state.search);
      this.pagination.dataCall();
    });
  }


  // ===================
  
componentDidMount() {
  this.getDurationsCall();
}
  getDurationsCall(){
    clmPlanService.getCLMDuration().then(res => {
        console.log('Durations',res);
        this.setState({durations:res.data});
      });
  }

  submitForm(formData, formType) {
    if (formType === 'add'){
        clmPlanService.storeCLMPlan(formData).then(res => {
          console.log("========> ",res);
            this.setState({
              addModalForm:false,
              subscription_plan:res.data.id,
              subscription_amount:res.data.amount 
            });
            this.handleSubmitPlan();
            this.pagination.dataCall();
          });
    }else if (formType === 'edit'){
        clmPlanService.editCLMPlan(formData, this.state.editRec.id).then(res => {
            this.setState({editModalForm:false});
            this.pagination.dataCall();
          });
    }
  }

  handleSubmitPlan(){
    const data = {
      subscription_plan: this.state.subscription_plan,
      subscription_amount: this.state.subscription_amount,
      id: this.state.value,
      caret_id: this.state.caret_id,
      caret_title: this.state.caret_title,
      user_id: this.state.user_id,
      company_name: this.state.company_name
    }
    // this.props.submitForm(this.state.license, this.state.formType);
    // if (this.state.formType === 'add') {
    corporateService.storeRequest(data).then(res => {
      console.log('After', res)
    });
    // }
  
}

toggleModal = (value, original) => {
  console.log("inside functions", value)
  console.log("inside functions original", original)
  this.setState({
    value: original.id,
    company_name: original.company_name,
    caret_title: original.caret_title,
    caret_id: original.caret_id,
    user_id: original.user && original.user.id  });
  this.setState({
    addModalForm:true
  })
  
};

  render() {
    const { filtered, editRec, durations, users, campaignData } = this.state;
    const { toDate, fromDate } = this.state.search;

    const columns = [
      {
        Header: "Splash Listing",
        headerClassName: 'headerPagination',
        columns: [

          {
            Header: "Caret Title",
            accessor: "caret_title",
            className: "",
            sortable: false
          },
          {
            Header: "Contact Person Name",
            accessor: "name",
            className: "",
            sortable: false
          },
          {
            Header: "Email",
            accessor: "email",
            className: "",
            sortable: false
          },
          {
            Header: "Designation",
            accessor: "designation",
            className: "",
            sortable: false
          },
          {
            Header: "Phone",
            accessor: "phone",
            className: "",
            sortable: false
          },
          
          {
            Header: "Company Name",
            accessor: "company_name",
            className: "",
            sortable: false
          },
          // {
          //   Header: "Company Address",
          //   accessor: "company_address",
          //   className: "",
          //   sortable: false
          // },
          {
            Header: "Company Description",
            accessor: "company_description",
            className: "",
            sortable: false
          },
          {
            Header: "Request Submitted",
            accessor: "created_at",
            Cell: ({ value }) => {
              return value ? new Date(value).toLocaleDateString() : "N/A"; 
          },
            className: "",
            sortable: false
          },
          {
            Header: "Status",
            accessor: "status",
            Cell: ({ value, original }) => {
              return original.status === 1 ? (
                <span>Approved</span>
              ) : (
                <div>{original.status_a ? original.status_a.title : ''}</div>
              );
            },
            className: "",
            sortable: false,
          },
          // {
          //   Header: "Status",
          //   accessor: "status",
          //   Cell: ({ value, original }) => (
          //     <span>
          //       {value === 1 ? "Approved" : "Pending"}
          //     </span>
          //   ),
          //   className: "",
          //   sortable: false
          // },

          {
            Header: "Approval Date",
            accessor: "approval_date",
            Cell: ({ value }) => {
              return value ? new Date(value).toLocaleDateString() : "N/A"; 
            },
            className: "",
            sortable: false
          },
          {
            Header: "Amount",
            accessor: "subscription_amount",
            className: "",
            sortable: false
          },
          // {
          //   Header: "Add Payment",
          //   accessor: "id",
          
          //   Cell: ({ value, original }) => {
          //     // Ensure value check is handled correctly
          //     if (original.status !== 1) {
          //       return (
          //         original.subscription_plan && (
          //           <Link
          //             to={{
          //               pathname: "/CLM/corporate-payment",
          //               state: { 
          //                 keyword: original.caret_title, 
          //                 plan: original.subscription_plan, 
          //                 price: original.subscription_amount,
          //                 stripe_id: original.plan?original.plan.stripe_id:null,
          //                 plan_title: original.plan?original.plan.title:null,
          //                 caret_id: original.caret_id,
          //                 company_name: original.company_name
          //               }
          //             }}
          //           >
          //             <FaCreditCard />
          //           </Link>
          //         )
          //       );
          //     }
          //     return null; // Return null if the condition is not met to avoid rendering anything
          //   }
          // }
          
          // {
          //   Header: "Status",
          //   Cell: ({ row, original}) => {
          //     console.log('row', row);
          //     const currentStatusId = row._original ? row._original.status_id : '';
          //     console.log('currentStatusId', currentStatusId);

          //     return (
          //       <div>
          //         <select
          //           style={{ cursor: 'pointer', width: "90%", border: "none" }}
          //           value={currentStatusId}
          //           onChange={(e) => {
          //             const newStatus = parseInt(e.target.value);
          //             this.handleUpdateLicenseStatus(row._original.id, newStatus);
          //           }}
          //         >
          //           {this.state.statusList
          //             .filter(status => currentStatusId === 1 || status.value !== 1)
          //             .map((status) => (
          //               <option key={status.value} value={status.value}>
          //                 {status.label}
          //               </option>
          //             ))}
          //         </select>
          //       </div>
          //     );
          //   },
          //   sortable: false
          // },

       
        
          // {
          //   Header: "Actions",
          //   accessor: "id",
          //   Cell: ({ row, original, value }) => (
          //     <div>

          //       <OverlayTrigger placement="bottom" overlay={<Popover id="tooltip">Default Selection</Popover>}>
          //         <Link to={'/CLM/CaretDefaultSelection/' + value}>
          //           <button type='button' className="editIcon orderDetailsAction">&nbsp;</button>
          //         </Link>
          //       </OverlayTrigger>

          //       <OverlayTrigger placement="bottom" overlay={<Popover id="tooltip">Delete License</Popover>}>
          //         <span>
          //           {
          //             // <button type='button' className="deleteIcon orderDetailsAction" onClick={() => this.deleteConfirmModal(row.id)}>&nbsp;</button>
          //           }
          //         </span>

          //       </OverlayTrigger>
          //     </div>
          //   ),
          //   className: "justifyEnd text-right",
          //   headerClassName: "justifyEnd",
          //   sortable: false
          // }


        ]
      }
    ];

    return (
      <div className="content pl-2-5F">
        <Row>
          <Col md={6}>
            <div className="secHeading">Corporate Request Listing</div>
          </Col>
          <Col md={6}>
            <div className="justifyEnd flexElem flexResponsive mb-lg" style={{ "alignItems": "flex-start", "justifyContent": "space-between" }}>
              {/* <div className="">
                <OpenModalButton
                  openModal={HF.openModal.bind(this, "addModalForm")}
                  classButton={['backButton pt-sm no_radius pb-sm primary mt-none btn-block btn-info btn']}
                  buttonName="Add Licanse"
                  classIcon={['fa', 'fa-plus']}
                />
              </div> */}


            </div>
          </Col>

          <div className='col-md-12 mt-1'>

          {/* <Col md={2}>
              <div className='row flexElem flexResponsive align-items-center' >
                  <div className="col-md-12" >
                  {
                      this.dateElement('Date From', 'fromDate', fromDate!=='' && moment(fromDate, 'YYYY-MM-DD'), [{ before_or_equal: moment(this.state.search.toDate) }])
                  }
                  </div>
              </div>
          </Col>
          <Col md={2} >
              <div className='row flexElem flexResponsive align-items-center' >
                  <div className="col-md-12" >
                  {
                      this.dateElement('Date To', 'toDate', toDate!=='' && moment(toDate, 'YYYY-MM-DD'), [{ after_or_equal: moment(this.state.search.fromDate) }])
                  }
                  </div>

              </div>
          </Col> */}

          {/* <Col md={2} >
            <div className='row flexElem flexResponsive align-items-center' >
                <div className="col-md-12" >
                <label>
                    <span>Pending Status</span>
                </label>
                </div>
                <div className="col-md-12 mt-sm" >
                <Switch onChange={this.handleCheckPending} checked={this.state.search.status_id} />
                </div>
            </div>
        </Col> */}
        </div>


          <Col md={12} className="mt-md">
            <Pagination
              ref={(p) => this.pagination = p}
              showPagination={false}
              columns={columns}
              pageSize={20}
              getDataCall={this.paginationCall}
              filterView={true}
              filterPlaceHolder={'Corporate'}
              defaultSorted={
                [
                  {
                    id: 'id',
                    desc: true
                  }
                ]
              }
              // downloadData={true}
              // downloadFileName={'Orders'}
              // lowerContent = {null}
              // filterPlaceHolder = {'Orders'}
              noDataText='No Record found'
              getRowProps={this.getRowProps}
            // showAllToggle={true}
            />
          </Col>
        </Row>


                    {/* { durations !== null &&
                      <Modal backdrop={'static'} show={this.state.addModalForm} onHide={HF.closeModal.bind(this, "addModalForm")} aria-labelledby="ModalHeader" >
                        <Modal.Header closeButton>
                          <Modal.Title id='ModalHeader' className="headerTitle">Add Plan</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <div className="row">
                            <div className="col-md-12">
                              <Card bsClass={["innerCard mb-none"]} content={
                                <CorporatePlanForm
                                  closeModel={HF.closeModal.bind(this, "addModalForm")}
                                  formTypePro="add"
                                  recPro={OC.CLMPLAN}
                                  submitForm= { this.submitForm }
                                  componentPro="AdListing"
                                  durations={durations}
                                />
                              } />
                            </div>
                          </div>
                        </Modal.Body>
                      </Modal>
                    } */}
                    {/*Add Record Modal End*/}

              		  {/*Edit Record Modal start*/}
                    {/* { editRec &&
                      <Modal backdrop={'static'} show={this.state.editModalForm} onHide={HF.closeModal.bind(this, "editModalForm")} aria-labelledby="ModalHeader" >
                      <Modal.Header closeButton>
                        <Modal.Title id='ModalHeader' className="headerTitle">Edit Plan</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="row">
                          <div className="col-md-12">
                          <Card bsClass={["innerCard mb-none"]} content={
                            <CorporatePlanForm
                              closeModel={HF.closeModal.bind(this, "editModalForm")}
                              formTypePro="edit"
                              recPro={editRec}
                              submitForm= { this.submitForm }
                              durations={durations}
                            />
                          } />
                          </div>
                        </div>
                      </Modal.Body>
                      </Modal>
                    } */}
                    



                    {
                      <Modal backdrop={'static'} show={this.state.openVideoModal} onHide={HF.closeModal.bind(this, "openVideoModal")} aria-labelledby="ModalHeader" >
                      <Modal.Header closeButton>
                        <Modal.Title id='ModalHeader' className="headerTitle">Video</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="row">
                          <div className="col-md-12" style={{textAlignVertical: "center",textAlign: "center"}}>
                          <video key={this.state.video_url} id="playVid" controls style={{width: 'auto', maxHeight: 'calc(100vh - 150px)'}} ><source src={this.state.video_url} type="video/mp4" /></video>
                          </div>
                        </div>
                      </Modal.Body>
                      </Modal>
                    }

      </div>
    );
  };
}
const CorporateListing= CorporateListingPage;
export default CorporateListing;


