import React, { Component, Fragment } from "react";
import { Grid, Row, Col, Button } from "react-bootstrap";
import { adService } from "../../../services/ad";
import { splashService } from "../../../services/splash";
import { Link } from "react-router-dom";
import lib from "react-ckeditor-component";
import { licenseService } from "services/license";
import { musicService } from "services/music";

class DefaultAudio extends Component {
  constructor(props) {
    super(props);
    const searchParams = new URLSearchParams(this.props.location.search);
    const caretId = searchParams.get('caret_id');

    const user = JSON.parse(localStorage.getItem("user"));
    const { id } = this.props.match.params;
    this.state = {
      default_ad: [],
      default_intro: [],
      default_finish: [],
      default_sound: [],

      xid: 0,
      adList: [],
      splashList: [],
      audioList: [],
      id: id,
      caretId: id,
      user_id: user.data ? user.data.id : null,
      searchTerm: '',      
    };
  }

  componentDidMount() {
    this.getSoundsCall(this.state.user_id, this.state.caretId, this.state.searchTerm);
    this.getdefaultCall();
  }

  getdefaultCall = () => {
    const data = {
      id:this.state.id
    }
  licenseService.CaretDefaultSettings(data).then((res) => {
   let index=0;
   let recState = { ...this.state.default_sound };
   recState[index] = res.data.default_sound;
   this.setState({
    default_sound :recState
   })

 });
}
handleSearchChange = (e) => {
  const searchTerm = e.target.value;
  this.setState({ searchTerm });
  this.getSoundsCall(this.state.user_id, this.state.caretId, searchTerm);
};

  getSoundsCall = (val, licenseId, searchTerm) => {
    musicService.getSoundsData(val,licenseId,  searchTerm).then((res) => {
      // console.log("audioList:", res.data);
      this.setState({ audioList: res.data });
    });
  };

  


  handleAudioSelect = (selectedOptionType) => {
    // console.log("selectedOptionType", selectedOptionType);

    const filtered = this.state.audioList.filter((obj) => {
      return obj.value === selectedOptionType["value"];
    });
    console.log(filtered);

    if (filtered) {
      let recState = { ...this.state.default_sound };
      recState[this.state.xid] = filtered[0].value;
      // console.log("default sound id ########## ", recState);
      this.setState({ default_sound: recState });
    }
  };


  handleToSaveDefaultSelected = () => {
    const data = {
      default_sound: this.state.default_sound,
      id: this.state.id,
    };
    licenseService.CaretDefaultSettings(data).then((res) => {
      // console.log(res);
      window.location.replace("/CLM/License");
      // window.location.reload();
    });
  };

    render() {
    const { tabs, default_ad, default_intro, default_finish, default_sound } =
      this.state;

    return (
      <Fragment>
        <div className="innerCustomContainer">
          

          <div>

          
              <div>
                <div className="">
                  <div
                    className="flexElem gap-2 items-center"
                    style={{ marginBottom: "20px", marginTop: "20px" }}
                  >
                    <h3 className="text-left font-bold m-0">
                      Select Default Audio
                    </h3>
                    <div className="flexElem ml-auto gap-2" style={{ gap: 10 }}>
                      <Button
                        type="submit"
                        className="btnDark px-5 font-boldF text-16"
                        disabled={
                          !(

                            this.state.default_sound[this.state.xid]
                          )
                        }
                        onClick={() => this.handleToSaveDefaultSelected()}
                      >
                        Proceed to Save
                      </Button>
                    </div>
                  </div>

                  <div className="gridSplash mb-2">
                    {this.state.audioList.map((audio, index) => (
                      <div>
                        <div className="splashWrap1">
                          <div className="flexElem gap10p itemsCenter mb-1">
                            <h6
                              className="text-primary m-0"
                              style={{
                                textTransform: "capitalize"
                              }}
                            >
                              Audio Title:
                            </h6>
                            <h6 className="text-primary m-0" style={{
                                textTransform: "capitalize"
                              }}>{audio.label}</h6>
                          </div>
                          <audio src={audio.sound_url} className="" controls />
                          <div
                            className="adContent flexElem flexColumn mt-auto gap10p"
                            style={{ wordBreak: "break-word" }}
                          ></div>
                        </div>
                        <button
                          className={`mt-1 selectBtn ${
                            this.state.default_sound[this.state.xid] ===
                            audio.value
                              ? "bcCelectBtn"
                              : "selectBtn"
                          }`}
                          onClick={() => this.handleAudioSelect(audio)}
                        >
                          Select
                        </button>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            
          </div>
        </div>
      </Fragment>
    );
  }
}

export default DefaultAudio;



