import React, { Component } from "react";
import { Switch, Redirect } from "react-router-dom";

import Header from "components/CLM/Header/Header";
import Footer from "components/CLM/Footer/Footer";
import Sidebar from "components/CLM/Sidebar/Sidebar";
// import { hasPermission } from '../../helper/hasPermission';
import CLMRoutes from "routes/CLM.jsx";
import { CLMAuthRoute } from 'helper/PrivateRouteCLM';
import { alertActions } from 'redux/actions/alert-actions';
import { settingsActions } from 'redux/actions/settings-actions';
import { connect } from 'react-redux';
import MessageCom from "./Message";
import arrowImg from "../../assets/img/arrow_down.png"
import { history } from 'helper/history';
import { loadProgressBar } from 'axios-progress-bar';
import ErrorBoundary from "./ErrorBoundary"
// import { Link,withRouter } from 'react-router-dom';
// import {basePathApi} from 'helper/basePathApi';
import 'axios-progress-bar/dist/nprogress.css';
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/animate.min.css";
import "../../assets/css/gen_classes.css";
import "../../assets/sass/light-bootstrap-dashboard.css?v=1.2.0";
import "../../assets/css/demo.css";
import "../../assets/css/pe-icon-7-stroke.css";
import "../../assets/fonts/robotoFont.css";
import $ from 'jquery';

class CLMPage extends Component {
  constructor(props) {
    super(props);
    // const { dispatch } = this.props;
    this.props.showLoaderCall(true);
    this.state = {
      canvasMenuToggle: false
    }



    history.listen((location, action) => {
      this.props.alertActionCall();
    });
    this.closeSideBarMenu = this.closeSideBarMenu.bind(this);
    this.showCanvasMenu = this.showCanvasMenu.bind(this);
  }
  closeSideBarMenu() {
    this.setState({ canvasMenuToggle: false }, function () {
      //$('.sidebar').addClass('showSideNav');
      var sub_height = $(".sidebar").outerWidth();
      $(".main-panel").css({ 'width': 'calc(100% - ' + sub_height + 'px)' });

    });
  }
  showCanvasMenu() {
    this.setState({ canvasMenuToggle: true }, function () {
      //$('.sidebar').addClass('showSideNav');
      var sub_height = $(".sidebar").outerWidth() - 20;
      // var sub_height = $(".sidebar").outerWidth();
      $(".main-panel").css({ 'width': 'calc(100% - ' + sub_height + 'px)' });

    });
  }

  render() {
    const { showLoader } = this.props;
    var sub_height = $(".sidebar").outerWidth() - 20;
    // var sub_height = $(".sidebar").outerWidth();
    $(".main-panel").css({ 'width': 'calc(100% - ' + sub_height + 'px)' });
    // console.log("var sub_height = $(.sidebar).outerWidth() - 20; ===", sub_height)


    return (
      <div className="wrapper">
        <Sidebar {...this.props} hideSideNav={this.showCanvasMenu} showSideNav={this.showCanvasMenu} />
        <div id="main-panel" className="main-panel" ref="mainPanel">
          <Header {...this.props} ref="child" />
          <div>
            <MessageCom />
            {loadProgressBar({ showSpinner: showLoader })}
          </div>
          {
            this.props.location.pathname !== '/CLM/dashboard' &&
            <div className='text-left backbutton'>
              <button onClick={() => { this.props.history.goBack() }} className='backButton pt-sm no_radius pb-sm primary text-xs btn btn-sm btn-info' >
                <img src={arrowImg} alt="Arrow" width="15" />
              </button>
            </div>
          }
          <ErrorBoundary>
            <Switch>
              {
                CLMRoutes.map((prop, key) => {
                  if (prop.redirect) {
                    return <Redirect from={prop.path} to={prop.to} key={key} />;
                  }
                  /*else if (hasPermission(prop.permission)){*/
                  return (
                    <CLMAuthRoute path={prop.path} component={prop.component} key={key} />
                  );
                  /*}*/
                })
              }
            </Switch>
          </ErrorBoundary>
          <Footer />
        </div>
      </div>

    );
  }
}


function mapStateToProps(state) {
  const { alert } = state;
  const { showLoader } = state.settingsReducer;
  const typeOfAlert = typeof alert.message;
  // console.log('showLoader', showLoader);
  return {
    alert,
    typeOfAlert,
    showLoader
  };
}

const mapDispatchToProps = dispatch => {
  return ({
    showLoaderCall: (value) => { dispatch(settingsActions.showLoader(value)) },
    alertActionCall: () => { dispatch(alertActions.clear()) },

  });
};

const CLM = connect(mapStateToProps, mapDispatchToProps)(CLMPage);

export default CLM;
