
import React, { Component } from "react";
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import {
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Image
} from "@react-pdf/renderer";

class ResumePDF extends Component {
    constructor(props){
    	super(props);
		this.state = {
			employee: {
				...this.props.data,
			},
            hobbies: [],
            skills: this.props.data.skills,
            educations: this.props.data.educations,
            eployeement: this.props.data.eployeement,
            projects: this.props.data.projects,
		    submitted: false,
		    formType: this.props.formTypePro 	|| '',
			allowedFormatsStringMime : 'image/jpeg, image/png',
			allowedFormatsStringExt : 'jpg, png',
		};

        console.log('initialData',this.props.data);
    }	
    
    styles = StyleSheet.create({
        page: {
            backgroundColor: "#ffffff"
        },
        section: {
            margin: 10,
            padding: 10,
            flexGrow: 1
        },

        Container: {
            display: "flex",
            flexDirection: "row",
            padding: 5
        },
        column1: {
            display: "flex",
            flexDirection: "column",
            padding: 5,
            width:'33%'
        },
        column2: {
            display: "flex",
            flexDirection: "column",
            padding: 5,
            width:'66%'
        },

        Details: {
            display: "flex",
            marginLeft: 5
        },

        Title: {
            fontSize: 20,display: "flex",
        },
        Heading: {
            fontSize: 20,display: "flex",
        },
        
        subtitle: {
            fontSize: 15,
            display: "flex",
        },
    });

	render(){
        console.log("Testing PDF");
        const {employee} = this.state;
		return(
			<Document>
                <Page size="A4" style={this.styles.page}>
                    <View style={this.styles.Container}>
                    <View style={this.styles.column1}>
                            <Text style={this.styles.Title}>{employee.name}</Text>
                            <Text style={this.styles.subtitle}>{employee.job_title}</Text>
                            <Text style={this.styles.Heading}>Profile</Text>
                            <Text style={this.styles.Heading}>Profile</Text>
                            
                        </View>   
                        <View style={this.styles.column2}>
                            <Text style={this.styles.Title}>{employee.name}</Text>
                            <Text style={this.styles.subtitle}>{employee.job_title}</Text>
                            <Text style={this.styles.Heading}>Profile</Text>
                            <Text style={this.styles.Heading}>Profile</Text>
                            
                        </View>     

                        <View style={this.styles.column2}>
                            <Text>adfhalsd fasdhf </Text>
                        </View>                 
                    </View>
                </Page>
            </Document>
		);
	};
}



export default ResumePDF;
