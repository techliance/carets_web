import React from 'react';
import {
  Button
} from "react-bootstrap";

import SimpleReactValidator from 'simple-react-validator';
import Select from 'react-select';
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';
import EducationInput from './EducationInputs';

class EmployeeForm extends React.Component {
	constructor(props){
		super(props);
		this.validatorEmployee = new SimpleReactValidator({autoForceUpdate: this, locale: 'en'});
		this.state = {
			employee: {
				...this.props.employeePro,
			},
            hobbies: [],
            skills: this.props.employeePro.skills,
            educations: this.props.employeePro.educations,
            eployeement: this.props.employeePro.eployeement,
            projects: this.props.employeePro.projects,
		    submitted: false,
		    formType: this.props.formTypePro 	|| '',
			allowedFormatsStringMime : 'image/jpeg, image/png',
			allowedFormatsStringExt : 'jpg, png',
		};

		this.handleEmployee = this.handleEmployee.bind(this);
  		this.handleEmployeeSubmitForm = this.handleEmployeeSubmitForm.bind(this);
        this.handleChangeHobbies = this.handleChangeHobbies.bind(this);
        console.log('initialData',this.state.employee);
	}
	handleEmployee(e){
	    const { name, value, type } = e.target;
	    const employeeState = { ...this.state.employee };
	    employeeState[name] = type === 'checkbox' ? e.target.checked ? 1 : 0 : value;
	    this.setState({employee: employeeState});
	}
	handleEmployeeSubmitForm(e){
        e.preventDefault();

	    if (this.validatorEmployee.allValid()) {
			this.setState({submitted: true}, function(){
				this.props.handleEmployeeSubmit(this.state.employee, this.state.formType);
			});
		} else {
			this.setState({submitted: false});
			this.validatorEmployee.showMessages();
			this.forceUpdate();
		}
	}
	handleChangeHobbies(val1) {
        const employeeState = { ...this.state.employee };
        employeeState['hobbies'] = val1;
        this.setState({employee: employeeState});
    }


      updatePropsData(){
        const employeeState = { ...this.state.employee };
        employeeState['educations'] = this.state.educations;
        employeeState['eployeement'] = this.state.eployeement;
        employeeState['projects'] = this.state.projects;
        employeeState['skills'] = this.state.skills;
        this.setState({employee: employeeState});
      }
      //Skills Changes
      handleSkillsChange = idx => evt => {
        const newShareholders = this.state.skills.map((shareholder, sidx) => {
            if (idx !== sidx) return shareholder;
            if(evt.target.name === 'name')
                return { ...shareholder, name: evt.target.value };
            if(evt.target.name === 'skill_level_percent')
                return { ...shareholder, skill_level_percent: evt.target.value };
        });
        this.setState({ skills: newShareholders },()=>console.log('Institute Changes',this.state.skills));
        this.updatePropsData();
      };

      handleAddSkills = () => {
        this.setState({
            skills: this.state.skills.concat([{ name: "", skill_level_percent:""}])
        });
        this.updatePropsData();
      };

      handleRemoveSkills = idx => () => {
        this.setState({
            skills: this.state.skills.filter((s, sidx) => idx !== sidx)
        });
        this.updatePropsData();
      };
      //JOb Changes
      handleJobChange = idx => evt => {
        const newShareholders = this.state.eployeement.map((shareholder, sidx) => {
            if (idx !== sidx) return shareholder;
            if(evt.target.name === 'job_title')
                return { ...shareholder, job_title: evt.target.value };
            if(evt.target.name === 'company_name')
                return { ...shareholder, company_name: evt.target.value };
            if(evt.target.name === 'job_details')
                return { ...shareholder, job_details: evt.target.value };
            if(evt.target.name === 'job_start')
                return { ...shareholder, job_start: evt.target.value };
            if(evt.target.name === 'job_end')
                return { ...shareholder, job_end: evt.target.value };
        });
        this.setState({ eployeement: newShareholders },()=>console.log('Institute Changes',this.state.eployeement));
        this.updatePropsData();
      };

      handleAddJob = () => {
        this.setState({
            eployeement: this.state.eployeement.concat([{ job_title: "", company_name:"",job_details:"", job_start:"", job_end:""}])
        });
        this.updatePropsData();
      };

      handleRemoveJob = idx => () => {
        this.setState({
            eployeement: this.state.eployeement.filter((s, sidx) => idx !== sidx)
        });
        this.updatePropsData();
      };
      //Projects Changes
      handleProjectsChange = idx => evt => {
        const newShareholders = this.state.projects.map((shareholder, sidx) => {
            if (idx !== sidx) return shareholder;
            if(evt.target.name === 'project_name')
                return { ...shareholder, project_name: evt.target.value };
            if(evt.target.name === 'company_name')
                return { ...shareholder, company_name: evt.target.value };
            if(evt.target.name === 'project_details')
                return { ...shareholder, project_details: evt.target.value };
            if(evt.target.name === 'project_link')
                return { ...shareholder, project_link: evt.target.value };
            if(evt.target.name === 'employee_role')
                return { ...shareholder, employee_role: evt.target.value };
        });
        this.setState({ projects: newShareholders },()=>console.log('Institute Changes',this.state.projects));
        this.updatePropsData();
      };

      handleAddProjects = () => {
        this.setState({
            projects: this.state.projects.concat([{ project_name: "", company_name:"",project_details:"", project_link:"", employee_role:""}])
        });
        this.updatePropsData();
      };

      handleRemoveProjects = idx => () => {
        this.setState({
            projects: this.state.projects.filter((s, sidx) => idx !== sidx)
        });
        this.updatePropsData();
      };
      //Institute Changes
      handleInstituteChange = idx => evt => {
        const newShareholders = this.state.educations.map((shareholder, sidx) => {
            if (idx !== sidx) return shareholder;
            if(evt.target.name === 'degree_title')
                return { ...shareholder, degree_title: evt.target.value };
            if(evt.target.name === 'institute_name')
                return { ...shareholder, institute_name: evt.target.value };
            if(evt.target.name === 'year_start')
                return { ...shareholder, year_start: evt.target.value };
            if(evt.target.name === 'year_end')
                return { ...shareholder, year_end: evt.target.value };
        });
        this.setState({ educations: newShareholders },()=>console.log('Institute Changes',this.state.educations));
        this.updatePropsData();
      };

      handleAddInstitute = () => {
        this.setState({
            educations: this.state.educations.concat([{ degree_title: "", institute_name:'', year_start:'', year_end:''}])
        });
        this.updatePropsData();
      };

      handleRemoveInstitute = idx => () => {
        this.setState({
            educations: this.state.educations.filter((s, sidx) => idx !== sidx)
        });
        this.updatePropsData();
      };

	render(){
		const { employee, formType, allowedFormatsStringMime, allowedFormatsStringExt } = this.state;
		return(
			<form onSubmit={this.handleEmployeeSubmitForm}>
			  <div className="row">
			    <div className="col-md-6">
					<div className='form-group'>
						<label htmlFor="name">Employee Name<span className="requiredClass">*</span></label>
						<input type="text" name="name" className="form-control"  value={employee.name} onChange={this.handleEmployee} />
						{this.validatorEmployee.message('employeeName', employee.name, 'required|min:5')}
					</div>
			    </div>
			    <div className="col-md-6">
					<div className='form-group'>
						<label htmlFor="job_title">Job Title<span className="requiredClass">*</span></label>
						<input type="text" name="job_title" className="form-control" value={employee.job_title} onChange={this.handleEmployee} />
						{this.validatorEmployee.message('job_title', employee.job_title, 'required|min:5')}
					</div>
			    </div>
			  </div>

			  <div className="row">
			    <div className="col-md-6">
					<div className='form-group'>
						<label htmlFor="charges_per_month">Charged Per Month<span className="requiredClass">*</span></label>
						<input type="text" name="charges_per_month" className="form-control"  value={employee.charges_per_month} onChange={this.handleEmployee} />
						{this.validatorEmployee.message('charges_per_month', employee.charges_per_month, 'required|min:1')}
					</div>
			    </div>
			    <div className="col-md-6">
					<div className='form-group'>
						<label htmlFor="charges_per_hour">Charged Per Hour<span className="requiredClass">*</span></label>
						<input type="text" name="charges_per_hour" className="form-control" value={employee.charges_per_hour} onChange={this.handleEmployee} />
						{this.validatorEmployee.message('charges_per_hour', employee.charges_per_hour, 'required|min:1')}
					</div>
			    </div>
			  </div>
              <div className="row">
			    <div className="col-md-3">
	              <div className=''>
	                <label className="container-check checkbox_down"> Is Available?
	                  <input type="checkbox" name="is_available_for_hire" className="checkbox" value={employee.is_available_for_hire? 1 : 0} onChange={this.handleEmployee} checked={employee.is_available_for_hire?1:0} />
	                  <span className="checkmark"></span>
	                </label>
	                {/* {this.validatorPermission.message('is_active', employee.is_active, 'required')} */}
	              </div>
	            </div>
                <div className="col-md-9">
					<div className='form-group'>
						<label htmlFor="career_summary">Career Summary</label>
                        <textarea name="career_summary" className="form-control" onChange={this.handleEmployee}>{employee.career_summary}</textarea>
						{/* {this.validatoremployee.message('career_summary', employee.career_summary, 'required|min:1')} */}
					</div>
			    </div>
			  </div>
              <div className="row">
			    <div className="col-md-6">
					<div className='form-group'>
                    <label htmlFor="career_summary">Hobbies</label>
                    <TagsInput className="form-control" value={employee.hobbies} onChange={this.handleChangeHobbies} />
						</div>
			    </div>

                <div className="col-md-6">

			    </div>
			  </div>

              <div className="row">
			    <div className="col-md-12">
                  <div className='form-group'>
                  <label htmlFor="Skills">Skills</label>
                    {this.state.skills.map((shareholder, idx) => (
                            <div className="shareholder">
                                <input
                                className="form-control"
                                type="text"
                                name={`name`}
                                placeholder={`Name`}
                                value={shareholder.name}
                                onChange={this.handleSkillsChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="number"
                                min="0"
                                max="100"
                                name={`skill_level_percent`}
                                placeholder={`Skill Level Percentage`}
                                value={shareholder.skill_level_percent}
                                onChange={this.handleSkillsChange(idx)}
                                />
                                <button
                                className="form-control"
                                type="button"
                                onClick={this.handleRemoveSkills(idx)}
                                className="small"
                                >
                                -
                                </button>
                            </div>
                            ))}
                            <button
                                className="form-control"
                            type="button"
                            onClick={this.handleAddSkills}
                            className="small"
                            >
                            Add New
                            </button>
						</div>
                  </div>
              </div>

              <div className="row">
			    <div className="col-md-12">
                  <div className='form-group'>
                  <label htmlFor="Education">Education</label>
                    {this.state.educations.map((shareholder, idx) => (
                            <div className="shareholder">
                                <input
                                className="form-control"
                                type="text"
                                name={`degree_title`}
                                placeholder={`Degree Title`}
                                value={shareholder.degree_title}
                                onChange={this.handleInstituteChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="text"
                                name={`institute_name`}
                                placeholder={`Institute Name`}
                                value={shareholder.institute_name}
                                onChange={this.handleInstituteChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="date"
                                name={`year_start`}
                                placeholder={`From`}
                                value={HF.dateFormatSystem(shareholder.year_start)}
                                onChange={this.handleInstituteChange(idx)}
                                />
                                <input
                                type="date"
                                className="form-control"
                                name={`year_end`}
                                placeholder={`To`}
                                value={HF.dateFormatSystem(shareholder.year_end)}
                                onChange={this.handleInstituteChange(idx)}
                                />
                                <button
                                type="button"
                                className="form-control"
                                onClick={this.handleRemoveInstitute(idx)}
                                className="small"
                                >
                                -
                                </button>
                            </div>
                            ))}
                            <button
                            type="button"
                            className="form-control"
                            onClick={this.handleAddInstitute}
                            className="small"
                            >
                            Add New
                            </button>
						</div>
                  </div>
              </div>
              <div className="row">
			    <div className="col-md-12">
                  <div className='form-group'>
                  <label htmlFor="Career">Career</label>
                    {this.state.eployeement.map((shareholder, idx) => (
                            <div className="shareholder">
                                <input
                                className="form-control"
                                type="text"
                                name={`job_title`}
                                placeholder={`Job Title`}
                                value={shareholder.job_title}
                                onChange={this.handleJobChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="text"
                                name={`company_name`}
                                placeholder={`Company Name`}
                                value={shareholder.company_name}
                                onChange={this.handleJobChange(idx)}
                                />
                                <textarea
                                className="form-control"
                                type="text"
                                name={`job_details`}
                                placeholder={`Job Details`}
                                value={shareholder.job_details}
                                onChange={this.handleJobChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="date"
                                name={`job_start`}
                                placeholder={`From`}
                                value={HF.dateFormatSystem(shareholder.job_start)}
                                onChange={this.handleJobChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="date"
                                name={`job_end`}
                                placeholder={`To`}
                                value={HF.dateFormatSystem(shareholder.job_end)}
                                onChange={this.handleJobChange(idx)}
                                />

                                <button
                                className="form-control"
                                type="button"
                                onClick={this.handleRemoveJob(idx)}
                                className="small"
                                >
                                -
                                </button>
                            </div>
                            ))}
                            <button
                                className="form-control"
                            type="button"
                            onClick={this.handleAddJob}
                            className="small"
                            >
                            Add New
                            </button>
						</div>
                  </div>
              </div>
              <div className="row">
			    <div className="col-md-12">
                  <div className='form-group'>
                  <label htmlFor="Projects">Projects</label>
                    {this.state.projects.map((shareholder, idx) => (
                            <div className="shareholder">
                                <input
                                className="form-control"
                                type="text"
                                name={`project_name`}
                                placeholder={`Project Name`}
                                value={shareholder.project_name}
                                onChange={this.handleProjectsChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="text"
                                name={`company_name`}
                                placeholder={`Company Name`}
                                value={shareholder.company_name}
                                onChange={this.handleProjectsChange(idx)}
                                />
                                <textarea
                                className="form-control"
                                type="text"
                                name={`project_details`}
                                placeholder={`Project Details`}
                                value={shareholder.project_details}
                                onChange={this.handleProjectsChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="text"
                                name={`project_link`}
                                placeholder={`Project Link`}
                                value={shareholder.project_link}
                                onChange={this.handleProjectsChange(idx)}
                                />
                                <input
                                className="form-control"
                                type="text"
                                name={`employee_role`}
                                placeholder={`Employee Role`}
                                value={shareholder.employee_role}
                                onChange={this.handleProjectsChange(idx)}
                                />

                                <button
                                className="form-control"
                                type="button"
                                onClick={this.handleRemoveProjects(idx)}
                                className="small"
                                >
                                -
                                </button>
                            </div>
                            ))}
                            <button
                                className="form-control"
                            type="button"
                            onClick={this.handleAddProjects}
                            className="small"
                            >
                            Add New
                            </button>
						</div>
                  </div>
              </div>

			  <div className="row">
			    <div className="col-md-12">
			      <div className='text-center'>
			        <Button  type='submit' bsSize="large" bsStyle="info" className="backButton pt-sm no_radius pb-sm success btn btn-lg btn-info ml-sm mt-sm btn-default " >Save</Button>
			        <Button bsSize="large" bsStyle="info" onClick={()=>this.props.closeModel() } className="backButton pt-sm no_radius pb-sm primary btn btn-lg ml-sm mt-sm btn-info " >Cancel</Button>
			      </div>
			    </div>
			  </div>
			</form>
		);
	}
}


export default EmployeeForm;
