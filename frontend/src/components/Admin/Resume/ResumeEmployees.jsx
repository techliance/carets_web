import React, { Component } from "react";
import { connect } from 'react-redux';
import { resumeActions } from "redux/actions/resume-actions";
import ReactValidator from "simple-react-validator";
import DatePicker from "react-datepicker";
import {
    Grid,
    Row,
    Col,
    // Tooltip,
    OverlayTrigger,
    // Button,
    Popover
  } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import ReactTable from "react-table";
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';
import { objectsConstants as OC } from 'redux/constant/objects-constant';
import EmployeeForm from "./EmployeeForm";
import OpenModalButton from '../../ThemeComponents/openModelButton';
import { hasPermission } from 'helper/hasPermission';
import Confirmalertfordelete from '../../ThemeComponents/confirmAlertForDelete';
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import { ThemeFunctions as TF } from 'helper/ThemeFunctions';
import { adminLabels } from '../../../redux/constant/admin-label-constant';
import { confirmAlert } from 'react-confirm-alert';
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
var Modal = require('react-bootstrap-modal');
const moment = window.moment;

class ResumeEmployeesPage extends Component{
  constructor(props){
  	super(props);
  	this.state = {
  		addModalEmployee  : false,
      editModalEmployee : false,
      sorted   : '',
      filtered : '',
      pageSize : 10,
  	};

  	this.callEditEmployee = this.callEditEmployee.bind(this);
    this.deleteEmployee = this.deleteEmployee.bind(this);
  	this.deleteConfirmEmployee = this.deleteConfirmEmployee.bind(this);
    this.EmployeesPaginationData  = this.EmployeesPaginationData.bind(this);
    this.filterSearchHandle     = this.filterSearchHandle.bind(this);
    this.handleEmployeeSubmit= this.handleEmployeeSubmit.bind(this);
  };
  handleEmployeeSubmit(EmployeeData, formType){
    if (formType === 'add')
      this.props.storeEmployeeCall(EmployeeData);
    else if (formType === 'edit')
      this.props.editEmployeeCall(EmployeeData, this.props.editEmployee.id);
  }

  filterSearchHandle(e){
    this.setState({
      'filtered': e.target.value
    });
  }
  callEditEmployee(id){
    this.props.getEmployeeCall(id);
  }

  deleteEmployee(EmployeeId){
    this.props.deleteEmployeeCall(EmployeeId);
  }
  deleteConfirmEmployee(EmployeeId){
  	confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <Confirmalertfordelete typeName="Employee" description="Are you sure to delete the admin Employee?" onClosePro={ onClose }  deleteType={()=>this.deleteEmployee(EmployeeId)} />
        )
      }
    })
  }
  EmployeesPaginationData(page, filter, sort, pageSize){
    this.setState({pageSize: pageSize}, function(){
      this.props.getEmployeesCall(page, filter, sort, pageSize);
    })
  }
  handleUpdateEmployee = EmployeeId => (e) => {
    this.props.updateEmployeeStatusCall(EmployeeId, {is_active: e.target.checked ? 1 : 0}).then(
      response => {
        this.props.getEmployeesCall(this.props.current_page, '', '', this.state.pageSize);
        // this.props.EmployeesPaginationData();
      }
    );
  }
  componentDidUpdate(prevProps, nextProps) {
    if ( (prevProps.editEmployee !== this.props.editEmployee) && !this.props.loading){
      this.setState({editModalEmployee:true});
    }
    if ((prevProps.storedEmployee !== this.props.storedEmployee) && !this.props.loading){
      this.setState({addModalEmployee:false}, function(){
        this.props.getEmployeesCall(this.props.current_page, '', '', this.state.pageSize);
      });
    }
    if (( prevProps.updatedEmployee !== this.props.updatedEmployee) && !this.props.loading){
      this.setState({editModalEmployee:false}, function(){
        this.props.getEmployeesCall(this.props.current_page, '', '', this.state.pageSize);
      });
    }
    if (( prevProps.deleteEmployee !== this.props.deleteEmployee) && !this.props.loading){
      this.props.getEmployeesCall(this.props.current_page, '', '', this.state.pageSize);
    }


  }
  render(){
  	const { filtered } = this.state;
      const { editEmployee, employeesData, pages, loading, roles } = this.props;
      console.log('editEmployee',editEmployee);
      const columns = [
        {
          Header: "resume Testing",
          headerClassName: 'headerPagination',
          columns: [
              {
                  Header: "ID",
                  accessor: "id",
                  Cell: ({ value }) => {
                    return <Link to={'/Admin/employee/' + value} >{value}</Link>
                  },
                  className: "action-center",
                  sortable:false,
                },
              {
                  Header: "Name",
                  accessor: "name",
                  className: "action-center",
                  sortable:false,
                },
              {
                  Header: "Title",
                  accessor: "job_title",
                  className: "action-center",
                  sortable:false,
              },
              {
                  Header: "Per Month",
                  accessor: "charges_per_month",
                  className: "action-center",
                  sortable:false,
              },
              {
                  Header: "Per Hour",
                  accessor: "charges_per_hour",
                  className: "action-center",
                  sortable:false,
              },
              {
                  Header:"Actions",
                  accessor: "id",
                  Cell: ({value}) => (
                    <div>
                    <OverlayTrigger placement="bottom" overlay={<Popover id="tooltip">{ hasPermission('user-edit')?'Edit Employee': adminLabels.DONT_ALLOW  }</Popover>}>
                      <span>
                      {
                        hasPermission('user-edit') &&
                        <button type='button' className="editIcon orderDetailsAction" onClick={() => this.callEditEmployee(value)}>&nbsp;</button>
                      }
                      {
                        !hasPermission('user-edit') &&
                        <button type='button' className="editIcon orderDetailsAction button_disabled" >&nbsp;</button>
                      }
                      </span>
                    </OverlayTrigger>
                    <OverlayTrigger placement="bottom" overlay={<Popover id="tooltip">{ hasPermission('user-delete')?'Delete Employee': adminLabels.DONT_ALLOW  }</Popover>}>
                      <span>
                      {
                        hasPermission('user-delete') &&
                        <button type='button' className="deleteIcon orderDetailsAction" onClick={() => this.deleteConfirmEmployee(value)}>&nbsp;</button>
                      }
                      {
                        !hasPermission('user-delete') &&
                        <button type='button' className="deleteIcon orderDetailsAction button_disabled" >&nbsp;</button>
                      }
                      </span>

                    </OverlayTrigger>
                    </div>
                    ),
                  className: "action-center",
                  sortable:false
                }

          ]
        }
  ];

  	return(
  		<div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableResponsive
                content={
                	<div>
                  	<Grid fluid>
                      <Row>
                          <Col md={12}>
                            <div className="secHeading">Employees List</div>
                          </Col>

                          <Col md={12}>
                            <div className="flexElem flexResponsive" style={{"alignItems": "flex-start", "justifyContent": "space-between"}}>
                              <div className="">
                                <OpenModalButton
                                  openModal={hasPermission('user-create')?HF.openModal.bind(this, "addModalEmployee"):null}
                                  classButton={['backButton pt-sm no_radius pb-sm primary mt-none btn-block btn-info btn', !hasPermission('user-create')?'button_disabled':'']}
                                  buttonName="Add Employee"
                                  tooltipText={hasPermission('user-create')?'Add User':adminLabels.DONT_ALLOW }
                                  classIcon={['fa', 'fa-plus']}
                                />
                              </div>

                              <div className="custom-search-input">
                                <FormInputs
                                  ncols={["col-md-12"]}
                                  onChange={this.filterSearchHandle}
                                  proprieties={[
                                    {
                                      type: "text",
                                      bsClass: "form-control",
                                      placeholder: "Search Employees",
                                      onChange:this.filterSearchHandle,
                                      name:"filter"
                                    }
                                  ]}
                                />
                              </div>
                            </div>
                          </Col>

                          <Col md={12} className="mt-md">
                            <ReactTable
                              noDataText= 'No Employee found'
                              data={employeesData}
                              pages={pages}
                              loading = {loading}
                              columns={columns}
                              filtered={filtered}
                              defaultPageSize={10}
                              className="-striped listing responsive"
                              pageData = {this.EmployeesPaginationData}
                              manual
                              onFetchData={(state, instance) => {
                                  var sort = state.sorted.length === 0 ? '' : state.sorted[0].id+',desc:'+state.sorted[0].desc;
                                  state.pageData(state.page+1, state.filtered, sort, state.pageSize);
                              }}
                            />
                          </Col>
                      </Row>
                    </Grid>

                    {/*Add Employee Modal Start*/}
                    {
                      <Modal backdrop={'static'} show={this.state.addModalEmployee} onHide={HF.closeModal.bind(this, "addModalEmployee")} aria-labelledby="ModalHeader" >
                        <Modal.Header closeButton>
                          <Modal.Title id='ModalHeader' className="headerTitle">Add Employee</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <div className="row">
                            <div className="col-md-12">
                              <Card bsClass={["innerCard mb-none"]} content={
                                <EmployeeForm
                                  closeModel={HF.closeModal.bind(this, "addModalEmployee")}
                                  formTypePro="add"
                                  employeePro={OC.EMPLOYEE}
                                  handleEmployeeSubmit= { this.handleEmployeeSubmit }
                                  componentPro="adminUser"
                                  roles={roles}
                                />
                              } />
                            </div>
                          </div>
                        </Modal.Body>
                      </Modal>
                    }

                    {/*Add Employee Modal End*/}

              		  {/*Edit Employee Modal start*/}
                    { editEmployee &&
                      <Modal backdrop={'static'} show={this.state.editModalEmployee} onHide={HF.closeModal.bind(this, "editModalEmployee")} aria-labelledby="ModalHeader" >
                      <Modal.Header closeButton>
                        <Modal.Title id='ModalHeader' className="headerTitle">Edit Employee</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="row">
                          <div className="col-md-12">
                          <Card bsClass={["innerCard mb-none"]} content={
                            <EmployeeForm
                              closeModel={HF.closeModal.bind(this, "editModalEmployee")}
                              formTypePro="edit"
                              employeePro={editEmployee}
                              handleEmployeeSubmit= { this.handleEmployeeSubmit }
                            />
                          } />
                          </div>
                        </div>
                      </Modal.Body>
                      </Modal>
                    }
                    {/*Edit Employee Modal End*/}
                	</div>
                } />
            </Col>
          </Row>
        </Grid>
      </div>
  	);
  };
}

const mapDispatchToProps = dispatch => {
  return ({
    getEmployeesCall  : (page, filter, sort, pageSize) => { dispatch(resumeActions.getEmployees(page, filter, sort, pageSize)) },
    getEmployeeCall   : (id) => { dispatch(resumeActions.getEmployee(id)) },
    storeEmployeeCall : (EmployeeData) => { dispatch(resumeActions.storeEmployee(EmployeeData)) },
    editEmployeeCall  : (EmployeeData, id) => { dispatch(resumeActions.editEmployee(EmployeeData, id)) },
    deleteEmployeeCall: (id) => { dispatch(resumeActions.deleteEmployee(id)) },
    updateEmployeeStatusCall   : (id, data) => { return dispatch(resumeActions.updateEmployeeStatus(id, data)) },

  });
};

function mapStateToProps(state) {
  const {  editEmployee, loading, employeesData, pages, storedEmployee, updatedEmployee, deleteEmployee, current_page } = state.resumeReducer;
  return {
    employeesData, loading, pages, storedEmployee, updatedEmployee, editEmployee, deleteEmployee, current_page
  };
}
const ResumeEmployees = connect(mapStateToProps, mapDispatchToProps)(ResumeEmployeesPage);
export default ResumeEmployees;
