import React from "react";
import {
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Image
} from "@react-pdf/renderer";
import moment from "moment";


const styles = StyleSheet.create({
    page: {
        backgroundColor: "#ffffff"
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    },
    Container: {
        backgroundColor: "#f6f6f5",
        display: "flex",
        flexDirection: "row",
        padding: 5
    },
    Details: {
        display: "flex",
        marginLeft: 5
    },
    Title: {
        fontSize: 15,
        marginBottom: 10
    },

    
    subtitle: {
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "row",
        width: 150,
        alignItems: "center",
        marginBottom: 12
    },
    vote: {
        display: "flex",
        flexDirection: "row"
    },
    rating: {
        height: 10,
        width: 10
    },
    vote_text: {
        fontSize: 10
    },
    vote_pop: {
        fontSize: 10,
        padding: 2,
        backgroundColor: "#61C74F",
        color: "#fff"
    },
    vote_pop_text: {
        fontSize: 10,
        marginLeft: 4
    },
    overviewContainer: {
        minHeight: 110
    },
    detailsFooter: {
        display: "flex",
        flexDirection: "row"
    },
    lang: {
        fontSize: 8,
        fontWeight: 700
    },
    vote_average: {
        fontSize: 8,
        marginLeft: 4,
        fontWeight: "bold"
    }
});

export default function PdfDocument(props) {
    console.log("pdf props", props.data);
    return (
        <Document>
            <Page style={styles.page}>
               
                                <View style={styles.movieContainer}>
                                    <View style={styles.movieDetails}>
                                        <Text style={styles.movieTitle}>{props.data.name}</Text>
                                                                     
                                </View>
                                <View style={styles.movieDetails}>
                                <Text style={styles.Heading}>Skills</Text>
                                            {props.data.skills.map((shareholder, idx) => (
                                            <Text style={styles.subtitle}>{shareholder.name}, {shareholder.skill_level_percent}%</Text>
                                            ))}          
                                </View>  
                                <View style={styles.movieDetails}>
                                <Text style={styles.Heading}>Skills</Text>
                                            {props.data.skills.map((shareholder, idx) => (
                                            <Text style={styles.subtitle}>{shareholder.name}, {shareholder.skill_level_percent}%</Text>
                                            ))}          
                                </View>  

                    </View>
            </Page>
        </Document>
    );
}
