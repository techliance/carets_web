
import React, { Component } from "react";
import {
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Image
} from "@react-pdf/renderer";

class TestingPDF extends Component {
	render(){
        console.log("Testing PDF");
		return(
			<Document>
                <Page size="A4">
                    <View><Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim lectus varius facilisis semper. Vivamus suscipit purus in nulla consectetur egestas. Cras pharetra ullamcorper tortor, sed vehicula lectus ornare eget. Quisque nec ultricies sapien, sed vulputate turpis. Ut sollicitudin convallis dolor, at auctor tortor sollicitudin id. Donec ornare, felis vel tristique accumsan, tortor erat laoreet leo, in hendrerit risus ligula eu elit. Suspendisse id mauris tristique massa mattis tincidunt a cursus turpis. Aliquam porttitor tristique mauris in molestie. In hac habitasse platea dictumst. Suspendisse pharetra suscipit ligula, venenatis varius mauris pharetra vel.

Ut vehicula magna elit, rutrum maximus libero blandit vel. Fusce feugiat lacus in erat posuere, eget rutrum est rhoncus. Sed quis neque purus. Ut bibendum, neque vitae elementum commodo, urna sapien condimentum ligula, vitae vestibulum ex urna vitae lacus. Ut feugiat pharetra condimentum. Sed ut sapien in felis sagittis vehicula. Donec in bibendum justo. Nullam tristique euismod neque vitae volutpat. Sed elementum lacus vel orci porta eleifend. Duis eget justo leo. Integer nisl turpis, gravida nec diam ac, ultrices commodo nulla. Vivamus in arcu cursus, elementum sem quis, gravida risus.

Phasellus cursus in augue quis posuere. Cras blandit imperdiet quam, ac mattis velit auctor in. Aliquam sapien nibh, rhoncus at dapibus porttitor, suscipit sit amet metus. Donec egestas tortor eget tempus malesuada. Etiam rhoncus mauris in mauris vulputate vehicula. Vivamus tempus sodales quam ac rutrum. Etiam imperdiet commodo orci, ac ultrices nunc feugiat a. Curabitur hendrerit commodo commodo. Aliquam ac ex in libero suscipit ullamcorper. Vestibulum et mi tortor. Nam id dolor semper, pellentesque augue non, pretium diam.

Vestibulum feugiat, tortor ac porttitor aliquet, metus nulla euismod sapien, quis cursus orci dui id enim. Mauris pharetra magna lacus, ac tempor neque pulvinar id. Aliquam et feugiat turpis, vitae elementum dolor. Curabitur nunc massa, convallis ut cursus a, pellentesque quis ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id mi lacus. Sed in mattis sapien. Suspendisse in cursus ex. Nullam congue in mi a lobortis. Nam pellentesque ex non nunc dapibus, id maximus libero aliquet. Quisque nec lacinia nisl. Nunc non justo vitae orci mattis commodo a venenatis est. Nam scelerisque porttitor augue at varius.

Vivamus sodales scelerisque suscipit. Donec a ligula molestie, pulvinar diam gravida, imperdiet tellus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim lectus varius facilisis semper. Vivamus suscipit purus in nulla consectetur egestas. Cras pharetra ullamcorper tortor, sed vehicula lectus ornare eget. Quisque nec ultricies sapien, sed vulputate turpis. Ut sollicitudin convallis dolor, at auctor tortor sollicitudin id. Donec ornare, felis vel tristique accumsan, tortor erat laoreet leo, in hendrerit risus ligula eu elit. Suspendisse id mauris tristique massa mattis tincidunt a cursus turpis. Aliquam porttitor tristique mauris in molestie. In hac habitasse platea dictumst. Suspendisse pharetra suscipit ligula, venenatis varius mauris pharetra vel.

Ut vehicula magna elit, rutrum maximus libero blandit vel. Fusce feugiat lacus in erat posuere, eget rutrum est rhoncus. Sed quis neque purus. Ut bibendum, neque vitae elementum commodo, urna sapien condimentum ligula, vitae vestibulum ex urna vitae lacus. Ut feugiat pharetra condimentum. Sed ut sapien in felis sagittis vehicula. Donec in bibendum justo. Nullam tristique euismod neque vitae volutpat. Sed elementum lacus vel orci porta eleifend. Duis eget justo leo. Integer nisl turpis, gravida nec diam ac, ultrices commodo nulla. Vivamus in arcu cursus, elementum sem quis, gravida risus.

Phasellus cursus in augue quis posuere. Cras blandit imperdiet quam, ac mattis velit auctor in. Aliquam sapien nibh, rhoncus at dapibus porttitor, suscipit sit amet metus. Donec egestas tortor eget tempus malesuada. Etiam rhoncus mauris in mauris vulputate vehicula. Vivamus tempus sodales quam ac rutrum. Etiam imperdiet commodo orci, ac ultrices nunc feugiat a. Curabitur hendrerit commodo commodo. Aliquam ac ex in libero suscipit ullamcorper. Vestibulum et mi tortor. Nam id dolor semper, pellentesque augue non, pretium diam.

Vestibulum feugiat, tortor ac porttitor aliquet, metus nulla euismod sapien, quis cursus orci dui id enim. Mauris pharetra magna lacus, ac tempor neque pulvinar id. Aliquam et feugiat turpis, vitae elementum dolor. Curabitur nunc massa, convallis ut cursus a, pellentesque quis ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id mi lacus. Sed in mattis sapien. Suspendisse in cursus ex. Nullam congue in mi a lobortis. Nam pellentesque ex non nunc dapibus, id maximus libero aliquet. Quisque nec lacinia nisl. Nunc non justo vitae orci mattis commodo a venenatis est. Nam scelerisque porttitor augue at varius.

Vivamus sodales scelerisque suscipit. Donec a ligula molestie, pulvinar diam gravida, imperdiet tellus. Maecenas sodales ante vel velit maximus, vitae venenatis libero sagittis. Donec sollicitudin sollicitudin est non semper. Integer accumsan nunc sit amet blandit vulputate. Nulla ultricies elit id imperdiet vestibulum. Fusce feugiat dignissim vestibulum. Curabitur volutpat nulla id molestie interdum. Pellentesque ac vestibulum mi. Morbi congue a ligula sed consectetur. Donec ac lacus nulla. Nunc ornare magna ut ullamcorper porttitor. Maecenas sodales ante vel velit maximus, vitae venenatis libero sagittis. Donec sollicitudin sollicitudin est non semper. Integer accumsan nunc sit amet blandit vulputate. Nulla ultricies elit id imperdiet vestibulum. Fusce feugiat dignissim vestibulum. Curabitur volutpat nulla id molestie interdum. Pellentesque ac vestibulum mi. Morbi congue a ligula sed consectetur. Donec ac lacus nulla. Nunc ornare magna ut ullamcorper porttitor.</Text></View>
                </Page>
            </Document>
		);
	};
}



export default TestingPDF;
