import React, { Component } from "react";
import { connect } from 'react-redux';
import { resumeActions } from "redux/actions/resume-actions";
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import { objectsConstants as OC } from 'redux/constant/objects-constant';
import Pdf from "react-to-pdf";

import { Row, Col, Grid } from "react-bootstrap";
import '../../../assets/css/circle.css';
import '../../../assets/css/resume.css';

class ResumeViewPage extends Component {
  constructor(props) {
    super(props);
    const { id } = this.props.match.params;
    this.ref = React.createRef();
    const options = {
        orientation: 'landscape',
        unit: 'in',
        format: [4,2]
    };
    
    this.state = {
        employeeId: id,
        employee: OC.EMPLOYEE,
    }
    this.props.getEmployeeCall(this.state.employeeId);    
  }
  componentDidUpdate(prevProps, nextProps) {

   if (this.props.editEmployee !== prevProps.editEmployee) {
        this.setState({ employee: this.props.editEmployee });
    }
  }
  render() {
    console.log(this.state.employee);
    const {employee} = this.state;
    return (
    <div class="main_page" id="height-example">
         <Pdf targetRef={this.ref} filename="techliance.pdf" options={this.options} x={.5} y={.5}>
            {({ toPdf }) => <button onClick={toPdf}>Generate Pdf</button>}
        </Pdf>
        
        <div className="row" style={{ padding: 50 }} ref={this.ref}>
        <Col md={4} >
            <div>

            </div>

            <div className="name_small">{employee.name}</div>
            <div className="job_title">{employee.job_title}</div>

            <div className="text-center content_container">
                <div className="icon_container">
                    <i className="icon-user"></i>
                </div>

                <div className="content_heading">
                    <span>Profile</span>
                </div>

                <p className="gen_text">{employee.career_summary}</p>
            </div>

            

            <div className="text-center content_container">
                <div className="icon_container">
                    <i className="icon-wrench"></i>
                </div>

                <div className="content_heading">
                    <span>Skills</span>
                </div>
                {employee.skills.map((shareholder, idx) => (
                <div className="skill_container pull-left">
                    <div className="skill_name">{shareholder.name}</div>
                    <div className="c100 p85 xt-small">
                        <span>{shareholder.skill_level_percent}%</span>
                        <div className="slice">
                            <div className="bar"></div>
                            <div className="fill"></div>
                        </div>
                    </div>
                </div>
                ))}
            </div>

            <div className="text-center content_container">
                <div className="icon_container" >
                    <i className="icon-game-controller"></i>
                </div>

                <div className="content_heading">
                    <span>Hobbies</span>
                </div>

                <p className="text-center mt_xs">
                {employee.hobbies.map((value, idx) => (
                    <span className="hobby_icon">{value}</span>
                ))}
                </p>
            </div>
        </Col>

        <Col md={8} >
            <div className="text-center content_container" >
                <div className="icon_container">
                    <i className="icon-graduation"></i>
                </div>

                <div className="content_heading">
                    <span>Education</span>
                </div>
                {employee.educations.map((shareholder, idx) => (
                <div className="job_history mt_xlg">
                    <div className="pull-left align-left mb_sm">
                        <div className="job_title align-left">
                            {shareholder.institute_name}
                        </div>
                        <p className="gen_text muted"><i>{shareholder.degree_title}</i></p>
                    </div>

                    <div className="job_title pull-right mb_sm">{HF.dateYear(shareholder.year_start)} &nbsp; - &nbsp; {HF.dateYear(shareholder.year_end)}</div>
                    <div className="clearfix"></div>
                   
                </div>
                ))}
            </div>

            <div className="text-center content_container">
                <div className="icon_container">
                    <i className="icon-hourglass"></i>
                </div>

                <div className="content_heading">
                    <span>Employment History</span>
                </div>
                {employee.eployeement.map((shareholder, idx) => (
                <div className="job_history mt_xlg">
                    <div className="pull-left align-left mb_sm">
                        <div className="job_title align-left">
                        {shareholder.company_name}
                        </div>
                        <p className="gen_text muted"><i>{shareholder.job_title}</i></p>
                    </div>

                    <div className="job_title pull-right mb_sm">{HF.dateYearMonth(shareholder.job_start)} &nbsp; - &nbsp; {HF.dateYearMonth(shareholder.job_end)}</div>
                    <div className="clearfix"></div>
                    <p className="gen_text align-left"> <b>Responsibilities:</b> {shareholder.job_details} </p>

                </div>
                ))}
                
            </div>

            <div className="text-center content_container">
                <div className="icon_container">
                    <i className="icon-briefcase"></i>
                </div>

                <div className="content_heading">
                    <span>Projects</span>
                </div>
                {employee.projects.map((shareholder, idx) => (
                <div className="job_history mt_xlg">
                    <div className="pull-left align-left mb_sm">
                        <div className="job_title align-left">
                        {shareholder.project_name}
                        </div>
                    </div>

                    <div className="clearfix"></div>
                    <p className="gen_text align-left">{shareholder.project_details} </p>

                </div>
            ))}
                
               

                

               

               
            </div>
        </Col>
     </div>
    </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
    return ({
      getEmployeeCall   : (id) => { dispatch(resumeActions.getEmployee(id)) },  
    });
  };
  
  function mapStateToProps(state) {
    const {  editEmployee, loading } = state.resumeReducer;
    return {
     loading, editEmployee
    };
  }
  const ResumeView = connect(mapStateToProps, mapDispatchToProps)(ResumeViewPage);
  export default ResumeView;