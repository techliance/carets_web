import React, { Component } from "react";
import { connect } from 'react-redux';
import { resumeActions } from "redux/actions/resume-actions";
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import { objectsConstants as OC } from 'redux/constant/objects-constant';
import { PDFDownloadLink } from '@react-pdf/renderer'
import PdfDocument from "./Movie";
import {
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Image
} from "@react-pdf/renderer";

import { Row, Col, Grid } from "react-bootstrap";
import '../../../assets/css/circle.css';
import '../../../assets/css/resume.css';
const styles = StyleSheet.create({
    page: {
        backgroundColor: "#ffffff"
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    },

    Container: {
        display: "flex",
        flexDirection: "row",
        padding: 5
    },
    column1: {
        display: "flex",
        flexDirection: "column",
        padding: 5,
        width:'33%'
    },
    column2: {
        display: "flex",
        flexDirection: "column",
        padding: 5,
        width:'66%'
    },

    Details: {
        display: "flex",
        display: "flex",
        marginLeft: 5
    },

    Title: {
        fontSize: 20,
        display: "flex",
        flexDirection: "row",
    },
    Heading: {
        fontSize: 20,
        flexDirection: "row",
    },
    
    subtitle: {
        fontSize: 15,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
    },
});
class ResumeNewPage extends Component {
  constructor(props) {
    super(props);
    const { id } = this.props.match.params;    
    
    this.state = {
        employeeId: id,
        employee: OC.EMPLOYEE,
        ready: true,
        foo: 'Unchanged Value',
    }
    this.props.getEmployeeCall(this.state.employeeId);    
  }
  setStateFromLink(state) { // set state from incoming <Link>
    if(state) {
      const { foo } = state 
      this.setState({ foo })
    }    

    
  }

  
  componentDidUpdate(prevProps, nextProps) {

   if (this.props.editEmployee !== prevProps.editEmployee) {
        this.setState({ employee: this.props.editEmployee });
        
        this.setState({ ready: false });
        setTimeout(()=>{
        this.setState({ ready: true });
        }, 1);
    }
  }

  

  render() {
    console.log("state data",this.state.employee);
    const {employee} = this.state;
    if (this.state.ready) {
    return (
    <Document>
        <Page >
        <View style={styles.Container}>
        <PDFDownloadLink
                document={<PdfDocument data={this.state.employee}/>}
                fileName="resume.pdf"
                style={{
                textDecoration: "none",
                padding: "10px",
                color: "#4a4a4a",
                backgroundColor: "#f2f2f2",
                border: "1px solid #4a4a4a"
                }}
            >
                {({ blob, url, loading, error }) =>
                loading ? "Loading document..." : "Download Pdf"
                }
            </PDFDownloadLink>
            
        </View>     
        <View style={styles.Container}>
                    <View style={styles.column1}>
                            <Text style={styles.Title}>{employee.name}</Text>
                            <Text style={styles.subtitle}>{employee.job_title}</Text>
                            <Text style={styles.Heading}>Profile</Text>
                            <Text style={styles.subtitle}>{employee.career_summary}</Text>
                            <Text style={styles.Heading}>Skills</Text>
                            {employee.skills.map((shareholder, idx) => (
                            <Text style={styles.subtitle}>{shareholder.name}, {shareholder.skill_level_percent}%</Text>
                            ))}

                            <Text style={styles.Heading}>Hobbies</Text>
                            {employee.hobbies.map((value, idx) => (
                            <Text style={styles.subtitle}>{value}</Text>
                            ))}

                    </View>        

                    <View style={styles.column2}>
                        <Text style={styles.Heading}>Education</Text>
                        {employee.educations.map((shareholder, idx) => (
                            <View>
                                <Text style={styles.subtitle}>{shareholder.institute_name}</Text>
                                <Text style={styles.subtitle}>{shareholder.degree_title}</Text>
                                <Text style={styles.subtitle}>{HF.dateYear(shareholder.year_start)} &nbsp; - &nbsp; {HF.dateYear(shareholder.year_end)}</Text>
                            </View>
                            
                            ))}
                         
                         <Text style={styles.Heading}>Employment History</Text>
                         {employee.eployeement.map((shareholder, idx) => (
                            <View>
                                <Text style={styles.subtitle}>{shareholder.company_name}</Text>
                                <Text style={styles.subtitle}>{shareholder.job_title}</Text>
                                <Text style={styles.subtitle}>{HF.dateYearMonth(shareholder.job_start)} &nbsp; - &nbsp; {HF.dateYearMonth(shareholder.job_end)}</Text>
                                <Text style={styles.subtitle}><b>Responsibilities:</b> {shareholder.job_details} </Text>
                            </View>
                            
                            ))}
                         
                         <Text style={styles.Heading}>Projects</Text>
                         {employee.projects.map((shareholder, idx) => (
                            <View>
                                <Text style={styles.subtitle}>{shareholder.project_name}</Text>
                                <Text style={styles.subtitle}>{shareholder.project_details}</Text>
                            </View>
                            
                            ))}


                    </View>  
        </View>   
        </Page>
    </Document>    
    );
}else{
    return null
}
  }
}
const mapDispatchToProps = dispatch => {
    return ({
      getEmployeeCall   : (id) => { dispatch(resumeActions.getEmployee(id)) },  
    });
  };
  
  function mapStateToProps(state) {
    const {  editEmployee, loading } = state.resumeReducer;
    return {
     loading, editEmployee
    };
  }
  const ResumeNew = connect(mapStateToProps, mapDispatchToProps)(ResumeNewPage);
  export default ResumeNew;