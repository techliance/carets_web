import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import { connect } from "react-redux";
import "react-confirm-alert/src/react-confirm-alert.css";
import { settingsActions } from "../../redux/actions/settings-actions";

class DashboardSuperAdminPage extends Component {
  constructor(props) {
    super(props)
    this.props.getOrderStatuses()
  }

  render() {
    const { orderStatuses } = this.props
    orderStatuses.find(item => {
      return item.label === 'Pending'
    })
    return (
      <div className="content">
        Admin Dashboard
      </div>
    );
  }
}


function mapStateToProps(state) {
  const { loading } = state.adminReducer;
  const { orderStatuses } = state.settingsReducer
  return {
    loading,
    orderStatuses
  };
}

const mapDispatchToProps = dispatch => {
  return {
    getOrderStatuses: () => { dispatch(settingsActions.getOrderStatuses()) }
  }
}

const DashboardSuperAdmin = connect(
  mapStateToProps, mapDispatchToProps
)(DashboardSuperAdminPage);

export default DashboardSuperAdmin;
