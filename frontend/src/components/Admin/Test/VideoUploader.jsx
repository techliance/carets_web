import React, { Component } from 'react';
import { createFFmpeg, fetchFile } from '@ffmpeg/ffmpeg';
import axios from 'axios';
import { videoService } from '../../../services/video';

class VideoUploader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      compressedFile: null,
      uploadProgress: 0,
      isCompressing: false,
      isUploading: false,
    };

    this.ffmpeg = createFFmpeg({ log: true });
  }

  componentDidMount() {
    this.loadFFmpeg();
  }

  loadFFmpeg = async () => {
    if (!this.ffmpeg.isLoaded()) {
      await this.ffmpeg.load();
    }
  };

  handleFileChange = (e) => {
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      this.setState({ file: selectedFile });
    }
  };

  compressVideo = async () => {
    const { file } = this.state;
    if (!file || !this.ffmpeg.isLoaded()) return;

    this.setState({ isCompressing: true });

    // Load the file into FFmpeg
    this.ffmpeg.FS('writeFile', file.name, await fetchFile(file));

    // Run FFmpeg command to compress the video
    await this.ffmpeg.run(
      '-i', file.name,
      '-vcodec', 'libx264',
      '-crf', '28', // Lower values mean better quality and bigger size
      'compressed.mp4'
    );

    // Retrieve the compressed file
    const compressedData = this.ffmpeg.FS('readFile', 'compressed.mp4');
    const compressedBlob = new Blob([compressedData.buffer], { type: 'video/mp4' });
    const compressedUrl = URL.createObjectURL(compressedBlob);

    this.setState({ compressedFile: compressedBlob, isCompressing: false });
  };

  uploadVideo = async () => {
    const { compressedFile } = this.state;
    if (!compressedFile) return;

    this.setState({ isUploading: true });

    const formData = new FormData();
    formData.append('video', compressedFile, 'compressed.mp4');

    try {
      // Correctly pass the `onUploadProgress` function as part of the configuration object
      await videoService.uploadtest(formData, {
        onUploadProgress: (progressEvent) => {
          const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
          this.setState({ uploadProgress: percentCompleted });
        },
      });

      alert('Upload successful!');
    } catch (error) {
      console.error('Upload failed:', error);
      alert('Upload failed!');
    } finally {
      this.setState({ isUploading: false });
    }
  };

  render() {
    const { file, compressedFile, uploadProgress, isCompressing, isUploading } = this.state;

    return (
      <div>
        <h2>Video Uploader with Compression</h2>
        <input type="file" accept="video/*" onChange={this.handleFileChange} />

        {file && (
          <div>
            <p>Original File: {file.name}</p>
            <button onClick={this.compressVideo} disabled={isCompressing}>
              {isCompressing ? 'Compressing...' : 'Compress Video'}
            </button>
          </div>
        )}

        {compressedFile && (
          <div>
            <p>Compressed File Ready for Upload</p>
            <button onClick={this.uploadVideo} disabled={isUploading}>
              {isUploading ? 'Uploading...' : 'Upload Video'}
            </button>
          </div>
        )}

        {isUploading && (
          <div>
            <progress value={uploadProgress} max="100" />
            <p>Upload Progress: {uploadProgress}%</p>
          </div>
        )}
      </div>
    );
  }
}

export default VideoUploader;
