import React, { Component } from "react";
import { connect } from 'react-redux';
import { orderActions } from "redux/actions/order-actions";
import ReactValidator from "simple-react-validator";
import DatePicker from "react-datepicker";
import { Row, Col, Grid } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import ReactTable from "react-table";
import { Link } from 'react-router-dom';
import 'react-table/react-table.css';
// import ReactPlayer from 'react-player';
import { helperFunctions as HF } from 'helper/helperFunctions';
const moment = window.moment;

class CustomerServicesPage extends Component {
	constructor(props) {
		super(props);
		this.dateValidator = new ReactValidator();
		this.state = {
			fromDate : '',
			to: '',
			pageSize: 10,
      		page    : 0,
      		sorted: '',
		}
	}
	DateChangeHandler = (value, field) => {
		this.setState({ [field] :  value === null ? '' : moment(value).format("YYYY-MM-DD") }, () => {
			if (this.dateValidator.allValid()) {
				const { fromDate, to, page, pageSize, sorted } = this.state;
    			this.props.getCsOrdersCall('finalize_by_qa', 1, fromDate, to , page, pageSize, sorted);
			} else {
				this.dateValidator.showMessages();
            	this.forceUpdate();
			}
		})
	}
	dateElement(label, name, value, rules) {
		var selectedDate =  this.state[name] !== '' ? new Date(this.state[name]) :'';
	    const { fromDate, to } = this.state;
	    return (
		    <div className='form-group fullCalendar' id={name}>
          		<label>{label}</label>
          		<DatePicker className="form-control calender_icon"  selected={  selectedDate } name={name} onChange={ (event) => this.DateChangeHandler(event, name) } />
          		{
          			( fromDate !== '' && to !== '' ) &&
          			this.dateValidator.message(name, value, rules)
          		}	
		    </div>
	    );
  	}
  	getCsOrdersData = (page = this.state.page, sorted = this.state.sorted, pageSize = this.state.pageSize) => {
  		const { fromDate, to} = this.state;
    	this.props.getCsOrdersCall('finalize_by_qa', 1, fromDate, to , page, pageSize, sorted);
  	}
	render() {
		const { to, fromDate } = this.state;
		const { csOrderData, OWSpages, loadingOrder } = this.props;
		const columns = [
	      	{
		        Header: "Completed Orders Listing",
		        headerClassName: 'headerPagination',
		        columns: [
		        	{
			            Header: "Barcode",
			            accessor: "barcode",
                        Cell: ({ value, original }) => (                              
                                <Link to={'/Admin/' + original.id + '/order-edit'} className="blue_link barcode">{value}</Link>
                        ),
			            className: "action-center"
		          	},
		          	{
			            Header: "Date Finished",
			            accessor: "updated_at",
						Cell: ({ value }) => (
						  <div>
							{HF.dateFormatMy(value)}
						  </div>
						),
			            className: "action-center",
			            sortable:false,
			        },
		          	{
			            Header: "Editor",
			            accessor: "editor.name",
			            className: "action-center",
			            sortable:false,
		          	},
		          	{
			            Header: "Produced By",
			            accessor: "producer.name",
			            className: "action-center",
			            sortable:false,
		          	},
		          	{
			            Header: "Quality Assurance",
			            accessor: "qa.name",
			            className: "action-center",
			            sortable:false,
		          	}
		        ]
	      	}
    	];
		return(
			<div className="content">
        		<Grid fluid>
          			<Row>
            			<Col md={12}>
              				<Card
                				ctTableResponsive
                				content={
              					<div>
                					<Grid fluid>
                  						<div className="row">
                  							<Col md={4} >
							                    <div className='row flexElem flexResponsive align-items-center' >
							                        <div className="col-md-6" >	
													{
														this.dateElement('Date From', 'fromDate', fromDate!=='' && moment(fromDate, 'YYYY-MM-DD'), [{ before_or_equal: moment(this.state.to) }])
													}
													</div>
							                    </div>
			                				</Col>
							                <Col md={4} >
							                    <div className='row flexElem flexResponsive align-items-center' >
							                        <div className="col-md-6" >	
													{
														this.dateElement('Date To', 'to', to!=='' && moment(to, 'YYYY-MM-DD'), [{ after_or_equal: moment(this.state.fromDate) }])
													}
													</div>
							                        
							                    </div>
							                </Col>
							                <Col md={12} className="mt-md">
					                        	<ReactTable
						                            noDataText= 'No orders report found'
						                            data={csOrderData}
						                            pages={OWSpages}
						                            columns={columns}
						                            defaultPageSize={10}
						                            loading={loadingOrder}
						                            className="-striped listing"
						                            loadingText={'Loading...'}
						                            pageData={this.getCsOrdersData}
						                            manual
						                            onFetchData={(state, instance) => {
						                             
						                            	var sort = state.sorted.length === 0 ? '' : state.sorted[0].id + ',desc:' + state.sorted[0].desc;
                              							state.pageData(state.page + 1, sort, state.pageSize);
						                            }}
					                          	/>
					                        </Col>
                  						</div>
                					</Grid>
              					</div>
                			} />
            			</Col>
          			</Row>
        		</Grid>
        	</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return ({
		getCsOrdersCall: (status, value, fromDate, toDate, page, pageSize, sorted) => { dispatch(orderActions.getCustomerServicesOrders(status, value, fromDate, toDate, page, pageSize, sorted)) },//example function
	});
};

function mapStateToProps(state) {
	const { csOrderData, OWSpages, OWScurrent_page, OWStotal_orders, loadingOrder, ordersCountWithModerator } = state.ordersReducer;
	return {
		csOrderData, OWSpages, OWScurrent_page, OWStotal_orders, loadingOrder, ordersCountWithModerator
	};
}


const CustomerServices = connect(mapStateToProps, mapDispatchToProps)(CustomerServicesPage);
export default CustomerServices;