import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Row,
  Col,
  OverlayTrigger,
  Popover,
} from "react-bootstrap";

import 'react-confirm-alert/src/react-confirm-alert.css';
import { Card } from "components/Card/Card.jsx";
import { hasPermission } from 'helper/hasPermission';
import { helperFunctions as HF } from '../../../helper/helperFunctions';
import { orderActions } from '../../../redux/actions/order-actions';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { Link } from 'react-router-dom';
import { adminLabels } from '../../../redux/constant/admin-label-constant';
import { ThemeFunctions as TF } from 'helper/ThemeFunctions';

class OrderListingForModeratorPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageSizeTotal: 0,
      sorted: '',
      filtered: '',
      orderPage: 0,
    };
    this.ordersPaginationData = this.ordersPaginationData.bind(this);
    this.filterSearchHandle = this.filterSearchHandle.bind(this);
  };
  
  assignEditorToOrder = (orderId) => {
    this.props.assignUserToOrderCall(orderId , {userId:this.props.userAdmin.user.id, role:'editor'});
  }
  assignProductionToOrder = (orderId) => {
    this.props.assignUserToOrderCall(orderId , {userId:this.props.userAdmin.user.id, role:'producer'});
  }
  assignQAToOrder = (orderId) => {
    this.props.assignUserToOrderCall(orderId , {userId:this.props.userAdmin.user.id, role:'qa'});
  }
  ordersPaginationData(page, filter, sort, pageSize) {
    this.setState({ orderPage: page, filtered: filter, sorted: sort, pageSizeTotal: pageSize }, function () {
      var searchData = {};
      this.props.getOrdersPaginationCall(page, filter, sort, 10, searchData);
    });
  }
  filterSearchHandle(e) {
    this.setState({
      'filtered': e.target.value
    });
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.order_details !== this.props.order_details) {
      var searchData = {};
      const { orderPage, filtered, sorted, pageSizeTotal  } = this.state;
      this.props.getOrdersPaginationCall(orderPage, filtered, sorted, pageSizeTotal, searchData);
    }
  }
  render() {
    const { filtered} = this.state;
    const { ordersData, pages, loadingOrder} = this.props;
    let columns = [
      {
        Header: "Orders Listing",
        headerClassName: 'headerPagination',
        columns: [
          {
            Header: "Editor",
            accessor: "created_at",
            Cell: ({ value, original }) => (
              <div >
              {
                original.editor_id ? original.editor.image?<img alt={original.editor.image.title} className="initialsLogo" src={original.editor.image.preview} />:TF.initialsLogo(original.editor.name) : 
                <OverlayTrigger placement="bottom" overlay={<Popover id="tooltip">{'Order assign me for editing process'}</Popover>}>
                  <button className="orderDetailsAction assignIcon" onClick={()=>this.assignEditorToOrder(original.id)} >&nbsp;</button>
                </OverlayTrigger>
              }
              </div>
            ),
            className: "action-center",
            show:HF.hasRole(['editor'])
          },
          {
            Header: "Production",
            accessor: "created_at",
            Cell: ({ value, original }) => (
              <div >
              {
                original.producer_id 
                ? 
                  original.producer.image
                  ?
                    <img alt={original.producer.image.title} className="initialsLogo" src={original.producer.image.preview} />
                    :
                      TF.initialsLogo(original.producer.name)
                  : 
                    <OverlayTrigger placement="bottom" overlay={
                      <Popover id="tooltip" typ={console.log('original.settings 789', original.settings)} >
                      {
                        "finalize_by_editor" in original.settings
                          ? 
                            parseInt(original.settings.finalize_by_editor.value, 10) === 1
                            ?
                              'Order assign me for production process'
                            :
                              'Order is yet to be finalized by editor'
                          :
                            'setting is not available'
                      }
                      </Popover>
                    }>
                    {
                      "finalize_by_editor" in original.settings
                        ? 
                          parseInt(original.settings.finalize_by_editor.value, 10) === 1
                          ?
                            <button className="orderDetailsAction assignIcon" onClick={()=>this.assignProductionToOrder(original.id)} >&nbsp;</button>
                          :
                            <button className="orderDetailsAction assignIcon button_disabled" >&nbsp;</button>
                        :
                          'setting is not available'          
                    }
                      
                    </OverlayTrigger>
              }
              </div>
            ),
            className: "action-center",
            show:HF.hasRole(['producer'])
          },
          {
            Header: "QA",
            accessor: "created_at",
            Cell: ({ value, original }) => (
              <div >
              {
                original.qa_id 
                ? 
                  original.qa.image
                  ?
                    <img className="initialsLogo" src={original.qa.image.preview} alt="imagepreview" />
                    :
                      TF.initialsLogo(original.qa.name)
                  : 
                    <OverlayTrigger placement="bottom" overlay={
                      <Popover id="tooltip"  >
                      {
                        "finalize_by_pm" in original.settings
                          ? 
                            parseInt(original.settings.finalize_by_pm.value, 10) === 1
                            ?
                              'Order assign me for QA'
                            :
                              'Order is yet to be finalized by PM'
                          :
                            'setting is not available'
                      }
                      </Popover>
                    }>
                    {
                      "finalize_by_pm" in original.settings
                        ? 
                          parseInt(original.settings.finalize_by_pm.value, 10) === 1
                          ?
                            <button className="orderDetailsAction assignIcon" onClick={()=>this.assignQAToOrder(original.id)} >&nbsp;</button>
                          :
                            <button className="orderDetailsAction assignIcon button_disabled" >&nbsp;</button>
                        :
                          'setting is not available'          
                    }
                      
                    </OverlayTrigger>
              }
              </div>
            ),
            className: "action-center",
            show:HF.hasRole(['qa'])
          },
          {
            Header: "Barcode",
            accessor: "barcode",
            className: "action-center",
            Cell: ({ value, original }) => (
              /*add true in below conditions just for time to ignore condition */
              <OverlayTrigger placement="bottom" overlay={<Popover id="tooltip">{( hasPermission('order-edit') && original.editor_id === this.props.userAdmin.user.id) || true ? 'Edit Order' : adminLabels.DONT_ALLOW}</Popover>}>
                  {
                    
                    ( hasPermission('order-edit') && original.editor_id === this.props.userAdmin.user.id ) || true ?
                    <Link to={'/Admin/' + original.id + '/order-edit'} className="blue_link barcode">
                      <span className={
                            HF.colorForBardcodeWithDate(original.order_dates.last_day_of_school.value, 'lastDayOfSchool') + " " +
                            HF.colorForBardcodeWithDate(original.order_dates.video_special_need_date.value, 'videoSpecialNeedDate')
                          }
                      >
                          {value}
                      </span>
                    </Link>
                    :
                    <span className="barcode">{value}</span>
                  }
              </OverlayTrigger>
            ),
          },
          {
            Header: "Agency Name",
            accessor: "agency",
            Cell: ({ value }) => (
              <div>
                {value.name}
              </div>
            ),
            className: "action-center"
          },
          {
            Header: "Group Type",
            accessor: "agency_type",
            Cell: ({ value }) => (
              <div>
                {value.title}
              </div>
            ),
            className: "action-center",
          },
          {
            Header: "Destinations",
            accessor: "destinations",
            className: "action-center"
          },
          {
            Header: "Return Date",
            accessor: "order_dates",
            Cell: ({ value }) => (
              <div>
                {HF.dateFormatMy(value.return_date.value)}
              </div>
            ),
            className: "action-center"
          },
          {
            Header: "Last Day At School",
            accessor: "order_dates",
            Cell: ({ value }) => (
              <div>
                {HF.dateFormatMy(value.last_day_of_school.value)}
              </div>
            ),
            className: "action-center"
          },
          {
            Header: "Video Special Need Date",
            accessor: "order_dates",
            Cell: ({ value }) => (
              <div>
                {HF.dateFormatMy(value.video_special_need_date.value)}
              </div>
            ),
            className: "action-center"
          },
          {
            Header: "Upload Cut Off Date",
            accessor: "order_dates",
            Cell: ({ value }) => (
              <div>
                {HF.dateFormatMy(value.upload_cut_off_date.value)}
              </div>
            ),
            className: "action-center"
          },
        ]
      }
    ];

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableResponsive
                content={
                  <div>
                    <Grid fluid>
                      <div className="secHeading">Orders Listing</div>
                      <div className="flexElem flexResponsive" style={{ "justifyContent": 'space-between' }}>
                        <div className="custom-search-input" >
                          <FormInputs
                            ncols={["col-md-12"]}
                            onChange={this.filterSearchHandle}
                            proprieties={[
                              {
                                type: "text",
                                bsClass: "form-control",
                                placeholder: "Search Orders",
                                onChange: this.filterSearchHandle,
                                name: "filter"
                              }
                            ]}
                          />
                        </div>
                      </div>
                    </Grid>
                    <Grid fluid>
                      <Row>
                        <Col md={12} className="mt-sm">
                          <ReactTable
                            noDataText='No order found'
                            data={ordersData}
                            pages={pages}
                            loading={loadingOrder}
                            columns={columns}
                            filtered={filtered}
                            defaultPageSize={10}
                            className="-striped listing"
                            pageData={this.ordersPaginationData}
                            manual
                            onFetchData={(state, instance) => {
                              var sort = state.sorted.length === 0 ? '' : state.sorted[0].id + ',desc:' + state.sorted[0].desc;
                              state.pageData(state.page + 1, state.filtered, sort, state.pageSize);
                            }}
                          />
                        </Col>
                      </Row>
                    </Grid>
                  </div>
                } />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  };
}

const mapDispatchToProps = dispatch => {
  return ({
    getOrdersPaginationCall: (page, filter, sort, pageSize, searchData) => { dispatch(orderActions.getOrdersPagination(page, filter, sort, pageSize, searchData)) },
    assignUserToOrderCall: (orderId, data) => { dispatch(orderActions.assignUserToOrder(orderId, data)) },
  });
};

function mapStateToProps(state) {
  const { loadingOrder, ordersData, pages, order_details } = state.ordersReducer;
  const { loading, userAdmin } = state.adminReducer;
  return {
    loadingOrder, loading,  ordersData, pages, userAdmin, order_details
  };
};

const OrderListingForModerator = connect(mapStateToProps, mapDispatchToProps)(OrderListingForModeratorPage);
export default OrderListingForModerator;