import React from 'react';
import { Step, Stepper, StepLabel, StepContent, FormStep, FormStepper } from 'react-form-stepper';
import { FormGroup, Label, Input, Row, Col, Button } from 'reactstrap';
export default class FormBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      formData: {
        firstName: '',
        lastName: '',
        email: '',
        password: ''
      }
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        [name]: value
      }
    }));
  };

  handleNextStep = () => {
    this.setState(prevState => ({
      step: prevState.step + 1
    }));
  };

  handlePreviousStep = () => {
    this.setState(prevState => ({
      step: prevState.step - 1
    }));
  };

  renderStep1() {
    const { formData } = this.state;

    return (
      <div className='container'>
        <Row className='mt-1'>
          <Col md={6}>
            <FormGroup className="position-relative">
              <Label for="fname">
                First Name
              </Label>
              <Input invalid name='fname' onChange={this.handleInputChange} />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup className="position-relative">
              <Label for="lname">
                Last Name
              </Label>
              <Input invalid name='lname' onChange={this.handleInputChange} />
            </FormGroup>
          </Col>
        </Row>
        <Row className='mt-1'>
          <Col md={6}>

          </Col>
          <Col md={6}>
            <Button color='primary' block onClick={this.handleNextStep}>Next</Button>
          </Col>
        </Row>
      </div>
    );
  }

  renderStep2() {
    const { formData } = this.state;

    return (
      <div className='container'>
        <Row className='mt-1'>
          <Col md={6}>
            <FormGroup className="position-relative">
              <Label for="company">
                Company
              </Label>
              <Input invalid name='company' onChange={this.handleInputChange} />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup className="position-relative">
              <Label for="Email">
                Email
              </Label>
              <Input type='email' invalid name='Email' onChange={this.handleInputChange} />
            </FormGroup>
          </Col>
        </Row>
        <Row className='mt-1'>
          <Col md={6}>
            <Button color='primary' block onClick={this.handlePreviousStep}>Previous</Button>
          </Col>
          <Col md={6}>
            <Button color='primary' block onClick={this.handleNextStep}>Next</Button>
          </Col>
        </Row>
      </div>
    );
  }

  renderStep3() {
    const { formData } = this.state;

    return (
      <div className='container'>
        <h3>Your summary will goes here...</h3>
      </div>
    );
  }

  handleSubmit = () => {
    console.log('Form submitted:', this.state.formData);
  };

  render() {
    const { step } = this.state;

    return (
      <div>
        {step === 1 && this.renderStep1()}
        {step === 2 && this.renderStep2()}
        {step === 3 && this.renderStep3()}
      </div>
    );
  }
}
