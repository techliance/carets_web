import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  // Tooltip,
  OverlayTrigger,
  // Button,
  Popover
} from "react-bootstrap";
import 'react-confirm-alert/src/react-confirm-alert.css';
import { Card } from "components/Card/Card.jsx";
import 'react-table/react-table.css';
import ReactTable from "react-table";
import { videoService } from '../../../services/video';
import { confirmAlert } from 'react-confirm-alert';
import Confirmalertcommon from '../../ThemeComponents/confirmAlertCommon';
import {RiVideoLine} from 'react-icons/ri';
import { helperFunctions as HF } from '../../../helper/helperFunctions';
const cloneDeep = require('lodash.clonedeep');

var Modal = require('react-bootstrap-modal');

class ReportsPage extends Component{
  constructor(props){
  	super(props);
  	this.state = {
        data  : this.props.reportData,
        selectedVideo  : this.props.selectedVideo,
        sorted   : '',
        filtered : '',
        pageSize : 10,
        video_url:'',
        openVideoModal: false,
      };
      console.log('state',this.state.selectedVideo)
    this.blockRecord = this.blockRecord.bind(this);
    this.deleteConfirmModal = this.deleteConfirmModal.bind(this);

    this.videoModal = this.videoModal.bind(this);
  };


  videoModal(url) {
    this.setState({video_url:url}, ()=>{
        this.setState({openVideoModal:true});
        console.log('url',this.state.video_url);
    });

  }

    blockRecord(recID)
    {
      videoService.blockRecord(recID).then(res => {
        this.props.closeModel();
      });
    }
    updateBlocked = (recID,rowID) => (e) => {
        // return false;
        videoService.blockRecord(recID).then(
            response => {
                let copy = cloneDeep(this.state.selectedVideo);
                copy.collages[rowID].videos = response.data;
                this.setState({selectedVideo:copy},()=>function(){
                    console.log('selectedVideo',this.state.selectedVideo);
                });
            }
        );
        }

    deleteConfirmModal(recID)
  {
      console.log('video',this.props.selectedVideo.id);
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <Confirmalertcommon
          heading="Block Caret"
          description="Are you sure to change?"
          onClosePro={onClose}
          deleteType={() => this.blockRecord(this.props.selectedVideo.id)} />
        )
      }
    })
  }

  render()
  {
    console.log('selectedVideo',this.state.selectedVideo);
    const columns = [
        {
          Header:"Listing",
          headerClassName:'headerPagination',
          columns: [
            {
              Header: "Remarks",
              accessor: "remarks",
            },
            {
                Header: "Reported By",
                accessor: "reportby",
                Cell: ({value}) => (
                  <div style={{ textAlign: "left" }}>
                    {
                      value.username
                    }
                  </div>
                  ),
                style:{"textAlign": "left"},
                sortable:false
              }
          ]
        }
    ];

    const videoColumns = [
        {
          Header:"Listing",
          headerClassName:'headerPagination',
          columns: [
            {
                Header: "Video",
                accessor: "videos",
                Cell: ({value}) => (
                    <div style={{cursor:'pointer',fontSize:'20px'}} onClick={()=>this.videoModal(value.video_url)}>
                        <RiVideoLine />
                    </div>
                ),

                className: "action-center"
            },

            {
                Header: "Details",
                accessor: "videos",
                Cell: ({value}) => (
                  <div>
                    {
                      value.video_description
                    }
                  </div>
                  ),
                className: "action-center"
            },
            {
                Header: "Blocked",
                accessor: "videos",
                Cell: ({value,row}) => (
                    <label className="container-check" eee={console.log("testing",row._index)}>
                      &nbsp;
                      <input
                        type="checkbox" name="is_blocked" className="checkbox" value={value.is_blocked? 1 : 0} onChange={this.updateBlocked(value.id,row._index)}  checked={value.is_blocked?1:0}
                       /><span className="checkmark"></span>
                    </label>
                  ),
                className: "action-center",
                sortable:false
            },


          ]
        }
    ];


  	return(
  		<div className="content">
        <Grid fluid>
          <Row>
          <Col md={12}>
            <div className="flexElem flexResponsive" style={{"alignItems": "flex-start", "justifyContent": "space-between"}}>
                <div className="">

                  {
                    this.state.selectedVideo.is_blocked == 0 ?
                    <button class={['backButton pt-sm no_radius pb-sm primary mt-none btn-block btn-info btn']} type='button' onClick={() => this.deleteConfirmModal(this.state.selectedVideo.id)}>Block Caret</button>
                    :
                    <button class={['backButton pt-sm no_radius pb-sm primary mt-none btn-block btn-info btn']} type='button' onClick={() => this.deleteConfirmModal(this.state.selectedVideo.id)}>Un-Block Caret</button>
                  }
                </div>


            </div>
            </Col>
            <Col md={12}></Col>
            {this.state.selectedVideo.collages.length > 0 &&
            <Col md={12}>
              <Card
                ctTableResponsive
                content={
                	<div>
                  	<Grid fluid>
                      <Row>

                          <Col md={12} className="mt-md">

                          <ReactTable
                                title="Videos"
                                columns={videoColumns}
                                data={this.state.selectedVideo.collages}
                                pageSize={5}
                            />

                          </Col>

                      </Row>
                    </Grid>
                	</div>
                } />
            </Col>
              }
            <Col md={12}>
              <Card
                ctTableResponsive
                content={
                	<div>
                  	<Grid fluid>
                      <Row>

                          <Col md={12} className="mt-md">

                          <ReactTable
                                title="Reports"
                                columns={columns}
                                data={this.state.data}
                                pageSize={5}
                            />

                          </Col>
                      </Row>
                    </Grid>
                	</div>
                } />
            </Col>
          </Row>
          {
            <Modal backdrop={'static'} show={this.state.openVideoModal} onHide={HF.closeModal.bind(this, "openVideoModal")} aria-labelledby="ModalHeader" >
            <Modal.Header closeButton>
            <Modal.Title id='ModalHeader' className="headerTitle">Video</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="row">
                <div className="col-md-12" style={{textAlignVertical: "center",textAlign: "center"}}>
                <video key={this.state.video_url} id="playVid" controls width="550" ><source src={this.state.video_url} type="video/mp4" /></video>
                </div>
            </div>
            </Modal.Body>
            </Modal>
        }

        </Grid>

      </div>
  	);
  };
}
const Reports = ReportsPage;
export default Reports;
