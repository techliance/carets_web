
import React, { Component } from "react";
import Select , { createFilter } from 'react-select';
import { helperFunctions as HF } from 'helper/helperFunctions';
import { objectsConstants as OC } from 'redux/constant/objects-constant';
import { connect } from 'react-redux';
import { settingsActions } from 'redux/actions/settings-actions';

class addressFormPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			adressObject: this.props.adressObject,
			states: []
		}

		this.handleChanegAddress = this.handleChanegAddress.bind(this);
		this.handleChangeAddressDropDown = this.handleChangeAddressDropDown.bind(this);
		this.returnAddressData = this.returnAddressData.bind(this);
		this.returnCountryName = this.returnCountryName.bind(this);
		if (this.props.adressObject.country_id)
			this.props.getStateCall(this.props.adressObject.country_id, this.props.type);
	}
	returnCountryName() {
		return HF.returnColumnValueWithColumnMatch( this.props.options.countries, "label", [ HF.returnLabelWithValue(this.state.adressObject.country_id) ], "label");
	}
	returnAddressData() {
		return { ...this.state.adressObject };
	}
	handleChanegAddress(e, parentObject) {
		const { name, value } = e.target;
		let updateAddress = { ...this.state.adressObject };
		updateAddress[name] = value;
		this.setState({ adressObject: updateAddress });
	}
	handleChangeAddressDropDown(selectedOptionType, info, parentObject) {
		if (parentObject === 'pre_trip_material_address' && (info.name === 'state' || info.name === 'country')) {
			if (selectedOptionType === null) {
				if ('getCarolinaTax' in this.props) {
					this.props.getCarolinaTax('tax');
				}
			}
		}
		
		//start chnage logic for north caroline will remove in future
		// if (info.name === 'state' && (selectedOptionType.label.toLowerCase() === 'north carolina')) {
		// 	if ('getCarolinaTax' in this.props) {
		// 		this.props.getCarolinaTax('tax', 'carolina');
		// 	}
		// }
		//end chnage logic for north caroline will remove in future
		
		if (info['action'] === 'select-option') {

			//start chnage logic for north caroline will remove in future
			// if (this.state.adressObject.state_name === 'north carolina') {
			// 	if ( this.props.updateNCTaxCheckBox ) {
			// 		this.props.updateNCTaxCheckBox(1, this.props.type);	
			// 	}
			// 	if ('getCarolinaTax' in this.props) {
			// 		this.props.getCarolinaTax('tax');
					
			// 	}
			// }
			//end chnage logic for north caroline will remove in future

			let shippingDetail = { ...this.state.adressObject };
			shippingDetail[info['name'] + '_id'] = selectedOptionType['value'];
			shippingDetail['state_name'] = selectedOptionType.label.toLowerCase();
			if (info['name'] === 'country')
			this.props.getStateCall(selectedOptionType['value'], this.props.type);
			this.setState({ adressObject: shippingDetail }, function(){
				if ( this.props.updateZipReq ) {
					if (this.state.adressObject.country_id) {
						var tempCountry = HF.returnLabelWithValue(this.state.adressObject.country_id, this.props.options.countries);
						if ( tempCountry ) {
							if ( OC.COUNTRIESNOTREQZIP.includes( tempCountry.label ) ) {
								this.props.updateZipReq(false, this.props.type);
							} else {
								this.props.updateZipReq(true, this.props.type);
							}
						}
					}
				}

				//start chnage logic for north caroline will remove in future
				// if ( this.props.updateNCTaxCheckBox && this.state.adressObject.state_name === 'north carolina') {
				// 	this.props.updateNCTaxCheckBox(1, this.props.type);	
				// } else if (this.props.updateNCTaxCheckBox && this.state.adressObject.state_name !== 'north carolina') {
				// 	this.props.updateNCTaxCheckBox(0, this.props.type);	
				// }
				//end chnage logic for north caroline will remove in future

			});
		}
		else if (info['action'] === 'clear') {
			let shippingDetailClear = { ...this.state.adressObject };
			shippingDetailClear['country_id'] = null;
			shippingDetailClear['state_id'] = null;
			this.setState({ adressObject: shippingDetailClear, states: [] });
			this.props.clearState(this.props.type);
		}
	}
	componentDidUpdate(prevProps) {
		if (prevProps.options.countries !== this.props.options.countries) {
			if (this.props.defaultCountry) {
				this.setState({
					adressObject: { ...this.props.adressObject, country_id: HF.returnColumnValueWithColumnMatch(this.props.options.countries, 'value',  [ OC.DEFAULTCOUNTRY ], 'label') }
				}, function () {
					if (this.state.adressObject.country_id)
						this.props.getStateCall(this.state.adressObject.country_id, this.props.type);
				});
			}
		}
		if (prevProps.states !== this.props.states && (this.props.type === this.props.statesType)) {
			this.setState({ states: this.props.states });
		}
		if (prevProps.adressObject !== this.props.adressObject) {
			this.setState({ adressObject: this.props.adressObject }, function () {
				if ((prevProps.adressObject.country_id !== this.props.adressObject.country_id) && this.props.adressObject.country_id) {
					this.props.getStateCall(this.props.adressObject.country_id, this.props.type);
				}
			})
		}
		var textReplace = document.querySelectorAll(".customMsg .srv-validation-message");
		if (textReplace.length) {
			for (let i = 0; i < textReplace.length; i++) {
				let str = textReplace[i].innerHTML;
				let newStr = str.replace(this.props.replaceText, "");
				textReplace[i].innerHTML = newStr;
			}
		}
		
		// if ( this.state.adressObject.state_name  )
	}
	componentDidMount() {
		this.props.provideCtrl({
			returnAddressData: () => this.returnAddressData()
		});
	}
	componentWillUnmount() {
		this.props.provideCtrl(null);
	}
	render() {
		// this.props.validatorInstance.purgeFields();
		const { options, type } = this.props;
		const { adressObject, states } = this.state;
		const filterConfig = {
	      ignoreCase : true,
	      matchFrom  : 'start',
	    };

		return (
			<div className='clearfix'>
				<span></span>
				<div className="col-md-6" id={this.props.htmlElement + "Country"}>
					<div className="form-group">
						<label htmlFor="country_id">Country
							{
								this.props.requiredField.country_id && this.props.requiredField.country_id.includes("required") &&
								<span className="requiredClass">*</span>
							}
						</label>
						<Select
							value={HF.returnLabelWithValue(adressObject.country_id, options.countries)}
							onChange={(event, info) => this.handleChangeAddressDropDown(event, info, type)}
							options={options.countries}
							isSearchable={true}
							isMulti={false}
							name='country'
							isClearable={this.props.isClearableCountryPro}
							placeholder={'Select Country'}
							filterOption={createFilter(filterConfig)}
						/>

						{
							this.props.requiredField.country_id &&
							<span className="customMsg">
								{this.props.validatorInstance.message(this.props.htmlElement + 'Country', adressObject.country_id, this.props.requiredField.country_id)}
							</span>
						}
					</div>
				</div>
				<div className="col-md-6" id={this.props.htmlElement + "Street"}>
					<div className='form-group'>
						<label htmlFor="proshow_file_company_name">Street
							{
								this.props.requiredField.street_address_1 && this.props.requiredField.street_address_1.includes("required") &&
								<span className="requiredClass">*</span>
							}
						</label>
						<input type="text" name="street_address_1" className="form-control" value={adressObject.street_address_1 ? adressObject.street_address_1 : ''} onChange={(event) => this.handleChanegAddress(event, type)} />
						{
							this.props.requiredField.street_address_1 &&
							<span className="customMsg">
								{this.props.validatorInstance.message(this.props.htmlElement + 'Street', adressObject.street_address_1, this.props.requiredField.street_address_1)}
							</span>
						}
					</div>
				</div>
				<div className="col-md-12" id={this.props.htmlElement + "City"}>
					<div className='form-group'>

						<div className="row">

							<div className="col-md-4" ref={this.props.htmlElement + "City"}>
								<label htmlFor="proshow_file_company_name">City
						        	{
										this.props.requiredField.city && this.props.requiredField.city.includes("required") &&
										<span className="requiredClass">*</span>
									}
								</label>
								<input type="text" name="city" className="form-control" value={adressObject.city ? adressObject.city : ''} onChange={(event) => this.handleChanegAddress(event, type)} />
								{
									this.props.requiredField.city &&
									<span className="customMsg">
										{this.props.validatorInstance.message(this.props.htmlElement + 'City', adressObject.city, this.props.requiredField.city)}
									</span>
								}
							</div>
							<div className="col-md-4" id={this.props.htmlElement + "State"}>
								<label htmlFor="proshow_file_company_name">State
						        	{
										this.props.requiredField.state_id && this.props.requiredField.state_id.includes("required") &&
										<span className="requiredClass">*</span>
									}
								</label>
								<Select
									value={HF.returnLabelWithValue(adressObject.state_id, states)}
									onChange={(event, info) => this.handleChangeAddressDropDown(event, info, type)}
									options={states}
									filterOption={createFilter(filterConfig)}
									isSearchable={true}
									isMulti={false}
									name='state'
									placeholder={'Select State'} 
								/>
								{
									this.props.requiredField.state_id &&
									<span className="customMsg">
										{this.props.validatorInstance.message(this.props.htmlElement + 'State', adressObject.state_id, this.props.requiredField.state_id)}
									</span>
								}
							</div>
							<div className="col-md-4">
								<label htmlFor="proshow_file_company_name">Zip
						        	{
										( (this.props.requiredField.zipcode && this.props.requiredField.zipcode.includes("required")) || ( HF.returnColumnValueWithColumnMatch(this.props.options.countries, 'label', [adressObject.country_id], 'value') === OC.DEFAULTCOUNTRY ) ) &&
										<span className="requiredClass">*</span>
									}
								</label>
								<input type="text" id={this.props.htmlElement + "Zip"} name="zipcode" className="form-control" value={adressObject.zipcode ? adressObject.zipcode : ''} onChange={(event) => this.handleChanegAddress(event, type)} />
								{
									this.props.requiredField.zipcode &&

									<span className="customMsg">
										{
											HF.returnColumnValueWithColumnMatch(this.props.options.countries, 'label', [adressObject.country_id], 'value') === OC.DEFAULTCOUNTRY
											?
												this.props.validatorInstance.message(this.props.htmlElement + 'Zip', adressObject.zipcode, ['required', 'integer'])
											:
												this.props.validatorInstance.message(this.props.htmlElement + 'Zip', adressObject.zipcode, this.props.requiredField.zipcode)
										}
									</span>
								}
							</div>
							<span></span>
						</div>
					</div>
				</div>
			</div>
		);
	};
}

const mapDispatchToProps = dispatch => {
	return ({
		getStateCall: (countryId, type) => { dispatch(settingsActions.getStates(countryId, type)) },
	});
};
function mapStateToProps(state) {
	const { loadingSettings, states, statesType } = state.settingsReducer;
	return {
		loadingSettings, states, statesType
	};
}

const addressForm = connect(mapStateToProps, mapDispatchToProps)(addressFormPage);


export default addressForm;
