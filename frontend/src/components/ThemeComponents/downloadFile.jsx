import React, { Component } from "react";
// import {
// 	// OverlayTrigger,
// 	// Popover,
// } from "react-bootstrap";
// import { helperFunctions as HF } from '../../helper/helperFunctions';
import { helperFunctions as HF } from "helper/helperFunctions";
import { toast } from 'react-toastify';
var FileSaver = require('file-saver');
var JSZip = require('jszip');
var JSZipUtils = require('jszip-utils');


class downloadFile extends Component {
	constructor(props) {
		super(props);
		this.downloadFile = this.downloadFile.bind(this);
		this.makezip = this.makezip.bind(this);
	}
	urlToPromise(url) {
		return new Promise(function (resolve, reject) {
			JSZipUtils.getBinaryContent(url, function (err, data) {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		});
	}
	downloadFile() {
		const { files, multiple } = this.props;
		let toastIdImg = null;
		toastIdImg = toast('Downloading in progressing', {
            type: toast.TYPE.INFO,
            progress: 0,
            position: "bottom-right",
        });
		if (multiple) {
			var zip = new JSZip();
			zip = this.makezip(files, zip);
			zip.generateAsync({ type: "blob" }).then(function (content) {
				FileSaver.saveAs(content, 'All.zip');
				toast.update(toastIdImg, {
                    // render: 'Downloading in progress '+ percentage +'%',
                    render: 'Downloading completed',
                    progress: 1,
                    type: toast.TYPE.INFO,

                    position: "bottom-right",
                    hideProgressBar:false,
                });
				// toast.dismiss(toastIdImg)
			});
		} else {
			FileSaver.saveAs(files['file_path'], files['file_title']);
			toast.update(toastIdImg, {
                render: 'Downloading completed',
                progress: 1,
                type: toast.TYPE.INFO,
                position: "bottom-right",
            });
			toast.dismiss(toastIdImg)	 
		}
	}
	makezip(files, zip, folderName = null ){
		if (folderName)
			var tempFolder = zip.folder(folderName);
		for (var i in files) {
			files[i].hasOwnProperty('file_title')
			if ( !Array.isArray(files[i])  ) {
				if ( typeof files[i]['file_title'] !== 'undefined' && typeof files[i]['file_path'] !== 'undefined') {
					if (!folderName)
						zip.file(files[i]['file_title']+'.'+HF.returnExtFromImage(files[i]['file_path']), this.urlToPromise(files[i]['file_path']), { base64: true, binary: true } );
					else
						tempFolder.file(files[i]['file_title']+'.'+HF.returnExtFromImage(files[i]['file_path']), this.urlToPromise(files[i]['file_path']), { base64: true, binary: true } );
				}
				
			} else if ( Array.isArray(files[i]) ){
				zip = this.makezip(files[i], zip,  i);
			}
			
		}
		return zip;
	}

	render() {
		const { title, extraClass} = this.props;
		return (
			<React.Fragment>
				<button className={"orderDetailsAction downloadIcon "+extraClass} onClick={this.downloadFile}>{title}</button>
				
			</React.Fragment>
		);
	}
}

export default downloadFile;