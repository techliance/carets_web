import React, { Component } from 'react'
import { agencyActions } from '../../redux/actions/agency-actions';
import { connect } from 'react-redux'
import User from '../../helper/User';
import placeholder from "../../assets/img/placeholder.png";

class AgencyLogo extends Component {
    constructor(props) {
        super(props);
        this.props.getAgencyLogo(User.agency('id'));
    }
    render() {
        return (
            <React.Fragment>
                {
                    this.props.agencyLogo ?
                        <img className={this.props.className} src={this.props.agencyLogo.file_path} alt="Company Name" />
                        :
                        <img className={this.props.className} src={placeholder} alt="Company Name" />
                }
            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => {
    const { agencyLogo } = state.frontEndUserReducer;
    return {
        agencyLogo
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAgencyLogo: (agencyId) => { dispatch(agencyActions.getAgencyLogo(agencyId)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AgencyLogo);