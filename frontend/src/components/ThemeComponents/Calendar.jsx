import React from "react";
import ReactCalendar from "react-calendar";
import { OverlayTrigger, Popover } from "react-bootstrap";
import { connect } from "react-redux";
import { holidayActions } from "../../redux/actions/holiday-actions";

class Calendar extends React.Component {

    constructor(props) {
        super(props);
        this.getHolidays = this.getHolidays.bind(this);
        this.props.getHolidaysCall(1);
    }

    getHolidays(date, func) {
        const holidays = [...this.props.holidays];
        let status;
        holidays.forEach(holiday => {
            if (date.getDate() === holiday.day && date.getMonth() + 1 === holiday.month) {
                if (func === 'disabled') {
                    status = true;
                }
                if (func === 'content') {
                    status = (<OverlayTrigger placement="bottom" overlay={<Popover id='tooltip' >{holiday.description}</Popover>}>
                        <p style={{ color: 'red', fontSize: '10px', marginBottom: '0px' }} >Holiday</p>
                    </OverlayTrigger>);
                }
                this.count = this.count + 1;
            }
        });
        return status;
    }

    render() {
        return (
            <ReactCalendar
                calendarType='US'
                view='month'
                showNeighboringMonth={false}
                tileDisabled={(this.props.holidays.length >= 1) ? ({ date }) => this.getHolidays(date, 'disabled') : null}
                tileContent={(this.props.holidays.length >= 1) ? ({ date }) => this.getHolidays(date, 'content') : null}
                className='ml-auto'
                value={new Date()} />
        );
    }
}

const mapStateToProps = state => {
    const { holidays } = state.holidayReducer
    return { holidays }
}

const mapDispatchToProps = dispatch => {
    return {
        getHolidaysCall: (id) => { dispatch(holidayActions.getHolidays(id)) }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
