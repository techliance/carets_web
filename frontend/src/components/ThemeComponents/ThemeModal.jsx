import React from "react";
import { Modal } from "react-bootstrap";
class ThemeModal extends React.Component {
    render() {
        return (
            <Modal backdrop={'static'}className={this.props.modalClass} bsSize={this.props.size} show={this.props.show} onHide={this.props.hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.props.title}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.props.children}
                </Modal.Body>
            </Modal>
        );
    }
}
export default ThemeModal;