import React from 'react';
import DropzoneComponent from 'react-dropzone-component';
import * as Cookies from "js-cookie";
import FileReader from "filereader";
export default class InputDropzoneComponent extends React.Component {
    constructor(props) {
      super(props);
        let headers = {
              'X-CSRFToken': Cookies.get('csrf_token'),
        };
        this.config = {
            iconFiletypes: ['.data'],
            showFiletypeIcon: true,
            postUrl: '/upload',
        };
        this.djsConfig = {
            headers:headers,
            autoProcessQueue: true,
            autoQueue: true,
            previewTemplate: ReactDOMServer.renderToStaticMarkup(
            <div className="col-3 dz-preview dz-file-preview">
              <div className="dz-details">
                <img className="dz-preview-image" data-dz-thumbnail="true" />
              </div>
            </div>
          ),
        };
        this.eventHandlers = {
            addedfile: (file) => {
                // THIS GETS CALLED PROPERLY but the provided does
                // not seem to be loadable by FileReader :-(
                const reader = new FileReader();
                reader.onloadend = function (e) {
                    console.log(e.target.result);
                };
                reader.onerror = function (e) {
                    console.log(e.target.error);
                };
                // THIS IS WHERE IT FAILS, "file" object obviously
                // misses "path" attribute for some reason and reader 
                // cannot open it, do you have any idea how/where to 
                // access local path or how to open and modify the file?
                reader.readAsArrayBuffer(file);
            },
        };
    };
    render() {
        return (
            <div className="dz-wrapper">
                <DropzoneComponent config={this.config}
                           eventHandlers={this.eventHandlers}
                           djsConfig={this.djsConfig}
                          className="row">
                </DropzoneComponent>
            </div>
        );
     }
};