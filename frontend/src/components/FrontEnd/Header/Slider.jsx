import React, { Component } from 'react';
import appStore from '../../../assets/img/appStore.png';
import playStore from '../../../assets/img/playStore.png';

class MainSlider extends Component{

  render(){

    return(
        <section id="main_slider" className="slider_main flexElem respFlex">
          <div className="container">
            <div className="col-md-6">
              <div className="banner_text_big">Storytelling Video</div>
              
              <div className="banner_text_small">
                Made From Tour Travel Pics!
              </div>

              <div className="downloadBtns flexElem respFlex">
                <a href="#!"><img src={appStore} alt="Download from App Store"/></a>
                <a href="#!"><img src={playStore} alt="Download from Play Store"/></a>
              </div>
              <p>
                Download Free App
              </p>
            </div>
        
            
          </div>
          <div className="clearfix"></div>
        </section>
    );
  }
}


export default MainSlider;