import React, { Component } from 'react';
import FrontEndRoutes from "routes/FrontEndRoutes.jsx";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

class MenuPage extends Component{

	constructor(props){
		super(props);
    this.state = {
      routes : ''
    };
	};
  sortByColumn(a, colIndex){
      a.sort(sortFunction);
      function sortFunction(a, b) {
          if (a[colIndex] === b[colIndex]) {
              return 0;
          }
          else {
              return (a[colIndex] < b[colIndex]) ? -1 : 1;
          }
      }
      return a;
  }
  render(){
    const {loggedIn} = this.props;
  	return(
    	<div>
      	<nav className="navbar navbar-default">
          <div className="container-fluid">
            <ul className="nav navbar-nav">
              {
                FrontEndRoutes.map((prop, key) => {
                  if (!prop.redirect && prop.showMenu){
                    // console.log('prop.name', prop.path)
                    if (loggedIn && prop.path === '/Adjudicator_Comments'){
                      return (
                        ''
                      ); 
                    } else if( !loggedIn && prop.path === '/Dashboard' ){
                      // console.log('prop.name else if', prop.path)
                      return (
                        ''
                      ); 
                    }
                    else {
                      return (
                        <li key={key}>
                          <Link to={prop.path} >
                            <p>{prop.name}</p>
                          </Link>
                        </li>
                      );
                    }
                  }  
                  return null;
                })
              }
              {
                loggedIn &&
                <li className="pull-right"><a href="/Adjudicator_Comments">Logout</a></li>
              }
              
            </ul>
          </div>
        </nav>
     	</div>
  	);
  }
}


function mapStateToProps(state) {
    const { loading, frontEndUser, loggedIn } = state.frontEndUserReducer;
    // console.log('loggedIn menu', loggedIn);
    return {
        loading, frontEndUser, loggedIn
    };
}

const Menu = connect(mapStateToProps)(MenuPage);
export default Menu;