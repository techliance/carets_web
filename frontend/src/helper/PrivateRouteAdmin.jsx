import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const AdminAuthRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props, matchProps) => (
        localStorage.getItem('user') 
        ? <Component {...props} {...matchProps} /> 
        : <Redirect to={{ pathname: '/Admin-login', state: { from: props.location } }} />
    )} />
)
