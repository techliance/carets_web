import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import User from './User';

export const PrivateFrontEndRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props, matchProps) => (
            User.isAuthenticated() !== undefined
                ? <Component {...props} {...matchProps} />
                : <Redirect to={{ pathname: '/Admin-login', state: { from: props.location } }} />
        )} />
    )
}