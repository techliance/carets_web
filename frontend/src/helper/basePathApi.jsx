
export const basePathApi = process.env.NODE_ENV === 'development' ? 'https://api.carets.tv/' : 'https://api.carets.tv/'
//export const basePathApi = process.env.NODE_ENV === 'development' ? 'https://devapi.carets.tv/' : 'https://devapi.carets.tv/'

//export const basePathApi = process.env.NODE_ENV === 'development' ? 'http://carets_web.test/' : 'https://devapi.carets.tv/'
// export const basePathApi = process.env.NODE_ENV === 'development' ? 'http://carets_web.test/' : 'https://api.carets.tv/'
