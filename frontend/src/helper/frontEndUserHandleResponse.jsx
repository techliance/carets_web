import { frontEndUserService } from '../services/frontEndUser';

export function frontEndUserHandleResponse(response) {
    return response.json().then(data => {

        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                frontEndUserService.logout();

                window.location.reload(true);
            }
            const error = (data && data.error) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}