import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const CLMAuthRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props, matchProps) => (
        localStorage.getItem('user')
        ? <Component {...props} {...matchProps} />
        : <Redirect to={{ pathname: '/CLM-login', state: { from: props.location } }} />
    )} />
)
