export function handleResponse(response) {
    return response.json().then(data => {

        if (!response.ok) {
            if (response.status === 401) {
                // localStorage.removeItem('user');
                // window.location.reload(true);
            }
            const error = (data && data.error) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}