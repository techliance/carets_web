import { SC } from '../helper/ServerCall';

export const paymentService = {
    paginationData,
    storePayment,
    getPayment,
    editPayment,
    deletePayment,
    updatePaymentStatus,
    getDuration,
    blockRecord,
    
}
function paginationData(user_id, page, filter, sort, pageSize,status,search=null) {
    const data = {'page': page,'filter': filter,'sort': sort,'pageSize': pageSize,'status': status,'search':search};
    return SC.getCall('CLMPayments/data/'+user_id+'?page=' + page + '&filter=' + filter + '&sort=' + sort + '&pageSize=' + pageSize);
}
function getDuration() {
    return SC.getCall('CLMPayments/durations');
}
function storePayment(catData) {
    return SC.postCall('CLMPayments/listing', catData);
}

function getPayment(id) {

    return SC.getCall('CLMPayments/listing/' + id);
}

function editPayment(data, PaymentId) {
    data['payment_id'] = PaymentId;
    return SC.postCall('CLMPayments/listing', data);
}

function deletePayment(catId) {
    return SC.deleteCall('CLMPayments/listing/' + catId);
}

function updatePaymentStatus(catId, data) {
    data['payment_id'] = catId;
    return SC.postCall('CLMPayments/listing/status',data);
}
function blockRecord(catId) {
    return SC.getCall('CLMPayments/block/status/'+catId);
}
