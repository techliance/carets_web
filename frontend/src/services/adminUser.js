import { SC } from 'helper/ServerCall';

export const adminUserService = {
    login,
    logout,
    dashboard,
    dashboardMenu,
}
function login(email, password) {
    return SC.postCallLoginAdmin('auth/login', { email, password });
}

function logout() {
    return SC.getCall('logout');
}

function dashboard() {
    return SC.getCall('dashboard');
}

function dashboardMenu() {
    return SC.getCall('getMenu');
}