import { SC } from "../helper/ServerCall";

export const resumeService = {
    getEmployees,
    storeEmployee,
    getEmployeeData,
    editEmployee,
    deleteEmployee,
};

function getEmployees(page, filter, sort, pageSize) {
  return SC.getCall('employees?page=' + page + '&filter=' + filter + '&sort=' + sort + '&pageSize=' + pageSize);
}

function storeEmployee(employeeData) {
    console.log('Testing');
    return SC.postCall('employees', employeeData);
}
function getEmployeeData(employeeId) {

    return SC.getCall('employees/' + employeeId);
}

function editEmployee(employeeData, employeeId) {
    return SC.putCall('employees/' + employeeId, employeeData);
}

function deleteEmployee(employeeId) {
    return SC.deleteCall('employees/' + employeeId);
}
