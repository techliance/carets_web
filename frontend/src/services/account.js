import { SC } from "../helper/ServerCall";

export const accountService = {
    getBrands,
    getBrandingInfoById,
    getBrandingInfoBySlug
}

function getBrands() {
    return SC.getCall(`getBrands`);
}

function getBrandingInfoById(id) {
    return SC.getCall(`getBrandingInfo/${id}`);
}

function getBrandingInfoBySlug(slug) {
    return SC.getCall(`getBrandingInfoBySlug/${slug}`);
}