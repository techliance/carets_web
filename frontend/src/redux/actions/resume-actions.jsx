import { adminConstants } from '../constant/admin-constant';
import { alertActions } from './alert-actions';
import { resumeService } from '../../services/resume';

export const resumeActions = {
    getEmployees,
    storeEmployee,
    getEmployee,
    editEmployee,
    deleteEmployee,
};


function getEmployees(page, filter, sort, pageSize){
    return dispatch => {
        dispatch(request())
        resumeService.getEmployees(page, filter, sort, pageSize).then(
            response => {
                console.log('response', response);
                dispatch(success(response))
            }).catch(error => {
                dispatch(failure(error))
            })
    }
    function request() { return { type: adminConstants.GET_RESUME_EMPLOYEES_REQUEST } }
    function success(response) { return { type: adminConstants.GET_RESUME_EMPLOYEES_SUCCESS, payload: response } }
    function failure(error) { return { type: adminConstants.GET_RESUME_EMPLOYEES_FAILURE, payload: error } }
}

function storeEmployee(UserData){
    return dispatch => {
        dispatch(request());
        resumeService.storeEmployee(UserData)
        .then(
            response => {
                // console.log('response 123', response);
                dispatch(success(response));
                dispatch(alertActions.success(response.message));
                // setTimeout(function(){ 
                //     history.go(0);
                // }, 1500);
            },
            error => {
                // console.log('error 123', error);
                dispatch(failure(error));
                dispatch(alertActions.error(error));
            }
        );
    }

    function request() { return { type: adminConstants.STORE_RESUME_EMPLOYEES_REQUEST } }
    function success(response)   { return { type: adminConstants.STORE_RESUME_EMPLOYEES_SUCCESS, payload:response } }
    function failure(error) { return { type: adminConstants.STORE_RESUME_EMPLOYEES_FAILURE, payload:error } }
}


function getEmployee(id){
    return dispatch => {
        dispatch(request());
        resumeService.getEmployeeData(id)
        .then(
            Employee => {
                dispatch(success(Employee));
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error));
            }
        );
    }

    function request() { return { type: adminConstants.GET_RESUME_EMPLOYEE_REQUEST } }
    function success(User)   { return { type: adminConstants.GET_RESUME_EMPLOYEE_SUCCESS, payload:User } }
    function failure(error) { return { type: adminConstants.GET_RESUME_EMPLOYEE_FAILURE, payload:error } }
}

function editEmployee(UserData, userId){

    return dispatch => {
        dispatch(request());
        resumeService.editEmployee(UserData, userId)
        .then(
            response => {
                dispatch(success(response));
                dispatch(alertActions.success(response.message));
                console.log('edit');
                // setTimeout(function(){ 
                //     history.go(0);
                // }, 1500);
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error));
            }
        );
    }

    function request() { return { type: adminConstants.EDIT_RESUME_EMPLOYEE_REQUEST } }
    function success(response)   { return { type: adminConstants.EDIT_RESUME_EMPLOYEE_SUCCESS, payload:response } }
    function failure(error) { return { type: adminConstants.EDIT_RESUME_EMPLOYEE_FAILURE, payload:error } }
}


function deleteEmployee(userId){
    return dispatch => {
        dispatch(request());
        resumeService.deleteEmployee(userId)
        .then(
            response => {
                dispatch(success(response));
                dispatch(alertActions.success(response.message));
                // setTimeout(function(){ 
                //     history.go(0);
                // }, 1500);
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error));
            }
        );
    }

    function request() { return { type: adminConstants.DELETE_RESUME_EMPLOYEE_REQUEST } }
    function success(response)   { return { type: adminConstants.DELETE_RESUME_EMPLOYEE_SUCCESS, payload:response } }
    function failure(error) { return { type: adminConstants.DELETE_RESUME_EMPLOYEE_FAILURE, payload:error } }
}


