import { accountService } from "../../services/account";
// import { alertActions } from "./alert-actions";
import { adminConstants } from "../constant/admin-constant";
import { generalActions } from "./generalActions";
import { frontEndUserConstants } from "../constant/front-end-constant";

export const accountActions = {
    getBrands,
    getBrandingInfoById,
    getBrandingInfoBySlug
}

function getBrands() {
    return dispatch => {
        accountService.getBrands().then(
            response => {
                dispatch(success(response))
            },
            error => {
                dispatch(failure(error))
            }
        )
    }
    function success(response) { return { type: adminConstants.GET_ALL_BRANDS_SUCCESS, payload: response } }
    function failure(error) { return { type: adminConstants.GET_ALL_BRANDS_FAILURE, payload: error } }
}

function getBrandingInfoById(id) {
    return dispatch => {
        accountService.getBrandingInfoById(id).then(
            res => {

            },
            err => {

            }
        )
    }
}

function getBrandingInfoBySlug(slug) {
    return dispatch => {
        accountService.getBrandingInfoBySlug(slug).then(
            res => {
                dispatch(generalActions.success(frontEndUserConstants.GET_BRANDING_INFO_SUCCESS, res))
            },
            err => {

            }
        )
    }
}