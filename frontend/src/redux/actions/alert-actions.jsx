import { alertConstants } from '../constant/alert-constant';
import { alertService } from "../../services/alert";

export const alertActions = {
    success,
    error,
    clear,
    errorNotify,
};

function success(message, notiType='default') {
    return { type: alertConstants.SUCCESS, message, notiType };
}

function error(message, notiType='default') {
    return { type: alertConstants.ERROR, message, notiType };
}

function clear() {
    return { type: alertConstants.CLEAR };
}

function errorNotify(error) {
	// console.log('error', error);
	return dispatch => {
        alertService.errorNotify(error).then(
            response => {
                // dispatch(success(response))
            },
            error => {
                // dispatch(failure(error))
            }
        )
    }
}