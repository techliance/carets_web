import { advertiserConstants } from '../constant/advertiser-constant';

let userAdmin = JSON.parse(localStorage.getItem('user'));
const initialState = userAdmin ? { loggedIn:true, userAdmin:userAdmin, loading:false, dashboardCount:'', menu:{'parent_labels':{}, 'permissions':{}}} : { loggedIn:false, userAdmin:'', loading:false };

export function advertiserReducer(state = initialState, action){
	// console.log('type', action.type);
	switch(action.type){
		case advertiserConstants.LOGIN_ADVERTISER_REQUEST:
			return { ...state, loading:true };
		case advertiserConstants.LOGIN_ADVERTISER_SUCCESS:
			return { ...state, loading:false, userAdmin : action.payload, loggedIn:true };
		case advertiserConstants.LOGIN_ADVERTISER_FAILURE:
			return { ...state, loading:false };

		case advertiserConstants.GET_DASHBOARD_REQUEST:
			return { ...state, loading:true };
		case advertiserConstants.GET_DASHBOARD_SUCCESS:
			return { ...state, loading:false, dashboardCount : action.payload.dashboardCount };
		case advertiserConstants.GET_DASHBOARD_FAILURE:
			return { ...state, loading:false, error : action.payload };


		case advertiserConstants.GET_MENU_DASHBOARD_REQUEST:
			return { ...state, loading:true };
		case advertiserConstants.GET_MENU_DASHBOARD_SUCCESS:
			return { ...state, loading:false, menu : action.payload.menu };
		case advertiserConstants.GET_MENU_DASHBOARD_FAILURE:
			return { ...state, loading:false, error : action.payload };

		case advertiserConstants.LOGOUT_ADVERTISER_REQUEST:
			return {...state};
		case advertiserConstants.LOGOUT_ADVERTISER_SUCCESS:
			return {...state};
		case advertiserConstants.LOGOUT_ADVERTISER_FAILURE:
			return {...state};

		default:

			return {...state};
	}
}
