import { frontEndUserConstants } from "../constant/front-end-constant";
import User from "../../helper/User";

const initialState = {
  User: User,
  orderUploadInfo: {
    max_allowed_photos: {},
    agency: {},
    photos_submitted_for_video_production: {},
    disable_traveler_for_upload: {},
    status: {},
    destination: {},
    preShippingContact: 0,
    approved_for_upload: {}
  },
  singleImage: {
    image_versions: {},
    image_format: {}
  },
  marketingFlyers: {
    traveler_flyer_version1: {},
    traveler_flyer_version2: {},
    before_departure_flyer: {}
  },
  progress: 0,
  shippingSettings: {}
};
export function frontEndUserReducer(state = initialState, action) {
  switch (action.type) {
    case frontEndUserConstants.LOGIN_FRONT_END_USER_REQUEST:
      return { ...state, loading: true };
    case frontEndUserConstants.LOGIN_FRONT_END_USER_SUCCESS:
      return {
        ...state,
        User: {
          ...action.payload,
          isAuthenticated: () => true
        }
      };
    case frontEndUserConstants.LOGIN_FRONT_END_USER_FAILURE:
      return { ...state, loading: false };

    case frontEndUserConstants.FORGET_FRONT_END_USER_REQUEST:
      return { ...state, loading: true };
    case frontEndUserConstants.FORGET_FRONT_END_USER_SUCCESS:
      return { ...state, loading: false };
    case frontEndUserConstants.FORGET_FRONT_END_USER_FAILURE:
      return { loading: false };
    case frontEndUserConstants.LOGOUT_FRONT_END_USER:
      return { ...state };
    case frontEndUserConstants.GET_ORDER_UPLOAD_INFO_SUCCESS:
      return {
        ...state,
        orderUploadInfo: { ...action.payload.data, ...action.payload.data.settings }
      };
    case frontEndUserConstants.GET_SINGLE_IMAGE_SUCCESS:
      return {
        ...state,
        singleImage: action.payload.data
      };
    case frontEndUserConstants.GET_AGENCY_LOGO_SUCCESS:
      return {
        ...state,
        agencyLogo: action.payload.data
      }
    case frontEndUserConstants.GET_AGENCY_LOGO_FAILURE:
      return {
        ...state,
        error: action.payload
      }
    case frontEndUserConstants.GET_AGENCY_RESOURCES_BY_ROLE_SUCCESS:
      return {
        ...state,
        marketingFlyers: action.payload.data
      }
    case frontEndUserConstants.GET_AGENCY_RESOURCES_BY_ROLE_FAILURE:
      return {
        ...state,
        error: action.payload
      }
    case frontEndUserConstants.SHOW_PROGRESS:
      return {
        ...state,
        progress: action.payload
      }
    case frontEndUserConstants.RESET_PROGRESS:
      return {
        ...state,
        progress: 0
      }
    case frontEndUserConstants.GET_SHIPPING_SETTINGS_SUCCESS:
      return {
        ...state,
        shippingSettings: action.payload.settings
      }
    case frontEndUserConstants.GET_SHIPPING_SETTINGS_FAILURE:
      return {
        ...state,
        error: action.payload
      }
    default:
      return { ...state };
  }
}
