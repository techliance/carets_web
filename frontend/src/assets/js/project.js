$(document).ready(function($) {


	$("[data-scroll]").click(function(e) {
		e.preventDefault();

		var scroll_elem = "#"+$(this).attr('data-scroll');

		var scroll_val = $(scroll_elem).offset().top;

		$("html, body").animate({scrollTop: scroll_val},500);
	});
});
