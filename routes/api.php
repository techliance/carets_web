<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SplashController;
use App\Http\Controllers\SplashPlanController;
use App\Http\Controllers\HashController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post("/downloadVideo", "VideoController@downloadVideo");
Route::post("/uploadtest", "VideoController@uploadtest");

Route::prefix('v1')->group(function () {

    Route::post('/password/send-otp', "PasswordResetController@sendOtp");
    Route::post('/password/verify-otp', "PasswordResetController@verifyOtp");
    Route::post('/password/reset', "PasswordResetController@resetPassword");

    Route::post("/processHLSVideos", "VideoController@processHLSVideos");
    Route::get('/cart/list', 'CartController@listCart');
    Route::post('/cart/add', 'CartController@addToCart');
    Route::post('/cart/meetingTime', 'CartController@meetingTime');
    Route::put('/cart/item/{itemId}', 'CartController@editSubscriptionPlan');
    Route::delete('/cart/item/{itemId}', 'CartController@deleteItem');
    Route::get('/getSalesTax', 'SettingController@getSalesTaxAmount');
    Route::get('getSalesAmount', 'SettingController@getSalesAmount');


    Route::get('generateSlideshow', 'SlideshowController@generateSlideshow');


    Route::post('stripe/webhooks', 'CampaignController@webhooks');

    Route::get('logout', 'PassportController@logout');
    Route::post('/auth/login', 'PassportController@loginAPI');
    Route::post('register', 'PassportController@register');
    Route::post('sendPasswordResetLink','Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('resetPassword','Auth\ResetPasswordController@reset');
    Route::get('getServerTime', 'SettingController@getServerTime');
    Route::post('guestUser', 'PassportController@guestUser');
    Route::post('socialLogin', 'PassportController@socialLogin');
    Route::post('setInterests', 'PassportController@setInterests');
    Route::get('signup/activate/{token}', 'PassportController@signupActivate');
    Route::get("hash/categories", "HashController@getCategories");
    Route::get('tagslisting', 'CaretController@tagListing');

    Route::get("caret/search", "LicenseController@checkCaretAvailability");
    Route::get("caret/check", "KeywordController@checkKeyword");
    Route::post('/clm-pricing', "CaretPricingController@clmPricingAlgo");
    Route::post('storeCaretSearchData', 'CLMRequestController@storeCaretSearchData');

    Route::get("concatenate-videos", "CaretController@concatenateVideos");

    Route::get("ccv", "CaretController@ccv");




    Route::get("copyvideos", "CaretController@copyvideos");


    Route::get('deletevideos', 'VideoController@deleteVideos');

    Route::get('videos/singleWeb/{id}', 'VideoController@singleWeb');

    Route::group(['middleware' => ['auth:api']], function () {
        Route::post('reportProblem', 'SettingController@reportProblem');
        Route::prefix('users')->group(function () {
            Route::resource('listing', 'UserController');
            Route::get('viewProfile/{id?}', 'UserController@viewProfile');
            Route::post('saveProfile', 'UserController@saveProfile');
            Route::post('status', 'UserController@status');
            Route::post('uniqueUserName', 'UserController@uniqueUserName');
            Route::post('uniqueEmail', 'UserController@uniqueEmail');
            Route::get('viewPrivacy/{id?}', 'UserController@viewPrivacy');
            Route::post('savePrivacy', 'UserController@savePrivacy');
            Route::post('addFollow', 'UserController@addFollow');
            Route::post('isFollow', 'UserController@isFollow');
            Route::post('unFollow', 'UserController@unFollow');
            Route::post('updatePassword','UserController@updatePassword');
            Route::get('deleteUser/{id?}', 'UserController@deleteUser');
            Route::get('createUserOnFirestore/{id?}', 'PassportController@addUpdateFireStore');


            Route::post('blockUser', 'UserController@blockUser');
            Route::post('isBlocked', 'UserController@isBlocked');
            Route::post('unBlock', 'UserController@unBlock');
            Route::get('blockList', 'UserController@blockList');
            Route::post('reportUser', 'UserController@reportUser');


            Route::post('addHTag', 'UserController@addHTag'); // user_id, title
            Route::post('removeHtag', 'UserController@removeHtag');
            Route::get('followers', 'UserController@followers');
            Route::get('following', 'UserController@following');

        });

        Route::get('homePage', 'VideoController@homePage');
        Route::get('sendNotification', 'VideoController@sendNotification');

        Route::prefix('videos')->group(function () {
            Route::resource('listing', 'VideoController');
            Route::get('homelisting', 'VideoController@mobileListing');
            Route::post('LikeUnlike', 'VideoController@LikeUnlike');
            Route::post('isLike', 'VideoController@isLike');
            Route::post('postComment', 'VideoController@postComment');
            Route::get('deleteComment/{id?}', 'VideoController@deleteComment');
            Route::post('LikeUnlikeComment', 'VideoController@LikeUnlikeComment');
            Route::get('comments', 'VideoController@comments');
            Route::get('followVideos', 'VideoController@followVideos');
            Route::get('likedVideos', 'VideoController@likedVideos');
            Route::post('reportVideos', 'VideoController@reportVideos');
            Route::post('watchVideos', 'VideoController@watchVideos');
            Route::get('search', 'VideoController@search');
            Route::get('searchByCaret', 'VideoController@searchByCaret');
            Route::get('searchByHash', 'VideoController@searchByHash');
            Route::get('searchByUser', 'VideoController@searchByUser');
            Route::get('getHash', 'VideoController@getHash');
            Route::get('getCarets', 'VideoController@getCarets');
            Route::get('getFriends', 'VideoController@getFriends');
            Route::get('showWeb/{id}', 'VideoController@showWeb');
            Route::get('trendingCarets', 'VideoController@trendingCarets');
            Route::get('trendingHash', 'VideoController@trendingHash');
            Route::get('trendingUsers', 'VideoController@trendingUsers');
            Route::get('checkNewVideo', 'VideoController@checkNewVideo');
        });

        Route::prefix('ads')->group(function () {
            Route::resource('listing', 'AdController');
            Route::post('data/{user_id}', 'AdController@index');
            Route::post('listing/status/', 'AdController@adSatus');
            Route::get('block/status/{id}', 'AdController@adBlock');
            Route::get('getUsers', 'AdController@getUsers');
            Route::post('watchCount', 'AdController@watchCount');
            Route::get('getAdsData/{user_id}', 'AdController@getAdsData');

        });


        Route::prefix('audios')->group(function () {
            Route::resource('listing', 'SoundController');
            Route::get('homelisting', 'SoundController@mobileListing');
            Route::get('getCategories', 'SoundController@getCategories');
        });

        Route::prefix('carets')->group(function () {
            Route::get('distance', 'CaretController@distanceListing');
            Route::get('tags', 'CaretController@tagListing');
            Route::get('ct', 'CaretController@commandTest');
            Route::get('caret', 'CaretController@vidAlgo');
            Route::get('caret_tag', 'CaretController@caret_based_merging');
        });

        Route::prefix('splash')->group(function () {
            Route::resource('listing', 'SplashController');
            Route::post('data/{user_id}', 'SplashController@index');
            Route::post('listing/status/', 'SplashController@splashSatus');
            Route::post('set_start_default_splash', 'SplashController@setStartDefaultSplash');
            Route::post('set_end_default_splash', 'SplashController@setEndDefaultSplash');
            Route::post('myCarets/{user_id}', 'VideoController@myCarets');
            Route::post('showCaret/{id}', 'VideoController@showCaret');
            Route::get('getRandomMyCaret/{user_id}', 'VideoController@getRandomMyCaret');
            Route::get('getRandomMyVideos/{user_id}', 'VideoController@getRandomMyVideos');
            Route::get('getVideos/{data}', 'VideoController@getVideos');
            Route::post('createCarets', 'VideoController@createCarets');


        });

        Route::prefix('license')->group(function () {
            Route::resource('listing', 'LicenseController');
            Route::post('data/{user_id}', 'LicenseController@index');
            Route::post('listing/status/', 'LicenseController@splashSatus');
            Route::post("/uploadLogo", 'LicenseController@uploadLogo');
            Route::post("/getUsers", 'LicenseController@getUsers');
            Route::post("/licenseStatus", 'LicenseController@licenseStatus');
            Route::get("getStatus", 'LicenseController@getStatus');
            Route::get("getLicense/{user_id}", 'LicenseController@getLicense');
            Route::get("getCaretLogoRendom/{user_id}", 'LicenseController@getCaretLogoRendom');
            Route::post("/defaultSettings", 'LicenseController@defaultSettings');
            Route::post("/toggleLicenseCancel", 'LicenseController@toggleLicenseCancel');
            Route::get("/curntPlan", 'LicenseController@curntPlan');
            Route::post("/calculatePaymentDifference", 'LicenseController@calculatePaymentDifference');
            Route::post("/changeSubscription", 'LicenseController@changeSubscription');

        });

        Route::prefix('corporate')->group(function () {
            Route::resource('listing', 'CLMRequestController');
            Route::post('data/{user_id}', 'CLMRequestController@index');
            Route::post("/licenseStatus", 'CLMRequestController@licenseStatus');
            Route::post("/checkPricing/{order_id}", 'CLMRequestController@checkPricing');
            Route::post("/requestEmail/{order_id}", 'CLMRequestController@requestEmail');

        });

        // Route::prefix('hash')->group(function () {
        //     Route::get("categories", "HashController@getCategories");

        // });


    });

});
Route::prefix('v2')->group(function () {
    Route::group(['middleware' => ['auth:api']], function () {
        Route::prefix('videos')->group(function (): void {
            Route::post('listing', 'VideoController@store_v2');
            Route::post('/upload-chunk', "FileUploadChunksController@uploadChunk");
        });
    });
});

#ADMIN APIS
Route::group([], function () {
    Route::post('/auth/login', 'PassportController@login');
    Route::post('/auth/advlogin', 'PassportController@advlogin');
    Route::post('/auth/clmlogin', 'PassportController@clmlogin');
    Route::post('register', 'PassportController@register');
    Route::post('/user/login', 'Frontend\PassportController@login');
    Route::post('registerAdvertiser', 'PassportController@registerAdvertiser');
    Route::post('registerClm', 'PassportController@registerClm');
    Route::post('registerClmForCart', 'PassportController@registerClmForCart');
    Route::post('/check_sign_up_user', 'PassportController@checkSignupAdvUser');
    Route::post('/check_signup_clm_user', 'PassportController@checkSignupClmUser');
    Route::post('/check_login', 'PassportController@checkLogin');
});

Route::group(['middleware' => ['auth:api']], function () {

    Route::post('/generateClmUserToken', 'PassportController@generateClmUserToken');
    Route::post('/generateAdvUserToken', 'PassportController@generateAdvUserToken');

        Route::prefix('cart')->group(function () {
            Route::post('associateUser', 'CheckoutController@associateUserWithGuestToken');
            Route::get('/checkout', 'CheckoutController@checkout');
            Route::get('orders/{userId}', 'CheckoutController@orderListing');
            Route::get('orderItems/{orderId}', 'CheckoutController@orderItemListing');
            Route::get('/checkLicense', 'CheckoutController@checkLicenseAvailability');
            Route::delete('/orderItem/{id}', 'CheckoutController@deleteOrderItem');
        });

        Route::prefix('settings')->group(function () {
            Route::resource('listing', 'SettingController');
            Route::get('settingsTypes', 'SettingController@getTypes');
            Route::post('listing/status/', 'SettingController@settingsStatus');
            // Route::get('getSalesAmount', 'SettingController@getSalesAmount');
            Route::post('associateUser', 'CheckoutController@associateUserWithGuestToken');
        });


        Route::prefix('cms')->group(function () {
            Route::resource('pages', 'PagesController');
            Route::post('pages/status/', 'PagesController@pageStatus');
        });


        Route::get('userDetails', 'PassportController@details');
        Route::get('logout', 'PassportController@logout');
        Route::get('getUserRoles', 'RoleController@getUserRoles')->name('get-user-roles');
        Route::get('getAgencyRoles', 'RoleController@getAgencyRoles')->name('get-agency-roles');
        Route::get('getAllUsers', 'UserController@index');
        Route::get('getAllPermissions', 'RoleController@getAllPermissions')->name('get-all-permissions');
        Route::get('getMenu', 'PermissionController@getMenu')->name('get-menu');
        Route::get('getAdvertiserMenu', 'PermissionController@getAdvertiserMenu');
        Route::get('getCLMMenu', 'PermissionController@getCLMMenu');

        Route::resource('users', 'UserController');
        Route::get('advertisers-listing', 'UserController@advertisersListing');
        Route::get('users-listing', 'UserController@listing');
        Route::put('users/updateStatus/{userId}', 'UserController@updateStatus');
        Route::put('users/blocked/{userId}', 'UserController@blocked');
        Route::resource('roles', 'RoleController');
        Route::get('getAlreadyExistingOrders', 'PassportController@getAlreadyExistingOrders')->name('getAlreadyExistingOrders');
        Route::get('getAllParentLabels', 'PassportController@getAllParentLabels')->name('getAllParentLabels');
        Route::get('getAllModules/{id?}', 'ModuleController@getAllModules');
        Route::resource('permission', 'PermissionController');
        Route::resource('modules', 'ModuleController');
        Route::post('users/getIndustry', 'UserController@getIndustry');
        Route::post('users/storeAdvertiser', 'UserController@storeAdvertiser');
        Route::post('users/updateAdvertiser', 'UserController@updateAdvertiser');
        Route::post('users/changePassword','UserController@changePassword');

        Route::get('deleteAdvertiser/{id?}', 'UserController@deleteAdvertiser');
        // CLM
        Route::get('clmUsers-listing', 'UserController@clmUsersListing');
        Route::post('users/storeClmUser', 'UserController@storeClmUser');
        Route::post('users/updateClmUser', 'UserController@updateClmUser');
        Route::get('deleteClmUser/{id?}', 'UserController@deleteClmUser');


        Route::prefix('music')->group(function () {
            Route::resource('listing', 'SoundController');
            Route::get('data/{user_id?}', 'SoundController@index');
            Route::post('listing/status/', 'SoundController@soundSatus');
            Route::get('categories', 'SoundController@musicCategories');
            Route::post('addCategory', 'SoundController@createCategory');
            Route::get('category/{id}', 'SoundController@getCategory');
            Route::post('category/status/', 'SoundController@categorySatus');
            Route::get('category/delete/{id}', 'SoundController@destroyCategory');
            Route::get('getCategories', 'SoundController@getCategories');
            Route::get('getSoundsData/{user_id}', 'SoundController@getSoundsData');
            Route::get('getSoundsDataRandom/{data}', 'SoundController@getSoundsDataRandom');
        });

        Route::prefix('video')->group(function () {
            Route::resource('listing', 'VideoController');
            Route::post('clmdata', 'VideoController@indexCLM');
            Route::post('data', 'VideoController@index');
            Route::post('listing/status/', 'VideoController@videoSatus');
            Route::get('block/status/{id}', 'VideoController@videoBlock');
            Route::get('getUsers', 'VideoController@getUsers');

        });

        Route::prefix('hash')->group(function () {
            Route::resource('listing', 'HashController');
            Route::post('/data', 'HashController@index');
            Route::post('addCategory', 'HashController@createCategory');
            Route::get('category/{id}', 'HashController@getCategory');
            Route::post('category/status', 'HashController@categorySatus');
            Route::post('/status', 'HashController@hashSatus');
            Route::get('category/delete/{id}', 'HashController@destroyCategory');
            Route::get('/categories', 'HashController@getCategories');
        });

        Route::prefix('hashCategory')->group(function () {
            Route::resource('listing', 'HashCategoryController');
            Route::post('/status', 'HashCategoryController@hashCategorySatus');

        });

        ##################################################################################

        Route::prefix('corporate')->group(function () {
            Route::resource('listing', 'CLMRequestController');
            Route::post('data/{user_id}', 'CLMRequestController@index');
            Route::post("/licenseStatus", 'CLMRequestController@licenseStatus');
            Route::post("/checkPricing/{order_id}", 'CLMRequestController@checkPricing');
            Route::post("/requestEmail/{order_id}", 'CLMRequestController@requestEmail');
            Route::post("/checkRequestStatus/{order_id}", 'CLMRequestController@checkRequestStatus');
        });

        Route::prefix('license')->group(function () {
            Route::resource('listing', 'LicenseController');
            Route::post('data/{user_id}', 'LicenseController@index');
            Route::post('listing/status/', 'LicenseController@splashSatus');
            Route::post("/licenseStatus", 'LicenseController@licenseStatus');
            Route::post("/uploadLogo", 'LicenseController@uploadLogo');
            Route::get("getStatus", 'LicenseController@getStatus');
            Route::get("getLicense/{user_id}", 'LicenseController@getLicense');
            Route::get("getCaretLogoRendom/{user_id}", 'LicenseController@getCaretLogoRendom');
            Route::post("/defaultSettings", 'LicenseController@defaultSettings');
            Route::post("/toggleLicenseCancel", 'LicenseController@toggleLicenseCancel');
            Route::get("/curntPlan", 'LicenseController@curntPlan');
            Route::post("/calculatePaymentDifference", 'LicenseController@calculatePaymentDifference');
            Route::post("/changeSubscription", 'LicenseController@changeSubscription');
            Route::get("/caretLicense", 'LicenseController@caretLicense');
            Route::get('/corporateLicense', 'LicenseController@corporateLicense');

        });

        Route::prefix('keyword')->group(function () {
            Route::resource('listing', 'KeywordController');
            Route::post('data', 'KeywordController@index');
            Route::post('/status', 'KeywordController@keywordStatus');


        });
        Route::prefix('caretPricing')->group(function () {
            Route::resource('listing', 'CaretPricingController');
            Route::post('data', 'CaretPricingController@index');
            Route::post('/status', 'CaretPricingController@caretPricingStatus');
            Route::post('/salesStatus', 'CaretPricingController@contactSalesStatus');

        });


        Route::prefix('splash')->group(function () {
            Route::resource('listing', 'SplashController');
            Route::post('data/{user_id}', 'SplashController@index');
            Route::post('listing/status/', 'SplashController@splashSatus');
            Route::post('set_start_default_splash', 'SplashController@setStartDefaultSplash');
            Route::post('set_end_default_splash', 'SplashController@setEndDefaultSplash');
            Route::get('getSplashes/{user_id}', 'SplashController@getSplashes');
            Route::get('getSplashesRandom/{user_id}', 'SplashController@getSplashesRandom');
            Route::get('getVideos/{data}', 'VideoController@getVideos');
            Route::post('showCaret/{id}', 'VideoController@showCaret');
            Route::get('getRandomMyCaret/{user_id}', 'VideoController@getRandomMyCaret');
            Route::get('getRandomMyVideos/{user_id}', 'VideoController@getRandomMyVideos');
            Route::post('createCarets', 'VideoController@createCarets');
            Route::post('myCarets/{user_id}', 'VideoController@myCarets');
            Route::post('caretVideos/{id}', 'VideoController@caretVideos');
        });





        Route::prefix('ads')->group(function () {
            Route::resource('listing', 'AdController');
            Route::post('data/{user_id}', 'AdController@index');
            Route::post('listing/status/', 'AdController@adSatus');
            Route::get('block/status/{id}', 'AdController@adBlock');
            Route::get('getUsers', 'AdController@getUsers');
            Route::post('watchCount', 'AdController@watchCount');
            Route::get('getAdsData/{user_id}', 'AdController@getAdsData');
            Route::get('getAdsDataRandom/{data}', 'AdController@getAdsDataRandom');
        });


        Route::prefix('plans')->group(function () {
            Route::resource('listing', 'PlanController');
            Route::get('data/{id}', 'PlanController@index');
            Route::post('listing/status/', 'PlanController@planStatus');
            Route::get('durations', 'PlanController@getDurations');
        });

        Route::prefix('clmPlans')->group(function () {
            Route::resource('listing', 'SplashPlanController');
            Route::get('data/{id}', 'SplashPlanController@index');
            Route::post('listing/status/', 'SplashPlanController@planStatus');
            Route::get('durations', 'SplashPlanController@getDurations');
            Route::get('getInfluencerPlan', 'SplashPlanController@getInfluencerPlan');
            Route::get('planData', 'SplashPlanController@getPlans');

        });

        Route::prefix('campaigns')->group(function () {

            Route::resource('listing', 'CampaignController');
            Route::get('getCampaignsSummary/{user_id}', 'CampaignController@getCampaignsSummary');
            Route::get('data/{user_id}', 'CampaignController@index');
            Route::post('listing/status/', 'CampaignController@planStatus');
            Route::post('listing/status/', 'CampaignController@campaignStatus');
            Route::get('block/status/{id}', 'CampaignController@campaignBlock');
            Route::post('getUsers', 'CampaignController@getUsers');
            Route::get('getPlans', 'CampaignController@getPlans');
            Route::get('getAds', 'CampaignController@getAds');
            Route::get('getGenders', 'CampaignController@getGenders');
            Route::get('getAges', 'CampaignController@getAges');
            Route::get('getCountries', 'CampaignController@getCountries');
            Route::get('getStatus', 'CampaignController@getStatus');


            Route::get('ads/{campaign_id}', 'CampaignController@indexAds');

            Route::post('ads/storeAds', 'CampaignController@storeAds');
            Route::post('ads/storeMultiAds', 'CampaignController@storeMultiAds');
            Route::post('ads/updateMultiAds', 'CampaignController@updateMultiAds');
            Route::get('ads/showAds/{id}', 'CampaignController@showAds');
            Route::get('ads/destroyAds/{id}', 'CampaignController@destroyAds');

            Route::post('ads/updateAdsStatus', 'CampaignController@updateAdsStatus');
            Route::post('ads/toggleAdsCancel', 'CampaignController@toggleAdsCancel');

            Route::get('user/{id}', 'UserController@profile');


            Route::post('activateSubscription', 'CampaignController@activateSubscription');
            Route::post('inActivateSubscription', 'CampaignController@inActivateSubscription');


        });

        Route::prefix('cards')->group(function () {
            Route::resource('listing', 'CardController');
            Route::get('data/{user_id}', 'CardController@index');
            Route::post('listing/status/', 'CardController@cardStatus');
            Route::post('useExistingCard', 'CardController@useExistingCard');
        });

        Route::prefix('payments')->group(function () {
            Route::resource('listing', 'PaymentController');
            Route::get('data/{user_id}', 'PaymentController@index');
        });
        Route::prefix('CLMPayments')->group(function () {
            Route::resource('listing', 'CLMPaymentController');
            Route::get('data/{user_id}', 'CLMPaymentController@index');
            Route::get('getInfluencerPlan', 'SplashPlanController@getInfluencerPlan');

        });


});
//Route::get("test", 'HashController@index');
