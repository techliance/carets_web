<?php

use Illuminate\Support\Facades\Route;
use App\AdPayments;
use App\CampaignsAds;
use App\AdPlans;
use App\Campaigns;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/video/{videoId}', 'VideoController@singleWeb');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/reset/{id}','Auth\ResetPasswordController@reset');

Route::get('/reset/{token}', function ($token) {
    return view('auth.passwords.reset', ['token' => $token]);
})->middleware('guest')->name('password.reset');

Route::get("test", function(){
    // $query = Campaigns::find(1322)->campaignAds;
    // $activeCount = 0;
    // $otherCount = 0;
    // foreach($query as $q){
    //     $activeCount += $q->status()->where("title", "Active")->count();
    //     $otherCount += $q->status()->where("title", "!=", "Active")->count();
    // }
    // return $otherCount;
    // Fetch the campaign with ID 1322 and eager load the 'campaignAds' relationship
$campaign = Campaigns::with('campaignAds')->find(1322);
$activeCount = 0;
$otherCount = 0;
foreach ($campaign->campaignAds as $campaignAd) {
    $activeCount += $campaignAd->status()->where("title", "Active")->count();
    $otherCount += $campaignAd->status()->count() - $activeCount;
}
return $otherCount;   
});