<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class Helper
{


    public static function putS3Image($file_path, $image, $type) {
        $confirmUpload = Storage::disk('s3')->put($file_path, $image, $type);
        if ($confirmUpload)
            return true;
        else
            return false;
    }

    public static function errorResponse($error, $message, $error_code=400) {
        return response([
            'status_code' => (int) $error_code,
            'result' => 'error',
            'error' => $error,
            'msg' => $message
        ], $error_code)
        ->header('error', $error)
        ->header('message', $message);
    }


    public static function clearNullValue($var) {
        if(strtolower($var) === "<null>")
            return NULL;
        return $var;
    }


    public static function remoteFileSize( $url ) {

        $url = str_replace('https:', 'http:', $url);

        // Assume failure.
        $result = [];

        $curl = curl_init( $url );

        // Issue a HEAD request and follow any redirects.
        curl_setopt( $curl, CURLOPT_NOBODY, true );
        curl_setopt( $curl, CURLOPT_HEADER, true );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );

        $data = curl_exec( $curl );
        curl_close( $curl );

        //return $data;
        if( $data ) {
            $content_length = "unknown";
            $status = "unknown";

            if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
                $status = (int)$matches[1];
            }

            if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
                $content_length = (int)$matches[1];
            }
            $content_type = '';
            if( preg_match( "/Content-Type: ([a-zA-Z_\-0-9\/]+)/", $data, $matches ) ) {
                $content_type = $matches[1] ?? $matches[0];
            }

            // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            if( $status == 200 || ($status > 300 && $status <= 308) ) {
                $result['size'] = $content_length;
                $result['mime_type'] = $content_type;
            }
        }

        return $result;
    }




}
