<?php
use App\Order;
use App\Account;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

if (!function_exists('generateUserName')) {
    function generateUserName($length = 5) {
        if (!$length) {
            return false;
        }

        $duplicationExist = true;
        $generatedUsername = '';
        while($duplicationExist) {
            // $generatedUsername = strtoupper(\Illuminate\Support\Str::random($length));
            $generatedUsername = random_code($length);
            // Make sure this username isn't being used
            $duplicationExist = \App\Order::where('username', $generatedUsername)->orWhere('photo_admin_username', $generatedUsername)->count();
        }
        return $generatedUsername;
    }
}
if (!function_exists('random_code')) {
    function random_code($limit)
        {
           $random_code = strtolower(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit));
            if (
                strpos($random_code, '1') !== false || 
                strpos($random_code, 'L') !== false || 
                strpos($random_code, 'l') !== false ||
                strpos($random_code, 'I') !== false ||
                strpos($random_code, 'o') !== false ||
                strpos($random_code, 'O') !== false ||
                strpos($random_code, '0') !== false
            ) {
                return random_code($limit);
            } else{
                return $random_code;
            }
        }
}


if (!function_exists('getStorageUrl')) {
    function getStorageUrl() {
        return Config::get('filesystems.disks.s3.url');
    }
}


if (!function_exists('generateResourceProfileImagesPath')) {
    function generateResourceProfileImagesPath($dynamicPath) {
        $aws_brand = Config::get('filesystems.s3_path.order.brand');
        $aws_resources = Config::get('filesystems.s3_path.order.resources');
        $brand_account_id = 1;
        $resource_path = $aws_resources . 'account_id/'.$dynamicPath;
        $order_resources_path = str_replace('/account_id/','/account_' . $brand_account_id . '/', $resource_path);
        return $order_resources_path;
    }
}

if (!function_exists('returnFileNameWithoutExt')) {
    function returnFileNameWithoutExt($file_name) {
        return preg_replace("/\.[^.]+$/", "", $file_name);
    }
}


if (!function_exists('abstractImageFromBase64')) {
    function abstractImageFromBase64($file) {
        list($baseType, $image) = explode(';', $file);
        list($temp, $mime_type) = explode(":", $baseType);
        list(, $image) = explode(',', $image);
        return base64_decode($image);
    }
}

if (!function_exists('putS3Image')) {
    function putS3Image($file_path, $image, $type) {
        $confirmUpload = Storage::disk('s3')->put($file_path, $image, $type);
        if ($confirmUpload)
            return true;
        else 
            return false;
    }
}


if (!function_exists('returnImagePath')) {
    function returnImagePath($data) {
        return !empty($data) ? $data['file_path'].$data['image_versions']['version_type'].'/'.$data['file_title'].'.'.$data['image_format']['extension']:null;
    }
}


if (!function_exists('getIdWithColFromTable')) {
    function getIdWithColFromTable($model, $column, $value) {
        return $model::where($column, $value)->first()->id;
    }
}

if (!function_exists('getOrderDetails')) {
    function getOrderDetails($data) {
        
        $data->traveler_link = App::make('url')->to('/');

        $data->photo_admin_link = App::make('url')->to('/');

        $destinations = $data->destinations->implode(',');
        unset($data->destinations);
        $data->destinations = $destinations;
        $details = [];
        $details['id'] = $data->id;
        // $details['account_id'] = $data->account_id;
        $details['group_name'] = $data->group_name;
        $details['barcode'] = $data->barcode;
        $details['order_number'] = $data->order_number;
        $details['group_leader_contact'] = $data->groupLeaderContact;
        $details['agency_id'] = $data->agency_id;
        $details['agency'] = $data->agency;
        $details['customer_slates_for_videos_id'] = $data->customer_slates_for_videos_id;
        $details['customer_slates_for_videos'] = $data->customerSlatesForVideos;
        $details['program_type_id'] = $data->program_type_id;
        $details['program_type'] = $data->programType;
        $details['agency_type_id'] = $data->agency_type_id;
        $details['agency_type'] = $data->agencyType;
        $details['agency_sales_rep_id'] = $data->agency_sales_rep_id;
        $details['agency_sales_rep'] = $data->agencySalesRep;
        $details['gtv_sales_rep_id'] = $data->gtv_sales_rep_id;
        $details['gtv_sales_rep'] = $data->gtvSalesRep;
        $details['editor_id'] = $data->editor_id;
        $details['editor'] = $data->editor;
        $details['producer_id'] = $data->producer_id;
        $details['producer'] = $data->producer;
        $details['qa_id'] = $data->qa_id;
        $details['qa'] = $data->qa;
        $details['number_of_cameras'] = $data->number_of_cameras;
        $details['username'] = $data->username;
        $details['password'] = $data->password;
        $details['traveler_link'] = $data->traveler_link;
        $details['photo_admin_username'] = $data->photo_admin_username;
        $details['photo_admin_password'] = $data->photo_admin_password;
        $details['photo_admin_link'] = $data->photo_admin_link;
        $details['client_tour_number'] = $data->client_tour_number;
        $details['client_tour_code'] = $data->client_tour_code;
        $details['status_id'] = $data->status_id;
        $details['status'] = $data->status;
        $details['music_library_id'] = $data->music_library_id;

        //$details['music_library'] = $data->music_library;
        $details['music_selection_criteria'] = $data->music_selection_criteria;
        $details['custom_music'] = $data->custom_music;
        $details['created_at'] = $data->created_at;
        $details['updated_at'] = $data->updated_at;
        $details['going_on_trip_count'] = $data->going_on_trip_count;
        $details['following_from_home_count'] = $data->following_from_home_count;
        $details['destinations'] = $data->destinations;
        $details['order_dates']['departure_date'] = $data->orderDates['departure_date'];
        $details['order_dates']['return_date'] = $data->orderDates['return_date'];
        $details['order_dates']['last_day_of_school'] = $data->orderDates['last_day_of_school'];
        $details['order_dates']['pre_trip_package_need_date'] = $data->orderDates['pre_trip_package_need_date'];
        $details['order_dates']['video_special_need_date'] = $data->orderDates['video_special_need_date'];
        //TODO please replace this priority dummy value with order priority value
        $details['priority'] = 1;
        $details['is_camera_shipped'] = $data->is_camera_shipped;
        $details['order_resources_count'] = $data->order_resources_count;
        $details['images_count'] = $data->images_count;
        $details['max_allowed_photos'] = $data->max_allowed_photos;
        $details['base_url'] = url('/api');
        $details['settings'] = $data->settings;
        
        return $details;
    }
    /*apply select, pluck, first or get to outer query*/
    if (!function_exists('SWPFA')) {
        function SPFA($query, $select=[],  $pluck = null, $first = false, $all = false) {
            if ( sizeof($select) > 0 )
                $query = $query->select($select);
            if ($all)
                $query = $query->get();
            if ($pluck)
                $query = $query->pluck($pluck);
            if ($first)
                $query = $query->first();
            return $query;
        }
    }
}