<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdCampaigns extends Model
{
    use SoftDeletes;
    protected $table = 'ad_campaigns';
    protected $fillable = ['id','ad_id', 'user_id','plan_id','ages','gender','location','campaign_title','is_active','is_blocked','watch_count','recurring','subscription_status','cancel_at_period_end','current_period_end','deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    public function ad()
    {
        return $this->hasOne('App\Ads', 'id', 'ad_id');
    }

    public function plan()
    {
        return $this->hasOne('App\AdPlans', 'id', 'plan_id');
    }

    public function age()
    {
        return $this->hasOne('App\Ages', 'id', 'ages');
    }

    public function genders()
    {
        return $this->hasOne('App\Gender', 'id', 'gender');
    }
}
