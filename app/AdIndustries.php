<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdIndustries extends Model
{
    protected $table = 'ad_industry';
    protected $fillable = ['id','title','deleted_at'];

}
