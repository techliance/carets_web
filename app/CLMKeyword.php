<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CLMKeyword extends Model
{
    #use SoftDeletes;
    use SoftDeletes;
    protected $table = 'clm_keywords';
    protected $fillable = ['id', 'title','type','is_active','deleted_at'];
}
