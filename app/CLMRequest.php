<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class CLMRequest extends Model
{
    use SoftDeletes;
    use PaginationWithHavings;
    protected $table = 'clm_requests';
    protected $fillable = ['id','user_id', 'order_id', 'order_item_id','status_id','caret_id','caret_title','subscription_plan','subscription_amount','name','email','phone','designation','company_name','company_address','company_description','status','approval_date','deleted_at',];


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function plan()
    {
        return $this->hasOne('App\CLMPlans', 'id', 'subscription_plan');
    }

    public function pricing()
    {
        return $this->hasOne('App\CaretPricing', 'id', 'subscription_plan');
    }

    public function statusA()
    {
        return $this->hasOne('App\AdStatus', 'id', 'status_id');
    }

}
