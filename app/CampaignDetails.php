<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignDetails extends Model
{
    use SoftDeletes;
    protected $table = 'campaign_details';
    protected $fillable = ['id','campaign_id','ages','gender','location','deleted_at'];

    public function campaign()
    {
        return $this->hasOne('App\Campaign', 'id', 'campaign_id');
    }

    public function age()
    {
        return $this->hasOne('App\Ages', 'id', 'ages');
    }

    public function genders()
    {
        return $this->hasOne('App\Gender', 'id', 'gender');
    }

}
