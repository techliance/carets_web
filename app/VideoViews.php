<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoViews extends Model
{
    protected $table = 'video_views';
    protected $fillable = ['id','video_id', 'user_id','deleted_at'];
}
