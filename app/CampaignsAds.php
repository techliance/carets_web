<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignsAds extends Model
{
    use SoftDeletes;
    protected $table = 'campaigns_ads';
    protected $fillable = ['id','ad_id','plan_id','campaign_id', 'watch_count','recurring','subscription_status','cancel_at_period_end','current_period_end','status_id','deleted_at'];


    public function status()
    {
        return $this->hasOne('App\AdStatus', 'id', 'status_id');
    }
    public function ad()
    {
        // return $this->hasOne(\App\Ads::class, 'id', 'ad_id');
        return $this->hasOne('App\Ads', 'id', 'ad_id');

    }

    public function campaign()
    {
        return $this->hasOne('App\Campaigns', 'id', 'campaign_id');
    }

    public function campaignViews()
    {
        return $this->hasMany(\App\AdViews::class, 'ad_id', 'ad_id');
    }

    public function payments()
    {
        return $this->hasMany(\App\AdPayments::class, 'campaign_ad_id', 'id');
    }


    public function plan()
    {
        return $this->hasOne('App\AdPlans', 'id', 'plan_id');
    }

}
