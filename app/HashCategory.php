<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HashCategory extends Model
{
    use SoftDeletes;
    protected $table = 'hash_categories';
    protected $fillable = ['id','title','is_active', 'deleted_at','updated_at'];

}
