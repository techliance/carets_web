<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchCaretByLike extends Model
{
    protected $table = 'search_caret_like';
    protected $fillable = ['id','video_id', 'age'];
}
