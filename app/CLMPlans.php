<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CLMPlans extends Model
{
    use SoftDeletes;
    protected $table = 'clm_plans';
    protected $fillable = ['id','stripe_id', 'title','duration','interval_count','amount','is_active','deleted_at'];
}
