<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavVideos extends Model
{
    protected $table = 'user_fav_videos';
    protected $fillable = ['id','user_id', 'video_id','deleted_at'];
}
