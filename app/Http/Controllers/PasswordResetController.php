<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\PasswordReset;

class PasswordResetController extends Controller
{
    public function sendOtp(Request $request)
    {
        $request->validate(['email' => 'required|email|exists:users,email']);

        $otp = rand(100000, 999999); // Generate 6-digit OTP

        // Store OTP in password_resets table
        PasswordReset::updateOrCreate(
            ['email' => $request->email],
            [
                'otp' => $otp,
                'expires_at' => Carbon::now()->addMinutes(10),
            ]
        );

        // Send email
        Mail::raw("Your password reset OTP is: $otp", function ($message) use ($request) {
            $message->to($request->email)
                    ->subject("Password Reset OTP");
        });

        return response()->json(['message' => 'OTP sent successfully.'], 200);
    }

    public function verifyOtp(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'otp' => 'required|digits:6'
        ]);

        $passwordReset = PasswordReset::where('email', $request->email)
            ->where('otp', $request->otp)
            ->where('expires_at', '>', Carbon::now())
            ->first();

        if (!$passwordReset) {
            return response()->json(['message' => 'Invalid or expired OTP.'], 400);
        }

        return response()->json(['message' => 'OTP verified. Proceed to reset password.'], 200);
    }

    public function resetPassword(Request $request)
{
    // ✅ Validate request
    $request->validate([
        'email' => 'required|email|exists:users,email',
        'otp' => 'required|digits:6',
        'password' => 'required|string|min:8|confirmed', // Added password confirmation
    ]);

    // ✅ Find OTP record
    $passwordReset = PasswordReset::where('email', $request->email)
        ->where('otp', $request->otp)
        ->where('expires_at', '>', Carbon::now())
        ->first();

    if (!$passwordReset) {
        return response()->json(['message' => 'Invalid or expired OTP.'], 400);
    }

    // ✅ Find user
    $user = User::where('email', $request->email)->first();
    if (!$user) {
        return response()->json(['message' => 'User not found.'], 404);
    }

    // ✅ Hash the new password securely
    $user->password = Hash::make($request->password);
    $user->save();

    // ✅ Remove OTP record after reset
    PasswordReset::where('email', $request->email)->delete();

    return response()->json(['message' => 'Password reset successful.'], 200);
}

}
