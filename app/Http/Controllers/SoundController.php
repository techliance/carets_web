<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\SoundCategories;
use App\SoundReports;
use App\Sounds;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class SoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id=null)
    {
        $Sounds = new Sounds();
        $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;



        if ($filter == '' && $sort == ''){
            $data = $Sounds->getMusicList($user_id,$pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $Sounds->getMusicList($user_id,$pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $Sounds->getMusicList($user_id,$pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $Sounds->getMusicList($user_id,$pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function mobileListing(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $cat_id = $request->input('cat_id', null);

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Sounds::with(['categories'])
        ->select(['*',
            DB::raw("CONCAT('".config('app.awsurl')."', sound_url) AS sound_url"),
            DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")
        ])
        ->where(function($q) use ($filter){
                $q->where('sound_title','like', '%' . $filter . '%');
            })
        ->when($cat_id,function($q) use($cat_id){
            $q->whereHas("categories", function($q) use($cat_id) {
                $q->where('sound_categories.id',$cat_id);
            });
        })
        ->where('is_active',1)
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })

            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function musicCategories(Request $request)
    {
        $Sounds = new Sounds();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $Sounds->getCategoriesList($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $Sounds->getCategoriesList($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $Sounds->getCategoriesList($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $Sounds->getCategoriesList($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function getCategories(Request $request){
        $categories = SoundCategories::orderBy('id','DESC')
        ->select('id as value', 'title as label')
        ->where('is_active',1)
        ->get();
        return $this->sendSuccessResponse('data', $categories, 'Record retrieved successfully!');
    }

    public function getCategory($id)
    {
        $SoundCategories = SoundCategories::where('id', $id)->first();
        return response()->json([ 'data' => $SoundCategories, 'message' => ''], 200);
    }

    public function createCategory(Request $request)
    {
        $data = $request->all();
        $data2['title'] = trim($data['title']);
        $data2['is_active'] = trim($data['is_active']);

        if(isset($data['id'])){
            $SoundCategories = SoundCategories::where('id', $data['id'])->first();
            if($SoundCategories) {
                $SoundCategories->update($data2);
            }
        }else{
            $SoundCategories = SoundCategories::create($data2);
            $data['id'] = $SoundCategories->id;
        }
        $SoundCategories = SoundCategories::where('id', $data['id'])->first();

        return response()->json([ 'data' => $SoundCategories, 'message' => 'Record stored successfully!'], 200);
    }
    public function categorySatus(Request $request) {
        //return $request->all();
        $category_id = $request->category_id;
        $SoundCategories = SoundCategories::where('id', $category_id)->first();
        if($SoundCategories){
            if($SoundCategories->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $SoundCategories->update($data);
            return response()->json([ 'data' => $SoundCategories, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function destroyCategory($id)
    {
        $SoundCategories = SoundCategories::where('id', $id)->first();
        if($SoundCategories){
            $SoundCategories->delete();
            return response()->json([ 'data' => $SoundCategories, 'message' => 'Record deleted successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }
    }

    public function createReport(Request $request)
    {
        $data = $request->all();
        $data2['sound_id'] = trim($data['sound_id']);
        $data2['report_by'] = trim($data['report_by']);
        $data2['remarks'] = trim($data['remarks']);

        if(isset($data['report_id'])){
            $SoundReports = SoundReports::where('id', $data['report_id'])->first();
            if($SoundReports) {
                $SoundReports->update($data2);
            }
        }else{
            $SoundReports = SoundReports::create($data2);
            $data['report_id'] = $SoundReports->id;
        }
        $SoundReports = SoundReports::where('id', $data['report_id'])->first();

        return response()->json([ 'data' => $SoundReports, 'message' => 'Reported successfully!'], 200);
    }


    public function soundSatus(Request $request) {
        $sound_id = $request->sound_id;
        $sound = Sounds::where('id', $sound_id)->first();
        if($sound){
            if($sound->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $sound->update($data);
            return response()->json([ 'data' => $sound, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data2['sound_title'] = trim($data['sound_title']);
        //$data2['sound_url'] = trim($data['sound_url']);
        $data2['sound_description'] = trim($data['sound_description']);
        //$data2['image_url'] = trim($data['image_url']);
        $data2['is_active'] = trim($data['is_active']);

        if(isset($data['user_id'])){
            $data2['user_id'] = trim($data['user_id']);
        }

        if(isset($data['license_id'])){
            $data2['license_id'] = trim($data['license_id']);
        }

        if(isset($data['sound_id'])){
            $Sounds = Sounds::where('id', $data['sound_id'])->first();
            if($Sounds) {
                $Sounds->update($data2);
            }
        }else{
            $Sounds = Sounds::create($data2);
            $data['sound_id'] = $Sounds->id;
        }

        $input['image'] = $request->input('image');
        if($input['image']) {
            $imageData = $input['image'];
            $file       = $imageData['file'];
            $file_name  = $imageData['name'];
            $image = file_get_contents($file);

            $file_path =  'Audio/' . $data['sound_id'] . '/' . $file_name;
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            if ($confirmUpload){
                $newdata['image_url'] = $file_path;
                $Sounds->update($newdata);
            }
        }

        if ($request->hasFile('image_url')) {
            $input_img = $request->file('image_url');
            $image = $input_img;
            $extension = $image->getClientOriginalExtension();
            $folder = 'Sounds/';
            $name = time();
            $file_path =   $folder. $data['sound_id'] . '_' . $name.'.'.$extension;
            $image = file_get_contents($image);

            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            $image_url = $file_path;

            if($image_url){
                $newdata['image_url'] = $image_url;
                $Sounds->update($newdata);
            }
        }

        $input['audioFile'] = $request->input('audioFile');
        if($input['audioFile']) {
            $FileData = $input['audioFile'];
            $file       = $FileData['file'];
            $file_name  = $FileData['name'];

            $file_name = preg_replace('/[^a-zA-Z0-9-_\.]/', '_', $file_name);

            $audio = file_get_contents($file);

            $file_path =  'Audio/' . $data['sound_id']. '_' . time().$file_name;

            $confirmUpload = Helper::putServerImage($file_path, $audio, 'public');


            if ($confirmUpload){

                $originalVideoPath = storage_path('app/' . $file_path);
                $scaledVideoPath = Helper::convertToMp3($originalVideoPath);

                if($scaledVideoPath){
                    $audio = file_get_contents($scaledVideoPath);
                    $fileInfo = pathinfo($scaledVideoPath);
                    $fileName = 'Audio/' . $fileInfo['filename'].'.mp3';
                    Helper::putS3Image($fileName, $audio, 'public');
                    $newdata['sound_url'] = $fileName;
                    $Sounds->update($newdata);
                }

            }
        }

        if ($request->hasFile('sound_url')) {
            $input_img = $request->file('sound_url');
            $image = $input_img;
            $extension = $image->getClientOriginalExtension();
            $folder = 'Sounds/';
            $name = time();
            $file_path =   $folder. $data['sound_id'] . '_' . time().'.'.$extension;
            $image = file_get_contents($image);
            Helper::putServerImage($file_path, $image, 'public');
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            $sound_url = $file_path;

            if($sound_url){
                $newdata['sound_url'] = $sound_url;
                $Sounds->update($newdata);
            }
        }


        $Sounds->categories()->sync($data['categories']);
        $data = Sounds::where('id', $data['sound_id'])->with('categories')->first();

        return response()->json([ 'data' => $data, 'message' => 'Record stored successfully!'], 200);
    }

    public function show($id)
    {
        $data = Sounds::select(['*',
        DB::raw("CONCAT('".config('app.awsurl')."', sound_url) AS sound_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")
        ])->where('id', $id)->first();
        $categories = $data->categories->map(function ($item, $key) {
            return $item->id;
        });
        unset($data->categories);
        $data->categories = $categories;
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $Sounds = Sounds::where('id', $id)->first();
        $Sounds->delete();
        return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
    }


    public function destroyReport($id)
    {
        $SoundReports = SoundReports::where('id', $id)->first();
        $SoundReports->delete();
        return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
    }

    public function getSoundsData(Request $request, $user_id){
        $signed_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $licenseId = $request->input('licenseId');
        // Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $users = Sounds::orderBy('id','DESC')
        ->select('id as value', 'sound_title as label',
        DB::raw("CONCAT('".config('app.awsurl')."', sound_url) AS sound_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")
        )
        ->where(function($q) use ($filter) {
            $q->where('sound_title', 'like', '%' . $filter . '%'); 
        })
        ->when($signed_id, function ($query, $signed_id) {
            $query->where('user_id', $signed_id);
        })
        ->when($licenseId, function ($q) use ($licenseId) {
            $q->where(function ($query) use ($licenseId) {
                $query->where('license_id', $licenseId)
                      ->orWhereNull('license_id'); // Check for null values explicitly
            });
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }
    public function getSoundsDataRandom(Request $request, $user_id){
        $filter = $request->input('filter');
        $licenseId = $request->input('licenseId');
        $signed_id = (int)$user_id > 0?$user_id:null;
        // Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $users = Sounds::orderBy('id','DESC')
        ->select('id as value', 'sound_title as label',
        DB::raw("CONCAT('".config('app.awsurl')."', sound_url) AS sound_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")
        )
        ->where(function($q) use ($filter) {
            $q->where('sound_title', 'like', '%' . $filter . '%'); 
        })
        ->when($signed_id, function ($query, $signed_id) {
            $query->where('user_id', $signed_id);
        })
        ->when($licenseId, function ($q) use ($licenseId) {
            $q->where('license_id', $licenseId);
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }
   
}
