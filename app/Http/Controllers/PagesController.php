<?php

namespace App\Http\Controllers;

use App\CMSPages;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CMSPages::where(function($q) use ($filter){
                $q->where('page_title','like', '%' . $filter . '%')
                    ->orWhere('page_slug','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data2['page_title'] = trim($data['page_title']);
        $data2['page_slug'] = trim($data['page_slug']);
        $data2['page_description'] = trim($data['page_description']);
        $data2['meta_title'] = trim($data['meta_title']);
        $data2['meta_description'] = trim($data['meta_description']);
        $data2['meta_tags'] = trim($data['meta_tags']);
        $data2['is_active'] = trim($data['is_active']);

        $data2['updated_by'] = Auth::user()->id;

        // if(!isset($data['id']))
        //     $data['id'] = 0;
        // $rules = [
        //     'page_title' => "required",
        //     'page_slug' => "required|unique:cms_pages,page_slug,".$data['id']
        // ];

        // $validator = Validator::make($request->except('token'), $rules);
        // if ($validator->fails()) {
        //     return response()->json( ["error"=>$validator->messages(),"status"=>"error", "msg"=>"invalidate" ], 400);
        // }

        if(isset($data['id'])){
            $CMSPages = CMSPages::where('id', $data['id'])->first();
            if($CMSPages) {
                $CMSPages->update($data2);
            }
        }else{
            $data2['created_by'] = Auth::user()->id;
            $CMSPages = CMSPages::create($data2);
            $data['id'] = $CMSPages->id;
        }
        $CMSPages = CMSPages::where('id', $data['id'])->first();

        return response()->json([ 'data' => $CMSPages, 'message' => 'Record stored successfully!'], 200);
    }

    public function show($id)
    {
        $data = CMSPages::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function destroy($id)
    {
        $CMSPages = CMSPages::where('id', $id)->first();
        $CMSPages->delete();
        return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
    }

    public function pageStatus(Request $request) {
        $page_id = $request->page_id;
        $page = CMSPages::where('id', $page_id)->first();
        if($page){
            if($page->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $page->update($data);
            return response()->json([ 'data' => $page, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }
}
