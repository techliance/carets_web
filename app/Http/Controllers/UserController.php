<?php


namespace App\Http\Controllers;

use App\AdCards;
use App\AdIndustries;
use App\AdPayments;
use App\Ads;
use App\CampaignDetails;
use App\Campaigns;
use App\CampaignsAds;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Resource;
use App\Rules\MatchOldPassword;
use App\SearchCaretByDate;
use App\SearchCaretByLike;
use App\SearchVideoByDate;
use App\SearchVideoByLike;
use App\User;
use App\UserBlocks;
use App\UserFavEffects;
use App\UserFavHashtags;
use App\UserFavSounds;
use App\UserFavVideos;
use App\UserFollows;
use App\UserPrivacy;
use App\UserProfiles;
use App\UserReports;
use App\Videos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class UserController extends Controller

{


    function __construct()
    {
        $this->middleware('permission:permission-list', ['only' => ['index']]);
        $this->middleware('permission:permission-create', ['only' => ['create','store']]);
        $this->middleware('permission:permission-edit', ['only' => ['edit','update', 'show']]);
        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);;
        //$this->userRepository = $userRepository;
    }

    public function index(Request $request)

    {
        $userRepository = new User();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllAdmins($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllAdmins($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllAdmins($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllAdmins($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Users retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function listing(Request $request)
    {
        $userRepository = new User();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllUsers($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllUsers($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllUsers($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllUsers($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Users retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function advertisersListing(Request $request)
    {
        $userRepository = new User();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllAdvertiser($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllAdvertiser($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllAdvertiser($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllAdvertiser($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Users retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function clmUsersListing(Request $request)
    {
        $userRepository = new User();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllClmUser($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $userRepository->getFilteredAndOrderByAttrAllClmUser($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllClmUser($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $userRepository->getFilteredAndOrderByAttrAllClmUser($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Users retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $roles = Role::pluck('name','name')->all();

        return view('users.create',compact('roles'));

    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    {
        //return $request->input('roles');
        $userRepository = new User();
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm_password',
            'roles' => 'required'
        ];
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'username' => $request->name,
            'phone' => $request->phone,
            'is_active' => $request->is_active,
            'roles' => $request->roles
        ];


        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->messages(),"status"=>"error", "msg"=>"invalidate" ], 400);
        }
        //$input = $request->all();
        $data['password'] = Hash::make($request->input('password'));

        $user = $userRepository->create($data);
        $user->assignRole($request->input('roles'));


        $newdata = [
            'user_id'=>$user->id,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'user_bio'=>$request->user_bio,
            'dob'=>$request->dob,
            'gender'=>$request->gender

        ];
        $input['image'] = $request->input('image');
        if($input['image']) {
            $imageData = $input['image'];
            $file       = $imageData['file'];
            $file_name  = $imageData['name'];
            $image = file_get_contents($file);

            $file_path =  'UserProfiles/' . $user->id . '_' . $file_name;
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            if ($confirmUpload){
                $newdata['user_photo'] = $file_path;
            }
        }
        $UserProfiles = UserProfiles::create($newdata);

        // $user->assignRole(['manager']);
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('user', $user, 'User has been created successfully!');
        }
        return redirect()->route('users.index')

            ->with('success','User created successfully');

    }

    public function storeAdvertiser(Request $request)
    {

        $userRepository = new User();
        if($request->user_id){
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->user_id,
                'password' => 'same:confirm_password'
            ];
        }else{
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|same:confirm_password'
            ];
            $data['password'] = Hash::make($request->input('password'));
        }
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'username' => $request->name,
            'is_active' => $request->is_active
        ];


        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->messages(),"status"=>"error", "msg"=>"invalidate" ], 400);
        }
        //$input = $request->all();
        if($request->user_id){
            $user = User::where('id', $request->user_id)->first();
            if($user) {
                $user->update($data);
            }
        }else{
            $user = $userRepository->create($data);
            $user->assignRole(['advertiser']);
        }

        $profileData = $request->profile;

        $newdata = [
            'user_id'=>$user->id,
            'first_name'=>$profileData['first_name'],
            'last_name'=>$profileData['last_name'],
            'user_bio'=>$profileData['user_bio'],
            'industry_id'=>$profileData['industry_id'],
            'business_name'=>$profileData['business_name'],
            'phone_number'=>$request->phone

        ];
        $input['image'] = $request->input('image');
        if($input['image']) {
            $imageData = $input['image'];
            $file       = $imageData['file'];
            $file_name  = $imageData['name'];
            $image = file_get_contents($file);

            $file_path =  'UserProfiles/' . $user->id . '_' . $file_name;
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            if ($confirmUpload){
                $newdata['user_photo'] = $file_path;
            }
        }
         //$input = $request->all();
         if($request->user_id){
            $UserProfiles = UserProfiles::where('user_id', $request->user_id)->first();
            $UserProfiles->update($newdata);
        }else{
            $UserProfiles = UserProfiles::create($newdata);
        }



        // $user->assignRole(['manager']);
        if( $request->is('api/*')){
            if($request->user_id){
                return $this->sendSuccessResponse('user', $user, 'User updated successfully!');
            }else{
                return $this->sendSuccessResponse('user', $user, 'User has been created successfully!');
            }


        }
        return redirect()->route('users.index')

            ->with('success','User created successfully');

    }

    public function updateAdvertiser(Request $request)
    {


       $profileData = $request->profile;

       $newdata = [
            'user_id'=>$profileData['user_id'],
            'first_name'=>$profileData['first_name'],
            'last_name'=>$profileData['last_name'],
            'user_bio'=>$profileData['user_bio'],
            'industry_id'=>$profileData['industry_id'],
            'business_name'=>$profileData['business_name'],
            'phone_number'=>$profileData['phone_number'],

        ];

        $UserProfiles = UserProfiles::where('user_id', $request->user_id)->first();
        $UserProfiles->update($newdata);


        // $user->assignRole(['manager']);
        if( $request->is('api/*')){
            if($request->user_id){
                return $this->sendSuccessResponse('user', $request, 'User updated successfully!');
            }else{
                return $this->sendSuccessResponse('user', $request, 'User has been created successfully!');
            }


        }
        return redirect()->route('users.index')

            ->with('success','User created successfully');

    }
    public function storeClmUser(Request $request)
    {

        $userRepository = new User();
        if($request->user_id){
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->user_id,
                'password' => 'same:confirm_password'
            ];
        }else{
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|same:confirm_password'
            ];
            $data['password'] = Hash::make($request->input('password'));
        }
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'username' => $request->name,
            'is_active' => $request->is_active
        ];


        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->messages(),"status"=>"error", "msg"=>"invalidate" ], 400);
        }
        //$input = $request->all();
        if($request->user_id){
            $user = User::where('id', $request->user_id)->first();
            if($user) {
                $user->update($data);
            }
        }else{
            $user = $userRepository->create($data);
            $user->assignRole(['clm']);
        }

        $profileData = $request->profile;

        $newdata = [
            'user_id'=>$user->id,
            'first_name'=>$profileData['first_name'],
            'last_name'=>$profileData['last_name'],
            'user_bio'=>$profileData['user_bio'],
            'industry_id'=>$profileData['industry_id'],
            'business_name'=>$profileData['business_name'],
            // 'phone_number'=>$request->phone
             'phone_number' => $request->phone ?? ''

        ];
        $input['image'] = $request->input('image');
        if($input['image']) {
            $imageData = $input['image'];
            $file       = $imageData['file'];
            $file_name  = $imageData['name'];
            $image = file_get_contents($file);

            $file_path =  'UserProfiles/' . $user->id . '_' . $file_name;
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            if ($confirmUpload){
                $newdata['user_photo'] = $file_path;
            }
        }
         //$input = $request->all();
         if($request->user_id){
            $UserProfiles = UserProfiles::where('user_id', $request->user_id)->first();
            $UserProfiles->update($newdata);
        }else{
            $UserProfiles = UserProfiles::create($newdata);
        }



        // $user->assignRole(['manager']);
        if( $request->is('api/*')){
            if($request->user_id){
                return $this->sendSuccessResponse('user', $user, 'User updated successfully!');
            }else{
                return $this->sendSuccessResponse('user', $user, 'User has been created successfully!');
            }


        }
        return redirect()->route('users.index')

            ->with('success','User created successfully');

    }

    public function updateClmUser(Request $request)
    {


       $profileData = $request->profile;

       $newdata = [
            'user_id'=>$profileData['user_id'],
            'first_name'=>$profileData['first_name'],
            'last_name'=>$profileData['last_name'],
            'user_title'=>$profileData['user_title'],
            'user_bio'=>$profileData['user_bio'],
            'industry_id'=>$profileData['industry_id'],
            'business_name'=>$profileData['business_name'],
            'phone_number'=>$profileData['phone_number'],

        ];

        $UserProfiles = UserProfiles::where('user_id', $request->user_id)->first();
        $UserProfiles->update($newdata);


        // $user->assignRole(['manager']);
        if( $request->is('api/*')){
            if($request->user_id){
                return $this->sendSuccessResponse('user', $request, 'User updated successfully!');
            }else{
                return $this->sendSuccessResponse('user', $request, 'User has been created successfully!');
            }


        }
        return redirect()->route('users.index')

            ->with('success','User created successfully');

    }

    public function getIndustry(Request $request){
        $filter = $request->input('filter');
        $users = AdIndustries::orderBy('title','ASC')
        ->select('id as value', 'title as label')
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show(Request $request, $id)

    {
        $userRepository = new User();
        // $user = $userRepository->userDetail($id);
        $user = $userRepository->with(['profile' => function($q) {
            $q->select([
                '*',
                DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo"),
                DB::raw("CONCAT('".config('app.awsurl')."', user_video) AS user_video")
            ]);
        }])->findOrFail($id);
        $roles = $user->roles->map(function ($item, $key) {
            return [
                'value' => $item->id,
                'label' => $item->name,
            ];
        });
        $current_roles = $user->roles->map(function ($item, $key) {
            return $item->id;
        });
        unset($user->roles);
        $user->roles = $current_roles;
        $user->currentRolesById = $current_roles;
        $user->profile =  $user->profile;


        $user->first_name =  $user->profile->first_name;
        $user->last_name =  $user->profile->last_name;
        $user->user_bio =  $user->profile->user_bio;
        $user->dob =  $user->profile->dob;
        $user->user_bio =  $user->profile->user_bio;
        $user->business_name =  $user->profile->business_name;
        $user->industry_id =  $user->profile->industry_id;




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('user', $user, 'User retrieved successfully!');
        }
        return view('users.show',compact('user'));

    }

    public function profile(Request $request, $id)

    {
        $userRepository = new User();
        $user = $userRepository->userDetail($id);

        $user->profile =  $user->profile;


        $user->first_name =  $user->profile->first_name;
        $user->last_name =  $user->profile->last_name;
        $user->user_bio =  $user->profile->user_bio;
        $user->dob =  $user->profile->dob;
        $user->user_bio =  $user->profile->user_bio;
        $user->business_name =  $user->profile->business_name;
        $user->industry_id =  $user->profile->industry_id;




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('user', $user, 'User retrieved successfully!');
        }
        return view('users.show',compact('user'));

    }

    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $user = User::find($id);

        $roles = Role::pluck('name','name')->all();

        $userRole = $user->roles->pluck('name','name')->all();


        return view('users.edit',compact('user','roles','userRole'));

    }


    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */
    public function updatePassword(Request $request){
       // return $request->all();
        $rules = [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|min:8',
            'new_confirm_password' => ['same:new_password'],
        ];

        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {


            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "msg"=>"invalidate" ], 400);
        }
        if($request->user_id){
            $user = User::find($request->user_id)->update(['password'=> Hash::make($request->new_password)]);
        }else{
            $user = User::find(auth('api')->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        }

        return $this->sendSuccessResponse('data', '', 'Password updated successfully!');

    }

    public function changePassword(Request $request){
        // return $request->all();
         $rules = [
             'current_password' => ['required', new MatchOldPassword],
             'new_password' => 'required|min:8',
             'new_confirm_password' => ['same:new_password'],
         ];

         $validator = Validator::make($request->except('token'), $rules);
         if ($validator->fails()) {


             return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "msg"=>"invalidate" ], 400);
         }
         if($request->user_id){
             $user = User::find($request->user_id)->update(['password'=> Hash::make($request->new_password)]);
         }else{
             $user = User::find(auth('api')->user()->id)->update(['password'=> Hash::make($request->new_password)]);
         }

         return $this->sendSuccessResponse('data', '', 'Password updated successfully!');

     }

    public function update(Request $request, $id)
    {
        $profile = $request->profile;
        $userRepository = new User();
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm_password',
            'roles' => 'required'
        ];

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'username' => $request->name,
            'phone' => $request->phone,
            'is_active' => $request->is_active,
            'password' => $request->password
        ];

        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->messages(),"status"=>"error", "msg"=>"invalidate" ], 400);
        }
        // $input = $request->only(['name', 'email', 'password', 'is_active']);
        // $imageData = $request->input('image');

        if(!empty($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }else{
            $data = Arr::except($data,['password']);
        }
        $user = User::find($id);
        $user->fill($data)->save();

        $userRepository->deleteFromModelHasRoles($id);

        // $user = User::find($id);
        $user = $userRepository->userDetail($id);
        $user->syncRoles($request->roles);


        $newdata = [
            'first_name'=> $profile['first_name'],
            'last_name'=>$profile['last_name'],
            'user_bio'=>$profile['user_bio'],
            'dob'=>$profile['dob'],
            'gender'=>$profile['gender']

        ];
        $input['image'] = $request->input('image');
        if($input['image']) {
            $imageData = $input['image'];
            $file       = $imageData['file'];
            $file_name  = $imageData['name'];
            $image = file_get_contents($file);

            $file_path =  'UserProfiles/' . $user->id . '/' . $file_name;
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
            if ($confirmUpload){
                $newdata['user_photo'] = $file_path;
            }
        }
        $UserProfiles = UserProfiles::where('user_id',$id)->update($newdata);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('user', $user, 'User has been updated successfully!');
        }
        return redirect()->route('users.index')
            ->with('success','User updated successfully');
    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */
    public function status(Request $request) {
        $data = $request->all();
        $user_id = $data['user_id'];
        $User = User::where('id', $user_id)->first();
        if($User){
            if($User->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $User->update($data);
            return response()->json([ 'data' => $User,"status"=>"success", 'message' => 'Status updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }





    public function destroy(Request $request, $id)
    {
        //$userRepository = new User();
        if($id == 1){
            return response()->json( ["error"=>"Can't delete super admin","status"=>"error", "message"=>"Can't delete super admin" ], 400);
        }
        $user = User::where('id', $id)->first();
        if($user){
            $UserProfiles = UserProfiles::where('user_id', $id)->first();
            if($UserProfiles){
                $UserProfiles->delete();
            }
            $user->myvideos()->delete();
            $user->delete();
            if( $request->is('api/*')){
                return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
            }
            return redirect()->route('users.index')->with('success','User deleted successfully');
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function deleteAdvertiser(Request $request, $id)
    {
        //$userRepository = new User();
        $user = User::where('id', $id)->first();
        if($user){



            $videos = $user->userVideos()->pluck('video_id');
            Videos::where('is_caret',0)->whereIn('id',$videos->toArray())->delete();

            AdPayments::where('user_id',$id)->delete();
            //AdCards::where('user_id',$id)->delete();

            $campaigns = $user->campaigns()->pluck('id');
            CampaignsAds::whereIn('campaign_id',$campaigns->toArray())->delete();
            CampaignDetails::whereIn('campaign_id',$campaigns->toArray())->delete();
            Ads::where('user_id',$id)->delete();
            Campaigns::where('user_id',$id)->delete();

            $UserProfiles = UserProfiles::where('user_id', $id)->first();
            if($UserProfiles){
                $UserProfiles->delete();
            }

            $user->delete();

            if( $request->is('api/*')){
                return $this->sendSuccessResponse('data', '', 'User has been deleted successfully!');
            }
            return redirect()->route('users.index')->with('success','User deleted successfully');
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function deleteClmUser(Request $request, $id)
    {
        //$userRepository = new User();
        $user = User::where('id', $id)->first();
        if($user){



            $videos = $user->userVideos()->pluck('video_id');
            Videos::where('is_caret',0)->whereIn('id',$videos->toArray())->delete();

            AdPayments::where('user_id',$id)->delete();
            //AdCards::where('user_id',$id)->delete();

            $campaigns = $user->campaigns()->pluck('id');
            CampaignsAds::whereIn('campaign_id',$campaigns->toArray())->delete();
            CampaignDetails::whereIn('campaign_id',$campaigns->toArray())->delete();
            Ads::where('user_id',$id)->delete();
            Campaigns::where('user_id',$id)->delete();

            $UserProfiles = UserProfiles::where('user_id', $id)->first();
            if($UserProfiles){
                $UserProfiles->delete();
            }

            $user->delete();

            if( $request->is('api/*')){
                return $this->sendSuccessResponse('data', '', 'User has been deleted successfully!');
            }
            return redirect()->route('users.index')->with('success','User deleted successfully');
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function deleteUser(Request $request, $id)
    {
        //$userRepository = new User();

        $user = User::where('id', $id)->first();
        if($user){
            $UserProfiles = UserProfiles::where('user_id', $id)->first();
            if($UserProfiles){
                $UserProfiles->delete();
            }
            $user->delete();
            $videos = $user->userVideos()->pluck('video_id');
            Videos::where('is_caret',0)->whereIn('id',$videos->toArray())->delete();

            SearchVideoByDate::whereIn('video_id',$videos->toArray())->delete();
            SearchVideoByLike::whereIn('video_id',$videos->toArray())->delete();
            SearchCaretByDate::whereIn('video_id',$videos->toArray())->delete();
            SearchCaretByLike::whereIn('video_id',$videos->toArray())->delete();




            if( $request->is('api/*')){
                return $this->sendSuccessResponse('data', '', 'User has been deleted successfully!');
            }
            return redirect()->route('users.index')->with('success','User deleted successfully');
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function viewProfile($id)
    {
        $UserProfiles = User::where('id', $id)->with(['profile'=>function($q){
            $q->select([
                '*',
                DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")
            ]);
        }])->first();
        if($UserProfiles){
            return response()->json([ 'data' => $UserProfiles,"status"=>"success", 'message' => 'Retrieved successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Information","status"=>"error", "message"=>"invalidate" ], 400);
        }


    }

    public function uniqueUserName(Request $request) {

        $data = $request->all();
        if($data['username']!=""){
            $User = User::where('username', $data['username'])->first();
            if($User){
                return response()->json( ["error"=>"Username already taken","status"=>"error", "msg"=>"invalidate" ], 400);
            }else{
                return response()->json(['data' => 'available', 'status'=>'success','message' => 'available'], 200);
            }
        }
    }

    public function uniqueEmail(Request $request) {

        $data = $request->all();
        if($data['email']!=""){
            $User = User::where('email', $data['email'])->first();
            if($User){
                return response()->json( ["error"=>"Email already taken","status"=>"error", "msg"=>"invalidate" ], 400);
            }else{
                return response()->json(['data' => 'available', 'status'=>'success','message' => 'available'], 200);
            }
        }
    }



    public function saveProfile(Request $request) {

        $data = $request->all();
        $validation_rules = [
            'user_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $token = $request->bearerToken();
        $data2['user_id'] = trim($data['user_id']);
        $data2['first_name'] = $data['first_name']?trim($data['first_name']):null;
        //$data2['middle_name'] = trim($data['middle_name']);
        $data2['last_name'] = $data['last_name']?trim($data['last_name']):null;
        //$data2['user_title'] = trim($data['user_title']);
        $data2['user_bio'] = $data['user_bio']?trim($data['user_bio']):null;
        $data2['dob'] = $data['dob']?trim($data['dob']):null;
        // $data2['user_photo'] = trim($data['user_photo']);
        // $data2['user_video'] = trim($data['user_video']);

        $data2['gender'] = isset($data['gender'])?trim($data['gender']):null;

        if(isset($data['username'])){
            $validation_rules = [
                'username' => 'required|min:5|unique:users'
            ];
            $validator = Validator::make($data, $validation_rules);
            if ($validator->fails()) {
                return response()->json( ["error"=>$validator->errors()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }
            User::where('id', $data2['user_id'])->update(['username'=>$data['username']]);
        }
        $existing_profile = UserProfiles::where('user_id', $data2['user_id'])->first();
        if($existing_profile) {
            $existing_profile->update($data2);
        }else{
            $existing_profile = UserProfiles::create($data2);
        }

        if ($request->hasFile('user_photo')) {
            $this->uploadImage($existing_profile->user_id,$request->file('user_photo'));
        }

        $this->addUpdateFireStore($data2['user_id']);
        $user = User::where('id',$data2['user_id'])->with(['profile'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        }])
        ->first();
        return response([
            'data'=> $user,
            'status' => 'success',
            'token' => $token
        ], 200)
            ->header('Access-Control-Expose-Headers', 'Authorization')
            ->header('Authorization', $token);

    }

    public function uploadImage($user_id,$input_img) {

        if ($input_img) {
            $image = $input_img;
            $name = time().'.'.$image->getClientOriginalExtension();
            $UserProfiles = UserProfiles::where('user_id', $user_id)->first();

            $file_path =  'UserProfiles/' . $user_id . '_' . $name;
            $image = file_get_contents($image);
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');

            if($UserProfiles->user_photo) {
               $imagest =  Storage::disk('s3')->delete($UserProfiles->user_photo);
            }

            $UserProfiles->fill(['user_photo' => $file_path]);
            $UserProfiles->save();
            return response()->json(['data' => 'available', 'status'=>'success','message' => 'uploaded'], 200);
        }

        return response()->json(['status'=>'error','error' => 'Image does not uploaded properly please try again.'], 400);
    }

    public function viewPrivacy($id){

        $UserPrivacy = UserPrivacy::where('user_id', $id)->first();
        if($UserPrivacy){
            return response()->json([ 'data' => $UserPrivacy, 'message' => 'Stored successfully!'], 200);
        }else{
            return response()->json( ["error"=>"No Record Found","status"=>"error", "msg"=>"invalidate" ], 400);
        }

    }

    public function savePrivacy(Request $request) {

        $data = $request->all();
        $validation_rules = [
            'user_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['user_id'] = trim($data['user_id']);

        if(isset($data['private_account']))
        $data2['private_account'] = trim($data['private_account']);

        if(isset($data['sugest_others']))
        $data2['sugest_others'] = trim($data['sugest_others']);

        if(isset($data['ad_auth']))
        $data2['ad_auth'] = trim($data['ad_auth']);

        if(isset($data['download_video']))
        $data2['download_video'] = trim($data['download_video']);

        if(isset($data['likes']))
        $data2['likes'] = trim($data['likes']);

        if(isset($data['comments']))
        $data2['comments'] = trim($data['comments']);

        if(isset($data['followers']))
        $data2['followers'] = trim($data['followers']);

        if(isset($data['mentions']))
        $data2['mentions'] = trim($data['mentions']);

        if(isset($data['direct_messages']))
        $data2['direct_messages'] = trim($data['direct_messages']);

        if(isset($data['video_from']))
        $data2['video_from'] = trim($data['video_from']);

        if(isset($data['video_suggestion']))
        $data2['video_suggestion'] = trim($data['video_suggestion']);

        $existing_profile = UserPrivacy::where('user_id', $data2['user_id'])->first();
        if($existing_profile) {
            $existing_profile->update($data2);
        }else{
            $user = UserPrivacy::create($data2);
        }

        $user = User::where('id',$data2['user_id'])->with('privacy')->first();
        return response()->json([ 'data' => $user, 'message' => 'Stored successfully!'], 200);
    }

    public function addFavEffect(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['effect_id'] = trim($data['effect_id']);
        $UserFavEffects = UserFavEffects::create($data2);
        $UserFavEffects = UserFavEffects::where('user_id',$data2['user_id'])->all();
        return response()->json([ 'data' => $UserFavEffects, 'message' => 'Stored successfully!'], 200);
    }

    public function removeFavEffect(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['effect_id'] = trim($data['effect_id']);
        $UserFavEffects = UserFavEffects::where('user_id',$data2['user_id'])->where('effect_id',$data2['effect_id'])->first();
        $UserFavEffects->delete();
        return response()->json([ 'data' => $UserFavEffects, 'message' => 'Removed successfully!'], 200);
    }

    public function addHTag(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['title'] = trim($data['title']);
        $UserFavHashtags = UserFavHashtags::create($data2);
        $UserFavHashtags = UserFavHashtags::where('user_id',$data2['user_id'])->all();
        return response()->json([ 'data' => $UserFavHashtags, 'message' => 'Stored successfully!'], 200);
    }

    public function removeHtag(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['tag_id'] = trim($data['tag_id']);
        $UserFavHashtags = UserFavHashtags::where('user_id',$data2['user_id'])->where('id',$data2['tag_id'])->first();
        $UserFavHashtags->delete();
        return response()->json([ 'data' => $UserFavHashtags, 'message' => 'Removed successfully!'], 200);
    }

    public function addFavSound(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['sound_id'] = trim($data['sound_id']);
        $UserFavSounds = UserFavSounds::create($data2);
        $UserFavSounds = UserFavSounds::where('user_id',$data2['user_id'])->all();
        return response()->json([ 'data' => $UserFavSounds, 'message' => 'Stored successfully!'], 200);
    }

    public function removeFavSound(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['sound_id'] = trim($data['sound_id']);
        $UserFavSounds = UserFavSounds::where('user_id',$data2['user_id'])->where('sound_id',$data2['sound_id'])->first();
        $UserFavSounds->delete();
        return response()->json([ 'data' => $UserFavSounds, 'message' => 'Removed successfully!'], 200);
    }

    public function addFavVideo(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['video_id'] = trim($data['video_id']);
        $UserFavVideos = UserFavVideos::create($data2);
        $UserFavVideos = UserFavVideos::where('user_id',$data2['user_id'])->all();
        return response()->json([ 'data' => $UserFavVideos, 'message' => 'Stored successfully!'], 200);
    }

    public function removeFavVideo(Request $request) {
        $data = $request->all();
        $data2['user_id'] = trim($data['user_id']);
        $data2['video_id'] = trim($data['video_id']);
        $UserFavVideos = UserFavVideos::where('user_id',$data2['user_id'])->where('video_id',$data2['video_id'])->first();
        $UserFavVideos->delete();
        return response()->json([ 'data' => $UserFavVideos, 'message' => 'Removed successfully!'], 200);
    }
    ##Block/UnBlock user
    public function reportUser(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'report_by' => 'required|int',
            'report_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['report_by'] = trim($data['report_by']);
        $data2['report_to'] = trim($data['report_to']);
        $data2['comments'] = trim($data['comments']);
        $existing = UserReports::where('report_to',$data2['report_to'])->where('report_by',$data2['report_by'])->first();
        if(!$existing){
            $UserReports = UserReports::create($data2);
            // User::where('id',$data2['report_by'])->increment('report_to_count' , 1);
            // User::where('id',$data2['report_to'])->increment('report_by_count' , 1);

            //$device_tokens = User::where('id',$data['report_to'])->get()->pluck('device_token')->toArray();
            // Helper::sendFCMNotification($device_tokens, ['title' => 'New report', 'body' => 'New report',
            // 'data' => [
            //     'title' => 'New Report', 'body' => 'New Report'
            //     ]
            // ]);
            $UserReports = UserReports::where('report_to',$data2['report_to'])->where('report_by',$data2['report_by'])->first();
            return response()->json([ 'data' => 'Reported successfully!', 'message' => 'Reported successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Already Reported","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function blockUser(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'block_by' => 'required|int',
            'block_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['block_by'] = trim($data['block_by']);
        $data2['block_to'] = trim($data['block_to']);

        $existing = UserBlocks::where('block_to',$data2['block_to'])->where('block_by',$data2['block_by'])->first();
        if(!$existing){
            $UserBlocks = UserBlocks::create($data2);
            // User::where('id',$data2['block_by'])->increment('block_to_count' , 1);
            // User::where('id',$data2['block_to'])->increment('block_by_count' , 1);

            // $device_tokens = User::where('id',$data['block_to'])->get()->pluck('device_token')->toArray();
            // Helper::sendFCMNotification($device_tokens, ['title' => 'New Block', 'body' => 'New Block',
            // 'data' => [
            //     'title' => 'New Block', 'body' => 'New Block'
            //     ]
            // ]);
            $UserBlocks = UserBlocks::where('block_to',$data2['block_to'])->where('block_by',$data2['block_by'])->first();
            return response()->json([ 'data' => $UserBlocks, 'message' => 'Stored successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Already blocked","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function isBlocked(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'block_by' => 'required|int',
            'block_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }


        $data2['block_by'] = trim($data['block_by']);
        $data2['block_to'] = trim($data['block_to']);
        $existing = UserBlocks::where('block_to',$data2['block_to'])->where('block_by',$data2['block_by'])->first();
        if($existing){
            return response()->json([ 'data' => $existing, 'message' => 'Found successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Not Blocked","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function unBlock(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'block_by' => 'required|int',
            'block_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }


        $data2['block_by'] = trim($data['block_by']);
        $data2['block_to'] = trim($data['block_to']);
        $UserBlocks = UserBlocks::where('block_to',$data2['block_to'])->where('block_by',$data2['block_by'])->first();

        if($UserBlocks){
            $UserBlocks->delete();
            // User::where('id',$data2['block_by'])->decrement('block_to_count' , 1);
            // User::where('id',$data2['block_to'])->decrement('block_by_count' , 1);
            return response()->json([ 'data' => $UserBlocks, 'message' => 'Removed successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Not Blocked","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function blockList(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $user_id = $request->input('user_id', null);
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }
        $data =  User::with(['profile'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        }])
        ->whereHas('blockby',function($q) use($user_id){
            $q->where('block_by',$user_id);
        })
        ->where(function($q) use ($filter){
            $q->where('id','like', '%' . $filter . '%')
                ->orWhere('username','like', '%' . $filter . '%');
        })
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->paginate($pagination);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }
    ##
    public function addFollow(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'follow_by' => 'required|int',
            'follow_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['follow_by'] = trim($data['follow_by']);
        $data2['follow_to'] = trim($data['follow_to']);
        $existing = UserFollows::where('follow_to',$data2['follow_to'])->where('follow_by',$data2['follow_by'])->first();
        if(!$existing){
            $UserFollows = UserFollows::create($data2);
            User::where('id',$data2['follow_by'])->increment('follow_to_count' , 1);
            User::where('id',$data2['follow_to'])->increment('follow_by_count' , 1);

            $device_tokens = User::where('id',$data['follow_to'])->get()->pluck('device_token')->toArray();
            if($device_tokens){
                Helper::sendFCMNotification($device_tokens, ['title' => 'New Follower', 'body' => 'New Follower',
                'data' => [
                    'title' => 'New Follower', 'body' => 'New Follower'
                    ]
                ]);
            }
            $UserFollows = UserFollows::where('follow_to',$data2['follow_to'])->where('follow_by',$data2['follow_by'])->first();
            return response()->json([ 'data' => $UserFollows, 'message' => 'Stored successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Already following","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function isFollow(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'follow_by' => 'required|int',
            'follow_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }


        $data2['follow_by'] = trim($data['follow_by']);
        $data2['follow_to'] = trim($data['follow_to']);
        $existing = UserFollows::where('follow_to',$data2['follow_to'])->where('follow_by',$data2['follow_by'])->first();
        if($existing){
            return response()->json([ 'data' => $existing, 'message' => 'Found successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Not following","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function unFollow(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'follow_by' => 'required|int',
            'follow_to' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }


        $data2['follow_by'] = trim($data['follow_by']);
        $data2['follow_to'] = trim($data['follow_to']);
        $UserFollows = UserFollows::where('follow_to',$data2['follow_to'])->where('follow_by',$data2['follow_by'])->first();

        if($UserFollows){
            $UserFollows->delete();
            User::where('id',$data2['follow_by'])->decrement('follow_to_count' , 1);
            User::where('id',$data2['follow_to'])->decrement('follow_by_count' , 1);
            return response()->json([ 'data' => $UserFollows, 'message' => 'Removed successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Not following","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    public function followers(Request $request)
    {
        $filter = $request->input('filter', null); // Default to null if not provided
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $user_id = $request->input('user_id', null);
        $pagination = $request->input('pageSize') ?? 20;

        if ($sort) {
            $sortEx = explode(',', $sort);
            $sortName = $sortEx[0];
            $orderByEx = explode(":", $sortEx[1]);
            $orderType = $orderByEx[0] === 'desc' && $orderByEx[1] === 'true' ? 'desc' : 'asc';
        }

        $data =   User::select('id', 'email', 'username', 'name', 'phone','firestore_reference_id', 'device_token', 'device_id') // Select specific fields
        ->with(['profile'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        }])
            ->withCount([
                'followby as isFollowing' => function ($q) use ($user_id) {
                    $q->where('follow_by', $user_id)->limit(1);
                }
            ])
            ->whereHas('followto', function ($q) use ($user_id) {
                $q->where('follow_to', $user_id);
            })
            ->when($filter, function ($q) use ($filter) {
                $q->where(function ($q) use ($filter) {
                    $q->where('id', 'like', '%' . $filter . '%')
                        ->orWhere('username', 'like', '%' . $filter . '%');
                });
            })
            ->when($sortName || $orderType, function ($query) use ($sortName, $orderType) {
                $query->orderBy($sortName, $orderType);
            })
            ->when(!$sortName, function ($q) {
                $q->orderBy('created_at', 'desc');
            })
            ->paginate($pagination);

        if ($request->is('api/*')) {
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }

        return view('users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    public function following(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $user_id = $request->input('user_id', null);
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        // ->where(function($q) use ($filter){
        //     $q->where('id','like', '%' . $filter . '%')
        //         ->orWhere('username','like', '%' . $filter . '%');
        // })
        $data =   User::select('id', 'email', 'username', 'name', 'phone','firestore_reference_id', 'device_token', 'device_id') // Select specific fields
        ->with(['profile'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        }])

        ->withCount([
            'followby as isFollowing'=>function($q) use($user_id){
                $q->where('follow_by',$user_id)->limit(1);
            }])
        ->whereHas('followby',function($q) use($user_id){
            $q->where('follow_by',$user_id);
        })


        ->when($filter, function ($q) use ($filter) {
            $q->where(function ($q) use ($filter) {
                $q->where('id', 'like', '%' . $filter . '%')
                    ->orWhere('username', 'like', '%' . $filter . '%');
            });
        })
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->paginate($pagination);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }


    #ADMIN END
    public function updateStatus($userId, Request $request) {
        $user = User::find($userId);
        $user->is_active = $request->input('is_active');
        if ($user->save() ) {
            return $this->sendSuccessResponse('user', $user, 'User status updated successfully!');
        }
    }

    public function blocked($userId, Request $request) {
        $user = User::find($userId);
        $user->is_blocked = $request->input('is_blocked');
        if ($user->save() ) {
            return $this->sendSuccessResponse('user', $user, 'User blocked successfully!');
        }
    }

    public function addUpdateFireStore($user_id) {
        $user = User::where('id',$user_id)->with(['profile'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        }])->first();
        $firestore = Helper::getFireStoreConnection();
        $collectionReference = $firestore->collection('users');

        //Create user on firestore
        if(isset($user->firestore_reference_id) && $user->firestore_reference_id != ''){

            $data = [
                'device_token' => (string) isset($user->device_token) ? $user->device_token : null,
                'device_id' => (string) isset($user->device_id) ? $user->device_id : null,
                'device_type' => (string) isset($user->device_type) ? $user->device_type : null,
                'device_model' => (string) isset($user->device_model) ? $user->device_model : null,
                'device_os' => (string) isset($user->device_os) ? $user->device_os : null,
                'username' => (string) isset($user->username) ? $user->username : null,
                'first_name' => (string) isset($user->profile->first_name) ? $user->profile->first_name : null,
                'last_name' => (string) isset($user->profile->last_name) ? $user->profile->last_name : null,
                'user_photo'=> (string) isset($user->profile->user_photo) ? $user->profile->user_photo : null,
                'firestore_reference_id'=> (string) isset($user->firestore_reference_id) ? $user->firestore_reference_id : null,
            ];

            $documentReference = $collectionReference->document($user->firestore_reference_id);
            if($documentReference) {
                $snapshot = $documentReference->set($data, ['merge' => true]);
            }else{
                return response()->json( ["error"=>"Invalid firestore refrence ID","status"=>"error", "msg"=>"Invalid firestore refrence ID" ], 400);
            }
            $userId = $user->firestore_reference_id;
        }else{
            $data = [
                'device_token' => (string) isset($user->device_token) ? $user->device_token : null,
                'device_id' => (string) isset($user->device_id) ? $user->device_id : null,
                'device_type' => (string) isset($user->device_type) ? $user->device_type : null,
                'device_model' => (string) isset($user->device_model) ? $user->device_model : null,
                'device_os' => (string) isset($user->device_os) ? $user->device_os : null,
                'username' => (string) isset($user->username) ? $user->username : null,
                'first_name' => (string) isset($user->profile->first_name) ? $user->profile->first_name : null,
                'last_name' => (string) isset($user->profile->last_name) ? $user->profile->last_name : null,
                'user_photo'=> (string) isset($user->profile->user_photo) ? $user->profile->user_photo : null,
                'seen' => '0',
                'status' => 'offline',
                'user_id' => $user->id
            ];
            $snapshot = $collectionReference->add($data);
            $userId = $snapshot->id();
            $user->firestore_reference_id = $userId;
            $user->save();
            $this->addUpdateFireStore($user->id);
        }
        return $userId;
    }
}
