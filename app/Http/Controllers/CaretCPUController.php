<?php
namespace App\Http\Controllers;
use App\Ads;
use App\Ages;
use App\Carets;
use App\CLMLicense;
use App\Hashs;
use App\Helpers\Helper;
use App\Jobs\ProcessCaretJob;
use App\Jobs\ProcessUploadVideoJob;
use App\Jobs\ProcessVideoJob;
use App\Sounds;
use App\SystemSettings;
use App\User;
use App\VideoAds;
use App\VideoCategories;
use App\VideoCommentLikes;
use App\VideoComments;
use App\VideoLikes;
use App\VideoReports;
use App\Videos;
use App\VideoSplash;
use App\VideoViews;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Exception;
class CaretCPUController extends Controller
{
    public function __construct() {
        $this->caret_videos = [];
        $this->caret_start_time = 0;
        $this->caret_end_time = 0;
        $this->iteration = 0;
        $this->resultant_videos = [];
        $this->scale_videos = [];
        $this->videos_to_concatenate = [];
        // for 2nd algo
        $this->result = null;
        $this->min_duration = null;
        $this->last_clip_end = 0;
        $this->longest_duration = 0;
        $this->output_path = storage_path('app/caret_videos/');
        $this->converted_path = storage_path('app/uploads/Videos/');
        $this->watermark_path = storage_path('app/uploads/Videos/watermark.png');
        $this->base_path = storage_path('app/');
    }
    public function commandTest(Request $request) {
        $collection = collect([
            [
                'id' => 1,
                'title' => 'a.mp4',
                'start_time' => Carbon::parse('2021-02-17T11:13:20.000000Z'),
                'end_time' => Carbon::parse('2021-02-17T11:13:28.000000Z'),
                'clipping_start'=> '00:00:00',
                'clipping_end'=> '00:00:00',
                'width' => null,
                'height' => null,
                'duration' => 16,
                'path' => 'Videos/a.mp4'
            ],
            [
                'id' => 2,
                'title' => 'b.mp4',
                'start_time' => Carbon::parse('2021-02-17T11:13:25.000000Z'),
                'end_time' => Carbon::parse('2021-02-17T11:13:32.000000Z'),
                'clipping_start'=> '00:00:00',
                'clipping_end'=> '00:00:00',
                'width' => null,
                'height' => null,
                'duration' => 12,
                'path' => 'Videos/b.mp4'
            ],
        ]);
         return $this->caret_based_merging($collection);
   }
   public function time_based_merging($videos) {
        $collage_made = 0;
        foreach($videos as $vIdx => $video) {
            $next_video =  isset($videos[$vIdx +1]) ? $videos[$vIdx +1] : null;
            if($next_video && ($video['end_time']->greaterThanOrEqualTo($next_video['start_time']))) {
                $collage_made += 1;
            }
        }
        if ($collage_made > 0 ) {
            $this->get_videos_for_carets($videos);
            //  $this->set_caret_end_time ($collection);
            $this->scale_videos_ffmpeg();

            return $this->concatenate_videos_ffmpeg();
        } else {
            return null;
        }
    }
    function get_videos_for_carets($videos) {
        $videos_with_dimensions = $this->get_correct_dimension_videos($videos);
        // $transformed_videos = $this->transform_landscape_to_portrait($videos_with_dimensions);
        // $scaled_down_videos = $this->scale_down_videos($videos_with_dimensions);
        $total_duration = 0;
        $max_duration = 180;
        $carets_videos = [];
        $dur_start_time = new Carbon('first day of January 1990');
        $dur_end_time = new Carbon('first day of January 1990');
        foreach ($videos_with_dimensions as $idx => $video) {
            if ($video['start_time']->greaterThanOrEqualTo($dur_start_time) && $video['end_time']->lessThanOrEqualTo($dur_end_time)) {
                array_push($carets_videos, $video);
                continue;
            }
            if ($video['end_time']->greaterThan($dur_end_time)) {
                if ($video['start_time']->greaterThan($dur_start_time)) {
                    $dur_start_time = $video['start_time'];
                } else {
                    $dur_start_time = $video['end_time'];
                }
                $dur_end_time = $video['end_time'];
            } else {
                continue;
            }
            $total_duration += $dur_end_time->diffInSeconds($dur_start_time);
            if ($total_duration <= $max_duration) {
                array_push($carets_videos, $video);
            } else {
                continue;
            }
        }
        $this->generate_caret_video($carets_videos);
    }
    function get_correct_dimension_videos($coll) {
        $videos = $coll;
        $videos->transform(function ($item, $key) {
            exec("ffprobe -v error -show_entries stream=width,height -of default=noprint_wrappers=1 $this->base_path{$item['path']} 2>&1", $output, $return_var);
            $item['width'] = Str::replaceFirst('width=', '', $output[0]);
            $item['height'] = Str::replaceFirst('height=', '', $output[1]);
            return $item;
        });
        return $videos;
    }
    function scale_down_videos ($videos) {
        $videos_to_transform = $videos;
        $transformed_videos = $videos_to_transform->transform(function ($video, $key) {
            if($video['height'] == "720" || $video['width'] == "720") {
                exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$this->base_path.$video['path'].' -vf "scale=480:640,fps=30" -acodec copy -y '.$this->base_path.'Videos/transformed_'.$video['title'].' 2>&1', $output, $return_var);
                $video['title'] = 'transformed_'.$video['title'];
                $video['path'] =  'Videos/'.$video['title'].'';
                return $video;
            } else {
                return $video;
            }
        });
        return $transformed_videos;
    }
    function transform_landscape_to_portrait ($videos) {
        $videos_to_transform = $videos;
        $transformed_videos = $videos_to_transform->transform(function ($video, $key) {
            if($video['width'] > $video['height']) {
                exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$video['title'].' -vf "scale=540:-2:force_original_aspect_ratio=1,pad=540:960:(ow-iw)/2:(oh-ih)/2,fps=30" -acodec copy -y transformed_'.$video['title'].' 2>&1', $output, $return_var);
                $video['title'] = 'transformed_'.$video['title'];
                return $video;
            } else {
                return $video;
            }
        });
        return $transformed_videos;
    }
    function generate_caret_video ($coll) {
    $clip_times = $this->set_caret_end_time($coll);
    $clip_start_time = $clip_times['start'];
    $clip_end_time = 0;
    $iteration = 1;
    $this->generate_next_clip($clip_start_time, $iteration);
    return $this->resultant_videos;
    }
    //* this method sets the global caret start and end time. also returns it in an object
    //* it also sets $this->caret_videos to the collection and  its called once
    function set_caret_end_time ($coll) {
        $this->caret_videos = $coll;
        $this->caret_start_time = $this->caret_videos[0]['start_time'];
        $this->caret_end_time = $this->caret_videos[0]['end_time'];
        foreach ($this->caret_videos as $caret_video) {
            if ($caret_video['start_time'] < $this->caret_start_time) {
                $this->caret_start_time = $caret_video['start_time'];
            }
            if ($caret_video['end_time'] > $this->caret_end_time) {
                $this->caret_end_time = $caret_video['end_time'];
            }
        }
        return ['start'=>$this->caret_start_time, 'end'=>  $this->caret_end_time];
    }
    // *
    function generate_next_clip($clip_start_time, $iteration) {
        $iteration++;
        $clip_start_time = $clip_start_time;
        if ($clip_start_time->greaterThanOrEqualTo($this->caret_end_time) || $iteration > 20) {
            return;
        }
        $clip_end_time = $this->calculate_end_time($clip_start_time);
        $merged_clip = $this->merge_and_save_clip($clip_start_time, $clip_end_time);
        array_push($this->resultant_videos, $merged_clip);
        // $clip_start_time = $clip_end_time->addSeconds(1); //Add one second
        $clip_start_time = $clip_end_time; //Add one second
        $this->generate_next_clip($clip_start_time, $iteration);
    }
    // * this func returns the end time for required for the clip length
    function calculate_end_time($clip_start_time) {
        $local_clip_end_time = null;
        foreach ($this->caret_videos as $caret_video) {
            if ($caret_video['end_time']->greaterThan($clip_start_time)){
                if ($local_clip_end_time == null){
                    $local_clip_end_time = $caret_video['end_time'];
                }
                else if ($caret_video['start_time']->lessThanOrEqualTo($local_clip_end_time) && $caret_video['start_time']->greaterThan($clip_start_time)){
                    // $local_clip_end_time = $caret_video['start_time']->subSeconds(1);  //Minus one second
                    $local_clip_end_time = $caret_video['start_time'];  //Minus one second
                } else if ($caret_video['end_time']->lessThan( $local_clip_end_time)){
                    $local_clip_end_time = $caret_video['end_time'];
                }
            }
        }
        return $local_clip_end_time;
    }
    function merge_and_save_clip($clip_start_time, $clip_end_time){
        $clips_to_merge = [];
        foreach ($this->caret_videos as $caret_video) {
            if ($caret_video['start_time']->lessThanOrEqualTo($clip_start_time) && $caret_video['end_time']->greaterThanOrEqualTo($clip_end_time)) {
                array_push($clips_to_merge, $caret_video);
            }
        }
        // foreach ($clips_to_merge as $vid) {
        //     echo($vid['title'] . '  ');
        // }
        $layout = count($clips_to_merge);
        if ($layout == 0) {
            return ['null'];
        }
        return  $merged_video = $this->merge_videos($clips_to_merge, $clip_start_time, $clip_end_time);
        // Save_Caret_Video (merged_video);
    }
    function merge_videos ($clips_to_merge, $clip_start_time, $clip_end_time){
        $merged_video = [];
        foreach ($clips_to_merge as $video_clip) {
            array_push($merged_video,[
                'id' => $video_clip['id'],
                'title' => $video_clip['title'],
                'start_time'=> $clip_start_time,
                'end_time' => $clip_end_time,
                'clipping_start' => $video_clip['clipping_start'],
                'clipping_end' => $video_clip['clipping_end'],
                'path' => $this->base_path.$video_clip['path'],
            ]);
        }
        $this->ffmpeg($merged_video);
    }
    function ffmpeg ($videos_to_merge) {
        $videos_array = $videos_to_merge;
        $layout = count($videos_to_merge);
        $caret_array = $this->caret_videos;
        $sub = Carbon::parse('2020-10-16 04:01:05');
        $merging_clips_arr = [];
        if ($layout == 1) {
            //! 'Append the array';
            foreach($caret_array as $item) {
                if($item['id'] === $videos_to_merge[0]['id']) {
                    $videos_array[0]['clipping_start'] = Carbon::parse($videos_array[0]['start_time']->diffInSeconds($item['start_time']))->toTimeString();
                    //! add condition: If clipping start = 0 then don't clip the video and return
                    $videos_array[0]['clipping_end'] = Carbon::parse($videos_array[0]['end_time']->diffInSeconds($item['start_time']))->toTimeString();
                    $title = now()->format('ymdhisu').'_'.Str::random(5).'_'.$videos_array[0]['title'];
                    $path = "$this->output_path.$title";
                    // if ($videos_array[0]['clipping_start'] != "00:00:00") {
                        exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$videos_array[0]['path'].' -ss '. $videos_array[0]['clipping_start'].' -to '.$videos_array[0]['clipping_end'].' '.$path.' 2>&1', $output, $return_var);
                        array_push($merging_clips_arr, [
                            'id' => $videos_array[0]['id'],
                            'title' => $title,
                            'start_time'=> $videos_array[0]['start_time'],
                            'end_time' => $videos_array[0]['end_time'],
                            'clipping_start' => $videos_array[0]['clipping_start'],
                            'clipping_end' => $videos_array[0]['clipping_end'],
                            'path' => $path,
                            ]);
                    // } else {
                    //     array_push($merging_clips_arr, [
                    //         'id' => $videos_array[0]['id'],
                    //         'title' =>$videos_array[0]['title'],
                    //         'start_time'=> $videos_array[0]['start_time'],
                    //         'end_time' => $videos_array[0]['end_time'],
                    //         'clipping_start' => $videos_array[0]['clipping_start'],
                    //         'clipping_end' => $videos_array[0]['clipping_end'],
                    //     ]);
                    // }
                }
            }
        } else {
            $counting = 1;
            foreach($caret_array as $itemIdx =>$item) {
                foreach ($videos_array as $vIdx => $video ) {
                    if($item['id'] == $video['id']) {
                        $videos_array[$vIdx]['clipping_start'] = Carbon::parse($videos_array[$vIdx]['start_time']->diffInSeconds($item['start_time']))->toTimeString();
                        $videos_array[$vIdx]['clipping_end'] = Carbon::parse($videos_array[$vIdx]['end_time']->diffInSeconds($item['start_time']))->toTimeString();
                        $clip_start = $videos_array[$vIdx]['clipping_start'];
                        $clip_end = $videos_array[$vIdx]['clipping_end'];
                        $title = now()->format('ymdhisu').'_'.Str::random(5).'_'.$videos_array[$vIdx]['title'];
                        $path = "$this->output_path.$title";
                        if ($clip_end != "00:00:00") {
                            exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$videos_array[$vIdx]['path'].' -ss '.$clip_start.' -to '.$clip_end.' '.$path.' 2>&1', $output, $return_var);
                            array_push($merging_clips_arr, [
                                'id' => $videos_array[$vIdx]['id'],
                                'title' => $title,
                                'start_time'=> $videos_array[$vIdx]['start_time'],
                                'end_time' => $videos_array[$vIdx]['end_time'],
                                'clipping_start' => $videos_array[$vIdx]['clipping_start'],
                                'clipping_end' => $videos_array[$vIdx]['clipping_end'],
                                'path' => $path,
                            ]);
                        }
                    }
                    $counting++;
                }
            }
        }
        $this->merging_clips_ffmpeg($merging_clips_arr, count($merging_clips_arr));
    }
    function merging_clips_ffmpeg ($merging_arr, $length) {

        //?  taskset -c 0,1,2,3,4,5 ffmpeg -i input.mp4 -lavfi "[0:v]scale=256/81*iw:256/81*ih,boxblur=luma_radius=min(h\,w)/40:luma_power=3:chroma_radius=min(cw\,ch)/40:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,setsar=1,crop=w=iw*81/256"  output.mp4
        // ? command for adding video frame up and down
        $output_title= now()->format('ymdhisu').'_'.Str::random(5).'_'.'mergedclip.mp4';
        $path= $this->output_path.$output_title;
        $layouts = [
            "",
            //! 2 videos
            '-filter_complex "[0:v]scale=540:480:force_original_aspect_ratio=1,pad=540:480:(ow-iw)/2:(oh-ih)/2, fps=30 [a];[1:v] scale=540:480:force_original_aspect_ratio=1,pad=540:480:(ow-iw)/2:(oh-ih)/2, fps=30 [b];[a][b]vstack=inputs=2[v]" -map "[v]" -map 0:a '.$path.' ',
            //! 3 videos
            '-filter_complex "[0:v]scale=270:480:force_original_aspect_ratio=1,pad=270:480:(ow-iw)/2:(oh-ih)/2, fps=30[a0]; [1:v]scale=270:480:force_original_aspect_ratio=1,pad=270:480:(ow-iw)/2:(oh-ih)/2, fps=30 [a1]; [2:v]scale=540:480:force_original_aspect_ratio=1,pad=540:480:(ow-iw)/2:(oh-ih)/2, fps=30 [a2]; [a0][a1][a2]xstack=inputs=3:layout=0_0|w0_0|0_h0:fill=#000000" -map 0:a '.$path.' ',
            //! 4 videos
            '-filter_complex "[0:v]scale=270:480, fps=30 [a0]; [1:v]scale=270:480, fps=30 [a1]; [2:v]scale=270:480, fps=30 [a2]; [3:v]scale=270:480, fps=30 [a3];[a0][a1][a2][a3]xstack=inputs=4:layout=0_0|0_h0|w0_0|w0_h0" -map 0:a '.$path.' ',
            //! 5 videos
            '-filter_complex "[0:v]scale=270:320:force_original_aspect_ratio=1, fps=30 [a0]; [1:v]scale=270:320:force_original_aspect_ratio=1, fps=30 [a1]; [2:v]scale=270:320:force_original_aspect_ratio=1, fps=30 [a2] ;[3:v]scale=270:320:force_original_aspect_ratio=1, fps=30 [a3]; [4:v] scale=270:320:force_original_aspect_ratio=1, fps=30 [a4]; [a0][a1][a2][a3][a4]xstack=inputs=5:layout=0_0|0_h0|w0_0|w0_h0|0_h0+h1:fill=#000000" -map 0:a '.$path.' ',
            //! 6 videos
            '-filter_complex "[0:v]scale=270:320:force_original_aspect_ratio=1,pad=ceil(iw/2)*2:ceil(ih/2)*2, fps=30[a0]; [1:v]scale=270:320:force_original_aspect_ratio=1,pad=ceil(iw/2)*2:ceil(ih/2)*2, fps=30 [a1]; [2:v]scale=270:320:force_original_aspect_ratio=1,pad=ceil(iw/2)*2:ceil(ih/2)*2, fps=30 [a2]; [3:v]scale=270:320:force_original_aspect_ratio=1,pad=ceil(iw/2)*2:ceil(ih/2)*2, fps=30 [a3]; [4:v]scale = 270:320:force_original_aspect_ratio=1,pad=ceil(iw/2)*2:ceil(ih/2)*2, fps=30 [a4]; [5:v]scale=270:320:force_original_aspect_ratio=1,pad=ceil(iw/2)*2:ceil(ih/2)*2, fps=30 [a5];[a0][a1][a2][a3][a4][a5]xstack=inputs=6:layout=0_0|0_h0|w0_0|w0_h0|0_h0+h1|w0_h0+h1:fill=#000000" -map 0:a '.$path.' '
        ];
        if ($length == 1) {
            array_push($this->scale_videos, ['title' => $merging_arr[0]['title'], 'path' => $merging_arr[0]['path']]);
        } elseif ($length > 1) {
            $ffmpeg = "taskset -c 0,1,2,3,4,5 ffmpeg ";
            foreach($merging_arr as $vIdx => $video) {
                $ffmpeg .=  "-i {$merging_arr[$vIdx]['path']} ";
            }
            $l_index =  count($merging_arr) - 1;
            $ffmpeg .= $layouts[$l_index];
            exec(''.$ffmpeg.' 2>&1', $output, $return_var);
            if($return_var != 0) {
            } else {
                if($length >= 5) {
                    exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$path.' -vf "scale=540:960::force_original_aspect_ratio=1,pad=540:960:(ow-iw)/2:(oh-ih)/2" -acodec copy -y '.$this->output_path.'55_'.$output_title.' 2>&1', $output, $return_var);
                    if($return_var != 0) {

                    } else {
                        // exec('rm '.$path.' 2>&1', $output, $return_var);
                        array_push($this->scale_videos, ['title' => '55_'.$output_title.'', 'path' => ''.$this->output_path.'55_'.$output_title.'']);
                    }
                } else {
                    // exec('rm '.$path.' 2>&1', $output, $return_var);
                    array_push($this->scale_videos, ['title' => $output_title, 'path' => "$this->output_path.$output_title"]);
                }

            }
        } else {
        }
    }
    function scale_videos_ffmpeg() {
        foreach($this->scale_videos as $Idx => $item) {
            if (Str::contains($this->scale_videos[$Idx]['title'], 'merged')) {
                array_push($this->videos_to_concatenate, ['title' => $this->scale_videos[$Idx]['title'], 'path' => $this->scale_videos[$Idx]['path']]);
            } else {
                exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$item['path'].' -vf scale=540:960,fps=30 -acodec copy -y '.$this->output_path.'scaled_'.$item['title'].' 2>&1', $output, $return_var);
                if ($return_var != 0) {
                }
                array_push($this->videos_to_concatenate, ['title' => 'scaled_'.$item['title'].'', 'path' => ''.$this->output_path.'scaled_'.$item['title'].'' ]);
            }
        }

        return;
    }
    function concatenate_videos_ffmpeg() {
        $txt_file_name = now()->format('ymdhisu').'_'.Str::random(5).'_'.'_filelist.txt';
        $result_file_name = now()->format('ymdhisu').'_'.Str::random(5).'_'.'_concatenated.mp4';
        $remove_extra_frames = 'fast_start_'.$result_file_name;
        foreach( $this->videos_to_concatenate as $vIdx => $video) {
            exec("echo file {$this->videos_to_concatenate[$vIdx]['title']} >> {$this->output_path}{$txt_file_name}");
        }
        exec("taskset -c 0,1,2,3,4,5 ffmpeg -f concat -i {$this->output_path}{$txt_file_name} -c copy {$this->output_path}{$remove_extra_frames} 2>&1", $output, $return_var);
        exec("taskset -c 0,1,2,3,4,5 ffmpeg -i {$this->output_path}{$remove_extra_frames} -map 0 -c copy -fflags +shortest -max_interleave_delta 1G -movflags +faststart {$this->output_path}{$result_file_name} 2>&1", $output, $return_var);
        if ($return_var == 0) {
            // exec("rm $this->output_path.$txt_file_name");
            // foreach($this->scale_videos as $Idx => $item) {
                // exec("rm $this->output_path.{$item['title']}");
            // }
            // foreach($this->videos_to_concatenate as $Idx => $item) {
                // exec("rm $this->output_path.{$item['title']}");
            // }
            $this->caret_videos = [];
            $this->caret_start_time = 0;
            $this->caret_end_time = 0;
            $this->iteration = 0;
            $this->resultant_videos = [];
            $this->scale_videos = [];
            $this->videos_to_concatenate = [];
            // for 2nd algo
            $this->result = null;
            $this->min_duration = null;
            $this->last_clip_end = 0;
            $this->longest_duration = 0;
            return "caret_videos/".$result_file_name;
        } else {
            //
        }
    }
    function concatenate_caret_videos_ffmpeg() {
        $txt_file_name = now()->format('ymdhisu').'_'.Str::random(5).'_'.'_filelist.txt';
        $result_file_name = now()->format('ymdhisu').'_'.Str::random(5).'_'.'_concatenated.mp4';
        $remove_extra_frames = 'fast_start_'.$result_file_name;
        foreach( $this->videos_to_concatenate as $vIdx => $video) {
            exec("echo file {$this->videos_to_concatenate[$vIdx]['title']} >> {$this->output_path}{$txt_file_name}");
        }
        exec("ffmpeg -f concat -i {$this->output_path}{$txt_file_name} -c copy {$this->output_path}{$remove_extra_frames} 2>&1", $output, $return_var);
        exec("taskset -c 0,1,2,3,4,5 ffmpeg -i {$this->output_path}{$remove_extra_frames} -map 0 -c copy -max_interleave_delta 1G -movflags +faststart {$this->output_path}{$result_file_name} 2>&1", $output, $return_var);


        if ($return_var == 0) {
            // exec("rm $this->output_path.$txt_file_name");
            // foreach($this->scale_videos as $Idx => $item) {
                // exec("rm $this->output_path.{$item['title']}");
            // }
            // foreach($this->videos_to_concatenate as $Idx => $item) {
                // exec("rm $this->output_path.{$item['title']}");
            // }
            $this->caret_videos = [];
            $this->caret_start_time = 0;
            $this->caret_end_time = 0;
            $this->iteration = 0;
            $this->resultant_videos = [];
            $this->scale_videos = [];
            $this->videos_to_concatenate = [];
            // for 2nd algo
            $this->result = null;
            $this->min_duration = null;
            $this->last_clip_end = 0;
            $this->longest_duration = 0;
            return "caret_videos/".$result_file_name;
        } else {
            //
        }
    }
    public function caret_based_merging ($videos) {
        $iteration = 1;
        if (count($videos) == 1) {
            $item =  $videos->first();
            return $item['path'];
        }
        else {
            $this->result = $videos;
            // $this->result = $this->scale_down_videos($videos_with_dimensions);
            $this->longest_duration = $this->result->where('duration','>','0')->max('duration');
            $this->create_clips($iteration);
            $this->scale_videos_ffmpeg();
            return $this->concatenate_caret_videos_ffmpeg();
        }
    }
    function create_clips($iteration) {
        if($iteration == 10 || $this->last_clip_end == $this->longest_duration) {
            return ;
        }
        $iteration++;
        $this->clips_merging();
        $this->create_clips($iteration);
        return $iteration;
    }
    function clips_merging() {
        $merging_videos = [];
        $this->min_duration = $this->result->where('duration','>','0')->min('duration');
        $this->result->transform(function ($video, $key) use(&$merging_videos) {
            if($video['duration'] > 0 ) {
                $video['duration'] = $video['duration'] - $this->min_duration;
                $video['clipping_start'] = Carbon::parse($this->last_clip_end)->toTimeString();
                $video['clipping_end'] =  Carbon::parse($this->min_duration)->addSeconds($this->last_clip_end)->toTimeString();
                array_push($merging_videos, $video);
                return $video;
            } else {
                $video['duration'] = 0;
                return $video;
            }
        });
        $this->last_clip_end +=  $this->min_duration;
        $this->clipping_videos($merging_videos);
    }
    function clipping_videos ($videos) {
        $merging_clips_arr = [];
        foreach ($videos as $vIdx => $video ) {
                $title = now()->format('ymdhisu').'_'.Str::random(5).'_'.$videos[$vIdx]['title'];
                exec('taskset -c 0,1,2,3,4,5 ffmpeg -i '.$this->base_path.$videos[$vIdx]['path'].' -ss '.$videos[$vIdx]['clipping_start'].' -to '.$videos[$vIdx]['clipping_end'].' '.$this->output_path.''.$title.' 2>&1', $output, $return_var);
                if($return_var != 0) {
                } else {
                    array_push($merging_clips_arr, [
                        'id' => $videos[$vIdx]['id'],
                        'title' => $title,
                        'start_time'=> $videos[$vIdx]['start_time'],
                        'end_time' => $videos[$vIdx]['end_time'],
                        'clipping_start' => $videos[$vIdx]['clipping_start'],
                        'clipping_end' => $videos[$vIdx]['clipping_end'],
                        'path' => "$this->output_path"."$title",
                        ]);
                    }
                }
        $this->merging_clips_ffmpeg($merging_clips_arr, count($merging_clips_arr));
    }




    public function distanceListing()
    {
        ini_set("max_execution_time", 90000);

        $message='';
        $checkArray = [];
        $initRecs = Videos::where('is_caret',0)
        ->where('is_active',1)
        ->where('is_allow_caretCreation',1)
        //->where('is_processing_caret',0)
        ->whereIn('distance_video_process', [1, 0, 3])
        ->where('distance_retry_count','<',3)
        ->whereNotNull('loc_lat')
        ->whereNotNull('loc_long')
        ->whereNotNull('start_time')
        ->orderBy('start_time','asc')
        ->get();
        if(!$initRecs) {
            return $this->sendSuccessResponse('data', '', 'No Record found!');
        }
        $dis = SystemSettings::where('name','distance')->first();
        $distance = $dis->value;
        foreach($initRecs as $first){
                if (in_array($first->id, $checkArray))
                {
                    continue;
                }
                $loc_lat = $first->loc_lat;
                $loc_long = $first->loc_long;
                $record = Videos::select('*')
                ->with(['users'])
                ->where('is_caret',0)
                //->where('is_marked_distance',0)
                ->where('is_allow_caretCreation',1)
                //->where('is_processing_caret',0)
                ->whereIn('distance_video_process', [1, 0, 3])
                ->where('distance_retry_count','<',3)
                ->whereNotNull('loc_lat')
                ->whereNotNull('loc_long')
                ->whereNotNull('start_time')
                ->when($distance,function($q) use($distance,$loc_lat,$loc_long){
                        #scope for radius
                        if($loc_lat && $loc_long)
                            $q->distance2($loc_lat,$loc_long,$distance);
                    })
                ->orderBy('start_time','asc')
                ->get();
                if($record->count()<=1){
                    $checkArray[] = $first->id;
                    Videos::where('id',$first->id)->update(['distance_video_process'=>3]);
                    Videos::where('id',$first->id)->increment('distance_retry_count' , 1);
                    continue;
                }
                $divider = $this->getRemaining($record->count(),6);
                $multiples = array_chunk($record->toArray(), $divider,true);
                foreach($multiples as $videos){
                    $newRec = [];
                    $newUser = [];
                    $newVid = [];
                    $hash_ids=[];
                    $caret_ids=[];
                    $tagged_ids=[];
                    $desc_hashs=[];
                    $desc_carets=[];
                    $desc_taggeds=[];
                    //return $videos;
                    foreach($videos as $video){
                        $vid_title = explode("/", $video['video_raw_url']);
                        $users = $video['users'];
                        $newRec[] = [
                            "id" => $video['id'],
                            'title' => end($vid_title),
                            'start_time' => Carbon::parse($video['start_time']),
                            'end_time' => Carbon::parse($video['end_time']),
                            'clipping_start' => '00:00:00',
                            'clipping_end' => '00:00:00',
                            'width' => null,
                            'height' => null,
                            'duration' => $video['duration'],
                            'path' => $video['video_raw_url'],
                        ];
                        foreach($users as $user){
                            $newUser[] = $user['id'];
                        }
                        if(isset($video['video_description'])){
                            $data2['video_description'] = trim($video['video_description']);
                            preg_match_all('/(?<!\w)#\w+/',$data2['video_description'],$hashes);
                            preg_match_all('/(?<!\w)@\w+/',$data2['video_description'],$mentions);
                            preg_match_all('/(?<!\w)\^\w+/',$data2['video_description'],$carets);
                            $hash_ids = array_unique (array_merge($hash_ids,$this->addHash($hashes)));
                            $caret_ids = array_unique (array_merge($caret_ids,$this->addCarets($carets)));
                            $tagged_ids = array_unique (array_merge($tagged_ids,$this->mentionedUsers($mentions)));
                            $desc_hashs = array_unique (array_merge($desc_hashs,$hashes[0]));
                            $desc_carets = array_unique (array_merge($desc_carets,$carets[0]));
                            $desc_taggeds = array_unique (array_merge($desc_taggeds,$mentions[0]));
                        }
                        $checkArray[] = $video['id'];
                        $newVid[] = $video['id'];
                        Videos::where('id',$video['id'])->update(['distance_video_process'=>1]);
                    }
                    // return $newRec;
                    $data2['is_active'] = 0;
                    $data2['is_caret'] = 1;
                    $data2['is_allow_comments'] = 1;
                    $data2['loc_lat'] = $first->loc_lat;
                    $data2['loc_long'] = $first->loc_long;
                    $data2['is_marked'] = 2;

                    $data2['video_description'] = '';
                    if (!empty($desc_carets)) {
                        $data2['video_description'] .= implode(', ', $desc_carets);
                    }
                    if (!empty($desc_hashs)) {
                        // Add a comma if there’s already content in the description
                        $data2['video_description'] .= ($data2['video_description'] ? ', ' : '') . implode(', ', $desc_hashs);
                    }
                    if (!empty($desc_taggeds)) {
                        // Add a comma if there’s already content in the description
                        $data2['video_description'] .= ($data2['video_description'] ? ', ' : '') . implode(', ', $desc_taggeds);
                    }

                    if (count($newRec) > 1){
                        Videos::whereIn('id',$newVid)->update(['distance_video_process'=>2]);
                        $data2['video_url'] = $this->time_based_merging(collect($newRec));
                        $data2['image_url'] = str_replace('.mp4','.gif', $data2['video_url']);
                        $data2['video_raw_url'] = str_replace('.mp4','_raw.mp4', $data2['video_url']);
                    } else {
                        Videos::whereIn('id',$newVid)->update(['distance_video_process'=>3]);
                        Videos::whereIn('id',$newVid)->increment('distance_retry_count' , 1);
                        continue;
                    }
                    if(isset($data2['video_url'])){
                        $path = "app/".$data2['video_url'];
                        $image_path = storage_path($path);
                        $isExists = File::exists($image_path);
                        if ($isExists) {
                            $image = file_get_contents($image_path);
                            //$path = config('app.env') . '/' . $folder . $person_id . '/' . $image_name;
                            Helper::putS3Image($data2['video_raw_url'], $image, 'public');
                            //unlink($image_path);
                            $data2['video_title'] = "time_based_merging";
                            $Videos = Videos::create($data2);
                            $Videos->users()->sync($newUser);
                            $Videos->users()->increment('caret_count' , 1);
                            $Videos->videos()->sync($newVid);
                            //$Videos->videos()->update(['distance_video_process'=>2]);
                            if(isset($hash_ids))
                                $Videos->hashs()->sync($hash_ids);
                            if(isset($caret_ids))
                                $Videos->carets()->sync($caret_ids);
                            if(isset($tagged_ids))
                                    $Videos->tagged()->sync($tagged_ids);
                            $user_age = $Videos->users()->withCount(['profile'=>function($q){
                                $q->select('dob');
                            }])->orderBy('profile_count')->first();
                            if(isset($user_age->profile_count)){
                                $age = Carbon::parse($user_age->profile_count)->diff(Carbon::now())->format('%y');
                                $ageRange = Ages::whereRaw('? between min and max', [$age])->first();
                                $ageData['age'] = $ageRange->id;
                                $Videos->update($ageData);
                            }

                            if(env('IS_GPU') == FALSE){
                                $queueName = env('UPLOAD_VIDEO_QUEUE'); // Default to 'default' if not set
                                ProcessUploadVideoJob::dispatch($Videos->id)->onQueue($queueName);
                            }

                            $data[] = $Videos;
                        }else{
                            //Videos::whereIn('id',$newVid)->update(['is_marked_distance'=>1,'is_processing_caret'=>0]);
                            // Videos::whereIn('id',$newVid)->update(['distance_video_process'=>3]);
                            // Videos::whereIn('id',$newVid)->increment('distance_retry_count' , 1);
                            $message = "distance_video_process - Failed";
                        }
                    }else{
                        //Videos::whereIn('id',$newVid)->update(['is_marked_distance'=>1,'is_processing_caret'=>0]);
                        Videos::whereIn('id',$newVid)->update(['distance_video_process'=>3]);
                        Videos::whereIn('id',$newVid)->increment('distance_retry_count' , 1);
                        $message = "distance_video_process - Failed";
                    }
                }
            }
    }
    public function getRemaining($total,$divider){
        //echo $divider;
        $ret = $total%$divider;
        if($divider == 1){
            return 6;
        }
        if($ret == 1){
           $divider-=1;
           return $this->getRemaining($total,$divider);
        }else{
            return $divider;
        }
    }
    #CRON JOBS
    public function unsetProcessingFlag(){
        Videos::where('is_processing_caret',1)
        ->where('updated_at', '<',Carbon::parse('-1 hours'))
        ->update(['is_processing_caret'=>0]);
    }
    public function unsetCaretFlag(){
        Videos::where('is_allow_caretCreation',1)
        ->where('is_marked_distance',0)
        ->where('updated_at', '<',Carbon::parse('-24 hours'))
        ->update(['is_marked_distance'=>1]);
    }
    #END CRON JOBS
    #mentionedUsers
    public function mentionedUsers($data){
        //return $data[0];
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                $res = User::where('username',trim(str_replace('@','',$dt)))->first();
                if($res)
                    $ret[] =  $res->id;
            }
        }
        return $ret;
    }
    public function addHash($data){
        //return $data[0];
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                //return $dt;
                $arr['title'] = trim(str_replace('#','',$dt));
                $res = Hashs::updateOrCreate($arr);
                $ret[] =  $res->id;
            }
        }
        return $ret;
    }
    public function addCarets($data){
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                $arr['title'] = trim(str_replace('^','',$dt));
                $Carets['id'] = '';
                $Carets = Carets::where('title', $arr['title'])->first();
                if(isset($Carets['id'])){
                    $arr['updated_at'] = Carbon::now();
                    $Carets->update($arr);
                }else{
                    //$arr['updated_at'] = Carbon::now();
                    $Carets = Carets::create($arr);
                }
                $ret[] =  $Carets->id;
            }
        }
        return $ret;
    }

    ###############################################################################################################
    private function finalMerging($introVideo, $main, $finishVideo, $videoSound, $export_video, $in_app_video, $carte_id)
    {
        // Define paths and settings
        $output = [];
        $target_resolution = '540x960';
        $target_fps = '30';
        $scaled_main = str_replace('.mp4', '_scaled.mp4', $main);

        // Default file paths
        $default_intro = storage_path('app/Audio/carets_intro.mp4');
        $default_finish = storage_path('app/Audio/carets_finish.mp4');
        $default_audio = storage_path('app/Audio/carets_audio.mp3');

        // Ensure main video exists
        if (!file_exists($main)) {
            Log::error("Main video file does not exist: $main");
            return false;
        }

        // Check or set intro video
        if (!file_exists($introVideo)) {
            $introVideo = null;
        } else {
            $scaled_introVideo = str_replace('.mp4', '_scaled.mp4', $introVideo);
            exec("taskset -c 0,1,2,3,4 ffmpeg -y -i $introVideo -vf \"scale=$target_resolution,fps=$target_fps\" -c:a aac $scaled_introVideo 2>&1", $output, $return_var);
            if ($return_var === 0) {
                $introVideo = $scaled_introVideo;
            } else {
                Log::warning("Failed to scale intro video. Using original: $introVideo");
            }

        }

        // Check or set finish video
        if (!file_exists($finishVideo)) {
            $finishVideo = null;
        } else {
            $scaled_finishVideo = str_replace('.mp4', '_scaled.mp4', $finishVideo);
            exec("taskset -c 0,1,2,3,4 ffmpeg -y -i $finishVideo -vf \"scale=$target_resolution,fps=$target_fps\" -c:a aac $scaled_finishVideo 2>&1", $output, $return_var);
            if ($return_var === 0) {
                $finishVideo = $scaled_finishVideo;
            } else {
                Log::warning("Failed to scale finish video. Using original: $finishVideo");
            }
        }


        // Scale the main video
        exec("taskset -c 0,1,2,3,4 ffmpeg -y -i $main -vf \"scale=$target_resolution,fps=$target_fps\" -c:a aac $scaled_main 2>&1", $output, $return_var);
        if ($return_var !== 0) {
            Log::error("Failed to scale main video: $main");
            return false;
        }


        if (!file_exists($videoSound) || filesize($videoSound) === 0) {
            $audio_file = storage_path('app/Audio/Self/'.$carte_id.'.mp3');
            if (file_exists($audio_file)) {
                $videoSound = $audio_file;
            }else{
                $videoSound = $default_audio;
            }

        }



        if($introVideo != null && $finishVideo != null){
            // Calculate durations
            $introDuration = $this->getMediaDuration($introVideo);
            $mainDuration = $this->getMediaDuration($scaled_main);
            $finishDuration = $this->getMediaDuration($finishVideo);
            $videoDuration = $introDuration + $mainDuration + $finishDuration;

            if ($videoSound && file_exists($videoSound) && filesize($videoSound) > 0) {
                $audioDuration = $this->getMediaDuration($videoSound);
                $loopCount = ceil($videoDuration / $audioDuration);
                $introDelayMs = $introDuration * 1000;

                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $introVideo \
                    -i $scaled_main \
                    -i $finishVideo \
                    -i $videoSound \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [2:v]setsar=1[v2]; \
                    [v0][v1][v2]concat=n=3:v=1:a=0[v]; \
                    [3:a]adelay={$introDelayMs}|{$introDelayMs},aloop=loop=$loopCount:size=2147483647,apad=pad_dur=$videoDuration,atrim=0:$videoDuration"."[a]\" \
                    -map \"[v]\" -map \"[a]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            } else {
                // Handle cases where no external audio is provided

                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $introVideo \
                    -i $scaled_main \
                    -i $finishVideo \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [2:v]setsar=1[v2]; \
                    [v0][v1][v2]concat=n=3:v=1:a=0[v]\" \
                    -map \"[v]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            }
        }
        else if ($introVideo != null && $finishVideo == null) {
            // Calculate durations
            $introDuration = $this->getMediaDuration($introVideo);
            $mainDuration = $this->getMediaDuration($scaled_main);
            $videoDuration = $introDuration + $mainDuration;

            if ($videoSound && file_exists($videoSound) && filesize($videoSound) > 0) {
                // Calculate audio duration and loop count
                $audioDuration = $this->getMediaDuration($videoSound);
                $loopCount = ceil($videoDuration / $audioDuration);
                $introDelayMs = $introDuration * 1000;

                // Construct FFmpeg command with intro video and external audio
                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $introVideo \
                    -i $scaled_main \
                    -i $videoSound \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [v0][v1]concat=n=2:v=1:a=0[v]; \
                    [2:a]adelay={$introDelayMs}|{$introDelayMs},aloop=loop=$loopCount:size=2147483647,apad=pad_dur=$videoDuration,atrim=0:$videoDuration"."[a]\" \
                    -map \"[v]\" -map \"[a]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            } else {
                // Handle cases where no external audio is provided

                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $introVideo \
                    -i $scaled_main \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [v0][v1]concat=n=2:v=1:a=0[v]\" \
                    -map \"[v]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            }
        }
        else if ($introVideo == null && $finishVideo != null) {
            // Calculate durations
            $mainDuration = $this->getMediaDuration($scaled_main);
            $finishDuration = $this->getMediaDuration($finishVideo);
            $videoDuration = $mainDuration + $finishDuration;

            if ($videoSound && file_exists($videoSound) && filesize($videoSound) > 0) {
                // Calculate audio duration and loop count
                $audioDuration = $this->getMediaDuration($videoSound);
                $loopCount = ceil($videoDuration / $audioDuration);

                // Construct FFmpeg command with main video, finish video, and external audio
                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $scaled_main \
                    -i $finishVideo \
                    -i $videoSound \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [v0][v1]concat=n=2:v=1:a=0[v]; \
                    [2:a]aloop=loop=$loopCount:size=2147483647,apad=pad_dur=$videoDuration,atrim=0:$videoDuration"."[a]\" \
                    -map \"[v]\" -map \"[a]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            } else {
                // Handle cases where no external audio is provided

                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $scaled_main \
                    -i $finishVideo \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [v0][v1]concat=n=2:v=1:a=0[v]\" \
                    -map \"[v]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            }
        }

        else{

            if ($videoSound && file_exists($videoSound) && filesize($videoSound) > 0) {
                // Calculate audio duration and loop count
                $videoDuration = $this->getMediaDuration($scaled_main);
                $audioDuration = $this->getMediaDuration($videoSound);
                $loopCount = ceil($videoDuration / $audioDuration);

                // Construct FFmpeg command with external audio
                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $scaled_main \
                    -i $videoSound \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [v0]concat=n=1:v=1:a=0[v]; \
                    [1:a]aloop=loop=$loopCount:size=2147483647,apad=pad_dur=$videoDuration,atrim=0:$videoDuration"."[a]\" \
                    -map \"[v]\" -map \"[a]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            } else {
                // Handle cases where no external audio is provided

                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $scaled_main \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [v0]concat=n=1:v=1:a=0[v]\" \
                    -map \"[v]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $in_app_video 2>&1";
            }

        }



        // Execute the concatenation command
        exec($concat_command, $output, $return_var);
        if ($return_var !== 0) {
            Log::error("FFmpeg Command Failed: " . implode("\n", $output));
            return false;
        }

        if ($finishVideo == null) {
            // Calculate durations
            $finishVideo = $default_finish;
            $mainDuration = $this->getMediaDuration($in_app_video);
            $finishDuration = $this->getMediaDuration($finishVideo);
            $videoDuration = $mainDuration + $finishDuration;

            if ($videoSound && file_exists($videoSound) && filesize($videoSound) > 0) {
                // Calculate audio duration and loop count
                $audioDuration = $this->getMediaDuration($videoSound);
                $loopCount = ceil($videoDuration / $audioDuration);

                // Construct FFmpeg command with main video, finish video, and external audio
                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $in_app_video \
                    -i $finishVideo \
                    -i $videoSound \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [v0][v1]concat=n=2:v=1:a=0[v]; \
                    [2:a]aloop=loop=$loopCount:size=2147483647,apad=pad_dur=$videoDuration,atrim=0:$videoDuration"."[a]\" \
                    -map \"[v]\" -map \"[a]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $export_video 2>&1";
            } else {
                // Handle cases where no external audio is provided

                $concat_command = "taskset -c 0,1,2,3,4 ffmpeg -y \
                    -i $in_app_video \
                    -i $finishVideo \
                    -filter_complex \"\
                    [0:v]setsar=1[v0]; \
                    [1:v]setsar=1[v1]; \
                    [v0][v1]concat=n=2:v=1:a=0[v]\" \
                    -map \"[v]\" \
                    -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 96k $export_video 2>&1";
            }

            exec($concat_command, $output, $return_var);
            return $export_video;
        }
        return $in_app_video;
    }

        public function caret_pro_merging($videos, $carte_id) {

            $video = Videos::where('id',$carte_id)->with('users')->first();
            $licenseData = CLMLicense::where('id',$video->license_id)->first();
            $watermarked_text = isset($licenseData->caret_title) ? "^".$licenseData->caret_title : $video->video_description;

            $output = [];
            $return_var = 0;
            $default_audio = storage_path('app/Audio/carets_audio.mp3');
            $output_file = storage_path('app/caret_videos/' . $carte_id . time() . '_scale.mp4');
            // Define the target resolution
            $target_resolution = '540x960';
            $fps = '30';
            // Calculate the resolution for each video in the 2x2 grid
            $grid_width = 540 / 2;
            $grid_height = 960 / 2;
            // Scale each video to fit in the grid
            $scaled_videos = [];

            foreach ($videos as $key => $video) {
                    $path = storage_path('app/' . $video['path']);
                    // Check if the video has an audio track
                    $scaled_video = str_replace('.mp4', '_scaled.mp4', $path);
                    exec("ffmpeg -y -i $path -vf \"scale=$grid_width:$grid_height\" -c:a aac $scaled_video 2>&1", $output, $return_var);

                    if ($return_var != 0) {
                        Log::error("Scaling video failed for: $path", ['output' => $output]);
                    } else {
                        $scaled_videos[] = $scaled_video;
                    }
            }

            if (count($scaled_videos) == 1) {

                $audio_video = $scaled_videos[0];
                $newAudioFile = str_replace('_scaled.mp4', '.mp4', $audio_video);
                $audio_output_file = storage_path('app/Audio/Self/' . $carte_id . '.mp3');
                $watermarked_video = $output_file;

                exec("taskset -c 0,1,2,3,4 ffmpeg -y -i $audio_video -vf \"scale=$target_resolution,fps=$fps,drawtext=text='{$watermarked_text}':fontfile=/path/to/bold-font.ttf:fontcolor=white:fontsize=24:x=10:y=100\" -c:v libx264 -pix_fmt yuv420p $output_file");





                exec("ffmpeg -y -i $newAudioFile -q:a 0 -map a $audio_output_file 2>&1", $audio_output, $audio_return_var);

                return $watermarked_video;
            }

                // Determine the audio track from the largest video that has audio
            $durations = array_map([$this, 'getMediaDuration'], $scaled_videos);
            arsort($durations);  // Sort by duration, descending

            // Select the audio source
            $audio_video = $this->selectAudioVideo($scaled_videos)?? $default_audio;



            $newAudioFile = str_replace('_scaled.mp4','.mp4',$audio_video);
            $audio_file = storage_path('app/Audio/Self/'.$carte_id.'.mp3');

            $audio_command = "ffmpeg -y -i $newAudioFile -q:a 0 -map a $audio_file 2>&1";
            exec($audio_command, $audio_output, $audio_return_var);



            if ($audio_return_var !== 0) {
                $audio_file = storage_path('app/Audio/carets_audio.mp3');
                Log::error("Audio extraction failed", [
                    'command' => $audio_command,
                    'output' => $audio_output,
                    'return_code' => $audio_return_var,
                ]);
                // Optionally handle fallback logic here.
            }

                // Determine durations for looping
            $durations = array_map([$this, 'getMediaDuration'], $scaled_videos);
            $longest_video_duration = max($durations);
            $audio_duration = $this->getMediaDuration($audio_video);

            $audio_filter = $audio_duration > $longest_video_duration
                ? "atrim=0:$longest_video_duration"
                : "aloop=loop=" . (ceil($longest_video_duration / $audio_duration) - 1) . ":size=2147483647";

            // Process based on video count
            try {
                $font_path = "/path/to/bold-font.ttf";
                $cpu_cores = "0-4"; // Adjust based on available cores
                if (count($scaled_videos) == 2) {

                    $filter_complex = "
                        [0:v]scale=270:480[v0]; \
                        [1:v]scale=270:480[v1]; \
                        [v0][v1]vstack=inputs=2[stacked]; \
                        [stacked]pad=540:960:(ow-iw)/2:(oh-ih)/2[final]; \
                        [final]drawtext=text='{$watermarked_text}':fontfile=/path/to/bold-font.ttf:fontcolor=white:fontsize=24:x=10:y=100[v]
                    ";

                    exec("taskset -c 0,1,2,3,4 ffmpeg -y -i {$scaled_videos[0]} -i {$scaled_videos[1]} -i $audio_video -filter_complex \"$filter_complex,$audio_filter\" -map \"[v]\" -map 2:a -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 128k $output_file 2>&1", $output, $return_var);



                } else if (count($scaled_videos) == 3) {

                    $filter_complex = "
                        [0:v]scale=270:480[v0];
                        [1:v]scale=270:480[v1];
                        [2:v]scale=270:480, pad=540:480:(ow-iw)/2:(oh-ih)/2[v2];
                        [v0][v1]hstack=inputs=2[top];
                        [top][v2]vstack=inputs=2[v];
                        [v]drawtext=text='{$watermarked_text}':fontfile=/path/to/bold-font.ttf:fontcolor=white:fontsize=24:x=10:y=100[v]
                    ";

                    $audio_video_index = $audio_video === $default_audio ? count($scaled_videos) : array_search($audio_video, $scaled_videos);

                    exec("taskset -c 0,1,2,3,4 ffmpeg -y -i {$scaled_videos[0]} -i {$scaled_videos[1]} -i {$scaled_videos[2]} -i $audio_video -filter_complex \"$filter_complex,$audio_filter\" -map \"[v]\" -map {$audio_video_index}:a -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 128k $output_file 2>&1", $output, $return_var);

                } else if (count($scaled_videos) == 4) {

                    $filter_complex = "[0:v][1:v]hstack=inputs=2[top];[2:v][3:v]hstack=inputs=2[bottom];[top][bottom]vstack=inputs=2[v];[v]drawtext=text='{$watermarked_text}':fontfile=/path/to/font.ttf:fontcolor=white:fontsize=24:x=10:y=100[v]";


                    exec("taskset -c 0,1,2,3,4 ffmpeg -y -i {$scaled_videos[0]} -i {$scaled_videos[1]} -i {$scaled_videos[2]} -i {$scaled_videos[3]} -i $audio_video -filter_complex \"$filter_complex,$audio_filter\" -map \"[v]\" -map 4:a -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 128k $output_file 2>&1", $output, $return_var);


                }else if (count($scaled_videos) == 5) {
                    $filter_complex = "
                        [0:v]scale=270:480[v0];
                        [1:v]scale=270:480[v1];
                        [2:v]scale=270:480[v2];
                        [3:v]scale=270:480[v3];
                        [4:v]scale=270:480, pad=540:480:(ow-iw)/2:(oh-ih)/2[v4];
                        [v0][v1]hstack=inputs=2[top];
                        [v2][v3]hstack=inputs=2[bottom];
                        [top][bottom]vstack=inputs=2[top_part];
                        [top_part][v4]vstack=inputs=2[v];
                        [v]drawtext=text='{$watermarked_text}':fontfile=/path/to/bold-font.ttf:fontcolor=white:fontsize=24:x=10:y=100[v]
                    ";

                    exec("taskset -c 0,1,2,3,4 ffmpeg -y -i {$scaled_videos[0]} -i {$scaled_videos[1]} -i {$scaled_videos[2]} -i {$scaled_videos[3]} -i {$scaled_videos[4]} -i $audio_video -filter_complex \"$filter_complex,$audio_filter\" -map \"[v]\" -map 5:a -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 128k $output_file 2>&1", $output, $return_var);


                }
                else if (count($scaled_videos) == 6) {
                    $filter_complex = "
                        [0:v]scale=270:480[v0];
                        [1:v]scale=270:480[v1];
                        [2:v]scale=270:480[v2];
                        [3:v]scale=270:480[v3];
                        [4:v]scale=270:480[v4];
                        [5:v]scale=270:480[v5];
                        [v0][v1]hstack=inputs=2[top1];
                        [v2][v3]hstack=inputs=2[top2];
                        [top1][top2]vstack=inputs=2[top_part];
                        [v4][v5]hstack=inputs=2[bottom];
                        [top_part][bottom]vstack=inputs=2[v];
                        [v]drawtext=text='{$watermarked_text}':fontfile=/path/to/bold-font.ttf:fontcolor=white:fontsize=24:x=10:y=100[v]
                    ";

                    exec("taskset -c 0,1,2,3,4 ffmpeg -y -i {$scaled_videos[0]} -i {$scaled_videos[1]} -i {$scaled_videos[2]} -i {$scaled_videos[3]} -i {$scaled_videos[4]} -i {$scaled_videos[5]} -i $audio_video -filter_complex \"$filter_complex,$audio_filter\" -map \"[v]\" -map 6:a -c:v libx264 -crf 23 -preset medium -c:a aac -b:a 128k $output_file 2>&1", $output, $return_var);
                } else {
                    return false;
                }

                if ($return_var != 0) {
                    Log::error("Video Merging Failed", ['output' => $output]);
                    return false;
                }


                return $output_file;

            } catch (Exception $e) {
                Log::error("An error occurred during video merging", ['message' => $e->getMessage(), 'output' => $output]);
                return false;
            }
        }
        private function selectAudioVideo($videos)
        {
            // Get durations for all videos
            $durations = array_map([$this, 'getMediaDuration'], $videos);



            // Sort by duration in descending order
            arsort($durations);

            // Log sorted durations for debugging


            // Iterate through sorted videos and find the first one with an audio track
            foreach (array_keys($durations) as $index) {

                if ($this->hasAudioTrack($videos[$index])) {

                    return $videos[$index];
                }
            }

            // Log if no video with audio track is found
            Log::warning('No video with an audio track found.');
            return null;
        }

        private function getMediaDuration($filePath)
        {
            $command = "ffprobe -i $filePath -show_entries format=duration -v quiet -of csv=\"p=0\" 2>&1";
            exec($command, $output, $return_var);


            if ($return_var != 0 || empty($output)) {
                Log::error("Failed to get duration for file: $filePath", ['command' => $command, 'output' => $output]);
                return 0;
            }

            // Convert the duration string to a float
            return (float)$output[0];
        }







        private function hasAudioTrack($filePath)
        {
            // Escape the file path to handle spaces and special characters
            $escapedFilePath = $filePath;

            // FFprobe command to check for audio streams
            $command = "ffprobe -i $escapedFilePath -show_streams -select_streams a -loglevel error";

            // Execute the command
            exec($command, $output, $return_var);

            // Log an error if the command fails
            if ($return_var !== 0) {
                Log::error("Failed to check audio track for file: $filePath", [
                    'command' => $command,
                    'output' => $output,
                    'return_var' => $return_var,
                ]);
                return false;
            }

            // Check if output is not empty, indicating the presence of an audio track
            return !empty($output);
        }


        public function copyVideos($videos)
        {
            $localPaths = [];
            $errors = [];
            foreach ($videos as $video) {
                $videoUrl = $video['path'];

                $vidTitle = explode("/", $videoUrl);
                $fileName = end($vidTitle);
                $localPath = $videoUrl; // Relative path within the storage/app directory
                // Debugging: Check resolved local path
                $resolvedPath = storage_path('app/' . $localPath);

                // Check if the file exists locally
                if (!Storage::disk('local')->exists($localPath)) {
                    try {
                        if (Storage::disk('s3')->exists($videoUrl)) {

                            // Download the file from S3
                            $fileContents = Storage::disk('s3')->get($videoUrl);
                            // Ensure the directory exists
                            Storage::disk('local')->makeDirectory(dirname($localPath));
                            // Save the file locally
                            Storage::disk('local')->put($localPath, $fileContents);

                            $localPaths[] = $resolvedPath;
                        } else {
                            $errors[] = "Video not found on S3: $videoUrl";
                            Log::error("Video not found on S3: $videoUrl");
                        }
                    } catch (Exception $e) {
                        $errors[] = "Failed to download video: $fileName. Error: {$e->getMessage()}";
                        Log::error("Failed to download video: $fileName. Error: {$e->getMessage()}");
                    }
                } else {
                    $localPaths[] = $resolvedPath;

                }
            }
            if (count($localPaths) > 0) {
                return true;
            } else {
                return false;
            }
        }


    public function concatenateVideos() {

        ini_set("max_execution_time", 150000);
         $record = Videos::with(['videoads','videos','videosplashes','videosound'])
         ->where('is_caret', 1)
         ->where('caret_processing', 1)
         ->where('caret_atempts', '<', 3)
         ->first();
        if($record){
            $UPD['caret_processing'] = 2;
            $record->increment('caret_atempts');
            $record->update($UPD);
            $videos = $record->videos;
            $ads = $record->videoads->first();
            $introVideo = $record->videosplashes->where('pivot.finish', 0)->first();
            $finishVideo = $record->videosplashes->where('pivot.finish', 1)->first();

           // $ads = Ads::where('duration','>','0')->orderBy('created_at','desc')->first();
           // $videos = Videos::where('is_caret', 0)->orderBy('created_at','desc')->limit(3)->get();
            $introVideo = isset($introVideo) ? $introVideo->video_url : null;
            $finishVideo = isset($finishVideo) ? $finishVideo->video_url: null ;
            $splashVideos = [];

            if($introVideo){
                $splashVideos[]['path'] = $introVideo;
            }

            if($introVideo){
                $splashVideos[]['path'] = $finishVideo;
            }



            $videoSound = isset($record->videosound) ? $record->videosound->sound_url : null;
            $newRec = [];
            #videos
            foreach ($videos as $video) {
                $vid_title = explode("/", $video['video_raw_url']);
                $newRec[] = [
                    "id" => $video['id'],
                    'title' => end($vid_title),
                    'start_time' => Carbon::parse($video['start_time']),
                    'end_time' => Carbon::parse($video['end_time']),
                    'clipping_start' => '00:00:00',
                    'clipping_end' => '00:00:00',
                    'width' => null,
                    'height' => null,
                    'duration' => $video['duration'],
                    'path' => $video['video_raw_url'],
                ];
            }
            #ads
            if($ads){
                $vid_title = explode("/", $ads['video_url']);
                $newRec[] = [
                    "id" => $ads['id'],
                    'title' => end($vid_title),
                    'start_time' => null,
                    'end_time' => null,
                    'clipping_start' => '00:00:00',
                    'clipping_end' => '00:00:00',
                    'width' => null,
                    'height' => null,
                    'duration' => $ads['duration']*1000,
                    'path' => $ads['video_url'],
                ];
            }
            $resultedVideo = "";
            if ($newRec) {

                $this->copyVideos($newRec);
                $this->copyVideos($splashVideos);

                $gridVideo = $this->caret_pro_merging(collect($newRec),$record->id); // caret_based_merging algo

                //$gridVideo = $this->caret_based_merging(collect($newRec)); // caret_based_merging
                if($gridVideo){
                    //$gridVideo = isset($gridVideo) ? storage_path('app/'.$gridVideo) : null;
                    $in_app_video = str_replace('.mp4','_'.time().'_app.mp4',$gridVideo);
                    $export_video = str_replace('.mp4','_'.time().'.mp4',$gridVideo);
                    $intro = isset($introVideo) ? storage_path('app/'.$introVideo) : null;
                    $finish = isset($finishVideo) ? storage_path('app/'.$finishVideo) : null;
                    $sound_file = isset($videoSound) ? storage_path('app/'.$videoSound) : null;
                    if(isset($videoSound)){
                        Helper::copyS3Video($videoSound);
                    }
                    $resultedVideo = $this->finalMerging($intro,$gridVideo,$finish,$sound_file,$export_video,$in_app_video, $record->id);
                }else{
                    $UPD['caret_processing'] = 1;
                    $record->update($UPD);
                    return 'Not Record Found - gridVideo';
                }
                if($resultedVideo){
                    #inapp
                    $in_app_path = explode('app/', $in_app_video);

                    //$inappcontent = file_get_contents($resultedVideo);
                    $in_app_video_url =   $in_app_path[1];
                    //Helper::putS3Image($in_app_video_url, $inappcontent, 'public');

                    #export
                    $path = explode('app/', $resultedVideo);
                    //$image = file_get_contents($resultedVideo);
                    $video_export_url =  $path[1];
                    //Helper::putS3Image($video_raw_url, $image, 'public');


                    $upd['image_url'] = '';
                    $upd['video_url'] = $in_app_video_url;
                    $upd['video_export_url'] = $video_export_url;
                    $upd['video_raw_url'] = $in_app_video_url;
                    $upd['caret_processing'] = 0;
                    $res = Videos::where('id',$record->id)->update($upd);

                    $queueName = env('AUTO_CARET_QUEUE', 'default');
                    dispatch(new ProcessCaretJob($record->id))->onQueue($queueName);

                    return response()->json(['record' =>  $res]);
                    //$video_raw_url =   $folder. $data['video_id'] . '_raw_' . $name.'.'.$extension;
                }else{
                    $UPD['caret_processing'] = 1;
                    $record->update($UPD);
                    return 'Not Record Found - resultedVideo';
                }
            }else{
                $UPD['caret_processing'] = 1;
                $record->update($UPD);
                return 'Not Record Found.';
            }
            // Handle cases where there are not enough videos or errors
        }
        return 'Not enough videos to process.';
    }

    public function tagListing()
    {
        ini_set("max_execution_time", 90000);



        $message = "Record retrieved successfully!";

        $dis = SystemSettings::where('name','time')->first();
        if($dis->value > 1){
            $from = Carbon::now()->subMinutes($dis->value);
        }else{
            $from = Carbon::now()->subMinutes(10);
        }
        $sevenDaysAgo = Carbon::now()->subDays(7);

        $records = Carets::with([
            'license' => function($q) {
                $q->with(['intro', 'finish', 'defaultAd','defaultSound']);
            }
        ,'videos' => function($q) use ($sevenDaysAgo) {
            $q->with(['users'])
            ->where('is_caret', 0)
            ->where('is_active',1)
            // ->where('is_marked', 0)
            // ->where('is_processing_caret', 0)
            ->whereIn('tag_video_process', [1, 0, 3])
            ->where('tag_retry_count', '<', 3)
            ->where('is_allow_caretCreation', 1)
            ->where('videos.created_at', '>=', $sevenDaysAgo)
            ; // Specify table name for created_at
        }])
        ->whereHas('videos', function ($q) use ($sevenDaysAgo) {
            $q->where('is_caret', 0)
            ->whereIn('tag_video_process', [1, 0, 3])
            ->where('tag_retry_count', '<', 3)
            ->where('is_allow_caretCreation', 1)
            ->where('videos.created_at', '>=', $sevenDaysAgo)
            ->where('is_active',1)
            ; // Specify table name for created_at
        }, '>', 1) // Ensures that the number of videos is greater than 3
        ->get();
        $data = array();
        foreach($records as $record){
            if($record->license){
                $divd = 6;
                $license_id = null;
                if(isset($record->license->id)){
                    $license_id = $record->license->id;
                }
                if(isset($record->license->default_ad)){
                    $adsFile = storage_path('app/'.$record->license->default_ad);
                    if (!file_exists($adsFile)) {
                        $divd = 5;
                    }else{
                        $divd = 6;
                    }

                }else{
                    $divd = 6;
                }
                $divider = $this->getRemaining($record->videos->count(),$divd);
                $data = $this->TagMergingPro($record,$divider,$license_id);
            }else{
                //$divd = 6;
                $divd = 6;
                $divider = $this->getRemaining($record->videos->count(),$divd);
                $data = $this->TagMergingPro($record,$divider,null);
            }
        }
        return $this->sendSuccessResponse('data', $data, $message);
    }
    public function TagMergingPro($record,$divider,$license_id){
        $multiples = array_chunk($record->videos->toArray(), $divider,true);
        $data = [];
        foreach($multiples as $videos){
            $newRec = [];
            $newUser = [];
            $newVid = [];
            $hash_ids=[];
            $caret_ids=[];
            $tagged_ids=[];
            $desc_hashs=[];
            $desc_carets=[];
            $desc_taggeds=[];
            // if(count($videos) < $divd){
            //     continue;
            // }
            foreach($videos as $video){
                $vid_title = explode("/", $video['video_raw_url']);
                $users = $video['users'];
                $newRec[] = [
                    "id" => $video['id'],
                    'title' => end($vid_title),
                    'start_time' => Carbon::parse($video['start_time']),
                    'end_time' => Carbon::parse($video['end_time']),
                    'clipping_start' => '00:00:00',
                    'clipping_end' => '00:00:00',
                    'width' => null,
                    'height' => null,
                    'duration' => $video['duration'],
                    'path' => $video['video_raw_url'],
                ];
                foreach($users as $user){
                    $newUser[] = $user['id'];
                }
                if(isset($video['video_description'])){
                    $data2['video_description'] = trim($video['video_description']);
                    preg_match_all('/(?<!\w)#\w+/',$data2['video_description'],$hashes);
                    preg_match_all('/(?<!\w)@\w+/',$data2['video_description'],$mentions);
                    preg_match_all('/(?<!\w)\^\w+/',$data2['video_description'],$carets);
                    $hash_ids = array_unique (array_merge($hash_ids,$this->addHash($hashes)));
                    $caret_ids = array_unique (array_merge($caret_ids,$this->addCarets($carets)));
                    $tagged_ids = array_unique (array_merge($tagged_ids,$this->mentionedUsers($mentions)));
                    $desc_hashs = array_unique (array_merge($desc_hashs,$hashes[0]));
                    $desc_carets = array_unique (array_merge($desc_carets,$carets[0]));
                    $desc_taggeds = array_unique (array_merge($desc_taggeds,$mentions[0]));
                }
                $newVid[] = $video['id'];
                Videos::where('id',$video['id'])->update(['tag_video_process'=>1]);
            }
            //// return $newRec;
            $data2['is_active'] = 0;
            $data2['is_marked'] = 1;
            $data2['is_caret'] = 1;
            $data2['caret_processing'] = 1;
            $data2['is_allow_comments'] = 1;

            $data2['license_id'] = $license_id;

            $data2['video_description'] = '';
            if (!empty($desc_carets)) {
                $data2['video_description'] .= implode(', ', $desc_carets);
            }
            if (!empty($desc_hashs)) {
                // Add a comma if there’s already content in the description
                $data2['video_description'] .= ($data2['video_description'] ? ', ' : '') . implode(', ', $desc_hashs);
            }
            if (!empty($desc_taggeds)) {
                // Add a comma if there’s already content in the description
                $data2['video_description'] .= ($data2['video_description'] ? ', ' : '') . implode(', ', $desc_taggeds);
            }

            if (count($newRec) > 1){
                Videos::whereIn('id',$newVid)->update(['tag_video_process'=>2]);
            } else {
                Videos::whereIn('id',$newVid)->update(['tag_video_process'=>3]);
                Videos::whereIn('id',$newVid)->increment('tag_retry_count' , 1);
                continue;
            }
                $data2['video_title'] = "tag_based_merging";
                if(isset($record->license->default_sound)){
                    $data2['sound_id'] = $record->license->default_sound;
                }
                $Videos = Videos::create($data2);
                if(isset($record->license->user_id)){
                    $newUser[] =$record->license->user_id;
                }
                $Videos->users()->sync($newUser);
                $Videos->users()->increment('caret_count' , 1);
                $Videos->videos()->sync($newVid);
                ###################################################
                if(isset($record->license->caret_logo)){
                    Helper::copyS3Video($record->license->caret_logo);
                }

                if(isset($record->license->default_ad)){
                    $addata['video_id'] = $Videos->id;
                    $addata['ad_id'] = $record->license->default_ad;
                    VideoAds::create($addata);
                }
                if(isset($record->license->default_intro)){
                    $spdata['video_id'] = $Videos->id;
                    $spdata['splash_id'] = $record->license->default_intro;
                    $spdata['finish'] = 0;
                    VideoSplash::create($spdata);
                }
                if(isset($record->license->default_finish)){
                    $spdata['video_id'] = $Videos->id;
                    $spdata['splash_id'] = $record->license->default_finish;
                    $spdata['finish'] = 1;
                    VideoSplash::create($spdata);
                }
                #####################################################
                if(isset($hash_ids))
                    $Videos->hashs()->sync($hash_ids);
                if(isset($caret_ids))
                    $Videos->carets()->sync($caret_ids);
                if(isset($tagged_ids))
                        $Videos->tagged()->sync($tagged_ids);
                $user_age = $Videos->users()->withCount(['profile'=>function($q){
                    $q->select('dob');
                }])->orderBy('profile_count')->first();
                if(isset($user_age->profile_count)){
                    $age = Carbon::parse($user_age->profile_count)->diff(Carbon::now())->format('%y');
                    $ageRange = Ages::whereRaw('? between min and max', [$age])->first();
                    $ageData['age'] = $ageRange->id;
                    $Videos->update($ageData);
                }
                $data[] = $Videos;
        }
        return $data;
    }
    public function TagMerging($record,$divider){
        $multiples = array_chunk($record->videos->toArray(), $divider,true);
        $data = [];
        foreach($multiples as $videos){
            $newRec = [];
            $newUser = [];
            $newVid = [];
            $hash_ids=[];
            $caret_ids=[];
            $tagged_ids=[];
            $desc_hashs=[];
            $desc_carets=[];
            $desc_taggeds=[];
            foreach($videos as $video){
                $vid_title = explode("/", $video['video_raw_url']);
                $users = $video['users'];
                $newRec[] = [
                    "id" => $video['id'],
                    'title' => end($vid_title),
                    'start_time' => Carbon::parse($video['start_time']),
                    'end_time' => Carbon::parse($video['end_time']),
                    'clipping_start' => '00:00:00',
                    'clipping_end' => '00:00:00',
                    'width' => null,
                    'height' => null,
                    'duration' => $video['duration'],
                    'path' => $video['video_raw_url'],
                ];
                foreach($users as $user){
                    $newUser[] = $user['id'];
                }
                if(isset($video['video_description'])){
                    $data2['video_description'] = trim($video['video_description']);
                    preg_match_all('/(?<!\w)#\w+/',$data2['video_description'],$hashes);
                    preg_match_all('/(?<!\w)@\w+/',$data2['video_description'],$mentions);
                    preg_match_all('/(?<!\w)\^\w+/',$data2['video_description'],$carets);
                    $hash_ids = array_unique (array_merge($hash_ids,$this->addHash($hashes)));
                    $caret_ids = array_unique (array_merge($caret_ids,$this->addCarets($carets)));
                    $tagged_ids = array_unique (array_merge($tagged_ids,$this->mentionedUsers($mentions)));
                    $desc_hashs = array_unique (array_merge($desc_hashs,$hashes[0]));
                    $desc_carets = array_unique (array_merge($desc_carets,$carets[0]));
                    $desc_taggeds = array_unique (array_merge($desc_taggeds,$mentions[0]));
                }
                $newVid[] = $video['id'];
                Videos::where('id',$video['id'])->update(['tag_video_process'=>1]);
            }
            //// return $newRec;
            $data2['is_active'] = 0;
            $data2['is_marked'] = 1;
            $data2['is_caret'] = 1;
            $data2['is_allow_comments'] = 1;

            $data2['video_description'] = '';
            if (!empty($desc_carets)) {
                $data2['video_description'] .= implode(', ', $desc_carets);
            }
            if (!empty($desc_hashs)) {
                // Add a comma if there’s already content in the description
                $data2['video_description'] .= ($data2['video_description'] ? ', ' : '') . implode(', ', $desc_hashs);
            }
            if (!empty($desc_taggeds)) {
                // Add a comma if there’s already content in the description
                $data2['video_description'] .= ($data2['video_description'] ? ', ' : '') . implode(', ', $desc_taggeds);
            }

            if (count($newRec) > 1){
                Videos::whereIn('id',$newVid)->update(['tag_video_process'=>2]);
                $data2['video_url'] = $this->caret_based_merging(collect($newRec));
                $data2['image_url'] = str_replace('.mp4','.gif', $data2['video_url']);
                $data2['video_raw_url'] = str_replace('.mp4','_raw.mp4', $data2['video_url']);
            } else {
                Videos::whereIn('id',$newVid)->update(['tag_video_process'=>3]);
                Videos::whereIn('id',$newVid)->increment('tag_retry_count' , 1);
                continue;
            }
            $path = "app/".$data2['video_url'];
            $image_path = storage_path($path);
            $isExists = File::exists($image_path);
            if ($isExists) {
                $image = file_get_contents($image_path);
                //$path = config('app.env') . '/' . $folder . $person_id . '/' . $image_name;
                Helper::putS3Image($data2['video_raw_url'], $image, 'public');
                //unlink($image_path);
                $data2['video_title'] = "tag_based_merging";
                $Videos = Videos::create($data2);
                $Videos->users()->sync($newUser);
                $Videos->users()->increment('caret_count' , 1);
                $Videos->videos()->sync($newVid);
                //$Videos->videos()->update(['tag_video_process'=>2]);
                if(isset($hash_ids))
                    $Videos->hashs()->sync($hash_ids);
                if(isset($caret_ids))
                    $Videos->carets()->sync($caret_ids);
                if(isset($tagged_ids))
                        $Videos->tagged()->sync($tagged_ids);
                $user_age = $Videos->users()->withCount(['profile'=>function($q){
                    $q->select('dob');
                }])->orderBy('profile_count')->first();
                if(isset($user_age->profile_count)){
                    $age = Carbon::parse($user_age->profile_count)->diff(Carbon::now())->format('%y');
                    $ageRange = Ages::whereRaw('? between min and max', [$age])->first();
                    $ageData['age'] = $ageRange->id;
                    $Videos->update($ageData);
                }
                $queueName = env('UPLOAD_VIDEO_QUEUE', 'default');
                dispatch(new ProcessVideoJob($Videos->id))->onQueue($queueName);
                $data[] = $Videos;
            }else{
                // Videos::whereIn('id',$newVid)->update(['tag_video_process'=>3]);
                // Videos::whereIn('id',$newVid)->increment('tag_retry_count' , 1);
                $message = "tag_video_process - Failed";
            }
        }
        return $data;
    }
}
