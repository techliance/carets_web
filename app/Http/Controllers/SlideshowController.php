<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SlideshowController extends Controller
{
    /**
     * Generate a video slideshow from images.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateSlideshow(Request $request)
    {

        $imagesFolderPath = storage_path('app/GTV/images'); // Path to the images folder

        // Fetch all .jpg images from the folder
        $imagePaths = glob($imagesFolderPath . '/*.jpg');

        // Ensure that we only get image files and filter out any non-image files
        $imagePaths = array_filter($imagePaths, function($file) {
            return in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), ['jpg', 'jpeg', 'png', 'gif']);
        });



        // Example input data
        $exampleInput = [
            "images" => $imagePaths, // Dynamic image paths
            "output_file" => storage_path('app/GTV/slideshow.mp4'),
            "slide_duration" => 2, // Ensure this is greater than 0
            "fps" => 30,
            "width" => 1920,
            "height" => 1080,
            "audio_file" => storage_path('app/GTV/audios/default_audio.mp3'), // Path to background music
            "start_videos" => [
                storage_path('app/GTV/videos/start_0001.mp4'),
                storage_path('app/GTV/videos/start_0002.mp4'),
                storage_path('app/GTV/videos/start_0003.mp4'),
            ],
            "end_videos" => [
                storage_path('app/GTV/videos/end_0001.mp4'),
                storage_path('app/GTV/videos/end_0002.mp4'),
                storage_path('app/GTV/videos/end_0003.mp4'),
                storage_path('app/GTV/videos/end_0004.mp4'),
            ],
        ];

        // Directly use the example input (no validation)
        $images = $exampleInput['images'];
        $outputFile = $exampleInput['output_file'];
        $slideDuration = $exampleInput['slide_duration'];
        $fps = $exampleInput['fps'];
        $width = $exampleInput['width'];
        $height = $exampleInput['height'];
        $audioFile = $exampleInput['audio_file'];
        $startVideos = $exampleInput['start_videos'];
        $endVideos = $exampleInput['end_videos'];

        // Prepare output path
        $outputPath = storage_path('app/GTV'); // You can modify the path if needed

        try {
            // Step 1: Scale images and create individual videos
            $videoFiles = $this->resizeImagesAndCreateVideos($images, $outputPath, $width, $height);

            // Step 2: Generate FFmpeg command to merge videos and handle audio looping/trimming
            $concatFilePath = $outputPath . '/concat_list.txt';

            // Create a text file with the list of video files for FFmpeg
            $concatFileContent = '';
            foreach ($videoFiles as $videoFile) {
                $concatFileContent .= "file '" . $videoFile . "'\n";
            }
            file_put_contents($concatFilePath, $concatFileContent);

            // FFmpeg command for merging videos and handling audio
            $command = "ffmpeg -y -f concat -safe 0 -i " . escapeshellarg($concatFilePath) . " -i " . escapeshellarg($audioFile) . " -c:v libx264 -preset medium -crf 23 -r {$fps} -pix_fmt yuv420p -c:a aac -shortest " . escapeshellarg($outputFile);
            exec($command . " 2>&1", $output, $returnVar);

            // Log the output from FFmpeg
            info("FFmpeg Output: ", $output);

            if ($returnVar !== 0) {
                return response()->json([
                    'success' => false,
                    'message' => 'Error generating slideshow with audio',
                    'error' => implode("\n", $output),
                ], 500);
            }

            // Step 3: Merge start videos
            $startConcatFile = $outputPath . '/start_concat.txt';
            $startConcatContent = '';
            foreach ($startVideos as $startVideo) {
                $startConcatContent .= "file '" . $startVideo . "'\n";
            }
            file_put_contents($startConcatFile, $startConcatContent);

            $mergedStart = $outputPath . '/merged_start.mp4';
            exec("ffmpeg -y -f concat -safe 0 -i " . escapeshellarg($startConcatFile) . " -c copy " . escapeshellarg($mergedStart));

            // Step 4: Merge end videos
            $endConcatFile = $outputPath . '/end_concat.txt';
            $endConcatContent = '';
            foreach ($endVideos as $endVideo) {
                $endConcatContent .= "file '" . $endVideo . "'\n";
            }
            file_put_contents($endConcatFile, $endConcatContent);

            $mergedEnd = $outputPath . '/merged_end.mp4';
            exec("ffmpeg -y -f concat -safe 0 -i " . escapeshellarg($endConcatFile) . " -c copy " . escapeshellarg($mergedEnd));

            // Step 5: Final merge
            $finalConcatFile = $outputPath . '/final_concat.txt';
            file_put_contents($finalConcatFile, "file '" . $mergedStart . "'\nfile '" . $outputFile . "'\nfile '" . $mergedEnd . "'\n");

            $finalOutput = $outputPath . '/final_video.mp4';
            exec("ffmpeg -y -f concat -safe 0 -i " . escapeshellarg($finalConcatFile) . " -c copy " . escapeshellarg($finalOutput));

            // Cleanup temporary files
            // unlink($concatFilePath);
            // unlink($startConcatFile);
            // unlink($endConcatFile);
            // unlink($finalConcatFile);

            return response()->json([
                'success' => true,
                'message' => 'Slideshow with start and end videos generated successfully',
                'output_file' => $finalOutput,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'An error occurred during slideshow generation',
                'error' => $e->getMessage(),
            ], 500);
        }
    }


    /**
     * Scale images to fit within the desired resolution.
     *
     * @param array $imagePaths
     * @param string $outputPath
     * @param int $width
     * @param int $height
     * @return array
     */
  ##################################################################################
  public function resizeImagesAndCreateVideos($imagePaths, $outputPath, $width, $height)
  {
      $resizedImages = $this->scaleImages($imagePaths, $outputPath, $width, $height);
      $videoFiles = [];
      $effects = [
        'fade=t=in:st=0:d=1', // Fade-in effect
        'fade=t=out:st=2:d=1', // Fade-out effect
        'zoompan=z=\'zoom+0.01\':d=125', // Zoom-in effect
        'zoompan=z=\'zoom-0.01\':d=125', // Zoom-out effect
        'crop=in_w-50:in_h-50', // Crop effect
        'rotate=PI/4*t', // Rotate effect
        'transpose=1', // Rotate 90 degrees clockwise
        'transpose=2', // Rotate 90 degrees counterclockwise
        'vflip', // Vertical flip
        'hflip', // Horizontal flip
        'scale=iw*1.5:ih*1.5', // Scale up
        'scale=iw/2:ih/2', // Scale down
        'colorchannelmixer=.3:.4:.3:0:.3:.4:.3:0:.3:.4:.3', // Sepia effect
        'boxblur=5:1', // Blur effect
        'eq=brightness=0.06:saturation=1.5', // Increase brightness and saturation
        'drawbox=x=10:y=20:w=200:h=60:color=red@0.5', // Draw a red box
        'lutrgb=r=2:g=0.5:b=0.5', // Change RGB channels
        'lenscorrection=k1=-0.1:k2=0.2', // Fish-eye lens effect
        'wave=frequency=2:amplitude=10', // Wavy distortion effect
        'perspective=x0=0:y0=0:x1=iw:y1=0:x2=iw:y2=ih:x3=0:y3=ih', // Perspective effect
        'geq=lum_expr=\'clip(100*sin(2*PI*t/3)+128,0,255)\'', // Pulsating brightness
        'split [original][copy];[copy] boxblur=10 [blurred];[original][blurred] overlay', // Partial blur
        'chromakey=green:0.1:0.2', // Green screen removal
        'noise=alls=20:allf=t+u', // Add random noise
        //'drawgrid=w=iw/3:h=ih/3:t=2:c=yellow', // Draw a grid
        //'tblend=all_mode=screen:all_opacity=0.7', // Blending effect
        //'edgedetect=low=0.1:high=0.4', // Edge detection
        //'tblend=all_mode=difference:all_opacity=0.3', // Difference blend effect
        //'palettegen', // Generate color palette
        //'negate', // Invert colors
        //'curves=r=\'0/0 0.5/1 1/1\':g=\'0/0 0.5/1 1/1\':b=\'0/0 0.5/1 1/1\'', // Custom color curves
    ];



      foreach ($resizedImages as $index => $resizedImage) {
          if (!file_exists($resizedImage)) {
              file_put_contents('missing_images_debug.txt', "{$resizedImage} not found\n", FILE_APPEND);
              continue;
          }

          $selectedEffect = $effects[$index % count($effects)];
          $timestamp = microtime(true);
          $outputVideoFile = $outputPath . '/' . uniqid($timestamp . '_', true) . '.mp4';

          $command = "ffmpeg -loop 1 -i {$resizedImage} -vf \"{$selectedEffect}\" -t 3 -c:v libx264 -pix_fmt yuv420p {$outputVideoFile}";
          file_put_contents('ffmpeg_commands_debug.txt', $command . "\n", FILE_APPEND);



          try {
              exec($command . " 2>&1", $output, $resultCode);
              if ($resultCode !== 0) {
                  throw new \Exception("Failed for {$resizedImage}: " . implode("\n", $output));
              }
          } catch (\Exception $e) {
              file_put_contents('ffmpeg_errors_debug.txt', "Error for {$resizedImage}: " . $e->getMessage() . "\n", FILE_APPEND);
              continue;
          }

          $videoFiles[] = $outputVideoFile;
      }

      return $videoFiles;
  }




  ##################################################################################
    public function scaleImages($imagePaths, $outputPath, $width, $height)
    {
        $resizedImages = [];

        foreach ($imagePaths as $imagePath) {
            // Generate a timestamp-based name for the resized image
            $timestamp = time();
            $outputFile = $outputPath . '/' . $timestamp . '_' . basename($imagePath);
            $resizedImages[] = $outputFile;

            // Get the dimensions of the image to keep the resolution unchanged
            $imageSize = getimagesize($imagePath);
            $imageWidth = $imageSize[0];
            $imageHeight = $imageSize[1];

            // Calculate the scale factor based on the image's dimensions and target resolution
            $scaleFactor = min($width / $imageWidth, $height / $imageHeight);

            // If the image is larger than the target resolution, scale it down
            if ($scaleFactor < 1) {
                $newWidth = round($imageWidth * $scaleFactor);
                $newHeight = round($imageHeight * $scaleFactor);
            } else {
                // If the image is smaller than the target resolution, keep the original size
                $newWidth = $imageWidth;
                $newHeight = $imageHeight;
            }

            // Construct FFmpeg command to resize the image while maintaining the aspect ratio
            $command = "ffmpeg -i {$imagePath} -vf scale={$newWidth}:{$newHeight} {$outputFile}";

            // Execute the command
            $output = null;
            $resultCode = null;
            exec($command, $output, $resultCode);

            // Check if the command was successful
            if ($resultCode !== 0) {
                throw new \Exception("Failed to resize image: {$imagePath}");
            }
        }

        return $resizedImages;
    }
}
