<?php

namespace App\Http\Controllers;

use App\Ads;
use App\AdsIndexing;
use App\AdViews;
use App\Ages;
use App\CampaignsAds;
use App\Carets;
use App\Hashs;
use App\Helpers\Helper;
use App\Jobs\ProcessAdsJob;
use App\Sounds;
use App\User;
use App\UserFollows;
use App\UserPrivacy;
use App\UserProfiles;
use App\VideoReports;
use App\Videos;
use App\VideoViews;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Profiler\Profile;

// use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
// use RuntimeException;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id)
    {

        $user_id = (int)$user_id > 0 ? $user_id : null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $search = $request->input('search');
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;


        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Ads::select('*')->with(['license',
            'user'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
            }]);
        }])
        ->addSelect([DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")])
        ->when($filter,function($q) use ($filter){
                $q->orWhere('ad_title','like', '%' . $filter . '%');
                $q->orWhere('license_id','like', '%' . $filter . '%');
            })
        ->when($search, function($q) use ($search){
            if (isset($search['fromDate']) && isset($search['toDate'])) {
                $q->where('created_at', '>=', $search['fromDate']);
                $q->where('created_at', '<=', $search['toDate']);
            } elseif (isset($search['fromDate']) && !isset($search['toDate'])) {
                $q->where('created_at', '>=', $search['fromDate']);
            } elseif (!isset($search['fromDate']) && isset($search['toDate'])) {
                $q->where('created_at', '<=', $search['toDate']);
            }
        })

        ->when($user_id > 0,function($q) use ($user_id){
            $q->where('user_id', $user_id);
        })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }


    public function adSatus(Request $request) {
        $ad_id = $request->ad_id;
        $ad = ads::where('id', $ad_id)->first();
        if($ad){
            if($ad->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $ad->update($data);
            $this->addRemoveIndexing ($ad_id);
            return response()->json([ 'data' => $ad, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function addRemoveIndexing ($ad_id) {
        $ad = ads::where('id', $ad_id)
        ->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")])
        ->first();
        if($ad){
            $AdsIndexing['video_url'] = $ad->video_url;
            $AdsIndexing['image_url'] = $ad->image_url;
            $AdsIndexing['description'] = $ad->ad_description;
            AdsIndexing::where('ad_id', $ad_id)->update($AdsIndexing);
        }
    }


    public function adBlock($id) {
        $ad_id = $id;
        $ad = ads::where('id', $ad_id)->first();
        if($ad){
            if($ad->is_blocked == 1){
                $data['is_blocked'] = 0;
            }else{
                $data['is_blocked'] = 1;
            }
            $ad->update($data);
            $this->addRemoveIndexing ($ad_id);
            return response()->json([ 'data' => $ad, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function destroy($id)
    {
        try {

        $ads = ads::where('id', $id)->first();
        CampaignsAds::where('ad_id', $id)->delete();
        AdsIndexing::where('ad_id', $id)->delete();
        $ads->delete();
        return response()->json(['data'=>'true','status'=>'success', 'message' => 'Deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this ad, Linked with compaign","status"=>"error", "msg"=> 'You can not delete this ad, Linked with compaign'], 400);
        }
    }



    public function store(Request $request)
    {
        $data = $request->all();

        $validation_rules = [
            'user_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $input['videoFile'] = $request->input('videoFile');
        $request->hasFile('video_url');

        if($request->hasFile('video_url') == false && !isset($input['videoFile'])){
            return response()->json( ["error"=>"Please select the video file","status"=>"error", "message"=>"invalidate" ], 400);
        }

        if(isset($data['user_id']))
            $data2['user_id'] = trim($data['user_id']);

        if(isset($data['license_id']))
            $data2['license_id'] = trim($data['license_id']);

        if(isset($data['ad_description']))
            $data2['ad_description'] = trim($data['ad_description']);

        if(isset($data['ad_title']))
            $data2['ad_title'] = trim($data['ad_title']);

        if(isset($data['is_active']))
            $data2['is_active'] = trim($data['is_active']);

        if(isset($data['ad_button_link']))
            $data2['ad_button_link'] = trim($data['ad_button_link']);

        if(isset($data['ad_button_text']))
            $data2['ad_button_text'] = trim($data['ad_button_text']);

        if(isset($data['duration']))
            $data2['duration'] = trim($data['duration']);

        if(isset($data['ad_id'])){
            $adv = Ads::where('id', $data['ad_id'])->first();
            if($adv) {
                $adv->update($data2);
            }
        }else{
            $adv = Ads::create($data2);
            $data['ad_id'] = $adv->id;
        }
        $this->addRemoveIndexing ($data['ad_id']);







        //  if ($request->hasFile('image_url')) {
        //     $input_img = $request->file('image_url');

        //     $image = $input_img;
        //     $name = time().'.'.$image->getClientOriginalExtension();
        //     $file_path =  'Videos/' . $data['ad_id'] . '_' . $name;
        //     $image = file_get_contents($image);
        //     $confirmUpload = Helper::putServerImage($file_path, $image, 'public');
        //     $image_url = $file_path;

        //     if($image_url){
        //         $data2['image_url'] = $image_url;
        //         $adv->update($data2);
        //     }

        // }


        // $input['image'] = $request->input('image');
        // if(isset($input['image'])) {
        //     $imageData = $input['image'];
        //     if(isset($imageData['file'])){
        //         $file       = $imageData['file'];
        //         $file_name  = $imageData['name'];
        //         $image = file_get_contents($file);

        //         $file_path =  'Videos/' . $data['ad_id'] . '_' . $file_name;
        //         $confirmUpload = Helper::putServerImage($file_path, $image, 'public');
        //         if ($confirmUpload){
        //             $newdata['image_url'] = $file_path;
        //             $adv->update($newdata);
        //         }
        //     }

        // }


        // if ($request->hasFile('video_url')) {
        //     $input_img = $request->file('video_url');

        //     $image = $input_img;
        //     $extension = $image->getClientOriginalExtension();
        //     $folder = 'Videos/';
        //     $name = time();
        //     $file_path =   $folder. $data['ad_id'] . '_' . $name.'.'.$extension;
        //     $image = file_get_contents($image);
        //     $confirmUpload = Helper::putServerImage($file_path, $image, 'public');
        //     $video_url = $file_path;

        //     // if(isset($data['is_allow_caretCreation']) && $data['is_allow_caretCreation'] == 1){
        //     //     Helper::putServerImage($file_path, $image, 'public');
        //     // }
        //     if($video_url){
        //         $uploadFile['video_url'] = $video_url;
        //         //$uploadFile['image_url'] = 'Images/'. $data['ad_id'] . '_' . $name.'.gif';
        //         //return $pic = Storage::disk('s3')->get(config('app.awsurl').$video_url);
        //         $adv->update($uploadFile);
        //     }

        // }

        if(isset($input['videoFile'])) {
            $FileData = $input['videoFile'];
            Log::info("Step1: Record Insertion.".date('Y-m-d H:i:s', time()));
            if(isset($FileData['file'])){
                $file       = $FileData['file'];
                $file_name  = $FileData['name'];

                $file_name = preg_replace('/[^a-zA-Z0-9-_\.]/', '_', $file_name);

                $video = file_get_contents($file);

                $file_path =  'Ads/' . $data['ad_id'] . '_' .time().$file_name;
                $confirmUpload = Helper::putServerImage($file_path, $video, 'public');

                $originalVideoPath = storage_path('app/' . $file_path);

                Log::info("Step2: prepareVideo.".date('Y-m-d H:i:s', time()));
                $scaledVideoPath = Helper::prepareVideo($originalVideoPath);

                Log::info("Step3: prepareVideo DONE".date('Y-m-d H:i:s', time()));
                if ($scaledVideoPath){

                    $outputGifPath = str_replace('.mp4', '.gif', $scaledVideoPath);
                    $gifWidth = 320;
                    //$uploadFile['video_url'] = $file_path;
                    //$uploadFile['image_url'] = str_replace('Videos/','Images/',str_replace('.mp4','.gif',$file_path));
                    $video = file_get_contents($scaledVideoPath);

                    // Upload to S3
                    $fileInfo = pathinfo($scaledVideoPath);
                    $extension = strtolower($fileInfo['extension']);

                    $file_link = 'Ads' . '/'.$data['ad_id'].'_'.time(). '.mp4';
                    $confirmUpload = Helper::putS3Image($file_link, $video, 'public');
                    Helper::putServerImage($file_link, $video, 'public');

                    if ($confirmUpload) {
                        $image_url = Helper::generateGif($file_link);
                        $original_image_url = storage_path('app/' . $image_url);
                        $image = file_get_contents($original_image_url);
                        Helper::putS3Image($image_url, $image, 'public');
                        $uploadFile['image_url'] = $image_url;
                        $uploadFile['video_url'] = $file_link;
                        $adv->update($uploadFile);
                    }


                }

            }
        }
        //$this->newhandle($data['ad_id']);
        $return = Ads::select(['*',
        DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")])
        ->where('id', $data['ad_id'])->first();

        //dispatch(new ProcessAdsJob($data['ad_id']));

        return response()->json([ 'data' => $return, 'message' => 'Record stored successfully!'], 200);
    }






    public function newhandle($ad_id=0)
    {

        $converted_path = storage_path('app/uploads/');
        $watermark_path = storage_path('app/uploads/Videos/watermark.png');

        $video = Ads::where('id',$ad_id)->first();
        if($video){
            if($video->video_url){
                 #waterMarkVideo
                $waterMark = Helper::scaleDownVideo($video->video_url,$watermark_path,$converted_path);

                $path = "app/uploads/".$waterMark;

                $video_path = storage_path($path);


                $isExists = File::exists($video_path);

                if ($isExists) {

                    $image = file_get_contents($video_path);
                    Helper::putS3Image($waterMark, $image, 'public');
                }else{

                }

            }

        }
    }
    public function show($id)
    {

        $data = ads::select([
            '*',
            DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS videoFile"),
            DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image")
        ])
        ->with([
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            }])
        ->where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);

    }

    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }






    public function watchCount(Request $request)
    {
        $data = $request->all();

        $data2['ad_id'] = trim($data['ad_id']);
        $data2['user_id'] = trim($data['user_id']);
        if(isset($data['campaign_ad_id'])){
            $CampaignsAds = CampaignsAds::where('id', $data['campaign_ad_id'])->first();
            if($CampaignsAds){
                $data2['campaign_id'] = $CampaignsAds->campaign_id;
            }
        }


        $data2['position'] = isset($data['position']) ? trim($data['position']) : null;
        $data2['location'] = isset($data['location']) ? trim($data['location']) : null;
        // will get age, gender, location, interests from users profile
        $user_profile = UserProfiles::where('user_id', $data2['user_id'])->first();
        if($user_profile){
            $age = $user_profile->dob ? Carbon::parse($user_profile->dob)->age : 0;
            $ages = Ages::where('min','<=', $age)->where('max','>', $age)->first();
            $data2['age'] = $ages ? $ages->id : '';
            $data2['gender'] = $user_profile->gender;
            $data2['interests'] = $user_profile->industry_id;
        }
        if(isset($data['trans_type'])){
            if($data['trans_type'] == 'watch'){
                $data2['complete'] = 0;
                $data2['clicked'] = 0;
                $VideoViews = AdViews::create($data2);
                if(isset($CampaignsAds->ad_id))
                    Ads::where('id',$data2['ad_id'])->increment('watch_count' , 1);

                CampaignsAds::where('id',$data['campaign_ad_id'])->increment('watch_count' , 1);

                return response()->json([ 'data' => $VideoViews,'status'=>'success', 'message' => 'Stored successfully!'], 200);
            }
            else if($data['trans_type'] == 'complete'){
                $data2['complete'] = 1;
                $VideoViews = AdViews::create($data2);
                return response()->json([ 'data' => $VideoViews,'status'=>'success', 'message' => 'Stored successfully!'], 200);

            }else if($data['trans_type'] == 'click'){

                $data2['clicked'] = 1;
                $VideoViews = AdViews::create($data2);
                return response()->json([ 'data' => $VideoViews,'status'=>'success', 'message' => 'Stored successfully!'], 200);

            }
        }else{
            $data2['complete'] = 0;
            $data2['clicked'] = 0;

            // $exist = AdViews::where('ad_id', $data['ad_id'])->where('user_id', $data['user_id'])->first();
            // if(!$exist){
                $VideoViews = AdViews::create($data2);

                if(isset($CampaignsAds->ad_id))
                    Ads::where('id',$CampaignsAds->ad_id)->increment('watch_count' , 1);

                CampaignsAds::where('id',$data2['ad_id'])->increment('watch_count' , 1);
                return response()->json([ 'data' => $VideoViews,'status'=>'success', 'message' => 'Stored successfully!'], 200);
            //}

        }


        return response()->json([ 'data' => [],'status'=>'success', 'message' => 'Stored successfully!'], 200);
    }



    // public function destroyCategory($id)
    // {
    //     $SoundCategories = SoundCategories::where('id', $id)->first();
    //     $SoundCategories->delete();
    //     return response()->json(['','status'=>'success', 'message' => 'Deleted successfully!'], 200);
    // }

    // public function destroyReport($id)
    // {
    //     $SoundReports = SoundReports::where('id', $id)->first();
    //     $SoundReports->delete();
    //     return response()->json(['','status'=>'success', 'message' => 'Deleted successfully!'], 200);
    // }

    public function getUsers(Request $request){
        $users = User::orderBy('id','DESC')
        ->select('id as value', 'username as label', 'email as email')
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }

    public function sendNotification(Request $request){
        $device_tokens = 'ceqpGeNYQh6GSkeuUKSrWQ:APA91bEI4x3VHFHyplsBH9GtrHX3suqttPVKOMKRu3mfLnefqBv0A5VTzJ3cV-TII8y6WF_HZUiKyAN5bpOEAFXLoJHvHSTHQHSt4fuiVms2EgpGKp3SJeMEE9Dqspk33yHk376xXzT5';
        $res = Helper::sendFCMNotification($device_tokens, ['title' => 'New Video', 'body' => 'video uploaded successfully',
        'data' => [
            'title' => 'New Video', 'body' => 'video uploaded successfully'
            ]
        ]);
        return $res;
    }



    public function getAdsData(Request $request, $user_id){
        $signed_id = (int)$user_id > 0?$user_id:null;
        $licenseId = $request->input('licenseId');
        $filter = $request->input('filter');
        // Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $users = Ads::orderBy('id','DESC')
        ->where('duration','!=', 0)
        ->select('id as value', 'ad_title as label', 'ad_button_link', 'ad_description',
        DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))
        ->where(function($q) use ($filter){
            $q->where('ad_title','like', '%' . $filter . '%');
        })
        ->where(function($q) use ($signed_id){
            $q->where('user_id', $signed_id);
        })
        ->when($licenseId, function ($q) use ($licenseId) {
            $q->where(function ($query) use ($licenseId) {
                $query->where('license_id', $licenseId)
                      ->orWhereNull('license_id'); // Check for null values explicitly
            });
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }
    public function getAdsDataRandom(Request $request, $user_id){
        $signed_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $licenseId = $request->input('licenseId');
        // Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $users = Ads::orderBy('id','DESC')
        ->select('id as value', 'ad_title as label', 'ad_button_link', 'ad_description',
        DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))
        ->where(function($q) use ($filter){
            $q->where('ad_title','like', '%' . $filter . '%');
        })
       ->when($signed_id, function ($query, $signed_id) {
           $query->where('user_id', '!=', $signed_id);
       })
        ->when($licenseId, function ($q) use ($licenseId) {
            $q->where('license_id', $licenseId);
        })
        ->limit(20)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }
}
