<?php

namespace App\Http\Controllers;

use App\HashCategory;
use App\Hashs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HashCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

       if ($filter == '' && $sort != ''){

           $sortEx     = explode(',', $sort);
           $sortName   = $sortEx[0];
           $orderByEx  = explode(":", $sortEx[0]);
           $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

       }

        $data = HashCategory::where(function($q) use ($filter){
               $q->where('title','like', '%' . $filter . '%');
           })
           ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
               $query->orderBy($sortName, $orderType);
           })
           ->when($sortName===null,function($q){
               $q->orderBy('created_at','desc');
           })
            ->paginate($pagination);


        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }
    public function store(Request $request)
    {
        $data = $request->all();
        $data2['title'] = trim($data['title']);
        $data2['is_active'] = trim($data['is_active']);
       
        if(isset($data['id'])){
            $Hash = HashCategory::where('id', $data['id'])->first();
            if($Hash) {
                $Hash->update($data2);
            }
        }else{
            $Hash = HashCategory::create($data2);
            $data['hash_id'] = $Hash->id;
        }

        return response()->json([ 'data' => $data, 'message' => 'Record stored successfully!'], 200);
    }
    public function show($id)
    {
        $data = HashCategory::where('id', $id)->first();
       
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        $Hash = HashCategory::where('id', $id)->first();
        $Hash->delete();
        return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
    }
    public function hashCategorySatus(Request $request) {
        $category_id = $request->id;
        $Hash = HashCategory::where('id', $category_id)->first();
        if($Hash){
            if($Hash->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $Hash->update($data);
            return response()->json([ 'data' => $Hash, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }
}
