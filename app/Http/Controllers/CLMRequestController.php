<?php

namespace App\Http\Controllers;
use App\Notifications\CorporateRequestNotification;
use App\Notifications\RequestNotificationForCorporate;
use Illuminate\Support\Facades\Notification;
use App\AdPayments;
use App\AdsIndexing;
use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdCampaigns;
use App\AdCards;
use App\AdPlans;
use App\Ads;
use App\AdStatus;
use App\Ages;
use App\CampaignDetails;
use App\Campaigns;
use App\CampaignsAds;
use App\Carets;
use App\CLMLicense;
use App\CLMRequest;
use App\CLMRequestStatus;
use App\CLMPayments;
use App\CaretSearchData;
use App\CLMPlans;
use App\CaretPricing;
use App\CLMOrderItems;
use App\Countries;
use App\Gender;
use App\Hashs;
use App\Notifications\RequestStatusNotificationForCorporate;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PharIo\Manifest\License;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Customer;
use Stripe\Plan;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

class CLMRequestController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id=null)
    {
        $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $search = "";
        $status_id=null;

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;
        if($request->input('search')){
            $search = $request->input('search');

        }
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CLMRequest::with(['plan', 'statusA', 'pricing',
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            }])
            ->where(function($q) use ($filter){
                $q->where('caret_title','like', '%' . $filter . '%');
                $q->orWhere('company_name','like', '%' . $filter . '%');
                $q->orWhere('company_address','like', '%' . $filter . '%');
                $q->orWhere('name','like', '%' . $filter . '%');
                $q->orWhere('designation','like', '%' . $filter . '%');
                $q->orWhere('company_description','like', '%' . $filter . '%');
                $q->orWhereHas('user', function($q) use ($filter) {
                    $q->where('email', 'like', '%' . $filter . '%');
                });
            })
            // ->when($search, function($q) use ($search){
            //     if ($search['fromDate'] && $search['toDate']) {
            //         $q->where('created_at', '>=', $search['fromDate']);
            //         $q->where('created_at', '<=', $search['toDate']);
            //     } elseif ($search['fromDate'] && !$search['toDate']) {
            //         $q->where('created_at', '>=', $search['fromDate']);
            //     } elseif (!$search['fromDate'] && $search['toDate']) {
            //         $q->where('created_at', '<=', $search['toDate']);
            //     }
            // })

            // ->when($user_id > 0,function($q) use ($user_id){
            //     $q->where('user_id', $user_id);
            // })
            ->when($user_id > 0,function($q) use ($user_id){
                $q->where('order_id', $user_id);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);



        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }


    public function store(Request $request)
    {
        $data = $request->all();

        $data2['caret_title'] = trim($data['caret_title']);
        $data2['user_id'] = trim($data['user_id']);
        $data2['company_name'] = trim($data['company_name']);
        $data2['status'] = 1;
        if(isset($data['status_id'])){
            $data2['status_id'] = trim($data['status_id']);
        }
        if(isset($data['order_item_id'])){
            $data2['order_item_id'] = trim($data['order_item_id']);
        }
        if(isset($data['company_address'])){
            $data2['company_address'] = trim($data['company_address']);
        }
        if(isset($data['company_description'])){
            $data2['company_description'] = trim($data['company_description']);
        }
        if(isset($data['subscription_plan'])){
            $data2['subscription_plan'] = trim($data['subscription_plan']);
        }
        if(isset($data['subscription_amount'])){
            $data2['subscription_amount'] = trim($data['subscription_amount']);
        }
        if(isset($data['name'])){
            $data2['name'] = trim($data['name']);
        }
        if(isset($data['email'])){
            $data2['email'] = trim($data['email']);
        }
        if(isset($data['phone'])){
            $data2['phone'] = trim($data['phone']);
        }
        if(isset($data['designation'])){
            $data2['designation'] = trim($data['designation']);
        }
        if (isset($data2['caret_id']) && !empty($data2['caret_id'])) {
            $data2['caret_id'] = trim($data2['caret_id']);
        } else {
            $caret_id = $this->addCarets($data2['caret_title']);
            $data2['caret_id'] = trim($caret_id);
        }

        $stripe_id = null;
        $interval_count = 0;
        if(isset($data['subscription_plan'])){
            $CaretPricing = CaretPricing::where('id', $data['subscription_plan'])->first();

            if ($CaretPricing) {
                // Check the subscription plan and fetch the corresponding Stripe ID
                if ($CaretPricing->stripe_id_one_year) {
                    $stripe_id = $CaretPricing->stripe_id_one_year;
                    $interval_count = 1;
                } elseif ($CaretPricing->stripe_id_two_year) {
                    $stripe_id = $CaretPricing->stripe_id_two_year;
                    $interval_count = 2;
                } elseif ($CaretPricing->stripe_id_three_year) {
                        $stripe_id = $CaretPricing->stripe_id_three_year;
                        $interval_count = 3;
                } else {
                    $stripe_id = null;
                    $interval_count = 0;
                }
            }
        }
        if (isset($data2['order_item_id'])) {
            // Retrieve the specific order item based on the provided order_item_id
            $orderItem = CLMOrderItems::where('id', $data2['order_item_id'])->first();

            if ($orderItem) {
                if (is_null($stripe_id)) {
                    return response()->json(['message' => 'Stripe ID is required for updating the order item'], 422);
                }
                $orderItem->update([
                    'subscription_pricing_id' => $data2['subscription_plan'], // Assuming data2 contains the subscription_plan value
                    'subscription_amount' => $data2['subscription_amount'], // Assuming data2 contains the subscription_amount value
                    'subscription_stripe_id' => $stripe_id,
                    'interval_count' => $interval_count,
                ]);
            }
        }



        if(isset($data['id'])){
            $CLMRequest = CLMRequest::where('id', $data['id'])->first();
            if($CLMRequest) {
                $CLMRequest->update($data2);
            }
            $userEmail = $CLMRequest->user->email ?? null;
            $userEmail = $data2['email'] ?? null;
            $userEmail = $CLMRequest->email;
            // if ($userEmail) {
            //     Notification::route('mail', $userEmail)->notify(new RequestNotificationForCorporate($CLMRequest));
            // }

        }else{

            $CLMRequest = CLMRequest::create($data2);
            $data['id'] = $CLMRequest->id;

            $adminEmails [] = 'awaisbajwa025@gmail.com';
            $adminEmails [] = 'james.r.jeffries@gmail.com';
            $adminEmails [] = $CLMRequest->email;
            foreach ($adminEmails as $adminEmail) {
            Notification::route('mail', $adminEmail)->notify(new CorporateRequestNotification($CLMRequest));
            }
        }

        return response()->json([ 'data' => $CLMRequest, 'message' => 'Record stored successfully!'], 200);
    }

    public function storeCaretSearchData(Request $request){
        $data = $request->all();

        $data2['keyword'] = trim($data['keyword']);

        if(isset($data['classification'])){
            $data2['classification'] = trim($data['classification']);
        }
        if(isset($data['net_worth'])){
            $data2['net_worth'] = trim($data['net_worth']);
        }
        if(isset($data['google_search_volume'])){
            $data2['google_search_volume'] = trim($data['google_search_volume']);
        }
        if(isset($data['instagram'])){
            $data2['instagram'] = trim($data['instagram']);
        }
        if(isset($data['twitter'])){
            $data2['twitter'] = trim($data['twitter']);
        }

        if(isset($data['keyword'])){
            $CaretSearchData = CaretSearchData::where('keyword', $data2['keyword'])->first();

            if($CaretSearchData) {
                $CaretSearchData->update($data2);
            }else{
                $CaretSearchData = CaretSearchData::create($data2);
            }
        }

        return response()->json([ 'data' => $CaretSearchData, 'message' => 'Record stored successfully!'], 200);

    }

    public function checkPricing(Request $request, $order_id)
    {
        $data = CLMRequest::where('order_id', $order_id)->get();
        $checkStatus = 1;
        foreach ($data as $value) {
            if (empty($value->subscription_amount) || $value->status != 1) {
                $checkStatus = 0;
            }
        }
        if($checkStatus == 1){
            return response()->json(['data' => $checkStatus, 'message' => 'Pricing is empty!'], 200);
        }else{
            return response()->json(['data' => $checkStatus,'message' => 'Pricing Done!'], 200);
        }
    }

    public function requestEmail(Request $request, $order_id)
    {
        $CLMRequests = CLMRequest::where('order_id', $order_id)->get();
        if ($CLMRequests->isEmpty()) {
            return response()->json([
                'success' => false,
                'message' => 'No requests found for the given order ID.'
            ], 404);
        }
        CLMRequest::where('order_id', $order_id)->update(['status' => 2]);
        $userEmail = $CLMRequests->first()->email;

        if ($userEmail) {
            Notification::route('mail', $userEmail)
                ->notify(new RequestNotificationForCorporate($CLMRequests));

            return response()->json([
                'success' => true,
                'message' => 'Email notification sent successfully.'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Email address not found for the given order ID.'
        ], 400);
    }

    public function checkRequestStatus(Request $request, $order_id)
    {
        $data = CLMRequest::where('order_id', $order_id)->get();
        foreach ($data as $value) {
            if (!empty($value->subscription_amount) && $value->status === 2) {
                return response()->json(['data' => 'YES', 'message' => 'Pricing is Set!'], 200);
            }
        }
        return response()->json(['data' => 'NO','message' => 'Pricing Not Set!'], 200);
    }

 // #################################################################


    public function getLicense(Request $request, $user_id){
        $users = CLMLicense::with(['plan'])
        ->orderBy('id','DESC')
        ->select('id as value', 'caret_title as label', 'subscription_plan')
        ->where('subscription_status','active')
        ->when($user_id,function($q,$user_id){
            $q->where('user_id',$user_id);
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }

    public function getCaretLogoRendom(Request $request, $user_id) {
        $filter = $request->input('filter');
        $CLMLicense = CLMLicense::select('id as value', 'caret_title as label', DB::raw("CONCAT('".config('app.awsurl')."', caret_logo) AS caret_logo"))
            ->where(function($q) use ($filter) {
                $q->where('caret_title', 'like', '%' . $filter . '%'); // Corrected field name
            })
            ->where('caret_logo', '!=', null)
            ->when($user_id, function($q, $user_id) {
                $q->where('user_id', $user_id);
            })
            ->inRandomOrder()
            ->limit(8)
            ->get();

        return $this->sendSuccessResponse('data', $CLMLicense, 'Record retrieved successfully!'); // Corrected variable name
    }
    public function defaultSettings(Request $request)
    {
        $data = $request->all();

        $data2['default_ad'] = trim($data['default_ad'][0]);
        $data2['default_intro'] = trim($data['default_intro'][0]);
        $data2['default_finish'] = trim($data['default_finish'][0]);
        $data2['default_sound'] = trim($data['default_sound'][0]);



        $CLMLicense = CLMLicense::where('id', $data['id'])->first();
            if($CLMLicense) {
                $CLMLicense->update($data2);
        }

        return response()->json([ 'data' => $CLMLicense, 'message' => 'Record updated successfully!'], 200);
    }



    public function addHash($data){
        //return $data[0];
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                //return $dt;
                $arr['title'] = trim(str_replace('#','',$dt));
                $res = Hashs::updateOrCreate($arr);
                $ret[] =  $res->id;
            }
        }
        return $ret;
    }


    public function addCarets($data){
        $arr['title'] = trim(str_replace('^','', $data));
        $carets = Carets::where('title', $arr['title'])->first();

        if ($carets) {
            $arr['updated_at'] = Carbon::now();
            $carets->update($arr);
        } else {
            $carets = Carets::create($arr);
        }

        return $carets->id;
    }

    public function destroy($id)
    {
        try {
            $CLMRequest = CLMRequest::where('id', $id)->get();
            $CLMRequest->delete();
            return response()->json(['', 'message' => 'Record deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
        }


    }

        public function toggleLicenseCancel(Request $request)
        {
            $response = '';
            $data = $request->all();



            if(isset($data['license_id'])){
                $CLMLicense = CLMLicense::where('id', $data['license_id'])->first();
                if($CLMLicense) {
                    {

                        if($data['cancel_at_period_end'] == 0){
                            $response =   $this->activateSubscription($CLMLicense->id);
                            $data2['status_id'] = 4;
                        }else{
                            $response =  $this->inActivateSubscription($CLMLicense->id);
                            $data2['status_id'] = 6;
                        }
                    }

                    if($response == 'success')
                        $CLMLicense->update($data2);
                }


            $CampaignsAds = CLMLicense::where('id', $data['license_id'])->first();

            //return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            if($response == 'error'){
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }else{
                return response()->json([ 'data' => $CLMLicense, 'message' => 'Record stored successfully!'], 200);
            }


            }else{
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }
        }


        public function licenseStatus(Request $request) {
            $data = $request->all();
            $data2['status_id'] = trim($data['status_id']);

            $request_id = $request->request_id;
            $CLMRequest = CLMRequest::where('id', $request_id)->first();
            if($CLMRequest){

                $CLMRequest->update($data2);

                $CLMRequest = CLMRequest::with(['statusA'])->where('id', $request_id)->first();
                $userEmail = $CLMRequest->user->email ?? null;
                $userEmail = $data2['email'] ?? null;
                $userEmail = $CLMRequest->email;
                if ($userEmail) {
                    Notification::route('mail', $userEmail)->notify(new RequestStatusNotificationForCorporate($CLMRequest));
                }
                return response()->json([ 'data' => $CLMRequest, 'message' => 'Record updated successfully!'], 200);
            }else{
                return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
            }

        }






     public function activateSubscription($license_id){


        $ret = CLMPayments::where('license_id', $license_id)
        ->orderBy('current_period_end', 'DESC')
        ->first();

            //campaign_ad_id
            $CLMLicense = CLMRequest::where('id', $license_id)->first();


             ##SUBSCRIPTION PART
            $user = User::where('id',  $CLMLicense->user_id)->first();
            $srtipe_id = 0;
            $CLMPlans = CLMPlans::where('id', $CLMLicense->subscription_plan)->first();



            Stripe::setApiKey(config("services.stripe.secret"));
            if($user->stripe_id != ""){
                try{
                    if(isset($ret->transaction_id)){
                        $Subscription = Subscription::update(
                            $ret->transaction_id,
                            [
                                'cancel_at_period_end' => false,
                            ]
                        );
                        $payment['transaction_type'] = "Re-Activate";
                        $payment['amount'] = 0;
                    }else{

                    $customer_srtipe_id = $user->stripe_id;
                    $plan_srtipe_id = $CLMPlans->stripe_id;

                        $Subscription = Subscription::create(array(
                            'customer' =>$customer_srtipe_id,
                            'items' => [
                                ['price' => $plan_srtipe_id],
                            ]
                        ));


                    $payment['transaction_type'] = "Payment";
                    $payment['amount'] = $CLMPlans->amount;

                    }
                }catch(\Stripe\Exception\InvalidRequestException $e) {
                    $error = $e->getError()->message . '\n';
                    return "error";
                }
            }else{
                return "error";
            }

            if($Subscription){

                $payment['license_id'] = $CLMLicense->id;
                $payment['user_id'] = $CLMLicense->user_id;

                $payment['transaction_id'] = $Subscription['id'];
                $payment['latest_invoice'] = $Subscription['latest_invoice'];
                $payment['customer'] = $Subscription['customer'];
                $payment['cancel_at'] = $Subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $Subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $Subscription['canceled_at'];
                $payment['collection_method'] = $Subscription['collection_method'];
                $payment['created'] = $Subscription['created'];
                $payment['current_period_end'] = $Subscription['current_period_end'];
                $payment['current_period_start'] = $Subscription['current_period_start'];
                $payment['days_until_due'] = $Subscription['days_until_due'];
                $payment['default_payment_method'] = $Subscription['default_payment_method'];
                $payment['plan_id'] = $Subscription['plan']['id'];
                $payment['plan_currency'] = $Subscription['plan']['currency'];
                $payment['plan_interval'] = $Subscription['plan']['interval'];
                $payment['plan_interval_count'] = $Subscription['plan']['interval_count'];
                $payment['plan_product'] = $Subscription['plan']['product'];
                $payment['start_date'] = $Subscription['start_date'];
                $payment['status'] = $Subscription['status'];

                $pay_return = CLMPayments::create($payment);

                $CLMLicense->fill([
                    'subscription_status'=>'active',
                    'cancel_at_period_end' => false,
                    'current_period_end' => $Subscription['current_period_end']
                    ]);
                $CLMLicense->save();
                return 'success'; //response()->json(['data' => $Subscription, 'success' => 'Udated successfully!'], 200);
            }else{

                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);

            }

     }


     public function inActivateSubscription($license_id){


        $ret = CLMPayments::where('license_id', $license_id)
        ->orderBy('current_period_end', 'DESC')
        ->first();

        $CLMLicense = CLMRequest::where('id', $license_id)->first();


        ##SUBSCRIPTION PART
       $user = User::where('id',  $CLMLicense->user_id)->first();
       $srtipe_id = 0;
       $CLMPlans = CLMPlans::where('id', $CLMLicense->subscription_plan)->first();



       Stripe::setApiKey(config("services.stripe.secret"));
            if($user->stripe_id != ""){
                try{
                    if(isset($ret->transaction_id)){
                        $Subscription = Subscription::update(
                            $ret->transaction_id,
                            [
                                'cancel_at_period_end' => true,
                            ]
                        );
                    }
                }catch(\Stripe\Exception\InvalidRequestException $e) {
                    $error = $e->getError()->message . '\n';
                    return "error"; // response()->json( ["error"=>"error","status"=>"error", "msg"=> $error], 400);
                }
            }else{
                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);
            }

            if(isset($Subscription)){
                //print_r($Subscription);
                $payment['license_id'] = $CLMLicense->id;
                $payment['user_id'] = $CLMLicense->user_id;
                $payment['amount'] = 0;
                $payment['transaction_type'] = "cancellation_requested";

                $payment['transaction_id'] = $Subscription['id'];
                $payment['latest_invoice'] = $Subscription['latest_invoice'];
                $payment['customer'] = $Subscription['customer'];
                $payment['cancel_at'] = $Subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $Subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $Subscription['canceled_at'];
                $payment['collection_method'] = $Subscription['collection_method'];
                $payment['created'] = $Subscription['created'];
                $payment['current_period_end'] = $Subscription['current_period_end'];
                $payment['current_period_start'] = $Subscription['current_period_start'];
                $payment['days_until_due'] = $Subscription['days_until_due'];
                $payment['default_payment_method'] = $Subscription['default_payment_method'];
                $payment['plan_id'] = $Subscription['plan']['id'];
                $payment['plan_currency'] = $Subscription['plan']['currency'];
                $payment['plan_interval'] = $Subscription['plan']['interval'];
                $payment['plan_interval_count'] = $Subscription['plan']['interval_count'];
                $payment['plan_product'] = $Subscription['plan']['product'];
                $payment['start_date'] = $Subscription['start_date'];
                $payment['status'] = $Subscription['status'];

                $pay_return = CLMPayments::create($payment);

                $CLMLicense->fill([
                    'subscription_status'=>'active',
                    'cancel_at_period_end' => 1,
                    'current_period_end' => $Subscription['current_period_end']
                    ]);
                $CLMLicense->save();
                return 'success'; //response()->json(['data' => $Subscription, 'success' => 'Udated successfully!'], 200);
            }else{
                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);

            }

     }




     public function listSubscription(Request $request){
        Stripe::setApiKey(config("services.stripe.secret"));
        return $Subscription = Subscription::all(['status'=>'active']);
     }




     public function getUsers(Request $request){
        $filter = $request->input('filter');
        $users = User::orderBy('id','DESC')
        ->select('id as value', 'email as label')
        ->where(function($q) use ($filter){
            $q->where('email','like', '%' . $filter . '%');
        })
        ->whereHas(
            'roles', function($q){
                $q->where('name', 'clm');
            }
        )
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }





    public function getStatus(Request $request){
        $filter = $request->input('filter');
        $AdStatus = AdStatus::orderBy('id','DESC')
        ->select('id as value', 'title as label')
        ->where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $AdStatus, 'Record retrieved successfully!');
    }

    public function getPlans(Request $request){
        $filter = $request->input('filter');
        $CLMPlans = CLMPlans::orderBy('id','DESC')
        ->select('*','id as value', 'title as label')
        ->where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
        ->where('is_active',1)
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $CLMPlans, 'Record retrieved successfully!');
    }

    public function webhooks(Request $request){
        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        switch ($event->type) {
            case 'invoice.payment_failed':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                ->orderBy('transaction_id', 'DESC')
                ->first();

                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $AdPayments->campaign_ad_id)->first();

                    $CampaignsAds->fill([
                    'cancel_at_period_end' => true
                    ]);
                    $CampaignsAds->save();
            break;
            case 'customer.subscription.deleted':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                ->orderBy('transaction_id', 'DESC')
                ->first();

                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $AdPayments->campaign_ad_id)->first();

                    $CampaignsAds->fill([
                    'cancel_at_period_end' => true
                    ]);
                    $CampaignsAds->save();
            break;
            case 'invoice.payment_succeeded':
                    $data = $event->data->object; // contains a \Stripe\PaymentIntent
                        $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                        ->orderBy('transaction_id', 'DESC')
                        ->first();

                        $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                            DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                        }])->where('id', $AdPayments->campaign_ad_id)->first();


                        $payment['campaign_ad_id'] = $CampaignsAds->id;
                        $payment['user_id'] = $AdPayments->user_id;

                        $payment['transaction_id'] = $data->id;
                        $payment['latest_invoice'] = $data->latest_invoice;
                        $payment['customer'] = $data->customer;
                        $payment['cancel_at'] = $data->cancel_at;
                        $payment['cancel_at_period_end'] = $data->cancel_at_period_end;
                        $payment['canceled_at'] = $data->canceled_at;
                        $payment['collection_method'] = $data->collection_method;
                        $payment['created'] = $data->created;
                        $payment['current_period_end'] = $data->current_period_end;
                        $payment['current_period_start'] = $data->current_period_start;
                        $payment['days_until_due'] = $data->days_until_due;
                        $payment['default_payment_method'] = $data->default_payment_method;
                        $payment['plan_id'] = $data->plan->id;
                        $payment['plan_currency'] = $data->plan->currency;
                        $payment['plan_interval'] = $data->plan->interval;
                        $payment['plan_interval_count'] = $data->plan->interval_count;
                        $payment['plan_product'] = $data->plan->product;
                        $payment['start_date'] = $data->start_date;
                        $payment['status'] = $data->status;

                        $pay_return = AdPayments::create($payment);

                        $CampaignsAds->fill([
                            'subscription_status'=>'active',
                            'cancel_at_period_end' => false,
                            'current_period_end' => $data->current_period_end
                            ]);
                        $CampaignsAds->save();
                break;
            // case 'payment_method.attached':
            //     $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
            //     // Then define and call a method to handle the successful attachment of a PaymentMethod.
            //     // handlePaymentMethodAttached($paymentMethod);
            //     break;
            // ... handle other event types
            default:
                // echo 'Received unknown event type ' . $event->type;
        }

        http_response_code(200);
     }


     public function uploadLogo(Request $request)
     {
         $clmLicense = CLMLicense::find($request->license_id);
         if($clmLicense){
             $imageData = $request->input('image');
             //$user_id = $request->user_id;
             $file = $imageData['file'];
             $file_name = $imageData['name'];
             $image = file_get_contents($file);
             $file_path =  'Logos/' . $request->license_id.time() . '.png';
             Helper::putServerImage($file_path, $image, 'public');
             $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
             if ($confirmUpload){
                 $clmLicense->caret_logo = $file_path;
                 $clmLicense->save();
             }
         }else{
             return response()->json([
                 "message" => "License not found"
             ]);
         }
     }

########################################################################################################

public function checkCaretAvailability(Request $request)
    {
        $keyword = $request->input('keyword');

        // Categorize the keyword
        $planType = $this->categorizeKeyword($keyword);

        // Fetch pricing based on plan type from the database
        $pricing = $this->getPricing();

        // Check if the keyword is already purchased
        $keywordStatus = CLMLicense::where('caret_title', $keyword)->exists();

        $return = [];

        if (!$keywordStatus) {
            // Calculate the price based on the plan type
            $price = $pricing[$planType] ?? null; // Handle case where plan type may not exist

            $return = [
                'status' => 'available',
                'message' => 'The keyword is available.',
                'price' => $price,
                'plan_type' => $planType
            ];
        } else {
            $return = [
                'status' => 'unavailable',
                'message' => 'The keyword is already purchased.'
            ];
        }

        // If purchased, generate similar keyword suggestions
        $suggestions = $this->generateKeywordSuggestions($keyword);

        // Filter out already purchased keywords using a database query
        $purchasedKeywords = CLMLicense::whereIn('caret_title', $suggestions)
            ->pluck('caret_title')
            ->toArray();

        // Remove purchased keywords from suggestions
        $availableSuggestions = array_diff($suggestions, $purchasedKeywords);

        $topSuggestion = $availableSuggestions ? array_shift($availableSuggestions) : null;

        $topSuggestionWithPlans = [
            'keyword' => $topSuggestion,
            'plan_type' => $this->categorizeKeyword($topSuggestion),
             'price' => $pricing[$this->categorizeKeyword($topSuggestion)] ?? null
        ];
        // Get the plan types and prices for the available suggestions
        $suggestionsWithPlans = array_map(function($suggestion) use ($pricing) {
            $planType = $this->categorizeKeyword($suggestion);
            return [
                'keyword' => $suggestion,
                'plan_type' => $planType,
                'price' => $pricing[$planType] ?? null // Handle case where plan type may not exist
            ];
        }, $availableSuggestions);

        $return['topSuggestion'] = $topSuggestionWithPlans;
        $return['suggestions'] = $suggestionsWithPlans;

        return response()->json($return);
    }

    private function categorizeKeyword($keyword)
    {
        $wordCount = str_word_count($keyword);

        if ($wordCount == 1) {
            return 'individual';
        } elseif ($wordCount == 2) {
            return 'influencer';
        } else {
            return 'corporate';
        }
    }

    private function generateKeywordSuggestions($keyword)
    {
        // More sophisticated logic to generate keyword suggestions
        $suggestions = [];
        $prefixes = ['best', 'top', 'my', 'your', 'new', 'super'];
        $suffixes = ['online', 'pro', 'expert', 'hub', 'world', 'zone'];

        // Add combinations of prefixes and suffixes
        foreach ($prefixes as $prefix) {
            $suggestions[] = $prefix . $keyword;
        }

        foreach ($suffixes as $suffix) {
            $suggestions[] = $keyword . $suffix;
        }

        // Include combinations of both
        foreach ($prefixes as $prefix) {
            foreach ($suffixes as $suffix) {
                $suggestions[] = $prefix . $keyword . $suffix;
            }
        }

        return $suggestions;
    }

    private function getPricing()
    {
        // Fetch the pricing plans from the database
        $plans = CLMPlans::where('is_active', true)->get();

        // Map the plans to a format where key is plan type
        $pricing = [];
        foreach ($plans as $plan) {
            if ($plan->title === 'individual') {
                $pricing['individual'] = $plan->amount;
            } elseif ($plan->title === 'influencer') {
                $pricing['influencer'] = $plan->amount;
            } elseif ($plan->title === 'corporate') {
                $pricing['corporate'] = $plan->amount;
            }
        }

        return $pricing;
    }

#########################################################################################################


}
