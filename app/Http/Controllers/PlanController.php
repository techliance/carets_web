<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdPlans;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Price;

class PlanController extends Controller
{
    public function getDurations() {
        $data = [
            ['label'=>'Day', 'value'=> 'day'],
            ['label'=>'Week', 'value'=> 'week'],
            ['label'=>'Month', 'value'=> 'month'],
            ['label'=>'Year', 'value'=> 'year']
        ];
        return $this->sendSuccessResponse('data', $data, 'Data retrieved successfully!');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = AdPlans::where(function($q) use ($filter){
                $q->where('title','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data2['title'] = trim($data['title']);
        $data2['position'] = trim($data['position']);
        $data2['amount'] = trim($data['amount']);
        if(isset($data['is_active']))
            $data2['is_active'] = trim($data['is_active']);

        $data2['duration'] = trim($data['duration']);

        Stripe::setApiKey(config("services.stripe.secret"));

        if(isset($data['id'])){
            $AdPlans = AdPlans::where('id', $data['id'])->first();
            if($AdPlans) {

                // $planData = Plan::update('price_1MxyNq2eZvKYlo2CkvSSTepV',array(
                //     "amount" =>$data2['amount']*100,
                //     "interval" => $data2['duration'],
                //     "nickname" => $data2['title'],
                //     "currency" => "usd",
                //     "product" => 'prod_RqquyjIKF0Xh4B',
                //     "id" => $AdPlans->stripe_id)
                //   );
                //   $data2['stripe_id'] = $planData->id;
                  $AdPlans->update($data2);
            }
        }else{
            $planData = Price::create(array(
                'unit_amount' =>$data2['amount']*100,
                'recurring' => ['interval' => $data2['duration']],
                'currency' => 'usd',
                'product' => 'prod_RqquyjIKF0Xh4B'
                )
              );
              $data2['stripe_id'] = $planData->id;
              $AdPlans = AdPlans::create($data2);
              $data['id'] = $AdPlans->id;
        }



        $AdPlans = AdPlans::where('id', $data['id'])->first();

        return response()->json([ 'data' => $AdPlans, 'message' => 'Record stored successfully!'], 200);
    }

    public function show($id)
    {
        $data = AdPlans::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function destroy($id)
    {

        try {
            $AdPlans = AdPlans::where('id', $id)->first();
            $AdPlans->delete();
            return response()->json(['', 'message' => 'Record deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
        }


    }

    public function planStatus(Request $request) {
        $plan_id = $request->plan_id;
        $AdPlans = AdPlans::where('id', $plan_id)->first();
        if($AdPlans){
            if($AdPlans->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $AdPlans->update($data);
            return response()->json([ 'data' => $AdPlans, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }
}
