<?php

namespace App\Http\Controllers;

use App\Ads;
use App\AdsIndexing;
use App\AdViews;
use App\Ages;
use App\CampaignsAds;
use App\Carets;
use App\CLMLicense;
use App\Hashs;
use App\Helpers\Helper;
use App\Helpers\HelperGPU;
use App\Jobs\ProcessUploadVideoJob;
use App\SearchCaretByDate;
use App\SearchCaretByLike;
use App\SearchVideoByDate;
use App\SearchVideoByLike;
use App\Sounds;
use App\SystemSettings;
use App\User;
use App\UserFollows;
use App\UserPrivacy;
use App\VideoAds;
use App\VideoCategories;
use App\VideoCollages;
use App\VideoCommentLikes;
use App\VideoComments;
use App\VideoLikes;
use App\VideoReports;
use App\Videos;
use App\VideoSplash;
use App\VideoViews;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Log;
// use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
// use RuntimeException;

class VideoController extends Controller
{

    protected $awsUrl;

    /**
     * VideoController constructor.
     */
    public function __construct()
    {
        $this->awsUrl = config('app.awsurl');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;
        if($request->input('search')){
            $search = $request->input('search');
            if($search['is_caret'] == true){
                $is_caret = 1;
            }else{
                $is_caret = null;
            }
            if($search['is_reported'] == true){
                $is_reported = 1;
            }else{
                $is_reported = null;
            }
        }
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Videos::select('*')->with(['videos'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS video_raw_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")
        ]);
        }
        ,'users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])
        ->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        },
        'collages'=>function($q){
            $q->with(['videos'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")]);
            }]);
        },
        'reported'=>function($q){
            $q->with('reportby')->where('is_active',1);
        }])
        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS video_raw_url"),

                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])

        ->when($filter, function($q) use ($filter) {
            $q->where(function ($query) use ($filter) {
                $query->where('id', 'like', '%' . $filter . '%')
                        ->orWhere('video_description', 'like', '%' . $filter . '%');
            });
        })
        ->when($search, function($q) use ($search){
            if ($search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
                $q->where('created_at', '<=', $search['toDate']);
            } elseif ($search['fromDate'] && !$search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
            } elseif (!$search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '<=', $search['toDate']);
            }
        })
        ->when($is_caret, function($q){
            $q->where('is_caret',1);
        })
        ->when($is_reported,function($q){
            $q->whereHas("reported", function($q) {
                $q->where('is_active', 1);
            });
        })
        // ->when(in_array($is_caret,[0,1]),function($q) use($is_caret){
        //         $q->where('is_caret',$is_caret);
        //     })
        // ->when($user_id,function($q) use($user_id){
        //     $q->whereHas("users", function($q) use($user_id) {
        //         $q->where('users.id',$user_id);
        //     });
        // })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function indexCLM(Request $request)
    {


        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        if($signed_id){
            $mycarets = CLMLicense::where('user_id', $signed_id)->get();
        }
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;
        $is_caret = null;
        $is_reported = null;
        if($request->input('search')){
            $search = $request->input('search');
            if($search['is_caret'] == true){
                $is_caret = 1;
            }else{
                $is_caret = null;
            }
            if($search['is_reported'] == true){
                $is_reported = 1;
            }else{
                $is_reported = null;
            }
        }
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Videos::select('*')->with(['videos'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS video_raw_url"),

        ]);
        }
        ,'users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])
        ->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        },
        'collages'=>function($q){
            $q->with(['videos'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")]);
            }]);
        },
        'reported'=>function($q){
            $q->with('reportby')->where('is_active',1);
        }])
        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS video_raw_url"),

                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])
        ->when($filter,function($q) use ($filter){
                $q->where('id',$filter)
                    ->orWhere('video_description','like', '%' . $filter . '%')
                    ->orWhere('license_id', $filter );
            })
        ->when($search, function($q) use ($search){
            if ($search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
                $q->where('created_at', '<=', $search['toDate']);
            } elseif ($search['fromDate'] && !$search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
            } elseif (!$search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '<=', $search['toDate']);
            }
        })
        ->when($is_caret, function($q){
            $q->where('is_caret',1);
        })

        // ->when($is_reported,function($q){
        //     $q->whereHas("reported", function($q) {
        //         $q->with('reportby')->where('is_active',1);
        //     });
        // })

        // ->where(function ($query) use ($signed_id, $mycarets) {
        //     // Existing whereHas condition for users linked to signed_id
        //     $query->whereHas('users', function ($q) use ($signed_id) {
        //         $q->where('users.id', $signed_id);
        //     });

        //     // Other where conditions can be added here
        //     $query->where('is_active','<>', 0);  // Example of another where clause

        //     // Add orWhere for matching video_description
        //     $query->orWhere(function ($q) use ($mycarets) {
        //         foreach ($mycarets as $mycaret) {
        //             $q->orWhere('video_description', 'like', '%' . trim($mycaret->caret_title) . '%');
        //         }
        //     });
        // })

        ->where(function ($query) use ($signed_id, $mycarets, $is_reported) {
            // Existing whereHas condition for users linked to signed_id
            $query->whereHas('users', function ($q) use ($signed_id) {
                $q->where('users.id', $signed_id);
            });
            // Other where conditions can be added here
                $query->where('is_active','<>', 0);  // Example of another where clause
            // Apply orWhere for video_description only if is_reported is not set
            $query->when(!$is_reported, function ($q) use ($mycarets) {
                $q->orWhere(function ($q) use ($mycarets) {
                    foreach ($mycarets as $mycaret) {
                        $q->orWhere('video_description', 'like', '%^' . trim($mycaret->caret_title) . '%');
                    }
                });
            });
        })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function myCarets(Request $request, $user_id)
    {
        $user_id = (int)$user_id > 0 ? $user_id : null;
        $search = "";
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        if($request->input('user_id')){
            $signed_id = $request->input('user_id');
        }
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;
        $is_caret = 1;
        if($request->input('search')){
            $search = $request->input('search');
        }
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Videos::select('*',DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"))
        ->with(['license','videos'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")]);
        }
        ,'users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        },
        'collages'=>function($q){
            $q->with(['videos'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")]);
            }]);
        },
        'reported'=>function($q){
            $q->with('reportby')->where('is_active',1);
        }])
        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])

        ->when($filter, function($q) use ($filter) {
            $q->where(function ($query) use ($filter) {
                $query->where('license_id','like', '%' . $filter . '%')
                        ->orWhere('video_description', 'like', '%' . $filter . '%');
            });
        })


        ->when($search, function($q) use ($search){
            if ($search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
                $q->where('created_at', '<=', $search['toDate']);
            } elseif ($search['fromDate'] && !$search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
            } elseif (!$search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '<=', $search['toDate']);
            }
        })

        ->where('is_caret',1)
        ->whereHas('users', function($q) use($signed_id) {
            $q->where('users.id', $signed_id);
        })
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function caretVideos(Request $request, $user_id)
    {
        $search = "";
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        if($request->input('user_id')){
            $signed_id = $request->input('user_id');
        }
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;
        $is_caret = 1;
        if($request->input('search')){
            $search = $request->input('search');
        }
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Videos::select('*',DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"))
        ->with(['license','videos'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")]);
        }
        ,'users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        },
        'collages'=>function($q){
            $q->with(['videos'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url")]);
            }]);
        },
        'reported'=>function($q){
            $q->with('reportby')->where('is_active',1);
        }])
        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])
        ->when($filter, function($q) use ($filter) {
            $q->where(function ($query) use ($filter) {
                $query->where('license_id', 'like', '%' . $filter . '%')
                        ->orWhere('video_description', 'like', '%' . $filter . '%');
            });
        })
        ->when($search, function($q) use ($search){
            if ($search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
                $q->where('created_at', '<=', $search['toDate']);
            } elseif ($search['fromDate'] && !$search['toDate']) {
                $q->where('created_at', '>=', $search['fromDate']);
            } elseif (!$search['fromDate'] && $search['toDate']) {
                $q->where('created_at', '<=', $search['toDate']);
            }
        })

        ->where('is_caret',1)
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }



    public function getVideos(Request $request, $filter)
    {   $search = $request->input('search');
        $videosQuery = Videos::query()
            ->select('id as value', 'video_title as label', 'video_description as description',
            DB::raw("CONCAT('" . $this->awsUrl . "', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
            DB::raw("CONCAT('" . $this->awsUrl . "', image_url) AS image_url"),
            DB::raw("CONCAT('" . $this->awsUrl . "', video_raw_url) AS video_raw_url"))

            ->where(function($q) use ($search) {
                $q->where('video_title', 'like', '%' . $search . '%');
                $q->orWhere('video_description', 'like', '%' . $search . '%');
            })
            ->orderBy('id', 'DESC')
            ->when($filter, function ($query, $filter) {
                return $query->where('video_description', 'like', '%' . $filter . '%');
            })->where('is_caret',0);


        $videos = $videosQuery->limit(50)->get();

        return $this->sendSuccessResponse('data', $videos, 'Record retrieved successfully!');
    }

    public function getRandomMyCaret(Request $request, $user_id)
    {   $licenseId = $request->input('licenseId');
        $signed_id = (int)$user_id > 0 ? $user_id : null;
        // Auth::check() ? $signed_id = Auth::user()->id : $signed_id = null;
        $randomCarets = Videos::with('license')
        ->orderBy('id','DESC')
        ->select('id as value', 'video_title as label', 'license_id as license_id', 'video_description as description',
        DB::raw("CONCAT('" . $this->awsUrl . "', video_raw_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('" . $this->awsUrl . "', image_url) AS image_url"))
            ->where('is_caret', 1)
            ->where('is_active', 1)
            ->when($signed_id, function ($query, $signed_id) {
                $query->whereHas('users', function($q) use ($signed_id) {
                    $q->where('users.id', $signed_id);
                });
            })
            ->when($licenseId, function ($q) use ($licenseId) {
                $q->where('license_id', $licenseId);
            })
            ->limit(12)
            ->get();

        if ($request->is('api/*')) {
            return $this->sendSuccessResponse('data', $randomCarets, 'Random caret videos retrieved successfully!');
        }
    }
    public function getRandomMyVideos(Request $request, $user_id)
    {
        $licenseId = $request->input('licenseId');
        $signed_id = (int)$user_id > 0 ? $user_id : null;
        // Auth::check() ? $signed_id = Auth::user()->id : $signed_id = null;
        $randomCarets = Videos::orderBy('id','DESC')
        ->select('id as value', 'video_title as label', 'video_description as description',
        DB::raw("CONCAT('" . $this->awsUrl . "', video_raw_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('" . $this->awsUrl . "', image_url) AS image_url"))
            ->where('is_caret', 0)
            ->where('is_active', 1)
            ->when($signed_id, function ($query, $signed_id) {
                $query->whereHas('users', function($q) use ($signed_id) {
                    $q->where('users.id', $signed_id);
                });
            })
            ->when($licenseId, function ($q) use ($licenseId) {
                $q->where('license_id', $licenseId);
            })
            ->limit(12)
            ->get();

        if ($request->is('api/*')) {
            return $this->sendSuccessResponse('data', $randomCarets, 'Random caret videos retrieved successfully!');
        }
    }






    // public function homePage(Request $request)
    // {
    //     $current_page = (int)$request->input('current_page', 1);
    //     $per_page = $request->input('per_page', 5);
    //     $skip = 5*($current_page-1);
    //     $is_caret = $request->input('is_caret', null);
    //     if($is_caret == 0)
    //         $is_caret = null;
    //     Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
    //     $user =User::where('id',$signed_id)->withCount(['profile'=>function($q){
    //         $q->select('dob');
    //     }])->orderBy('profile_count')->first();

    //     if($current_page == 1){
    //         $store['last_visit'] = Carbon::now();
    //         $user->update($store);
    //         $last_visit_time = $store['last_visit'];
    //     }else{
    //         $last_visit_time = $user->last_visit;
    //     }

    //     $age = Carbon::parse($user->profile_count)->diff(Carbon::now())->format('%y');
    //     $ageRange = Ages::whereRaw('? between min and max', [$age])->first();

    //     $SearchVideoByDate = SearchVideoByDate::where('created_at','<',$last_visit_time)
    //     ->where('age','<=',$ageRange->id)
    //     ->orderBy('created_at','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

    //     $SearchVideoByLike = SearchVideoByLike::where('created_at','<',$last_visit_time)
    //     ->where('age','<=',$ageRange->id)
    //     ->orderBy('like_count','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

    //     $SearchCaretByDate = SearchCaretByDate::where('created_at','<',$last_visit_time)
    //     ->where('age','<=',$ageRange->id)
    //     ->orderBy('created_at','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

    //     $SearchCaretByLike = SearchCaretByLike::where('created_at','<',$last_visit_time)
    //     ->where('age','<=',$ageRange->id)
    //     ->orderBy('like_count','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

    //     $recordsIds = array_unique(array_merge ($SearchVideoByDate, $SearchVideoByLike,$SearchCaretByDate,$SearchCaretByLike));

    //     $records = Videos::with([
    //         'users'=>function($q) use($signed_id){
    //             $q->with(['profile'=>function($q) {
    //                 $q->select(['*', DB::raw("CONCAT('".env('AWS_URL')."', user_photo) AS user_photo")]);
    //             }])
    //             ->withCount([
    //                 'followby as isFollowing'=>function($q) use($signed_id){
    //                     $q->where('follow_by',$signed_id)->limit(1);
    //             }]);
    //         }])
    //         ->where(function($q) use($signed_id){
    //             $q->whereHas('users.followby', function($q)use($signed_id){
    //                 $q->where('follow_by',$signed_id)->limit(1);
    //             })->orWhere('is_private',0);
    //         })
    //         ->withCount([
    //             'likedme as isLiked'=>function($q) use($signed_id){
    //                 $q->where('user_id',$signed_id);
    //             }])
    //         ->addSelect([DB::raw("CONCAT('".env('AWS_URL')."', video_url) AS video_url"),
    //             DB::raw("CONCAT('".env('AWS_URL')."', image_url) AS image_url")])
    //         ->whereIn('id',$recordsIds)
    //         ->when($is_caret,function($q) use($is_caret){
    //             $q->where('is_caret',$is_caret);
    //         })
    //         ->where('is_blocked',0)
    //         ->where('is_active',1)
    //        ->get();
    //        //$records = shuffle($records);
    //         $shuffled = $records->shuffle();
    //         $pagination['pagination']['current_page'] = $current_page;
    //         $pagination['pagination']['data'] = $shuffled;
    //     return $this->sendSuccessResponse('data', $pagination, 'Record retrieved successfully!');

    // }
    public function videoLogic($myInterests, $ageRange, $last_visit_time, $skip, $per_page)
    {
        $SearchVideoByInterest = [];

        // Search videos by interests
        if ($myInterests) {
            $hashes = Hashs::whereIn('category_id', $myInterests)->get();
            $hashIds = $hashes->pluck('id'); // Get the hash IDs

            $SearchVideoByInterest = SearchVideoByDate::whereNotNull('created_at')
                ->whereHas('hashs', function ($query) use ($hashIds) {
                    $query->whereIn('hashs.id', $hashIds);
                })
                ->when($ageRange, function ($q) use ($ageRange) {
                    $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
                }, function ($q) {
                    $q->whereNull('age');
                })
                ->orderBy('created_at', 'DESC')
                ->take($per_page)
                ->get()
                ->pluck('video_id')
                ->toArray();
        }

        // Search videos by date
        $SearchVideoByDate = SearchVideoByDate::whereNotNull('created_at')
            ->when($ageRange, function ($q) use ($ageRange) {
                $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
            }, function ($q) {
                $q->whereNull('age');
            })
            ->orderBy('created_at', 'DESC')
            ->skip($skip)
            ->take($per_page)
            ->get()
            ->pluck('video_id')
            ->toArray();

        // Search videos by likes
        $SearchVideoByLike = SearchVideoByLike::whereNotNull('created_at')
            ->when($ageRange, function ($q) use ($ageRange) {
                $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
            }, function ($q) {
                $q->whereNull('age');
            })
            ->orderBy('like_count', 'DESC')
            ->skip($skip)
            ->take($per_page)
            ->get()
            ->pluck('video_id')
            ->toArray();

        // Search caret videos by date
        $SearchCaretByDate = SearchCaretByDate::whereNotNull('created_at')
            ->when($ageRange, function ($q) use ($ageRange) {
                $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
            }, function ($q) {
                $q->whereNull('age');
            })
            ->orderBy('created_at', 'DESC')
            ->skip($skip)
            ->take($per_page)
            ->get()
            ->pluck('video_id')
            ->toArray();

        // Search caret videos by likes
        $SearchCaretByLike = SearchCaretByLike::whereNotNull('created_at')
            ->when($ageRange, function ($q) use ($ageRange) {
                $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
            }, function ($q) {
                $q->whereNull('age');
            })
            ->orderBy('like_count', 'DESC')
            ->skip($skip)
            ->take($per_page)
            ->get()
            ->pluck('video_id')
            ->toArray();

        // Merge and remove duplicate video IDs
        $recordsIds = array_unique(array_merge(
            $SearchVideoByInterest,
            $SearchVideoByDate,
            $SearchVideoByLike,
            $SearchCaretByDate,
            $SearchCaretByLike
        ));

        return $recordsIds;
    }

    public function homePage(Request $request)
    {
        $current_page = (int)$request->input('current_page', 1);

        $per_page = $request->input('per_page', 25);

        $skip =  $per_page*($current_page-1);
        $is_caret = $request->input('is_caret', null);
        if($is_caret == 0)
            $is_caret = null;
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;

       $user = User::where('id', $signed_id)
        ->with(['userInterests'])
        ->withCount(['profile' => function($q) {
            $q->select('dob');
        }])
        ->orderBy('profile_count')->first();

        if($current_page == 1){
            $store['last_visit'] = Carbon::now();
            $user->update($store);
            $last_visit_time = $store['last_visit'];
        }else{
            $last_visit_time = $user->last_visit;
        }

        $age = Carbon::parse($user->profile_count)->diff(Carbon::now())->format('%y');
        $ageRange = Ages::whereRaw('? between min and max', [$age])->first();

        $myInterests = $user->userInterests->pluck('cat_id');

        $recordsIds = $this->videoLogic($myInterests, $ageRange,$last_visit_time,$skip,$per_page);
        // $SearchVideoByDate = SearchVideoByDate::where('created_at','<',$last_visit_time)
        // ->where('age','<=',$ageRange->id)
        // ->orderBy('created_at','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

        // $SearchVideoByLike = SearchVideoByLike::where('created_at','<',$last_visit_time)
        // ->where('age','<=',$ageRange->id)
        // ->orderBy('like_count','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

        // $SearchCaretByDate = SearchCaretByDate::where('created_at','<',$last_visit_time)
        // ->where('age','<=',$ageRange->id)
        // ->orderBy('created_at','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

        // $SearchCaretByLike = SearchCaretByLike::where('created_at','<',$last_visit_time)
        // ->where('age','<=',$ageRange->id)
        // ->orderBy('like_count','DESC')->skip($skip)->take($per_page)->get()->pluck('video_id')->toArray();

        // $recordsIds = array_unique(array_merge ($SearchVideoByDate, $SearchVideoByLike,$SearchCaretByDate,$SearchCaretByLike));

        $records = Videos::with([
            'users'=>function($q) use($signed_id){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }])
                ->withCount([
                    'followby as isFollowing'=>function($q) use($signed_id){
                        $q->where('follow_by',$signed_id)->limit(1);
                }]);
            }])
            ->where(function($q) use($signed_id){
                $q->whereHas('users.followby', function($q)use($signed_id){
                    $q->where('follow_by',$signed_id)->limit(1);
                })->orWhere('is_private',0);
            })
            ->withCount([
                'likedme as isLiked'=>function($q) use($signed_id){
                    $q->where('user_id',$signed_id);
                }])

            ->addSelect([
                DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url"),
                DB::raw("
                    CASE
                        WHEN is_caret = 1
                        AND TRIM(video_description) REGEXP '^\\\\^[^ ]+$'
                        THEN ''
                        ELSE TRIM(video_description)
                    END AS video_description
                ")
            ])

           // ->whereIn('id',$recordsIds)

            ->when($is_caret,function($q) use($is_caret){
                $q->where('is_caret',$is_caret);
            })
            ->where('videos.is_blocked',0)
            ->where('videos.is_active',1)
            ################################################
            // ->where('created_at','<',$last_visit_time)

            // ->where(function ($query) use($ageRange) {
            //     $query->where('age','<=',$ageRange->id)
            //         ->orWhereNull('age');
            // })
            #################################################
            ->orderBy('created_at','desc')
            ->skip($skip)->take($per_page)
            ->get();


           $recordsCount = $records->count();


           $min = $skip > 0 ? $skip : 0;
           $max = $min + $per_page;

        //    where(function ($query) use($ageRange) {
        //     $query->where('age_range','<=',$ageRange->id)
        //         ->orWhereNull('age_range');
        //     })
        //     ->

        if($records){

           $ads = AdsIndexing::with([
            'addata'=>function($q) {
                $q->with(['user.profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }]);
            }])
            ->whereBetween('position', [$min, $max])

           ->get();
           $retAds = $ads->shuffle();
           $count = $min;
           $records = $records->shuffle();
           foreach($records as $record){
               $count++;
               $adrc = $retAds->where('position', $count)->first();
               if($adrc){
                   $adrc['type'] = "AD";
                   $newRecord[] = $adrc;
               }

            //    if ($record->is_caret == 1 && preg_match('/^\^(\s+|$)/', trim($record->video_description))) {
            //         $record->video_description = '';
            //     }

               $record['type'] = "VIDEO";
               $newRecord[] = $record;
           }

           //
          //$records = shuffle($records);

           //$shuffled = $records;
           $pagination['pagination']['current_page'] = $current_page;
           $pagination['pagination']['data'] = isset($newRecord)?$newRecord:null;

            return $this->sendSuccessResponse('data', $pagination, 'Record retrieved successfully!');
        }else{
            return response()->json( ["error"=>"No Record Found","status"=>"error", "msg"=>"No Record Found" ], 400);

        }

    }

    public function checkNewVideo(Request $request)
    {
        try {

            // Log::info('Authorization Header: ' . $request->header('Authorization'));

            // if (!Auth::check()) {
            //     Log::info('Unauthorized user in checkNewVideo');
            //     return $this->sendErrorResponse('Unauthorized user', [], 401);

            // }
            $signed_id = Auth::check() ? Auth::user()->id : null;
            if($signed_id){
                $last_visit_time = $request->input('last_visit_time', Carbon::now());
                $user = User::where('id', $signed_id)
                    ->with(['profile' => function ($q) {
                        $q->select('dob');
                    }])
                    ->first();

                // Calculate the user's age
                $age = $user && $user->profile ?
                    Carbon::parse($user->profile->dob)->diff(Carbon::now())->format('%y') :
                    null;

                $ageRange = $age ? Ages::whereRaw('? BETWEEN min AND max', [$age])->first() : null;

                // Fetch counts for videos and carets (include videos without age range)
                $SearchVideoByDate = SearchVideoByDate::where('created_at', '>', $last_visit_time)
                    ->when($ageRange, function ($q) use ($ageRange) {
                        $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
                    }, function ($q) {
                        $q->whereNull('age');
                    })
                    ->count();

                $SearchVideoByLike = SearchVideoByLike::where('created_at', '>', $last_visit_time)
                    ->when($ageRange, function ($q) use ($ageRange) {
                        $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
                    }, function ($q) {
                        $q->whereNull('age');
                    })
                    ->count();

                $SearchCaretByDate = SearchCaretByDate::where('created_at', '>', $last_visit_time)
                    ->when($ageRange, function ($q) use ($ageRange) {
                        $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
                    }, function ($q) {
                        $q->whereNull('age');
                    })
                    ->count();

                $SearchCaretByLike = SearchCaretByLike::where('created_at', '>', $last_visit_time)
                    ->when($ageRange, function ($q) use ($ageRange) {
                        $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
                    }, function ($q) {
                        $q->whereNull('age');
                    })
                    ->count();

                // Determine the status
                $status = ($SearchVideoByDate > 0 || $SearchVideoByLike > 0 ||
                        $SearchCaretByDate > 0 || $SearchCaretByLike > 0) ? 1 : 0;

                $data = [
                    'newvideo' => $status,
                    'last_visit_time' => Carbon::now(),
                ];
            }else{
                $data = [
                    'newvideo' => 0,
                    'last_visit_time' => Carbon::now(),
                ];
            }

            return $this->sendSuccessResponse('data', $data, 'Record retrieved successfully!');
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['Log message' => $e->getMessage()], 502);        }
    }
    public function homeListing(Request $request)
    {
        //Bugsnag::notifyException(new RuntimeException("Test error"));
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $is_caret = $request->input('is_caret', '0');
        $user_id = $request->input('user_id', null);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;

        $loc_lat = $request->input('loc_lat', null);
        $loc_long = $request->input('loc_long', null);

        $dis = SystemSettings::where('name','home_distance')->first();
        $distance = $dis->value;

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 500;

        $user =User::where('id',$signed_id)->withCount(['profile'=>function($q){
            $q->select('dob');
        }])->orderBy('profile_count')->first();
       // Calculate user's age
        $age = $user && $user->profile ? Carbon::parse($user->profile->dob)->diff(Carbon::now())->format('%y') : null;

        $ageRange = $age ? Ages::whereRaw('? BETWEEN min AND max', [$age])->first() : null;


        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }
        $records = Videos::with([
        'users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])
            ->withCount([
                'followby as isFollowing'=>function($q) use($signed_id){
                    $q->where('follow_by',$signed_id)->limit(1);
            }]);
        }])
        ->where(function($q) use($signed_id){
            $q->whereHas('users.followby', function($q)use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            })->orWhere('is_private',0);
        })
        ->withCount([
            'likedme as isLiked'=>function($q) use($signed_id){
                $q->where('user_id',$signed_id);
            }])
        ->when($distance,function($q) use($distance,$loc_lat,$loc_long){
            #scope fdistanceor radius
            if($loc_lat && $loc_long)
                $q->distance($loc_lat,$loc_long,$distance);
        })
        ->when($distance==null,function($q){
            $q->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
            DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url"),
            DB::raw("
                CASE
                    WHEN is_caret = 1
                    AND TRIM(video_description) REGEXP '^\\\\^[^ ]+$'
                    THEN ''
                    ELSE TRIM(video_description)
                END AS video_description
            ")]);
        })
        ->when($ageRange, function ($q) use ($ageRange) {
            $q->where('age', '<=', $ageRange->id)->orWhereNull('age');
        }, function ($q) {
            $q->whereNull('age');
        })
        ->where('videos.is_active',1)
        ->where('videos.is_blocked',0)
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->limit(500)->get();

        $firestore = Helper::getFireStoreConnection();
        $collectionReference = $firestore->collection('users');
        $documentReference = $collectionReference->document($user->firestore_reference_id);
        $data = ['videos' => (string)$records];
        if($documentReference) {
            $snapshot = $documentReference->set($data, ['merge' => true]);
            return 'true';
        }else{
            return response()->json( ["error"=>"Invalid firestore refrence ID","status"=>"error", "msg"=>"Invalid firestore refrence ID" ], 400);
        }
    }

    public function mobileListing(Request $request)
    {
        //Bugsnag::notifyException(new RuntimeException("Test error"));
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $is_caret = $request->input('is_caret', '0');
        $user_id = $request->input('user_id', null);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;

        $loc_lat = $request->input('loc_lat', null);
        $loc_long = $request->input('loc_long', null);
        $distance = $request->input('distance', null);

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }
        // $test = Videos::withCount([
        //     'likedme' =>function($q) use($signed_id){
        //         $q->where('user_id',$signed_id);
        //     }])->get();
        //     return $test;
            //
        $data = Videos::with([
        'users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])
        ->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        }])
        ->where(function($q) use($signed_id){
            $q->whereHas('users.followby', function($q)use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            })->orWhere('is_private',0);
        })
        ->withCount([
            'likedme as isLiked'=>function($q) use($signed_id){
                $q->where('user_id',$signed_id);
            }])
        ->when($distance,function($q) use($distance,$loc_lat,$loc_long){
            #scope fdistanceor radius
            if($loc_lat && $loc_long)
                $q->distance($loc_lat,$loc_long,$distance);
        })
        ->when($distance==null,function($q){
            $q->addSelect([
                DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url"),
                DB::raw("
                    CASE
                        WHEN is_caret = 1
                        AND TRIM(video_description) REGEXP '^\\\\^[^ ]+$'
                        THEN ''
                        ELSE TRIM(video_description)
                    END AS video_description
                ")
            ]);
        })
        ->when($filter, function($q) use ($filter) {
            $q->where(function ($query) use ($filter) {
                $query->where('id', 'like', '%' . $filter . '%')
                      ->orWhere('video_description', 'like', '%' . $filter . '%');
            });
        })

        ->when(in_array($is_caret,[0,1]),function($q) use($is_caret){
                $q->where('is_caret',$is_caret);
            })
            ->when($user_id,function($q) use($user_id){
                $q->whereHas("users", function($q) use($user_id) {
                    $q->where('users.id',$user_id);
                });
            })
            ->where('videos.is_active',1)
            ->where('videos.is_blocked',0)
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);



        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function trendingCarets($ret)
    {
        $dis = SystemSettings::where('name','trending')->first();
        if($dis->value > 1){
            $subDays = Carbon::now()->subDays($dis->value);
        }else{
            $subDays = Carbon::now()->subDays(10);
        }
        $data = Carets::withCount(['videos'=>function($q) use($subDays){
            $q->where('carets_videos.created_at', '>',$subDays)
            ->select(DB::raw('sum(videos.like_count)'));
        }])
        ->orderBy('videos_count','DESC')
        ->limit(3)
        ->get()->pluck($ret)->toArray();
        return $data;
    }

    public function trendingHash($ret)
    {
        $dis = SystemSettings::where('name','trending')->first();
        if($dis->value > 1){
            $subDays = Carbon::now()->subDays($dis->value);
        }else{
            $subDays = Carbon::now()->subDays(10);
        }
        $data = Hashs::withCount(['videos'=>function($q) use($subDays){
            $q->where('hashs_videos.created_at', '>',$subDays)
            ->select(DB::raw('sum(videos.like_count)'));
        }])
        ->orderBy('videos_count','DESC')
        ->limit(3)
        ->get()->pluck($ret)->toArray();
        return $data;
    }

    public function trendingUsers($ret)
    {
        $dis = SystemSettings::where('name','trending')->first();
        if($dis->value > 1){
            $subDays = Carbon::now()->subDays($dis->value);
        }else{
            $subDays = Carbon::now()->subDays(10);
        }
        $data = User::withCount(['myvideos'=>function($q) use($subDays){
            $q->where('users_videos.created_at', '>',$subDays)
            ->select(DB::raw('sum(videos.like_count)'));
        }])
        ->orderBy('myvideos_count','DESC')
        ->limit(3)
        ->get()->pluck($ret)->toArray();
        return $data;
    }

    public function search(Request $request)
    {
        $data['hashVideos'] = collect($this->searchByHash($request,1));
        $data['hashVideos']['trendingHash'] = $this->trendingHash('title');
        $data['caretVideos'] =  collect($this->searchByCaret($request,1));
        $data['caretVideos']['trendingCarets'] = $this->trendingCarets('title');
        $data['usersVideos'] =  collect($this->searchByUser($request,1));
        $data['usersVideos']['trendingUsers'] = $this->trendingUsers('username');

        return $data;
    }
    public function searchByCaret(Request $request, $call = null)
    {
        $filter = $request->input('filter', null);
        $sort = $request->input('sort');
        $is_caret = $request->input('is_caret', 1);
        $signed_id = Auth::check() ? Auth::user()->id : null;
        $pagination = $request->input('pageSize', 25);

        $sortName = null;
        $orderType = 'desc'; // Default to descending order
        $trendingCarets = $filter === null ? $this->trendingCarets('id') : null;

        // Parse sort parameters
        if ($sort) {
            [$sortName, $orderDetails] = explode(',', $sort);
            $orderType = strpos($orderDetails, 'true') !== false ? 'desc' : 'asc';
        }

        $data = Videos::select('*')
            ->with([
                'license',
                'users' => function ($q) use ($signed_id) {
                    $q->where('is_blocked', 0) // Exclude blocked users
                        ->with(['profile' => function ($q) {
                            $q->select([
                                '*',
                                DB::raw("CONCAT('" . $this->awsUrl . "', user_photo) AS user_photo"),
                            ]);
                        }])
                        ->withCount([
                            'followby as isFollowing' => function ($q) use ($signed_id) {
                                $q->where('follow_by', $signed_id)->limit(1);
                            },
                        ]);
                },
            ])
            ->addSelect([
                DB::raw("CONCAT('" . $this->awsUrl . "', video_url) AS video_url"),
                DB::raw("CONCAT('" . $this->awsUrl . "', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('" . $this->awsUrl . "', image_url) AS image_url"),
                DB::raw("
                    CASE
                        WHEN is_caret = 1
                        AND TRIM(video_description) REGEXP '^\\\\^[^ ]+$'
                        THEN ''
                        ELSE TRIM(video_description)
                    END AS video_description
                ")
            ])
            ->when($filter, function ($q) use ($filter) {
                $cleanFilter = str_replace('^', '', $filter);
                $q->where(function ($query) use ($cleanFilter) {
                    $query->whereHas('license', function ($q) use ($cleanFilter) {
                        $q->where('clm_licenses.caret_title', 'like', '%' . $cleanFilter . '%');
                    })->orWhere('video_description', 'like', '%^' . $cleanFilter . '%');
                });
            })
            ->when($trendingCarets, function ($q) use ($trendingCarets) {
                $q->whereHas('carets', function ($q) use ($trendingCarets) {
                    $q->whereIn('carets.id', $trendingCarets);
                });
            })
            ->where('videos.is_active', 1)
            ->where('videos.is_blocked', 0) // Exclude blocked videos
            //->where('videos.is_caret', $is_caret)
            ->when($sortName, function ($q) use ($sortName, $orderType) {
                $q->orderBy($sortName, $orderType);
            }, function ($q) {
                $q->orderBy('created_at', 'desc'); // Default sorting
            })
            ->paginate($pagination);

        if ($call === 1) {
            return $data;
        }

        if ($request->is('api/*')) {
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
    }


    public function searchByHash(Request $request,$call=null)
    {
        //return $request->all();
        $filter = $request->input('filter',null);
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $is_caret = $request->input('is_caret', 0);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;
        $trendingHash = null;
        if($filter == null){
            $trendingHash = $this->trendingHash('id');
        }
        if ($filter == '' && $sort != ''){
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
        }

        $data = Videos::select('*')->with(['users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])
        ->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        }])
        ->where(function($q) use($signed_id){
            $q->whereHas('users.followby', function($q)use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            })->orWhere('is_private',0);
        })

        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url"),
        DB::raw("
                    CASE
                        WHEN is_caret = 1
                        AND TRIM(video_description) REGEXP '^\\\\^[^ ]+$'
                        THEN ''
                        ELSE TRIM(video_description)
                    END AS video_description
                ")])
        ->when($filter,function($q) use ($filter){
                $q->where('video_description','like', '%#' . str_replace('#','',$filter) . '%');
            })
        ->when($trendingHash,function($q) use($trendingHash){
            $q->whereHas("hashs", function($q) use($trendingHash) {
                $q->whereIn('hashs.id',$trendingHash);
            });
        })
        ->where('videos.is_active',1)
        ->where('videos.is_blocked',0)
        ->where('videos.is_caret',0)
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->paginate($pagination);


        if($call == 1){
            return $data;
        }

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
    }
    public function searchByUser(Request $request,$call=null)
    {
        //return $request->all();
        $filter = $request->input('filter',null);
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $is_caret = $request->input('is_caret', 0);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;
        $trendingUsers = null;
        if($filter == null){
            $trendingUsers = $this->trendingUsers('id');
        }
        if ($filter == '' && $sort != ''){
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
        }

        $data = Videos::select('*')->with(['users'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }])
        ->withCount([
            'followby as isFollowing'=>function($q) use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            }]);
        }])
        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url"),
                DB::raw("
                CASE
                    WHEN is_caret = 1
                    AND TRIM(video_description) REGEXP '^\\\\^[^ ]+$'
                    THEN ''
                    ELSE TRIM(video_description)
                END AS video_description
            ")])

        ->where(function($q) use($signed_id){
            $q->whereHas('users.followby', function($q)use($signed_id){
                $q->where('follow_by',$signed_id)->limit(1);
            })->orWhere('is_private',0);
        })


        ->when($filter, function($q) use($filter) {
            $cleanFilter = str_replace('@', '', $filter);
            $q->where(function ($query) use ($cleanFilter) {
                $query->whereHas('users', function ($q) use ($cleanFilter) {
                    $q->where('users.username', 'like', '%' . $cleanFilter . '%');
                })->orWhere('video_description', 'like', '%@' . $cleanFilter . '%');
            });
        })


        ->when($trendingUsers,function($q) use($trendingUsers){
            $q->whereHas("users", function($q) use($trendingUsers) {
                $q->whereIn('users.id',$trendingUsers);
            });
        })
        ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
            $query->orderBy($sortName, $orderType);
        })
        ->where('videos.is_active',1)
        ->where('videos.is_blocked',0)
        ->when($sortName===null,function($q){
            $q->orderBy('created_at','desc');
        })
        ->paginate($pagination);

        if($call == 1){
            return $data;
        }

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
    }
    public function likedVideos(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $is_caret = $request->input('is_caret', 0);
        $user_id = $request->input('user_id', 0);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        // with(["users" => function($q) use($user_id){
        //     $q->whereHas("followby", function($q) use($user_id) {
        //         $q->where("user_follows.follow_by", $user_id);
        //     })->with(["followby" => function($q) use($user_id) {
        //         $q->where("user_follows.follow_by", $user_id);
        //     }]);
        // }])->
         $data = Videos::select('*')->with([
            'users'=>function($q) use($signed_id){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }])
            ->withCount([
                'followby as isFollowing'=>function($q) use($signed_id){
                    $q->where('follow_by',$signed_id)->limit(1);
                }]);
            }])
         ->whereHas("likedBy", function($q) use($signed_id) {
            $q->where("video_likes.user_id", $signed_id);
        })
        ->withCount([
            'likedme as isLiked'=>function($q) use($signed_id){
                $q->where('user_id',$signed_id);
            }])
        ->addSelect([DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])
                ->when($filter, function($q) use ($filter) {
                    $q->where(function ($query) use ($filter) {
                        $query->where('id', 'like', '%' . $filter . '%')
                              ->orWhere('video_description', 'like', '%' . $filter . '%');
                    });
                })

            ->when($is_caret,function($q) use($is_caret){
                $q->where('is_caret',$is_caret);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->where('videos.is_blocked',0)
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function followVideos(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $is_caret = $request->input('is_caret', 0);
        $user_id = $request->input('user_id', 0);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        // with(["users" => function($q) use($user_id){
        //     $q->whereHas("followby", function($q) use($user_id) {
        //         $q->where("user_follows.follow_by", $user_id);
        //     })->with(["followby" => function($q) use($user_id) {
        //         $q->where("user_follows.follow_by", $user_id);
        //     }]);
        // }])->
         $data = Videos::select('*')->with([
            'users'=>function($q) use($signed_id){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }])
            ->withCount([
                'followby as isFollowing'=>function($q) use($signed_id){
                    $q->where('follow_by',$signed_id)->limit(1);
                }]);
            }])
            ->withCount([
                'likedme as isLiked'=>function($q) use($signed_id){
                    $q->where('user_id',$signed_id);
                }])
         ->whereHas("users", function($q) use($user_id) {
            $q->whereHas("followby", function($q) use($user_id) {
                $q->where("user_follows.follow_by", $user_id);
            });
        })
        ->addSelect([
            DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
            DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])

            ->when($filter, function($q) use ($filter) {
                $q->where(function ($query) use ($filter) {
                    $query->where('id', 'like', '%' . $filter . '%')
                          ->orWhere('video_description', 'like', '%' . $filter . '%');
                });
            })

            ->when($is_caret,function($q) use($is_caret){
                $q->where('is_caret',$is_caret);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->where('videos.is_blocked',0)
            ->where('videos.is_active',1)
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function videoSatus(Request $request) {
        $video_id = $request->video_id;
        $video = Videos::where('id', $video_id)->first();
        if($video){
            if($video->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $video->update($data);
            return response()->json([ 'data' => $video, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function videoBlock($id) {
        $video_id = $id;
        $video = Videos::where('id', $video_id)->first();
        if($video){
            if($video->is_blocked == 1){
                $data['is_blocked'] = 0;
            }else{
                $data['is_blocked'] = 1;
            }
            $video->update($data);
            return response()->json([ 'data' => $video, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function createCarets(Request $request)
    {
        $data = $request->all();
        $video_ads = $data['video_ids'];

        $data2['license_id'] = isset($data['license_id']) ? trim($data['license_id']): null;
        $data2['sound_id'] = isset($data['sound_id']) ? trim($data['sound_id']): null;
        $data2['video_title'] = trim($data['video_title']);
        $data2['video_description'] = trim($data['video_description']);
        $data2['is_active'] = 0;
        $data2['is_caret'] = 1;
        $data2['is_manual'] = 1;
        $data2['is_allow_comments'] = 0;
        $data2['is_allow_caretCreation'] = 0;
        $data2['caret_processing'] = 1;

        $Videos = Videos::create($data2);
        $data['video_id'] = $Videos->id;
        $Videos->users()->attach($data['user_id']);

        foreach($video_ads as $video_id){
            $vdata['video_id'] =  $data['video_id'];
            $vdata['video_added'] = $video_id;
            $clg = VideoCollages::create($vdata);
        }

        $addata['video_id'] = $data['video_id'];
        if(isset($data['ad_id'])){
            $addata['ad_id'] = $data['ad_id'];
            $ad = VideoAds::create($addata);
        }


        $spdata['video_id'] = $data['video_id'];
        if(isset($data['splash_start_id'])){
            $spdata['splash_id'] = $data['splash_start_id'];
            $spdata['finish'] = 0;
            $spstart = VideoSplash::create($spdata);

        }


        $spdata['video_id'] = $data['video_id'];
        if(isset($data['splash_end_id'])){
            $spdata['splash_id'] = $data['splash_end_id'];
            $spdata['finish'] = 1;
            $spend = VideoSplash::create($spdata);

        }

        return response()->json([ 'data' => $Videos,'status'=>'success', 'message' => 'Caret submitted successfully!'], 200);
    }
    public function uploadtest(Request $request)
    {

    }

    public function store(Request $request)
    {
        ini_set("max_execution_time", 90000);
        Log::info("Step1: Record Insertion.".date('Y-m-d H:i:s', time()));
        $data = $request->all();
        // $validation_rules = [
        //     'user_id' => 'required|int'
        // ];
        // $validator = Validator::make($data, $validation_rules);
        //     if($validator->fails()){
        //         return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        //     }
        if(isset($data['video_description'])){
            $data2['video_description'] = trim($data['video_description']);
            preg_match_all('/(?<!\w)#\w+/',$data2['video_description'],$hashes);
            preg_match_all('/(?<!\w)@\w+/',$data2['video_description'],$mentions);
            preg_match_all('/(?<!\w)\^\w+/',$data2['video_description'],$carets);

           $hash_ids = $this->addHash($hashes);
           $caret_ids = $this->addCarets($carets);
           $tagged_ids = $this->mentionedUsers($mentions);
        }
        $add_new_video = 0;

        if(isset($data['video_title']))
            $data2['video_title'] = trim($data['video_title']);

        if(isset($data['is_private']))
            $data2['is_private'] = trim($data['is_private']);


        $data2['is_active'] = 0;

        if(isset($data['is_allow_comments']))
            $data2['is_allow_comments'] = trim($data['is_allow_comments']);

        if(isset($data['is_allow_caretCreation']))
            $data2['is_allow_caretCreation'] = trim($data['is_allow_caretCreation']);

        if(isset($data['loc_lat']))
            $data2['loc_lat'] = trim($data['loc_lat']);

        if(isset($data['loc_long']))
            $data2['loc_long'] = trim($data['loc_long']);

        if(isset($data['sound_id']))
            $data2['sound_id'] = trim($data['sound_id']);

        if(isset($data['duration']))
            $data2['duration'] = trim($data['duration']);

        if(isset($data['start_time']))
            $data2['start_time'] = trim($data['start_time']);

        if(isset($data['end_time']))
            $data2['end_time'] = trim($data['end_time']);


        if(isset($data['video_id'])){
            $Videos = Videos::where('id', $data['video_id'])->first();
            if($Videos) {
                $Videos->update($data2);
            }
        }else{
            $Videos = Videos::create($data2);
            $data['video_id'] = $Videos->id;
            $add_new_video = 1;
            if(isset($data2['sound_id'])){
                Sounds::where('id',$data2['sound_id'])->increment('video_count' , 1);
            }

        }
        if ($request->hasFile('video_url')) {
            Log::info("Step2: Start Video uploading.".date('Y-m-d H:i:s', time()));
            $input_img = $request->file('video_url'); // $this->uploadFile($data['user_id'],$request->file('video_url'));

            $image = $input_img;
            $extension = $image->getClientOriginalExtension();
            $folder = 'Videos/';
            $name = time();
            $file_path =   $folder. $data['video_id'] . '_' . $name.'.'.$extension;
            $video_export_url =   $folder. $data['video_id'] . '_raw_' . $name.'.'.$extension;
            $video = file_get_contents($image);
            $confirmUpload = Helper::putServerImage($file_path, $video, 'public');


            $uploadFile['video_url'] = $file_path;
            $Videos->update($uploadFile);
            Log::info("Step2A: File Uploaded.".date('Y-m-d H:i:s', time()));
            Log::info("Step3: Complete Video uploading.".date('Y-m-d H:i:s', time()));

        }

        $input['videoFile'] = $request->input('videoFile');
        if(isset($input['videoFile'])) {
            Log::info("Step2: Start Video uploading.".date('Y-m-d H:i:s', time()));
            $FileData = $input['videoFile'];
            if(isset($FileData['file'])){
                $file       = $FileData['file'];
                $file_name  = $FileData['name'];
                $file_name = preg_replace('/[^a-zA-Z0-9-_\.]/', '_', $file_name);

                $video = file_get_contents($file);

                $file_path =  'Videos/' . $data['video_id'] . '_' . time().$file_name;
                $confirmUpload = Helper::putServerImage($file_path, $video, 'public');
                $uploadFile['video_url'] = $file_path;
                $Videos->update($uploadFile);
                Log::info("Step2A: File Uploaded.".date('Y-m-d H:i:s', time()));


            }
            Log::info("Step3: Complete Video uploading.".date('Y-m-d H:i:s', time()));
        }




        $is_notify = 0;
        if(isset($data['user_id'])){
            $data['users'][] = $data['user_id'];
            $is_notify = UserPrivacy::where('id',$data['user_id'])->get()->pluck('followers')->first();
        }
        if(isset($data['users'])){
            $Videos->users()->sync($data['users']);
            if($add_new_video == 1 && $Videos->is_caret == 1){
                $Videos->users()->increment('caret_count' , 1);
            }else if($add_new_video == 1){
                $Videos->users()->increment('video_count' , 1);
            }

            $user_age = $Videos->users()->withCount(['profile'=>function($q){
                $q->select('dob');
            }])->orderBy('profile_count')->first();
            $age = Carbon::parse($user_age->profile_count)->diff(Carbon::now())->format('%y');
            $ageRange = Ages::whereRaw('? between min and max', [$age])->first();
            $ageData['age'] = $ageRange->id;
            $Videos->update($ageData);

        }

        if(isset($hash_ids))
            $Videos->hashs()->sync($hash_ids);
        if(isset($caret_ids))
            $Videos->carets()->sync($caret_ids);
        if(isset($tagged_ids))
                $Videos->tagged()->sync($tagged_ids);

        $return = Videos::select(['*',
        DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])
        ->where('id', $data['video_id'])->first();


        if(env('IS_GPU') == FALSE){
            $queueName = env('UPLOAD_VIDEO_QUEUE'); // Default to 'default' if not set
            ProcessUploadVideoJob::dispatch($data['video_id'])->onQueue($queueName);
        }

        Log::info("Step4: Video dispatch.".date('Y-m-d H:i:s', time()));
        return response()->json([ 'data' => $return, 'message' => 'Record stored successfully!'], 200);
    }
    #mentionedUsers
    public function mentionedUsers($data){
        //return $data[0];
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                $res = User::where('username',trim(str_replace('@','',$dt)))->first();
                if($res)
                    $ret[] =  $res->id;
            }
        }
        return $ret;
    }
    public function addHash($data){
        //return $data[0];
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                //return $dt;
                $arr['title'] = trim(str_replace('#','',$dt));
                $res = Hashs::updateOrCreate($arr);
                $ret[] =  $res->id;
            }
        }
        return $ret;
    }
    public function addCarets($data){
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                $arr['title'] = trim(str_replace('^','',$dt));
                $Carets['id'] = '';
                $Carets = Carets::where('title', $arr['title'])->first();
                if(isset($Carets['id'])){
                    $arr['updated_at'] = Carbon::now();
                    $Carets->update($arr);
                }else{
                    //$arr['updated_at'] = Carbon::now();
                    $Carets = Carets::create($arr);
                }
                $ret[] =  $Carets->id;
            }
        }
        return $ret;
    }

    public function getHash(Request $request){

        $filter_ch = $request->input('filter_ch', null);
        $filter = $request->input('filter', null);
        $pageSize = $request->get('pageSize', 25);
        $Hashs = Hashs::orderBy('title','ASC')
        ->select('id as value', 'title as label')
        ->when($filter_ch, function($q) use ($filter_ch) {
            $q->where('title', 'LIKE', "$filter_ch%");
        })->limit($pageSize)->get();
        return $this->sendSuccessResponse('data', $Hashs, 'Record retrieved successfully!');
    }

    public function getCarets(Request $request){
        $filter_ch = $request->input('filter_ch', null);
        $filter = $request->input('filter', null);
        $pageSize = $request->get('pageSize', 25);
        $Carets = Carets::orderBy('title','ASC')
        ->select('id as value', 'title as label')
        ->when($filter_ch, function($q) use ($filter_ch) {
            $q->where('title', 'LIKE', "$filter_ch%");
        })->limit($pageSize)->get();
        return $this->sendSuccessResponse('data', $Carets, 'Record retrieved successfully!');
    }

    public function getFriends(Request $request){
        $user_id = $request->input('user_id', null);
        $filter_ch = $request->input('filter_ch', null);
        $filter = $request->input('filter', null);
        $pageSize = $request->get('pageSize', 25);

        $User = User::select('id as value', 'username as label')
        ->where('username', 'LIKE', "$filter_ch%")
        ->where('id','<>',$user_id)
        ->orderBy('username','ASC')
        ->limit($pageSize)->get();
        if(isset($User)){
            return $this->sendSuccessResponse('data', $User, 'Record retrieved successfully!');
        }else{

            return $this->sendSuccessResponse('data',[], 'Record retrieved successfully!');
        }
    }

    // public function getFriends(Request $request){
    //     $user_id = $request->input('user_id', null);
    //     $filter_ch = $request->input('filter_ch', null);
    //     $filter = $request->input('filter', null);
    //     $pageSize = $request->get('pageSize', 25);

    //     $User = User::with(['followby'=>function($q) use ($filter_ch,$pageSize,$user_id) {
    //         $q->select('follow_by as value', 'username as label')
    //         ->where('username', 'LIKE', "$filter_ch%")
    //         ->where('follow_by','<>',$user_id)
    //         ->whereNull('user_follows.deleted_at')
    //         ->limit($pageSize);
    //     }
    //     ])->where('id',$user_id)->first();
    //     if(isset($User->followby)){
    //         return $this->sendSuccessResponse('data', $User->followby, 'Record retrieved successfully!');
    //     }else{

    //         return $this->sendSuccessResponse('data',[], 'Record retrieved successfully!');
    //     }
    // }

    public function uploadFile($user_id,$input_img) {

        if ($input_img) {
            $image = $input_img;
            $name = time().'.'.$image->getClientOriginalExtension();
            $file_path =  'Videos/' . $user_id . '_' . $name;
            $image = file_get_contents($image);
            $confirmUpload = Helper::putS3Image($file_path, $image, 'public');

            if($confirmUpload)
                return $file_path;
            else
                return response()->json(['status'=>'error','error' => 'File does not uploaded properly please try again.'], 400);
        }
        return response()->json(['status'=>'error','error' => 'File does not uploaded properly please try again.'], 400);
    }

    public function showWeb($id)
    {

        $data = Videos::select([
            '*',
            DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
            DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")
        ])
        ->with([
            'users'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }]);
            },'sounds'])
            ->where('id', $id)->first();

                return $this->sendSuccessResponse('data', $data, 'Record retrieved successfully!');

    }
    public function singleWeb($id)
    {
        if(!isset($id)){
            $id = 0;
        }

        $data = Videos::select([
            '*',
            DB::raw("CONCAT('".$this->awsUrl."', video_url) AS videoUrl"),
            DB::raw("CONCAT('".$this->awsUrl."', image_url) AS imageUrl")
        ])
        ->with([
            'users'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }]);
            },'sounds'])
            ->where('id', $id)->first();
            if(isset($data->videoUrl)){
                $res['videoUrl'] = $data->videoUrl;
                $res['imageUrl'] = $data->imageUrl;
                if(isset($data->users[0]['username'])){
                    $res['username'] = "@".$data->users[0]['username'];
                }else{
                    $res['username'] = "@Guest" ;
                }
                if(isset($data->sounds[0]['sound_title'])){
                    $res['music'] = $data->users[0]['sound_title'];
                }else{
                    $res['music'] = "Original";
                }
                $res['tags'] = $data->video_description;

                return view("videoPage", ["data"=>$res]);
            }else{
                return redirect('/');
            }

    }
    public function show($id)
    {

        $data = Videos::select([
            '*',
            DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS videoFile"),
            DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image"),
        ])
        ->with([
            'users'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }]);
            }])
        ->where('id', $id)->first();
        $users = $data->users->map(function ($item, $key) {
            return $item->id;
        });
        unset($data->users);
        $data->users = $users;
        return response()->json([ 'data' => $data, 'message' => ''], 200);

    }

    public function showCaret($id)
    {

        $data = Videos::select([
            '*',
            DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS video_raw_url"),
            DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
            DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")
        ])
        ->with([
            'users'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }]);
            },
            'videos'=>function($q){
                $q->select(['*',
                DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_raw_url) AS video_raw_url"),
                DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")]);
            },
            'videoads'=>function($q){
                $q->select(['*',
                DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")]);
            },
            'videosplashes'=>function($q){
                $q->select(['*',
                DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
                DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")]);
            },
            'videosound'=>function($q){
                $q->select(['*',
                DB::raw("CONCAT('".$this->awsUrl."', sound_url) AS sound_url")]);
            },
            'license'=>function($q){
                $q->select(['*',
                DB::raw("CONCAT('".$this->awsUrl."', caret_logo) AS caret_logo")]);
            }])
        ->where('id', $id)->first();
        $users = $data->users->map(function ($item, $key) {
            return $item->id;
        });
        unset($data->users);
        $data->users = $users;
        return response()->json([ 'data' => $data, 'message' => ''], 200);

    }


    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $Videos = Videos::where('id', $id)->first();
        if($Videos->is_caret == 1){
            $Videos->users()->decrement('caret_count' , 1);
        }else{
            $Videos->users()->decrement('video_count' , 1);
        }
        $Videos->delete();
        return response()->json(['data'=>'true','status'=>'success', 'message' => 'Deleted successfully!'], 200);
    }

    public function LikeUnlike(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'user_id' => 'required|int',
            'video_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['video_id'] = trim($data['video_id']);
        $data2['user_id'] = trim($data['user_id']);
        $existing = VideoLikes::where('video_id',$data2['video_id'])->where('user_id',$data2['user_id'])->first();
        $Videos = Videos::where('id',$data2['video_id'])->with('users')->first();
        if(!$Videos){
            return response()->json(['status'=>'error','error' => 'Invalid Information'], 400);
        }
        if(!$existing){
            $VideoLikes = VideoLikes::create($data2);
            $Videos->increment('like_count' , 1);
            $Videos->users()->increment('video_like_count' , 1);
            $return = ['isLiked'=>1,'video_id'=>$data2['video_id']];

            if($Videos){

                $users = $Videos->users()->where('users.id', '<>', $data['user_id'])->get();

                // Fetch the privacy settings for all these users in one go
                $user_privacies = UserPrivacy::whereIn('user_id', $users->pluck('id'))->get()->keyBy('user_id');

                // Initialize device tokens array
                $device_tokens = [];

                // Loop through the users and check if notifications are allowed
                foreach ($users as $user) {
                    // Check if user has privacy settings and notifications are allowed
                    $is_notify = $user_privacies->get($user->id)->likes ?? 0;

                    // If notifications are allowed, add the device token to the list
                    if ($is_notify == 1) {
                        $device_tokens[] = $user->device_token;
                    }
                }

                // Get unique device tokens only
                $device_tokens = collect($device_tokens)->filter()->unique()->values()->toArray();

               //->privacy()->where('likes',1)
               if($device_tokens){
                Helper::sendFCMNotification($device_tokens, ['title' => 'New Video Like', 'body' => 'New Video Like',
                'data' => [
                    'title' => 'New Video Like', 'body' => 'New Video Like'
                    ]
                ]);
               }

            }

            return response()->json([ 'data' => $return, 'message' => 'Liked successfully!'], 200);
        }else{
            $existing->delete();
            $Videos->decrement('like_count' , 1);
            $Videos->users()->decrement('video_like_count' , 1);
            $return = ['isLiked'=>0, 'video_id'=>$data2['video_id']];
            return response()->json([ 'data' => $return, 'message' => 'Un-Liked successfully!'], 200);
        }
    }

    public function isLike(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'user_id' => 'required|int',
            'video_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['video_id'] = trim($data['video_id']);
        $data2['user_id'] = trim($data['user_id']);
        $existing = VideoLikes::where('video_id',$data2['video_id'])->where('user_id',$data2['user_id'])->first();
        if($existing){
            return response()->json([ 'data' => $existing, 'message' => 'Found successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Not Found","status"=>"error", "msg"=>"invalidate" ], 400);
        }
    }

    #####COMMENTS###################################
    public function comments(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $video_id = $request->input('video_id', null);
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 25;

        if ($sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = VideoComments::with(['user'=>function($q){
            $q->with(['profile'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
            }]);
        },'reply'=>function($q){$q->with('user');}])
        ->withCount([
            'likedme as isLiked'=>function($q) use($signed_id){
                $q->where('user_id',$signed_id);
            }])
        ->whereNull('parent_id')
        ->when($filter,function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                    ->orWhere('comments','like', '%' . $filter . '%');
            })
            ->when($video_id,function($q) use($video_id){
                $q->where('video_id',$video_id);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function postComment(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'user_id' => 'required|int',
            'video_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }
        $data2['video_id'] = trim($data['video_id']);
        $data2['user_id'] = trim($data['user_id']);
        $data2['comments'] = trim($data['comments']);

        if(isset($data['parent_id']))
            $data2['parent_id'] = trim($data['parent_id']);

        // $Videos = Videos::where('id',$data2['video_id'])->first();
        // if(!$Videos){
        //     return response()->json(['status'=>'error','error' => 'Invalid Information'], 400);
        // }

        if(isset($data['comment_id'])){
            $existing = VideoComments::where('id',$data2['comment_id'])
            ->where('video_id',$data2['video_id'])
            ->where('user_id',$data2['user_id'])
            ->first();
            if($existing)
                $existing->update($data2);
            else
                return response()->json(['status'=>'error','error' => 'Invalid Information'], 400);

            //Videos::where('id',$data2['video_id'])->increment('like_count' , 1);
            return response()->json([ 'data' => $existing, 'message' => 'Liked successfully!'], 200);
        }else{

            $existing = VideoComments::create($data2);
            Videos::where('id',$data2['video_id'])->increment('comments_count' , 1);

            $data = VideoComments::with(['user'=>function($q){
                $q->with(['profile'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".$this->awsUrl."', user_photo) AS user_photo")]);
                }]);
            },'reply'=>function($q){$q->with('user');}])
            ->withCount([
            'likedme as isLiked'=>function($q) use( $data2){
                $q->where('user_id', $data2['user_id']);
            }])
            ->where('id',$existing->id)->first();

            $Video = Videos::where('id',$data2['video_id'])->first();
            if($Video){
                // Fetch the users associated with the video, excluding the current user
                    $users = $Video->users()->where('users.id', '<>', $data['user_id'])->get();

                    // Fetch the privacy settings for all these users in one go
                    $user_privacies = UserPrivacy::whereIn('user_id', $users->pluck('id'))->get()->keyBy('user_id');

                    // Initialize device tokens array
                    $device_tokens = [];

                    // Loop through the users and check if notifications are allowed
                    foreach ($users as $user) {
                        // Check if user has privacy settings and allows comment notifications
                        $is_notify = $user_privacies->get($user->id)->comments ?? 0;

                        // If notifications are allowed, add the device token to the list
                        if ($is_notify == 1) {
                            $device_tokens[] = $user->device_token;
                        }
                    }
                // Get unique device tokens only
                $device_tokens = collect($device_tokens)->filter()->unique()->values()->toArray();
               if($device_tokens){
                    Helper::sendFCMNotification($device_tokens, ['title' => 'New Comment', 'body' => 'New video Comment',
                    'data' => [
                        'title' => 'New Comment', 'body' => 'New video Comment'
                        ]
                    ]);
                }
            }

            return response()->json([ 'data' => $data, 'message' => 'Liked successfully!'], 200);
        }
    }

    public function deleteComment(Request $request,$id)
    {
        $VideoComments = VideoComments::where('id', $id)->first();
        $replies = VideoComments::where('parent_id', $VideoComments->id)->count();
        $VideoComments->delete();
        VideoComments::where('parent_id', $VideoComments->id)->delete();
        $replies++;
        Videos::where('id', $VideoComments->video_id)->decrement('comments_count' , $replies);
        return response()->json([ 'data' => '','status'=>'success', 'message' => 'Deleted successfully!'], 200);
    }



    public function LikeUnlikeComment(Request $request) {
        $data = $request->all();
        $validation_rules = [
            'user_id' => 'required|int',
            'comment_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data = $request->all();
        $data2['comment_id'] = trim($data['comment_id']);
        $data2['user_id'] = trim($data['user_id']);
        $existing = VideoCommentLikes::where('comment_id',$data2['comment_id'])->where('user_id',$data2['user_id'])->first();
        $VideoComments = VideoComments::where('id',$data2['comment_id'])->first();
        if(!$VideoComments){
            return response()->json(['status'=>'error','error' => 'Invalid Information'], 400);
        }

        if(!$existing){
            VideoCommentLikes::create($data2);
            $VideoComments->increment('like_count' , 1);
            $return = ['isLiked'=>1,'comment_id'=>$data2['comment_id']];


            if($VideoComments){
                //$device_tokens = $VideoComments->user()->get()->pluck('device_token')->toArray();
               // Fetch the users who commented, excluding the current user
                $users = $VideoComments->user()->where('users.id', '<>', $data['user_id'])->get();

                // Fetch the privacy settings for these users in a single query
                $user_privacies = UserPrivacy::whereIn('user_id', $users->pluck('id'))->get()->keyBy('user_id');

                // Initialize device tokens array
                $device_tokens = [];

                // Loop through the users and check if notifications for comments are enabled
                foreach ($users as $user) {
                    // Get the user's comment notification setting (default to 0 if not found)
                    $is_notify = $user_privacies->get($user->id)->comments ?? 0;

                    // If notifications are enabled, add the device token
                    if ($is_notify == 1) {
                        $device_tokens[] = $user->device_token;
                    }
                }

                // Now $device_tokens contains the device tokens of users who allow comment notifications

               if($device_tokens){
                    Helper::sendFCMNotification($device_tokens, ['title' => 'New Comment Like', 'body' => 'New Video Comment Like',
                    'data' => [
                        'title' => 'New Comment Like', 'body' => 'New Video Comment Like'
                        ]
                    ]);
               }
            }

            return response()->json([ 'data' => $return, 'message' => 'Liked successfully!'], 200);
        }else{
            $existing->delete();
            $VideoComments->decrement('like_count' , 1);
            $return = ['isLiked'=>0,'comment_id'=>$data2['comment_id']];
            return response()->json([ 'data' => $return, 'message' => 'Un-Liked successfully!'], 200);
        }
    }

    public function reportVideos(Request $request)
    {
        $data = $request->all();
        $validation_rules = [
            'report_by' => 'required|int',
            'video_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $data2['video_id'] = trim($data['video_id']);
        $data2['report_by'] = trim($data['report_by']);

        if(isset($data['remarks']))
            $data2['remarks'] = trim($data['remarks']);

        $exist = VideoReports::where('video_id', $data['video_id'])->where('report_by', $data['report_by'])->first();
        if(!$exist){
            $VideoReports = VideoReports::create($data2);
        }else{
            return response()->json(['status'=>'error','error' => 'Already Reported'], 400);
        }
        $Videos = Videos::where('id',$data2['video_id'])->first();
        $Videos->fill(['is_blocked'=>1]);
        $Videos->save();
        return response()->json([ 'data' => '','status'=>'success', 'message' => 'Reported successfully!'], 200);
    }

    public function watchVideos(Request $request)
    {
        try {
            // Retrieve and validate the request data
            $data = $request->all();

            if (!isset($data['video_id']) || !isset($data['user_id'])) {
                return response()->json([
                    'data' => [],
                    'status' => 'error',
                    'message' => 'Invalid data provided.',
                ], 400);
            }

            $data2['video_id'] = trim($data['video_id']);
            $data2['user_id'] = trim($data['user_id']);

            // Check if the view already exists
            $exist = VideoViews::where('video_id', $data2['video_id'])
                ->where('user_id', $data2['user_id'])
                ->first();

            if (!$exist) {
                // Create a new view entry
                $VideoViews = VideoViews::create($data2);

                // Increment the watch count for the video
                Videos::where('id', $data2['video_id'])->increment('watch_count', 1);

                return response()->json([
                    'data' => $VideoViews,
                    'status' => 'success',
                    'message' => 'Stored successfully!',
                ], 200);
            }

            // Return response if the view already exists
            return response()->json([
                'data' => [],
                'status' => 'success',
                'message' => 'Stored successfully!',
            ], 200);

        } catch (\Exception $e) {
            // Handle any exceptions and log the error
            \Log::error('Error storing video view: ' . $e->getMessage());

            return response()->json([
                'data' => [],
                'status' => 'error',
                'message' => 'An error occurred while storing the video view.',
            ], 500);
        }
    }

    public function watchAdCount(Request $request)
    {
        $data = $request->all();
        $data2['ad_id'] = trim($data['ad_id']);
        $data2['user_id'] = trim($data['user_id']);
        $exist = AdViews::where('ad_id', $data['ad_id'])->where('user_id', $data['user_id'])->first();
        if(!$exist){
            $VideoViews = AdViews::create($data2);
            Ads::where('id',$data2['ad_id'])->increment('watch_count' , 1);
            CampaignsAds::where('ad_id',$data2['ad_id'])->increment('watch_count' , 1);
            return response()->json([ 'data' => $VideoViews,'status'=>'success', 'message' => 'Stored successfully!'], 200);
        }
        return response()->json([ 'data' => [],'status'=>'success', 'message' => 'Stored successfully!'], 200);
    }

    // public function destroyCategory($id)
    // {
    //     $SoundCategories = SoundCategories::where('id', $id)->first();
    //     $SoundCategories->delete();
    //     return response()->json(['','status'=>'success', 'message' => 'Deleted successfully!'], 200);
    // }

    // public function destroyReport($id)
    // {
    //     $SoundReports = SoundReports::where('id', $id)->first();
    //     $SoundReports->delete();
    //     return response()->json(['','status'=>'success', 'message' => 'Deleted successfully!'], 200);
    // }

    public function getUsers(Request $request){
        $users = User::orderBy('id','DESC')
        ->select('id as value', 'username as label', 'email as email')
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }

    public function sendNotification(Request $request){
        $data = $request->all();
        $device_tokens = trim($data['device_tokens']);
        //$device_tokens = 'ceEECLe4T01BhQYN6Ms3_Z:APA91bF6lfA1hrKg1nQE2wAkaxqGe-xpaJVQww8Utc3plc2YcImHrAdGLZvqbgDHj_R4yb4V6njLBqN4sFV_2xBsrJTIfjgK8Z8rhf1vc3R8tAtdxSlQzIQbEOe1AnPEyhNXHLVj4H2p';

        $res = Helper::sendFCMNotification($device_tokens, ['title' => 'New Video', 'body' => 'video uploaded successfully',
        'data' => [
            'title' => 'New Video', 'body' => 'video uploaded successfully'
            ]
        ]);
        return $res;
    }

    public function deleteVideos(){
        $oldVideos = array();

        //$files = collect(Storage::listContents('', true));
        $files = collect(Storage::listContents('', true))
            ->reject(function ($file) {
                // Reject files that are within the 'Audio' folder
                return str_contains($file['path'], 'Audio/');
        });

         foreach($files as $file) {
            if (isset($file['extension'])){
                if (($file['extension'] == 'mp4' || $file['extension'] == 'gif') && $file['timestamp'] < now()->subDays(7)->getTimestamp()) {
                    $oldVideos[] = $file;
                    Storage::delete($file['path']);
                    }
            }

        }
    }


    public function downloadVideo(Request $request)
    {
        // Validate request
        $request->validate([
            'video_url' => 'required|url',
        ]);

        $videoUrl = $request->input('video_url');
        $localFilePath = 'public/Videos/my_video.mp4';

        try {
            // Fetch the file content from the URL
            $fileContent = file_get_contents($videoUrl);

            if ($fileContent === false) {
                return response()->json(['message' => 'Failed to fetch the file from the URL.'], 500);
            }

            $localDir = dirname(storage_path('app/' . $localFilePath));
            if (!file_exists($localDir)) {
                mkdir($localDir, 0777, true);
            }

            // Save the file content to local storage
            $success = Storage::disk('local')->put($localFilePath, $fileContent);

            if ($success) {
                return response()->json(['message' => 'Video downloaded and saved locally.'], 200);
            } else {
                return response()->json(['message' => 'Failed to save the video locally.'], 500);
            }
        } catch (\Exception $e) {

            return response()->json(['message' => 'Failed to download or save the video.'], 500);
        }
    }

    public function store_v2(Request $request)
    {
        ini_set("max_execution_time", 90000);
        Log::info("Step1: Record Insertion.".date('Y-m-d H:i:s', time()));
        $data = $request->all();
        // $validation_rules = [
        //     'user_id' => 'required|int'
        // ];
        // $validator = Validator::make($data, $validation_rules);
        //     if($validator->fails()){
        //         return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        //     }
        if(isset($data['video_description'])){
            $data2['video_description'] = trim($data['video_description']);
            preg_match_all('/(?<!\w)#\w+/',$data2['video_description'],$hashes);
            preg_match_all('/(?<!\w)@\w+/',$data2['video_description'],$mentions);
            preg_match_all('/(?<!\w)\^\w+/',$data2['video_description'],$carets);

           $hash_ids = $this->addHash($hashes);
           $caret_ids = $this->addCarets($carets);
           $tagged_ids = $this->mentionedUsers($mentions);
        }
        $add_new_video = 0;

        if(isset($data['video_title']))
            $data2['video_title'] = trim($data['video_title']);

        if(isset($data['is_private']))
            $data2['is_private'] = trim($data['is_private']);


        $data2['is_active'] = 0;

        if(isset($data['is_allow_comments']))
            $data2['is_allow_comments'] = trim($data['is_allow_comments']);

        if(isset($data['is_allow_caretCreation']))
            $data2['is_allow_caretCreation'] = trim($data['is_allow_caretCreation']);

        if(isset($data['loc_lat']))
            $data2['loc_lat'] = trim($data['loc_lat']);

        if(isset($data['loc_long']))
            $data2['loc_long'] = trim($data['loc_long']);

        if(isset($data['sound_id']))
            $data2['sound_id'] = trim($data['sound_id']);

        if(isset($data['duration']))
            $data2['duration'] = trim($data['duration']);

        if(isset($data['start_time']))
            $data2['start_time'] = trim($data['start_time']);

        if(isset($data['end_time']))
            $data2['end_time'] = trim($data['end_time']);


        if(isset($data['video_id'])){
            $Videos = Videos::where('id', $data['video_id'])->first();
            if($Videos) {
                $Videos->update($data2);
            }
        }else{
            $Videos = Videos::create($data2);
            $data['video_id'] = $Videos->id;
            $add_new_video = 1;
            if(isset($data2['sound_id'])){
                Sounds::where('id',$data2['sound_id'])->increment('video_count' , 1);
            }

        }
        $fileName = $data['file_name'];
        $uploadFile['video_url'] ="Videos/{$fileName}";
        $Videos->update($uploadFile);
        Log::info("Step3: Complete Video uploading.".date('Y-m-d H:i:s', time()));

       /* if ($request->hasFile('video_url')) {
            Log::info("Step2: Start Video uploading.".date('Y-m-d H:i:s', time()));
            $input_img = $request->file('video_url'); // $this->uploadFile($data['user_id'],$request->file('video_url'));

            $image = $input_img;
            $extension = $image->getClientOriginalExtension();
            $folder = 'Videos/';
            $name = time();
            $file_path =   $folder. $data['video_id'] . '_' . $name.'.'.$extension;
            $video_export_url =   $folder. $data['video_id'] . '_raw_' . $name.'.'.$extension;
            $video = file_get_contents($image);
            $confirmUpload = Helper::putServerImage($file_path, $video, 'public');


            $uploadFile['video_url'] = $file_path;
            $Videos->update($uploadFile);
            Log::info("Step2A: File Uploaded.".date('Y-m-d H:i:s', time()));
            Log::info("Step3: Complete Video uploading.".date('Y-m-d H:i:s', time()));

        }*/

        $input['videoFile'] = $request->input('videoFile');
        if(isset($input['videoFile'])) {
            Log::info("Step2: Start Video uploading.".date('Y-m-d H:i:s', time()));
            $FileData = $input['videoFile'];
            if(isset($FileData['file'])){
                $file       = $FileData['file'];
                $file_name  = $FileData['name'];
                $file_name = preg_replace('/[^a-zA-Z0-9-_\.]/', '_', $file_name);

                $video = file_get_contents($file);

                $file_path =  'Videos/' . $data['video_id'] . '_' . time().$file_name;
                $confirmUpload = Helper::putServerImage($file_path, $video, 'public');
                $uploadFile['video_url'] = $file_path;
                $Videos->update($uploadFile);
                Log::info("Step2A: File Uploaded.".date('Y-m-d H:i:s', time()));


            }
            Log::info("Step3: Complete Video uploading.".date('Y-m-d H:i:s', time()));
        }




        $is_notify = 0;
        if(isset($data['user_id'])){
            $data['users'][] = $data['user_id'];
            $is_notify = UserPrivacy::where('id',$data['user_id'])->get()->pluck('followers')->first();
        }
        if(isset($data['users'])){
            $Videos->users()->sync($data['users']);
            if($add_new_video == 1 && $Videos->is_caret == 1){
                $Videos->users()->increment('caret_count' , 1);
            }else if($add_new_video == 1){
                $Videos->users()->increment('video_count' , 1);
            }

            $user_age = $Videos->users()->withCount(['profile'=>function($q){
                $q->select('dob');
            }])->orderBy('profile_count')->first();
            $age = Carbon::parse($user_age->profile_count)->diff(Carbon::now())->format('%y');
            $ageRange = Ages::whereRaw('? between min and max', [$age])->first();
            $ageData['age'] = $ageRange->id;
            $Videos->update($ageData);

        }

        if(isset($hash_ids))
            $Videos->hashs()->sync($hash_ids);
        if(isset($caret_ids))
            $Videos->carets()->sync($caret_ids);
        if(isset($tagged_ids))
                $Videos->tagged()->sync($tagged_ids);

        $return = Videos::select(['*',
        DB::raw("CONCAT('".$this->awsUrl."', video_url) AS video_url"),
        DB::raw("CONCAT('".$this->awsUrl."', video_export_url) AS video_export_url"),
        DB::raw("CONCAT('".$this->awsUrl."', image_url) AS image_url")])
        ->where('id', $data['video_id'])->first();

        if(env('IS_GPU') == FALSE){
            $queueName = env('UPLOAD_VIDEO_QUEUE'); // Default to 'default' if not set
            ProcessUploadVideoJob::dispatch($data['video_id'])->onQueue($queueName);
        }

        Log::info("Step4: Video dispatch.".date('Y-m-d H:i:s', time()));
        return response()->json([ 'data' => $return, 'message' => 'Record stored successfully!'], 200);
    }

    public function processHLSVideo()
    {
        $video = Videos::where('is_active', 1)
        ->where('is_m3u8', 0)
        ->orderBy('id', 'desc')
        ->first();

        if (!$video) {
            return false;
        }
        $isVideo = Helper::copyS3Video($video->video_raw_url);
        $arr['is_m3u8'] = 1;
        $video_url = $video->video_raw_url;
        if(!isset($video->video_raw_url)){
            $arr['video_raw_url'] = $video->video_url;
            $video_url = $video->video_url;
        }
        $video->update($arr);
        if($isVideo){
            Helper::convertToHLS($video_url, $video->id);
            // if(env('IS_GPU') == true){
            //     HelperGPU::convertToHLS_GPU($video_url, $video->id);
            // }else{
            //     Helper::convertToHLS($video_url, $video->id);
            // }

        }

        return response()->json($video);
    }

    public function processHLSVideos(Request $request)
    {
        $videos = Videos::where('is_active', 1)
                    ->where('is_m3u8', 0) // Exclude video URLs containing "m3u8"
                    ->orderBy('id', 'desc')
                    ->limit(10) // Process 10 videos
                    ->get();

        if ($videos->isEmpty()) {
            return false;
        }

        foreach ($videos as $video) {
            $isVideo = Helper::copyS3Video($video->video_raw_url);
            $arr['is_m3u8'] = 1;
            $video_url = $video->video_raw_url;

            if (!$video->video_raw_url) {
                $arr['video_raw_url'] = $video->video_url;
                $video_url = $video->video_url;
            }

            $video->update($arr);

            Helper::convertToHLS($video_url, $video->id);
        }

        return response()->json(['message' => 'Processing started for 10 videos']);
    }





}
