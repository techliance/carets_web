<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Interests;
use App\Mail\SendMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use App\Permission as ExtendedPermission;
use App\Module;
use App\UserPrivacy;
use App\UserProfiles;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use App\Notifications\SignupActivate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Symfony\Component\HttpKernel\Profiler\Profile;

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setInterests(Request $request){
        if($request->user_interest_values){
            $user_interest_values = explode(",",$request->user_interest_values);
            foreach($user_interest_values as $interest){
                $inte['user_id'] = $request->user_id;
                $inte['cat_id'] = $interest;
                Interests::updateOrCreate(['user_id' =>  $request->user_id,'cat_id' => $interest],$inte);
            }
            return response()->json(['updated successfully.'], 200);
        }else{
            return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
        }
    }

    public function register(Request $request)
    {
        //return $request->all();
        $validate = 0;
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'username' => $request->username,
            'device_id' => $request->device_id,
            'device_type' => $request->device_type,
            'device_model' => $request->device_model,
            'device_token' => $request->device_token,
            'device_os' => $request->device_os
        ];



        if($request->password){
            $validation_rules = [
                'username' => 'required|min:5|unique:users',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8'
            ];
            $data['password'] = bcrypt($request->password);
            $data['activation_token'] = str_random(60);

            $validator = Validator::make($data, $validation_rules);
            if ($validator->fails()) {
                return response()->json( ["error"=>$validator->errors()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }

            $validate = 1;
        }
        if($request->fb_token){
            $validation_rules = [
                'fb_token' => 'required|min:3|unique:users'
            ];
            $data['fb_token'] = $request->fb_token;
            $data['email_verified_at'] = Carbon::now();
            $validator = Validator::make($data, $validation_rules);
            if($validator->fails()){
                return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }


            $validate = 1;
        }
        if($request->google_token){
            $validation_rules = [
                'google_token' => 'required|min:3|unique:users'
            ];
            $data['google_token'] = $request->google_token;
            $data['email_verified_at'] = Carbon::now();
            $validator = Validator::make($data, $validation_rules);
            if($validator->fails()){
                return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }
            $validate = 1;
        }


        if($request->ios_token){
            $validation_rules = [
                'ios_token' => 'required|min:3|unique:users'
            ];
            $data['ios_token'] = $request->ios_token;
            $data['email_verified_at'] = Carbon::now();
            $validator = Validator::make($data, $validation_rules);
            if($validator->fails()){
                return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }
            $validate = 1;
        }

        if($validate == 1){

            $user = User::create($data);
            $newdata = ['user_id'=>$user->id];
            $UserProfiles = UserProfiles::create($newdata);
            $user->assignRole([2]);


            $UserPrivacy['user_id'] = $user->id;
            $UserPrivacy['likes'] = 1;
            $UserPrivacy['comments'] = 1;
            $UserPrivacy['followers'] = 1;
            $UserPrivacy['mentions'] = 1;
            UserPrivacy::create($UserPrivacy);
            //return $user->id;
            $token = $user->createToken('TutsForWeb')->accessToken;
            $user = User::where('id',$user->id)->with(['userInterests','profile'])->first();

            if($request->user_interest_values){
                $user_interest_values = explode(",",$request->user_interest_values);
                foreach($user_interest_values as $interest){
                    $inte['user_id'] = $user->id;
                    $inte['cat_id'] = $interest;
                    Interests::updateOrCreate(['user_id' => $user->id,'cat_id' => $interest],$inte);
                }
            }

            if($request->password){
                $user->notify(new SignupActivate($user));
                if($user->email){
                    $data = array(
                        'name'      =>  $user->username,
                        'message'   =>   'Welcome to Carets. Create, Upload and share videos ',
                        'subject'   =>   'Welcome to Carets'
                    );

                    Mail::to($user->email)->send(new SendMail($data));
                }
            }
          if($token!=''){
                return response([
                    'data'=> $user,
                    'status' => 'success',
                    'token' => $token
                ], 200)
                    ->header('Access-Control-Expose-Headers', 'Authorization')
                    ->header('Authorization', $token);
            }
        }else{
            return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
        }
    }

    public function checkSignupClmUser(Request $request)
    {
        $user = User::where("email", $request->email)->first();
        if($user){
            $roleFound = false;
            foreach ($user->roles as $item) {
                if ($item['name'] === "advertiser") {
                    $roleFound = true;
                    break;
                }
            }
            if (!$roleFound) {
                return response()->json([
                    "customMessage" => "User with Email already exist with the Role CLM, Do you want to Create new User with Advertiser Roles?",
                    "error" => true,
                ]);
            }else if($roleFound){
                return response()->json([
                    "customMessage" => "User already exists",
                    "error" => true,
                ]);
            }
        }
        return $this->registerAdvertiser($request);
    }
    public function registerAdvertiser(Request $request){


            //return $request->email;
            $validation_rules = [
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8'
            ];
            $data['email'] = $request->email;
            $data['password'] = bcrypt($request->password);
            $data['activation_token'] = str_random(60);


            $validator = Validator::make($data, $validation_rules);
            if ($validator->fails()) {
                return response()->json( ["error"=>$validator->errors()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }else{
                $user = User::create($data);
                $newdata = ['user_id'=>$user->id];
                $UserProfiles = UserProfiles::create($newdata);
                $user->assignRole(['advertiser']);
                $user->notify(new SignupActivate($user));
            }

            if($user){
                return response([
                    'data'=> $user,
                    'status' => 'success',
                    'token' => $data['activation_token']
                ], 200)
                    ->header('Access-Control-Expose-Headers', 'Authorization')
                    ->header('Authorization', $data['activation_token']);
            } else {
                return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
            }



    }

    public function checkLogin(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if($request->has("role") && $request->role !== null)
        {
            $roleToAssign = $request->role;
            $userToAssignRole = User::where("email", $request->email)->first();
            if($userToAssignRole){
                $userToAssignRole->assignRole([$roleToAssign]);
            }
        }

        if (auth()->attempt($credentials)) {
            $user = User::with(['userInterests','profile'])->find(Auth::user()->id);
            $rolePermissions = $user->getAllPermissions();
            $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                return $item->name;
            });
            $user->role_permissions = $rolePermissionsMapped;
            if($user->activation_token == "" || $user->activation_token == null){
                $token = auth()->user()->createToken('baseline')->accessToken;

            }else{
                return response([
                    'status' => 'error',
                    'error' => 'Please verify your email address to login, verification email is already sent!',
                    'message' => 'Email not verified.'
                ], 400);
            }
        }

        if(isset($token) && $token!=''){
            //$profile = UserProfiles::where('user_id', $user->id)->first();
            //$user['profile'] = $profile;
            return response([
                'data'=> $user,
                'status' => 'success',
                'token' => $token
            ], 200)
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Authorization', $token);
        }else {
            return response([
                'status' => 'error',
                'error' => 'You have entered invalid email or password!',
                'message' => 'Invalid Credentials.'
            ], 400);
        }
    }

    public function checkSignupAdvUser(Request $request)
    {
        $user = User::where("email", $request->email)->first();
        if($user){
            $roleFound = false;
            foreach ($user->roles as $item) {
                if ($item['name'] === "clm") {
                    $roleFound = true;
                    break;
                }
            }
            if (!$roleFound) {
                return response()->json([
                    "customMessage" => "User with Email already exist with the Role Advertiser, Do you want to Create new User with CLM Roles?",
                    "error" => true,
                ]);
            }else if($roleFound){
                return response()->json([
                    "customMessage" => "User already exists",
                    "error" => true,
                ]);
            }
        }
        return $this->registerClm($request);
    }

    public function registerClm(Request $request){

        //return $request->email;
        $validation_rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ];
        $data['email'] = $request->email;
        $data['password'] = bcrypt($request->password);
        $data['activation_token'] = str_random(60);
        $data["email_verified_at"] = Carbon::now()->toDateTimeString();


        $validator = Validator::make($data, $validation_rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->errors()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }else{
            $user = User::create($data);
            $newdata = ['user_id'=>$user->id];
            $UserProfiles = UserProfiles::create($newdata);
            $user->assignRole(['clm']);
           $user->notify(new SignupActivate($user));
        }

        if($user){
            return response([
                'data'=> $user,
                'status' => 'success',
                'token' => $data['activation_token']
            ], 200)
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Authorization', $data['activation_token']);
        } else {
            return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
        }
}

public function registerClmForCart(Request $request)
{
    //return $request->email;
    $validation_rules = [
        'email' => 'required|email|unique:users',
        'password' => 'required|min:8',

        'first_name' => 'required',
        'last_name' => 'required',
        'phone_number' => 'required',
        'user_title' => 'required',
        'business_name' => 'required',
        'user_bio' => 'required'
    ];
    // return $request->all();
    $data['first_name'] = $request->first_name;
    $data['last_name'] = $request->last_name;
    $data['phone_number'] = $request->phone_number;
    $data['user_title'] = $request->user_title;
    $data['business_name'] = $request->business_name;
    $data['user_bio'] = $request->user_bio;
    
    $data['email'] = $request->email;
    $data['password'] = bcrypt($request->password);
    $data['activation_token'] = str_random(60);
    $data["email_verified_at"] = Carbon::now()->toDateTimeString();


    $validator = Validator::make($data, $validation_rules);
    if ($validator->fails()) {
        return response()->json( ["error"=>$validator->errors()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
    }else{
        $user = User::create($data);
        $newdata = $request->only('first_name', 'last_name', 'phone_number');
        $newdata = ['user_id'=>$user->id];
        
        $newdata['first_name'] = $request->first_name;
        $newdata['last_name'] = $request->last_name;
        $newdata['phone_number'] = $request->phone_number;
        $newdata['user_title'] = $request->user_title;
        $newdata['business_name'] = $request->business_name;
        $newdata['user_bio'] = $request->user_bio;
        $UserProfiles = UserProfiles::create($newdata);
        $user->assignRole(['clm']);
    //    $user->notify(new SignupActivate($user));
    }

    if($user){
        return response([
            'data'=> $user,
            'status' => 'success',
            'token' => $data['activation_token']
        ], 200)
            ->header('Access-Control-Expose-Headers', 'Authorization')
            ->header('Authorization', $data['activation_token']);
    } else {
        return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
    }
}

    public function socialLogin(Request $request){

        //return $request->all();
        $validate = 0;
        $data = [
            'device_id' => $request->device_id,
            'device_type' => $request->device_type,
            'device_model' => $request->device_model,
            'device_token' => $request->device_token,
            'device_os' => $request->device_os
        ];
        $UpdateData = $data;
        if(isset($request->email)){
            $data = ['email' => $request->email ];
        }
        if(isset($request->phone)){
            $data = ['phone' => $request->phone ];
        }
        if($request->fb_token){
            $validation_rules = [
                'fb_token' => 'required|min:3'
            ];
            $data['fb_token'] = $request->fb_token;
            $validator = Validator::make($data, $validation_rules);
            if($validator->fails()){
                return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }else{

                $user = User::where(function($q) use($request) {

                    $q->when($request->email, function($q) use ($request) {
                       $q->where("email", $request->email);
                    })->when($request->phone , function($q) use ($request) {
                       $q->orWhere("phone", $request->phone );
                    })->when($request->fb_token, function($q) use ($request) {
                       $q->orWhere("fb_token", $request->fb_token);
                    });
                })->with(['userInterests','profile'])->first();

                if(!$user){
                    $validate = 1;
                    $data['email_verified_at'] = Carbon::now();
                }
            }
        }
        if($request->google_token){

            $validation_rules = [
                'google_token' => 'required|min:3'
            ];
            $data['google_token'] = $request->google_token;
            $data['email_verified_at'] = Carbon::now();
            $validator = Validator::make($data, $validation_rules);
            if($validator->fails()){
                return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }else{
                //$user = User::where('google_token',$request->google_token)->with('profile')->first();
                $user = User::where(function($q) use($request) {
                    $q->when($request->email, function($q) use ($request) {
                       $q->where("email", $request->email);
                    })->when($request->phone , function($q) use ($request) {
                       $q->orWhere("phone", $request->phone );
                    })->when($request->google_token, function($q) use ($request) {
                       $q->orWhere("google_token", $request->google_token);
                    });
                })->with(['userInterests','profile'])->first();

                if(!$user){
                    $validate = 1;
                    $validator = Validator::make($data, $validation_rules);
                }
            }
        }
        if($request->ios_token){

            $validation_rules = [
                'ios_token' => 'required|min:3'
            ];
            $data['ios_token'] = $request->ios_token;
            $data['email_verified_at'] = Carbon::now();
            $validator = Validator::make($data, $validation_rules);
            if($validator->fails()){
                return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            }else{
                //$user = User::where('ios_token',$request->ios_token)->with('profile')->first();
                $user = User::where(function($q) use($request) {
                    $q->when($request->email, function($q) use ($request) {
                       $q->where("email", $request->email);
                    })->when($request->phone , function($q) use ($request) {
                       $q->orWhere("phone", $request->phone );
                    })->when($request->ios_token, function($q) use ($request) {
                       $q->orWhere("ios_token", $request->ios_token);
                    });
                })->with('profile','userInterests')->first();

                if(!$user){
                    $validate = 1;
                    $validator = Validator::make($data, $validation_rules);
                }
            }
        }
        if(isset($user)){

            $token = $user->createToken('TutsForWeb')->accessToken;
            $user->update($UpdateData);
            $user = User::where('id',$user->id)->with(['userInterests','profile'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
            }])->first();

            if($user){
                $this->addUpdateFireStore($user->id);

                if($request->user_interest_values){
                    $user_interest_values = explode(",",$request->user_interest_values);
                    foreach($user_interest_values as $interest){
                        $inte['user_id'] = $user->id;
                        $inte['cat_id'] = $interest;
                        Interests::updateOrCreate(['user_id' => $user->id,'cat_id' => $interest],$inte);
                    }
                }

            }

            if($token!=''){
                return response([
                    'data'=> $user,
                    'status' => 'success',
                    'token' => $token
                ], 200)
                    ->header('Access-Control-Expose-Headers', 'Authorization')
                    ->header('Authorization', $token);
            }

        }else if($validate == 1){
            $user = User::create($data);
            $newdata = ['user_id'=>$user->id];
            $UserProfiles = UserProfiles::create($newdata);
            $user->assignRole([2]);


            $UserPrivacy['user_id'] = $user->id;
            $UserPrivacy['likes'] = 1;
            $UserPrivacy['comments'] = 1;
            $UserPrivacy['followers'] = 1;
            $UserPrivacy['mentions'] = 1;
            UserPrivacy::create($UserPrivacy);
            //return $user->id;
            $token = $user->createToken('TutsForWeb')->accessToken;



            $user->update($UpdateData);
            $user = User::where('id',$user->id)->with(['userInterests','profile'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
            }])->first();

            if($user){
                $this->addUpdateFireStore($user->id);

                if($request->user_interest_values){
                    $user_interest_values = explode(",",$request->user_interest_values);
                    foreach($user_interest_values as $interest){
                        $inte['user_id'] = $user->id;
                        $inte['cat_id'] = $interest;
                        Interests::updateOrCreate(['user_id' => $user->id,'cat_id' => $interest],$inte);
                    }
                }
            }

            if($token!=''){
                return response([
                    'data'=> $user,
                    'status' => 'success',
                    'token' => $token
                ], 200)
                    ->header('Access-Control-Expose-Headers', 'Authorization')
                    ->header('Authorization', $token);
            }
        }else{
            return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
        }

    }
    public function guestUser(Request $request)
    {
        //return $request->all();
        $validate = 0;
        $data = [
            'is_guest' => 1,
            'device_id' => $request->device_id,
            'device_type' => $request->device_type,
            'device_model' => $request->device_model,
            'device_token' => $request->device_token,
            'device_os' => $request->device_os
        ];
        $user = User::where('device_id',$request->device_id)->where('is_guest',1)->with(['userInterests','profile'])->first();
        if($user){
            $validate = 1;
        }
        elseif($request->device_id){
            // $validation_rules = [
            //     'device_id' => 'required|min:3|unique:users',
            // ];
            // $data['google_token'] = $request->google_token;
            // $validator = Validator::make($data, $validation_rules);
            // if($validator->fails()){
            //     return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
            // }

            $user = User::create($data);
            $userdata['username'] = 'guest'.$user->id;
            $user->update($userdata);
            $newdata = ['user_id'=>$user->id];
            $UserProfiles = UserProfiles::create($newdata);
            $user->assignRole([2]);

            $UserPrivacy['user_id'] = $user->id;
            $UserPrivacy['likes'] = 1;
            $UserPrivacy['comments'] = 1;
            $UserPrivacy['followers'] = 1;
            $UserPrivacy['mentions'] = 1;
            UserPrivacy::create($UserPrivacy);

            $user = User::where('id',$user->id)->with(['userInterests', 'profile'])->first();
            $validate = 1;
        }

        if($validate == 1){

            if($request->user_interest_values){
                $user_interest_values = explode(",",$request->user_interest_values);
                foreach($user_interest_values as $interest){
                    $inte['user_id'] = $user->id;
                    $inte['cat_id'] = $interest;
                    Interests::updateOrCreate(['user_id' => $user->id,'cat_id' => $interest],$inte);
                }
            }

            $token = $user->createToken('TutsForWeb')->accessToken;
            if($token!=''){
                return response([
                    'data'=> $user,
                    'status' => 'success',
                    'token' => $token
                ], 200)
                    ->header('Access-Control-Expose-Headers', 'Authorization')
                    ->header('Authorization', $token);
            }
        }else{
            return response()->json( ["error"=>"Something went wrong!","status"=>"error", "message"=>"invalidate" ], 400);
        }
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if($request->fb_login){
            $user = User::where('fb_token',$request->fb_token)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken('baseline')->accessToken;
                $rolePermissions = $user->getAllPermissions();
                $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                    return $item->name;
                });
                $user->role_permissions = $rolePermissionsMapped;
            }
        }
        else if($request->google_login){

            $user = User::where('google_token',$request->google_token)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken('baseline')->accessToken;
                $rolePermissions = $user->getAllPermissions();
                $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                    return $item->name;
                });
                $user->role_permissions = $rolePermissionsMapped;
            }

        }
        else if($request->ios_login){

            $user = User::where('ios_token',$request->ios_token)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken('baseline')->accessToken;
                $rolePermissions = $user->getAllPermissions();
                $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                    return $item->name;
                });
                $user->role_permissions = $rolePermissionsMapped;
            }

        }
        else if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('baseline')->accessToken;
            $user = User::with(['userInterests'])->find(Auth::user()->id);
            $rolePermissions = $user->getAllPermissions();
            $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                return $item->name;
            });
            $user->role_permissions = $rolePermissionsMapped;
        }

        if(isset($token) && $token!=''){
            return response([
                'data'=> $user,
                'status' => 'success',
                'token' => $token
            ], 200)
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Authorization', $token);
        }else {
            return response([
                'status' => 'error',
                'error' => 'You have entered invalid email or password!',
                'message' => 'Invalid Credentials.'
            ], 400);
        }
    }

    public function advlogin(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if($request->has("role") && $request->role !== null)
        {
            $roleToAssign = $request->role;
            $userToAssignRole = User::where("email", $request->email)->first();
            if($userToAssignRole){
                $userToAssignRole->assignRole([$roleToAssign]);
            }
        }
        if (auth()->attempt($credentials)) {
            $user = User::with(['userInterests','profile'])->find(Auth::user()->id);
            $rolePermissions = $user->getAllPermissions();
            $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                return $item->name;
            });
            $user->role_permissions = $rolePermissionsMapped;
            if($user->activation_token == ""){
                $token = auth()->user()->createToken('baseline')->accessToken;

            }else{
                return response([
                    'status' => 'error',
                    'error' => 'Please verify your email address to login, verification email is already sent!',
                    'message' => 'Email not verified.'
                ], 400);
            }
        }

        if(isset($token) && $token!=''){
            //$profile = UserProfiles::where('user_id', $user->id)->first();
            //$user['profile'] = $profile;
            return response([
                'data'=> $user,
                'status' => 'success',
                'token' => $token
            ], 200)
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Authorization', $token);
        }else {
            return response([
                'status' => 'error',
                'error' => 'You have entered invalid email or password!',
                'message' => 'Invalid Credentials.'
            ], 400);
        }
    }

    public function clmlogin(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if($request->has("role") && $request->role !== null)
        {
            $roleToAssign = $request->role;
            $userToAssignRole = User::where("email", $request->email)->first();
            if($userToAssignRole){
                $userToAssignRole->assignRole([$roleToAssign]);
            }
        }

        if (auth()->attempt($credentials)) {
            $user = User::with(['userInterests','profile'])->find(Auth::user()->id);
            $rolePermissions = $user->getAllPermissions();
            $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                return $item->name;
            });
            $user->role_permissions = $rolePermissionsMapped;
            $token = auth()->user()->createToken('baseline')->accessToken;
            // if($user->activation_token == "" || $user->activation_token == null){
            //     $token = auth()->user()->createToken('baseline')->accessToken;

            // }else{
            //     return response([
            //         'status' => 'error',
            //         'error' => 'Please verify your email address to login, verification email is already sent!',
            //         'message' => 'Email not verified.'
            //     ], 400);
            // }
        }

        if(isset($token) && $token!=''){
            //$profile = UserProfiles::where('user_id', $user->id)->first();
            //$user['profile'] = $profile;
            return response([
                'data'=> $user,
                'status' => 'success',
                'token' => $token
            ], 200)
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Authorization', $token);
        }else {
            return response([
                'status' => 'error',
                'error' => 'You have entered invalid email or password!',
                'message' => 'Invalid Credentials.'
            ], 400);
        }
    }
    
    

    public function generateAdvUserToken(Request $request)
    {
        $advUserId = $request->input('adv_user_id');

        // Fetch the adv user
        $advUser = User::with(['userInterests', 'profile', ])
            ->where('id', $advUserId)
            ->whereHas('roles', function ($query) {
                $query->where('name', 'advertiser');
            })
            ->first();

        if (!$advUser) {
            return response()->json(['status' => 'error', 'message' => 'adv user not found'], 404);
        }


        // Generate token for the adv user
        $token = $advUser->createToken('advUserAccessToken')->accessToken;

        // Fetch role permissions
        $rolePermissions = $advUser->getAllPermissions();
        $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
            return $item->name;
        });
        $advUser->role_permissions = $rolePermissionsMapped;

        return response()->json([
            'status' => 'success',
            'data' => $advUser,
            'token' => $token,
            'role_permissions' => $rolePermissions,
        ], 200);
    }

    public function generateClmUserToken(Request $request)
    {
        $clmUserId = $request->input('clm_user_id');

        // Fetch the CLM user
        $clmUser = User::with(['userInterests', 'profile', ])
            ->where('id', $clmUserId)
            ->whereHas('roles', function ($query) {
                $query->where('name', 'clm');
            })
            ->first();

        if (!$clmUser) {
            return response()->json(['status' => 'error', 'message' => 'CLM user not found'], 404);
        }


        // Generate token for the CLM user
        $token = $clmUser->createToken('CLMUserAccessToken')->accessToken;

        // Fetch role permissions
        $rolePermissions = $clmUser->getAllPermissions();
        $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
            return $item->name;
        });
        $clmUser->role_permissions = $rolePermissionsMapped;

        return response()->json([
            'status' => 'success',
            'data' => $clmUser,
            'token' => $token,
            'role_permissions' => $rolePermissions,
        ], 200);
    }
    public function loginAPI(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if($request->fb_login){
            $user = User::where('fb_token',$request->fb_token)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken('baseline')->accessToken;
                $rolePermissions = $user->getAllPermissions();
                $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                    return $item->name;
                });
                //$user->role_permissions = $rolePermissionsMapped;
            }
        }
        else if($request->google_login){
            $user = User::where('google_token',$request->google_token)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken('baseline')->accessToken;
                $rolePermissions = $user->getAllPermissions();
                $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                    return $item->name;
                });
                $user->role_permissions = $rolePermissionsMapped;
            }
        }
        else if($request->ios_login){
            $user = User::where('ios_token',$request->ios_token)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken('baseline')->accessToken;
                $rolePermissions = $user->getAllPermissions();
                $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                    return $item->name;
                });
                $user->role_permissions = $rolePermissionsMapped;
            }
        }

        else if (auth()->attempt($credentials)) {
            $user = User::find(Auth::user()->id);
            $rolePermissions = $user->getAllPermissions();
            $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
                return $item->name;
            });

            if($user->activation_token == ""){
                $token = auth()->user()->createToken('baseline')->accessToken;

            }else{
                return response([
                    'status' => 'error',
                    'error' => 'Please verify your email address to login, verification email is already sent!',
                    'message' => 'Email not verified.'
                ], 400);
            }

            //$user->role_permissions = $rolePermissionsMapped;
        }



        if(isset($token) && $token!=''){
            $user = User::find($user->id);
            $data_array = [
                'device_token' => (string) $request->device_token ? $request->device_token : null,
                'device_id' => (string) $request->device_id ? $request->device_id : null,
                'device_type' => (string) $request->device_type ? $request->device_type : null,
                'device_model' => (string) $request->device_model ? $request->device_model : null,
                'device_os' => (string) $request->device_os ? $request->device_os : null,
            ];
            $user->update($data_array);

            $user = User::where('id',$user->id)->with(['userInterests','profile'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
            }])->first();

            if($user)
                $this->addUpdateFireStore($user->id);


            return response([
                'data'=> $user,
                'status' => 'success',
                'token' => $token
            ], 200)
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Authorization', $token);
        }else {
            return response([
                'status' => 'error',
                'error' => 'You have entered invalid email or password!',
                'message' => 'Invalid Credentials.'
            ], 400);
        }
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request){
        $request->user()->token()->revoke();

        return response()->json(['You have been successfully logged out.'], 200);
    }
    public function details()
    {
        return response()->json(['data' => auth()->user()], 200);
    }


    public function getAlreadyExistingOrders(Request $request){
        $order = ExtendedPermission::where('is_menu',1)
            ->orderBy('order','asc')
            ->select('id', 'order as value', DB::raw("CONCAT(`label`, ', Order no: ', `order`) as label"))
            ->get();
        $maxOrder = $order->max('value');
        $newOrder = $maxOrder + 1;
        $order->push(['value'=> $newOrder, 'label' => 'New Order no: '.$newOrder]);
        return $this->sendSuccessResponse('orders', $order, 'Orders retrieved successfully!');
    }
    public function getAllParentLabels(Request $request){
        $labels = ExtendedPermission::select('id as value', 'label')
        ->where('module_id', Module::where('name', 'Parent Labels')->value('id'))
        ->where('permission_type','=','label')
            // ->where('parent_label', '!=',0)
            ->get()->sortBy('label')->values()->all();
        return $this->sendSuccessResponse('parentLabels', $labels, 'Parent Labels retrieved successfully!');
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return Redirect::to('https://carets.tv/?success=false');
        }
        $user->email_verified_at = Carbon::now();
        $user->activation_token = '';
        $user->is_active = 1;
        $user->save();
        return Redirect::to('https://carets.tv/?success=true');
    }

    public function addUpdateFireStore($id) {
        $userId = 0;
        $user = User::where('id',$id)->with(['userInterests','profile'=>function($q){
            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        }])->first();
        if(!$user){
            return $userId;
        }
        $firestore = Helper::getFireStoreConnection();
        $collectionReference = $firestore->collection('users');

        //Create user on firestore
        if(isset($user->firestore_reference_id) && $user->firestore_reference_id != ''){

            $data = [
                'device_token' => (string) $user->device_token ? $user->device_token : null,
                'device_id' => (string) $user->device_id ? $user->device_id : null,
                'device_type' => (string) $user->device_type ? $user->device_type : null,
                'device_model' => (string) $user->device_model ? $user->device_model : null,
                'device_os' => (string) $user->device_os ? $user->device_os : null,
                'username' => (string) $user->username ? $user->username : null,
                'first_name' => (string) $user->profile->first_name ? $user->profile->first_name : null,
                'last_name' => (string) $user->profile->last_name ? $user->profile->last_name : null,
                'user_photo'=> (string) $user->profile->user_photo ? $user->profile->user_photo : null,
                'firestore_reference_id'=> (string) $user->firestore_reference_id ? $user->firestore_reference_id : null,
            ];

            $documentReference = $collectionReference->document($user->firestore_reference_id);
            if($documentReference) {
                $snapshot = $documentReference->set($data, ['merge' => true]);
            }else{
                return response()->json( ["error"=>"Invalid firestore refrence ID","status"=>"error", "msg"=>"Invalid firestore refrence ID" ], 400);
            }
            $userId = $user->firestore_reference_id;
        }else{
            $data = [
                'device_token' => (string) isset($user->device_token) ? $user->device_token : null,
                'device_id' => (string) isset($user->device_id) ? $user->device_id : null,
                'device_type' => (string) isset($user->device_type) ? $user->device_type : null,
                'device_model' => (string) isset($user->device_model) ? $user->device_model : null,
                'device_os' => (string) isset($user->device_os) ? $user->device_os : null,
                'username' => (string) isset($user->username) ? $user->username : null,
                'first_name' => (string) isset($user->profile->first_name) ? $user->profile->first_name : null,
                'last_name' => (string) isset($user->profile->last_name) ? $user->profile->last_name : null,
                'user_photo'=> (string) isset($user->profile->user_photo) ? $user->profile->user_photo : null,
                'seen' => '0',
                'status' => 'offline',
                'user_id' => $id
            ];
            $snapshot = $collectionReference->add($data);
            $userId = $snapshot->id();
            $user->firestore_reference_id = $userId;
            $user->save();
            $this->addUpdateFireStore($userId);
        }
        return $userId;
    }

}
