<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdPayments;
use App\CLMPayments;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class CLMPaymentController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id)
    {
        //return $request->all();
        $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        // $data = AdPayments::with([
        //     'user'=>function($q){
        //         $q->with(['profile'=>function($q) {
        //             $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
        //         }]);
        //     },'campaign'=>function($q){
        //         $q->with(['campaign']);
        //         }])

        $data = CLMPayments::with([
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            },
          "license"          
        ])
        ->where(function($q) use ($filter){
            $q->whereHas('license', function($q) use ($filter) {
                $q->where('caret_title', 'like', '%' . $filter . '%');
            });
            $q->orWhereHas('user', function($q) use ($filter) {
                $q->where('email', 'like', '%' . $filter . '%');
            });   
           
        })

            ->when($user_id > 0,function($q) use ($user_id){
                $q->where('user_id', $user_id);
            })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data2['title'] = strtolower(trim($data['title']));
        $data2['position'] = trim($data['position']);
        if(isset($data['is_active']))
            $data2['is_active'] = trim($data['is_active']);

        $data2['duration'] = trim($data['duration']);

        if(isset($data['id'])){
            $CLMPayments = CLMPayments::where('id', $data['id'])->first();
            if($CLMPayments) {
                $CLMPayments->update($data2);
            }
        }else{
            $CLMPayments = CLMPayments::create($data2);
            $data['id'] = $CLMPayments->id;
        }
        $CLMPayments = CLMPayments::where('id', $data['id'])->first();

        return response()->json([ 'data' => $CLMPayments, 'message' => 'Record stored successfully!'], 200);
    }

    public function show($id)
    {
        $data = CLMPayments::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function destroy($id)
    {
        $CLMPayments = CLMPayments::where('id', $id)->first();
        $CLMPayments->delete();
        return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
    }

    public function PaymentStatus(Request $request) {
        $Payment_id = $request->Payment_id;
        $AdPayments = CLMPayments::where('id', $Payment_id)->first();
        if($AdPayments){
            if($AdPayments->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $AdPayments->update($data);
            return response()->json([ 'data' => $AdPayments, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }
}
