<?php

namespace App\Http\Controllers;

use App\AdPayments;
use App\AdsIndexing;
use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdCampaigns;
use App\AdCards;
use App\AdPlans;
use App\Ads;
use App\AdStatus;
use App\Ages;
use App\CampaignDetails;
use App\Campaigns;
use App\CampaignsAds;
use App\Countries;
use App\Gender;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Price;

class CampaignController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id)
    {
        $user_id = (int)$user_id > 0?$user_id:null;
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = Campaigns::with([
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            },'campaignAds','details'=>function($q){
                $q->with(['age','genders']);
            }])
            ->where(function($q) use ($filter){
                $q->where('campaign_title','like', '%' . $filter . '%');
            })
            ->when($user_id > 0,function($q) use ($user_id){
                $q->where('user_id', $user_id);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);

            $data->getCollection()->transform(function ($campaign) {
                $activeCount = 0;
                $otherCount = 0;
                $totalAds = 0;
                    foreach ($campaign->campaignAds as $campaignAd) {
                    $activeCount += $campaignAd->status()->where("title", "Active")->count();
                    $totalAds +=1;
                }
                $otherCount = $totalAds - $activeCount;

                $campaign->active_count = $activeCount;
                $campaign->other_count = $otherCount;
                $campaign->totalAds = $totalAds;
                return $campaign;
            });

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data2['campaign_title'] = trim($data['campaign_title']);
        $data2['user_id'] = trim($data['user_id']);

        $details['ages'] = null;
        $details['gender'] = null;
        $details['location'] = null;

        if(isset($data['details']['ages']))
            $details['ages'] = trim($data['details']['ages']);
        if(isset($data['details']['gender']))
            $details['gender'] = trim($data['details']['gender']);
        if(isset($data['details']['location']))
            $details['location'] = trim($data['details']['location']);


        if(isset($data['id'])){
            $AdCampaigns = Campaigns::where('id', $data['id'])->first();
            if($AdCampaigns) {
                $AdCampaigns->update($data2);
            }
        }else{
            $data2['is_active'] = 1;
            $AdCampaigns = Campaigns::create($data2);
            $data['id'] = $AdCampaigns->id;
        }

         //$input = $request->all();

        $details['campaign_id'] = $data['id'];

         if($request->campaign_id){
            $CampaignDetails = CampaignDetails::where('campaign_id', $request->campaign_id)->first();
            $CampaignDetails->update($details);
        }else{
            $CampaignDetails = CampaignDetails::create($details);
        }

        //$AdCampaigns = Campaigns::where('id', $data['id'])->first();
        $AdCampaigns = Campaigns::with(['campaignAds','details'=>function($q){
            $q->with(['age','genders']);
        }])

        ->withCount([
        'campaignAds as activeAds'=>function($q){
            $q->where('status_id',4)->limit(1);
        }])

        ->withCount([
        'campaignAds as inActiveAds'=>function($q){
            $q->whereNotIn('status_id',[4])->limit(1);
        }])
        ->withCount(['campaignAds'=>function($q){
            $q->select(DB::raw('sum(campaigns_ads.watch_count)'));
        }])

        ->where('id', $data['id'])->first();

        return response()->json([ 'data' => $AdCampaigns, 'message' => 'Record stored successfully!'], 200);
    }



        public function storeAds(Request $request)
        {
            $response = ''; // response status

            $data = $request->all();
            $data2['ad_id'] = trim($data['ad_id']);
            $data2['campaign_id'] = trim($data['campaign_id']);
            $data2['plan_id'] = trim($data['plan_id']);
            $data2['status_id'] = trim($data['status_id']);

            if($data2['ad_id'] == "" || $data2['plan_id'] == ""){
                return response()->json( ["error"=>"In-complete Information","status"=>"error", "message"=>"Invalid Data" ], 400);
            }


            if(isset($data['id'])){
                $CampaignsAds = CampaignsAds::where('id', $data['id'])->first();
                if($CampaignsAds) {
                    if($data2['status_id'] != $CampaignsAds->status_id){

                        if($data2['status_id'] == 4){
                            $response = $this->activateSubscription($CampaignsAds->id);
                        }else{
                            $response =  $this->inActivateSubscription($CampaignsAds->id);
                        }
                        if($response == 'success')
                            $this->addRemoveIndexing ($CampaignsAds->id, $data2['status_id']);
                    }
                    if($response == 'success')
                        $CampaignsAds->update($data2);
                }
            }else{
                $CampaignsAds = CampaignsAds::create($data2);
                $data['id'] = $CampaignsAds->id;

                if($data2['status_id'] == 4){
                    $response = $this->activateSubscription($CampaignsAds->id);
                    //if($response == 'success')
                }
            }


            $CampaignsAds = CampaignsAds::where('id', $data['id'])->first();
            if($response == 'error'){
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }else{
                return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            }

        }





        public function updateAdsStatus(Request $request)
        {
            $response = '';
            $data = $request->all();
            $data2['ad'] = trim($data['ad_id']);
            $data2['status_id'] = trim($data['status_id']);


            if(isset($data['ad_id'])){
                $CampaignsAds = CampaignsAds::where('id', $data['ad_id'])->first();
                if($CampaignsAds) {
                    if($data2['status_id'] != $CampaignsAds->status_id){

                        if($data2['status_id'] == 4){
                            $response =   $this->activateSubscription($CampaignsAds->id);
                        }else{
                            if($CampaignsAds->status_id != 1){
                                $response =  $this->inActivateSubscription($CampaignsAds->id);
                            }else{
                                $CampaignsAds->update($data2);
                            }
                        }
                        if($response == 'success')
                            $this->addRemoveIndexing ($CampaignsAds->id, $data2['status_id']);
                    }
                    if($response == 'success')
                        $CampaignsAds->update($data2);
                }


            $CampaignsAds = CampaignsAds::where('id', $data['ad_id'])->first();

            //return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            if($response == 'error'){
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }else{
                return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            }


            }else{
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }
        }


        public function storeMultiAds(Request $request)
        {
            $newCampaign = Campaigns::where("id", $request->campaign_id)->first();
            foreach ($newCampaign->campaignAds as $campaignAd) {
                $campaignAd->delete();
            }

            $data = $request->all();

            foreach($data['adsData'] as $ad){
                if($ad['ad_id'] == "" || $ad['plan_id'] == ""){
                    return response()->json( ["error"=>"In-complete Information","status"=>"error", "message"=>"Invalid Data" ], 400);
                }
            }

            foreach($data['adsData'] as $ad){

                $data2['ad_id'] = trim($ad['ad_id']);
                $data2['campaign_id'] = trim($data['campaign_id']);
                $data2['plan_id'] = trim($ad['plan_id']);
                $data2['status_id'] = trim($data['status_id']);

                $CampaignsAds = CampaignsAds::create($data2);
                $data['id'] = $CampaignsAds->id;
            }

            $AdCards = AdCards::where('user_id',$data['user_id'])->count();
            $data['cards'] = $AdCards;
            //$CampaignsAds = CampaignsAds::where('id', $data['id'])->first();

            return response()->json([ 'data' => $data, 'message' => 'Record stored successfully!'], 200);
        }

        public function updateMultiAds(Request $request)
        {
            $data = $request->all();

            foreach($data['adsData'] as $ad){
                if($ad['ad_id'] == "" || $ad['plan_id'] == ""){
                    return response()->json( ["error"=>"In-complete Information","status"=>"error", "message"=>"Invalid Data" ], 400);
                }
            }

            foreach($data['adsData'] as $ad){

                $data2['ad_id'] = trim($ad['ad_id']);
                $data2['campaign_id'] = trim($data['campaign_id']);
                $data2['plan_id'] = trim($ad['plan_id']);
                $data2['status_id'] = trim($data['status_id']);


                $CampaignsAds = CampaignsAds::create($data2);
                $data['id'] = $CampaignsAds->id;
            }

            $AdCards = AdCards::where('user_id',$data['user_id'])->count();
            $data['cards'] = $AdCards;
            //$CampaignsAds = CampaignsAds::where('id', $data['id'])->first();

            return response()->json([ 'data' => $data, 'message' => 'Record stored successfully!'], 200);
        }

        public function showAds($id)
        {
            $data = CampaignsAds::with(['plan','campaign','status','ad'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url")]);
            }])->where('id', $id)->first();
            return response()->json([ 'data' => $data, 'message' => ''], 200);
        }

        public function indexAds(Request $request, $campaign_id)
        {
            $campaign_id = (int)$campaign_id > 0?$campaign_id:null;
            //return $request->all();
            $filter = $request->input('filter');
            $sort = $request->input('sort');
            $sortName = null;
            $orderType = null;
            $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

            if ($filter == '' && $sort != ''){

                $sortEx     = explode(',', $sort);
                $sortName   = $sortEx[0];
                $orderByEx  = explode(":", $sortEx[1]);
                $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

            }

            $data = CampaignsAds::with(['plan','campaign','status','ad'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);

            },'payments'])
            ->withCount([
                'campaignViews as totalComplete'=>function($q) use ($campaign_id){
                    $q->where('complete',1)->where('campaign_id',$campaign_id);
                }])

            ->withCount([
                'campaignViews as totalClicks'=>function($q) use ($campaign_id){
                    $q->where('clicked',1)->where('campaign_id',$campaign_id);
                }])

            ->withCount([
                'payments as startedOn'=>function($q) {
                    $q->select('start_date')->limit(1)->orderBy('start_date', 'ASC');
                }])
            ->withCount([
                'payments as spent'=>function($q) {
                    $q->select(DB::raw('sum(ad_payments.amount)'));
                }])

                ->when($campaign_id > 0,function($q) use ($campaign_id){
                    $q->where('campaign_id', $campaign_id);
                })
                ->when($filter, function($q) use ($filter) {
                    $q->whereHas('ad', function($query) use ($filter) {
                        $query->where('ad_title', 'like', '%' . $filter . '%');
                    });
                })
                ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                    $query->orderBy($sortName, $orderType);
                })
                ->when($sortName===null,function($q){
                    $q->orderBy('created_at','desc');
                })
                ->paginate($pagination);




            if( $request->is('api/*')){
                return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
            }
            return null;

        }

        public function show($id)
        {
            $data = Campaigns::with([


            'campaignAds',
            'details'=>function($q){
                $q->with(['age','genders']);
            }])

            ->with([
                'genderStats' => function ($q) {
                    $q->select('gender','campaign_id','genders.title', DB::raw('count(*) as total'))
                    ->join('genders', 'genders.id', '=', 'gender')
                    ->orderBy('gender','asc')
                    ->groupBy('gender');
                }
            ])
            ->with([
                'ageStats' => function ($q) {
                    $q->select('age','campaign_id', DB::raw('count(*) as total'),
                    DB::raw('(SELECT CONCAT(ages.min, " - ", ages.max) as t FROM ages WHERE id = age) as title')
                    )
                    ->orderBy('age','asc')
                    ->groupBy('age');
                }
            ])
            ->with([
                'positionStats' => function ($q) {
                    $q->select('position','campaign_id',
                    DB::raw('count(*) as total'))
                    ->orderBy('position','asc')
                    ->groupBy('position');
                }
            ])

            ->withCount([
            'campaignAds as activeAds'=>function($q){
                $q->where('status_id',4)->limit(1);
            }])



            // ->with([
            //     'campaignViews'=>function($q){
            //         $q->select('gender',DB::raw('count(*) as total'))
            //         ->groupBy('gender')
            //         ->get();
            //     }])


            ->withCount([
                'campaignViews as totalComplete'=>function($q){
                    $q->where('complete',1);
                }])


            ->withCount([
                'campaignViews as totalClicks'=>function($q){
                    $q->where('clicked',1);
                }])

            ->withCount([
            'campaignAds as inActiveAds'=>function($q){
                $q->whereNotIn('status_id',[4])->limit(1);
            }])

            ->withCount(['campaignAds'=>function($q){
                $q->select(DB::raw('sum(campaigns_ads.watch_count)'));
            }])



            ->where('id', $id)->first();
            return response()->json([ 'data' => $data, 'message' => ''], 200);
        }

        public function getCampaignsSummary($user_id)
        {
            $rows = Campaigns::with(['campaignAds','details'=>function($q){
                $q->with(['age','genders']);
            }])

            ->withCount([
            'campaignAds as activeAds'=>function($q){
                $q->where('status_id',4)->limit(1);
            }])

            ->withCount([
            'campaignAds as inActiveAds'=>function($q){
                $q->whereNotIn('status_id',[4])->limit(1);
            }])
            ->withCount(['campaignAds'=>function($q){
                $q->select(DB::raw('sum(campaigns_ads.watch_count)'));
            }])
            ->withCount([
                'campaignViews as totalComplete'=>function($q){
                    $q->where('complete',1);
                }])

            ->withCount([
                'campaignViews as totalClicks'=>function($q){
                    $q->where('clicked',1);
                }])


            ->where('user_id', $user_id)->get();

            $totalCampaigns = 0;
            $totalAds = 0;
            $activeAds = 0;
            $inActiveAds = 0;
            $campaign_ads_count = 0;
            $totalClicks = 0;
            $totalComplete = 0;

            foreach($rows as $row){
                $totalCampaigns++;
                $totalAds = $totalAds + $row->inActiveAds + $row->activeAds ;
                $inActiveAds += $row->inActiveAds ;
                $activeAds += $row->activeAds ;
                $campaign_ads_count += $row->campaign_ads_count ;
                $totalClicks += $row->totalClicks ;
                $totalComplete += $row->totalComplete ;
            }
            $data['totalCampaigns'] = $totalCampaigns;
            $data['totalAds'] = $totalAds;
            $data['inActiveAds'] = $inActiveAds;
            $data['activeAds'] = $activeAds;
            $data['totalClicks'] = $totalClicks;
            $data['totalComplete'] = $totalComplete;
            $data['campaign_ads_count'] = $campaign_ads_count;
            return response()->json([ 'data' => $data, 'message' => ''], 200);
        }

        public function destroyAds($id)
        {



            try {

                $CampaignsAds = CampaignsAds::where('id', $id)->first();
                AdsIndexing::where('campaign_ad_id', $id)->delete();
                $CampaignsAds->delete();
                return response()->json(['', 'message' => 'Record deleted successfully!'], 200);

            } catch (\Exception $e) {
                return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
            }


        }

        // public function AdsStatus(Request $request) {
        //     $campaign_id = $request->campaign_id;
        //     $Campaigns = CampaignsAds::where('id', $campaign_id)->first();
        //     if($Campaigns){
        //         if($Campaigns->is_active == 1){
        //             $data['is_active'] = 0;
        //         }else{
        //             $data['is_active'] = 1;
        //         }
        //         $Campaigns->update($data);
        //         return response()->json([ 'data' => $Campaigns, 'message' => 'Record updated successfully!'], 200);
        //     }else{
        //         return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        //     }

        // }

        // public function AdsBlock($id) {
        //     $campaign_id = $id;
        //     $campaign = CampaignsAds::where('id', $campaign_id)->first();
        //     if($campaign){
        //         if($campaign->is_blocked == 1){
        //             $data['is_blocked'] = 0;
        //         }else{
        //             $data['is_blocked'] = 1;
        //         }
        //         $campaign->update($data);
        //         return response()->json([ 'data' => $campaign, 'message' => 'Record updated successfully!'], 200);
        //     }else{
        //         return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        //     }
        // }









        public function destroy($id)
        {
            try {

                $AdCampaigns = CampaignsAds::where('campaign_id', $id)->get();
                foreach($AdCampaigns as $AdCamp){
                    AdsIndexing::where('campaign_ad_id', $AdCamp->id)->delete();
                }
                $AdCampaigns = CampaignsAds::where('campaign_id', $id)->delete();

                $AdCampaigns = CampaignDetails::where('campaign_id', $id)->delete();
                $AdCampaigns = Campaigns::where('id', $id)->first();
                $AdCampaigns->delete();

                return response()->json(['', 'message' => 'Record deleted successfully!'], 200);

            } catch (\Exception $e) {
                return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
            }


        }

        public function campaignStatus(Request $request) {
            $campaign_id = $request->campaign_id;
            $Campaigns = Campaigns::where('id', $campaign_id)->first();
            if($Campaigns){
                if($Campaigns->is_active == 1){
                    $data['is_active'] = 0;
                }else{
                    $data['is_active'] = 1;
                }
                $Campaigns->update($data);
                return response()->json([ 'data' => $Campaigns, 'message' => 'Record updated successfully!'], 200);
            }else{
                return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
            }

        }

        public function campaignBlock($id) {
            $campaign_id = $id;
            $campaign = Campaigns::where('id', $campaign_id)->first();
            if($campaign){
                if($campaign->is_blocked == 1){
                    $data['is_blocked'] = 0;
                }else{
                    $data['is_blocked'] = 1;
                }
                $campaign->update($data);
                return response()->json([ 'data' => $campaign, 'message' => 'Record updated successfully!'], 200);
            }else{
                return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
            }
        }



        public function addRemoveIndexing ($campaign_ad_id, $status_id) {

            if($status_id == 4){
                $ret = AdPayments::where('campaign_ad_id', $campaign_ad_id)
                ->orderBy('current_period_end', 'DESC')
                ->first();

                //campaign_ad_id
                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $campaign_ad_id)->first();

                $Campaigns = Campaigns::with(['details'])->where('id', $CampaignsAds->campaign_id)->first();
                    ##SUBSCRIPTION PART
                $user = User::where('id',  $Campaigns->user_id)->first();
                $srtipe_id = 0;
                $AdPlans = AdPlans::where('id', $CampaignsAds->plan_id)->first();

                $AdsIndexing['campaign_ad_id'] =  $CampaignsAds->id;
                $AdsIndexing['ad_id'] = $CampaignsAds->ad_id;
                $AdsIndexing['video_url'] = $CampaignsAds->ad->video_url;
                $AdsIndexing['image_url'] = $CampaignsAds->ad->image_url;
                $AdsIndexing['age_range'] = $Campaigns->details->ages;
                $AdsIndexing['gender'] = $Campaigns->details->gender;
                $AdsIndexing['location'] = $Campaigns->details->location;
                $AdsIndexing['position'] = $AdPlans->position;
                $AdsIndexing['description'] = $CampaignsAds->ad->ad_description;

                AdsIndexing::updateOrCreate(['campaign_ad_id' => $CampaignsAds->id],$AdsIndexing);
            }else if($status_id != 6){
                AdsIndexing::where('campaign_ad_id', $campaign_ad_id)->delete();
            }

        }

        public function toggleAdsCancel(Request $request)
        {
            $response = '';
            $data = $request->all();



            if(isset($data['ad_id'])){
                $CampaignsAds = CampaignsAds::where('id', $data['ad_id'])->first();
                if($CampaignsAds) {
                    {

                        if($data['cancel_at_period_end'] == 0){
                            $response =   $this->activateSubscription($CampaignsAds->id);
                            $data2['status_id'] = 4;
                        }else{
                            $response =  $this->inActivateSubscription($CampaignsAds->id);
                            $data2['status_id'] = 6;
                        }
                    }

                    if($response == 'success')
                        $CampaignsAds->update($data2);
                }


            $CampaignsAds = CampaignsAds::where('id', $data['ad_id'])->first();

            //return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            if($response == 'error'){
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }else{
                return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            }


            }else{
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }
        }

     public function activateSubscription($campaign_ad_id){


        $ret = AdPayments::where('campaign_ad_id', $campaign_ad_id)
        ->orderBy('current_period_end', 'DESC')
        ->first();

            //campaign_ad_id
            $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
            }])->where('id', $campaign_ad_id)->first();

            $Campaigns = Campaigns::with(['details'])->where('id', $CampaignsAds->campaign_id)->first();
             ##SUBSCRIPTION PART
            $user = User::where('id',  $Campaigns->user_id)->first();
            $srtipe_id = 0;
            $AdPlans = AdPlans::where('id', $CampaignsAds->plan_id)->first();



            Stripe::setApiKey(config("services.stripe.secret"));
            if($user->stripe_id != ""){
                try{
                    if(isset($ret->transaction_id)){
                        $Subscription = Subscription::update(
                            $ret->transaction_id,
                            [
                                'cancel_at_period_end' => false,
                            ]
                        );
                        $payment['transaction_type'] = "Re-Activate";
                        $payment['amount'] = 0;
                    }else{

                        $customer_srtipe_id = $user->stripe_id;
                        $plan_srtipe_id = $AdPlans->stripe_id;

                        // **Step 1: Check if plan_srtipe_id exists on Stripe**
                        if (!empty($plan_srtipe_id)) {
                            try {
                                $stripePlan = Price::retrieve($plan_srtipe_id);
                            } catch (\Stripe\Exception\InvalidRequestException $e) {
                                // Plan does not exist on Stripe, we need to create it
                                $plan_srtipe_id = null;
                            }
                        }

                        if (empty($plan_srtipe_id)) {
                            $newPlan = Price::create([
                                'unit_amount' => $AdPlans->amount * 100, // Amount in cents
                                'currency' => 'usd',
                                'recurring' => ['interval' => $AdPlans->duration], // Adjust as needed
                                'product_data' => ['name' => $AdPlans->title],
                            ]);

                            // Update the database with the new Stripe plan ID
                            $AdPlans->stripe_id = $newPlan->id;
                            $AdPlans->save();
                            $plan_srtipe_id = $newPlan->id;
                        }

                        $Subscription = Subscription::create(array(
                            'customer' =>$customer_srtipe_id,
                            'items' => [
                                ['price' => $plan_srtipe_id],
                            ]
                        ));


                    $payment['transaction_type'] = "Payment";
                    $payment['amount'] = $AdPlans->amount;

                    }
                }catch(\Stripe\Exception\InvalidRequestException $e) {
                    $error = $e->getError()->message . '\n';
                    return "error";
                }
            }else{
                return "error";
            }

            if($Subscription){

                $payment['campaign_ad_id'] = $CampaignsAds->id;
                $payment['user_id'] = $Campaigns->user_id;

                $payment['transaction_id'] = $Subscription['id'];
                $payment['latest_invoice'] = $Subscription['latest_invoice'];
                $payment['customer'] = $Subscription['customer'];
                $payment['cancel_at'] = $Subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $Subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $Subscription['canceled_at'];
                $payment['collection_method'] = $Subscription['collection_method'];
                $payment['created'] = $Subscription['created'];
                $payment['current_period_end'] = $Subscription['current_period_end'];
                $payment['current_period_start'] = $Subscription['current_period_start'];
                $payment['days_until_due'] = $Subscription['days_until_due'];
                $payment['default_payment_method'] = $Subscription['default_payment_method'];
                $payment['plan_id'] = $Subscription['plan']['id'];
                $payment['plan_currency'] = $Subscription['plan']['currency'];
                $payment['plan_interval'] = $Subscription['plan']['interval'];
                $payment['plan_interval_count'] = $Subscription['plan']['interval_count'];
                $payment['plan_product'] = $Subscription['plan']['product'];
                $payment['start_date'] = $Subscription['start_date'];
                $payment['status'] = $Subscription['status'];

                $pay_return = AdPayments::create($payment);

                $CampaignsAds->fill([
                    'subscription_status'=>'active',
                    'cancel_at_period_end' => false,
                    'current_period_end' => $Subscription['current_period_end']
                    ]);
                $CampaignsAds->save();
                return 'success'; //response()->json(['data' => $Subscription, 'success' => 'Udated successfully!'], 200);
            }else{

                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);

            }

     }


     public function inActivateSubscription($campaign_ad_id){


        $ret = AdPayments::where('campaign_ad_id', $campaign_ad_id)
        ->orderBy('current_period_end', 'DESC')
        ->first();

            //campaign_ad_id
            $CampaignsAds = CampaignsAds::where('id', $campaign_ad_id)->first();

            $Campaigns = Campaigns::where('id', $CampaignsAds->campaign_id)->first();
             ##SUBSCRIPTION PART
            $user = User::where('id',  $Campaigns->user_id)->first();
            $srtipe_id = 0;
            $AdPlans = AdPlans::where('id', $CampaignsAds->plan_id)->first();
            Stripe::setApiKey(config("services.stripe.secret"));
            if($user->stripe_id != ""){
                try{
                    if(isset($ret->transaction_id)){
                        $Subscription = Subscription::update(
                            $ret->transaction_id,
                            [
                                'cancel_at_period_end' => true,
                            ]
                        );
                    }
                }catch(\Stripe\Exception\InvalidRequestException $e) {
                    $error = $e->getError()->message . '\n';
                    return "error"; // response()->json( ["error"=>"error","status"=>"error", "msg"=> $error], 400);
                }
            }else{
                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);
            }

            if(isset($Subscription)){
                //print_r($Subscription);
                $payment['campaign_ad_id'] = $CampaignsAds->id;
                $payment['user_id'] = $Campaigns->user_id;
                $payment['amount'] = 0;
                $payment['transaction_type'] = "cancellation_requested";

                $payment['transaction_id'] = $Subscription['id'];
                $payment['latest_invoice'] = $Subscription['latest_invoice'];
                $payment['customer'] = $Subscription['customer'];
                $payment['cancel_at'] = $Subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $Subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $Subscription['canceled_at'];
                $payment['collection_method'] = $Subscription['collection_method'];
                $payment['created'] = $Subscription['created'];
                $payment['current_period_end'] = $Subscription['current_period_end'];
                $payment['current_period_start'] = $Subscription['current_period_start'];
                $payment['days_until_due'] = $Subscription['days_until_due'];
                $payment['default_payment_method'] = $Subscription['default_payment_method'];
                $payment['plan_id'] = $Subscription['plan']['id'];
                $payment['plan_currency'] = $Subscription['plan']['currency'];
                $payment['plan_interval'] = $Subscription['plan']['interval'];
                $payment['plan_interval_count'] = $Subscription['plan']['interval_count'];
                $payment['plan_product'] = $Subscription['plan']['product'];
                $payment['start_date'] = $Subscription['start_date'];
                $payment['status'] = $Subscription['status'];

                $pay_return = AdPayments::create($payment);

                $CampaignsAds->fill([
                    'subscription_status'=>'active',
                    'cancel_at_period_end' => 1,
                    'current_period_end' => $Subscription['current_period_end']
                    ]);
                $CampaignsAds->save();
                return 'success'; //response()->json(['data' => $Subscription, 'success' => 'Udated successfully!'], 200);
            }else{
                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);

            }

     }




     public function listSubscription(Request $request){
        Stripe::setApiKey(config("services.stripe.secret"));
        return $Subscription = Subscription::all(['status'=>'active']);
     }

     public function webhooks(Request $request){
        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        switch ($event->type) {
            case 'invoice.payment_failed':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                ->orderBy('transaction_id', 'DESC')
                ->first();

                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $AdPayments->campaign_ad_id)->first();

                    $CampaignsAds->fill([
                    'cancel_at_period_end' => true
                    ]);
                    $CampaignsAds->save();
            break;
            case 'customer.subscription.deleted':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                ->orderBy('transaction_id', 'DESC')
                ->first();

                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $AdPayments->campaign_ad_id)->first();

                    $CampaignsAds->fill([
                    'cancel_at_period_end' => true
                    ]);
                    $CampaignsAds->save();
            break;
            case 'invoice.payment_succeeded':
                    $data = $event->data->object; // contains a \Stripe\PaymentIntent
                        $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                        ->orderBy('transaction_id', 'DESC')
                        ->first();

                        $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                            DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                        }])->where('id', $AdPayments->campaign_ad_id)->first();


                        $payment['campaign_ad_id'] = $CampaignsAds->id;
                        $payment['user_id'] = $AdPayments->user_id;

                        $payment['transaction_id'] = $data->id;
                        $payment['latest_invoice'] = $data->latest_invoice;
                        $payment['customer'] = $data->customer;
                        $payment['cancel_at'] = $data->cancel_at;
                        $payment['cancel_at_period_end'] = $data->cancel_at_period_end;
                        $payment['canceled_at'] = $data->canceled_at;
                        $payment['collection_method'] = $data->collection_method;
                        $payment['created'] = $data->created;
                        $payment['current_period_end'] = $data->current_period_end;
                        $payment['current_period_start'] = $data->current_period_start;
                        $payment['days_until_due'] = $data->days_until_due;
                        $payment['default_payment_method'] = $data->default_payment_method;
                        $payment['plan_id'] = $data->plan->id;
                        $payment['plan_currency'] = $data->plan->currency;
                        $payment['plan_interval'] = $data->plan->interval;
                        $payment['plan_interval_count'] = $data->plan->interval_count;
                        $payment['plan_product'] = $data->plan->product;
                        $payment['start_date'] = $data->start_date;
                        $payment['status'] = $data->status;

                        $pay_return = AdPayments::create($payment);

                        $CampaignsAds->fill([
                            'subscription_status'=>'active',
                            'cancel_at_period_end' => false,
                            'current_period_end' => $data->current_period_end
                            ]);
                        $CampaignsAds->save();
                break;
            // case 'payment_method.attached':
            //     $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
            //     // Then define and call a method to handle the successful attachment of a PaymentMethod.
            //     // handlePaymentMethodAttached($paymentMethod);
            //     break;
            // ... handle other event types
            default:
                // echo 'Received unknown event type ' . $event->type;
        }

        http_response_code(200);
     }


     public function getUsers(Request $request){
        $filter = $request->input('filter');
        $users = User::orderBy('id','DESC')
        ->select('id as value', 'email as label')
        ->where(function($q) use ($filter){
            $q->where('email','like', '%' . $filter . '%');
        })
        ->whereHas(
            'roles', function($q){
                $q->where('name', 'Advertiser');
            }
        )
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }


    public function getStatus(Request $request){
        $filter = $request->input('filter');
        $AdStatus = AdStatus::orderBy('id','DESC')
        ->select('id as value', 'title as label')
        ->where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $AdStatus, 'Record retrieved successfully!');
    }

    public function getPlans(Request $request){
        $filter = $request->input('filter');
        $users = AdPlans::orderBy('id','DESC')
        ->select('*','id as value', 'title as label')
        ->where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
        ->where('is_active',1)
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }
    public function getAds(Request $request){
       $filter = $request->input('filter');
       $signed_id = Auth::check() ? Auth::user()->id : null;
       $user_id = $request->input('user_id');
       $users = Ads::orderBy('id','DESC')
       ->select('id as value', 'ad_title as label', 'ad_button_link', 'ad_description', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"), DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))
       ->where(function($q) use ($filter){
           $q->where('ad_title','like', '%' . $filter . '%');
       })
    //    ->when($user_id,function($q,$user_id){
    //        $q->where('user_id',$user_id);
    //    })
      ->when($user_id || $signed_id, function($q) use ($user_id, $signed_id) {
        // Apply the user_id filter if provided, else use signed_id (authenticated user's ID)
        if ($user_id) {
            $q->where('user_id', $user_id);
        } elseif ($signed_id) {
            $q->where('user_id', $signed_id);
        }
    })
       ->limit(50)
       ->get();
       return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
   }
   public function getGenders(Request $request){
    $filter = $request->input('filter');
    $users = Gender::orderBy('id','DESC')
    ->select('id as value', 'title as label')
    ->where(function($q) use ($filter){
        $q->where('title','like', '%' . $filter . '%');
    })
    ->limit(50)
    ->get();
    return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
}
        public function getAges(Request $request){
            $filter = $request->input('filter');
            $users = Ages::orderBy('sortorder','ASC')
            ->select(DB::raw('id as value, title AS label'))
            ->limit(50)
            ->get();
            return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
        }

        public function getCountries(Request $request){
            $filter = $request->input('filter');
            $users = Countries::where('status',1)->orderBy('sortOrder','ASC')
            ->select(DB::raw('code3 as value, name AS label'))
            ->get();
            return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
        }

}
