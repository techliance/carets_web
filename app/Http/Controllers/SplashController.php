<?php

namespace App\Http\Controllers;

use App\Ads;
use App\AdsIndexing;
use App\AdViews;
use App\Ages;
use App\CampaignsAds;
use App\Carets;
use App\CLMSplash;
use App\Hashs;
use App\Helpers\Helper;
use App\Jobs\ProcessAdsJob;
use App\Sounds;
use App\User;
use App\UserFollows;
use App\UserPrivacy;
use App\UserProfiles;
use App\VideoReports;
use App\Videos;
use App\VideoViews;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Profiler\Profile;

// use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
// use RuntimeException;

class SplashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id)
    {

        $user_id = (int)$user_id > 0 ? $user_id : null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $search = $request->input('search');
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;


        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CLMSplash::select('*')->with(['license','user'=>function($q) use($signed_id){
            $q->with(['profile'=>function($q) {
                $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
            }]);
        }])
        ->addSelect([DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")])
        ->when($filter,function($q) use ($filter){
                $q->orWhere('splash_title','like', '%' . $filter . '%')
                ->orWhere('license_id','like', '%' . $filter . '%');
            })
        ->when($search, function($q) use ($search){
            if (isset($search['fromDate']) && isset($search['toDate'])) {
                $q->where('created_at', '>=', $search['fromDate']);
                $q->where('created_at', '<=', $search['toDate']);
            } elseif (isset($search['fromDate']) && !isset($search['toDate'])) {
                $q->where('created_at', '>=', $search['fromDate']);
            } elseif (!isset($search['fromDate']) && isset($search['toDate'])) {
                $q->where('created_at', '<=', $search['toDate']);
            }
        })
     
        ->when($user_id > 0,function($q) use ($user_id){
            $q->where('user_id', $user_id)
            ->where('is_active', 1);
        })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }



    public function getSplashes(Request $request, $user_id) {
        $signed_id = (int)$user_id > 0 ? $user_id : null;

        $filter = $request->input('filter');
        $licenseId = $request->input('licenseId');

        $users = CLMSplash::orderBy('id','DESC')
        ->select('id as value', 'splash_title as label',
        DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))
        ->where('is_active', 1)
        ->where(function($q) use ($filter){
            $q->where('splash_title','like', '%' . $filter . '%');
        })
        ->when($signed_id, function ($query, $signed_id) {
            $query->where('user_id', $signed_id);
        })
        ->when($licenseId, function ($q) use ($licenseId) {
            $q->where(function ($query) use ($licenseId) {
                $query->where('license_id', $licenseId)
                      ->orWhereNull('license_id'); // Check for null values explicitly
            });
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }

    public function getSplashesRandom(Request $request, $licenseId) {
        $filter = $request->input('filter');
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        $users = CLMSplash::orderBy('id','DESC')
        ->select('id as value', 'splash_title as label',
        DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))
            ->where('video_url', '!=', null)
            ->where(function($q) use ($filter) {
                $q->where('splash_title', 'like', '%' . $filter . '%');
            })
            ->when($signed_id, function ($query, $signed_id) {
                $query->where('user_id', $signed_id);
            })
            ->when($licenseId, function ($q) use ($licenseId) {
               $q->where('license_id', $licenseId);
            })
            ->limit(12)
            ->get();

        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }


    public function splashSatus(Request $request) {
        $splash_id = $request->splash_id;
        $splash = CLMSplash::where('id', $splash_id)->first();
        if($splash){
            if($splash->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $splash->update($data);
            return response()->json([ 'data' => $splash, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }
    }

    public function setStartDefaultSplash(Request $request) {
        $splash_id = $request->splash_id;
        $splash = CLMSplash::where('id', $splash_id)->first();
        CLMSplash::where("start_default", 1)->update(["start_default" => 0]);
        if($splash){
            if($splash->start_default == 1){
                $data['start_default'] = 0;
            }else{
                $data['start_default'] = 1;
            }
            $splash->update($data);
            return response()->json([ 'data' => $splash, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }
    }

    public function setEndDefaultSplash(Request $request) {
        $splash_id = $request->splash_id;
        $splash = CLMSplash::where('id', $splash_id)->first();
        CLMSplash::where("end_default", 1)->update(["end_default" => 0]);
        if($splash){
            if($splash->end_default == 1){
                $data['end_default'] = 0;
            }else{
                $data['end_default'] = 1;
            }
            $splash->update($data);
            return response()->json([ 'data' => $splash, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }
    }



    public function destroy($id)
    {
        try {

        $splash = CLMSplash::where('id', $id)->first();
        $splash->delete();
        return response()->json(['data'=>'true','status'=>'success', 'message' => 'Deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this ad, Linked with compaign","status"=>"error", "msg"=> 'You can not delete this ad, Linked with compaign'], 400);
        }
    }



    public function store(Request $request)
    {
        $data = $request->all();

        $validation_rules = [
            'user_id' => 'required|int'
        ];
        $validator = Validator::make($data, $validation_rules);
        if($validator->fails()){
            return response()->json( ["error"=>$validator->messages()->first(),"status"=>"error", "message"=>"invalidate" ], 400);
        }

        $input['videoFile'] = $request->input('videoFile');
        $request->hasFile('video_url');

        if($request->hasFile('video_url') == false && !isset($input['videoFile'])){
            return response()->json( ["error"=>"Please select the video file","status"=>"error", "message"=>"invalidate" ], 400);
        }

        if(isset($data['user_id']))
            $data2['user_id'] = trim($data['user_id']);

        if(isset($data['license_id']))
            $data2['license_id'] = trim($data['license_id']);

        if(isset($data['splash_title']))
            $data2['splash_title'] = trim($data['splash_title']);

        if(isset($data['is_active']))
            $data2['is_active'] = trim($data['is_active']);

        if(isset($data['start_default']))
            $data2['start_default'] = trim($data['start_default']);

        if(isset($data['end_default']))
            $data2['end_default'] = trim($data['end_default']);



        if(isset($data['splash_id'])){
            $splash = CLMSplash::where('id', $data['splash_id'])->first();
            if($splash) {
                $splash->update($data2);
            }
        }else{
            $splash = CLMSplash::create($data2);
            $data['splash_id'] = $splash->id;
        }


        //  if ($request->hasFile('image_url')) {
        //     $input_img = $request->file('image_url');

        //     $image = $input_img;
        //     $name = time().'.'.$image->getClientOriginalExtension();
        //     $file_path =  'Videos/' . $data['splash_id'] . '_' . $name;
        //     $image = file_get_contents($image);
        //     $confirmUpload = Helper::putServerImage($file_path, $image, 'public');
        //     $image_url = $file_path;

        //     if($image_url){
        //         $data2['image_url'] = $image_url;
        //         $splash->update($data2);
        //     }

        // }



//        new


        if (isset($input['videoFile'])) {
            $FileData = $input['videoFile'];
            if (isset($FileData['file'])) {
                $file = $FileData['file'];
                $file_name = $FileData['name'];

                $file_name = preg_replace('/[^a-zA-Z0-9-_\.]/', '_', $file_name);

                // Save the uploaded file to a temporary location
                //file_put_contents($originalVideoPath, file_get_contents($file));
                $image = file_get_contents($file);
                $file_path =  'Splash/' . $data['splash_id'].time(). $file_name;
                $confirmUpload = Helper::putServerImage($file_path, $image, 'public');

                 // Define paths
                 $originalVideoPath = storage_path('app/' . $file_path); // Temporary storage for the original video
                 //$scaledVideoPath = storage_path('app/Splash/scaled_'. $data['splash_id'] . '_' . $file_name);
                 //$scaledVideoPath = str_replace('.png','.mp4',$scaledVideoPath);
                 // Temporary storage for the scaled video
                 $scaledVideoPath = Helper::prepareVideo($originalVideoPath);
                 if($scaledVideoPath){
                    $outputGifPath = str_replace('.mp4', '.gif', $scaledVideoPath);
                    $gifWidth = 320;


                    $video = file_get_contents($scaledVideoPath);

                    // Upload to S3
                    $fileInfo = pathinfo($scaledVideoPath);
                    $extension = strtolower($fileInfo['extension']);
                    $file_link = 'Splash' . '/'.$data['splash_id'].'_'.time(). '.mp4';
                    $confirmUpload = Helper::putS3Image($file_link, $video, 'public');
                    Helper::putServerImage($file_link, $video, 'public');
                    if ($confirmUpload) {

                        $image_url = Helper::generateGif($file_link);
                        $original_image_url = storage_path('app/' . $image_url);
                        $image = file_get_contents($original_image_url);
                        Helper::putS3Image($image_url, $image, 'public');
                        $uploadFile['image_url'] = $image_url;
                        $uploadFile['video_url'] = $file_link;
                        $splash->update($uploadFile);
                    }
                 }


            }
        }
        //$this->newhandle($data['splash_id']);
        $return = CLMSplash::select(['*',
        DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
        DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")])
        ->where('id', $data['splash_id'])->first();



        //dispatch(new ProcessAdsJob($data['splash_id']));

        return response()->json([ 'data' => $return, 'message' => 'Record stored successfully!'], 200);
    }




     public function show($id)
    {

        $data = CLMSplash::select([
            '*',
            DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS videoFile"),
            DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image")
        ])
        ->with([
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            }])
        ->where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);

    }

    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


}
