<?php


namespace App\Http\Controllers;


use App\Jobs\RevokeTokensForRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use App\Events\NotificationEvent;
use Illuminate\Support\Facades\Auth;

use App\Role as MyRoll;

class RoleController extends Controller

{


    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    function __construct()
    {
        $this->middleware('permission:role-list', ['only' => ['index']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update', 'show']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->middleware('permission:get-all-permissions', ['only' => ['getAllPermissions']]);
        $this->middleware('permission:get-all-roles', ['only' => ['getAllRoles']]);
        //$this->roleRepository = $roleRepository;
    }


    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {
        $roleRepository = new MyRoll();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $roleRepository->getFilteredAndOrderByAttrAllRoles($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $roleRepository->getFilteredAndOrderByAttrAllRoles($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $roleRepository->getFilteredAndOrderByAttrAllRoles($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $roleRepository->getFilteredAndOrderByAttrAllRoles($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('roles', $data, 'Users retrieved successfully!');
        }
        $roles = $data;
        return view('roles.index',compact('roles'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function getUserRoles(Request $request){
        $roleRepository = new MyRoll();
        $roles = $roleRepository->getRoles('App\\Users');
        return $this->sendSuccessResponse('userRoles', $roles, 'Roles retrieved successfully!');
    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create(Request $request)
    {
        $permission = Permission::get();
        $permissions = $permission->map(function ($item, $key) {
            return [
                'value' => $item->id,
                'label' => $item->name,
            ];
        });
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('permissions', $permissions, 'Permissions retrieved successfully!');
        }
        return view('roles.create',compact('permission'));
    }

    public function getAllPermissions(Request $request){
        $permission = Permission::get();
        $permissions = $permission->map(function ($item, $key) {
            return [
                'value' => $item->id,
                'label' => $item->name,
            ];
        });
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('permissions', $permissions, 'Permissions retrieved successfully!');
        }
        return view('roles.create',compact('permission'));
    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request){
        $rules = [
            'name' => 'required|unique:roles,name',
            // 'permission' => 'required',
        ];
        $roleRepository = new MyRoll();
        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            if( $request->is('api/*')){
                return response()->json( ["error"=>$validator->messages(),"status"=>"error", "message"=>"invalidate" ], 400);
            }else{
                return redirect('roles/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }
        $role = $roleRepository->create(['name' => $request->input('name'), 'guard_name' => 'web']);
        // $role->syncPermissions($request->input('permission'));
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('role', $role, 'Role created successfully!');
        }

        return redirect()->route('roles.index')

            ->with('success','Role created successfully');

    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show(Request $request, $id)

    {
        $roleRepository = new MyRoll();
        $role = $roleRepository->find($id, ['id as value', 'name as label']);
        $rolePermissions = $roleRepository->getPermissionForRole($id);
        $rolePermissionsMapped = $rolePermissions->map(function ($item, $key) {
            return [
                'value' => $item->id,
                'label' => $item->name,
            ];
        });
        $role->role_permissions = $rolePermissionsMapped;
        $role->rolePermissionsById = $rolePermissions->map(function ($item, $key) {
            return $item->id;

        });
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('role', $role, 'Role retrieved successfully!');
        }

        return view('roles.show',compact('role','rolePermissions'));

    }


    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Request $request, $id)

    {
        $role = Role::find($id);
        // $permission = Permission::get();
        // $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
        //     ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
        //     ->all();
        $data['role'] = $role;
        // $data['rolePermissions'] = $rolePermissions;
        // $data['permission'] = $permission;
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', $data, 'Role retrieved successfully!');
        }

        return view('roles.edit',compact('role','permission','rolePermissions'));

    }


    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {
        $roleRepository = new MyRoll();
        $rules = [
            'name' => "required|unique:roles,name,$id",
        ];
        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            if( $request->is('api/*')){
                return response()->json( ["error"=>$validator->messages(),"status"=>"error", "message"=>"invalidate" ], 400);
            }else{
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $role = Role::find($id);
        $role_name = $request->input('name');
        if(isset($role_name)){
            $role->name = $request->input('name');
            $role->save();
        }
        $permissions = $request->input('permission');
        if(isset($permissions)){
            $role->syncPermissions($request->input('permission'));
        }
        // if( $request->is('api/*')){
        //     return $userIdsWithThisRole = $roleRepository->getUserIdsWithThisRole($id);
        //      event(new NotificationEvent(
        //         "Logout, change in permission",
        //         "Change in permission",
        //         "/Admin-login",
        //         "all",
        //         Auth::user()->id,
        //         "logout"
        //     ));
        //     dispatch(new RevokeTokensForRole($userIdsWithThisRole));

        //     return $this->sendSuccessResponse('role', $role, 'Role updated successfully!');
        // }
        return redirect()->route('roles.index')->with('success','Role updated successfully');

    }

    // public function updatePermission(Request $request, $id)
    // {
    //     $rules = [
    //         // 'name' => "required|unique:roles,name,$id",
    //         'permission' => 'required',
    //     ];
    //     $validator = Validator::make($request->except('token'), $rules);
    //     if ($validator->fails()) {
    //         if( $request->is('api/*')){
    //             return response()->json( ["error"=>$validator->messages(),"status"=>"error", "message"=>"invalidate" ], 400);
    //         }else{
    //             return redirect()->back()
    //                 ->withErrors($validator)
    //                 ->withInput();
    //         }
    //     }

    //     $role = $this->roleRepository->find($id);
    //     $permissions = $request->input('permission');
    //     if(isset($permissions)){
    //         $role->syncPermissions($request->input('permission'));
    //     }
    //     if( $request->is('api/*')){
    //         $userIdsWithThisRole = $this->roleRepository->getUserIdsWithThisRole($id);
    //         // dispatch(new RevokeTokensForRole($userIdsWithThisRole));
    //         return $this->sendSuccessResponse('role', $role, 'Permission updated successfully!');
    //     }
    //     return redirect()->route('roles.index')

    //         ->with('success','Role updated successfully');

    // }
    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Request $request, $id)

    {
        $roleRepository = new MyRoll();
        $model_has_roles = $roleRepository->modelHasRole($id);
        if($model_has_roles){
            return response()->json( ["error" => [ "message" => "Unable to delete this role, as users found with this role"],"status"=>"error", "message"=>"invalidate" ], 400);
           return $this->sendErrorResponse('Unable to delete this role, as users found with this role');
        }
        $roleRepository->delete($id);
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('', '', 'Role deleted successfully!');
        }
        return redirect()->route('roles.index')
            ->with('success','Role deleted successfully');

    }

    public function checkBroadCast(Request $request) {
        return true;
        die;
        \Debugbar::disable();
        $user = auth()->user();

        if ($user) {
            $pusher = new \Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
            echo $pusher->socket_auth(request()->input('channel_name'), request()->input('socket_id'));
            return;
        }else {
            header('', true, 403);
            echo "Forbidden";
            return;
        }
        return $this->sendSuccessResponse('passValidation', true, 'validation pass');
    }
}
