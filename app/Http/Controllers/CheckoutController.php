<?php

namespace App\Http\Controllers;

use App\Carts;
use App\CartItems;
use App\CLMOrders;
use App\CLMOrderItems;
use App\CLMOrderPayments;
use App\CLMLicense;
use App\CLMRequest;
use App\CLMPayments;
use App\CLMPlans;
use App\User;

use App\Carets;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Product;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\PaymentIntent;
use Stripe\Price;

class CheckoutController extends Controller
{
    public function checkout(Request $request)
    {
        $order_type = $request->input('order_type');
        $order_id = $request->input('order_id');

        try {
            if ($order_id === null) {

                $cart = $this->getCurrentCart();

                if ($cart->items()->count() === 0) {
                    return response()->json(['message' => 'Cart is empty'], 400);
                }

                // Create the order
                $order = CLMOrders::create([
                    'user_id' => auth()->id(), // Assuming the user is logged in
                    'order_number' => $this->generateOrderNumber(),
                    'total_items' => $cart->total_items,
                    'total_amount' => $cart->total_amount,
                    'order_type' => $order_type, // Adjust as needed
                ]);

                // Copy cart items to order items
                $orderItems = [];
                foreach ($cart->items as $cartItem) {
                    $orderItem = CLMOrderItems::create([
                        'order_id' => $order->id,
                        'caret_title' => $cartItem->caret_title,
                        'interval_count' => $cartItem->interval_count,
                        'subscription_stripe_id' => $cartItem->subscription_stripe_id,
                        'subscription_pricing_id' => $cartItem->subscription_pricing_id,
                        'subscription_amount' => $cartItem->subscription_amount,
                    ]);

                    $orderItems[] = $orderItem;
                }

                if($order_type == 'order'){

                    return $this->processMultipleSubscriptions(auth()->user(), $orderItems);
                }else if($order_type == 'request'){

                    $this->generateOrderRequest(auth()->user(), $orderItems);
                }else{
                    return response()->json(['message' => 'Order type not found'], 400);
                }


                $cart->items()->delete();
                $cart->delete();
                $cart->update(['total_items' => 0, 'total_amount' => 0]);
                return response()->json(['message' => 'Checkout successful', 'order' => $order], 200);

            } else if($order_id) {
                $order = CLMOrders::findOrFail($order_id);
                $order->update([
                    'order_type' => 'order',
                ]);
                $orderItems = CLMOrderItems::where('order_id', $order_id)->get();
                $this->processMultipleSubscriptions(auth()->user(), $orderItems);
                return response()->json(['message' => 'Checkout successful with order', 'orderItems' => $orderItems], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Checkout failed', 'error' => $e->getMessage()], 500);
        }
    }




    public function generateOrderRequest($user, $orderItems)
    {

       $profile = $user->profile()->where('user_id', $user->id)->first();

        if (!$profile) {
            throw new \Exception("Profile data not found for user ID: {$user->id}");
        }

        foreach ($orderItems as $orderItem) {

            $caretId = $this->addCarets($orderItem->caret_title);
            $clmRequest = CLMRequest::updateOrCreate([

                'user_id' => $user->id,
                'order_id' => $orderItem->order_id,
                'order_item_id' => $orderItem->id,
                'caret_title' => $orderItem->caret_title,
                'caret_id' => $caretId,
                'name' => $profile->first_name . ' ' . $profile->last_name,
                'company_name' => $profile->business_name,
                'email' => $user->email,
                'phone' => $profile->phone_number,
                'company_description' => $profile->user_bio,
                'designation' => $profile->user_title,
                'status_id' => 1,
                // 'interval_count' => $orderItem->interval_count,
                'subscription_plan' => $orderItem->subscription_pricing_id,
                'subscription_amount' => $orderItem->subscription_amount
            ]);
        }

        return response()->json(['data' => $clmRequest, 'message' => 'Order request generated successfully'], 200);

    }


    public function orderListing(Request $request, $user_id)
    {
        $user_id = (int)$user_id > 0?$user_id:null;

        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CLMOrders::with(['items'])
            ->orderBy('id', 'DESC')
            ->where(function($q) use ($filter){
                $q->where('order_number','like', '%' . $filter . '%');
            })
            ->when($user_id > 0,function($q) use ($user_id){
                $q->where('user_id', $user_id);
            })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);

            foreach ($data as $order) {
                // Calculate the sum of subscription_amount from the related items
                $totalAmount = $order->items->sum('subscription_amount');

                // Update the total_amount field for the order
                $order->total_amount = $totalAmount;
                $order->save(); // Save the updated total_amount
            }

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }





    public function orderItemListing(Request $request, $order_id)
    {
        $order_id = (int)$order_id > 0?$order_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CLMOrderItems::with(['pricing', 'order'])
            ->orderBy('id', 'DESC')
            ->when($order_id > 0,function($q) use ($order_id){
                $q->where('order_id', $order_id);
            })
            ->where(function($q) use ($filter){
                $q->where('caret_title','like', '%' . $filter . '%');
            })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function deleteOrderItem($itemId)
    {
        DB::beginTransaction();
        try {
            $orderItems = CLMOrderItems::findOrFail($itemId);
            $order = $orderItems->order;

            if (!$order) {
                return response()->json(['message' => 'order not found'], 404);
            }

            $clm_request = CLMRequest::where('order_item_id', $itemId)->first();
            if (!$clm_request) {
                return response()->json(['message' => 'request not found'], 404);
            }

            // Update totals and delete items
            $this->updateCartTotals($order, $orderItems);
            $orderItems->delete();
            $clm_request->delete();

            DB::commit();

            return response()->json(['message' => 'Item removed from order', 'order' => $order->fresh()]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Error removing item from order', 'error' => $e->getMessage()], 500);
        }
    }

    private function updateCartTotals(CLMOrders $order, CLMOrderItems $orderItems = null)
    {
        if ($orderItems) {
            // Subtract specific item's quantity and amount
            $order->decrement('total_items', 1);
            $order->decrement('total_amount', $orderItems->subscription_amount);
        } else {
            // Recalculate totals for the entire order
            $totalItems = $order->items()->count();
            $totalAmount = $order->items()->sum('subscription_amount');
            $cart->update([
                'total_items' => $totalItems,
                'total_amount' => $totalAmount,
            ]);
        }
    }


    /**
     * Retrieve the current cart (user or guest).
     */
    private function getCurrentCart()
    {
        // if (auth()->check()) {
        //     return Carts::firstOrCreate(['user_id' => auth()->id()]);
        // }

        if (auth()->check()) {
            $latestCart = Carts::where('user_id', auth()->id())
                ->latest() // Get the latest cart by created_at
                ->first(); // Fetch only the latest one

            if ($latestCart) {
                // Delete older carts along with their items
                Carts::where('user_id', auth()->id())
                    ->where('id', '!=', $latestCart->id)
                    ->each(function ($cart) {
                        $cart->items()->delete(); // Assuming a relationship method 'items'
                        $cart->delete(); // Delete the older cart
                    });

                return $latestCart;
            }else{
                return Carts::create(['user_id' => auth()->id()]);
            }


        }

        $guestToken = session('guest_token');
        if (!$guestToken) {
            throw new \Exception('Guest token is missing');
        }

        return Carts::firstOrCreate(['guest_token' => $guestToken]);
    }
    public function associateUserWithGuestToken(Request $request)
    {
        $guestToken = $request->guest_token;
        if (!$guestToken) {
            return response()->json(['message' => 'Guest token is required'], 400);
        }
        if (!auth()->check()) {
            return response()->json(['message' => 'User must be authenticated'], 401);
        }
        $userId = auth()->id();
        // Find the cart by guest_token
        $cart = Carts::where('guest_token', $guestToken)->first();

        if (!$cart) {
            return response()->json(['message' => 'Cart not found'], 404);
        }


        // Update the user_id for the cart
        $cart->user_id = $userId;
        $cart->save();

        return response()->json(['message' => 'Cart successfully associated with user', 'cart' => $cart], 200);
    }


    /**
     * Generate a unique order number.
     */
    private function generateOrderNumber()
    {
        return strtoupper(uniqid('ORDER-'));
    }
    public function processMultipleSubscriptions($user, $orderItems)
    {


        Stripe::setApiKey(config("services.stripe.secret"));

        if (empty($user->stripe_id)) {
            return response()->json(['success' => false, 'error' => 'User does not have a Stripe customer ID'], 400);
        }

        $customerStripeId = $user->stripe_id;

        try {

            // Check if the customer has a default payment method set
            $customer = Customer::retrieve($customerStripeId);

            if (empty($customer->invoice_settings->default_payment_method)) {
                return response()->json(['success' => false, 'error' => 'No default payment method attached to the customer.'], 400);
            }

            // Use the default payment method
            $paymentMethodId = $customer->invoice_settings->default_payment_method;

            // Calculate the total amount for all subscriptions
            $totalAmount = 0;
            $order_id = 0;
            $subscriptionItems = []; // Store subscription items to add to the invoice
            foreach ($orderItems as $orderItem) {
                $totalAmount += $orderItem->subscription_amount * 100; // Convert to cents
                $order_id = $orderItem->order_id;
                // Add the subscription items to the list

                $subscriptionItems[] = [
                    'price' => $orderItem->subscription_stripe_id, // Use existing price ID
                    'quantity' => 1, // Assuming 1 subscription per order item
                ];
            }

            if ($totalAmount <= 0) {
                return response()->json(['success' => false, 'error' => 'Total amount must be greater than zero'], 400);
            }

            // Create a one-time payment for the total amount
            $paymentIntent = PaymentIntent::create([
                'amount' => $totalAmount,
                'currency' => 'usd',
                'customer' => $customerStripeId,
                'payment_method' => $paymentMethodId, // Use the default payment method
                'off_session' => true, // Off-session payment (customer not present)
                'confirm' => true, // Confirm the payment immediately
                'description' => 'Payment for multiple subscriptions',
                'metadata' => [
                    'user_id' => $user->id,
                    'order_count' => count($orderItems),
                ],
            ]);

            // Check if payment was successful
            if ($paymentIntent->status !== 'succeeded') {
                return response()->json(['success' => false, 'error' => 'Payment failed'], 400);
            }

            // Create subscriptions for each order item
            $subscriptions = [];
            foreach ($orderItems as $orderItem) {

                // Retrieve the Stripe Price ID based on the order item
                $priceId = $orderItem->subscription_stripe_id;
                if (!$this->isValidStripePriceId($priceId)) {
                    $priceId = $this->createStripePrice($orderItem->subscription_amount);
                    $orderItem->update(['subscription_stripe_id' => $priceId]);
                }

                if (!$priceId) {
                    return response()->json(['success' => false, 'error' => 'Stripe Price ID not found for order item'], 400);
                }

                // Create the subscription
                $subscription = Subscription::create([
                    'customer' => $customerStripeId,
                    'items' => [
                        ['price' => $priceId],
                    ],
                    'metadata' => [
                        'order_item_id' => $orderItem->id,
                    ],
                ]);

                $subscriptions[] = $subscription;

                // Update order item with subscription details
                $orderItem->update([
                    'subscription_status' => 'active',
                    'subscription_id' => $subscription->id,
                    'cancel_at_period_end' => $subscription->cancel_at_period_end,
                    'current_period_end' => $subscription->current_period_end,
                ]);
                $caretId = $this->addCarets($orderItem->caret_title);
                $CLMLicense = CLMLicense::updateOrCreate(

                    [
                        "user_id" => $user->id,
                        "caret_title" => $orderItem->caret_title,
                        "caret_id" =>  $caretId,
                        "subscription_amount" => $orderItem->subscription_amount,
                        "subscription_plan" => $orderItem->subscription_pricing_id,
                        "subscription_stripe_id" => $orderItem->subscription_stripe_id,
                        "subscription_status" => 'active',
                        "status_id" => 4,

                        // 'current_period_end' => $subscriptions['current_period_end'],
                        "cancel_at_period_end" => $subscription['subscription']['cancel_at_period_end'] ?? false,
                        "current_period_end" => $subscription['current_period_end'], // Individual period end
                    ]
                );

                $payment['transaction_type'] = "Payment";
                $payment['amount'] = $orderItem->subscription_amount;

                $payment['license_id'] = $CLMLicense->id;
                $payment['user_id'] = $CLMLicense->user_id;

                $payment['transaction_id'] = $subscription['id'];
                $payment['latest_invoice'] = $subscription['latest_invoice'];
                $payment['customer'] = $subscription['customer'];
                $payment['cancel_at'] = $subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $subscription['canceled_at'];
                $payment['collection_method'] = $subscription['collection_method'];
                $payment['created'] = $subscription['created'];
                $payment['current_period_end'] = $subscription['current_period_end'];
                $payment['current_period_start'] = $subscription['current_period_start'];
                $payment['days_until_due'] = $subscription['days_until_due'];
                $payment['default_payment_method'] = $subscription['default_payment_method'];
                $payment['plan_id'] = $subscription['plan']['id'];
                $payment['plan_currency'] = $subscription['plan']['currency'];
                $payment['plan_interval'] = $subscription['plan']['interval'];
                $payment['plan_interval_count'] = $subscription['plan']['interval_count'];
                $payment['plan_product'] = $subscription['plan']['product'];
                $payment['start_date'] = $subscription['start_date'];
                $payment['status'] = $subscription['status'];
                CLMPayments::create($payment);



            }

            // Create the invoice item for the total amount
            InvoiceItem::create([
                'customer' => $customerStripeId,
                'amount' => $totalAmount,
                'currency' => 'usd',
                'description' => 'Total payment for multiple subscriptions',
            ]);

            // Create the invoice with all subscriptions
            $invoice = Invoice::create([
                'customer' => $customerStripeId,
                'collection_method' => 'send_invoice', // Set to send_invoice to manually send the invoice
                'auto_advance' => true, // Automatically finalize the invoice
                'days_until_due' => 30, // Optional: Set a due date, e.g., 30 days
            ]);

            // Finalize and send the invoice
            $invoice->finalizeInvoice();
            $invoice->sendInvoice();

            CLMOrderPayments::create([
                'transaction_id' => $paymentIntent->id,
                'amount' => $totalAmount / 100, // Convert back to dollars
                'order_id' => $order_id,
                'status' => 'completed',
                'payment_method' => 'stripe',
            ]);

            return response()->json(['success' => true, 'subscriptions' => $subscriptions, 'invoice' => $invoice], 200);

        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $error = $e->getError()->message;
            return response()->json(['success' => false, 'error' => $error], 400);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    private function isValidStripePriceId($priceId)
    {
        try {
            if (!$priceId) {
                return false;
            }
            $price = Price::retrieve($priceId);
            return !empty($price) && $price->active;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Create a new Stripe Price ID for the given amount
     */
    private function createStripePrice($amount)
    {
        $price = Price::create([
            'unit_amount' => $amount * 100,
            'currency' => 'usd',
            'recurring' => ['interval' => 'month'],
            'product_data' => ['name' => 'Subscription Plan'],
        ]);
        return $price->id;
    }

    public function addCarets($data){
        $arr['title'] = trim(str_replace('^','', $data));
        $carets = Carets::where('title', $arr['title'])->first();

        if ($carets) {
            $arr['updated_at'] = Carbon::now();
            $carets->update($arr);
        } else {
            $carets = Carets::create($arr);
        }

        return $carets->id;
    }

    public function checkLicenseAvailability(Request $request)
    {
        $keyword = $request->input('keyword');
        $keyword = str_replace(['^', '#'], '', $keyword);

        // Check if the keyword is already purchased
        $keywordStatus = CLMLicense::where('caret_title', $keyword)->exists();

        if ($keywordStatus) {
            return response()->json(['data' => 'NO', 'message' => 'The keyword is already purchased.']);
        }

        return response()->json(['data' => 'YES', 'message' => 'The keyword is available.']);
    }




}


