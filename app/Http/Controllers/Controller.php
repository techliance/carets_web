<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function sendResponse($data = [], $messages = [], $status_code = 200){
        return response()->json( [
            "result" => $data,
            "status" => ['code' => (int) $status_code, 'messages' => $messages]
        ], 200);
    }
    protected function  sendErrorResponse($message, $request_type = 'web'){
        if($request_type == 'web'){
            return response()->json( [
                "message" => $message,
                "status" => 'error'
            ], 400);
        }
        return response()->json( [
            "message" => $message,
            "status" => 400],
            200);

    }
    protected function  sendSuccessResponse($key, $data, $message, $request_type = 'web'){
        if($request_type == 'web'){
            return response()->json( [
                $key => $data,
                "message" => $message,
                "status" => 'success'
            ], 200);
        }
        return response()->json( [
            $key => $data,
            "message" => $message,
            "status" => 200],
            200);

    }
}
