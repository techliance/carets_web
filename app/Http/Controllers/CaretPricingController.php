<?php

namespace App\Http\Controllers;

use App\AdPayments;
use App\AdsIndexing;
use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdCampaigns;
use App\AdCards;
use App\AdPlans;
use App\Ads;
use App\AdStatus;
use App\Ages;
use App\CampaignDetails;
use App\Campaigns;
use App\CampaignsAds;
use App\Carets;
use App\CLMLicense;
use App\CLMPayments;
use App\CLMPlans;
use App\CLMRequest;
use App\CLMKeyword;
use App\Countries;
use App\CaretPricing;
use App\Gender;
use App\Hashs;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PharIo\Manifest\License;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Price;

class CaretPricingController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $search = "";

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if($request->input('search')){
            $search = $request->input('search');

        }

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CaretPricing::with(['plan'])
                ->where(function($q) use ($filter){
                    $q->where('id','like', '%' . $filter . '%');
                    $q->orWhere('title','like', '%' . $filter . '%');
                })
            ->when($search, function($q) use ($search){
                if ($search['fromDate'] && $search['toDate']) {
                    $q->where('created_at', '>=', $search['fromDate']);
                    $q->where('created_at', '<=', $search['toDate']);
                } elseif ($search['fromDate'] && !$search['toDate']) {
                    $q->where('created_at', '>=', $search['fromDate']);
                } elseif (!$search['fromDate'] && $search['toDate']) {
                    $q->where('created_at', '<=', $search['toDate']);
                }
            })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);



        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }



    public function store(Request $request)
    {
        $data = $request->all();

        $data2['title'] = trim($data['title']);

        if(isset($data['is_active']))
            $data2['is_active'] = trim($data['is_active']);
        if(isset($data['networth']))
            $data2['networth'] = trim($data['networth']);
        if(isset($data['instagram_followers']))
            $data2['instagram_followers'] = trim($data['instagram_followers']);
        if(isset($data['twitter_followers']))
            $data2['twitter_followers'] = trim($data['twitter_followers']);
        if(isset($data['google_searches']))
            $data2['google_searches'] = trim($data['google_searches']);
        if(isset($data['one_year_license']))
            $data2['one_year_license'] = trim($data['one_year_license']);
        if(isset($data['two_year_license']))
            $data2['two_year_license'] = trim($data['two_year_license']);
        if(isset($data['three_year_license']))
            $data2['three_year_license'] = trim($data['three_year_license']);
        if(isset($data['special_instructions']))
            $data2['special_instructions'] = trim($data['special_instructions']);
        if(isset($data['sales_price']))
            $data2['sales_price'] = trim($data['sales_price']);
        if(isset($data['plan_id']))
            $data2['plan_id'] = trim($data['plan_id']);

        if(isset($data['contact_sales']))
            $data2['contact_sales'] = trim($data['contact_sales']);
        if(isset($data['contact_amount']))
            $data2['contact_amount'] = trim($data['contact_amount']);

        Stripe::setApiKey(config("services.stripe.secret"));

        if(isset($data['id'])){
            $CaretPricing = CaretPricing::where('id', $data['id'])->first();
            if ($CaretPricing) {
                // Check if the CaretPricing record already has Stripe IDs
                if (empty($CaretPricing->stripe_id_one_year) || empty($CaretPricing->stripe_id_two_year) || empty($CaretPricing->stripe_id_three_year)) {
                    // Create or update Stripe prices for each license if not already set
                    $oneYearPrice = Price::create([
                        'unit_amount' => $data2['one_year_license'] * 100,
                        'recurring' => [
                            'interval' => 'year',
                            'interval_count' => 1  // 1 year
                        ],
                        'currency' => 'usd',
                        'product' => 'prod_RqquyjIKF0Xh4B'
                    ]);

                    $twoYearPrice = Price::create([
                        'unit_amount' => $data2['two_year_license'] * 100,
                        'recurring' => [
                            'interval' => 'year',
                            'interval_count' => 2  // 2 years
                        ],
                        'currency' => 'usd',
                        'product' => 'prod_RqquyjIKF0Xh4B'
                    ]);

                    $threeYearPrice = Price::create([
                        'unit_amount' => $data2['three_year_license'] * 100,
                        'recurring' => [
                            'interval' => 'year',
                            'interval_count' => 3  // 3 years
                        ],
                        'currency' => 'usd',
                        'product' => 'prod_RqquyjIKF0Xh4B'
                    ]);

                    // Update the CaretPricing model with Stripe IDs
                    $data2['stripe_id_one_year'] = $oneYearPrice->id;
                    $data2['stripe_id_two_year'] = $twoYearPrice->id;
                    $data2['stripe_id_three_year'] = $threeYearPrice->id;
                }

                $newPriceOneYear = $data['new_stripe_id_one_year'] ?? null; // New price for 1-year license
                $newPriceTwoYear = $data['new_stripe_id_two_year'] ?? null; // New price for 2-year license
                $newPriceThreeYear = $data['new_stripe_id_three_year'] ?? null; // New price for 3-year license

                //$subscription = Subscription::retrieve($CaretPricing->stripe_subscription_id);
                $updateItems  = [];

                if (!empty($CaretPricing->stripe_subscription_item_id_one_year)) {
                    $updateItems[] = [
                        'id' => $CaretPricing->stripe_subscription_item_id_one_year,
                        'price' => $newPriceOneYear,
                    ];
                }
                if (!empty($CaretPricing->stripe_subscription_item_id_two_year)) {
                    $updateItems[] = [
                        'id' => $CaretPricing->stripe_subscription_item_id_two_year,
                        'price' => $newPriceTwoYear,
                    ];
                }
                if (!empty($CaretPricing->stripe_subscription_item_id_three_year)) {
                    $updateItems[] = [
                        'id' => $CaretPricing->stripe_subscription_item_id_three_year,
                        'price' => $newPriceThreeYear,
                    ];
                }

               // Update the subscription if there are any items to update
            if (!empty($updateItems)) {
                Subscription::update($CaretPricing->stripe_subscription_id, [
                    'items' => $updateItems,
                ]);
            }

                $CaretPricing->update($data2);
            }
        } else{
            // Create new Stripe prices for one, two, and three years
            $oneYearPrice = Price::create([
                'unit_amount' => $data2['one_year_license'] * 100,
                'recurring' => [
                    'interval' => 'year',
                    'interval_count' => 1  // 1 year
                ],
                'currency' => 'usd',
                'product' => 'prod_RqquyjIKF0Xh4B'
            ]);

            $twoYearPrice = Price::create([
                'unit_amount' => $data2['two_year_license'] * 100,
                'recurring' => [
                    'interval' => 'year',
                    'interval_count' => 2  // 2 years
                ],
                'currency' => 'usd',
                'product' => 'prod_RqquyjIKF0Xh4B'
            ]);

            $threeYearPrice = Price::create([
                'unit_amount' => $data2['three_year_license'] * 100,
                'recurring' => [
                    'interval' => 'year',
                    'interval_count' => 3  // 3 years
                ],
                'currency' => 'usd',
                'product' => 'prod_RqquyjIKF0Xh4B'
            ]);

            // Update the data with Stripe IDs
            $data2['stripe_id_one_year'] = $oneYearPrice->id;
            $data2['stripe_id_two_year'] = $twoYearPrice->id;
            $data2['stripe_id_three_year'] = $threeYearPrice->id;
            $data2['stripe_subscription_id'] = $oneYearPrice->subscription ?? null;

            $CaretPricing = CaretPricing::create($data2);
            $data['id'] = $CaretPricing->id;
        }

        return response()->json([ 'data' => $CaretPricing, 'message' => 'Record stored successfully!'], 200);
    }


    public function show($id){
        $data = CaretPricing::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => 'Record retrieved successfully!'], 200);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $CaretPricing = CaretPricing::where('id', $id)->first();
        if($CaretPricing) {
            $CaretPricing->update($data);
            return response()->json([ 'data' => $CaretPricing, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }
    }

    public function caretPricingStatus(Request $request) {
        $caretPricing_id = $request->caretPricing_id;
        $CaretPricing = CaretPricing::where('id', $caretPricing_id)->first();
        if($CaretPricing){
            if($CaretPricing->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $CaretPricing->update($data);
            return response()->json([ 'data' => $CaretPricing, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }
    public function contactSalesStatus(Request $request) {
        $caretPricing_id = $request->caretPricing_id;
        $CaretPricing = CaretPricing::where('id', $caretPricing_id)->first();
        if($CaretPricing){
            if($CaretPricing->contact_sales == 1){
                $data['contact_sales'] = 0;
            }else{
                $data['contact_sales'] = 1;
            }
            $CaretPricing->update($data);
            return response()->json([ 'data' => $CaretPricing, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }


    public function destroy($id)
    {
        try {
        $CaretPricing = CaretPricing::where('id', $id)->first();
        $CaretPricing->delete();
        return response()->json(['data'=>'true','status'=>'success', 'message' => 'Deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this Record","status"=>"error", "msg"=> 'You can not delete this ad, Linked with compaign'], 400);
        }
    }


    // =================================

    public function clmPricingAlgo(Request $request)
    {
        $data = $request->all();
        // $keyword = $data['keyword'] ?? null;
        $classification = $data['classification'] ?? null;
        $networth = $data['networth'] ?? null;
        $instagram = $data['instagram'] ?? null;
        $twitter = $data['twitter'] ?? null;
        $googleSearches = $data['GoogleSearchVolume'] ?? null;

        $networthRange = explode('-', $networth);
        $instagramRange = explode('-', $instagram);
        $twitterRange = explode('-', $twitter);
        $googleSearchesRange = explode('-', $googleSearches);

        // Set default min values and check for max values
        $networthMin = isset($networthRange[0]) ? $this->convertToNumeric($networthRange[0]) : 0;
        $networthMax = isset($networthRange[1]) ? $this->convertToNumeric($networthRange[1]) : null;

        $instagramMin = isset($instagramRange[0]) ? $this->convertToNumeric($instagramRange[0]) : 0;
        $instagramMax = isset($instagramRange[1]) ? $this->convertToNumeric($instagramRange[1]) : null;

        $twitterMin = isset($twitterRange[0]) ? $this->convertToNumeric($twitterRange[0]) : 0;
        $twitterMax = isset($twitterRange[1]) ? $this->convertToNumeric($twitterRange[1]) : null;

        $googleSearchesMin = isset($googleSearchesRange[0]) ? $this->convertToNumeric($googleSearchesRange[0]) : 0;
        $googleSearchesMax = isset($googleSearchesRange[1]) ? $this->convertToNumeric($googleSearchesRange[1]) : null;

        // Perform the query using the min values and checking for null in the max values
        $pricing = CaretPricing::where('title', $classification)
            // ->where('networth', '>=', $networthMin)
            // ->when($networthMax, function($query, $networthMax) {
            //     return $query->where('networth', '<=', $networthMax);
            // })
            ->where('instagram_followers', '>=', $instagramMin)
            ->when($instagramMax, function($query, $instagramMax) {
                return $query->where('instagram_followers', '<=', $instagramMax);
            })
            ->where('twitter_followers', '>=', $twitterMin)
            ->when($twitterMax, function($query, $twitterMax) {
                return $query->where('twitter_followers', '<=', $twitterMax);
            })
            ->where('google_searches', '>=', $googleSearchesMin)
            ->when($googleSearchesMax, function($query, $googleSearchesMax) {
                return $query->where('google_searches', '<=', $googleSearchesMax);
            })
            ->orderBy('one_year_license', 'desc')
            ->first();

        // If pricing found, return it; otherwise, return error
        if ($pricing) {
            return response()->json($pricing, 200);
        }

        return response()->json(['error' => 'No matching tier found'], 404);
    }


    public function convertToNumeric($value)
    {
        $value = strtoupper(str_replace(['$', '<', '>', ' searches per month'], '', trim($value)));
            if (strpos($value, '-') !== false) {
            $values = explode('-', $value);

            if (count($values) === 2) {
                $minValue = $this->conversion(trim($values[0]));
                $maxValue = $this->conversion(trim($values[1]));

                return "{$minValue}-{$maxValue}";
            }
        }

        // Convert a single value to numeric
        return $this->conversion($value);
    }
    public function conversion($value)
    {

        if (strpos($value, 'B') !== false) {
            $number = floatval(str_replace('B', '', $value));
            return $number * 1000000000;
        }

        if (strpos($value, 'M') !== false) {
            $number = floatval(str_replace('M', '', $value));
            return $number * 1000000;
        }

        if (strpos($value, 'K') !== false) {
            $number = floatval(str_replace('K', '', $value));
            return $number * 1000;
        }

        return floatval($value);
    }

}
