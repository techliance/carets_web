<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function validateEmail($request)
    {
        $request->validate(['email' => 'required']);
    }

    /**
     * Get the needed authentication credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $user = User::where('email',$request->email)->first();
        // $request['email'] = $user->email;.
        if($user){
            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );
        }else{
            return response([
                'status' => 'error',
                'error' => 'You have entered invalid email',
                'message' => 'Invalid Credentials.'
            ], 400);
        }

        $response == Password::RESET_LINK_SENT
            ? response()->json(trans($response))
            : response()->json(trans($response));

        return response()->json([ 'data' => $response,'status'=>'success', 'message' => 'Email sent to user successfully'], 200);
    }
}
