<?php

namespace App\Http\Controllers;

use App\Carts;
use App\CartItems;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CartController extends Controller
{
    public function listCart(Request $request)
    {
        $cart = $this->getCurrentCart($request);
        // Include cart items and their related subscription plan
        $cartWithItems = $cart->load(['items.pricing']);

        return response()->json([
            'cart' => $cartWithItems,
            'message' => 'Cart retrieved successfully'
        ]);
    }

    private function updateCartTotals(Carts $cart, CartItems $cartItem = null)
    {
        if ($cartItem) {
            // Subtract specific item's quantity and amount
            $cart->decrement('total_items', 1);
            $cart->decrement('total_amount', $cartItem->subscription_amount);
        } else {
            // Recalculate totals for the entire cart
            $totalItems = $cart->items()->count();
            $totalAmount = $cart->items()->sum('subscription_amount');
            $cart->update([
                'total_items' => $totalItems,
                'total_amount' => $totalAmount,
            ]);

            if($totalItems < 1){
                Carts::where('id', $cart->id)->delete();
            }
        }
    }

    /**
     * Add an item to the cart.
     */
    public function addToCart(Request $request)
    {
        $validated = $request->validate([
            'caret_title' => 'required|string|max:255',
            'subscription_pricing_id' => 'required|int|max:255',
            'subscription_stripe_id' => 'required|string|max:255',
            'subscription_amount' => 'required|numeric|min:0',
        ]);
        $cart = $this->getCurrentCart($request);

        $intervalCount = $this->getIntervalCount($validated['subscription_stripe_id']);

        // $existingItem =  $cart->items()->$validated['caret_title']->exist();
        $existingItem = $cart->items()->where('caret_title', $validated['caret_title'])->first();

        if ($existingItem) {
            return response()->json(['message' => 'Item already exists in cart'], 400);
        }


        $cart->items()->create([
            'caret_title' => $validated['caret_title'],
            'subscription_pricing_id' => $validated['subscription_pricing_id'],
            'interval_count' => 1,
            'subscription_stripe_id' => $validated['subscription_stripe_id'],
            'subscription_amount' => $validated['subscription_amount'],
        ]);
        // Update totals
        $this->updateCartTotals($cart);
        return response()->json(['message' => 'Item added to cart', 'cart' => $cart->fresh()]);
    }

    Public function checkLicenseInCart(Request $request){

    }

    public function getIntervalCount($subscriptionStripeId)
    {
        switch ($subscriptionStripeId) {
            case 'stripe_id_one_year':
                return 1; // Interval for one year
            case 'stripe_id_two_year':
                return 2; // Interval for two years
            case 'stripe_id_three_year':
                return 3; // Interval for three years
            default:
                return 0; // Default interval
        }
    }
    /**
     * Edit the subscription plan of an item in the cart.
     */
    public function editSubscriptionPlan(Request $request, $itemId)
    {
        $validated = $request->validate([
            'subscription_pricing_id' => 'required|int|max:255',
            'subscription_stripe_id' => 'required|string|max:255',
            'subscription_amount' => 'required|numeric|min:0',
            'interval_count'=> 'required'
        ]);

        $cartItem = CartItems::findOrFail($itemId);
        $cartItem->update($validated);
        // Update totals
        $this->updateCartTotals($cartItem->cart);
        return response()->json(['message' => 'Subscription plan updated', 'cart' => $cartItem->cart->fresh()]);
    }

    /**
     * Delete an item from the cart.
     */
    public function deleteItem($itemId)
    {
        $cartItem = CartItems::findOrFail($itemId);
        $cart = $cartItem->cart;
        if (!$cart) {
            return response()->json(['message' => 'Cart not found'], 404);
        }
        // Update totals
        $this->updateCartTotals($cart, $cartItem);
        $cartItem->delete();

        $totalItems = $cart->items()->count();

        if($totalItems < 1){
            Carts::where('id', $cart->id)->delete();
        }

        return response()->json(['message' => 'Item removed from cart', 'cart' => $cart->fresh()]);
    }

    /**
     * Retrieve the current cart (user or guest).
     */
    private function getCurrentCart(Request $request)
    {
        if (auth()->check()) {
            // If the user is authenticated, return the user's cart
            return Carts::firstOrCreate(['user_id' => auth()->id()]);
        }

        // Check if the guest_token is provided in the request
        $guestToken = $request->input('guest_token');

        if (!$guestToken) {
            // If no guest_token is provided, check the session
            $guestToken = session('guest_token');

            if (!$guestToken) {
                // If no guest_token is found in the session, generate a new one and store it
                $guestToken = Str::uuid();
                session(['guest_token' => $guestToken]);
            }
        }

        // Return the cart associated with the guest_token
        return Carts::firstOrCreate(['guest_token' => $guestToken]);
    }

    public function meetingTime(Request $request)
    {
        // Validate the request input
        $request->validate([
            'guest_token' => 'required',
            'meeting_time' => 'required',
        ]);

        try {
            $guestToken = $request->input('guest_token');
            $meetingTime = $request->input('meeting_time');

            // Update the meeting time for the corresponding guest token
            $updated = Carts::where('guest_token', $guestToken)->update(['meeting_time' => $meetingTime]);

            // Check if the update was successful
            if ($updated) {
                return response()->json(['message' => 'Meeting time updated successfully'], 200);
            } else {
                return response()->json(['message' => 'Guest token not found'], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error updating meeting time', 'error' => $e->getMessage()], 500);
        }
    }


}

