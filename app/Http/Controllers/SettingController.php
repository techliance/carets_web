<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\SystemSettings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return $request->all();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = SystemSettings::where(function($q) use ($filter){
                $q->where('setting_type','like', '%' . $filter . '%')
                    ->orWhere('name','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);




        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data2['name'] = trim($data['name']);
        // $data2['value'] = trim($data['value']);
        if(isset($data['value']))
            $data2['value'] = trim($data['value']);
        if(isset($data['setting_type']))
            $data2['setting_type'] = trim($data['setting_type']);

        $data2['is_active'] = trim($data['is_active']);

        if(isset($data['id'])){
            $SystemSettings = SystemSettings::where('id', $data['id'])->first();
            if($SystemSettings) {
                $SystemSettings->update($data2);
            }
        }else{
            $SystemSettings = SystemSettings::create($data2);
            $data['id'] = $SystemSettings->id;
        }
        $SystemSettings = SystemSettings::where('id', $data['id'])->first();

        return response()->json([ 'data' => $SystemSettings, 'message' => 'Record stored successfully!'], 200);
    }

    public function show($id)
    {
        $data = SystemSettings::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function destroy($id)
    {
        $SystemSettings = SystemSettings::where('id', $id)->first();
        $SystemSettings->delete();
        return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
    }

    public function settingsStatus(Request $request) {
        $setting_id = $request->setting_id;
        $sound = SystemSettings::where('id', $setting_id)->first();
        if($sound){
            if($sound->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $sound->update($data);
            return response()->json([ 'data' => $sound, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

    public function getTypes() {
        $data = [
            ['label'=>'Video Filters', 'value'=> 'video_filter'],
            ['label'=>'Sales Contact Amount', 'value'=> 'sales_contact_amount'],
            ['label'=>'Sales tax Amount', 'value'=> 'sales_tax_amount']
        ];
        return $this->sendSuccessResponse('data', $data, 'Data retrieved successfully!');
    }

    public function getSalesAmount() {
        $data = SystemSettings::where('setting_type', 'sales_contact_amount')
        ->where('is_active', 1)
        ->first();
        return $this->sendSuccessResponse('data', $data, 'Data retrieved successfully!');
    }

    public function getSalesTaxAmount() {
        $data = SystemSettings::where('setting_type', 'sales_tax_amount')
        ->where('is_active', 1)
        ->first();
        return $this->sendSuccessResponse('data', $data, 'Data retrieved successfully!');
    }

    public function getServerTime(){
        $data = Carbon::now();
        return $this->sendSuccessResponse('data', $data, 'Data retrieved successfully!');
    }

    public function reportProblem(Request $request){
        $admins = ['hafizshahid@gmail.com'];
        $data = [];
        Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
        if($signed_id){
            $data['username'] =  Auth::user()->username;
        }
        Mail::to($admins)->send(new RequestEmail($data, 'Caret - Problem Reported', $request->message));
        return $this->sendSuccessResponse('data', '', 'Email sent successfully!');
    }
}
