<?php

namespace App\Http\Controllers;

use App\AdPayments;
use App\AdsIndexing;
use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdCampaigns;
use App\AdCards;
use App\AdPlans;
use App\Ads;
use App\AdStatus;
use App\Ages;
use App\CampaignDetails;
use App\Campaigns;
use App\CampaignsAds;
use App\Carets;
use App\CLMLicense;
use App\CLMPayments;
use App\CLMPlans;
use App\CLMRequest;
use App\CLMKeyword;
use App\Countries;
use App\Gender;
use App\Hashs;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use PharIo\Manifest\License;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Customer;
use Stripe\Plan;

class KeywordController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $search = "";
        
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if($request->input('search')){
            $search = $request->input('search');
           
        }
        
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CLMKeyword::where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%');
                $q->orWhere('title','like', '%' . $filter . '%');
                $q->orWhere('type','like', '%' . $filter . '%');
            })
            // ->when($search, function($q) use ($search){
            //     if ($search['fromDate'] && $search['toDate']) {
            //         $q->where('created_at', '>=', $search['fromDate']);
            //         $q->where('created_at', '<=', $search['toDate']);
            //     } elseif ($search['fromDate'] && !$search['toDate']) {
            //         $q->where('created_at', '>=', $search['fromDate']);
            //     } elseif (!$search['fromDate'] && $search['toDate']) {
            //         $q->where('created_at', '<=', $search['toDate']);
            //     }
            // })
          
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);



        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }



    public function store(Request $request)
    {
        $data = $request->all();

        $data2['title'] = trim($data['title']);
        $data2['type'] = trim($data['type']);

        if(isset($data['is_active']))
        $data2['is_active'] = trim($data['is_active']);

        if(isset($data['id'])){
            $CLMKeyword = CLMKeyword::where('id', $data['id'])->first();
            if($CLMKeyword) {
                $CLMKeyword->update($data2);
            }
        }else{
            $CLMKeyword = CLMKeyword::create($data2);
            $data['id'] = $CLMKeyword->id;
        }

        return response()->json([ 'data' => $CLMKeyword, 'message' => 'Record stored successfully!'], 200);
    }

    public function keywordStatus(Request $request) {
        $keyword_id = $request->keyword_id;
        $CLMKeyword = CLMKeyword::where('id', $keyword_id)->first();
        if($CLMKeyword){
            if($CLMKeyword->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $CLMKeyword->update($data);
            return response()->json([ 'data' => $CLMKeyword, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }


    public function show($id){
        $data = CLMKeyword::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => 'Record retrieved successfully!'], 200);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $CLMLicense = CLMKeyword::where('id', $id)->first();
        if($CLMLicense) {   
            $CLMLicense->update($data);
            return response()->json([ 'data' => $CLMLicense, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400); 
        }
    }


    public function destroy($id)
    {
        try {
        $CLMKeyword = CLMKeyword::where('id', $id)->first();
        $CLMKeyword->delete();
        return response()->json(['data'=>'true','status'=>'success', 'message' => 'Deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this keyword","status"=>"error", "msg"=> 'You can not delete this ad, Linked with compaign'], 400);
        }
    }

 

    public function checkKeyword(Request $request)
    {
        $keyword = $request->input('keyword');
        $keyword = str_replace(['^', '#'], '', $keyword);
        $keyword = CLMKeyword::where('title', $keyword)->where('is_active', 1)->first();
        if ($keyword) {
            switch ($keyword->type) {
                case 'naughty':
                    return response()->json([ 
                        'keyword'=> $keyword,
                        'status'=>403,
                        'message' => 'The requested caret is not authorized because it violates our terms of service.'
                    ]);
                
                case 'not_for_sale':
                    return response()->json([
                        'keyword'=> $keyword,
                        'status'=>403,
                        'message' => 'The requested caret is not available.'
                    ]);
                
                case 'assigned':
                    return response()->json([
                        'keyword'=> $keyword,
                        'status'=>403,
                        'message' => 'The requested caret is not currently available.'
                    ]);
                
                case 'special_pricing':
                    return response()->json([
                        'keyword'=> $keyword,
                        'status'=>403,
                        'message' => 'The requested caret is available for sale at a special price.'
                    ]);
                
                default:
                    return response()->json([
                        'message' => 'The requested caret is available.'
                    ]);
                }   
            
        } else {
            return response()->json([
                'status'=>200,
                'message' => 'The requested caret is available.'
            ]);
        }
    }

}