<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission as ExtendedPermission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    private $permissionRepository;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:permission-list', ['only' => ['index']]);
        $this->middleware('permission:permission-create', ['only' => ['create','store']]);
        $this->middleware('permission:permission-edit', ['only' => ['edit','update', 'show']]);
        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);;
        //$this->permissionRepository = $permissionRepository;
    }
    public function index(Request $request)

    {
       $permissionRepository = new ExtendedPermission();
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 10;

        if ($filter == '' && $sort == ''){
            $data = $permissionRepository->getFilteredAndOrderByAttrAllPermissions($pagination);
        }
        elseif ($filter != '' && $sort == ''){
            $data = $permissionRepository->getFilteredAndOrderByAttrAllPermissions($pagination, $filter);
        }
        elseif ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
            $data    = $permissionRepository->getFilteredAndOrderByAttrAllPermissions($pagination, $filter , $sortName, $orderType);

        }
        else {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1] ? 'desc' : 'asc';
            $data    = $permissionRepository->getFilteredAndOrderByAttrAllPermissions($pagination, $filter , $sortName, $orderType);
        }
        if( $request->is('api/*')){
            return $this->sendSuccessResponse('permissions', $data, 'Permissions retrieved successfully!');
        }
        $roles = $data;
        return view('roles.index',compact('roles'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permissionRepository = new ExtendedPermission();
        $rules = ExtendedPermission::$rules;
        $rules['name'] = 'required|unique:permissions,name';
        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->messages(),"status"=>"error", "message"=>"invalidate" ], 400);
        }
        $data = $request->except('token');
        $data['guard_name'] = 'web';
        $data['module_id'] = $request['module_id']['value'];
        if(isset($data['order']) && !$data['is_menu']){
            $data['order'] = 0;
        }
        elseif(isset($data['order'])){
            $ifOrderExist = ExtendedPermission::where('order', $data['order']);
            if($ifOrderExist){
                $newOrder = ExtendedPermission::max('order') + 1;
                $ifOrderExist->update(['order' => $newOrder]);
            }
        }else{
            $data['order'] = 0;
        }
        if(!isset($data['parent_label'])){
            $data['parent_label'] = 0;
        }

        $permission = $permissionRepository->create($data);
//        $permission = ExtendedPermission::create($data);
        return $this->sendSuccessResponse('permission', $permission, 'Permission stored successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($permission_id)
    {
        $permissionRepository = new ExtendedPermission();
        $permission = $permissionRepository->findPermissionWithParent($permission_id);
        if($permission){
            $permissionTypeOptions[] = ['value' => $permission->permission_type, 'label' => ucfirst($permission->permission_type)];
            if($permission->parent){
                $parentLabelOptions[] = ['value' => $permission->parent_label, 'label' => $permission->parent->label];
            }else{
                $parentLabelOptions = null;
            }
            if($permission->order == 0){
                $orderOptions = null;
            }else{
                $orderOptions[] = ['value' => $permission->order, 'label' => $permission->label. ', Order no: '. $permission->order];
            }
            $data['permission'] = $permission;
            $data['permissionTypeOptions'] = $permissionTypeOptions;
            $data['parentLabelOptions'] = $parentLabelOptions;
            $data['orderOptions'] = $orderOptions;
            $data['permission']['module_id'] = [
                'label' => $permission->module->name,
                'value' => $permission->module->id
            ];
            return $this->sendSuccessResponse('data', $data, 'Permission retrieved successfully!');
        }
        else{
            return $this->sendErrorResponse('Permission not found.');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $permission_id)
    {
        $permissionRepository = new ExtendedPermission();
        $rules = ExtendedPermission::$rules;
        $rules['name'] = "unique:permissions,name,$permission_id";
        $validator = Validator::make($request->except('token'), $rules);
        if ($validator->fails()) {
            return response()->json( ["error"=>$validator->messages(),"status"=>"error", "message"=>"invalidate" ], 400);
        }
        $data = $request->except('_method');
        $data['guard_name'] = 'web';
        $data['module_id'] = $request['module_id']['value'];
        if(isset($data['order']) && !$data['is_menu']){
            $data['order'] = 0;
        }
        elseif(isset($data['order'])){
            $ifOrderExist = $permissionRepository->findIfOrderExist($data['order'], $permission_id);
            if($ifOrderExist){
//                $exchangeOrder = ExtendedPermission::where('id', $permission_id)->value('order');
                $exchangeOrder = $permissionRepository->getExchangeOrderValue($permission_id);
//                $ifOrderExist->update(['order' => $exchangeOrder]);
                $permissionRepository->update(['order' => $exchangeOrder], $ifOrderExist->id);
            }
        }else{
            $data['order'] = 0;
        }
        if(!isset($data['parent_label'])){
            $data['parent_label'] = 0;
        }
//        $ifUpdated = ExtendedPermission::
//            where('id', $permission_id)
//            ->update($data);
        print_r($data);
        echo $permission_id;
        return $ifUpdated = ExtendedPermission::where('id',$permission_id)->update($data);
//        $permission = ExtendedPermission::where('id', $permission_id)->first();
        $permission = ExtendedPermission::find($permission_id);
        return $this->sendSuccessResponse('permission', $permission, 'Permission updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($permission_id)
    {
        $permission = ExtendedPermission::find($permission_id);
        $permission = $this->permissionRepository->find($permission_id);
        if($permission){
//            $highestOrder = ExtendedPermission::max('order');
//            if($highestOrder == $permission->order){
                $permission->delete();
//            }else{

//            }
            return $this->sendSuccessResponse('data', [], 'Permission deleted successfully');
        }else{
            return $this->sendErrorResponse('Permission not found.');
        }
    }
    public function getMenu(Request $request){
        $ExtendedPermission = new ExtendedPermission();

        $role_id = $request->role_id?$request->role_id:1;

        $permissionsData = $ExtendedPermission->getMenuData($role_id);
        $permission = array(
            'permissions' => array(),
            'parent_labels' => array()
        );

        foreach($permissionsData as $item){
            $permission['permissions'][$item->id] = $item;
            $permission['parent_labels'][$item->parent_label][] = $item->id;
        }
        return $this->sendSuccessResponse('menu', $permission, 'Menu Items retrieved successfully!');
    }

    public function getAdvertiserMenu(Request $request){
        $ExtendedPermission = new ExtendedPermission();

        $role_id = $request->role_id?$request->role_id:5;

        $permissionsData = $ExtendedPermission->getMenuData($role_id);
        $permission = array(
            'permissions' => array(),
            'parent_labels' => array()
        );

        foreach($permissionsData as $item){
            $permission['permissions'][$item->id] = $item;
            $permission['parent_labels'][$item->parent_label][] = $item->id;
        }
        return $this->sendSuccessResponse('menu', $permission, 'Menu Items retrieved successfully!');
    }


    public function getCLMMenu(Request $request){
        $ExtendedPermission = new ExtendedPermission();

        $role_id = $request->role_id?$request->role_id:6;

        $permissionsData = $ExtendedPermission->getMenuData($role_id);
        $permission = array(
            'permissions' => array(),
            'parent_labels' => array()
        );

        foreach($permissionsData as $item){
            $permission['permissions'][$item->id] = $item;
            $permission['parent_labels'][$item->parent_label][] = $item->id;
        }
        return $this->sendSuccessResponse('menu', $permission, 'Menu Items retrieved successfully!');
    }

}
