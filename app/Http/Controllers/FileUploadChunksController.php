<?php

namespace App\Http\Controllers;

use App\FileUpload;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Profiler\Profile;

class FileUploadChunksController extends Controller
{

        public function uploadChunk(Request $request)
        {

            Log::info("Step1 Chunk: Chunk Received.".date('Y-m-d H:i:s', time()));
           
            // Validate the incoming request
            $request->validate([
                'video_url' => 'required|file',
                'chunk_index' => 'required|integer',
                'total_chunks' => 'required|integer',
                'file_name' => 'required|string',
                'unique_id' => 'required|string',
                'chunk_hash' => 'required|string'
            ]); 
    
            try{
                if ($request->hasFile('video_url')) {
                
                    $uploadedHash = md5_file($request->file('video_url')->getRealPath());
                    if ($uploadedHash !== $request->input('chunk_hash')) {
                    Log::info("Chunk HASH: Chunk Courrupt.".date('Y-m-d H:i:s', time()));

                        return response()->json(['message' => 'Chunk hash mismatch', 'status_code' => 400], 400);
                    }
                    $uniqueId = $request->input('unique_id');
                    $fileName = $request->input('file_name');
                    $chunkIndex = $request->input('chunk_index');
                    $totalChunks = $request->input('total_chunks');
            
                    // Ensure the temporary directory for this file exists
                    $tempDir = storage_path("app/chunks/{$uniqueId}");
                    if (!is_dir($tempDir)) {
                        mkdir($tempDir, 0777, true);
                    }
            
                    // Save the chunk to the temporary directory
                    $chunkPath = "{$tempDir}/chunk_{$chunkIndex}";
                    if (file_exists($chunkPath)) {
                        Log::info("Chunk {$chunkIndex} already exists. Skipping...");
                        return response()->json(['message' => 'Duplicate chunk skipped', "status_code" => 200], 200);
                    }
                    
                    $request->file('video_url')->move($tempDir, "chunk_{$chunkIndex}");
            
                    // Update the database record
                    $fileUpload = FileUpload::firstOrCreate(
                        ['unique_id' => $uniqueId],
                        [
                            'file_name' => $fileName,
                            'total_chunks' => $totalChunks,
                            'received_chunks' => 0,
                        ]
                    );
            
                    $fileUpload->increment('received_chunks');
            
                    // If all chunks are received, assemble the file
                    if ($fileUpload->received_chunks == $totalChunks) {
                        
                        $finalPath = storage_path("app/Videos/{$fileName}");
                        
                        $this->assembleChunks($tempDir, $finalPath);
                        // $this->assembleChunksFFMPEG($tempDir, $finalPath);
                        // $this->validateVideo($finalPath);

                        // Delete the temporary chunk directory
                        $this->deleteDirectory($tempDir);
            
                        // Remove the database record for this upload
                        $fileUpload->delete();
            
                        return response()->json(['message' => 'File uploaded, assembled, and record removed successfully',"status_code"=>200], 200);
                    }
                    
                    return response()->json(['message' => 'Chunk uploaded successfully',"status_code"=>200], 200);
                }else{
                    return response()->json(['message' => 'Chunk not found',"status_code"=>404], 404);
                }
            }catch(\Exception $e){
                Log::error($e->getMessage());
                return response()->json(['message' => $e->getMessage(),"status_code"=>502], 502);
            }
            
        }
    
        private function assembleChunks($chunkDir, $finalPath)
        {

            $lockFile = "{$chunkDir}/lock";
            if (file_exists($lockFile)) {
                Log::info("Chunk Assembly: File assembly in progress.");
                throw new \Exception('File assembly in progress.');
            }
            touch($lockFile);
            try {
                $files = glob("{$chunkDir}/chunk_*");
                usort($files, function ($a, $b) {
                    return intval(str_replace('chunk_', '', basename($a))) - intval(str_replace('chunk_', '', basename($b)));
                });
                
                $output = fopen($finalPath, 'wb');
                foreach ($files as $file) {
                    $chunk = fopen($file, 'rb');
                    stream_copy_to_stream($chunk, $output);
                    fclose($chunk);
                }
                fclose($output);
            } finally {
                unlink($lockFile); // Ensure lock file is removed
            }
            /*$files = glob("{$chunkDir}/chunk_*");
            usort($files, function ($a, $b) {
                return intval(basename($a)) - intval(basename($b));
            });
    
            $output = fopen($finalPath, 'wb');
            foreach ($files as $file) {
                $chunk = fopen($file, 'rb');
                stream_copy_to_stream($chunk, $output);
                fclose($chunk);
            }
            fclose($output);*/
        }
    
        private function deleteDirectory($dir)
        {
            $files = array_diff(scandir($dir), ['.', '..']);
            foreach ($files as $file) {
                $path = "{$dir}/{$file}";
                is_dir($path) ? $this->deleteDirectory($path) : unlink($path);
            }
            return rmdir($dir);
        }

        private function assembleChunksFFMPEG($chunkDir, $finalPath)
        {
            $files = glob("{$chunkDir}/chunk_*");
            usort($files, function ($a, $b) {
                return intval(str_replace('chunk_', '', basename($a))) - intval(str_replace('chunk_', '', basename($b)));
            });
        
            $fileList = "{$chunkDir}/file_list.txt";
            file_put_contents($fileList, implode("\n", array_map(function($file) {
                return "file '$file'";
            }, $files)));

            $command = "ffmpeg -f concat -safe 0 -i $fileList -c copy $finalPath";
            exec($command, $output, $returnCode);

            if ($returnCode !== 0) {
                Log::info("Chunk Assembly: File assembly failed.");
                Log::error($output);
                throw new \Exception("FFmpeg assembly failed: " . implode("\n", $output));
            }
        }

        private function validateVideo($filePath)
        {
            $command = "ffprobe -v error -select_streams v:0 -show_entries stream=codec_type -of json \"$filePath\"";
            $output = shell_exec($command);
            $data = json_decode($output, true);

            if (!isset($data['streams']) || count($data['streams']) == 0) {
                throw new \Exception('Assembled file is invalid or contains no video stream.');
            }
        }

        public function deleteOldChunks(Request $request)
        {
            try {
                $deletedChunks = [];

                // Retrieve all directories in the 'chunks' directory
                $directories = Storage::directories('chunks');

                foreach ($directories as $directory) {
                    // Get the last modified timestamp of the directory
                    $lastModified = Storage::lastModified($directory);

                    // Check if the directory is older than 24 hours
                    if ($lastModified < now()->subHours(24)->getTimestamp()) {
                        $deletedChunks[] = $directory;

                        // Delete the directory and its contents
                        Storage::deleteDirectory($directory);
                        
                        // Extract unique_id from the directory name
                        $uniqueId = basename($directory);
                        Log::info('Chunk unique ID: ' . $uniqueId);
                        // Remove associated FileUpload record
                        $fileUpload = FileUpload::where('unique_id', $uniqueId)->first();
                        if ($fileUpload) {
                            $fileUpload->delete();
                            Log::info("Deleted FileUpload record for unique ID: {$uniqueId}");
                        }
                    }
                }

                if (!empty($deletedChunks)) {
                    Log::info("Deleted old chunk directories: " . implode(', ', $deletedChunks));
                    return response()->json([
                        'message' => 'Old chunks and associated records deleted successfully.',
                        'status_code' => 200,
                    ], 200);
                } else {
                    return response()->json([
                        'message' => 'No old chunks found.',
                        'status_code' => 200,
                    ], 200);
                }
            } catch (\Exception $e) {
                Log::error("Error deleting old chunks: " . $e->getMessage());
                return response()->json([
                    'message' => 'An error occurred while deleting old chunks.',
                    'error' => $e->getMessage(),
                    'status_code' => 500,
                ], 500);
            }
        }


}