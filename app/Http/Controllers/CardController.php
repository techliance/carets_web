<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdCards;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\PaymentMethod;
use Stripe\Customer;

class CardController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id)
    {
        $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = AdCards::with([
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            }])
            ->where(function($q) use ($filter){
                $q->where('card_number','like', '%' . $filter . '%');
            })

            ->when($user_id > 0,function($q) use ($user_id){
                $q->where('user_id', $user_id);
            })

            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }

    public function store(Request $request)
    {
        $data = $request->all();

        // Ensure stripe_token is provided
        if (!isset($data['stripe_token'])) {
            return response()->json(["error" => "error", "status" => "error", "msg" => "Stripe token is required."], 400);
        }

        Stripe::setApiKey(config("services.stripe.secret"));

        $user = User::where('id', $data['user_id'])->first();

        if (!$user) {
            return response()->json(["error" => "error", "status" => "error", "msg" => "User not found."], 404);
        }

        // Create Stripe Customer if not exists
        if (!$user->stripe_id) {
            $customer = Customer::create([
                'description' => 'Carets subscription',
                'name' => $user->name,
                'email' => $user->email
            ]);
            $user->update(['stripe_id' => $customer->id]);
            $customerStripeId = $customer->id;
        } else {
            $customerStripeId = $user->stripe_id;
        }

        try {
            // Attach the payment method to the customer
            $paymentMethod = PaymentMethod::retrieve($data['stripe_token']);
            $paymentMethod->attach(['customer' => $customerStripeId]);

            // Update customer's default payment method if requested
            if (isset($data['set_default']) && $data['set_default']) {
                Customer::update($customerStripeId, [
                    'invoice_settings' => ['default_payment_method' => $paymentMethod->id]
                ]);

                // Reset all previous default cards
                AdCards::where('user_id', $user->id)->update(['set_default' => 0]);
            }

            // Save card details in DB
            $cardData = [
                'user_id' => $user->id,
                'stripe_id' => $paymentMethod->id,
                'card_number' => "xxxx xxxx xxxx " . $paymentMethod->card->last4,
                'card_expiry' => $paymentMethod->card->exp_month . '/' . $paymentMethod->card->exp_year,
                'card_cvc' => "xxx",
                'set_default' => isset($data['set_default']) ? $data['set_default'] : 0
            ];

            $adCard = AdCards::create($cardData);

            return response()->json(['data' => $adCard, 'message' => 'Card stored successfully!'], 200);

        } catch (\Stripe\Exception\CardException $e) {
            return response()->json(["error" => "error", "status" => "error", "msg" => $e->getError()->message], 400);
        } catch (\Exception $e) {
            return response()->json(["error" => "error", "status" => "error", "msg" => "An error occurred while processing the request."], 500);
        }
    }

    public function show($id)
    {
        $data = AdCards::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }

    public function destroy($id)
    {
        try {

            $AdCards = AdCards::where('id', $id)->first();
            $AdCards->delete();
            return response()->json(['', 'message' => 'Record deleted successfully!'], 200);

        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
        }


    }

    public function cardStatus(Request $request) {
        $card_id = $request->card_id;
        $data['set_default'] = 0;

        $AdCards = AdCards::where('id', $card_id)->first();
        AdCards::where('user_id', $AdCards->user_id)->update($data);
        if($AdCards){
            Stripe::setApiKey(config("services.stripe.secret"));
            $AdCard = AdCards::where('id', $card_id)->first();
            $user = User::where('id', $AdCard->user_id)->first();
            $customer_srtipe_id = $user->stripe_id;

            $StripeCustomer = Customer::update(
                $customer_srtipe_id,
                ['invoice_settings' => ['default_payment_method' => $AdCards->stripe_id]]
              );

            $data2['set_default'] = 1;
            $AdCard->update($data2);
            return response()->json([ 'data' => $AdCards, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }



    }


    public function useExistingCard(Request $request) {
        $card_id = $request->card_id;
        $AdCards = AdCards::where('id', $card_id)->first();
        if($AdCards){
            Stripe::setApiKey(config("services.stripe.secret"));
            $user = User::where('id', $AdCards->user_id)->first();
            $customer_srtipe_id = $user->stripe_id;

            $StripeCustomer = Customer::update(
                $customer_srtipe_id,
                ['invoice_settings' => ['default_payment_method' => $AdCards->stripe_id]]
              );

            $data2['set_default'] = 1;
            $AdCards->update($data2);
            return response()->json([ 'data' => $AdCards, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }

    }

}
