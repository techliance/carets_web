<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Role;

class ModuleController extends Controller
{



    function __construct()
    {
        //$this->moduleRepository = $moduleRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ModuleRepository = new Module();
        $validator = Validator::make($request->except('token'),Module::$rules);
        if($validator->fails()){
            return response()->json(['error'=>$validator->messages(),'status'=>'error','msg'=>'invalidate'],400);
        }
        $data = $request->except('token');
        $module = $ModuleRepository->create($data);
        if($module){
            return $this->sendSuccessResponse('message',$module,'Module Created Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        //
    }

    /**
     * Get ALl modules from the database
     */
    public function getAllModules(Request $request)
    {
        $moduleRepository = new Module();
        $modules = $moduleRepository->all();
        $modules = $modules->map(function($item){
            return [
                'label' => $item->name,
                'value' => $item->id,
            ];
        });
        if($request->id){
            $modules = $moduleRepository->all();
            $role = Role::find($request->id);
            foreach ($modules as $module) {
                foreach ($module->permissions as $permission) {
                    if($role->hasPermissionTo($permission)){
                        $permission['checked'] = true;
                    }else{
                        $permission['checked'] = false;
                    }
                }
            }
            $permissions = $role->permissions;
            if ($request->is('api/*')) {
                return $this->sendSuccessResponse('data',['modules'=>$modules,'permissions' =>$permissions,'role'=>$role->name],'Modules Retrieved Successfully');
            }
        }
        return $this->sendSuccessResponse('data',['modules'=>$modules],'Modules Retrieved Successfully');
    }
}
