<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResumeEmployee;

class ResumeController extends Controller
{
    function __construct()
    {

    }

    // public function getAgencyTypes(){
    //     $agencyTypes = $this->agencyRepository->getAgencyTypes();
    //     return $this->sendSuccessResponse('agencyTypes', $agencyTypes, 'Company Types retrieved successfully!');
    // }

    public function index(Request $request)
    {
        $ResumeEmployee = new ResumeEmployee();
        $orders = $ResumeEmployee->getEmployees($request);
        return $this->sendSuccessResponse('employees', $orders, 'employees data retrieved successfully!');
    }

    public function store(Request $request)
    {
        $ResumeEmployee = new ResumeEmployee();
        $input = $request->all();
        $user = $ResumeEmployee->addEmployee($input);
        return $this->sendSuccessResponse('employee', $user, 'Employee has been created successfully!');

    }

    public function update(Request $request, $id)
    {
        $ResumeEmployee = new ResumeEmployee();
        $input = $request->all();
        $user = $ResumeEmployee->updateEmployee($input,$id);
        return $this->sendSuccessResponse('employee', $user, 'Employee has been updated successfully!');

    }


    public function show(Request $request, $id)
    {
        $ResumeEmployee = new ResumeEmployee();
        $user = $ResumeEmployee->employeeDetail($id);
        return $this->sendSuccessResponse('employee', $user, 'Employee retrieved successfully!');

    }

    public function destroy(Request $request, $id)
    {
        ResumeEmployee::where('id',$id)->delete();
        return $this->sendSuccessResponse('data', '', 'Employee has been deleted successfully!');

    }


}
