<?php

namespace App\Http\Controllers;

use App\AdPlans;
use App\CLMPlans;
use App\CLMRequest;
use App\CaretPricing;
use Illuminate\Http\Request;
use Stripe\Price;
use Stripe\Stripe;

class SplashPlanController extends Controller
{
    public function getDurations() {
        $data = [

            ['label'=>'Year', 'value'=> 'year']
        ];
        return $this->sendSuccessResponse('data', $data, 'Data retrieved successfully!');
    }

    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;

        if ($filter == '' && $sort != '')
        {
            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';
        }

        $data = CLMPlans::where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
           ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
           })
           ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
           })
           ->paginate($pagination);

        if( $request->is('api/*')){
            return $this->sendSuccessResponse(
                'data', ['pagination' => $data],
                'Record retrieved successfully!');
        }
        return null;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data2['title'] = trim($data['title']);
        $data2['amount'] = trim($data['amount']);
        // $data2['duration'] = trim($data['duration']);
        $data2['duration'] = 'year';

        if(isset($data['interval_count'])){
            $data2['interval_count'] = trim($data['interval_count']);
        }
        if(isset($data['is_active'])){
            $data2['is_active'] = trim($data['is_active']);
        }
        Stripe::setApiKey(config("services.stripe.secret"));

        if(isset($data['id'])){
            $clmPlan = CLMPlans::where('id', $data['id'])->first();
            if($clmPlan) {
                $clmPlan->update($data2);
            }
        }else{
            $newSplashPlan = Price::create(array(
                    'unit_amount' =>$data2['amount']*100,
                    'recurring' => ['interval' => $data2['duration'],
                    'interval_count' => $data2['interval_count']],
                    'currency' => 'usd',
                    'product' => 'prod_RqquyjIKF0Xh4B'
                )
            );
            $data2['stripe_id'] = $newSplashPlan->id;
            $clmPlan = CLMPlans::create($data2);
            $data['id'] = $clmPlan->id;
            // $caretPricingData = ['plan_id' => $clmPlan->id, 'title' => $clmPlan->title, 'one_year_license' => $clmPlan->amount, 'stripe_id_one_year' => $newSplashPlan->id];
            // CaretPricing::create($caretPricingData);

            // $CaretPricing = CaretPricing::create([
            //     'plan_id' => $clmPlan->id,
            //     'title' => $clmPlan->title,
            //     'one_year_license' => $clmPlan->amount,
            //     'stripe_id_one_year' => $newSplashPlan->id,
            //     'is_active' => 0,
            // ]);

            $caretPricingData = [
                'plan_id' => $clmPlan->id,
                'title' => $clmPlan->title,
                'is_active' => 0,
            ];


            if ($data2['interval_count'] == 1) {
                $caretPricingData['one_year_license'] = $clmPlan->amount;
                $caretPricingData['stripe_id_one_year'] = $newSplashPlan->id;
            } elseif ($data2['interval_count'] == 2) {
                $caretPricingData['two_year_license'] = $clmPlan->amount;
                $caretPricingData['stripe_id_two_year'] = $newSplashPlan->id;
            } elseif ($data2['interval_count'] == 3) {
                $caretPricingData['three_year_license'] = $clmPlan->amount;
                $caretPricingData['stripe_id_three_year'] = $newSplashPlan->id;
            }else{
                $caretPricingData['one_year_license'] = $clmPlan->amount;
                $caretPricingData['stripe_id_one_year'] = $newSplashPlan->id;
            }

            $CaretPricing = CaretPricing::create($caretPricingData);

        }


        $clmPlan = CLMPlans::where('id', $data['id'])->first();

        $CaretPricing = CaretPricing::where('plan_id', $clmPlan->id)->first();

        return response()->json([ 'data' => $clmPlan, 'newPricing' => $CaretPricing , 'message' => 'Record stored successfully!'], 200);
    }

    public function show($id)
    {
        $data = CLMPlans::where('id', $id)->first();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }


    public function destroy($id)
    {
        try {
            $clmPlan = CLMPlans::where('id', $id)->first();
            $clmPlan->delete();
            return response()->json(['', 'message' => 'Record deleted successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
        }
    }

    public function planStatus(Request $request) {
        $plan_id = $request->plan_id;
        $CLMPlans = CLMPlans::where('id', $plan_id)->first();
        if($CLMPlans){
            if($CLMPlans->is_active == 1){
                $data['is_active'] = 0;
            }else{
                $data['is_active'] = 1;
            }
            $CLMPlans->update($data);
            return response()->json([ 'data' => $CLMPlans, 'message' => 'Record updated successfully!'], 200);
        }else{
            return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
        }
    }

    public function getInfluencerPlan() {
        $data = CLMPlans::where('title', 'influencer')
        ->where('is_active', 1)
        ->get();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }
    public function getPlans()
    {
        $data = CLMPlans::where('is_active', 1)
        ->orderBy('id', 'DESC')
        ->select('id as value', 'title as label')
        ->get();
        return response()->json([ 'data' => $data, 'message' => ''], 200);
    }
}
