<?php

namespace App\Http\Controllers;

use App\AdPayments;
use App\AdsIndexing;
use App\Helpers\Helper;
use App\Mail\RequestEmail;
use App\AdCampaigns;
use App\AdCards;
use App\AdPlans;
use App\Ads;
use App\AdStatus;
use App\Ages;
use App\CampaignDetails;
use App\Campaigns;
use App\CampaignsAds;
use App\CaretPricing;
use App\Carets;
use App\CLMLicense;
use App\CLMPayments;
use App\CLMPlans;
use App\CLMRequest;
use App\CLMKeyword;
use App\Countries;
use App\Gender;
use App\Hashs;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PharIo\Manifest\License;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Invoice;

class LicenseController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id=null)
    {
        $user_id = (int)$user_id > 0?$user_id:null;
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        $sortName = null;
        $orderType = null;
        $search = "";
        $status_id=null;

        $pagination = ($request->input('pageSize')) ? $request->input('pageSize') : 20;
        if($request->input('search')){
            $search = $request->input('search');
            if (isset($search['status_id']) && $search['status_id'] == true) {
                $status_id = 1;
            } else {
                $status_id = null;
            }
        }
        if ($filter == '' && $sort != ''){

            $sortEx     = explode(',', $sort);
            $sortName   = $sortEx[0];
            $orderByEx  = explode(":", $sortEx[1]);
            $orderType  = $orderByEx[0] == 'desc' && $orderByEx[1]=='true' ? 'desc' : 'asc';

        }

        $data = CLMLicense::with(['status','payments', 'plan', 'clmRequest', 'pricing',
            'user'=>function($q){
                $q->with(['profile'=>function($q) {
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                }]);
            }])
            ->select([
                'clm_licenses.*',
                DB::raw("CONCAT('".config('app.awsurl')."', caret_logo) AS caret_logo")
            ])->where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%');
                $q->orWhere('caret_title','like', '%' . $filter . '%');
                $q->orWhere('company_name','like', '%' . $filter . '%');
                $q->orWhereHas('user', function($q) use ($filter) {
                    $q->where('email', 'like', '%' . $filter . '%');
                });
                $q->orWhereHas('plan', function($q) use ($filter) {
                    $q->where('title', 'like', '%' . $filter . '%');
                });
            })
            ->when($search, function($q) use ($search){
                if ($search['fromDate'] && $search['toDate']) {
                    $q->where('created_at', '>=', $search['fromDate']);
                    $q->where('created_at', '<=', $search['toDate']);
                } elseif ($search['fromDate'] && !$search['toDate']) {
                    $q->where('created_at', '>=', $search['fromDate']);
                } elseif (!$search['fromDate'] && $search['toDate']) {
                    $q->where('created_at', '<=', $search['toDate']);
                }
            })
            ->when($status_id, function($q){
                $q->where('status_id',1);
            })
            ->when($user_id > 0,function($q) use ($user_id){
                $q->where('user_id', $user_id);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);



        if( $request->is('api/*')){
            return $this->sendSuccessResponse('data', ['pagination' => $data], 'Record retrieved successfully!');
        }
        return null;

    }


    public function store(Request $request)
    {
        $data = $request->all();

        $data2['caret_title'] = str_replace(['^', '#'], '', trim($data['caret_title']));
        $data2['user_id'] = trim($data['user_id']);
        $data2['status_id'] = trim($data['status_id']);



        if (isset($data['subscription_plan'])) {
            if (is_numeric($data['subscription_plan'])) {
                $data2['subscription_plan'] = trim($data['subscription_plan']);
            } else {
                $subscription_plan = CLMPlans::where('title', $data['subscription_plan'])->first();
                if ($subscription_plan) {
                    $data2['subscription_plan'] = $subscription_plan->id;
                }
            }
        }

        // $subscription_plan_id = CLMPlans::where('title', $data['subscription_plan'])->first()->id;

        // if ( $subscription_plan_id){
        //     $data2['subscription_plan'] = trim($subscription_plan_id);
        //     }

        // if(isset($data['subscription_plan']))
        //     $data2['subscription_plan'] = trim($data['subscription_plan']);

        if(isset($data['subscription_amount']))
            $data2['subscription_amount'] = trim($data['subscription_amount']);


        if(isset($data['subscription_stripe_id']))
            $data2['subscription_stripe_id'] = trim($data['subscription_stripe_id']);

        if(isset($data['company_name']))
            $data2['company_name'] = trim($data['company_name']);



            if (isset($data['caret_id']) && !empty($data['caret_id'])) {
                $data2['caret_id'] = trim($data['caret_id']);
            } else {
                $caret_id = $this->addCarets($data2['caret_title']);
                $data2['caret_id'] = trim($caret_id);
            }
        //  $caret_id = $this->addCarets($data2['caret_title']);
        //  $data2['caret_id'] = trim($caret_id);

        if(isset($data['id'])){
            $CLMLicense = CLMLicense::where('id', $data['id'])->first();
            if($CLMLicense) {
                $CLMLicense->update($data2);
            }
        }else{
            $data2['is_active'] = 1;
            $CLMLicense = CLMLicense::create($data2);
            $data['id'] = $CLMLicense->id;
        }

        if($data2['status_id'] == 4){

            $response = $this->activateSubscription($CLMLicense->id);

        }

        return response()->json([ 'data' => $CLMLicense, 'message' => 'Record stored successfully!'], 200);
    }

    public function getLicense(Request $request, $user_id){
        $filter = $request->input('filter');
        $user_id = (int)$user_id > 0 ? $user_id : null;
        $users = CLMLicense::with(['plan'])
        ->orderBy('caret_title','ASC')
        ->select('id as value', 'caret_title as label', 'subscription_plan', 
        DB::raw("CONCAT('".config('app.awsurl')."', caret_logo) AS caret_logo"))
        ->where('status_id', 4)
        ->where(function($q) use ($filter) {
            $q->where('caret_title', 'like', '%' . $filter . '%'); // Corrected field name
        })
        ->when($user_id,function($q,$user_id){
            $q->where('user_id',$user_id);
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }

    public function getCaretLogoRendom(Request $request, $user_id){ 
        $filter = $request->input('filter');
        $licenseId = $request->input('licenseId');

        $user_id = (int)$user_id > 0 ? $user_id : null;
        // Auth::check() ? $user_id = Auth::user()->id : $user_id = null;
        $CLMLicense = CLMLicense::orderBy('id','DESC')
            ->where('caret_logo', '!=', null)
            ->select('id as value', 'caret_title as label', DB::raw("CONCAT('".config('app.awsurl')."', caret_logo) AS caret_logo"))
            ->where(function($q) use ($filter) {
                $q->where('caret_title', 'like', '%' . $filter . '%'); // Corrected field name
            })
            ->where('caret_logo', '!=', null)
           ->when($user_id, function($q, $user_id) {
               $q->where('user_id', $user_id);
           })
           ->when($licenseId, function($q) use ($licenseId) {
               $q->where('id', $licenseId);
           })
            ->limit(12)
            ->get();

        return $this->sendSuccessResponse('data', $CLMLicense, 'Record retrieved successfully!'); // Corrected variable name
    }
    public function defaultSettings(Request $request)
    {
        $data = $request->all();

        $license = CLMLicense::where('id', $data['id'])->first();
        if (count($data) == 1 && isset($data['id'])){
            return response()->json(['data' => $license, 'message' => 'Record retrieved successfully!'], 200);
        }

        if (isset($data['default_ad']))
            $data2['default_ad'] = trim($data['default_ad'][0]);
        if (isset($data['default_intro']))
            $data2['default_intro'] = trim($data['default_intro'][0]);
        if (isset($data['default_finish']))
            $data2['default_finish'] = trim($data['default_finish'][0]);
        if (isset($data['default_sound']))
            $data2['default_sound'] = trim($data['default_sound'][0]);


        $CLMLicense = CLMLicense::where('id', $data['id'])->first();
            if($CLMLicense) {
                $CLMLicense->update($data2);
        }

        return response()->json([ 'data' => $CLMLicense, 'message' => 'Record updated successfully!'], 200);
    }

    public function addHash($data){
        //return $data[0];
        $ret = array();
        if(count($data[0])>0){
            foreach($data[0] as $dt){
                //return $dt;
                $arr['title'] = trim(str_replace('#','',$dt));
                $res = Hashs::updateOrCreate($arr);
                $ret[] =  $res->id;
            }
        }
        return $ret;
    }


    // public function addCarets($data){
    //     $ret = array();
    //     if(count($data[0])>0){
    //         foreach($data[0] as $dt){
    //             $arr['title'] = trim(str_replace('^','',$dt));
    //             $Carets['id'] = '';
    //             $Carets = Carets::where('title', $arr['title'])->first();
    //             if(isset($Carets['id'])){
    //                 $arr['updated_at'] = Carbon::now();
    //                 $Carets->update($arr);
    //             }else{
    //                 //$arr['updated_at'] = Carbon::now();
    //                 $Carets = Carets::create($arr);
    //             }
    //             $ret[] =  $Carets->id;
    //         }
    //     }
    //     return $ret;
    // }

    public function addCarets($data){
        $arr['title'] = trim(str_replace('^','', $data));
        $carets = Carets::where('title', $arr['title'])->first();

        if ($carets) {
            $arr['updated_at'] = Carbon::now();
            $carets->update($arr);
        } else {
            $carets = Carets::create($arr);
        }

        return $carets->id;
    }





        public function show($id)
        {
            $data = Campaigns::with([


            'campaignAds',
            'details'=>function($q){
                $q->with(['age','genders']);
            }])

            ->with([
                'genderStats' => function ($q) {
                    $q->select('gender','campaign_id','genders.title', DB::raw('count(*) as total'))
                    ->join('genders', 'genders.id', '=', 'gender')
                    ->orderBy('gender','asc')
                    ->groupBy('gender');
                }
            ])
            ->with([
                'ageStats' => function ($q) {
                    $q->select('age','campaign_id', DB::raw('count(*) as total'),
                    DB::raw('(SELECT CONCAT(ages.min, " - ", ages.max) as t FROM ages WHERE id = age) as title')
                    )
                    ->orderBy('age','asc')
                    ->groupBy('age');
                }
            ])
            ->with([
                'positionStats' => function ($q) {
                    $q->select('position','campaign_id',
                    DB::raw('count(*) as total'))
                    ->orderBy('position','asc')
                    ->groupBy('position');
                }
            ])

            ->withCount([
            'campaignAds as activeAds'=>function($q){
                $q->where('status_id',4)->limit(1);
            }])



            // ->with([
            //     'campaignViews'=>function($q){
            //         $q->select('gender',DB::raw('count(*) as total'))
            //         ->groupBy('gender')
            //         ->get();
            //     }])


            ->withCount([
                'campaignViews as totalComplete'=>function($q){
                    $q->where('complete',1);
                }])


            ->withCount([
                'campaignViews as totalClicks'=>function($q){
                    $q->where('clicked',1);
                }])

            ->withCount([
            'campaignAds as inActiveAds'=>function($q){
                $q->whereNotIn('status_id',[4])->limit(1);
            }])

            ->withCount(['campaignAds'=>function($q){
                $q->select(DB::raw('sum(campaigns_ads.watch_count)'));
            }])



            ->where('id', $id)->first();
            return response()->json([ 'data' => $data, 'message' => ''], 200);
        }







        public function destroy($id)
        {
            try {

                $CLMLicense = CLMLicense::where('id', $id)->get();
                $CLMLicense->delete();

                return response()->json(['', 'message' => 'Record deleted successfully!'], 200);

            } catch (\Exception $e) {
                return response()->json( ["error"=>"You can not delete this record","status"=>"error", "msg"=> "You can not delete this record"], 400);
            }


        }

        public function caretLicense(Request $request)
        {
            Auth::check()? $signed_id = Auth::user()->id : $signed_id = null;
            $data = CLMLicense::orderBy('id','DESC')
            ->select('id as value', 'caret_title as label')
            ->when($signed_id, function ($query, $signed_id) {
                $query->where('user_id', $signed_id);
            })
            ->get();
            return $this->sendSuccessResponse('data', $data, 'Record retrieved successfully!');
        }
        public function corporateLicense(Request $request)
        {
            $signed_id = Auth::check() ? Auth::user()->id : null;

            $data = CLMLicense::orderBy('id', 'DESC')
                ->select('id as value', 'caret_title as label')
                ->whereHas('pricing', function ($query) {
                    $query->where('title', 'corporate');
                })
                ->when($signed_id, function ($query) use ($signed_id) {
                    $query->where('user_id', $signed_id);
                })
                ->get();

            return $this->sendSuccessResponse('data', $data, 'Record retrieved successfully!');
        }

        public function toggleLicenseCancel(Request $request)
        {
            $response = '';
            $data = $request->all();



            if(isset($data['license_id'])){
                $CLMLicense = CLMLicense::where('id', $data['license_id'])->first();
                if($CLMLicense) {
                    {

                        if($data['cancel_at_period_end'] == 0){
                            $response =   $this->activateSubscription($CLMLicense->id);
                            $data2['status_id'] = 4;
                        }else{
                            $response =  $this->inActivateSubscription($CLMLicense->id);
                            $data2['status_id'] = 6;
                        }
                    }

                    if($response == 'success')
                        $CLMLicense->update($data2);
                }


            $CampaignsAds = CLMLicense::where('id', $data['license_id'])->first();

            //return response()->json([ 'data' => $CampaignsAds, 'message' => 'Record stored successfully!'], 200);
            if($response == 'error'){
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }else{
                return response()->json([ 'data' => $CLMLicense, 'message' => 'Record stored successfully!'], 200);
            }


            }else{
                return response()->json( ["error"=>"Wrong Information","status"=>"error", "message"=>"Invalid Data" ], 400);

            }
        }


        public function licenseStatus(Request $request) {
            $data = $request->all();
            $data2['status_id'] = trim($data['status_id']);

            $license_id = $request->license_id;
            $CLMLicense = CLMLicense::where('id', $license_id)->first();
            if($CLMLicense){

                if($data2['status_id'] == 4){
                    $response =   $this->activateSubscription($CLMLicense->id);

                    $CLMRequest = CLMRequest::where('caret_id', $CLMLicense->caret_id)->first();
                    if ($CLMRequest) {
                        $CLMRequest->update([
                            'status' => 1,
                            'approval_date' => Carbon::now()
                        ]);
                    }
                }else{
                    if($CLMLicense->status_id != 1){
                        $response =  $this->inActivateSubscription($CLMLicense->id);
                    }
                }

                $CLMLicense->update($data2);
                return response()->json([ 'data' => $CLMLicense, 'message' => 'Record updated successfully!'], 200);
            }else{
                return response()->json( ["error"=>"Invalid Data","status"=>"error", "message"=>"Invalid Data" ], 400);
            }

        }






     public function activateSubscription($license_id){


        $ret = CLMPayments::where('license_id', $license_id)
        ->orderBy('current_period_end', 'DESC')
        ->first();

            //campaign_ad_id
            $CLMLicense = CLMLicense::where('id', $license_id)->first();


             ##SUBSCRIPTION PART
            $user = User::where('id',  $CLMLicense->user_id)->first();
            $srtipe_id = 0;
            $CLMPlans = CLMPlans::where('id', $CLMLicense->subscription_plan)->first();



            Stripe::setApiKey(config("services.stripe.secret"));
            if($user->stripe_id != ""){
                try{
                    if(isset($ret->transaction_id)){
                        $Subscription = Subscription::update(
                            $ret->transaction_id,
                            [
                                'cancel_at_period_end' => false,
                            ]
                        );
                        $payment['transaction_type'] = "Re-Activate";
                        $payment['amount'] = 0;
                    }else{

                    $customer_srtipe_id = $user->stripe_id;
                    $plan_srtipe_id = $CLMLicense->subscription_stripe_id;

                        $Subscription = Subscription::create(array(
                            'customer' =>$customer_srtipe_id,
                            'items' => [
                                ['price' => $plan_srtipe_id],
                            ]
                        ));


                    $payment['transaction_type'] = "Payment";
                    $payment['amount'] = $CLMLicense->subscription_amount;

                    }
                }catch(\Stripe\Exception\InvalidRequestException $e) {
                    $error = $e->getError()->message . '\n';
                    return "error";
                }
            }else{
                return "error";
            }

            if($Subscription){

                $payment['license_id'] = $CLMLicense->id;
                $payment['user_id'] = $CLMLicense->user_id;

                $payment['transaction_id'] = $Subscription['id'];
                $payment['latest_invoice'] = $Subscription['latest_invoice'];
                $payment['customer'] = $Subscription['customer'];
                $payment['cancel_at'] = $Subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $Subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $Subscription['canceled_at'];
                $payment['collection_method'] = $Subscription['collection_method'];
                $payment['created'] = $Subscription['created'];
                $payment['current_period_end'] = $Subscription['current_period_end'];
                $payment['current_period_start'] = $Subscription['current_period_start'];
                $payment['days_until_due'] = $Subscription['days_until_due'];
                $payment['default_payment_method'] = $Subscription['default_payment_method'];
                $payment['plan_id'] = $Subscription['plan']['id'];
                $payment['plan_currency'] = $Subscription['plan']['currency'];
                $payment['plan_interval'] = $Subscription['plan']['interval'];
                $payment['plan_interval_count'] = $Subscription['plan']['interval_count'];
                $payment['plan_product'] = $Subscription['plan']['product'];
                $payment['start_date'] = $Subscription['start_date'];
                $payment['status'] = $Subscription['status'];

                $pay_return = CLMPayments::create($payment);

                $CLMLicense->fill([
                    'subscription_status'=>'active',
                    'cancel_at_period_end' => false,
                    'current_period_end' => $Subscription['current_period_end']
                    ]);
                $CLMLicense->save();
                return 'success'; //response()->json(['data' => $Subscription, 'success' => 'Udated successfully!'], 200);
            }else{

                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);

            }

     }


     public function inActivateSubscription($license_id){


        $ret = CLMPayments::where('license_id', $license_id)
        ->orderBy('current_period_end', 'DESC')
        ->first();

        $CLMLicense = CLMLicense::where('id', $license_id)->first();


        ##SUBSCRIPTION PART
       $user = User::where('id',  $CLMLicense->user_id)->first();
       $srtipe_id = 0;
       $CLMPlans = CLMPlans::where('id', $CLMLicense->subscription_plan)->first();



       Stripe::setApiKey(config("services.stripe.secret"));
            if($user->stripe_id != ""){
                try{
                    if(isset($ret->transaction_id)){
                        $Subscription = Subscription::update(
                            $ret->transaction_id,
                            [
                                'cancel_at_period_end' => true,
                            ]
                        );
                    }
                }catch(\Stripe\Exception\InvalidRequestException $e) {
                    $error = $e->getError()->message . '\n';
                    return "error"; // response()->json( ["error"=>"error","status"=>"error", "msg"=> $error], 400);
                }
            }else{
                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);
            }

            if(isset($Subscription)){
                //print_r($Subscription);
                $payment['license_id'] = $CLMLicense->id;
                $payment['user_id'] = $CLMLicense->user_id;
                $payment['amount'] = 0;
                $payment['transaction_type'] = "cancellation_requested";

                $payment['transaction_id'] = $Subscription['id'];
                $payment['latest_invoice'] = $Subscription['latest_invoice'];
                $payment['customer'] = $Subscription['customer'];
                $payment['cancel_at'] = $Subscription['cancel_at'];
                $payment['cancel_at_period_end'] = $Subscription['cancel_at_period_end'];
                $payment['canceled_at'] = $Subscription['canceled_at'];
                $payment['collection_method'] = $Subscription['collection_method'];
                $payment['created'] = $Subscription['created'];
                $payment['current_period_end'] = $Subscription['current_period_end'];
                $payment['current_period_start'] = $Subscription['current_period_start'];
                $payment['days_until_due'] = $Subscription['days_until_due'];
                $payment['default_payment_method'] = $Subscription['default_payment_method'];
                $payment['plan_id'] = $Subscription['plan']['id'];
                $payment['plan_currency'] = $Subscription['plan']['currency'];
                $payment['plan_interval'] = $Subscription['plan']['interval'];
                $payment['plan_interval_count'] = $Subscription['plan']['interval_count'];
                $payment['plan_product'] = $Subscription['plan']['product'];
                $payment['start_date'] = $Subscription['start_date'];
                $payment['status'] = $Subscription['status'];

                $pay_return = CLMPayments::create($payment);

                $CLMLicense->fill([
                    'subscription_status'=>'active',
                    'cancel_at_period_end' => 1,
                    'current_period_end' => $Subscription['current_period_end']
                    ]);
                $CLMLicense->save();
                return 'success'; //response()->json(['data' => $Subscription, 'success' => 'Udated successfully!'], 200);
            }else{
                return "error"; //response()->json( ["error"=>"error","status"=>"error", "msg"=> 'Oops something went wrong'], 400);

            }

     }

    public function curntPlan(request $request) {
        $stripe_id = $request->input('stripe_id');
        $CLMLicense = CaretPricing::where('stripe_id_one_year', $stripe_id)
        ->orWhere('stripe_id_two_year', $stripe_id)
        ->orWhere('stripe_id_three_year', $stripe_id)
        ->first();
        if (!$CLMLicense) {
            return response()->json(['message' => 'No matching license found'], 404);
        }

        return response()->json(['data' => $CLMLicense->toArray(), 'message' => 'Record retrieved successfully!'], 200);

    }


     public function calculatePaymentDifference(Request $request) {
        // get current plan

        $data = $request->all();
        $user_id = trim($data['user_id']);
        $license_id = trim($data['license_id']);
        $new_plan_id = trim($data['new_plan_id']);
        $new_duration = trim($data['new_duration']);

        $lastPayment = CLMPayments::where('license_id', $license_id)
                        ->where('user_id', $user_id)
                        ->orderBy('created_at', 'desc')
                        ->first();
        if (!$lastPayment) return "No previous payment found for this license";

        // Get the current plan details from the payment and pricing tables
        $currentPlanId = $lastPayment->plan_id;
        //$currentPricing = CaretPricing::where('plan_id', $currentPlanId)->first();
        $currentPricing = CaretPricing::where('stripe_id_one_year', $currentPlanId)
        ->orWhere('stripe_id_two_year', $currentPlanId)
        ->orWhere('stripe_id_three_year', $currentPlanId)
        ->first();

        //$newPricing = CaretPricing::where('plan_id', $new_plan_id)->first();

        $newPricing = CaretPricing::where('stripe_id_one_year', $new_plan_id)
        ->orWhere('stripe_id_two_year', $new_plan_id)
        ->orWhere('stripe_id_three_year', $new_plan_id)
        ->first();

        if (!$currentPricing || !$newPricing) return "Pricing plan not found";

        // Retrieve the price based on duration for the current and new plans
        $currentDuration = (int) $lastPayment->plan_interval_count; // Convert interval to years
        $currentPrice = $this->getPriceByDuration($currentPricing, $currentDuration);
        $newPrice = $this->getPriceByDuration($newPricing, $new_duration);
        $costDifference = $newPrice - $currentPrice;
        // // Calculate remaining time based on the current period end
        // $currentPeriodEnd = Carbon::createFromTimestamp((int) $lastPayment->current_period_end);
        // $remainingDays = Carbon::now()->diffInDays($currentPeriodEnd, false);

        // // Recalculate remaining days based on the new duration
        // $remainingDurationRatio = $new_duration / $currentDuration;
        // $adjustedRemainingDays = $remainingDays * $remainingDurationRatio;

        // // Calculate daily rates for the current and new plans
        // $currentDailyRate = $currentPrice / ($currentDuration * 365);
        // $newDailyRate = $newPrice / ($new_duration * 365);

        // // Calculate the cost difference based on the adjusted remaining days
        // $costDifference = ($newDailyRate - $currentDailyRate) * $adjustedRemainingDays;

        return round($costDifference, 2);
    }

    // Helper function to get price based on subscription duration
    protected function getPriceByDuration($pricing, $duration) {
        switch ($duration) {
            case 1:
                return $pricing->one_year_license;
            case 2:
                return $pricing->two_year_license;
            case 3:
                return $pricing->three_year_license;
            default:
                return 0;
        }
    }

    public function changeSubscription(Request $request) {
        $data = $request->all();
        $license_id = trim($data['license_id']);
        $new_plan_id = trim($data['new_plan_id']);
        $new_plan_price = trim($data['new_plan_price']); // Updated variable name

        // Fetch the license and the related user
        $currentLicense = CLMLicense::where('id', $license_id)->first();
        if (!$currentLicense) return "License not found";

        $user = User::find($currentLicense->user_id);
        if (!$user || !$user->stripe_id) return "User or Stripe ID not found";

        // Fetch the new plan
        $newPricing = CaretPricing::where('stripe_id_one_year', $new_plan_id)
            ->orWhere('stripe_id_two_year', $new_plan_id)
            ->orWhere('stripe_id_three_year', $new_plan_id)
            ->first();

        if (!$newPricing) return "New Plan or Stripe ID not found";

        Stripe::setApiKey(config("services.stripe.secret"));

        try {
            // Check if user already has an active subscription
            $activeSubscription = CLMPayments::where('license_id', $license_id)
                ->orderBy('current_period_end', 'DESC')
                ->first();

            if ($activeSubscription && isset($activeSubscription->transaction_id)) {
                // Fetch the existing subscription
                $subscription = Subscription::retrieve($activeSubscription->transaction_id);

                // Determine the action based on the new plan price
                $transactionType = "";
                $message = "";
                $startDate = null;

                // Check if the plan is a downgrade
                if ($new_plan_price < $activeSubscription->amount) { // Downgrade
                    // Schedule downgrade for next period
                    $subscriptionItemId = $subscription->items->data[0]->id; // Get the existing subscription item ID

                    // Update the existing subscription item
                    $subscription = Subscription::update($activeSubscription->transaction_id, [
                        'items' => [
                            [
                                'id' => $subscriptionItemId, // Existing item ID
                                'price' => $new_plan_id
                            ]
                        ],
                        'cancel_at_period_end' => true, // Downgrade will take effect at the end of the current period
                    ]);
                    $transactionType = "Scheduled Downgrade";
                    $startDate = Carbon::createFromTimestamp($subscription->current_period_end);
                    $message = "The plan will be downgraded at the end of the current period, effective from " . $startDate->toDateString();
                } else { // Upgrade
                    // Immediate upgrade with proration
                    $subscriptionItemId = $subscription->items->data[0]->id; // Get the existing subscription item ID

                    // Update the existing subscription item
                    $subscription = Subscription::update($activeSubscription->transaction_id, [
                        'items' => [
                            [
                                'id' => $subscriptionItemId, // Existing item ID
                                'price' => $new_plan_id
                            ]
                        ],
                        'cancel_at_period_end' => false, // Immediate upgrade
                        'proration_behavior' => 'create_prorations', // Enable proration on upgrade
                    ]);
                    $transactionType = "Immediate Plan Change";
                    $message = "Subscription change successful";
                }

                // Save payment data for the scheduled downgrade or upgrade

                 // Retrieve the latest invoice amount charged
                $latestInvoice = Invoice::retrieve($subscription->latest_invoice);
                $amountCharged = $latestInvoice->amount_paid; // Amount charged in cents


                CLMPayments::create([
                    'license_id' => $license_id,
                    'user_id' => $user->id,
                    'transaction_id' => $subscription->id,
                    'amount' => $amountCharged / 100,
                    'transaction_type' => $transactionType,
                    'scheduled_start_date' => $startDate ?? null,
                    'status' => $subscription->status,
                    'latest_invoice' => $subscription->latest_invoice,
                    'customer' => $subscription->customer,
                    'cancel_at' => $subscription->cancel_at,
                    'cancel_at_period_end' => $subscription->cancel_at_period_end,
                    'canceled_at' => $subscription->canceled_at,
                    'collection_method' => $subscription->collection_method,
                    'created' => $subscription->created,
                    'current_period_end' => $subscription->current_period_end,
                    'current_period_start' => $subscription->current_period_start,
                    'days_until_due' => $subscription->days_until_due,
                    'default_payment_method' => $subscription->default_payment_method,
                ]);


                // Update license
                $currentLicense->update([
                    'subscription_plan' => $newPricing->plan_id,
                    'subscription_amount' => $new_plan_price,
                    'subscription_status' => 'active',
                    'current_period_end' => $subscription->current_period_end,
                    'cancel_at_period_end' => isset($startDate) ? true : false,
                ]);

                return $message;

            } else {
                return "No active subscription found for downgrade.";
            }

        } catch (\Exception $e) {
            Log::error("Subscription change failed: " . $e->getMessage());
            return "Error changing subscription";
        }
    }



     public function listSubscription(Request $request){
        Stripe::setApiKey(config("services.stripe.secret"));
        return $Subscription = Subscription::all(['status'=>'active']);
     }




     public function getUsers(Request $request){
        $filter = $request->input('filter');
        $users = User::orderBy('id','DESC')
        ->select('id as value', 'email as label')
        ->where(function($q) use ($filter){
            $q->where('email','like', '%' . $filter . '%');
        })
        ->whereHas(
            'roles', function($q){
                $q->where('name', 'clm');
            }
        )
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $users, 'Record retrieved successfully!');
    }





    public function getStatus(Request $request){
        $filter = $request->input('filter');
        $AdStatus = AdStatus::orderBy('id','DESC')
        ->select('id as value', 'title as label')
        ->where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $AdStatus, 'Record retrieved successfully!');
    }

    public function getPlans(Request $request){
        $filter = $request->input('filter');
        $CLMPlans = CLMPlans::orderBy('id','DESC')
        ->select('*','id as value', 'title as label')
        ->where(function($q) use ($filter){
            $q->where('title','like', '%' . $filter . '%');
        })
        ->where('is_active',1)
        ->limit(50)
        ->get();
        return $this->sendSuccessResponse('data', $CLMPlans, 'Record retrieved successfully!');
    }

    public function webhooks(Request $request){
        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        switch ($event->type) {
            case 'invoice.payment_failed':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                ->orderBy('transaction_id', 'DESC')
                ->first();

                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $AdPayments->campaign_ad_id)->first();

                    $CampaignsAds->fill([
                    'cancel_at_period_end' => true
                    ]);
                    $CampaignsAds->save();
            break;
            case 'customer.subscription.deleted':
                $data = $event->data->object; // contains a \Stripe\PaymentIntent
                $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                ->orderBy('transaction_id', 'DESC')
                ->first();

                $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                    $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                    DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                }])->where('id', $AdPayments->campaign_ad_id)->first();

                    $CampaignsAds->fill([
                    'cancel_at_period_end' => true
                    ]);
                    $CampaignsAds->save();
            break;
            case 'invoice.payment_succeeded':
                    $data = $event->data->object; // contains a \Stripe\PaymentIntent
                        $AdPayments = AdPayments::where('campaign_ad_id', $data->id)
                        ->orderBy('transaction_id', 'DESC')
                        ->first();

                        $CampaignsAds = CampaignsAds::with(['ad'=>function($q){
                            $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"),
                            DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")]);
                        }])->where('id', $AdPayments->campaign_ad_id)->first();


                        $payment['campaign_ad_id'] = $CampaignsAds->id;
                        $payment['user_id'] = $AdPayments->user_id;

                        $payment['transaction_id'] = $data->id;
                        $payment['latest_invoice'] = $data->latest_invoice;
                        $payment['customer'] = $data->customer;
                        $payment['cancel_at'] = $data->cancel_at;
                        $payment['cancel_at_period_end'] = $data->cancel_at_period_end;
                        $payment['canceled_at'] = $data->canceled_at;
                        $payment['collection_method'] = $data->collection_method;
                        $payment['created'] = $data->created;
                        $payment['current_period_end'] = $data->current_period_end;
                        $payment['current_period_start'] = $data->current_period_start;
                        $payment['days_until_due'] = $data->days_until_due;
                        $payment['default_payment_method'] = $data->default_payment_method;
                        $payment['plan_id'] = $data->plan->id;
                        $payment['plan_currency'] = $data->plan->currency;
                        $payment['plan_interval'] = $data->plan->interval;
                        $payment['plan_interval_count'] = $data->plan->interval_count;
                        $payment['plan_product'] = $data->plan->product;
                        $payment['start_date'] = $data->start_date;
                        $payment['status'] = $data->status;

                        $pay_return = AdPayments::create($payment);

                        $CampaignsAds->fill([
                            'subscription_status'=>'active',
                            'cancel_at_period_end' => false,
                            'current_period_end' => $data->current_period_end
                            ]);
                        $CampaignsAds->save();
                break;
            // case 'payment_method.attached':
            //     $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
            //     // Then define and call a method to handle the successful attachment of a PaymentMethod.
            //     // handlePaymentMethodAttached($paymentMethod);
            //     break;
            // ... handle other event types
            default:
                // echo 'Received unknown event type ' . $event->type;
        }

        http_response_code(200);
     }


     public function uploadLogo(Request $request)
     {
         $clmLicense = CLMLicense::find($request->license_id);
         if($clmLicense){
             $imageData = $request->input('image');
             //$user_id = $request->user_id;
             $file = $imageData['file'];
             $file_name = $imageData['name'];
             $image = file_get_contents($file);
             $file_path =  'Logos/' . $request->license_id.time() . '.png';
             Helper::putServerImage($file_path, $image, 'public');
             $confirmUpload = Helper::putS3Image($file_path, $image, 'public');
             if ($confirmUpload){
                 $clmLicense->caret_logo = $file_path;
                 $clmLicense->save();
             }
         }else{
             return response()->json([ "status" => "error", "message" => "License not found"], 404);
         }
     }

########################################################################################################


public function checkCaretAvailability(Request $request)
    {
        $keyword = $request->input('keyword');
        $keyword = str_replace(['^', '#'], '', $keyword);
        $classification = $request->input('classification');

        // Categorize the keyword
        $planType = $this->categorizeKeyword($classification);
        $reqData = $request->all();
        // Fetch pricing based on plan type from the database
        $pricing = $this->clmPricingAlgo($reqData);
        $sujPricing = $this->suggestionsPricingAlgo();

        // Check if the keyword is already purchased
        $keywordStatus = CLMLicense::where('caret_title', $keyword)->exists()
                        ||CLMRequest::where('caret_title', $keyword)->exists();

        $return = [];

        if (!$keywordStatus) {


            $return = [
                'status' => 'available',
                'keyword' => $keyword,
                'message' => 'The keyword is available.',
                'price' => $pricing,
                'plan_type' => $planType
            ];
        } else {
            $return = [
                'status' => 'unavailable',
                'message' => 'The keyword is already purchased.'
            ];
        }

        // If purchased, generate similar keyword suggestions
        $suggestions = $this->generateKeywordSuggestions($keyword);

        // Filter out already purchased keywords using a database query
        $purchasedKeywords = CLMLicense::whereIn('caret_title', $suggestions)
            ->pluck('caret_title')
            ->toArray();

        // Remove purchased keywords from suggestions
        $availableSuggestions = array_diff($suggestions, $purchasedKeywords);

        $topSuggestion = $availableSuggestions ? array_shift($availableSuggestions) : null;

        $topSuggestionWithPlans = [
            'keyword' => $topSuggestion,
            'plan_type' => $this->categorizeKeyword($topSuggestion),
             'price' => $sujPricing ?? null
        ];
        // Get the plan types and prices for the available suggestions
        $suggestionsWithPlans = array_map(function($suggestion) use ($sujPricing) {
            $planType = $this->categorizeKeyword($suggestion);
            return [
                'keyword' => $suggestion,
                'plan_type' => $planType,
                'price' => $sujPricing[0] ?? null // Handle case where plan type may not exist
            ];
        }, $availableSuggestions);

        $return['topSuggestion'] = $topSuggestionWithPlans;
        $return['suggestions'] = $suggestionsWithPlans;

        return response()->json($return);
    }

    private function categorizeKeyword($classification)
    {
        // $wordCount = str_word_count($keyword);
        $classificationLower = strtolower($classification);
        if ($classificationLower == "corporate") {
            return 'corporate';
        } elseif ($classificationLower == "influencer") {
            return 'influencer';
        } elseif ($classificationLower == "individual") {
            return 'individual';
        } else {
            return 'individual';
        }
    }

    private function generateKeywordSuggestions($keyword)
    {
        // More sophisticated logic to generate keyword suggestions
        $suggestions = [];
        $prefixes = ['best', 'top', 'my', 'your', 'new', 'super'];
        $suffixes = ['online', 'pro', 'expert', 'hub', 'world', 'zone'];

        // Add combinations of prefixes and suffixes
        foreach ($prefixes as $prefix) {
            $suggestions[] = $prefix . $keyword;
        }

        foreach ($suffixes as $suffix) {
            $suggestions[] = $keyword . $suffix;
        }

        // Include combinations of both
        foreach ($prefixes as $prefix) {
            foreach ($suffixes as $suffix) {
                $suggestions[] = $prefix . $keyword . $suffix;
            }
        }

        return $suggestions;
    }

    public function clmPricingAlgo($data)
    {

        // $keyword = $data['keyword'] ?? null;
        $classification = $data['classification'] ?? null;
        $networth = $data['networth'] ?? null;
        $instagram = $data['instagram'] ?? null;
        $twitter = $data['twitter'] ?? null;
        $googleSearches = $data['GoogleSearchVolume'] ?? null;

        $networthRange = explode('-', $networth);
        $instagramRange = explode('-', $instagram);
        $twitterRange = explode('-', $twitter);
        $googleSearchesRange = explode('-', $googleSearches);

        // Set default min values and check for max values
        $networthMin = isset($networthRange[0]) ? $this->convertToNumeric($networthRange[0]) : 0;
        $networthMax = isset($networthRange[1]) ? $this->convertToNumeric($networthRange[1]) : null;

        $instagramMin = isset($instagramRange[0]) ? $this->convertToNumeric($instagramRange[0]) : 0;
        $instagramMax = isset($instagramRange[1]) ? $this->convertToNumeric($instagramRange[1]) : null;

        $twitterMin = isset($twitterRange[0]) ? $this->convertToNumeric($twitterRange[0]) : 0;
        $twitterMax = isset($twitterRange[1]) ? $this->convertToNumeric($twitterRange[1]) : null;

        $googleSearchesMin = isset($googleSearchesRange[0]) ? $this->convertToNumeric($googleSearchesRange[0]) : 0;
        $googleSearchesMax = isset($googleSearchesRange[1]) ? $this->convertToNumeric($googleSearchesRange[1]) : null;

        // Perform the query using the min values and checking for null in the max values
        $pricing = CaretPricing::where('title', $classification)
            ->where('is_active', '1')
            ->where(function ($query) use ($networthMin, $networthMax, $instagramMin, $instagramMax, $twitterMin, $twitterMax, $googleSearchesMin, $googleSearchesMax) {
                // Networth condition
                $query->where(function ($q) use ($networthMin, $networthMax) {
                    $q->where('networth', '>=', $networthMin);
                    if (!is_null($networthMax)) {
                        $q->where('networth', '<=', $networthMax);
                    }
                });

                // Instagram followers condition
                $query->orWhere(function ($q) use ($instagramMin, $instagramMax) {
                    $q->where('instagram_followers', '>=', $instagramMin);
                    if (!is_null($instagramMax)) {
                        $q->where('instagram_followers', '<=', $instagramMax);
                    }
                });

                // Twitter followers condition
                $query->orWhere(function ($q) use ($twitterMin, $twitterMax) {
                    $q->where('twitter_followers', '>=', $twitterMin);
                    if (!is_null($twitterMax)) {
                        $q->where('twitter_followers', '<=', $twitterMax);
                    }
                });

                // Google searches condition
                $query->orWhere(function ($q) use ($googleSearchesMin, $googleSearchesMax) {
                    $q->where('google_searches', '>=', $googleSearchesMin);
                    if (!is_null($googleSearchesMax)) {
                        $q->where('google_searches', '<=', $googleSearchesMax);
                    }
                });
            })
            ->orderBy('one_year_license', 'desc')  // Order by one_year_license in descending order
            //->groupBy('title')  // Group by the title column
            ->get();
        return $pricing;
    }

    public function suggestionsPricingAlgo()
    {

        // $keyword = $data['keyword'] ?? null;
        $classification = 'individual'; // Static value for classification
        $networth = '0';  // Static value for net worth
        $instagram = '0';  // Static value for Instagram followers
        $twitter = '0';  // Static value for Twitter followers
        $googleSearches = '0';  // Static value for Google searches

        $networthRange = explode('-', $networth);
        $instagramRange = explode('-', $instagram);
        $twitterRange = explode('-', $twitter);
        $googleSearchesRange = explode('-', $googleSearches);

        // Set default min values and check for max values
        $networthMin = isset($networthRange[0]) ? $this->convertToNumeric($networthRange[0]) : 0;
        $networthMax = isset($networthRange[1]) ? $this->convertToNumeric($networthRange[1]) : null;

        $instagramMin = isset($instagramRange[0]) ? $this->convertToNumeric($instagramRange[0]) : 0;
        $instagramMax = isset($instagramRange[1]) ? $this->convertToNumeric($instagramRange[1]) : null;

        $twitterMin = isset($twitterRange[0]) ? $this->convertToNumeric($twitterRange[0]) : 0;
        $twitterMax = isset($twitterRange[1]) ? $this->convertToNumeric($twitterRange[1]) : null;

        $googleSearchesMin = isset($googleSearchesRange[0]) ? $this->convertToNumeric($googleSearchesRange[0]) : 0;
        $googleSearchesMax = isset($googleSearchesRange[1]) ? $this->convertToNumeric($googleSearchesRange[1]) : null;

        // Perform the query using the min values and checking for null in the max values
        $pricing = CaretPricing::where('title', $classification)
            ->where('is_active', '1')
            ->where(function ($query) use ($networthMin, $networthMax, $instagramMin, $instagramMax, $twitterMin, $twitterMax, $googleSearchesMin, $googleSearchesMax) {
                // Networth condition
                $query->where(function ($q) use ($networthMin, $networthMax) {
                    $q->where('networth', '>=', $networthMin);
                    if (!is_null($networthMax)) {
                        $q->where('networth', '<=', $networthMax);
                    }
                });

                // Instagram followers condition
                $query->orWhere(function ($q) use ($instagramMin, $instagramMax) {
                    $q->where('instagram_followers', '>=', $instagramMin);
                    if (!is_null($instagramMax)) {
                        $q->where('instagram_followers', '<=', $instagramMax);
                    }
                });

                // Twitter followers condition
                $query->orWhere(function ($q) use ($twitterMin, $twitterMax) {
                    $q->where('twitter_followers', '>=', $twitterMin);
                    if (!is_null($twitterMax)) {
                        $q->where('twitter_followers', '<=', $twitterMax);
                    }
                });

                // Google searches condition
                $query->orWhere(function ($q) use ($googleSearchesMin, $googleSearchesMax) {
                    $q->where('google_searches', '>=', $googleSearchesMin);
                    if (!is_null($googleSearchesMax)) {
                        $q->where('google_searches', '<=', $googleSearchesMax);
                    }
                });
            })
            ->orderBy('one_year_license', 'desc')  // Order by one_year_license in descending order
            //->groupBy('title')  // Group by the title column
            ->get();
        return $pricing;
    }

public function convertToNumeric($value)
{
    $value = strtoupper(str_replace(['$', '<', '>', ' searches per month'], '', trim($value)));
        if (strpos($value, '-') !== false) {
        $values = explode('-', $value);

        if (count($values) === 2) {
            $minValue = $this->conversion(trim($values[0]));
            $maxValue = $this->conversion(trim($values[1]));

            return "{$minValue}-{$maxValue}";
        }
    }

    // Convert a single value to numeric
    return $this->conversion($value);
}
public function conversion($value)
{

    if (strpos($value, 'B') !== false) {
        $number = floatval(str_replace('B', '', $value));
        return $number * 1000000000;
    }

    if (strpos($value, 'M') !== false) {
        $number = floatval(str_replace('M', '', $value));
        return $number * 1000000;
    }

    if (strpos($value, 'K') !== false) {
        $number = floatval(str_replace('K', '', $value));
        return $number * 1000;
    }

    return floatval($value);
}

    private function getPricing()
    {
        // Fetch the pricing plans from the database
        $plans = CLMPlans::where('is_active', true)->get();

        // Map the plans to a format where key is plan type
        $pricing = [];
        foreach ($plans as $plan) {
            if ($plan->title === 'individual') {
                $pricing['individual'] = $plan->amount;
            } elseif ($plan->title === 'influencer') {
                $pricing['influencer'] = $plan->amount;
            } elseif ($plan->title === 'corporate') {
                $pricing['corporate'] = $plan->amount;
            }
        }

        return $pricing;
    }

#########################################################################################################


}
