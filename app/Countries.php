<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
    use SoftDeletes;
    protected $table = 'countries';
    protected $fillable = ['id','name', 'code2', 'code3'];
}
