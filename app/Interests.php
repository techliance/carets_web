<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interests extends Model
{
    use SoftDeletes;
    use PaginationWithHavings;
    protected $table = 'user_interests';
    protected $fillable = ['id','user_id','cat_id','deleted_at'];


}
