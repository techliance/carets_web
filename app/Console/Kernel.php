<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Jobs\TagListingJob;
use App\Jobs\DistanceListingJob;
use App\Jobs\ConcatenateVideosJob;
use App\Jobs\ProcessMp4VideosJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // \Laravelista\LumenVendorPublish\VendorPublishCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$queue_name = config('queue.queue_name'); // Getting queue name from the config
        ######GPU COMMANDS####################
        // Schedule TagListingJob
        if(env('IS_GPU') == true){

            // $schedule->call(function ()  {
            //     $queueName = env('AUTO_CARET_QUEUE', 'default'); // Default to 'default' if not set
            //     dispatch(new TagListingJob())->onQueue($queueName); // Dispatch on dynamic queue
            // })->everyFiveMinutes();

            //  // Schedule ConcatenateVideosJob
            //  $schedule->call(function () {
            //     $queueName = env('AUTO_CARET_QUEUE', 'default'); // Default to 'default' if not set
            //     dispatch(new ConcatenateVideosJob())->onQueue($queueName); // Dispatch on specified queue
            // })->everyMinute();


            //  // Schedule ConcatenateVideosJob
            //  $schedule->call(function () {
            //     $queueName = env('AUTO_CARET_QUEUE', 'default'); // Default to 'default' if not set
            //     dispatch(new ProcessMp4VideosJob())->onQueue($queueName); // Dispatch on specified queue
            // })->everyMinute();

        } else {

            $schedule->call(function ()  {
                $queueName = env('AUTO_CARET_QUEUE', 'default'); // Default to 'default' if not set
                dispatch(new TagListingJob())->onQueue($queueName); // Dispatch on dynamic queue
            })->everyFiveMinutes();

             // Schedule ConcatenateVideosJob
             $schedule->call(function () {
                $queueName = env('AUTO_CARET_QUEUE', 'default'); // Default to 'default' if not set
                dispatch(new ConcatenateVideosJob())->onQueue($queueName); // Dispatch on specified queue
            })->everyMinute();


             // Schedule ConcatenateVideosJob
             $schedule->call(function () {
                $queueName = env('AUTO_CARET_QUEUE', 'default'); // Default to 'default' if not set
                dispatch(new ProcessMp4VideosJob())->onQueue($queueName); // Dispatch on specified queue
            })->everyMinute();

        // Schedule DistanceListingJob
            $schedule->call(function () {
                $queueName = env('UPLOAD_VIDEO_QUEUE', 'default'); // Default to 'default' if not set
                dispatch(new DistanceListingJob())->onQueue($queueName); // Dispatch on specified queue
            })->everyFiveMinutes();
        }
        // Schedule deleteVideos action
        $schedule->call('App\Http\Controllers\VideoController@deleteVideos')->daily(); // Direct call to controller method for daily video deletion
        $schedule->call('App\Http\Controllers\FileUploadChunksController@deleteOldChunks')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
