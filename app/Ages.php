<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ages extends Model
{
    use SoftDeletes;
    protected $table = 'ages';
    protected $fillable = ['id','min', 'max', 'title'];
}
