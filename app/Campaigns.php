<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaigns extends Model
{

    use SoftDeletes;
    protected $table = 'campaigns';
    protected $fillable = ['id','user_id','campaign_title','is_active','is_blocked','deleted_at'];

    public function details()
    {
        return $this->hasOne(\App\CampaignDetails::class, 'campaign_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    // ads
    public function campaignAds()
    {
        return $this->hasMany(\App\CampaignsAds::class, 'campaign_id', 'id');
    }

    public function campaignViews()
    {
        return $this->hasMany(\App\AdViews::class, 'campaign_id', 'id');
    }

    public function genderStats()
    {
        return $this->hasMany(\App\AdViews::class, 'campaign_id', 'id');
    }

    public function ageStats()
    {
        return $this->hasMany(\App\AdViews::class, 'campaign_id', 'id');
    }

    public function positionStats()
    {
        return $this->hasMany(\App\AdViews::class, 'campaign_id', 'id');
    }






    public function ads()
    {
        return $this->belongsToMany('App\Ads', 'campaigns_ads', 'campaign_id', 'ad_id');
    }


}
