<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaretSearchData extends Model
{
    use SoftDeletes;
    use PaginationWithHavings;
    protected $table = 'caret_search_data';
    protected $fillable = ['id','keyword','classification','net_worth','google_search_volume','instagram','twitter','created_at','updated_at','deleted_at',];


}
