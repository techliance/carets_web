<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdStatus extends Model
{
    use SoftDeletes;
    protected $table = 'ad_status';
    protected $fillable = ['id','title','deleted_at'];
}
