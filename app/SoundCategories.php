<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoundCategories extends Model
{
    use SoftDeletes;
    protected $table = 'sound_categories';
    protected $fillable = ['id','title', 'is_active','deleted_at'];
}
