<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUpload  extends Model
{
    protected $table = 'file_uploads';
    protected $fillable = [
        'unique_id',
        'file_name',
        'total_chunks',
        'received_chunks',
        'status',
    ];


}
