<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ads extends Model
{
    use SoftDeletes;
    use PaginationWithHavings;
    protected $table = 'ads';
    protected $fillable = ['id','user_id','license_id','ad_title','is_caret','video_url','image_url','ad_share_link','ad_description','is_active','watch_count','deleted_at','is_blocked',
    'ad_button_link','ad_button_text','duration'];


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function license()
    {
        return $this->hasMany('App\CLMLicense', 'id', 'license_id');
    }


    public function getVideoList($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return Videos::with(['users'])
        ->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url")])
        ->where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                    ->orWhere('video_description','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);
    }

    //$ads = Ad::with(['user','photos'])->distance($from_latitude,$from_longitude,$distance)->get();


    public function ScopeDistance($query,$from_latitude,$from_longitude,$distance)
    {
        // This will calculate the distance in km
        // if you want in miles use 3959 instead of 6371
        $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$from_latitude.') ) * cos( radians( loc_lat ) ) * cos( radians( loc_long ) - radians('.$from_longitude.') ) + sin( radians('.$from_latitude.') ) * sin( radians( loc_lat ) ) ) ) ) AS distance');
        return $query->addSelect(DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"))->addSelect(DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))->addSelect($raw)
        ->orderBy( 'distance', 'ASC' )
        ->having('distance', '<=', $distance)
        ;
    }

    public function ScopeDistance2($query,$from_latitude,$from_longitude,$distance)
    {
        // This will calculate the distance in km
        // if you want in miles use 3959 instead of 6371
        $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$from_latitude.') ) * cos( radians( loc_lat ) ) * cos( radians( loc_long ) - radians('.$from_longitude.') ) + sin( radians('.$from_latitude.') ) * sin( radians( loc_lat ) ) ) ) ) AS distance');
        return $query->addSelect($raw)
        ->orderBy( 'distance', 'ASC' )
        ->having('distance', '<=', $distance)
        ;
    }

}
