<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CLMOrderPayments extends Model
{
    protected $table = 'clm_order_payments';
    protected $fillable = [
        'order_id',
        'payment_method',
        'transaction_id',
        'amount',
        'status',
    ];
}
