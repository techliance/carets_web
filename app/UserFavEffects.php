<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavEffects extends Model
{
    protected $table = 'user_fav_effects';
    protected $fillable = ['id','user_id', 'effect_id','deleted_at'];
}
