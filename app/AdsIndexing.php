<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsIndexing extends Model
{
    protected $table = 'ads_indexing';
    protected $fillable = ['id','ad_id', 'campaign_ad_id','video_url','image_url','age_range','gender','position','location','description'];

    public function addata()
    {
        return $this->hasOne('App\Ads', 'id', 'ad_id');
    }

}
