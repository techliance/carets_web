<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdViews extends Model
{
    protected $table = 'ad_views';
    protected $fillable = ['id','ad_id', 'user_id','campaign_id',
    'position', 'age', 'gender', 'location', 'interests', 'complete', 'clicked',
    'deleted_at'];
}
