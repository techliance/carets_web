<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CustomResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = env('APP_FRONT_END_URL_PRODUCTION', 'https://admin.carets.tv') . "/resetPassword/" . $this->token."?email=".$notifiable->email;
        //$url = env('APP_FRONT_END_URL_PRODUCTION', 'http://devadmin.carets.tv') "http://localhost:3000/resetPassword/" . $this->token."?username=".$notifiable->email;
        return (new MailMessage)
            ->greeting('Reset Password')
            ->subject("Reset Password - " . env('APP_NAME'))
            ->line('Reset your password by clicking button below.')
            ->action('Reset Password', url($url))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
