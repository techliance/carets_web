<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CorporateRequestNotification extends Notification
{
    use Queueable;
    public $CLMRequest;


    /**
     * Create a new notification instance.
     * @param  $CLMRequest
     * @return void
     */
    public function __construct($CLMRequest)
    {
        $this->CLMRequest = $CLMRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->greeting('Hello,')
        ->subject('New ^Caret Request - ' . env('APP_NAME'))
        ->line('We are pleased to inform you that a new ^Caret request has been submitted. Here are the details:')
        ->line('**Caret Title:** ' . $this->CLMRequest->caret_title)
        ->line('**Company Name:** ' . $this->CLMRequest->company_name)
        ->line('**Company Address:** ' . ($this->CLMRequest->company_address ?? 'N/A'))
        ->line('**Company Description:** ' . ($this->CLMRequest->company_description ?? 'N/A'))
        ->line('Please review the request and take the necessary action.')
        ->line('Thank you for your continued support!')
        ->salutation('Best Regards,')
        ->salutation(env('APP_NAME') . ' Team');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
