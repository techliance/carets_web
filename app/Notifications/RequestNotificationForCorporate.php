<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestNotificationForCorporate extends Notification
{
    use Queueable;
    public $CLMRequests;


    /**
     * Create a new notification instance.
     * @param  $CLMRequest
     * @return void
     */
    public function __construct($CLMRequests)
    {
        $this->CLMRequests = $CLMRequests;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        $url = env('APP_FRONT_END_URL_PRODUCTION', 'http://devadmin.carets.tv') . "/CLM/orders/";

        // Initialize the message
        $mailMessage = (new MailMessage)
            ->greeting('Hello,')
            ->subject('New Corporate Subscription Request - ' . env('APP_NAME'))
            ->line('We are excited to inform you that new corporate subscription requests have been created. Below are the details:');

        // Add details for each request
        foreach ($this->CLMRequests as $request) {
            $mailMessage->line('---')
                ->line('**Caret Title:** ' . $request->caret_title)
                ->line('**Subscription Amount:** $' . number_format($request->subscription_amount, 2));
        }

        // Add action button and closing lines
        $mailMessage->line('Please review the requests at your earliest convenience.')
            ->action('View Request Details', url($url))
            ->line('Thank you for partnering with us to make our platform successful!')
            ->salutation('Best Regards,')
            ->salutation(env('APP_NAME') . ' Team');

        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
