<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestStatusNotificationForCorporate extends Notification
{
    use Queueable;
    public $CLMRequest;


    /**
     * Create a new notification instance.
     * @param  $CLMRequest
     * @return void
     */
    public function __construct($CLMRequest)
    {
        $this->CLMRequest = $CLMRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = env('APP_FRONT_END_URL_PRODUCTION', 'http://devadmin.carets.tv') . "/CLM-login/";

        // return (new MailMessage)
        // ->greeting('Subscription Request')
        // ->subject('Subscription Request Notification - ' . env('APP_NAME'))
        // ->line('A new corporate request has been created with the following details:')
        // ->line('Caret Title: ' . $this->CLMRequest->caret_title)
        // ->line('Subscription amount: ' . $this->CLMRequest->subscription_amount)
        // ->action('Click Here', url($url))
        // ->line('Thank you for using our application!');



        return (new MailMessage)
        ->greeting('Hello,')
        ->subject('Your Corporate Subscription Request Status Update - ' . env('APP_NAME'))
        ->line('We want to inform you that the status of your corporate subscription request has been updated. Below are the details:')
        ->line('**Caret Title:** ' . $this->CLMRequest->caret_title)
        ->line('**Updated Status:** ' . $this->CLMRequest->statusA->title)
        ->line('You can log in to your account to view more details about your request.')
        ->line('Thank you for choosing our platform! We appreciate your partnership.')
        ->salutation('Best Regards,')
        ->salutation(env('APP_NAME') . ' Team');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
