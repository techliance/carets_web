<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Sounds extends Model
{
    use SoftDeletes;
    protected $table = 'sounds';
    protected $fillable = ['id','sound_title', 'license_id', 'sound_url','image_url','sound_description','is_active','deleted_at','video_count','user_id'];

    public function categories()
    {
        return $this->belongsToMany('App\SoundCategories', 'sound_to_categories', 'sound_id', 'cat_id');
    }

    public function license()
    {
        return $this->hasMany('App\CLMLicense', 'id', 'license_id');
    }



    public function getCategoriesList($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return SoundCategories::where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                    ->orWhere('title','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);
    }


    public function getMusicList($user_id,$pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return Sounds::with('license')
            ->where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                ->orWhere('sound_title','like', '%' . $filter . '%')
                ->orWhere('license_id','like', '%' . $filter . '%');

            })
            ->select(['*',
                DB::raw("CONCAT('".config('app.awsurl')."', sound_url) AS sound_url"),
                DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url")
            ])
            ->when($user_id, function($query) use ($user_id){
                $query->where('user_id', $user_id);
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);
    }

}
