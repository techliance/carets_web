<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchVideoByLike extends Model
{
    protected $table = 'search_video_like';
    protected $fillable = ['id','video_id', 'age'];
}
