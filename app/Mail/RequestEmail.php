<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $subject;
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $subject = null, $msg = null)
    {
        $this->data = $data;
        $this->subject = $subject ?? 'New Request Submitted';
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.person.request')
            ->subject($this->subject)
                ->with([
                    'data' => $this->data,
                    'msg' => $this->msg
                ]);
    }
}
