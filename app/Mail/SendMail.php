<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $subject;
     /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $subject = null)
    {
        $this->data = $data;
        $this->subject = $subject ?? 'Welcome to Carets';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.register')
            ->subject($this->subject)
                ->with([
                    'data' => $this->data
                ]);
    }

}

?>
