<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelHasPermission extends Model {

    /**
     * Generated
     */

    protected $table = 'model_has_permissions';
    protected $fillable = ['permission_id', 'model_type', 'model_id'];


    public function permission() {
        return $this->belongsTo(\App\Permission::class, 'permission_id', 'id');
    }


}
