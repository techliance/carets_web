<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoDisplayTo extends Model
{
    protected $table = 'video_display_to';
    protected $fillable = ['id','title', 'is_active','deleted_at'];
}
