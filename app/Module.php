<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table='modules';
    protected $fillable=['name'];

    public static $rules = [
        'name' => 'required|string'
    ];

    public function permissions()
    {
        return $this->hasMany('App\Permission');
    }
}
