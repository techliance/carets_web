<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoReports extends Model
{
    protected $table = 'video_reports';
    protected $fillable = ['id','video_id', 'report_by','remarks','deleted_at'];

    public function reportby()
    {
        return $this->hasOne('App\User', 'id', 'report_by');
    }
}
