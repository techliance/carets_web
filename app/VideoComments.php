<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoComments extends Model
{
    protected $table = 'video_comments';
    protected $fillable = ['id','video_id', 'user_id','parent_id','comments','like_count','deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function reply()
    {
        return $this->hasMany('App\VideoComments', 'parent_id', 'id');
    }

    public function likedme()
    {
        return $this->hasOne('App\VideoCommentLikes', 'comment_id', 'id');
    }

}
