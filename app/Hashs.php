<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hashs extends Model
{
    use SoftDeletes;
    protected $table = 'hashs';
    protected $fillable = ['id','title', 'category_id', 'is_active', 'deleted_at'];

    public function hashCategories()
    {
        return $this->hasOne('App\HashCategory', 'id', 'category_id', 'title');
    }

    public function videos()
    {
        return $this->belongsToMany('App\Videos', 'hashs_videos', 'hash_id', 'video_id')->withTimestamps();
    }

}
