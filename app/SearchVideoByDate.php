<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchVideoByDate extends Model
{
    protected $table = 'search_video_created';
    protected $fillable = ['id','video_id', 'age'];

    public function hashs()
    {
        return $this->belongsToMany('App\Hashs', 'hashs_videos', 'video_id', 'hash_id');
    }

}
