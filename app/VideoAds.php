<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoAds extends Model
{
    protected $table = 'video_ads';
    protected $fillable = ['id','video_id', 'ad_id','deleted_at'];

}
