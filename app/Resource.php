<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Resource extends Model
{

    use SoftDeletes,LogsActivity;
    /**
     * Generated
     */

    protected $table = 'resources';
    protected $fillable = ['id', 'mime_type', 'extension', 'model_type', 'model_id', 'resource_type_id', 'file_path', 'file_title', 'original_file_name', 'file_size', 'status', 'deleted_at'];

    protected static $logAttributes = ['*'];

    /**
     * Get all of the accounts for this record.
     */
    public function accounts()
    {
        return $this->morphToMany(\App\Account::class, 'accountable')->withTimestamps();
    }

    public function agencyName()
    {
        return $this->belongsTo(\App\Agency::class, 'model_id', 'id');
    }

    public function accountName()
    {
        return $this->belongsTo(\App\Account::class, 'model_id', 'id');
    }

    public function resourceType()
    {
        return $this->belongsTo(\App\ResourceType::class, 'resource_type_id', 'id');
    }

    public function getFilePathAttribute($value)
    {
        return config('app.awsurl').$value;
    }
}
