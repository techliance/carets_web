<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaretPricing extends Model
{
    #use SoftDeletes;
    use SoftDeletes;
    protected $table = 'caret_pricing';
    protected $fillable = [
        'title',
        'plan_id',
        'stripe_id',
        'stripe_subscription_id',
        'networth',
        'instagram_followers',
        'twitter_followers',
        'google_searches',
        'one_year_license',
        'two_year_license',
        'three_year_license',
        'special_instructions',
        'is_active',
        'sales_price',
        'deleted_at',
        'stripe_id_one_year',
        'stripe_id_two_year',
        'stripe_id_three_year',
        'contact_sales',
        'contact_amount'
    ];

    public function plan()
    {
        return $this->hasOne('App\CLMPlans', 'id', 'plan_id');
    }
}
