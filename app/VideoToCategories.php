<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoToCategories extends Model
{
    protected $table = 'video_to_categories';
    protected $fillable = ['id','video_id', 'cat_id','deleted_at'];
}
