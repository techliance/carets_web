<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class UserVideos extends Model
{
    use PaginationWithHavings;
    protected $table = 'users_videos';
    protected $fillable = ['id','user_id', 'video_id','deleted_at'];
}
