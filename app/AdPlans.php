<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdPlans extends Model
{
    use SoftDeletes;
    protected $table = 'ad_plans';
    protected $fillable = ['id','stripe_id', 'title', 'position','duration','amount','is_active','deleted_at'];
}
