<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchCaretByDate extends Model
{
    protected $table = 'search_caret_created';
    protected $fillable = ['id','video_id', 'age'];
}
