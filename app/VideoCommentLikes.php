<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCommentLikes extends Model
{
    protected $table = 'video_comment_likes';
    protected $fillable = ['id','comment_id', 'user_id','deleted_at'];
}
