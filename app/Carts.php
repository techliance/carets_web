<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carts extends Model
{
    protected $table = 'carts';
    protected $fillable = ['user_id', 'guest_token', 'total_items', 'total_amount','meeting_time'];

    public function items()
    {
        return $this->hasMany('App\CartItems', 'cart_id', 'id');
    }


}
