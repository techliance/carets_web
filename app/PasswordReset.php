<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    protected $table = 'password_resets'; // Explicitly define the table

    protected $fillable = ['email', 'otp', 'expires_at', 'token', 'created_at'];

    protected $primaryKey = 'email'; // Set primary key
    public $incrementing = false; // Disable auto-incrementing
    protected $keyType = 'string'; // Set key type to string
    public $timestamps = false; // Laravel assumes created_at/updated_at, disable them
}
