<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class UserFollows extends Model
{
    use PaginationWithHavings;
    protected $table = 'user_follows';
    protected $fillable = ['id','follow_to', 'follow_by','deleted_at'];

    public function followby()
    {
        return $this->hasOne('App\User', 'id', 'follow_by');
    }

    public function followto()
    {
        return $this->hasOne('App\User', 'id', 'follow_to');
    }
}
