<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission as packagePermission;

class Permission extends packagePermission
{
    protected $fillable = [
        'name', 'guard_name', 'description', 'permission_type', 'is_menu', 'icon', 'label', 'parent_label', 'order','module_id','path'
    ];
    private static $permissionsArr;
        static $rules = [
        'description' => 'required',
        'is_menu' => 'required',
        'label' => 'required',
        'name' => 'required|unique:permissions',
        'permission_type' => 'required',
        'module_id' => 'required'
        ];
    public function parent() {
        return $this->hasOne(Permission::class, 'id', 'parent_label');
    }

    public function children() {
        return $this->hasMany(Permission::class, 'parent_label', 'id')
            ->whereIn('id', Static::$permissionsArr)
            ->orderBy('order', 'asc');
    }
    public static function tree($rolePermissionsArray = null) {
        static::$permissionsArr = $rolePermissionsArray;
        return static::with(implode('.', array_fill(0, 4, 'children')))
            ->where('parent_label', '=', 0)
            ->where('is_menu', 1)
            ->whereIn('id', $rolePermissionsArray)
            ->orderBy('order', 'asc')
            ->get();

    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function getMenuData($role_id){
        return DB::select("SELECT p.id, p.name, p.order, p.permission_type, p.path, p.is_menu, p.icon, p.label, p.description,
                       ( CASE
                           WHEN p.id IN (SELECT p.id
                                         FROM   role_has_permissions AS rhp
                                                LEFT JOIN permissions AS p
                                                       ON rhp.permission_id = p.id
                                         WHERE  rhp.role_id = $role_id
                                                AND p.parent_label NOT IN (SELECT p.id
                                                                           FROM
                                                    role_has_permissions AS rhp

                                                    LEFT JOIN permissions AS p
                                                           ON rhp.permission_id =
                                                              p.id
                                                                           WHERE
                                                    rhp.role_id = $role_id AND p.is_menu = true)
                                                AND p.parent_label != 0 ) THEN 0
                           ELSE p.parent_label
                         END ) AS parent_label
                FROM   permissions AS p
                       LEFT JOIN role_has_permissions AS rhp
                              ON rhp.permission_id = p.id
                WHERE  rhp.role_id = $role_id AND p.is_menu = true
                ORDER  BY p.order ");
    }

    public function getFilteredAndOrderByAttrAllPermissions($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return Permission::with(['parent:id,name','module:id,name'])->select('permissions.id', 'name', 'label','is_menu','parent_label','module_id')
            ->where(function($q) use ($filter){
                $q->where('permissions.id','like', '%' . $filter . '%')
                    ->orWhere('permissions.name','like', '%' . $filter . '%');
            })
            ->when($sortName == 'module.name', function ($q) use ($sortName, $orderType) {
                $q->join('modules', 'modules.id', '=', 'permissions.module_id')
                    ->select('modules.*', 'permissions.id', 'permissions.name', 'permissions.label','permissions.is_menu','permissions.parent_label','permissions.module_id')
                    // ->withoutAccount()
                    ->orderBy("modules.name", $orderType);
            })
            ->when($sortName && $sortName != "module.name", function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->paginate($pagination);
    }
    public function findPermissionWithParent($permission_id){
        return Permission::with('parent:id,name,label')->find($permission_id);
    }
    public function findIfOrderExist($order, $permission_id){
        return Permission::where('order', $order)
            ->where('id', '!=',$permission_id)
            ->first();
    }
    public function getExchangeOrderValue($permission_id){
        return Permission::where('id', $permission_id)->value('order');
    }
}
