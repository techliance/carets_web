<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavHashtags extends Model
{
    protected $table = 'user_fav_hashtags';
    protected $fillable = ['id','user_id', 'title','deleted_at'];
}
