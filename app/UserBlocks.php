<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class UserBlocks extends Model
{
    use PaginationWithHavings;
    protected $table = 'user_blocks';
    protected $fillable = ['id','block_to', 'block_by','deleted_at'];

    public function blockby()
    {
        return $this->hasOne('App\User', 'id', 'block_by');
    }

    public function blockto()
    {
        return $this->hasOne('App\User', 'id', 'block_to');
    }
}
