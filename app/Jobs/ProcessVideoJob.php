<?php

namespace App\Jobs;

use App\CLMLicense;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Videos;
use App\Helpers\Helper;
use App\User;
use App\UserFollows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProcessVideoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $video_id,$converted_path,$watermark_path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($video_id)
    {
        $this->video_id = $video_id;
        $this->converted_path = storage_path('app/uploads/');
        $this->watermark_path = storage_path('app/uploads/Videos/watermark.png');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $video = Videos::where('id',$this->video_id)->with('users')->first();

        $licenseData = CLMLicense::where('id',$video->license_id)->first();

        if ($licenseData && !empty($licenseData->caret_logo)) {

            $myWaterMark = storage_path('app/'.$licenseData->caret_logo);
            if (!file_exists($myWaterMark)) {
                $myWaterMark = $this->watermark_path;
            }

        }else{
            $myWaterMark = $this->watermark_path;
        }

        $video->update(['is_active'=>2]);
        if($video){
            if($video->video_url){
                $file_path = $video->video_url;
                $video_id = $video->id;
                $uploadFile['is_active'] = 3;
                    $originalVideoPath = storage_path('app/' . $file_path);
                    $scaledVideoPath = Helper::convertToMp4($originalVideoPath);

                    if($scaledVideoPath){
                        $video_path = file_get_contents($scaledVideoPath);

                        $fileInfo = pathinfo($scaledVideoPath);
                        $extension = strtolower($fileInfo['extension']);
                        $file_link = 'Videos' . '/'.$video_id.'_'.time(). '.mp4';
                        $video_export_url =  str_replace('.mp4','_export.mp4',$file_link);

                        Helper::putServerImage($file_link, $video_path, 'public');
                        Helper::putServerImage($video_export_url, $video_path, 'public');



                        Helper::putS3Image($video_export_url, $video_path, 'public');
                        Helper::putS3Image($file_link, $video_path, 'public');

                        $uploadFile['video_raw_url'] = $file_link;
                        $uploadFile['video_export_url'] = $video_export_url;
                        $uploadFile['video_url'] = $file_link;
                        $uploadFile['image_url'] = str_replace('Videos/','Images/',str_replace('.mp4','.gif',$file_link));



                    }

                $video->update($uploadFile);
                 #waterMarkVideo

                $waterMark = Helper::addWatermark($video->video_export_url,$myWaterMark,$this->converted_path);

                $path = "app/uploads/".$waterMark;
                        $video_path = storage_path($path);
                        $isExists = File::exists($video_path);

                if ($isExists) {
                    $image = file_get_contents($video_path);
                    Helper::putS3Image($waterMark, $image, 'public');
                    $video->update(['is_active'=>1]);
                }
                #GIF Creation
                $gif = Helper::createGif($video->video_url,$this->converted_path);

                $path2 = "app/uploads/".$gif;
                $gif_path = storage_path($path2);
                $isExists = File::exists($gif_path);
                if ($isExists) {
                    $image = file_get_contents($gif_path);
                    if($video->is_caret == 1){
                        $file_path = str_replace('Videos/','Images/',str_replace('.mp4','.gif',$gif));
                    }else{
                        $file_path = str_replace('Videos/','Images/',str_replace('.mp4','.gif',$gif));
                    }
                    Helper::putS3Image($file_path, $image, 'public');
                }


               // Step 1: Get device tokens of users associated with the video
                $users = $video->users()->whereNotNull('device_token')->get();
                $device_tokens_users = $users->pluck('device_token')
                    ->filter() // Remove any null values
                    ->unique() // Remove duplicate tokens
                    ->values(); // Reset keys

                // Step 2: Get device tokens of followers of these users
                // $device_tokens_followers = UserFollows::whereIn('follow_to', $users->pluck('id')->toArray())
                //     ->with(['followby' => function ($query) {
                //         $query->select('id', 'device_token'); // Select only needed fields
                //     }])
                //     ->get()
                //     ->pluck('followby.device_token')
                //     ->filter() // Remove null values
                //     ->unique() // Remove duplicates
                //     ->values(); // Reset keys




                 // Send notification to users
                 if ($device_tokens_users->isNotEmpty()) {
                     Helper::sendFCMNotification($device_tokens_users->toArray(), [
                         'title' => 'New Video',
                         'body' => 'Your video has been uploaded!',
                         'data' => [
                             'title' => 'New Video',
                             'body' => 'Your video has been uploaded!'
                         ]
                     ]);
                 }

                // Send notification to followers
                // if ($device_tokens_followers->isNotEmpty()) {
                //     Helper::sendFCMNotification($device_tokens_followers->toArray(), [
                //         'title' => 'New Video',
                //         'body' => 'A user you follow has uploaded a new video!',
                //         'data' => [
                //             'title' => 'New Video',
                //             'body' => 'A user you follow has uploaded a new video!'
                //         ]
                //     ]);
                // }


            }else{

            }

        }

    }
}
