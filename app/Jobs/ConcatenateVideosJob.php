<?php
namespace App\Jobs;


use App\Http\Controllers\CaretCPUController;
use App\Http\Controllers\CaretGPUController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ConcatenateVideosJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public function handle()
    {
        if (env('IS_GPU', true)) {
            $controller = new CaretGPUController();

        } else {
            $controller = new CaretCPUController();
        }
        $controller->concatenateVideos();
    }
}
