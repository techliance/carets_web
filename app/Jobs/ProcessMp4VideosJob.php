<?php
namespace App\Jobs;

use App\Http\Controllers\VideoController;
use App\Videos;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessMp4VideosJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        // Check if there's a new video that needs processing
        $pendingVideo =  Videos::where('is_active', 1)
        ->where('is_m3u8', 0)
        ->orderBy('id', 'desc')
        ->first();

        if (!$pendingVideo) {
            Log::info('No pending videos to process. Job will not re-dispatch.');
            return;
        }

        try {
            $controller = new VideoController();
            $controller->processHLSVideo();

            // Only re-dispatch if there are still pending videos
            if (Videos::where('is_m3u8', 0)->where('is_active', 1)->exists()) {
                self::dispatch()->delay(now()->addSeconds(30));
            }

        } catch (\Exception $e) {
            Log::error('Error processing video: ' . $e->getMessage());
        }
    }
}
