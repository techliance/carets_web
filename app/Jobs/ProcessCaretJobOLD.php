<?php

namespace App\Jobs;

use App\CLMLicense;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Videos;
use App\Helpers\Helper;
use App\User;
use App\UserFollows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProcessCaretJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $video_id,$converted_path,$watermark_path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($video_id)
    {
        $this->video_id = $video_id;
        $this->converted_path = storage_path('app/uploads/');
        $this->watermark_path = storage_path('app/uploads/Videos/watermark.png');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $video = Videos::where('id',$this->video_id)->with('users')->first();

        $licenseData = CLMLicense::where('id',$video->license_id)->first();


        $video->update(['is_active'=>3]);
        if($video){
            if($video->video_url){
                #waterMarkVideo
                Log::info("START");
                $basic_video_path = $video->video_url;
                $waterMarkExist = 0;
                $uploadFile['is_active'] = 1;


                if ($licenseData && !empty($licenseData->caret_logo)) {
                    $waterMarkExist = 1;
                    $myWaterMark = storage_path('app/'.$licenseData->caret_logo);
                    if (!file_exists($myWaterMark)) {
                        $waterMarkExist = 0;
                    }

                }else{
                    $waterMarkExist = 0;
                }
                $waterMarkExist = 0;


                if($waterMarkExist == 1){
                    $myWaterMark = storage_path('app/'.$licenseData->caret_logo);

                    $waterMark_app = Helper::addWatermark($video->video_url,$myWaterMark,$this->converted_path);
                    Log::info("waterMark_app: ".$waterMark_app);
                    $path = "app/uploads/".$waterMark_app;
                    $video_path = storage_path($path);
                    $isExists = File::exists($video_path);

                    if ($isExists) {
                        $uploadFile['video_url'] = str_replace('caret_videos/','Videos/',$waterMark_app);
                        $uploadFile['video_raw_url'] = str_replace('caret_videos/','Videos/',$waterMark_app);

                        $image = file_get_contents($video_path);
                        Helper::putS3Image($uploadFile['video_url'], $image, 'public');
                    }

                    $waterMark_export = Helper::addWatermark($video->video_export_url,$myWaterMark,$this->converted_path);
                    Log::info("waterMark_app 2: ".$waterMark_app);
                    $path = "app/uploads/".$waterMark_export;
                    $video_path = storage_path($path);
                    $isExists = File::exists($video_path);

                    if ($isExists) {
                        $uploadFile['video_export_url'] = str_replace('caret_videos/','Videos/',$waterMark_export);


                        $image = file_get_contents($video_path);
                        Helper::putS3Image( $uploadFile['video_export_url'], $image, 'public');
                    }

                }else{
                    $myWaterMark = $this->watermark_path;

                    $waterMark_export = Helper::addWatermark($video->video_export_url,$myWaterMark,$this->converted_path);
                    Log::info("waterMark_app 3: ".$waterMark_export);
                    $path = "app/uploads/".$waterMark_export;
                    $video_path = storage_path($path);
                    $isExistsExp = File::exists($video_path);

                    sleep(5);


                    $originalVideo = storage_path('app/'.$video->video_url);
                    $isExists = File::exists($originalVideo);

                    if ($isExists) {
                        Log::info("File Exist originalVideo: ".$originalVideo);
                        $uploadFile['video_url'] = str_replace('caret_videos/','Videos/',$video->video_url);
                        $uploadFile['video_raw_url'] = str_replace('caret_videos/','Videos/',$video->video_url);
                        $image = file_get_contents($originalVideo);

                        $uploadFile['video_export_url'] = str_replace('caret_videos/','Videos/',$waterMark_export);
                        Helper::putS3Image($uploadFile['video_export_url'], $image, 'public');
                        Helper::putS3Image( $uploadFile['video_url'], $image, 'public');
                    }else{
                        return null;
                    }

                    if ($isExistsExp) {
                        Log::info("File Exist waterMark_export: ".$video_path);
                        $uploadFile['video_export_url'] = str_replace('caret_videos/','Videos/',$waterMark_export);
                        $image = file_get_contents($video_path);
                        Helper::putS3Image($uploadFile['video_export_url'], $image, 'public');
                    }

                }



                $video->update($uploadFile);


                #GIF Creation
                $gif = Helper::createGif($basic_video_path,$this->converted_path);
                Log::info("File Created gif: ".$gif);
                $path2 = "app/uploads/".$gif;
                $gif_path = storage_path($path2);
                $isExists = File::exists($gif_path);
                if ($isExists) {
                    Log::info("File Exist gif: ".$gif);
                    $image = file_get_contents($gif_path);
                    $file_path = str_replace('caret_videos/','Images/',str_replace('.mp4','.gif',$gif));
                    Helper::putS3Image($file_path, $image, 'public');

                    $uploadImageFile['image_url'] = $file_path;
                    $video->update($uploadImageFile);
                }





               // Step 1: Get device tokens of users associated with the video
                $users = $video->users()->whereNotNull('device_token')->get();
                $device_tokens_users = $users->pluck('device_token')
                    ->filter() // Remove any null values
                    ->unique() // Remove duplicate tokens
                    ->values(); // Reset keys


                // Send notification to users
                if ($device_tokens_users->isNotEmpty()) {
                    Helper::sendFCMNotification($device_tokens_users->toArray(), [
                        'title' => 'New Video',
                        'body' => 'Your video has been uploaded!',
                        'data' => [
                            'title' => 'New Video',
                            'body' => 'Your video has been uploaded!'
                        ]
                    ]);
                }



            }else{

            }

        }

    }
}
