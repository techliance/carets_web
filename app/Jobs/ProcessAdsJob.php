<?php

namespace App\Jobs;

use App\Ads;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Videos;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProcessAdsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $ad_id,$converted_path,$watermark_path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ad_id)
    {
        $this->ad_id = $ad_id;
        $this->converted_path = storage_path('app/uploads/');
        $this->watermark_path = storage_path('app/uploads/Videos/watermark.png');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //DB::enableQueryLog();
        $video = Ads::where('id',$this->ad_id)->first();
        if($video){
            if($video->video_url){
                 #waterMarkVideo
                $waterMark = Helper::scaleDownVideo($video->video_url,$this->watermark_path,$this->converted_path);

                $path = "app/uploads/".$waterMark;
                $video_path = storage_path($path);
                $isExists = File::exists($video_path);

                if ($isExists) {
                    $image = file_get_contents($video_path);
                    Helper::putS3Image($waterMark, $image, 'public');
                }

                #GIF Creation
                $gif = Helper::createGif($video->video_url,$this->converted_path);
                $path2 = "app/uploads/".$gif;
                $gif_path = storage_path($path2);
                $isExists = File::exists($gif_path);
                if ($isExists) {
                    $image = file_get_contents($gif_path);
                    $file_path = str_replace('.mp4','.gif',$gif);
                    Helper::putS3Image($file_path, $image, 'public');
                    $video->update(['image_url'=>$file_path]);
                }

            }else{

            }

        }

    }
}
