<?php

namespace App\Jobs;

use App\Ads;
use App\CLMSplash;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Videos;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProcessSplashJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $splash_id,$converted_path,$watermark_path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($splash_id)
    {
        $this->splash_id = $splash_id;
        $this->converted_path = storage_path('app/uploads/');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //DB::enableQueryLog();
        $splash = CLMSplash::where('id',$this->splash_id)->first();

        if($splash){
            if($splash->video_url){


                $path = "app/uploads/".$splash->video_url;
                $video_path = storage_path($path);

                $image = file_get_contents($video_path);
                Helper::putS3Image($video_path, $image, 'public');


            }else{

            }

        }

    }
}
