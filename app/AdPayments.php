<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdPayments extends Model
{
    use SoftDeletes;
    protected $table = 'ad_payments';
    protected $fillable = ['id','campaign_ad_id', 'user_id','card_id','amount','deleted_at'
    ,'transaction_id'
    ,'customer'
    ,'cancel_at'
    ,'cancel_at_period_end'
    ,'canceled_at'
    ,'collection_method'
    ,'created'
    ,'current_period_end'
    ,'current_period_start'
    ,'days_until_due'
    ,'default_payment_method'
    ,'plan_id'
    ,'plan_currency'
    ,'plan_interval'
    ,'plan_interval_count'
    ,'plan_product'
    ,'start_date'
    ,'status',
    'transaction_type',
    'latest_invoice'
    ];



    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    public function campaign()
    {
        return $this->hasOne('App\CampaignsAds', 'id', 'campaign_ad_id');
    }

    public function card()
    {
        return $this->hasOne('App\AdCards', 'id', 'card_id');
    }

}
