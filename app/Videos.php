<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class Videos extends Model
{
    use PaginationWithHavings;
    protected $table = 'videos';
    protected $fillable = ['id','is_private', 'sound_id','video_title','is_caret','video_url','image_url','video_share_link','video_description','is_active','watch_count','share_count','loc_lat','loc_long','hashtags','is_allow_comments','is_allow_caretCreation','deleted_at','is_blocked','duration','start_time','end_time','is_marked_distance','is_marked','age','tag_video_process','distance_video_process','tag_retry_count','distance_retry_count','video_export_url','video_raw_url','license_id', 'is_manual','caret_processing','caret_atempts','is_m3u8'];

    public function videoads()
    {
        return $this->belongsToMany('App\Ads', 'video_ads', 'video_id', 'ad_id');
    }

    public function videosplashes()
    {
        return $this->belongsToMany('App\CLMSplash', 'video_splashes', 'video_id', 'splash_id')->withPivot('finish');
    }

    public function videosound()
    {
        return $this->hasOne(\App\Sounds::class, 'id', 'sound_id');
    }

    public function license()
    {
        return $this->hasOne('App\CLMLicense', 'id', 'license_id');
    }

    public function sounds()
    {
        return $this->hasMany('App\Sounds', 'id', 'sound_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_videos', 'video_id', 'user_id');
    }

    public function likedme()
    {
        return $this->hasMany('App\VideoLikes', 'video_id', 'id');
    }

    public function likedBy()
    {
        return $this->hasMany('App\VideoLikes', 'video_id', 'id');
    }

    public function tagged()
    {
        return $this->belongsToMany('App\User', 'users_tag_videos', 'video_id', 'user_id');
    }


    public function hashs()
    {
        return $this->belongsToMany('App\Hashs', 'hashs_videos', 'video_id', 'hash_id');
    }


    public function carets()
    {
        return $this->belongsToMany('App\Carets', 'carets_videos', 'video_id', 'caret_id')->withTimestamps();
    }

    public function reported()
    {
        return $this->hasMany('App\VideoReports', 'video_id', 'id');
    }

    public function collages()
    {
        return $this->hasMany('App\VideoCollages', 'video_id', 'id');
    }

    public function videos()
    {
        return $this->belongsToMany('App\Videos', 'video_collages', 'video_id', 'video_added');
    }

    public function getVideoList($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return Videos::with(['users'])
        ->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url")])
        ->where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                    ->orWhere('video_description','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);
    }

    //$ads = Ad::with(['user','photos'])->distance($from_latitude,$from_longitude,$distance)->get();


    public function ScopeDistance($query,$from_latitude,$from_longitude,$distance)
    {
        // This will calculate the distance in km
        // if you want in miles use 3959 instead of 6371
        $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$from_latitude.') ) * cos( radians( loc_lat ) ) * cos( radians( loc_long ) - radians('.$from_longitude.') ) + sin( radians('.$from_latitude.') ) * sin( radians( loc_lat ) ) ) ) ) AS distance');
        return $query->addSelect(DB::raw("CONCAT('".config('app.awsurl')."', video_url) AS video_url"))->addSelect(DB::raw("CONCAT('".config('app.awsurl')."', image_url) AS image_url"))->addSelect($raw)
        ->orderBy( 'distance', 'ASC' )
        ->having('distance', '<=', $distance)
        ;
    }

    public function ScopeDistance2($query,$from_latitude,$from_longitude,$distance)
    {
        // This will calculate the distance in km
        // if you want in miles use 3959 instead of 6371
        $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians('.$from_latitude.') ) * cos( radians( loc_lat ) ) * cos( radians( loc_long ) - radians('.$from_longitude.') ) + sin( radians('.$from_latitude.') ) * sin( radians( loc_lat ) ) ) ) ) AS distance');
        return $query->addSelect($raw)
        ->orderBy( 'distance', 'ASC' )
        ->having('distance', '<=', $distance)
        ;
    }

}
