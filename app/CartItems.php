<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItems extends Model
{
    protected $table = 'cart_items';
    protected $fillable = ['cart_id', 'caret_title', 'interval_count', 'subscription_pricing_id', 'subscription_amount','subscription_stripe_id'];



    public function pricing()
    {
        return $this->hasOne('App\CaretPricing', 'id', 'subscription_pricing_id');
    }

    public function cart()
    {
        return $this->belongsTo('App\Carts', 'cart_id', 'id');
    }

}
