<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoSplash extends Model
{
    protected $table = 'video_splashes';
    protected $fillable = ['id','video_id', 'splash_id','finish','deleted_at'];

}
