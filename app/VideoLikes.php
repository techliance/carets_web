<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoLikes extends Model
{
    protected $table = 'video_likes';
    protected $fillable = ['id','video_id', 'user_id','deleted_at'];
}
