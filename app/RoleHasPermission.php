<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleHasPermission extends Model {

    /**
     * Generated
     */

    protected $table = 'role_has_permissions';
    protected $fillable = ['permission_id', 'role_id'];


    public function permission() {
        return $this->belongsTo(\App\Permission::class, 'permission_id', 'id');
    }

    public function role() {
        return $this->belongsTo(\App\Role::class, 'role_id', 'id');
    }


}
