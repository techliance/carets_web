<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as packageRole;
use Illuminate\Support\Facades\DB;
use App\Permission;

class Role extends packageRole
{
    protected $fillable = [
        'name', 'guard_name', 'model_type'
    ];
    public function getFilteredAndOrderByAttrAllRoles($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return Role::select('id', 'name')
            ->where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                    ->orWhere('name','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->paginate($pagination);
    }
    public function getPermissionForRole($role_id){
        return Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$role_id)
            ->get();
    }
    public function getUserIdsWithThisRole($role_id){
        return DB::table('model_has_roles as mhr')
            ->rightJoin('oauth_access_tokens as oat', 'mhr.model_id', '=', 'oat.user_id')
            ->where('oat.revoked', '=', 0)
            ->where('role_id', $role_id)
            ->pluck('model_id');
    }

    public function getRoles($modelType = 'App\\Users'){
        return Role::orderBy('id','DESC')
            ->select('id as value', 'name as label')
            ->where('model_type', $modelType)
            ->get();
    }
    public function modelHasRole($role_id){
        return DB::table('model_has_roles')->where('role_id', $role_id)->first();
    }

}
