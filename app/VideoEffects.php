<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoEffects extends Model
{
    protected $table = 'video_effects';
    protected $fillable = ['id','title', 'effect_url','is_active', 'deleted_at'];
}
