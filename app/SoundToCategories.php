<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoundToCategories extends Model
{
    protected $table = 'sound_to_categories';
    protected $fillable = ['id','sound_id', 'cat_id','deleted_at'];
}
