<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CLMOrders extends Model
{
    protected $table = 'clm_orders';

    protected $fillable = [
        'user_id',
        'order_number',
        'total_items',
        'total_amount',
        'order_type',
    ];

    /**
     * Relationship: An order has many order items.
     */
    public function items()
    {
        return $this->hasMany('App\CLMOrderItems', 'order_id', 'id');
    }

    /**
     * Relationship: An order has many payments.
     */
    public function payments()
    {
        return $this->hasOne('App\CLMOrderPayments', 'order_id', 'id');
    }

}
