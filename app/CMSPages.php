<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSPages extends Model
{
    use SoftDeletes;
    protected $table = 'cms_pages';
    protected $fillable = ['id','created_by', 'updated_by', 'page_title', 'page_slug', 'page_description', 'meta_title', 'meta_description', 'meta_tags','is_active', 'deleted_at'];

    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

}
