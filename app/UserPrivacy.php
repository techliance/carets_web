<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPrivacy extends Model
{
    protected $table = 'user_privacy';
    protected $fillable = ['id','user_id', 'private_account','sugest_others','ad_auth', 'download_video','likes','comments','followers','mentions','direct_messages','video_from','video_suggestion','deleted_at'];
}
