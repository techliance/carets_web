<?php

namespace App\Helpers;

use App\SignupStep;
use App\Videos;
//use Google\Cloud\Firestore\FirestoreClient;
use Illuminate\Support\Facades\Storage;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client as TwilioClient;
use Twilio\Exceptions\TwilioException;

use Illuminate\Support\Facades\Log;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\ServiceAccount;
use Exception;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;

class Helper {





    public static function getTwilioObject($debug = true)
    {
        try {
            // Determine if in debug mode and set appropriate credentials
            if (config('app.debug') && $debug === true) {
                $twilio_account_sid = config('app.TWILIO_ACCOUNT_SID_TEST', null);
                $twilio_account_auth_token = config('app.TWILIO_ACCOUNT_AUTH_TOKEN_TEST', null);
            } else {
                $twilio_account_sid = config('app.TWILIO_ACCOUNT_SID', null);
                $twilio_account_auth_token = config('app.TWILIO_ACCOUNT_AUTH_TOKEN', null);
            }

            // Check if SID and Auth Token are provided
            if (empty($twilio_account_sid) || empty($twilio_account_auth_token)) {
                throw new Exception("Twilio credentials are missing. Please check your configuration.");
            }

            // Create and return the Twilio Client instance
            return new TwilioClient($twilio_account_sid, $twilio_account_auth_token);
        } catch (ConfigurationException $e) {
            // Handle Twilio-specific configuration exceptions
            logger()->error("Twilio Configuration Exception: " . $e->getMessage());
            throw new Exception("Failed to configure Twilio client. Please check your credentials.");
        } catch (Exception $e) {
            // Handle other exceptions
            logger()->error("General Exception: " . $e->getMessage());
            throw new Exception("An error occurred while creating the Twilio client: " . $e->getMessage());
        }
    }

    // public static function deleteTwilioService($twilio, $twilio_service_sid) {
    //     $twilio->verify->v2->services($twilio_service_sid)->delete();
    // }

    public static function deleteTwilioService($twilio, $twilio_service_sid)
    {
        try {
            // Ensure the Twilio service SID is provided
            if (empty($twilio_service_sid)) {
                throw new Exception("Twilio service SID is required for deletion.");
            }

            // Attempt to delete the service
            $twilio->verify->v2->services($twilio_service_sid)->delete();
        } catch (TwilioException $e) {
            // Handle Twilio-specific exceptions
            logger()->error("Twilio Exception: " . $e->getMessage());
            throw new Exception("Failed to delete the Twilio service: " . $e->getMessage());
        } catch (Exception $e) {
            // Handle general exceptions
            logger()->error("General Exception: " . $e->getMessage());
            throw new Exception("An error occurred while deleting the Twilio service: " . $e->getMessage());
        }
    }

    // public static function putS3Image($file_path, $image, $type) {
    //     $confirmUpload = Storage::disk('s3')->put($file_path, $image, $type);
    //     if ($confirmUpload)
    //         return true;
    //     else
    //         return false;
    // }


    public static function putS3Image($file_path, $image, $type) {
        try {
            $confirmUpload = Storage::disk('s3')->put($file_path, $image, $type);

            if ($confirmUpload) {
                Log::info("Successfully uploaded to S3: " . $file_path);
                return true;
            } else {
                Log::error("Failed to upload to S3: " . $file_path);
                return false;
            }
        } catch (Exception $e) {
            Log::error("Error uploading to S3: " . $e->getMessage());
            return false;
        }
    }



    public static function putServerImage($file_path, $image, $type)
    {
        try {
            // Validate inputs
            if (empty($file_path)) {
                throw new Exception("File path cannot be empty.");
            }

            if (empty($image)) {
                throw new Exception("Image content cannot be empty.");
            }

            if (empty($type)) {
                throw new Exception("File type must be specified.");
            }

            // Attempt to upload the image to the server
            $confirmUpload = Storage::put($file_path, $image, $type);

            if ($confirmUpload) {
                return true;
            } else {
                throw new Exception("Failed to upload the image to the server.");
            }
        } catch (Exception $e) {
            // Log the error
            logger()->error("Error in putServerImage: " . $e->getMessage());

            // Optionally, rethrow the exception if you want the caller to handle it
            throw new Exception("An error occurred while uploading the image: " . $e->getMessage());
        }
    }


    public static function putServerImageR($file_path, $image, $type)
    {
        try {
            // Use fopen for memory efficiency on large files
            $stream = is_resource($image) ? $image : fopen($image, 'r+');

            $confirmUpload = Storage::put($file_path, $stream, $type);

            // Close stream if we opened it
            if (!is_resource($image)) {
                fclose($stream);
            }

            // Check if the file was uploaded
            if ($confirmUpload) {
                return $file_path; // Return path for further reference
            } else {
                Log::error("File upload failed for path: {$file_path}");
                return false;
            }
        } catch (Exception $e) {
            Log::error("Error uploading file to {$file_path}: " . $e->getMessage());
            return false;
        }
    }




    public static function sendFCMNotification($deviceTokens, $input)
    {
        try {
            // Initialize Firebase with credentials
            $firebase = (new Factory)
                ->withServiceAccount(storage_path('app/firebase/caret-7f711-firebase-adminsdk-1wn14-a2c2d521d1.json'))
                ->createMessaging();

            // Prepare the notification data
            if (is_array($deviceTokens)) {
                foreach($deviceTokens as $deviceToken) {
                    $message = CloudMessage::withTarget('token', $deviceToken)
                        ->withNotification([
                            'title' => $input['title'],
                            'body'  => $input['body'],
                        ])
                        ->withData($input['data']);  // Additional data if needed

                    // Send the notification and capture the response
                    $response = $firebase->send($message);

                    // Log the response
                    Log::info("1 - FCM Notification sent to {$deviceToken}: " . json_encode($response));
                }
            } else {
                if (count($deviceTokens) === 1) {
                    $device_tokens[] = $deviceTokens;
                }
                $message = CloudMessage::withTarget('token', $device_tokens)
                    ->withNotification([
                        'title' => $input['title'],
                        'body'  => $input['body'],
                    ])
                    ->withData($input['data']);  // Additional data if needed

                // Send the notification and capture the response
                $response = $firebase->send($message);

                // Log the response
                Log::info("2 - FCM Notification sent to {$device_tokens}: " . json_encode($response));
            }

        } catch (Exception $e) {
            Log::error("FCM Notification failed: " . $e->getMessage());
        }
    }



    public static function getFireStoreConnection()
    {
        try {
            // Define the path to your credentials file
            $firebase_json_file = storage_path('app/firebase/caret-7f711-firebase-adminsdk-1wn14-a2c2d521d1.json');

            // Check if the credentials file exists
            if (!file_exists($firebase_json_file)) {
                throw new Exception("Firebase credentials file not found at: $firebase_json_file");
            }

            // Create a Firebase instance with Firestore support
            $firebase = (new Factory)
                ->withServiceAccount($firebase_json_file)
                ->createFirestore();

            return $firebase->database();
        } catch (AuthException $e) {
            // Handle authentication-specific exceptions
            logger()->error("Firebase Auth Exception: " . $e->getMessage());
            throw new Exception("Failed to authenticate with Firebase. Please check your credentials.");
        } catch (FirebaseException $e) {
            // Handle general Firebase exceptions
            logger()->error("Firebase Exception: " . $e->getMessage());
            throw new Exception("Failed to connect to Firebase. Please try again later.");
        } catch (Exception $e) {
            // Handle other exceptions
            logger()->error("General Exception: " . $e->getMessage());
            throw new Exception("An error occurred while connecting to Firestore: " . $e->getMessage());
        }
    }




    public static function convertToMp4($input_file)
    {
        try {
            // Validate the input file
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // Determine if the file is already in MP4 format
            $file_info = pathinfo($input_file);
            $is_mp4 = strtolower($file_info['extension']) === 'mp4';
            $output_file = $file_info['dirname'] . '/' . $file_info['filename'] . ($is_mp4 ? '_converted.mp4' : '.mp4');

            // FFmpeg options for optimization
            $preset = 'fast'; // Speed optimization
            $crf = 21; // Quality setting
            $resolution = '540x960'; // Resolution scaling
            $fps = '30'; // Frame rate

            // Use CPU cores explicitly (taskset)
            $cpu_cores = '0,1,2'; // Adjust core allocation as needed
            $taskset_prefix = "taskset -c $cpu_cores";

            // Construct FFmpeg command
            $ffmpeg_cmd = "$taskset_prefix ffmpeg -y -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:a aac -strict -2 \"$output_file\" 2>&1";

            // Execute the FFmpeg command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg conversion failed: $error_message");
            }

            // Success message
            echo "Video conversion succeeded: $output_file\n";
            // self::convertToHLS($output_file);
            return $output_file;
        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToMp4: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }

    public static function convertToHLS_GPU($video_file, $video_id)
    {
        try {
            $input_file = storage_path('app/' . $video_file);

            // Validate the input file
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // File info and output directory
            $file_info = pathinfo($input_file);
            $output_dir = storage_path('app/caret_videos/' . $video_id);
            if (!is_dir($output_dir)) {
                mkdir($output_dir, 0777, true);
            }

            // Output playlist and segment files
            $output_playlist = $output_dir . '/output.m3u8';
            $segment_filename = $output_dir . '/chunk_%03d.ts';

            // FFmpeg options for HLS
            $segment_duration = 3; // Segment duration in seconds
            $resolution = '540x960'; // Resolution scaling
            $fps = 30; // Frame rate
            $preset = 'p4'; // NVENC equivalent to "fast"
            $cq = 21; // NVENC does not support CRF, use CQ (Constant Quality)

            // Construct GPU-optimized FFmpeg command
            //$ffmpeg_cmd = "ffmpeg -y -hwaccel cuda -i \"$input_file\" -vf \"scale=w=540:h=960, fps=fps=30\" -c:v h264_nvenc -preset $preset -cq $cq -c:a aac -b:a 128k -hls_time $segment_duration -hls_playlist_type vod -hls_segment_filename \"$segment_filename\" \"$output_playlist\" 2>&1";

            $ffmpeg_cmd = "ffmpeg -y -hwaccel cuda -i \"$input_file\" " .
            "-vf \"scale=540:960,fps=30\" " .
            "-c:v h264_nvenc -b:v 1500k -preset fast -g 48 -sc_threshold 0 " .
            "-c:a aac -b:a 128k " .
            "-hls_time $segment_duration -hls_playlist_type vod " .
            "-hls_segment_filename \"$segment_filename\" \"$output_playlist\" 2>&1";

            // Execute the FFmpeg command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg HLS conversion failed: $error_message");
            }

            // Sleep for 5 seconds before pushing to S3
            sleep(5);

            // Define the S3 folder path using the extracted ID
            $s3_folder = 'Videos/' . $video_id;

            // Get all files in the output directory
            $files = glob($output_dir . '/*');

            foreach ($files as $file) {
                $file_name = basename($file);
                $s3_path = $s3_folder . '/' . $file_name;
                $file_content = file_get_contents($file);

                $upload_success = self::putS3Image($s3_path, $file_content, 'public');

                if (!$upload_success) {
                    throw new Exception("Failed to upload $file_name to S3.");
                }
            }
            $FinalUpdate['video_url'] = 'Videos' . '/'.$video_id.'/output.m3u8';
            $FinalUpdate['is_m3u8'] = 1;
            Videos::where('id',$video_id)->update($FinalUpdate);

        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToHLS_GPU: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }


    public static function convertToHLS($video_file,$video_id)
    {
        Log::info("Helper::convertToHLS");
        try {
            $input_file = storage_path('app/' . $video_file);
            // Validate the input file
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // File info and output directory
            $file_info = pathinfo($input_file);
            //$file_parts = explode('_', $file_info['filename']);
            //$video_id = $file_parts[0]; // The first part is the ID

            $output_dir = $file_info['dirname'] . '/' . $video_id;
            if (!is_dir($output_dir)) {
                mkdir($output_dir, 0777, true);
            }

            // Output playlist and segment files
            $output_playlist = $output_dir . '/output.m3u8';
            $segment_filename = $output_dir . '/chunk_%03d.ts';

            // FFmpeg options for HLS
            $segment_duration = 3; // Segment duration in seconds
            $resolution = '540x960'; // Resolution scaling
            $fps = '30'; // Frame rate
            $preset = 'fast'; // Speed optimization
            $crf = 21; // Quality setting

            // Use CPU cores explicitly (taskset)
            $cpu_cores = '0,1,2,3,4,5'; // Adjust core allocation as needed
            $taskset_prefix = "taskset -c $cpu_cores";



            $ffmpeg_cmd = "$taskset_prefix ffmpeg -y -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:v libx264 -b:v 2000k -g 90 -keyint_min 90 -sc_threshold 0 -c:a aac -strict -2 -hls_time $segment_duration -hls_flags split_by_time -hls_playlist_type vod -hls_segment_filename \"$segment_filename\" \"$output_playlist\" 2>&1";





            // Execute the FFmpeg command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg HLS conversion failed: $error_message");
            }

            // Success message
            //echo "HLS conversion succeeded: Playlist created at $output_playlist\n";
            sleep(5);
             // Push files to S3



             // Define the S3 folder path using the extracted ID
             $s3_folder = 'Videos/' . $video_id;

             // Get all files in the output directory
             $files = glob($output_dir . '/*');

            foreach ($files as $file) {
                $file_name = basename($file);
                $s3_path = $s3_folder . '/' . $file_name;
                $file_content = file_get_contents($file);

                $upload_success = self::putS3Image($s3_path, $file_content, 'public');

                if (!$upload_success) {
                    throw new Exception("Failed to upload $file_name to S3.");
                }
            }
            $FinalUpdate['is_m3u8'] = 1;
            $FinalUpdate['video_url'] = 'Videos' . '/'.$video_id.'/output.m3u8';
            Videos::where('id',$video_id)->update($FinalUpdate);

            //return $output_playlist;
        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToHLS: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }



    public static function copyS3Video($videoUrl)
    {
        Log::info("copyS3Video -> Copy File: $videoUrl");

        $localPaths = [];
        $errors = [];


            $vidTitle = explode("/", $videoUrl);
            $fileName = end($vidTitle);
            $localPath = $videoUrl; // Relative path within the storage/app directory
            // Debugging: Check resolved local path
            $resolvedPath = storage_path('app/' . $localPath);

            // Check if the file exists locally
            if (!Storage::disk('local')->exists($localPath)) {
                try {
                    if (Storage::disk('s3')->exists($videoUrl)) {

                        // Download the file from S3
                        $fileContents = Storage::disk('s3')->get($videoUrl);
                        // Ensure the directory exists
                        Storage::disk('local')->makeDirectory(dirname($localPath));
                        // Save the file locally
                        Storage::disk('local')->put($localPath, $fileContents);

                        $localPaths[] = $resolvedPath;

                        Log::info("copyS3Video -> File Downloaded");

                    } else {
                        $errors[] = "File not found on S3: $videoUrl";
                        Log::error("File not found on S3: $videoUrl");
                    }
                } catch (Exception $e) {
                    $errors[] = "Failed to download File: $fileName. Error: {$e->getMessage()}";
                    Log::error("Failed to download File: $fileName. Error: {$e->getMessage()}");
                }
            } else {
                Log::info("copyS3Video -> Copy File Already Exist");
                $localPaths[] = $resolvedPath;

            }
        if (count($localPaths) > 0) {
            return true;
        } else {
            return false;
        }
    }


    public static function convertToMp3($input_file) {
        // Check if the input file is already in MP3 format
        if (pathinfo($input_file, PATHINFO_EXTENSION) === 'mp3') {
            echo "Input file is already in MP3 format: $input_file\n";
            return $input_file;
        }

        $fileInfo = pathinfo($input_file);
        $extension = strtolower($fileInfo['extension']);

        // Define the output file path by replacing the original extension with .mp3
        $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '.mp3';

        // Execute the FFmpeg command to convert the input file to MP3
        if (in_array($extension, ['wma', 'flac', 'aac', 'mp4'])) {
            exec("taskset -c 0,1,2 ffmpeg -y -i \"$input_file\" -vn -acodec libmp3lame -b:a 192k \"$output_file\" 2>&1", $output, $return_var);

        }elseif (in_array($extension, ['mp3', 'mpega', 'wav'])) {

            exec("taskset -c 0,1,2 ffmpeg -y -i $input_file -vn -ar 44100 -ac 2 -b:a 192k $output_file 2>&1", $output, $return_var);
        }else{
            return false;
        }

        // Check if the command was successful
        if ($return_var != 0) {
            echo "FFmpeg Conversion Failed:\n";
            print_r($output);
            return false;
        } else {
            echo "Audio Conversion Succeeded: $output_file\n";
            return $output_file;
        }
    }







    public static function addWatermark($video_url, $watermark_path, $converted_path)
    {
        $vid = explode("/", $video_url);
        $vid_title = end($vid);
        $base_path = storage_path('app/');

        $output = [];
        $return_var = 0;

        // Use faster preset for encoding
        $preset = 'fast'; // Options: ultrafast, superfast, faster, fast, medium, slow, slower
        $crf = 21;

        // FFmpeg command for adding a watermark, with faster encoding

        exec('taskset -c 0,1,2 ffmpeg -y -i '.$base_path.$video_url.' -i '.$watermark_path.' -filter_complex "[1:v]scale=80:-1,format=yuva444p,fade=t=out:st=5:d=1[watermark]; [0:v][watermark]overlay=W-w-10:50" -preset '.$preset.' -crf '.$crf.' -codec:a copy '.$converted_path.$video_url.' 2>&1', $output, $return_var);


        if ($return_var != 0) {
            echo "Watermarking Failed:\n";
            print_r($output);
            return false;
        } else {
            echo "Watermarking Succeeded: ".$converted_path.$video_url."\n";
            return $video_url;
        }
    }





    // public static function createGif($video_url, $converted_path) {
    //     $vid = explode("/", $video_url);
    //     $vid_title = end($vid);
    //     $gif_title = str_replace('.mp4', '.gif', $vid_title);
    //     $gif_url = str_replace('.mp4', '.gif', $video_url);
    //     $base_path = storage_path('app/');

    //     // Get the video duration in seconds
    //     $command_duration = "ffprobe -i {$base_path}{$video_url} -show_entries format=duration -v quiet -of csv='p=0'";
    //     exec($command_duration, $duration_output, $duration_return_var);
    //     $video_duration = floatval($duration_output[0]);

    //     // Set the start time for GIF creation
    //     $start_time = ($video_duration > 5) ? 5 : 1;

    //     // Get the video resolution
    //     $command_resolution = "ffprobe -i {$base_path}{$video_url} -show_entries stream=width,height -v quiet -of csv='p=0'";
    //     exec($command_resolution, $resolution_output, $resolution_return_var);
    //     list($width, $height) = explode(',', $resolution_output[0]);

    //     // Ensure that the width and height are above the minimum supported resolution for h264_nvenc
    //     $min_width = 128;
    //     $min_height = 128;

    //     // If the video width or height is below the minimum, adjust the scale
    //     if ($width < $min_width || $height < $min_height) {
    //         $width = $min_width;
    //         $height = $min_height;
    //     }

    //     // Generate GIF starting at $start_time without using h264_nvenc codec
    //     $command = 'ffmpeg -y -ss ' . $start_time . ' -t 0.5 -i ' . $base_path . $video_url . ' -vf "fps=3,scale=' . $width . ':' . $height . ':flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 ' . $converted_path . $gif_url . ' 2>&1';

    //     exec($command, $output, $return_var);

    //     if ($return_var != 0) {
    //         Log::error("GIF creation failed", ['output' => $output]);
    //         return false;
    //     }

    //     Log::info("GIF created successfully", ['gif_url' => $gif_url]);
    //     return str_replace('.mp4', '.gif', $video_url);
    // }

    public static function createGif($video_url, $converted_path) {
        $vid = explode("/", $video_url);
        $vid_title = end($vid);
        $gif_title = str_replace('.mp4', '.gif', $vid_title);
        $gif_url = str_replace('.mp4', '.gif', $video_url);
        $base_path = storage_path('app/');

        // Get the video duration in seconds
        $video_duration = shell_exec("ffprobe -i {$base_path}{$video_url} -show_entries format=duration -v quiet -of csv='p=0'");
        $video_duration = floatval($video_duration);

        // Set the start time for GIF creation
        $start_time = ($video_duration > 11) ? 11 : 1;

        // Generate GIF starting at $start_time
        exec('taskset -c 0,1,2 ffmpeg -ss ' . $start_time . ' -t 0.5 -i ' . $base_path . $video_url . ' -vf "fps=3,scale=120:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 ' . $converted_path . $gif_url . ' 2>&1', $output, $return_var);

        if ($return_var != 0) {
            Log::error("GIF creation failed", ['output' => $output]);
            return false;
        }

        Log::info("GIF created successfully", ['gif_url' => $gif_url]);
        return str_replace('.mp4', '.gif', $video_url);
    }

    public static function generateGif($video_url) {
        $gif_url= str_replace('.mp4', '.gif', $video_url);
        $base_path = storage_path('app/');
        exec('taskset -c 0,1,2 ffmpeg -t 0.5 -i '.$base_path.$video_url.' -vf "fps=3,scale=120:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0  '.$base_path.''.$gif_url.' 2>&1', $output, $return_var);

        return str_replace('.mp4', '.gif', $video_url);
    }




    ##helper function to scaledown videos
    public static function scaleDownVideo($video_url,$watermark_path,$converted_path) {


        $vid = explode("/", $video_url);
        $vid_title = end($vid);
        $base_path = storage_path('app/');



        exec('taskset -c 0,1,2 ffmpeg -i '.$base_path.$video_url.' -vf "scale=360:640,fps=30" -acodec copy -y '.$converted_path.$video_url, $output, $return_var);



       return $video_url;




    }


    public static function prepareVideo($filePath)
    {
        $resolution = '540x960'; // Example resolution
        $fps = '30'; // Example frame rate

        $fileInfo = pathinfo($filePath);
        $extension = strtolower($fileInfo['extension']);
        $outputVideoPath = $fileInfo['dirname'] . '/' . time() . '_' . $fileInfo['filename'] . '_scaled.mp4';

        // Convert HEIC to JPEG if needed
        if ($extension === 'heic') {
            $jpegFilePath = $fileInfo['dirname'] . '/' . $fileInfo['filename'] . '.jpg';
            exec("convert \"$filePath\" \"$jpegFilePath\"", $output, $return_var);
            if ($return_var !== 0) {
                echo "ImageMagick conversion of HEIC to JPEG failed.\n";
                return false;
            }
            $filePath = $jpegFilePath;
            $extension = 'jpg';
        }

        if (in_array($extension, ['png', 'jpeg', 'jpg'])) {
            // Convert static images (PNG, JPEG, etc.) to video
            exec("taskset -c 0,1 ffmpeg -y -loop 1 -i \"$filePath\" -c:v libx264 -t 1 -pix_fmt yuv420p -vf 'scale=$resolution,format=yuv420p' -r $fps \"$outputVideoPath\"");
        } elseif ($extension === 'gif') {
            // Convert GIF to MP4
            exec("taskset -c 0,1 ffmpeg -y -i \"$filePath\" -t 1 -movflags faststart -pix_fmt yuv420p -vf 'scale=$resolution,fps=$fps' \"$outputVideoPath\"");
        } elseif ($extension === 'webp') {
            exec("taskset -c 0,1 ffmpeg -y -i \"$filePath\" -t 1 -c:v libx264 -pix_fmt yuv420p -vf 'scale=$resolution' \"$outputVideoPath\"");

        } else {
            // Scale existing video formats (e.g., MP4)
            exec("taskset -c 0,1 ffmpeg -y -i \"$filePath\" -vf 'scale=$resolution,fps=$fps' -c:v libx264 -pix_fmt yuv420p \"$outputVideoPath\"");
        }

        // Verify the output file was created successfully
        return file_exists($outputVideoPath) ? $outputVideoPath : false;
    }



}
