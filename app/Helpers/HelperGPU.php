<?php

namespace App\Helpers;

use App\SignupStep;
use App\Videos;
//use Google\Cloud\Firestore\FirestoreClient;
use Illuminate\Support\Facades\Storage;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client as TwilioClient;
use Twilio\Exceptions\TwilioException;

use Illuminate\Support\Facades\Log;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\ServiceAccount;
use Exception;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;

class HelperGPU {



    // public static function getTwilioObject($debug = true) {
    //     //https://support.twilio.com/hc/en-us/articles/223183808-Why-aren-t-my-test-credentials-working-
    //     if(config('app.debug') && $debug === true) {
    //         // $twilio_number = config('app.TWILIO_NUMBER_TEST', null);
    //         $twilio_account_sid = config('app.TWILIO_ACCOUNT_SID_TEST', null);
    //         $twilio_account_auth_token = config('app.TWILIO_ACCOUNT_AUTH_TOKEN_TEST', null);
    //     }
    //     else {
    //         // $twilio_number = config('app.TWILIO_NUMBER', null);
    //         $twilio_account_sid = config('app.TWILIO_ACCOUNT_SID', null);
    //         $twilio_account_auth_token = config('app.TWILIO_ACCOUNT_AUTH_TOKEN', null);
    //     }
    //     //Secrete key guide https://www.screencast.com/t/fJ0Epgzo
    //     return new TwilioClient($twilio_account_sid, $twilio_account_auth_token);//Successfully working
    // }

    public static function getTwilioObject($debug = true)
    {
        try {
            // Determine if in debug mode and set appropriate credentials
            if (config('app.debug') && $debug === true) {
                $twilio_account_sid = config('app.TWILIO_ACCOUNT_SID_TEST', null);
                $twilio_account_auth_token = config('app.TWILIO_ACCOUNT_AUTH_TOKEN_TEST', null);
            } else {
                $twilio_account_sid = config('app.TWILIO_ACCOUNT_SID', null);
                $twilio_account_auth_token = config('app.TWILIO_ACCOUNT_AUTH_TOKEN', null);
            }

            // Check if SID and Auth Token are provided
            if (empty($twilio_account_sid) || empty($twilio_account_auth_token)) {
                throw new Exception("Twilio credentials are missing. Please check your configuration.");
            }

            // Create and return the Twilio Client instance
            return new TwilioClient($twilio_account_sid, $twilio_account_auth_token);
        } catch (ConfigurationException $e) {
            // Handle Twilio-specific configuration exceptions
            logger()->error("Twilio Configuration Exception: " . $e->getMessage());
            throw new Exception("Failed to configure Twilio client. Please check your credentials.");
        } catch (Exception $e) {
            // Handle other exceptions
            logger()->error("General Exception: " . $e->getMessage());
            throw new Exception("An error occurred while creating the Twilio client: " . $e->getMessage());
        }
    }

    // public static function deleteTwilioService($twilio, $twilio_service_sid) {
    //     $twilio->verify->v2->services($twilio_service_sid)->delete();
    // }

    public static function deleteTwilioService($twilio, $twilio_service_sid)
    {
        try {
            // Ensure the Twilio service SID is provided
            if (empty($twilio_service_sid)) {
                throw new Exception("Twilio service SID is required for deletion.");
            }

            // Attempt to delete the service
            $twilio->verify->v2->services($twilio_service_sid)->delete();
        } catch (TwilioException $e) {
            // Handle Twilio-specific exceptions
            logger()->error("Twilio Exception: " . $e->getMessage());
            throw new Exception("Failed to delete the Twilio service: " . $e->getMessage());
        } catch (Exception $e) {
            // Handle general exceptions
            logger()->error("General Exception: " . $e->getMessage());
            throw new Exception("An error occurred while deleting the Twilio service: " . $e->getMessage());
        }
    }

    // public static function putS3Image($file_path, $image, $type) {
    //     $confirmUpload = Storage::disk('s3')->put($file_path, $image, $type);
    //     if ($confirmUpload)
    //         return true;
    //     else
    //         return false;
    // }


    public static function putS3Image($file_path, $image, $type) {
        try {
            $confirmUpload = Storage::disk('s3')->put($file_path, $image, $type);

            if ($confirmUpload) {
                Log::info("Successfully uploaded to S3: " . $file_path);
                return true;
            } else {
                Log::error("Failed to upload to S3: " . $file_path);
                return false;
            }
        } catch (Exception $e) {
            Log::error("Error uploading to S3: " . $e->getMessage());
            return false;
        }
    }

    // public static function putServerImage($file_path, $image, $type) {
    //     $confirmUpload = Storage::put($file_path, $image, $type);
    //     if ($confirmUpload)
    //         return true;
    //     else
    //         return false;
    // }

    public static function putServerImage($file_path, $image, $type)
    {
        try {
            // Validate inputs
            if (empty($file_path)) {
                throw new Exception("File path cannot be empty.");
            }

            if (empty($image)) {
                throw new Exception("Image content cannot be empty.");
            }

            if (empty($type)) {
                throw new Exception("File type must be specified.");
            }

            // Attempt to upload the image to the server
            $confirmUpload = Storage::put($file_path, $image, $type);

            if ($confirmUpload) {
                return true;
            } else {
                throw new Exception("Failed to upload the image to the server.");
            }
        } catch (Exception $e) {
            // Log the error
            logger()->error("Error in putServerImage: " . $e->getMessage());

            // Optionally, rethrow the exception if you want the caller to handle it
            throw new Exception("An error occurred while uploading the image: " . $e->getMessage());
        }
    }


    public static function putServerImageR($file_path, $image, $type)
    {
        try {
            // Use fopen for memory efficiency on large files
            $stream = is_resource($image) ? $image : fopen($image, 'r+');

            $confirmUpload = Storage::put($file_path, $stream, $type);

            // Close stream if we opened it
            if (!is_resource($image)) {
                fclose($stream);
            }

            // Check if the file was uploaded
            if ($confirmUpload) {
                return $file_path; // Return path for further reference
            } else {
                Log::error("File upload failed for path: {$file_path}");
                return false;
            }
        } catch (Exception $e) {
            Log::error("Error uploading file to {$file_path}: " . $e->getMessage());
            return false;
        }
    }




    public static function sendFCMNotification($deviceTokens, $input)
    {
        try {
            // Initialize Firebase with credentials
            $firebase = (new Factory)
                ->withServiceAccount(storage_path('app/firebase/caret-7f711-firebase-adminsdk-1wn14-a2c2d521d1.json'))
                ->createMessaging();

            // Prepare the notification data
            if (is_array($deviceTokens)) {
                foreach($deviceTokens as $deviceToken) {
                    $message = CloudMessage::withTarget('token', $deviceToken)
                        ->withNotification([
                            'title' => $input['title'],
                            'body'  => $input['body'],
                        ])
                        ->withData($input['data']);  // Additional data if needed

                    // Send the notification and capture the response
                    $response = $firebase->send($message);

                    // Log the response
                    Log::info("1 - FCM Notification sent to {$deviceToken}: " . json_encode($response));
                }
            } else {
                if (count($deviceTokens) === 1) {
                    $device_tokens[] = $deviceTokens;
                }
                $message = CloudMessage::withTarget('token', $device_tokens)
                    ->withNotification([
                        'title' => $input['title'],
                        'body'  => $input['body'],
                    ])
                    ->withData($input['data']);  // Additional data if needed

                // Send the notification and capture the response
                $response = $firebase->send($message);

                // Log the response
                Log::info("2 - FCM Notification sent to {$device_tokens}: " . json_encode($response));
            }

        } catch (Exception $e) {
            Log::error("FCM Notification failed: " . $e->getMessage());
        }
    }

    // public static function getFireStoreConnection() {
    //     // Define the path to your credentials file
    //     $firebase_json_file = storage_path('app/firebase/caret-7f711-firebase-adminsdk-1wn14-a2c2d521d1.json');

    //     // Create a Firebase instance with Firestore support
    //     $firebase = (new Factory)
    //         ->withServiceAccount($firebase_json_file)
    //         ->createFirestore();

    //     return $firebase->database();
    // }

    public static function getFireStoreConnection()
    {
        try {
            // Define the path to your credentials file
            $firebase_json_file = storage_path('app/firebase/caret-7f711-firebase-adminsdk-1wn14-a2c2d521d1.json');

            // Check if the credentials file exists
            if (!file_exists($firebase_json_file)) {
                throw new Exception("Firebase credentials file not found at: $firebase_json_file");
            }

            // Create a Firebase instance with Firestore support
            $firebase = (new Factory)
                ->withServiceAccount($firebase_json_file)
                ->createFirestore();

            return $firebase->database();
        } catch (AuthException $e) {
            // Handle authentication-specific exceptions
            logger()->error("Firebase Auth Exception: " . $e->getMessage());
            throw new Exception("Failed to authenticate with Firebase. Please check your credentials.");
        } catch (FirebaseException $e) {
            // Handle general Firebase exceptions
            logger()->error("Firebase Exception: " . $e->getMessage());
            throw new Exception("Failed to connect to Firebase. Please try again later.");
        } catch (Exception $e) {
            // Handle other exceptions
            logger()->error("General Exception: " . $e->getMessage());
            throw new Exception("An error occurred while connecting to Firestore: " . $e->getMessage());
        }
    }


    // public static function convertToMp4($input_file)
    // {
    //     if (pathinfo($input_file, PATHINFO_EXTENSION) === 'mp4') {
    //         echo "Input file is already in MP4 format: $input_file\n";
    //         $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '_.mp4';
    //     } else {
    //         // Define output file path
    //         $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '.mp4';
    //     }

    //     // FFmpeg options for optimization
    //     $preset = 'fast'; // Speed optimization (use 'fast', 'faster', or 'medium' for a balance)
    //     $crf = 21; // Constant Rate Factor (lower is higher quality, default is 23)
    //     $resolution = '540x960'; // Optional scaling (leave empty if you don't want to change resolution)
    //     $fps = '30'; // Frame rate (optional)

    //     // Use CUDA hardware acceleration (NVIDIA GPU)
    //     $hardware_acceleration = '-hwaccel cuda -c:v h264_nvenc'; // Use CUDA for decoding and NVENC for encoding

    //     // Construct FFmpeg command with GPU acceleration
    //     $ffmpeg_cmd = "ffmpeg -y $hardware_acceleration -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:a aac -strict -2 \"$output_file\" 2>&1";

    //     // Execute the FFmpeg command
    //     exec($ffmpeg_cmd, $output, $return_var);

    //     // Check for errors
    //     if ($return_var != 0) {
    //         echo "FFmpeg Conversion Failed:\n";
    //         print_r($output);
    //         return false;
    //     } else {
    //         echo "Video Conversion Succeeded: $output_file\n";
    //         return $output_file;
    //     }
    // }

    public static function convertToMp4($input_file)
    {
        try {
            // Validate input file existence
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // Determine if the file is already in MP4 format
            $file_info = pathinfo($input_file);
            $is_mp4 = strtolower($file_info['extension']) === 'mp4';
            $output_file = $file_info['dirname'] . '/' . $file_info['filename'] . ($is_mp4 ? '_converted.mp4' : '.mp4');

            // FFmpeg options
            $preset = 'fast'; // Speed optimization
            $crf = 21; // Quality setting
            $resolution = '540x960'; // Resolution scaling
            $fps = '30'; // Frame rate

            // Use CUDA hardware acceleration (if supported)
            $hardware_acceleration = '-hwaccel cuda -c:v h264_nvenc'; // NVIDIA GPU acceleration

            // Construct the FFmpeg command
            $ffmpeg_cmd = escapeshellcmd("ffmpeg -y $hardware_acceleration -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:a aac -strict -2 \"$output_file\" 2>&1");

            // Execute the command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg conversion failed: $error_message");
            }

            // Success message
            echo "Video conversion succeeded: $output_file\n";
            return $output_file;
        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToMp4: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }


    // public static function convertToMp4CPU($input_file)
    // {

    //     if (pathinfo($input_file, PATHINFO_EXTENSION) === 'mp4') {
    //         echo "Input file is already in MP4 format: $input_file\n";
    //         //return $input_file;
    //         $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '_.mp4';
    //     }else{

    //         // Define output file path
    //         $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '.mp4';

    //     }

    //     // FFmpeg options for optimization
    //     $preset = 'fast'; // Speed optimization (use 'fast', 'faster', or 'medium' for a balance)
    //     $crf = 21; // Constant Rate Factor (lower is higher quality, default is 23)
    //     $resolution = '540x960'; // Optional scaling (leave empty if you don't want to change resolution)
    //     $fps = '30'; // Frame rate (optional)

    //     // Check if hardware acceleration is available
    //     // NVIDIA GPU acceleration (optional)
    //     $hardware_acceleration = ''; // For NVIDIA: '-hwaccel nvdec -c:v h264_nvenc'

    //     // Construct FFmpeg command
    //     $ffmpeg_cmd = "taskset -c 0,1,2 ffmpeg -y $hardware_acceleration -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:a aac -strict -2 \"$output_file\" 2>&1";

    //     // Execute the FFmpeg command
    //     exec($ffmpeg_cmd, $output, $return_var);

    //     // Check for errors
    //     if ($return_var != 0) {
    //         echo "FFmpeg Conversion Failed:\n";
    //         print_r($output);
    //         return false;
    //     } else {
    //         echo "Video Conversion Succeeded: $output_file\n";
    //         return $output_file;
    //     }
    // }

    public static function convertToMp4CPU($input_file)
    {
        try {
            // Validate the input file
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // Determine if the file is already in MP4 format
            $file_info = pathinfo($input_file);
            $is_mp4 = strtolower($file_info['extension']) === 'mp4';
            $output_file = $file_info['dirname'] . '/' . $file_info['filename'] . ($is_mp4 ? '_converted.mp4' : '.mp4');

            // FFmpeg options for optimization
            $preset = 'fast'; // Speed optimization
            $crf = 21; // Quality setting
            $resolution = '540x960'; // Resolution scaling
            $fps = '30'; // Frame rate

            // Use CPU cores explicitly (taskset)
            $cpu_cores = '0,1,2'; // Adjust core allocation as needed
            $taskset_prefix = "taskset -c $cpu_cores";

            // Construct FFmpeg command
            $ffmpeg_cmd = "$taskset_prefix ffmpeg -y -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:a aac -strict -2 \"$output_file\" 2>&1";

            // Execute the FFmpeg command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg conversion failed: $error_message");
            }

            // Success message
            echo "Video conversion succeeded: $output_file\n";
            // self::convertToHLS($output_file);
            return $output_file;
        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToMp4CPU: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }

    public static function convertToHLS_GPU($video_file, $video_id)
    {
        try {
            $input_file = storage_path('app/' . $video_file);

            // Validate the input file
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // File info and output directory
            $file_info = pathinfo($input_file);
            $output_dir = storage_path('app/caret_videos/' . $video_id);
            if (!is_dir($output_dir)) {
                mkdir($output_dir, 0777, true);
            }

            // Output playlist and segment files
            $output_playlist = $output_dir . '/output.m3u8';
            $segment_filename = $output_dir . '/chunk_%03d.ts';

            // FFmpeg options for HLS
            $segment_duration = 2; // Segment duration in seconds
            $resolution = '540x960'; // Resolution scaling (adjust as needed)
            $fps = 25; // Frame rate (optimized to 25fps for a balance of quality and compression)
            $preset = 'slow'; // NVENC preset for better quality (can use 'p4', 'p5', 'p6' for faster/slower)
            $cq = 21; // Constant Quality for NVENC (range 18-28, 21 is a good balance between quality and compression)
            $bitrate = '1500k'; // Target bitrate for video stream (adjust for desired quality/size)
            $keyframe_interval = 2; // Enforce keyframes every 2 seconds (adjust as needed)

            // Construct GPU-optimized FFmpeg command for H.264 encoding with NVENC
            $ffmpeg_cmd = "ffmpeg -y -hwaccel cuda -i \"$input_file\" " .
                "-vf \"scale=$resolution,fps=$fps\" " .
                "-c:v h264_nvenc -b:v $bitrate -preset $preset -cq $cq -g " . ($fps * $keyframe_interval) . " -keyint_min " . ($fps * $keyframe_interval) . " -sc_threshold 0 " .
                "-force_key_frames \"expr:gte(t,n*$segment_duration)\" " .
                "-c:a aac -b:a 128k -ac 2 " .
                "-hls_time $segment_duration -hls_playlist_type vod " .
                "-hls_segment_filename \"$segment_filename\" \"$output_playlist\" 2>&1";

            // Execute the FFmpeg command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg HLS conversion failed: $error_message");
            }

            // Sleep for 5 seconds before pushing to S3
            sleep(5);

            // Define the S3 folder path using the extracted ID
            $s3_folder = 'Videos/' . $video_id;

            // Get all files in the output directory
            $files = glob($output_dir . '/*');

            foreach ($files as $file) {
                $file_name = basename($file);
                $s3_path = $s3_folder . '/' . $file_name;
                $file_content = file_get_contents($file);

                $upload_success = self::putS3Image($s3_path, $file_content, 'public');

                if (!$upload_success) {
                    throw new Exception("Failed to upload $file_name to S3.");
                }
            }

            // Update the video entry in the database
            $FinalUpdate['video_url'] = 'Videos' . '/'.$video_id.'/output.m3u8';
            $FinalUpdate['is_m3u8'] = 1;
            Videos::where('id',$video_id)->update($FinalUpdate);

        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToHLS_GPU: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }


    public static function convertToHLS($video_file,$video_id)
    {
        Log::info("HelperGPU::convertToHLS");
        try {
            $input_file = storage_path('app/' . $video_file);
            // Validate the input file
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // File info and output directory
            $file_info = pathinfo($input_file);
            //$file_parts = explode('_', $file_info['filename']);
            //$video_id = $file_parts[0]; // The first part is the ID

            $output_dir = $file_info['dirname'] . '/' . $video_id;
            if (!is_dir($output_dir)) {
                mkdir($output_dir, 0777, true);
            }

            // Output playlist and segment files
            $output_playlist = $output_dir . '/output.m3u8';
            $segment_filename = $output_dir . '/chunk_%03d.ts';

            // FFmpeg options for HLS
            $segment_duration = 3; // Segment duration in seconds
            $resolution = '540x960'; // Resolution scaling
            $fps = '30'; // Frame rate
            $preset = 'fast'; // Speed optimization
            $crf = 21; // Quality setting

            // Use CPU cores explicitly (taskset)
            $cpu_cores = '0,1,2,3,4,5'; // Adjust core allocation as needed
            $taskset_prefix = "taskset -c $cpu_cores";

            // Construct FFmpeg command
            $ffmpeg_cmd = "$taskset_prefix ffmpeg -y -i \"$input_file\" -vf scale=$resolution,fps=$fps -preset $preset -crf $crf -c:v libx264 -b:v 2000k -g 90 -keyint_min 90 -sc_threshold 0 -c:a aac -strict -2 -hls_time $segment_duration -hls_flags split_by_time -hls_playlist_type vod -hls_segment_filename \"$segment_filename\" \"$output_playlist\" 2>&1";



            // Execute the FFmpeg command
            exec($ffmpeg_cmd, $output, $return_var);

            // Check for FFmpeg execution result
            if ($return_var !== 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg HLS conversion failed: $error_message");
            }

            // Success message
            //echo "HLS conversion succeeded: Playlist created at $output_playlist\n";
            sleep(5);
             // Push files to S3



             // Define the S3 folder path using the extracted ID
             $s3_folder = 'Videos/' . $video_id;

             // Get all files in the output directory
             $files = glob($output_dir . '/*');

            foreach ($files as $file) {
                $file_name = basename($file);
                $s3_path = $s3_folder . '/' . $file_name;
                $file_content = file_get_contents($file);

                $upload_success = self::putS3Image($s3_path, $file_content, 'public');

                if (!$upload_success) {
                    throw new Exception("Failed to upload $file_name to S3.");
                }
            }
            $FinalUpdate['is_m3u8'] = 1;
            $FinalUpdate['video_url'] = 'Videos' . '/'.$video_id.'/output.m3u8';
            Videos::where('id',$video_id)->update($FinalUpdate);

            //return $output_playlist;
        } catch (Exception $e) {
            // Log and display errors
            logger()->error("Error in convertToHLS: " . $e->getMessage());
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }

    // public static function convertToHLS($video_file, $video_id)
    // {
    //     try {
    //         $input_file = storage_path('app/' . $video_file);

    //         // Validate the input file
    //         if (!file_exists($input_file)) {
    //             throw new Exception("Input file does not exist: $input_file");
    //         }

    //         // File info and output directory
    //         $file_info = pathinfo($input_file);
    //         $output_dir = $file_info['dirname'] . '/' . $video_id;
    //         if (!is_dir($output_dir)) {
    //             mkdir($output_dir, 0777, true);
    //         }

    //         // Output playlist and segment files
    //         $output_playlist = $output_dir . '/output.m3u8';
    //         $segment_filename = $output_dir . '/chunk_%03d.ts';

    //         // FFmpeg options for HLS
    //         $segment_duration = 2; // Strictly enforce 2s segment duration
    //         $resolution = '540x960';
    //         $fps = '30';
    //         $preset = 'slow'; // Using 'slow' preset for better compression efficiency (higher quality)
    //         $crf = 24; // Higher value for better compression but still good quality (range is 18-28)

    //         // Enforce keyframes every 2 seconds
    //         $keyframe_interval = 2; // Keyframe every 2 seconds

    //         // Construct FFmpeg command for HEVC encoding with optimizations
    //         $ffmpeg_cmd = "ffmpeg -y -i \"$input_file\" "
    //         . "-vf \"scale=$resolution,fps=$fps\" "
    //         . "-preset $preset -crf $crf "
    //         . "-c:v libx265 -b:v 1500k -g " . ($fps * 2) . " -keyint_min " . ($fps * 2) . " -sc_threshold 0 "
    //         . "-c:a aac -b:a 128k -ac 2 -strict -2 "
    //         . "-hls_time $segment_duration -hls_playlist_type vod "
    //         . "-force_key_frames \"expr:gte(t,n*$segment_duration)\" "
    //         . "-hls_segment_filename \"$segment_filename\" \"$output_playlist\" 2>&1";

    //         // Execute the FFmpeg command
    //         exec($ffmpeg_cmd, $output, $return_var);

    //         // Check for FFmpeg execution result
    //         if ($return_var !== 0) {
    //             $error_message = implode("\n", $output);
    //             throw new Exception("FFmpeg HLS conversion failed: $error_message");
    //         }

    //         sleep(5);

    //         // Push files to S3
    //         $s3_folder = 'Videos/' . $video_id;
    //         $files = glob($output_dir . '/*');

    //         foreach ($files as $file) {
    //             $file_name = basename($file);
    //             $s3_path = $s3_folder . '/' . $file_name;
    //             $file_content = file_get_contents($file);

    //             $upload_success = self::putS3Image($s3_path, $file_content, 'public');

    //             if (!$upload_success) {
    //                 throw new Exception("Failed to upload $file_name to S3.");
    //             }
    //         }

    //         $FinalUpdate['is_m3u8'] = 1;
    //         $FinalUpdate['video_url'] = 'Videos' . '/'.$video_id.'/output.m3u8';
    //         Videos::where('id', $video_id)->update($FinalUpdate);

    //     } catch (Exception $e) {
    //         logger()->error("Error in convertToHLS: " . $e->getMessage());
    //         echo "Error: " . $e->getMessage() . "\n";
    //         return false;
    //     }
    // }



    public static function copyS3Video($videoUrl)
    {
        Log::info("copyS3Video -> Copy File: $videoUrl");

        $localPaths = [];
        $errors = [];


            $vidTitle = explode("/", $videoUrl);
            $fileName = end($vidTitle);
            $localPath = $videoUrl; // Relative path within the storage/app directory
            // Debugging: Check resolved local path
            $resolvedPath = storage_path('app/' . $localPath);

            // Check if the file exists locally
            if (!Storage::disk('local')->exists($localPath)) {
                try {
                    if (Storage::disk('s3')->exists($videoUrl)) {

                        // Download the file from S3
                        $fileContents = Storage::disk('s3')->get($videoUrl);
                        // Ensure the directory exists
                        Storage::disk('local')->makeDirectory(dirname($localPath));
                        // Save the file locally
                        Storage::disk('local')->put($localPath, $fileContents);

                        $localPaths[] = $resolvedPath;

                        Log::info("copyS3Video -> File Downloaded");

                    } else {
                        $errors[] = "File not found on S3: $videoUrl";
                        Log::error("File not found on S3: $videoUrl");
                    }
                } catch (Exception $e) {
                    $errors[] = "Failed to download File: $fileName. Error: {$e->getMessage()}";
                    Log::error("Failed to download File: $fileName. Error: {$e->getMessage()}");
                }
            } else {
                Log::info("copyS3Video -> Copy File Already Exist");
                $localPaths[] = $resolvedPath;

            }
        if (count($localPaths) > 0) {
            return true;
        } else {
            return false;
        }
    }


    public static function convertToMp3($input_file) {
        // Check if the input file is already in MP3 format
        if (pathinfo($input_file, PATHINFO_EXTENSION) === 'mp3') {
            echo "Input file is already in MP3 format: $input_file\n";
            return $input_file;
        }

        $fileInfo = pathinfo($input_file);
        $extension = strtolower($fileInfo['extension']);

        // Define the output file path by replacing the original extension with .mp3
        $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '.mp3';

        // Use GPU hardware acceleration for supported formats
        $hardware_acceleration = '-hwaccel nvdec';  // For NVIDIA GPUs (or you can use other hardware acceleration options like vaapi for Intel/AMD)

        // Execute the FFmpeg command to convert the input file to MP3
        if (in_array($extension, ['wma', 'flac', 'aac', 'mp4'])) {
            exec("ffmpeg -y $hardware_acceleration -i \"$input_file\" -vn -acodec libmp3lame -b:a 192k \"$output_file\" 2>&1", $output, $return_var);
        } elseif (in_array($extension, ['mp3', 'mpega', 'wav'])) {
            exec("ffmpeg -y $hardware_acceleration -i $input_file -vn -ar 44100 -ac 2 -b:a 192k $output_file 2>&1", $output, $return_var);
        } else {
            return false;
        }

        // Check if the command was successful
        if ($return_var != 0) {
            echo "FFmpeg Conversion Failed:\n";
            print_r($output);
            return false;
        } else {
            echo "Audio Conversion Succeeded: $output_file\n";
            return $output_file;
        }
    }


    public static function convertToMp3GPU($input_file) {
        try {
            // Check if the input file exists
            if (!file_exists($input_file)) {
                throw new Exception("Input file does not exist: $input_file");
            }

            // Check if the input file is already in MP3 format
            if (pathinfo($input_file, PATHINFO_EXTENSION) === 'mp3') {
                echo "Input file is already in MP3 format: $input_file\n";
                return $input_file;
            }

            $fileInfo = pathinfo($input_file);
            $extension = strtolower($fileInfo['extension']);

            // Define the output file path by replacing the original extension with .mp3
            $output_file = pathinfo($input_file, PATHINFO_DIRNAME) . '/' . pathinfo($input_file, PATHINFO_FILENAME) . '.mp3';

            // Use GPU hardware acceleration for supported formats
            $hardware_acceleration = '-hwaccel nvdec';  // For NVIDIA GPUs (or you can use other hardware acceleration options like vaapi for Intel/AMD)

            // Execute the FFmpeg command to convert the input file to MP3
            if (in_array($extension, ['wma', 'flac', 'aac', 'mp4'])) {
                exec("ffmpeg -y $hardware_acceleration -i \"$input_file\" -vn -acodec libmp3lame -b:a 192k \"$output_file\" 2>&1", $output, $return_var);
            } elseif (in_array($extension, ['mp3', 'mpega', 'wav'])) {
                exec("ffmpeg -y $hardware_acceleration -i $input_file -vn -ar 44100 -ac 2 -b:a 192k $output_file 2>&1", $output, $return_var);
            } else {
                throw new Exception("Unsupported file format: $extension");
            }

            // Check if the command was successful
            if ($return_var != 0) {
                $error_message = implode("\n", $output);
                throw new Exception("FFmpeg conversion failed: $error_message");
            } else {
                echo "Audio Conversion Succeeded: $output_file\n";
                return $output_file;
            }
        } catch (Exception $e) {
            // Display the error message
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }





    // public static function addWatermark($video_url, $watermark_path, $converted_path)
    // {
    //     $vid = explode("/", $video_url);
    //     $vid_title = end($vid);
    //     $base_path = storage_path('app/');

    //     $output = [];
    //     $return_var = 0;

    //     // Use faster preset for encoding
    //     $preset = 'fast'; // Options: ultrafast, superfast, faster, fast, medium, slow, slower
    //     $cq = 23; // Adjust this value for quality control (lower value = better quality, larger file size)

    //     // Escaping paths for FFmpeg command
    //     $escaped_video_url = escapeshellarg($base_path . $video_url);

    //     $escaped_output_path = escapeshellarg($converted_path.$video_url);

    //     // FFmpeg command for adding a watermark


    //     $command = 'ffmpeg -y -hwaccel cuda -i ' . $escaped_video_url . ' -i ' . $watermark_path .' -filter_complex "[1:v]scale=60:-1,format=yuva444p,fade=t=out:st=5:d=1[watermark];[0:v][watermark]overlay=W-w-10:100" ' .'-preset ' . $preset . ' -cq ' . $cq . ' -c:v h264_nvenc -c:a copy ' . $escaped_output_path . ' 2>&1';



    //     exec($command, $output, $return_var);

    //     // Logging the command and output for debugging
    //     Log::info("Single Video: ".$command);
    //             if ($return_var != 0) {
    //                 Log::error("Video failed", [
    //                     'command' => $command,
    //                     'output' => $output,
    //                     'return_code' => $return_var,
    //                 ]);
    //                 return false;
    //             }

    //     return $video_url;
    // }


    public static function addWatermark($video_url, $watermark_path, $converted_path)
    {
        try {
            $vid = explode("/", $video_url);
            $vid_title = end($vid);
            $base_path = storage_path('app/');

            $output = [];
            $return_var = 0;

            // Use faster preset for encoding
            $preset = 'fast'; // Options: ultrafast, superfast, faster, fast, medium, slow, slower
            $cq = 23; // Adjust this value for quality control (lower value = better quality, larger file size)

            // Check if the video and watermark file exist
            if (!file_exists($base_path . $video_url)) {
                throw new Exception("Video file does not exist: $video_url");
            }

            if (!file_exists($watermark_path)) {
                throw new Exception("Watermark file does not exist: $watermark_path");
            }

            // Escaping paths for FFmpeg command
            $escaped_video_url = escapeshellarg($base_path . $video_url);
            $escaped_output_path = escapeshellarg($converted_path . $video_url);

            // FFmpeg command for adding a watermark
            $command = 'ffmpeg -y -hwaccel cuda -i ' . $escaped_video_url . ' -i ' . $watermark_path .' -filter_complex "[1:v]scale=60:-1,format=yuva444p,fade=t=out:st=5:d=1[watermark];[0:v][watermark]overlay=W-w-10:100" ' .'-preset ' . $preset . ' -cq ' . $cq . ' -c:v h264_nvenc -c:a copy ' . $escaped_output_path . ' 2>&1';

            exec($command, $output, $return_var);

            // Logging the command and output for debugging
            Log::info("Single Video: ".$command);

            // Check for FFmpeg command execution failure
            if ($return_var != 0) {
                throw new Exception("FFmpeg command failed: " . implode("\n", $output));
            }

            return $video_url;
        } catch (Exception $e) {
            // Log the error and display the exception message
            Log::error("Error adding watermark to video", [
                'message' => $e->getMessage(),
                'video_url' => $video_url,
                'watermark_path' => $watermark_path,
                'converted_path' => $converted_path
            ]);
            return false;
        }
    }




    // public static function addWatermarkCPU($video_url, $watermark_path, $converted_path)
    // {
    //     $vid = explode("/", $video_url);
    //     $vid_title = end($vid);
    //     $base_path = storage_path('app/');

    //     $output = [];
    //     $return_var = 0;

    //     // Use faster preset for encoding
    //     $preset = 'fast'; // Options: ultrafast, superfast, faster, fast, medium, slow, slower
    //     $crf = 21;

    //     // FFmpeg command for adding a watermark, with faster encoding

    //     exec('taskset -c 0,1,2 ffmpeg -y -i '.$base_path.$video_url.' -i '.$watermark_path.' -filter_complex "[1:v]scale=60:-1,format=yuva444p,fade=t=out:st=5:d=1[watermark]; [0:v][watermark]overlay=W-w-10:100" -preset '.$preset.' -crf '.$crf.' -codec:a copy '.$converted_path.$video_url.' 2>&1', $output, $return_var);





    //     if ($return_var != 0) {
    //         echo "Watermarking Failed:\n";
    //         print_r($output);
    //         return false;
    //     } else {
    //         echo "Watermarking Succeeded: ".$converted_path.$video_url."\n";
    //         return $video_url;
    //     }
    // }

    public static function addWatermarkCPU($video_url, $watermark_path, $converted_path)
    {
        try {
            $vid = explode("/", $video_url);
            $vid_title = end($vid);
            $base_path = storage_path('app/');

            $output = [];
            $return_var = 0;

            // Use faster preset for encoding
            $preset = 'fast'; // Options: ultrafast, superfast, faster, fast, medium, slow, slower
            $crf = 21;

            // Check if the video and watermark file exist
            if (!file_exists($base_path . $video_url)) {
                throw new Exception("Video file does not exist: $video_url");
            }

            if (!file_exists($watermark_path)) {
                throw new Exception("Watermark file does not exist: $watermark_path");
            }

            // FFmpeg command for adding a watermark, with faster encoding
            $command = 'taskset -c 0,1,2 ffmpeg -y -i ' . escapeshellarg($base_path . $video_url) . ' -i ' . escapeshellarg($watermark_path) . ' -filter_complex "[1:v]scale=60:-1,format=yuva444p,fade=t=out:st=5:d=1[watermark]; [0:v][watermark]overlay=W-w-10:100" -preset ' . $preset . ' -crf ' . $crf . ' -codec:a copy ' . escapeshellarg($converted_path . $video_url) . ' 2>&1';

            exec($command, $output, $return_var);

            // Check for FFmpeg command execution failure
            if ($return_var != 0) {
                throw new Exception("FFmpeg command failed: " . implode("\n", $output));
            }

            echo "Watermarking Succeeded: " . $converted_path . $video_url . "\n";
            return $video_url;
        } catch (Exception $e) {
            // Log the error and display the exception message
            Log::error("Error adding watermark to video", [
                'message' => $e->getMessage(),
                'video_url' => $video_url,
                'watermark_path' => $watermark_path,
                'converted_path' => $converted_path
            ]);
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }


    // public static function createGif($video_url, $converted_path) {
    //     $vid = explode("/", $video_url);
    //     $vid_title = end($vid);
    //     $gif_title = str_replace('.mp4', '.gif', $vid_title);
    //     $gif_url = str_replace('.mp4', '.gif', $video_url);
    //     $base_path = storage_path('app/');

    //     // Get the video duration in seconds
    //     $command_duration = "ffprobe -i {$base_path}{$video_url} -show_entries format=duration -v quiet -of csv='p=0'";
    //     exec($command_duration, $duration_output, $duration_return_var);
    //     $video_duration = floatval($duration_output[0]);

    //     // Set the start time for GIF creation
    //     $start_time = ($video_duration > 5) ? 5 : 1;

    //     // Get the video resolution
    //     $command_resolution = "ffprobe -i {$base_path}{$video_url} -show_entries stream=width,height -v quiet -of csv='p=0'";
    //     exec($command_resolution, $resolution_output, $resolution_return_var);
    //     list($width, $height) = explode(',', $resolution_output[0]);

    //     // Ensure that the width and height are above the minimum supported resolution for h264_nvenc
    //     $min_width = 128;
    //     $min_height = 128;

    //     // If the video width or height is below the minimum, adjust the scale
    //     if ($width < $min_width || $height < $min_height) {
    //         $width = $min_width;
    //         $height = $min_height;
    //     }

    //     // Generate GIF starting at $start_time without using h264_nvenc codec
    //     $command = 'ffmpeg -y -ss ' . $start_time . ' -t 0.5 -i ' . $base_path . $video_url . ' -vf "fps=3,scale=' . $width . ':' . $height . ':flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 ' . $converted_path . $gif_url . ' 2>&1';

    //     exec($command, $output, $return_var);

    //     if ($return_var != 0) {
    //         Log::error("GIF creation failed", ['output' => $output]);
    //         return false;
    //     }

    //     Log::info("GIF created successfully", ['gif_url' => $gif_url]);
    //     return str_replace('.mp4', '.gif', $video_url);
    // }

    public static function createGif($video_url, $converted_path)
    {
        try {
            $vid = explode("/", $video_url);
            $vid_title = end($vid);
            $gif_title = str_replace('.mp4', '.gif', $vid_title);
            $gif_url = str_replace('.mp4', '.gif', $video_url);
            $base_path = storage_path('app/');

            // Get the video duration in seconds
            $command_duration = "ffprobe -i {$base_path}{$video_url} -show_entries format=duration -v quiet -of csv='p=0'";
            exec($command_duration, $duration_output, $duration_return_var);

            if ($duration_return_var != 0) {
                throw new Exception("Failed to get video duration.");
            }

            $video_duration = floatval($duration_output[0]);

            // Set the start time for GIF creation
            $start_time = ($video_duration > 3) ? 3 : 1;

            // Get the video resolution
            $command_resolution = "ffprobe -i {$base_path}{$video_url} -show_entries stream=width,height -v quiet -of csv='p=0'";
            exec($command_resolution, $resolution_output, $resolution_return_var);

            if ($resolution_return_var != 0) {
                throw new Exception("Failed to get video resolution.");
            }

            list($width, $height) = explode(',', $resolution_output[0]);

            // Ensure that the width and height are above the minimum supported resolution for h264_nvenc
            $min_width = 128;
            $min_height = 128;

            // If the video width or height is below the minimum, adjust the scale
            if ($width < $min_width || $height < $min_height) {
                $width = $min_width;
                $height = $min_height;
            }

            // Generate GIF starting at $start_time without using h264_nvenc codec
            $command = 'ffmpeg -y -ss ' . $start_time . ' -t 0.5 -i ' . escapeshellarg($base_path . $video_url) . ' -vf "fps=3,scale=' . $width . ':' . $height . ':flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 ' . escapeshellarg($converted_path . $gif_url) . ' 2>&1';

            exec($command, $output, $return_var);

            if ($return_var != 0) {
                throw new Exception("GIF creation failed: " . implode("\n", $output));
            }

            Log::info("GIF created successfully", ['gif_url' => $gif_url]);
            return $gif_url;
        } catch (Exception $e) {
            // Log the error and display the exception message
            Log::error("Error creating GIF", [
                'message' => $e->getMessage(),
                'video_url' => $video_url,
                'converted_path' => $converted_path
            ]);
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }




    // public static function createGifCPU($video_url, $converted_path) {
    //     $vid = explode("/", $video_url);
    //     $vid_title = end($vid);
    //     $gif_title = str_replace('.mp4', '.gif', $vid_title);
    //     $gif_url = str_replace('.mp4', '.gif', $video_url);
    //     $base_path = storage_path('app/');

    //     // Get the video duration in seconds
    //     $video_duration = shell_exec("ffprobe -i {$base_path}{$video_url} -show_entries format=duration -v quiet -of csv='p=0'");
    //     $video_duration = floatval($video_duration);

    //     // Set the start time for GIF creation
    //     $start_time = ($video_duration > 5) ? 5 : 1;

    //     // Generate GIF starting at $start_time
    //     exec('taskset -c 0,1,2 ffmpeg -ss ' . $start_time . ' -t 0.5 -i ' . $base_path . $video_url . ' -vf "fps=3,scale=120:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 ' . $converted_path . $gif_url . ' 2>&1', $output, $return_var);

    //     if ($return_var != 0) {
    //         Log::error("GIF creation failed", ['output' => $output]);
    //         return false;
    //     }

    //     Log::info("GIF created successfully", ['gif_url' => $gif_url]);
    //     return str_replace('.mp4', '.gif', $video_url);
    // }

    public static function createGifCPU($video_url, $converted_path)
    {
        try {
            $vid = explode("/", $video_url);
            $vid_title = end($vid);
            $gif_title = str_replace('.mp4', '.gif', $vid_title);
            $gif_url = str_replace('.mp4', '.gif', $video_url);
            $base_path = storage_path('app/');

            // Get the video duration in seconds
            $video_duration = shell_exec("ffprobe -i {$base_path}{$video_url} -show_entries format=duration -v quiet -of csv='p=0'");
            if ($video_duration === null) {
                throw new Exception("Failed to get video duration.");
            }
            $video_duration = floatval($video_duration);

            // Set the start time for GIF creation
            $start_time = ($video_duration > 3) ? 3 : 1;

            // Generate GIF starting at $start_time
            exec('taskset -c 0,1,2 ffmpeg -ss ' . $start_time . ' -t 0.5 -i ' . escapeshellarg($base_path . $video_url) . ' -vf "fps=3,scale=120:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 ' . escapeshellarg($converted_path . $gif_url) . ' 2>&1', $output, $return_var);

            if ($return_var != 0) {
                throw new Exception("GIF creation failed: " . implode("\n", $output));
            }

            Log::info("GIF created successfully", ['gif_url' => $gif_url]);
            return $gif_url;
        } catch (Exception $e) {
            // Log the error and display the exception message
            Log::error("Error creating GIF", [
                'message' => $e->getMessage(),
                'video_url' => $video_url,
                'converted_path' => $converted_path
            ]);
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }

    public static function generateGif($video_url) {
        $gif_url= str_replace('.mp4', '.gif', $video_url);
        $base_path = storage_path('app/');
        exec('taskset -c 0,1,2,3,4,5 ffmpeg -t 0.5 -i '.$base_path.$video_url.' -vf "fps=3,scale=120:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0  '.$base_path.''.$gif_url.' 2>&1', $output, $return_var);

        return str_replace('.mp4', '.gif', $video_url);
    }


    public static function generateGifGPU($video_url)
    {
        try {
            $gif_url = str_replace('.mp4', '.gif', $video_url);
            $base_path = storage_path('app/');

            // Generate GIF with CUDA support
            $command = 'ffmpeg -hwaccel cuda -t 0.5 -i ' . escapeshellarg($base_path . $video_url) . ' -vf "fps=3,scale=120:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 -c:v h264_nvenc ' . escapeshellarg($base_path . $gif_url) . ' 2>&1';

            exec($command, $output, $return_var);

            if ($return_var != 0) {
                throw new Exception("GIF generation failed: " . implode("\n", $output));
            }

            Log::info("GIF generated successfully", ['gif_url' => $gif_url]);
            return $gif_url;
        } catch (Exception $e) {
            // Log the error and display the exception message
            Log::error("Error generating GIF", [
                'message' => $e->getMessage(),
                'video_url' => $video_url
            ]);
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }




    ##helper function to scaledown videos
    // public static function scaleDownVideo($video_url, $watermark_path, $converted_path) {

    //     $vid = explode("/", $video_url);
    //     $vid_title = end($vid);
    //     $base_path = storage_path('app/');

    //     // Use GPU acceleration for scaling down the video
    //     $command = 'ffmpeg -hwaccel cuda -i ' . $base_path . $video_url . ' -vf "scale=360:640,fps=30" -acodec copy -y ' . $converted_path . $video_url . ' 2>&1';

    //     exec($command, $output, $return_var);

    //     if ($return_var != 0) {
    //         echo "Scaling failed:\n";
    //         print_r($output);
    //         return false;
    //     }

    //     return $video_url;
    // }

    public static function scaleDownVideo($video_url, $watermark_path, $converted_path)
    {
        try {
            $vid = explode("/", $video_url);
            $vid_title = end($vid);
            $base_path = storage_path('app/');

            // Use GPU acceleration for scaling down the video
            $command = 'ffmpeg -hwaccel cuda -i ' . escapeshellarg($base_path . $video_url) . ' -vf "scale=360:640,fps=30" -acodec copy -y ' . escapeshellarg($converted_path . $video_url) . ' 2>&1';

            exec($command, $output, $return_var);

            if ($return_var != 0) {
                throw new Exception("Scaling failed: " . implode("\n", $output));
            }

            return $video_url;
        } catch (Exception $e) {
            // Log the error and display the exception message
            Log::error("Error scaling down video", [
                'message' => $e->getMessage(),
                'video_url' => $video_url
            ]);
            echo "Error: " . $e->getMessage() . "\n";
            return false;
        }
    }

    public static function prepareVideo($filePath)
    {
        $resolution = '540x960'; // Example resolution
        $fps = '30'; // Example frame rate

        $fileInfo = pathinfo($filePath);
        $extension = strtolower($fileInfo['extension']);
        $outputVideoPath = $fileInfo['dirname'] . '/' . time() . '_' . $fileInfo['filename'] . '_scaled.mp4';

        // Convert HEIC to JPEG if needed
        if ($extension === 'heic') {
            $jpegFilePath = $fileInfo['dirname'] . '/' . $fileInfo['filename'] . '.jpg';
            exec("convert \"$filePath\" \"$jpegFilePath\"", $output, $return_var);
            if ($return_var !== 0) {
                echo "ImageMagick conversion of HEIC to JPEG failed.\n";
                return false;
            }
            $filePath = $jpegFilePath;
            $extension = 'jpg';
        }

        if (in_array($extension, ['png', 'jpeg', 'jpg'])) {
            // Convert static images (PNG, JPEG, etc.) to video
            exec("taskset -c 0,1 ffmpeg -y -loop 1 -i \"$filePath\" -c:v libx264 -t 1 -pix_fmt yuv420p -vf 'scale=$resolution,format=yuv420p' -r $fps \"$outputVideoPath\"");
        } elseif ($extension === 'gif') {
            // Convert GIF to MP4
            exec("taskset -c 0,1 ffmpeg -y -i \"$filePath\" -t 1 -movflags faststart -pix_fmt yuv420p -vf 'scale=$resolution,fps=$fps' \"$outputVideoPath\"");
        } elseif ($extension === 'webp') {
            exec("taskset -c 0,1 ffmpeg -y -i \"$filePath\" -t 1 -c:v libx264 -pix_fmt yuv420p -vf 'scale=$resolution' \"$outputVideoPath\"");

        } else {
            // Scale existing video formats (e.g., MP4)
            exec("taskset -c 0,1 ffmpeg -y -i \"$filePath\" -vf 'scale=$resolution,fps=$fps' -c:v libx264 -pix_fmt yuv420p \"$outputVideoPath\"");
        }

        // Verify the output file was created successfully
        return file_exists($outputVideoPath) ? $outputVideoPath : false;
    }



}
