<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSettings extends Model
{
    protected $table = 'system_settings';
    protected $fillable = ['id','name', 'setting_type','value', 'sales_contact_amount', 'is_active'];

}
