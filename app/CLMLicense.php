<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class CLMLicense extends Model
{
    use SoftDeletes;
    use PaginationWithHavings;
    protected $table = 'clm_licenses';
    protected $fillable = ['id','user_id','caret_id','company_name', 'caret_title','caret_logo','caret_certificate','deleted_at','is_active','status_id','subscription_plan','subscription_amount','subscription_status','cancel_at_period_end','current_period_end','default_ad', 'default_intro','default_finish','default_sound','subscription_stripe_id'];


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function status()
    {
        return $this->hasOne('App\AdStatus', 'id', 'status_id');
    }

    public function payments()
    {
        return $this->hasMany(\App\CLMPayments::class, 'license_id', 'id');
    }

    public function plan()
    {
        return $this->hasOne('App\CLMPlans', 'id', 'subscription_plan');
    }
    public function pricing(){
        return $this->hasOne('App\CaretPricing', 'id', 'subscription_plan');
    }
    public function intro()
    {
        return $this->hasOne('App\CLMSplash', 'id', 'default_intro');
    }
    public function finish()
    {
        return $this->hasOne('App\CLMSplash', 'id', 'default_finish');
    }
    public function defaultAd()
    {
        return $this->hasOne('App\Ads', 'id', 'default_ad');
    }
    public function defaultSound()
    {
        return $this->hasOne('App\Sounds', 'id', 'default_sound');
    }

    public function clmRequest()
    {
        return $this->hasOne('App\CLMRequest', 'caret_id', 'caret_id');
    }



}
