<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoundReports extends Model
{
    protected $table = 'sound_reports';
    protected $fillable = ['id','sound_id', 'report_by','remarks','deleted_at'];
}
