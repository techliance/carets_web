<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdCards extends Model
{
    use SoftDeletes;
    protected $table = 'ad_cards';
    protected $fillable = ['id','stripe_id', 'user_id','card_number','card_expiry', 'set_default', 'deleted_at'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


}
