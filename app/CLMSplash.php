<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Illuminate\Database\Eloquent\SoftDeletes;

class CLMSplash extends Model
{
    use SoftDeletes;
    use PaginationWithHavings;
    protected $table = 'clm_splash';
    protected $fillable = ['id','user_id','license_id','splash_title','video_url','image_url','deleted_at','is_active','start_default','end_default'];


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
    public function license()
    {
        return $this->hasMany('App\CLMLicense', 'id', 'license_id');
    }

}
