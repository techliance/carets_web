<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CLMOrderItems extends Model
{
    protected $table = 'clm_order_items';
    protected $fillable = [
        'order_id',
        'caret_title',
        'interval_count',
        'subscription_stripe_id',
        'subscription_pricing_id',
        'subscription_amount',
        'subscription_status',
        'cancel_at_period_end',
        'current_period_end',

    ];

    public function pricing() {
        return $this->hasOne('App\CaretPricing', 'id', 'subscription_pricing_id');
    }
    public function order()
    {
        return $this->belongsTo('App\CLMOrders', 'order_id', 'id'); // Corrected
    }
}
