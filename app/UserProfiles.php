<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfiles extends Model
{
    use SoftDeletes;
    protected $fillable = ['id','user_id', 'first_name','last_name','middle_name', 'user_title', 'user_bio', 'user_photo', 'user_video','dob', 'industry_id', 'business_name', 'phone_number','gender', 'deleted_at'];
}
