<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavSounds extends Model
{
    protected $table = 'user_fav_sounds';
    protected $fillable = ['id','user_id', 'sound_id','deleted_at'];
}
