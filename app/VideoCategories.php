<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategories extends Model
{
    protected $table = 'video_categories';
    protected $fillable = ['id','title', 'is_active','deleted_at'];
}
