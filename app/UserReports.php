<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class UserReports extends Model
{
    use PaginationWithHavings;
    protected $table = 'user_reports';
    protected $fillable = ['id','report_to', 'report_by','comments','deleted_at'];

    public function reportby()
    {
        return $this->hasOne('App\User', 'id', 'report_by');
    }

    public function reportto()
    {
        return $this->hasOne('App\User', 'id', 'report_to');
    }
}
