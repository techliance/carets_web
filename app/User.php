<?php

namespace App;

use App\Notifications\CustomResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class User extends Authenticatable
{
    use PaginationWithHavings;
    use HasApiTokens, Notifiable;
    use HasRoles;
    use SoftDeletes;

    protected $hidden = [
        'password', 'remember_token','activation_token'
    ];

    // public function findForPassport($username) {
    //     return $this->where('id', $username)->first();
    // }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','stripe_id','name', 'email', 'password', 'phone','username', 'is_active', 'deleted_at','email_verified_at','phone_verified_at','follow_count','follow_by_count','remember_token','fb_token','google_token','ios_token','device_id','device_type','device_model','device_token','device_os','is_blocked','video_count','caret_count','video_like_count','follow_to_count','firestore_reference_id','last_visit','is_guest','activation_token','active'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function profile()
    {
        return $this->hasOne(\App\UserProfiles::class, 'user_id', 'id');
    }

    // public function foll()
    // {
    //     return $this->hasMany(\App\UserFollows::class, 'follow_to', 'id');
    // }

    public function followto()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'follow_by', 'follow_to');
    }

    public function followby()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'follow_to', 'follow_by');
    }


    public function blockto()
    {
        return $this->belongsToMany('App\User', 'user_blocks', 'block_by', 'block_to');
    }

    public function blockby()
    {
        return $this->belongsToMany('App\User', 'user_blocks', 'block_to', 'block_by');
    }


    public function privacy()
    {
        return $this->hasOne(\App\UserPrivacy::class, 'user_id', 'id');
    }
    public function comments()
    {
        return $this->hasOne(\App\VideoComments::class, 'user_id', 'id');
    }
    public function commentLike()
    {
        return $this->hasOne(\App\VideoCommentLikes::class, 'user_id', 'id');
    }

    public function userInterests()
    {
        return $this->hasMany(\App\Interests::class, 'user_id', 'id');
    }

    public function userVideos()
    {
        return $this->hasMany(\App\UserVideos::class, 'user_id', 'id');
    }

    public function favEffects()
    {
        return $this->hasMany(\App\UserFavEffects::class, 'user_id', 'id');
    }

    public function favHTags()
    {
        return $this->hasMany(\App\UserFavHashtags::class, 'user_id', 'id');
    }

    public function favSounds()
    {
        return $this->hasMany(\App\UserFavSounds::class, 'user_id', 'id');
    }

    public function favVideos()
    {
        return $this->hasMany(\App\UserFavVideos::class, 'user_id', 'id');
    }

    #my videos
    public function myvideos()
    {
        return $this->belongsToMany('App\Videos', 'users_videos', 'user_id', 'video_id');
    }

    ##Advertisers Section
    public function campaigns()
    {
        return $this->hasMany(\App\Campaigns::class, 'user_id', 'id');
    }

    public function ads()
    {
        return $this->hasMany(\App\Ads::class, 'user_id', 'id');
    }
    public function cards()
    {
        return $this->hasMany(\App\AdCards::class, 'user_id', 'id');
    }
    public function payments()
    {
        return $this->hasMany(\App\AdPayments::class, 'user_id', 'id');
    }

    public function role()
    {

    }



    public function scopeRole(Builder $query, $roles, $guard = null, $model_type = null): Builder
    {
        if ($roles instanceof Collection) {
            $roles = $roles->all();
        }

        if (! is_array($roles)) {
            $roles = [$roles];
        }

        $roles = array_map(function ($role) use ($guard) {
            if ($role instanceof Role) {
                return $role;
            }

            $method = is_numeric($role) ? 'findById' : 'findByName';
            $guard = $guard ?: $this->getDefaultGuardName();

            return $this->getRoleClass()->{$method}($role, $guard);
        }, $roles);
        return $query->whereHas('roles', function ($query) use ($roles, $model_type) {
            $query->where(function ($query) use ($roles) {
                foreach ($roles as $role) {
                    $query->orWhere(config('permission.table_names.roles').'.id', $role->id);
                }
            })
                ->when($model_type, function ($q) use ($model_type) {
                    return $q->where('roles.model_type','=', $model_type);
                });

        });
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($user) { // before delete() method call this
            //$user->contact()->delete();
            // do the rest of the cleanup...
        });
    }

    public function getFilteredAndOrderByAttrAllAdmins($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
        return User::with([
                'roles'
            ])
            ->whereHas('roles', function($query){
                $query->whereNotIn('name', ['user']);
            })
            ->select('id', 'name', 'email', 'is_active', 'is_blocked')
            ->where(function($q) use ($filter){
                $q->where('id','like', '%' . $filter . '%')
                    ->orWhere('name','like', '%' . $filter . '%')
                    ->orWhere('email','like', '%' . $filter . '%');
            })
            ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                $query->orderBy($sortName, $orderType);
            })
            ->when($sortName===null,function($q){
                $q->orderBy('created_at','desc');
            })
            ->paginate($pagination);
        }

        public function getFilteredAndOrderByAttrAllUsers($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
            return User::with([
                    'roles',
                    'profile'=>function($q){
                        $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                    }
                ])
                ->orderBy('id','DESC')
                ->whereHas('roles', function($query){
                    $query->whereIn('name', ['user']);
                })
                ->select('id', 'name', 'email','username', 'is_active', 'is_blocked')
                ->where(function($q) use ($filter){
                    $q->where('id','like', '%' . $filter . '%')
                        ->orWhere('name','like', '%' . $filter . '%')
                        ->orWhere('email','like', '%' . $filter . '%');
                })
                ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                    $query->orderBy($sortName, $orderType);
                })
                ->when($sortName===null,function($q){
                    $q->orderBy('created_at','desc');
                })
                ->paginate($pagination);
        }

        public function getFilteredAndOrderByAttrAllAdvertiser($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
            return User::with([
                    'roles',
                    'profile'=>function($q){
                        $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                    }
                ])
                ->whereHas('roles', function($query){
                    $query->whereIn('name', ['advertiser']);
                })
                ->select('id', 'name', 'email','username', 'is_active', 'is_blocked')
                ->where(function($q) use ($filter){
                    $q->where('id','like', '%' . $filter . '%')
                        ->orWhere('name','like', '%' . $filter . '%')
                        ->orWhere('email','like', '%' . $filter . '%');
                })
                ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                    $query->orderBy($sortName, $orderType);
                })
                ->when($sortName===null,function($q){
                    $q->orderBy('created_at','desc');
                })
                ->paginate($pagination);
        }

        public function getFilteredAndOrderByAttrAllClmUser($pagination = 10, $filter = null , $sortName = null, $orderType = null) {
            return User::with([
                    'roles',
                    'profile'=>function($q){
                        $q->select(['*', DB::raw("CONCAT('".config('app.awsurl')."', user_photo) AS user_photo")]);
                    }
                ])
                ->whereHas('roles', function($query){
                    $query->whereIn('name', ['clm']);
                })
                ->select('id', 'name', 'email','username', 'is_active', 'is_blocked')
                ->where(function($q) use ($filter){
                    $q->where('id','like', '%' . $filter . '%')
                        ->orWhere('name','like', '%' . $filter . '%')
                        ->orWhere('email','like', '%' . $filter . '%');
                })
                ->when($sortName || $orderType, function($query) use ($sortName, $orderType){
                    $query->orderBy($sortName, $orderType);
                })
                ->when($sortName===null,function($q){
                    $q->orderBy('created_at','desc');
                })
                ->paginate($pagination);
        }


        public function userDetail($id){
            return $this->find($id);
        }

        public function deleteFromModelHasRoles($id){
            return DB::table('model_has_roles')->where('model_id',$id)->delete();
        }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }


}
