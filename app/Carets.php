<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carets extends Model
{
    use SoftDeletes;
    protected $table = 'carets';
    protected $fillable = ['id','title', 'deleted_at','updated_at'];

    public function videos()
    {
        return $this->belongsToMany('App\Videos', 'carets_videos', 'caret_id', 'video_id')->withTimestamps();
    }

    public function license()
    {
        return $this->hasOne('App\CLMLicense', 'caret_id', 'id');
    }


    // public function likedBy()
    // {
    //     return $this->hasMany('App\VideoLikes', 'video_id', 'id');
    // }

}
