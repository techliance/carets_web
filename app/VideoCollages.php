<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCollages extends Model
{
    protected $table = 'video_collages';
    protected $fillable = ['id','video_id', 'video_added','start_order','duration','deleted_at'];

    public function videos()
    {
        return $this->hasOne('App\Videos', 'id', 'video_added');
    }

}
