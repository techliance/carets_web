#!/bin/bash
chown -R ubuntu:ubuntu /var/www/public_html
ln -sf /var/www/shared/storage /var/www/public_html/
ln -sf /var/www/shared/vendor /var/www/public_html/
ln -sf /var/www/shared/cache /var/www/public_html/bootstrap/
ln -sf /var/www/shared/carets_videos /var/www/public_html/public/

chmod -R 777 /var/www/public_html/bootstrap/cache
chmod -R 777 /var/www/public_html/storage
cp /var/www/shared/.env /var/www/public_html/
chown -R ubuntu:ubuntu /var/www/public_html
cd /var/www/public_html/ && composer update --no-progress
cd /var/www/public_html/ && composer dumpautoload
cd /var/www/public_html/ && composer install --no-progress
#cd /var/www/public_html/ && php artisan migrate //test
cd /var/www/public_html/ && php artisan config:cache
cd /var/www/public_html/ && php artisan config:clear
