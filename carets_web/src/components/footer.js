import React from "react";
import { Link } from "components/Router";
import facebook from "../assets/imgs/facebook.svg";
import twitter from "../assets/imgs/twitter.svg";
import instagram from "../assets/imgs/instagram.svg";
import youtube from "../assets/imgs/youtube.svg";
import footer_logo from "../assets/imgs/footer_logo.svg";

const footer = () => {
    return (
        <footer>
            <div
                className="bg-gray-200 w-full mt-16"
                style={{ height: "1px" }}
            ></div>
            <div className="customContainer py-7">
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 md:text-left text-center">
                    <div>
                        <div className="text-xl font-semibold themeDarkBlue mt-5">
                            ^Carets
                        </div>
                        <ul className="list-none p-0 mt-5">
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/introduction"
                                >
                                    Introduction
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/community-guidelines"
                                >
                                    Community Guidelines
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/legal"
                                >
                                    Legal
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/help"
                                >
                                    Help
                                </Link>
                            </li>
                        </ul>
                    </div>

                    {/* <div>
                    <div className="text-xl font-semibold themeDarkBlue mt-5">Resources</div>
                    <ul className="list-none p-0 mt-5">
                        <li className="mt-3 text-lg 2xl:text-base"><Link className="hover:text-blue-800 hover:underline" to="/">Creators</Link></li>
                        <li className="mt-3 text-lg 2xl:text-base"><Link className="hover:text-blue-800 hover:underline" to="/">Partners</Link></li>
                    </ul>
                </div> */}

                    <div>
                        <div className="text-xl font-semibold themeDarkBlue mt-5">
                            Company
                        </div>
                        <ul className="list-none p-0 mt-5">
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/about"
                                >
                                    About Carets
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/terms"
                                >
                                    Advertising T&C
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/clmTerms"
                                >
                                    CLM T&C
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <a
                                    className="hover:text-blue-800 hover:underline"
                                    href="https://admin.carets.tv/"
                                    target="_blank"
                                >
                                    Advertise with Us
                                </a>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <Link
                                    className="hover:text-blue-800 hover:underline"
                                    to="/clmTutorials"
                                >
                                    CLM Tutorials
                                </Link>
                            </li>
                            <li className="mt-3 text-lg 2xl:text-base">
                                <a
                                    className="hover:text-blue-800 hover:underline"
                                    href="/deleteInstructions"
                                    target="_blank"
                                >
                                    Delete Account
                                </a>
                            </li>
                            {/* <li className="mt-3 text-lg 2xl:text-base"><Link className="hover:text-blue-800 hover:underline" to="/">Related Press</Link></li> */}
                        </ul>
                    </div>

                    <div>&nbsp;</div>

                    <div className="justify-self-center">
                        <div className="text-xl font-semibold themeDarkBlue mt-5">
                            Follow us
                        </div>
                        <ul className="socialIcons list-none p-0 mt-5 inline-grid grid-cols-4 gap-4 grid-flow-col-dense">
                            <li>
                                <a
                                    target="_blank"
                                    rel="noreferrer"
                                    href="https://www.facebook.com/profile.php?id=100069312018877"
                                >
                                    <img
                                        className="h-6 absolute hidden"
                                        src={facebook}
                                        alt="Facebook"
                                    />
                                    <img
                                        className="h-6"
                                        src={facebook}
                                        alt="Facebook"
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    target="_blank"
                                    rel="noreferrer"
                                    href="https://twitter.com/CaretsApp"
                                >
                                    <img
                                        className="h-6 absolute hidden"
                                        src={twitter}
                                        alt="Twitter"
                                    />
                                    <img
                                        className="h-6"
                                        src={twitter}
                                        alt="Twitter"
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    target="_blank"
                                    rel="noreferrer"
                                    href="https://www.instagram.com/caretsapp/"
                                >
                                    <img
                                        className="h-6 absolute hidden"
                                        src={instagram}
                                        alt="Instagram"
                                    />
                                    <img
                                        className="h-6"
                                        src={instagram}
                                        alt="Instagram"
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    target="_blank"
                                    rel="noreferrer"
                                    href="https://www.youtube.com/channel/UCBb6TaWtn3BScytshsQksmw"
                                >
                                    <img
                                        className="h-6 absolute hidden"
                                        src={youtube}
                                        alt="Youtube"
                                    />
                                    <img
                                        className="h-6"
                                        src={youtube}
                                        alt="Youtube"
                                    />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div
                className="bg-gray-200 w-full mt-16"
                style={{ height: "1px" }}
            ></div>

            <div className="customContainer py-5">
                <div className="flex">
                    <div className="text-sm">
                        Copyright &copy; {new Date().getFullYear()}, Carets
                        Corporation, All Rights Reserved.
                    </div>
                    <div className="ml-auto">
                        <Link to="/">
                            <img
                                src={footer_logo}
                                alt="Carets Logo"
                                className="h-10"
                            />
                        </Link>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default footer;
