import React, { useRef, useState } from "react";
import triangle from "../assets/imgs/triangle.svg";

export default (props) => {
    const videoEl = useRef(null);
    const [isPlaying, setPlaying] = useState(false);

    const playVideo = () => {
        videoEl.current.play();
        videoEl.current.addEventListener("ended", myHandler, false);
        setPlaying(true);
    };

    const myHandler = () => {
        setPlaying(false);
    };

    let data = props.data;

    return (
        <div className="text-center">
            <div className="text-3xl font-semibold">{data.username}</div>
            <div>
                {data.tags &&
                    data.tags.map((elem, index) => (
                        <span className="inline-flex mx-3 mt-1" key={index}>
                            {elem}
                        </span>
                    ))}
            </div>
            <div className="inline-flex items-center mt-2">
                <span className="text-lg font-bold">♬</span>
                <span className="ml-1">{data.music}</span>
            </div>
            <div className="mt-5">
                <div className="relative inline-block w-full max-w-5xl mx-auto">
                    <video
                        playsInline={true}
                        className="w-full"
                        ref={videoEl}
                        // src={data.videoUrl}
                        src="https://caretsffmpeg.s3.amazonaws.com/promosvideos/caretsintrodesktop.mp4"
                    />
                    {!isPlaying && (
                        <button className="playBtn" onClick={playVideo}>
                            <img src={triangle} alt="Play" />
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
};
