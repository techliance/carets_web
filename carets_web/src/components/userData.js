import React, { useRef, useState } from "react";
import triangle from "../assets/imgs/triangle.svg";

export default (props) => {
    const videoEl = useRef(null);
    const [isPlaying, setPlaying] = useState(false);

    const playVideo = () => {
        videoEl.current.play();
        videoEl.current.addEventListener("ended", myHandler, false);
        setPlaying(true);
    };

    const myHandler = () => {
        setPlaying(false);
    };

    let data = props.data.data;

    return (
        <div className="text-center" clg={console.log(data)}>
            {data ? (
                <React.Fragment>
                    {data.users &&
                        data.users.map((usr, idx) => (
                            <div className="text-3xl font-semibold" key={idx}>
                                @{usr.username}
                            </div>
                        ))}
                    <div>
                        {data.hashtags &&
                            data.hashtags.map((elem, index) => (
                                <span
                                    className="inline-flex mx-3 mt-1"
                                    key={index}
                                >
                                    {elem ? elem : "Loading..."}
                                </span>
                            ))}
                    </div>
                    {data.music && (
                        <div className="inline-flex items-center mt-2">
                            <span className="text-lg font-bold">♬</span>
                            <span className="ml-1">
                                {data.music ? (
                                    data.music
                                ) : (
                                    <div>Loading...</div>
                                )}
                            </span>
                        </div>
                    )}
                    <div className="mt-5">
                        <div className="relative inline-block">
                            {data.video_url && false ? (
                                <React.Fragment>
                                    <video
                                        playsInline={true}
                                        className="max-w-screen-lg w-full inline-flex"
                                        ref={videoEl}
                                        src={data.video_url}
                                        poster={
                                            data.image_url
                                                ? data.image_url
                                                : null
                                        }
                                    />
                                    {!isPlaying && (
                                        <button
                                            className="playBtn"
                                            onClick={playVideo}
                                        >
                                            <img src={triangle} alt="Play" />
                                        </button>
                                    )}
                                </React.Fragment>
                            ) : (
                                <React.Fragment>
                                    <video
                                        playsInline={true}
                                        className="max-w-screen-lg w-full inline-flex"
                                        ref={videoEl}
                                        src={props.dummy}
                                        poster={
                                            data.image_url
                                                ? data.image_url
                                                : null
                                        }
                                    />
                                    {!isPlaying && (
                                        <button
                                            className="playBtn"
                                            onClick={playVideo}
                                        >
                                            <img src={triangle} alt="Play" />
                                        </button>
                                    )}
                                </React.Fragment>
                            )}
                        </div>
                    </div>
                </React.Fragment>
            ) : (
                <div>No video found</div>
            )}
        </div>
    );
};
