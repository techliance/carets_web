import React from 'react'
import loaderGif from '../../assets/imgs/ajax-loader.gif';

const loader = () => {
    return (
        <div className="flex justify-center items-center" style={{height: '100vh', width: '100vw'}}>
            <img src={loaderGif} alt="Loading..." />
        </div>
    )
}

export default loader
