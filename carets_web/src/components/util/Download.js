import React from 'react';
import { Link } from 'components/Router';

const Download = (props) => {
    let data = props.data;
    return (
        props.link ? 
        <Link className={data.extraClasses ? data.extraClasses + " inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100" : "inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100"} style={{backgroundColor: data.bgColor ? data.bgColor : '#444'}}>
            <span><img style={{height: '2rem'}} src={data.icon} alt={data.alt} /></span>
            <span className="ml-2">
                <span className="block text-xs">{data.smallText}</span>
                <span className="block text-lg font-semibold leading-none">{data.largeText}</span>
            </span>
        </Link>
        :
        <a href={props.extLink ? props.extLink : "#!"} target={props.extLink && '_blank'} className={data.extraClasses ? data.extraClasses + " inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100" : "inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100"} style={{backgroundColor: data.bgColor ? data.bgColor : '#444'}}>
            <span><img style={{height: '2rem'}} src={data.icon} alt={data.alt} /></span>
            <span className="ml-2">
                <span className="block text-xs">{data.smallText}</span>
                <span className="block text-lg font-semibold leading-none">{data.largeText}</span>
            </span>
        </a>
    )
}

export default Download
