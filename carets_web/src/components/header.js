import React, { useEffect } from "react";
import Download from "../components/util/Download";
import { Link } from "components/Router";
import Modal from "react-modal";

import logo from "../assets/imgs/caretLogo.svg";
import apple from "../assets/imgs/apple.svg";
import playstore from "../assets/imgs/playstore.svg";

const appleBtn = {
    bgColor: "#050038",
    icon: apple,
    smallText: "Download from",
    largeText: "App Store",
    extraClasses: "mx-2.5 mt-5",
};

const playBtn = {
    bgColor: "#050038",
    icon: playstore,
    smallText: "Download from",
    largeText: "Google Play",
    extraClasses: "mx-2.5 mt-5",
};

const header = (props) => {
    const customStyles = {
        content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
        },
    };

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [modalData, setModalData] = React.useState(false);
    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
        if (window) {
            window.history.pushState({}, document.title, "/");
        }
    }

    useEffect(() => {
        if (window) {
            const urlParams = new URLSearchParams(window.location.search);
            const reset = urlParams.get("reset");
            const success = urlParams.get("success");
            if (reset == "true") {
                setModalData({
                    icon: true,
                    msg: "Password Reset Successfully",
                });
                openModal();
            } else if (success == "true") {
                setModalData({
                    icon: true,
                    msg: "Email Verified Successfully",
                });
                openModal();
            } else if (success == "false") {
                setModalData({
                    icon: false,
                    msg: "Error! Invalid Data",
                });
                openModal();
            }
        }
    }, []);

    return (
        <React.Fragment>
            <div className="w-full mx-auto pb-4 border-b border-gray-300 mb-8">
                <div className="flex flex-wrap md:flex-nowrap gap-4 items-center justify-center md:justify-between w-full max-w-5xl mx-auto">
                    <Link to="/" className="pt-4">
                        <img src={logo} className="w-56" alt="Carets" />
                    </Link>
                    <div
                        className={
                            props.isCenter
                                ? "text-center downloadBtns"
                                : "downloadBtns flex items-center justify-center sm:text-left lg:ml-10 w-full lg:w-auto whitespace-nowrap"
                        }
                    >
                        <Download
                            extLink="https://apps.apple.com/us/app/carets/id1571154682"
                            data={appleBtn}
                        />
                        <Download
                            extLink="https://play.google.com/store/apps/details?id=com.carets.tv"
                            data={playBtn}
                        />
                    </div>
                </div>
            </div>
            {/* <div
                className={
                    props.isCenter
                        ? "mt-5 flex justify-center"
                        : "mt-5 flex justify-center"
                }
            >
                <Link to="/">
                    <img src={logo} className="w-32" alt="Carets" />
                </Link>
            </div> */}
            <div className="flex flex-col gap-2 items-center justify-center w-full max-w-5xl mx-auto">
                <h1
                    className={
                        props.isCenter
                            ? "text-3xl mt-8 font-semibold text-center"
                            : "text-3xl mt-8 font-semibold text-center sm:text-left"
                    }
                >
                    Create, share, and combine video content.
                </h1>
                <h2
                    className={
                        props.isCenter
                            ? "text-2xl text-center md:text-left"
                            : "text-2xl text-center md:text-left"
                    }
                >
                    What we see with ^Carets is extraordinary.
                </h2>

                {/* <div
                    className={
                        props.isCenter
                            ? "text-center downloadBtns"
                            : "downloadBtns flex items-center justify-center sm:text-left lg:ml-10 w-full lg:w-auto whitespace-nowrap"
                    }
                >
                    <Download
                        extLink="https://apps.apple.com/us/app/carets/id1571154682"
                        data={appleBtn}
                    />
                    <Download
                        extLink="https://play.google.com/store/apps/details?id=com.carets.tv"
                        data={playBtn}
                    />
                </div> */}
            </div>

            <Modal
                isOpen={modalIsOpen}
                style={customStyles}
                onRequestClose={closeModal}
                ariaHideApp={false}
                contentLabel="Password Reset Successfull"
            >
                <div className="f-modal-alert">
                    {modalData.icon ? (
                        <div class="f-modal-icon f-modal-success animate">
                            <span class="f-modal-line f-modal-tip animateSuccessTip"></span>
                            <span class="f-modal-line f-modal-long animateSuccessLong"></span>
                            <div class="f-modal-placeholder"></div>
                            <div class="f-modal-fix"></div>
                        </div>
                    ) : (
                        <div class="f-modal-icon f-modal-error animate">
                            <span class="f-modal-x-mark">
                                <span class="f-modal-line f-modal-left animateXLeft"></span>
                                <span class="f-modal-line f-modal-right animateXRight"></span>
                            </span>
                            <div class="f-modal-placeholder"></div>
                            <div class="f-modal-fix"></div>
                        </div>
                    )}

                    {/* <div class="f-modal-icon f-modal-warning scaleWarning">
                        <span class="f-modal-body pulseWarningIns"></span>
                        <span class="f-modal-dot pulseWarningIns"></span>
                    </div> */}
                </div>

                <div
                    className={
                        modalData.icon ? "reset_msg success" : "reset_msg error"
                    }
                >
                    {modalData.msg}
                </div>

                <div style={{ textAlign: "center", marginTop: "2em" }}>
                    <button className="modal_close" onClick={closeModal}>
                        OK
                    </button>
                </div>
            </Modal>
        </React.Fragment>
    );
};

export default header;
