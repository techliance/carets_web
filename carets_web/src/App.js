import React from "react";
import { Root, Routes, addPrefetchExcludes } from "react-static";
import Footer from "./components/footer";
//
import { Router } from "components/Router";
import Video from "containers/Video";
import Loader from "./components/util/loader";

import "./tw.css";
import "./assets/fonts/fonts.css";
import "./app.css";
import "./styleCustom.css";

// Any routes that start with 'video' will be treated as non-static routes
addPrefetchExcludes(["video"]);

function App() {
    return (
        <Root>
            <div className="content">
                <React.Suspense fallback={<Loader />}>
                    <header className="topBar"></header>
                    <Router>
                        <Video path="video" />
                        <Routes path="*" />
                    </Router>
                    <Footer />
                </React.Suspense>
            </div>
        </Root>
    );
}

export default App;
