<div style="background-color: #fff;">
  <div class="customContainer">
    <div>
      <div class="mt-5 flex justify-center"><a href="/"><img src="https://carets.tv/static/logo.b284305e.svg"
            class="w-32" alt="Carets"></a></div>
      <h1 class="text-3xl mt-8 font-semibold text-center">Create, share, and combine video content.</h1>
      <h2 class="text-2xl text-center">What we see with ^Carets is extraordinary 3513510.</h2>
      <div class="text-center downloadBtns">
        <a href="https://apps.apple.com/us/app/carets/id1571154682" target="_blank"
          class="mx-2.5 mt-5 inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100"
          style="background-color: rgb(5, 0, 56);">
          <span>
            <img
              src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sbnM6c3ZnanM9Imh0dHA6Ly9zdmdqcy5jb20vc3ZnanMiIHZlcnNpb249IjEuMSIgd2lkdGg9IjUxMiIgaGVpZ2h0PSI1MTIiIHg9IjAiIHk9IjAiIHZpZXdCb3g9IjAgMCA1MTIuMDAzIDUxMi4wMDMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KICA8Zz4NCjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0zNTEuOTgsMGMtMjcuMjk2LDEuODg4LTU5LjIsMTkuMzYtNzcuNzkyLDQyLjExMmMtMTYuOTYsMjAuNjQtMzAuOTEyLDUxLjI5Ni0yNS40NzIsODEuMDg4ICAgIGMyOS44MjQsMC45MjgsNjAuNjQtMTYuOTYsNzguNDk2LTQwLjA5NkMzNDMuOTE2LDYxLjU2OCwzNTYuNTU2LDMxLjEwNCwzNTEuOTgsMHoiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiLz4NCgk8L2c+DQo8L2c+DQo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPg0KCTxnPg0KCQk8cGF0aCBkPSJNNDU5Ljg1MiwxNzEuNzc2Yy0yNi4yMDgtMzIuODY0LTYzLjA0LTUxLjkzNi05Ny44MjQtNTEuOTM2Yy00NS45MiwwLTY1LjM0NCwyMS45ODQtOTcuMjQ4LDIxLjk4NCAgICBjLTMyLjg5NiwwLTU3Ljg4OC0yMS45Mi05Ny42LTIxLjkyYy0zOS4wMDgsMC04MC41NDQsMjMuODQtMTA2Ljg4LDY0LjYwOGMtMzcuMDI0LDU3LjQwOC0zMC42ODgsMTY1LjM0NCwyOS4zMTIsMjU3LjI4ICAgIGMyMS40NzIsMzIuODk2LDUwLjE0NCw2OS44ODgsODcuNjQ4LDcwLjIwOGMzMy4zNzYsMC4zMiw0Mi43ODQtMjEuNDA4LDg4LTIxLjYzMmM0NS4yMTYtMC4yNTYsNTMuNzkyLDIxLjkyLDg3LjEwNCwyMS41NjggICAgYzM3LjUzNi0wLjI4OCw2Ny43NzYtNDEuMjgsODkuMjQ4LTc0LjE3NmMxNS4zOTItMjMuNTg0LDIxLjEyLTM1LjQ1NiwzMy4wNTYtNjIuMDggICAgQzM4Ny44NTIsMzQyLjYyNCwzNzMuOTMyLDIxOS4xNjgsNDU5Ljg1MiwxNzEuNzc2eiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiIvPg0KCTwvZz4NCjwvZz4NCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCjwvZz4NCjwvc3ZnPg0K"
              style="height: 2rem;" />
          </span>
          <span class="ml-2">
            <span class="block text-xs">Download from</span>
            <span class="block text-lg font-semibold leading-none">App Store</span>
          </span>
        </a>
        <a href="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sbnM6c3ZnanM9Imh0dHA6Ly9zdmdqcy5jb20vc3ZnanMiIHZlcnNpb249IjEuMSIgd2lkdGg9IjUxMiIgaGVpZ2h0PSI1MTIiIHg9IjAiIHk9IjAiIHZpZXdCb3g9IjAgMCA1MTIuMDAzIDUxMi4wMDMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KICA8Zz4NCjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0zNTEuOTgsMGMtMjcuMjk2LDEuODg4LTU5LjIsMTkuMzYtNzcuNzkyLDQyLjExMmMtMTYuOTYsMjAuNjQtMzAuOTEyLDUxLjI5Ni0yNS40NzIsODEuMDg4ICAgIGMyOS44MjQsMC45MjgsNjAuNjQtMTYuOTYsNzguNDk2LTQwLjA5NkMzNDMuOTE2LDYxLjU2OCwzNTYuNTU2LDMxLjEwNCwzNTEuOTgsMHoiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiLz4NCgk8L2c+DQo8L2c+DQo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPg0KCTxnPg0KCQk8cGF0aCBkPSJNNDU5Ljg1MiwxNzEuNzc2Yy0yNi4yMDgtMzIuODY0LTYzLjA0LTUxLjkzNi05Ny44MjQtNTEuOTM2Yy00NS45MiwwLTY1LjM0NCwyMS45ODQtOTcuMjQ4LDIxLjk4NCAgICBjLTMyLjg5NiwwLTU3Ljg4OC0yMS45Mi05Ny42LTIxLjkyYy0zOS4wMDgsMC04MC41NDQsMjMuODQtMTA2Ljg4LDY0LjYwOGMtMzcuMDI0LDU3LjQwOC0zMC42ODgsMTY1LjM0NCwyOS4zMTIsMjU3LjI4ICAgIGMyMS40NzIsMzIuODk2LDUwLjE0NCw2OS44ODgsODcuNjQ4LDcwLjIwOGMzMy4zNzYsMC4zMiw0Mi43ODQtMjEuNDA4LDg4LTIxLjYzMmM0NS4yMTYtMC4yNTYsNTMuNzkyLDIxLjkyLDg3LjEwNCwyMS41NjggICAgYzM3LjUzNi0wLjI4OCw2Ny43NzYtNDEuMjgsODkuMjQ4LTc0LjE3NmMxNS4zOTItMjMuNTg0LDIxLjEyLTM1LjQ1NiwzMy4wNTYtNjIuMDggICAgQzM4Ny44NTIsMzQyLjYyNCwzNzMuOTMyLDIxOS4xNjgsNDU5Ljg1MiwxNzEuNzc2eiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiIvPg0KCTwvZz4NCjwvZz4NCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCjwvZz4NCjwvc3ZnPg0K"
          target="_blank"
          class="mx-2.5 mt-5 inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100"
          style="background-color: rgb(5, 0, 56);">
          <span>
            <img
              src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDUxMS45OTkgNTExLjk5OSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTExLjk5OSA1MTEuOTk5OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojMzJCQkZGOyIgZD0iTTM4Mi4zNjksMTc1LjYyM0MzMjIuODkxLDE0Mi4zNTYsMjI3LjQyNyw4OC45MzcsNzkuMzU1LDYuMDI4DQoJCUM2OS4zNzItMC41NjUsNTcuODg2LTEuNDI5LDQ3Ljk2MiwxLjkzbDI1NC4wNSwyNTQuMDVMMzgyLjM2OSwxNzUuNjIzeiIvPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiMzMkJCRkY7IiBkPSJNNDcuOTYyLDEuOTNjLTEuODYsMC42My0zLjY3LDEuMzktNS40MDEsMi4zMDhDMzEuNjAyLDEwLjE2NiwyMy41NDksMjEuNTczLDIzLjU0OSwzNnY0MzkuOTYNCgkJYzAsMTQuNDI3LDguMDUyLDI1LjgzNCwxOS4wMTIsMzEuNzYxYzEuNzI4LDAuOTE3LDMuNTM3LDEuNjgsNS4zOTUsMi4zMTRMMzAyLjAxMiwyNTUuOThMNDcuOTYyLDEuOTN6Ii8+DQoJPHBhdGggc3R5bGU9ImZpbGw6IzMyQkJGRjsiIGQ9Ik0zMDIuMDEyLDI1NS45OEw0Ny45NTYsNTEwLjAzNWM5LjkyNywzLjM4NCwyMS40MTMsMi41ODYsMzEuMzk5LTQuMTAzDQoJCWMxNDMuNTk4LTgwLjQxLDIzNy45ODYtMTMzLjE5NiwyOTguMTUyLTE2Ni43NDZjMS42NzUtMC45NDEsMy4zMTYtMS44NjEsNC45MzgtMi43NzJMMzAyLjAxMiwyNTUuOTh6Ii8+DQo8L2c+DQo8cGF0aCBzdHlsZT0iZmlsbDojMkM5RkQ5OyIgZD0iTTIzLjU0OSwyNTUuOTh2MjE5Ljk4YzAsMTQuNDI3LDguMDUyLDI1LjgzNCwxOS4wMTIsMzEuNzYxYzEuNzI4LDAuOTE3LDMuNTM3LDEuNjgsNS4zOTUsMi4zMTQNCglMMzAyLjAxMiwyNTUuOThIMjMuNTQ5eiIvPg0KPHBhdGggc3R5bGU9ImZpbGw6IzI5Q0M1RTsiIGQ9Ik03OS4zNTUsNi4wMjhDNjcuNS0xLjgsNTMuNTItMS41NzcsNDIuNTYxLDQuMjM5bDI1NS41OTUsMjU1LjU5Nmw4NC4yMTItODQuMjEyDQoJQzMyMi44OTEsMTQyLjM1NiwyMjcuNDI3LDg4LjkzNyw3OS4zNTUsNi4wMjh6Ii8+DQo8cGF0aCBzdHlsZT0iZmlsbDojRDkzRjIxOyIgZD0iTTI5OC4xNTgsMjUyLjEyNkw0Mi41NjEsNTA3LjcyMWMxMC45Niw1LjgxNSwyNC45MzksNi4xNTEsMzYuNzk0LTEuNzg5DQoJYzE0My41OTgtODAuNDEsMjM3Ljk4Ni0xMzMuMTk2LDI5OC4xNTItMTY2Ljc0NmMxLjY3NS0wLjk0MSwzLjMxNi0xLjg2MSw0LjkzOC0yLjc3MkwyOTguMTU4LDI1Mi4xMjZ6Ii8+DQo8cGF0aCBzdHlsZT0iZmlsbDojRkZENTAwOyIgZD0iTTQ4OC40NSwyNTUuOThjMC0xMi4xOS02LjE1MS0yNC40OTItMTguMzQyLTMxLjMxNGMwLDAtMjIuNzk5LTEyLjcyMS05Mi42ODItNTEuODA5bC04My4xMjMsODMuMTIzDQoJbDgzLjIwNCw4My4yMDVjNjkuMTE2LTM4LjgwNyw5Mi42LTUxLjg5Miw5Mi42LTUxLjg5MkM0ODIuMjk5LDI4MC40NzIsNDg4LjQ1LDI2OC4xNyw0ODguNDUsMjU1Ljk4eiIvPg0KPHBhdGggc3R5bGU9ImZpbGw6I0ZGQUEwMDsiIGQ9Ik00NzAuMTA4LDI4Ny4yOTRjMTIuMTkxLTYuODIyLDE4LjM0Mi0xOS4xMjQsMTguMzQyLTMxLjMxNEgyOTQuMzAzbDgzLjIwNCw4My4yMDUNCglDNDQ2LjYyNCwzMDAuMzc5LDQ3MC4xMDgsMjg3LjI5NCw0NzAuMTA4LDI4Ny4yOTR6Ii8+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg=="
              style="height: 2rem;" />
          </span>
          <span class="ml-2">
            <span class="block text-xs">Download from</span>
            <span class="block text-lg font-semibold leading-none">Google Play</span>
          </span>
        </a>
      </div>
    </div>
    <div class="mt-14">
      <div class="text-center">
        <div class="text-3xl font-semibold">@Jamesjafere</div>
        <div><span class="inline-flex mx-3 mt-1">^S1</span></div>
        <div class="inline-flex items-center mt-2"><span class="text-lg font-bold">♬</span><span
            class="ml-1">Original</span></div>
        <div class="mt-5">
          <div class="relative inline-block">
            <video playsinline="" class="max-w-screen-lg w-full inline-flex"
              src="https://caretsapp.s3.us-east-1.amazonaws.com/Videos/741_1613699250.mp4"></video>
            <button class="playBtn">
              <img
                src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGZpbGw9IiNmZmZmZmYiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgMzg0IDM4NCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzg0IDM4NDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0wLDIxLjMzM2wxOTIsMzQxLjMzM0wzODQsMjEuMzMzSDB6IE03Miw2NGgyNDBMMTkyLDI3Ny4zMzNMNzIsNjR6Ii8+DQoJPC9nPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo="
                alt="Play" />
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>