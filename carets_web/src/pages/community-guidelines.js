import React, {useState} from 'react';
import {Helmet} from 'react-helmet'
import { Scrollbars } from 'react-custom-scrollbars';
import Header from '../components/header';

const faqs = [
  {
    quest: `How does Carets work?`,
    ans: `The concept of Carets is simple. Videos are created through the Carets app, content is uploaded to the cloud via a wireless connection, and your videos, along with others matching location or tagging criteria, is available to view through our patented video player.`,
    show:false
  },

  {
    quest: `Do I require a user name?`,
    ans: `To create video content you are required to set up a user account which requires a user name.  For viewing content on a handheld device the media player is designed to work in the app. `,
    show:false
  },

  {
    quest: `I forgot my username and password; how do I retrieve this information?`,
    ans: `Username and password retrieval can be accomplished through the app.`,
    show:false
  },

  {
    quest: `Can I upload video content that isn’t created through the app?`,
    ans: `Yes, video stored on your mobile device can be uploaded through the app however the syncing features are limited.  `,
    show:false
  },

  {
    quest: `What kind of devices can I use with Carets?`,
    ans: `Carets is designed for Apple iOS and Android devices.  `,
    show:false
  },

  {
    quest: `What kind of content can I create?`,
    ans: `This platform has been designed for our users to create whatever they want as long it isn’t offensive.  We encourage our users to get creative and come up with new and exciting ways to implement the technology for everyone’s entertainment. You can learn more about acceptable content through the Carets Community Guidelines.`,
    show:false
  },

  {
    quest: `What do I do if I see a video that does not meet the community guidelines?`,
    ans: `There is a reporting feature in the app that allows users to flag content. Once flagged the Carets team is notified to review content related to that video for content.  Violators of the Carets Community Guidelines or any of the Terms and Conditions may result in account termination as outlined. Final determination is made by Carets.`,
    show:false
  },

  {
    quest: `I have created a video but I’m having a difficult time uploading it; what do ,I do?`,
    ans: `The app is designed to upload directly to the server.  In the event there are network connectivity problems between your device and the network the upload may fail resulting in video loss.  `,
    show:false
  },

  {
    quest: `Can I store the videos I created in the app on my device?`,
    ans: `Yes, there is an option to store the user’s videos at the time of upload.`,
    show:false
  },

  {
    quest: `How can I remove videos I don’t want anymore?`,
    ans: `To remove videos you must use the app to retrieve and manage user content.`,
    show:false
  },
]

export default () => {
  const[faqData, setFaqData] = useState(faqs);

  const faqSow = (idx) => {
    let newFaq = [];
    
    faqs.forEach((element, index) => {
      if(idx === index){
        element.show = !element.show;
      }
      newFaq.push(element);
    });

    setFaqData(newFaq);
  }

  return (<React.Fragment>
  <Helmet>
    <meta name="robots" content="noindex,nofollow" />
    <meta name="googlebot" content="noindex,nofollow" />
    <title>Carets | Create, share, and combine video content</title>
  </Helmet>

    <div className="customContainer">
        <div>
        <Header isCenter={true}/>
        </div>
    </div>

    <div className="bg-gray-100 py-12 mt-16">
        <h1 className="text-4xl text-center px-5 font-semibold mb-6">^Carets – Community Guidelines</h1>
        <div className="customContainer border-t border-b py-7 px-7">
            <Scrollbars style={{ width: '100%' }} className="mt-3" autoHeight autoHeightMax={400}>
              <p className="text-lg 2xl:text-base">The ^Carets™ platform is a family friendly service model designed to enable our users to easily generate video content using handheld devices and posting this content to the app. The app is designed to aggregate videos from multiple users for concurrent and simultaneous viewing.  The content you have generated may be viewed with other user’s content enabling a different perspective; this combined video is what we call a ^Carets Video™.</p>
            </Scrollbars>
        </div>
    </div>
    
    <div className="customContainer">
      <h2 className="text-3xl mt-16 font-semibold">^Carets Community Guidelines</h2>
      <p className="mt-3 text-lg 2xl:text-base">The Carets community is intended to enable user to be creative in developing content and sharing with fellow users of the platform.  Carets potential is only gated by the creativity and imagination of its users. The platform is intended to be a family friendly and we ask that you help us accomplish this vision by creating and posting content that aligns with this intent.  </p>
      <p className="mt-3 text-lg 2xl:text-base">As with all good plans there are usually groups and individuals who have a different set of values and will attempt to abuse the system. We ask for your help to notify us if content appears to be out of line by reporting issues through the website.</p>
      
      <div>
        <h3 className="text-2xl font-semibold mt-10">Common Sense and General Guidelines</h3>
        <p className="mt-3 text-lg 2xl:text-base">This is a family friend site; the following are <u>examples of unacceptable content</u> and violation of these common sense rules may result in removal of your account and content. In cases where videos containing illegal activities are uploaded to the site and it is reported by other users this content and associated user account information will be issued to the appropriate authorities.</p>  
        
        <div>
            <ul className="styledList mt-3">
              <li>Nudity or Sexual Content: This includes pornography or sexually explicit content.</li>
              <li>Harmful or Dangerous Content: Any content that encourages others to do things that may be harmful to themselves or to others. </li>
              <li>Violent or Graphic Content: There are two types of content; anything that is intended to sensational or disrespectful is inappropriate. Content that could be found on the news or presented in a manner consistent with a documentary may be deemed acceptable.  In the latter case please provide audible narrative providing context as you create content. </li>
              <li>Hateful Content: This includes derogatory or inflammatory content that is intended to degrade or condones violence towards individuals or groups based on race, ethnicity, religion, or sexual orientation.</li>
              <li>Threats: This includes anything that could be represented as threatening to individuals or groups.  The Carets platform is not intended to be used for bullying, stalking, harassment, invading privacy, or predatory behaviors.  </li>
              <li>Misrepresentations, Spam, and Trolling: Please refrain from misrepresentation, spamming, and trolling various users. This also includes creating content and misleading users with headlines and titles that do not align with the intentions. </li>
              <li>Copyright: Please respect copyrights and be mindful that pre-recorded content and images may be copyrighted and that redistribution may not be allowed or licensed.</li>
            </ul>
        </div>

        <p className="mt-5 text-lg 2xl:text-base">We review reports from the website carefully. In the event that content is considered inappropriate, and Carets reserves the right to be the final judge in such decisions, that content may be removed from viewers and accounts may be permanently deactivated.</p>

        <h4 className="text-3xl mt-16 font-semibold">Carets Privacy Notice</h4>
        <p className="mt-3 text-lg 2xl:text-base">There are many different ways you can use Carets – to search for and share video content created on User Account’s mobile devices.  We’ve attempted to keep our Privacy Notice simple. Your privacy matters so whether you are new or a long-time user, please do take the time to get to know our practices – and if you have any questions contact us.</p>

        <p className="mt-5 text-xl 2xl:text-base font-medium">We collect information to provide better services to all of our users. We collect information in the following ways:</p>  
        
        <div className="mt-5">
            <ul className="styledList">
              <li>Information you give us. </li>
              <li>Information we get from your use of our services. </li>
              <li>Device information</li>
              <li>Log-in information</li>
              <li>Location information</li>
              <li>Unique application numbers</li>
              <li>Local storage</li>
              <li>Cookies and similar technologies</li>
              <li>User name</li>
              <li>Comments posted on the app</li>
            </ul>

            <div className="text-2xl font-semibold mt-10">Transparency and choice</div>
            <p className="mt-3 text-lg 2xl:text-base">People have different privacy concerns. Our goal is to be clear about what information we collect, so that you can make meaningful choices about how it is used. For example, you can:</p>
            <ul className="styledList mt-3">
              <li>Review and update your Carets controls to decide what types of data, such as videos you’ve watched on Carets or past searches, you would like saved with your account when you use our services.</li>
              <li>Adjust how the Profile associated with your Account appears to others. Our default is to use your user name on published video content. </li>
            </ul>
            
            <div className="text-2xl font-semibold mt-10">Information you share</div>
            <p className="mt-3 text-lg 2xl:text-base">Our service is intended to allow you to share information with others. Remember that when you share information publicly it may be indexable by search engines.  You agree to share your User Name with your published content. You also agree to share GPS and time data with video content that is uploaded. You may adjust your settings to limit data provided to Carets. </p>
            
            <div className="text-2xl font-semibold mt-10">Information we share</div>
            <p className="mt-3 text-lg 2xl:text-base">We do not share personal information with companies, organizations and individuals outside of Carets unless one of the following circumstances applies:</p>
            <ul className="styledList mt-3">
              <li>With your consent</li>
              <li>With domain administrators <br />
              If your Account is managed for you by a domain administrator then your domain administrator and resellers who provide user support to your organization will have access to your User Account information (including your email and other data). Your domain administrator may be able to:
                <ul className="capitalLetter">
                  <li>view statistics regarding your account, like statistics regarding applications you install.</li>
                  <li>change your account password.</li>
                  <li>suspend or terminate your account access.</li>
                  <li>access or retain information stored as part of your account.</li>
                  <li>receive your account information in order to satisfy applicable law, regulation, legal process or enforceable governmental request.</li>
                  <li>restrict your ability to delete or edit information or privacy settings.</li>
                </ul>
              </li>
              <li>For legal reasons <br />
              We will share personal information with companies, organizations or individuals outside of Carets if we have a good-faith belief that access, use, preservation or disclosure of the information is reasonably necessary to:
                <ul className="capitalLetter">
                  <li>meet any applicable law, regulation, legal process or enforceable governmental request.</li>
                  <li>enforce applicable Terms of Service, including investigation of potential violations.</li>
                  <li>detect, prevent, or otherwise address fraud, security or technical issues.</li>
                  <li>protect against harm to the rights, property or safety of Carets, our users or the public as required or permitted by law.</li>
                </ul>
              </li>
            </ul>

            <p className="mt-5 text-lg 2xl:text-base">We may share non-personally identifiable information publicly and with our partners – like publishers, advertisers or connected sites. For example, we may share information publicly to show trends about the general use of our services. This may include a user’s name displayed with associated content.</p>
            <p className="mt-3 text-lg 2xl:text-base">If Carets is involved in a merger, acquisition or asset sale, we will continue to ensure the confidentiality of any personal information and give affected users notice before personal information is transferred or becomes subject to a different privacy policy.</p>
            
            <div className="text-2xl font-semibold mt-10">Information security</div>
            <p className="mt-3 text-lg 2xl:text-base">We work hard to protect Carets and our users from unauthorized access to or unauthorized alteration, disclosure or destruction of information we hold. In particular:</p>
            <ul className="styledList mt-3">
              <li>We encrypt many of our services using SSL.</li>
              <li>We review our information collection, storage and processing practices, including physical security measures, to guard against unauthorized access to systems.</li>
              <li>We restrict access to personal information to Carets employees, contractors and agents who need to know that information in order to process it for us, and who are subject to strict contractual confidentiality obligations and may be disciplined or terminated if they fail to meet these obligations.</li>
            </ul>

            <div className="text-2xl font-semibold mt-10">When this Privacy Notice applies</div>
            <p className="mt-3 text-lg 2xl:text-base">Our Privacy Notice applies to all of the services offered by Carets and its affiliates. Our Privacy Notice does not apply to services offered by other companies or individuals, including products or sites that may be displayed to you in search results, sites that may include services, or other sites linked from our services. Our Privacy Notice does not cover the information practices of other companies and organizations who advertise our services, and who may use cookies, pixel tags and other technologies to serve and offer relevant ads.</p>

            <div className="text-2xl font-semibold mt-10">Compliance and cooperation with regulatory authorities</div>
            <p className="mt-3 text-lg 2xl:text-base">We regularly review our compliance with our Privacy Notice. When we receive formal written complaints, we will contact the person who made the complaint to follow up. We work with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of personal data that we cannot resolve with our users directly.</p>

            <div className="text-2xl font-semibold mt-10">Changes</div>
            <p className="mt-3 text-lg 2xl:text-base">Our Privacy Notice may change from time to time. We will post any Privacy Notice changes on this page and, if the changes are significant, we will provide a more prominent notice (including, for certain services, email notification of Privacy Notice changes). </p>
          </div>  
    </div>
  </div>  
  </React.Fragment>)
}
