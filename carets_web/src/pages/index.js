import React from "react";
import Header from "../components/header";
import { Helmet } from "react-helmet";

import video1 from "../assets/imgs/11.png";
import video2 from "../assets/imgs/22.png";
import video3 from "../assets/imgs/33.png";
import video4 from "../assets/imgs/44.png";
// import video from "../assets/intro.mp4";
// import videoDesktop from "../assets/caretsintrodesktop.mp4";

export default () => (
    <React.Fragment>
        <Helmet>
            <meta name="robots" content="noindex,nofollow" />
            <meta name="googlebot" content="noindex,nofollow" />
            <title>Carets | Create, share, and combine video content</title>
        </Helmet>

        <div className="homePage">
            <div className="container1 mx-auto mb-8">
                <Header />
            </div>
            <div className="flex flex-col container w-full max-w-5xl mx-auto">
                <div className="">
                    <div
                        // style={{ backgroundColor: "#E8E9EE" }}
                        className="flex overflow-hidden justify-center items-end mx-auto"
                    >
                        <video
                            playsInline={true}
                            src="https://caretsffmpeg.s3.amazonaws.com/promosvideos/caretsintrodesktop.mp4"
                            autoPlay={true}
                            muted={false}
                            loop={true}
                            preload="auto"
                            controls={true}
                            style={{ marginBottom: "-1px" }}
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 md:grid-cols-4 gap-4 mt-auto w-full max-w-5xl mx-auto">
                    <div className="mt-9">
                        <img src={video1} className="w-full" alt="Video 1" />
                    </div>
                    <div className="mt-9">
                        <img src={video2} className="w-full" alt="Video 2" />
                    </div>
                    <div className="mt-9">
                        <img src={video3} className="w-full" alt="Video 3" />
                    </div>
                    <div className="mt-9">
                        <img
                            src={video4}
                            className="w-full h-full object-cover"
                            alt="Video 4"
                        />
                    </div>
                </div>
            </div>
            {/* <div className="grid grid-cols-1 lg:grid-cols-2 gap-6 ml-0 lg:ml-24">
                <div className="flex flex-col px-7 lg:px-0 items-center lg:items-start">
                    <Header />

                    <div className="grid grid-cols-3 gap-4 mt-auto w-full">
                        <div className="mt-9">
                            <img
                                src={video_1}
                                className="w-full"
                                alt="Video 1"
                            />
                        </div>
                        <div className="mt-9">
                            <img
                                src={video_2}
                                className="w-full"
                                alt="Video 2"
                            />
                        </div>
                        <div className="mt-9">
                            <img
                                src={video_3}
                                className="w-full"
                                alt="Video 3"
                            />
                        </div>
                    </div>
                </div>

                <div
                    style={{ backgroundColor: "#E8E9EE" }}
                    className="flex overflow-hidden justify-center md:justify-end items-end"
                >
                    <video
                        playsInline={true}
                        src={video}
                        autoPlay={true}
                        muted={true}
                        loop={true}
                        preload="auto"
                        style={{ marginBottom: "-1px" }}
                    />
                </div>
            </div> */}
        </div>
    </React.Fragment>
);
