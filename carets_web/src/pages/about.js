import React, {useState} from 'react';
import {Helmet} from 'react-helmet'
import { Scrollbars } from 'react-custom-scrollbars';
import Header from '../components/header';

export default () => {
  return (<React.Fragment>
  <Helmet>
    <meta name="robots" content="noindex,nofollow" />
    <meta name="googlebot" content="noindex,nofollow" />
    <title>Carets | Create, share, and combine video content</title>
  </Helmet>

    <div className="customContainer">
        <div>
        <Header isCenter={true}/>
        </div>
    </div>

    <div className="bg-gray-100 py-12 mt-16">
        <h1 className="text-4xl text-center px-5 font-semibold mb-6">About ^Carets</h1>
        <div className="customContainer border-t border-b py-7 px-7">
            <Scrollbars style={{ width: '100%' }} className="mt-3" autoHeight autoHeightMax={400}>
                <p className="text-lg 2xl:text-base">Carets is fun, family friendly app designed to enable users to integrate multiple videos from multiple users into a single interface.   We have created an our video interface based on our patented media player technology showing consolidated content. </p>
                <p className="mt-5 text-lg 2xl:text-base">Its simple to use. You can create a video, tag it with a ^Caret designation just like how you use a #hashtag or @user symbol, and your videos will be consolidated automatically with content from other users with the same ^Caret designation. Its fun, it’s easy, and it is pretty amazing to think of the possibilities.</p>
                <p className="mt-5 text-lg 2xl:text-base">Image, the power of multiple cameras generating videos from their own perspective and being presented with other content in a single medium; you may not even know who the other people are who creating concurrent content of the same event but here you can see it.   </p>
                <p className="mt-5 text-lg 2xl:text-base">We invite you to download the ^Carets app, create your own content, and share your view of the world.  Invite your friends and family to participate and then you can all start to see what we see.</p>
            </Scrollbars>
        </div>
    </div>
</React.Fragment>)
}
