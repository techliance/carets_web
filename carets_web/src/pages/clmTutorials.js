import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { Scrollbars } from "react-custom-scrollbars";
import Header from "../components/header";
// useEffect(() => {
//     window.scrollTo({ top: 0, behavior: "smooth" });
// }, []);
export default () => {
    return (
        <React.Fragment>
            <Helmet>
                <meta name="robots" content="noindex,nofollow" />
                <meta name="googlebot" content="noindex,nofollow" />
                {/* <title>Carets | Create, share, and combine video content</title> */}
                <title>CLM Tutorials</title>
            </Helmet>

            <div className="customContainer" tabIndex={-1}>
                <div>
                    <Header isCenter={true} />
                </div>
            </div>

            <div className="bg-gray-100 mt-16">
                {/* <h1 className="text-4xl text-center px-5 font-semibold mb-6">
                    CLM Tutorials
                </h1> */}
                <div className="LMmainSection">
                    <div className="flexRowF">
                        <div className="content container py-16 mx-auto">
                            <div className="LMheroSection">
                                <div className="py-8">
                                    <h1 className="text-white text-2xl md:text-5xl font-bold text-center">
                                        Licensing Module Tutorial
                                    </h1>
                                    <h3 className="text-white text-center text-xl md:text-3xl mt-5">
                                        Learn how to build new videos
                                        <br />
                                        with your licensed content.
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="container py-16 mx-auto">
                    <h2 class="text-center mb-5 mt-3 text-xl md:text-4xl font-semibold">
                        When app users tag their videos with your ^Caret you
                        <br />
                        can use that video to create new content.
                    </h2>
                    <div className="w-full max-w-sm mx-auto grid grid-cols-2 px-4">
                        <div>
                            <img
                                className="appuser-videoBanner w-full"
                                src="/video1.png"
                                alt="Video 1"
                            />
                        </div>
                        <div>
                            <img
                                className="appuser-videoBanner w-full"
                                src="/video2.png"
                                alt="Video 2"
                            />
                        </div>
                        <div>
                            <img
                                className="appuser-videoBanner w-full"
                                src="/video3.png"
                                alt="Video 3"
                            />
                        </div>
                        <div>
                            <img
                                className="appuser-videoBanner w-full"
                                src="/video4.png"
                                alt="Video 4"
                            />
                        </div>
                    </div>
                </section>
            </div>
            <section className="licensedCarets py-16 px-4 mx-auto">
                <div className="container mx-auto">
                    <h3 className="text-center mb-5 mt-3 text-xl md:text-4xl font-semibold">
                        Get started in three simple steps.
                    </h3>
                    <section className="mb-16">
                        <div className="stepOne relative">
                            <h4 className="text-center text-xl md:text-3xl font-semibold mt-10 mb-8">
                                Upload custom content
                            </h4>
                            <div className="currentStep">Step: 1</div>
                            <div className="BLGridStep">
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig1.png"
                                            alt="Figure One"
                                            className="mx-auto"
                                        />
                                        <figcaption className="pt-2">
                                            Splash
                                        </figcaption>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig2.png"
                                            alt="Figure Two"
                                            className="mx-auto"
                                        />
                                        <figcaption className="relative pt-2">
                                            Videos
                                        </figcaption>
                                        <div className="assetVideo">
                                            <img
                                                src="/play-icon.svg"
                                                alt="Play"
                                                width={18}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig3.png"
                                            alt="Figure Three"
                                            className="mx-auto"
                                        />
                                        <figcaption className="relative pt-2">
                                            Watermark
                                        </figcaption>
                                        <div className="assetWatermark">
                                            <img
                                                src="/caretFavicon.svg"
                                                alt="Audio"
                                                width={48}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig4.png"
                                            alt="Figure Four"
                                            className="mx-auto"
                                        />
                                        <figcaption className="relative pt-2">
                                            Custom Audio
                                        </figcaption>
                                        <div className="assetAudio">
                                            <img
                                                src="/Audio.svg"
                                                alt="Audio"
                                                width={40}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig5.png"
                                            alt="Figure Five"
                                            className="mx-auto"
                                        />
                                        <figcaption className="relative pt-2">
                                            Promotions
                                        </figcaption>
                                        <div className="assetPromo">
                                            <img
                                                src="/promo.png"
                                                alt="Audio"
                                                width={72}
                                            />
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div className="stepTitle">
                            <figcaption className="relative pt-8">
                                Splash
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Videos
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Watermark
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Custom Audio
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Promotions
                            </figcaption>
                        </div>
                    </section>
                    <section className="mb-16">
                        <div className="stepOne stepTwo relative">
                            <h4 className="text-center text-xl md:text-3xl font-semibold mt-10 mb-8">
                                Assemble a new licensed ^Carets video.
                            </h4>
                            <div className="currentStep">Step: 2</div>
                            <div className="BLGridStep">
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig1.png"
                                            alt="Figure One"
                                            className="mx-auto"
                                        />
                                        <button
                                            className="mt-1 btn btnPurple text-white"
                                            style={{ maxWidth: 110 }}
                                        >
                                            Selected
                                        </button>
                                        <figcaption className="pt-2">
                                            Splash
                                        </figcaption>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig2.png"
                                            alt="Figure Two"
                                            className="mx-auto"
                                        />
                                        <button
                                            className="mt-1 btn btnPurple text-white"
                                            style={{ maxWidth: 110 }}
                                        >
                                            Selected
                                        </button>
                                        <figcaption className="relative pt-2">
                                            Videos
                                        </figcaption>
                                        <div className="assetVideo">
                                            <img
                                                src="/play-icon.svg"
                                                alt="Play"
                                                width={18}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig3.png"
                                            alt="Figure Three"
                                            className="mx-auto"
                                        />
                                        <button
                                            className="mt-1 btn btnPurple text-white"
                                            style={{ maxWidth: 110 }}
                                        >
                                            Selected
                                        </button>
                                        <figcaption className="relative pt-2">
                                            Watermark
                                        </figcaption>
                                        <div className="assetWatermark">
                                            <img
                                                src="/caretFavicon.svg"
                                                alt="Audio"
                                                width={48}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig4.png"
                                            alt="Figure Four"
                                            className="mx-auto"
                                        />
                                        <button
                                            className="mt-1 btn btnPurple text-white"
                                            style={{ maxWidth: 110 }}
                                        >
                                            Selected
                                        </button>
                                        <figcaption className="relative pt-2">
                                            Custom Audio
                                        </figcaption>
                                        <div className="assetAudio">
                                            <img
                                                src="/Audio.svg"
                                                alt="Audio"
                                                width={40}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig5.png"
                                            alt="Figure Five"
                                            className="mx-auto"
                                        />
                                        <button
                                            className="mt-1 btn btnPurple text-white"
                                            style={{ maxWidth: 110 }}
                                        >
                                            Selected
                                        </button>
                                        <figcaption className="relative pt-2">
                                            Promotions
                                        </figcaption>
                                        <div className="assetPromo">
                                            <img
                                                src="/promo.png"
                                                alt="Audio"
                                                width={72}
                                            />
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div className="stepTitle">
                            <figcaption className="relative pt-8">
                                Select Splash
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Select Videos
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Select Watermark
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Select Audio
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Select Promotion
                            </figcaption>
                        </div>
                    </section>
                    <section>
                        <div className="stepOne stepThree relative">
                            <h4 className="text-center text-xl md:text-3xl font-semibold mt-10 mb-8">
                                Use your new licensed ^Caret videos.
                            </h4>
                            <div className="currentStep">Step: 3</div>
                            <div className="BLGridStep">
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig1.png"
                                            alt="Figure One"
                                            className="mx-auto"
                                        />
                                        <figcaption className="pt-2">
                                            Splash
                                        </figcaption>
                                        <div className="publishApp">
                                            <img
                                                src="/publish-icon.png"
                                                alt="publishIcon"
                                                width={60}
                                            />
                                        </div>
                                    </figure>
                                </div>
                                <div>
                                    <figure className="text-center relative">
                                        <img
                                            src="/fig2.png"
                                            alt="Figure Two"
                                            className="mx-auto"
                                        />
                                        <figcaption className="relative pt-8">
                                            Videos
                                        </figcaption>
                                        <div className="downloadApp">
                                            <img
                                                src="/video-play-download-icon.svg"
                                                alt="video Download Icon"
                                                width={60}
                                            />
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div className="stepTitle">
                            <figcaption className="relative pt-8">
                                Publish in the Carets App
                            </figcaption>
                            <figcaption className="relative pt-8">
                                Download for your use
                            </figcaption>
                        </div>
                    </section>
                </div>
            </section>
        </React.Fragment>
    );
};
