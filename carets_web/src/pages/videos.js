import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import Header from "../components/header";
import User from "../components/user";
import axios from "axios";

import { basePath } from "../helpers/basePath";
//import video from 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4';
const baseURL = basePath; //"http://carets_web.test/api/v1/videos/singleWeb/";
import { Route, useParams } from "react-router-dom";
// Get ID from URL

export default () => {
    const [videoData, setVideoData] = useState({
        username: "",
        tags: [],
        music: "",
        videoUrl: "",
        imageUrl: ""
    });


    useEffect(() => {
        const video_id = new URLSearchParams(location.search).get("video_id");
        console.log(video_id);
        const newurl = baseURL + "videos/singleWeb/" + video_id;
        axios.get(newurl).then(response => {
            setVideoData(response.data);
        });
    }, []);

    return (
        <React.Fragment>
            <Helmet>
                <title>{videoData.username}</title>
                <meta property="og:image" content={videoData.imageUrl} />
                <meta
                    property="og:image:secure_url"
                    content={videoData.imageUrl}
                />
                <meta property="og:image:type" content="image/gif" />
                <meta property="og:image:width" content="400" />
                <meta property="og:image:height" content="300" />
                <meta property="og:video" content={videoData.videoUrl} />
                <meta
                    property="og:video:secure_url"
                    content={videoData.videoUrl}
                />
                <meta property="og:video:type" content="video/mp4" />
                <meta property="og:video:width" content="1200" />
                <meta property="og:video:height" content="630" />
                <meta property="og:type" content="video" />
                <meta property="og:title" content={videoData.username} />
                <meta property="og:description" content={videoData.tags[0]} />
            </Helmet>

            <div className="customContainer">
                <div>
                    <Header isCenter={true} />
                </div>

                <div className="mt-14">
                    <User data={videoData} />
                </div>
            </div>
        </React.Fragment>
    );
};
