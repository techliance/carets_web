import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { Scrollbars } from "react-custom-scrollbars";
import Header from "../components/header";
import Instruct1 from "../assets/imgs/instruct-1.png";
import Instruct2 from "../assets/imgs/instruct-2.png";
import Instruct3 from "../assets/imgs/instruct-3.png";
import deleteIcon from "../assets/imgs/delete-icon.svg";
import deleteHeroBG from "../assets/imgs/delete-hero-bg.png";
export default () => {
    return (
        <React.Fragment>
            <Helmet>
                <meta name="robots" content="noindex,nofollow" />
                <meta name="googlebot" content="noindex,nofollow" />
                <title>Carets | Create, share, and combine video content</title>
            </Helmet>

            <div className="customContainer">
                <div>
                    <Header isCenter={true} />
                </div>
            </div>
            <div className="grid md:grid-cols-2 gap-10 bgDelete py-16 px-8 items-center mt-10">
                <h3 style={{ textAlign: "center" }} className="text-2xl md:text-5xl font-bold text-white leading-normal text-center md:text-left">
                    Want To Delete Your Account?
                </h3>
                <img
                    src={deleteIcon}
                    alt="delete icon"
                    className="max-h-96 mx-auto"
                />
            </div>
            <div className="bg-gray-1001 py-12 mt-16">
                <h1 className="text-4xl text-center px-5 font-bold mb-6 text-gray-800">
                    Delete Account Instructions
                </h1>
                <div className="grid md:grid-cols-3 gap-8 px-5 py-8">
                    <div className="text-center px-8 md:px-24">
                        <img
                            src={Instruct1}
                            alt="instruct 1"
                            className="mx-auto -mr-4 md:-mr-8"
                        />
                        <p className="text-lg 2xl:text-base mt-4">
                            Login into the app.
                        </p>
                    </div>
                    <div className="text-center px-8 md:px-24">
                        <img
                            src={Instruct2}
                            alt="instruct 2"
                            className="mx-auto -mr-4 md:-mr-8"
                        />
                        <p className="mt-4 text-lg 2xl:text-base">
                            After successfully authorizing, you will able to see
                            your video feed list screen. You need to click on
                            Profile icon from bottom bar.
                        </p>
                    </div>
                    <div className="text-center px-8 md:px-24">
                        <img
                            src={Instruct3}
                            alt="instruct 3"
                            className="mx-auto -mr-4 md:-mr-8"
                        />
                        <p className="mt-4 text-lg 2xl:text-base">
                            On Profile Screen you will see the{" "}
                            <span className="text-red-600 font-semibold">
                                DELETE ACCOUNT{" "}
                            </span>
                            button. Click on it and you will see a confirmation
                            dialog on the screen. Press{" "}
                            <span className="text-red-600 font-semibold">
                                YES
                            </span>{" "}
                            to delete the account.
                        </p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};
