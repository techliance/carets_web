import React, {useState} from 'react';
import {Helmet} from 'react-helmet'
import Header from '../components/header';

const faqs = [
  {
    quest: `How does Carets work?`,
    ans: `The concept of Carets is simple. Videos are created through the Carets app, content is uploaded to the cloud via a wireless connection, and your videos, along with others matching location or tagging criteria, is available to view through our patented video player.`,
    show:false
  },

  {
    quest: `Do I require a user name?`,
    ans: `To create video content you are required to set up a user account which requires a user name.  For viewing content on a handheld device the media player is designed to work in the app. `,
    show:false
  },

  {
    quest: `I forgot my username and password; how do I retrieve this information?`,
    ans: `Username and password retrieval can be accomplished through the app.`,
    show:false
  },

  {
    quest: `Can I upload video content that isn’t created through the app?`,
    ans: `Yes, video stored on your mobile device can be uploaded through the app however the syncing features are limited.  `,
    show:false
  },

  {
    quest: `What kind of devices can I use with Carets?`,
    ans: `Carets is designed for Apple iOS and Android devices.  `,
    show:false
  },

  {
    quest: `What kind of content can I create?`,
    ans: `This platform has been designed for our users to create whatever they want as long it isn’t offensive.  We encourage our users to get creative and come up with new and exciting ways to implement the technology for everyone’s entertainment. You can learn more about acceptable content through the Carets Community Guidelines.`,
    show:false
  },

  {
    quest: `What do I do if I see a video that does not meet the community guidelines?`,
    ans: `There is a reporting feature in the app that allows users to flag content. Once flagged the Carets team is notified to review content related to that video for content.  Violators of the Carets Community Guidelines or any of the Terms and Conditions may result in account termination as outlined. Final determination is made by Carets.`,
    show:false
  },

  {
    quest: `I have created a video but I’m having a difficult time uploading it; what do ,I do?`,
    ans: `The app is designed to upload directly to the server.  In the event there are network connectivity problems between your device and the network the upload may fail resulting in video loss.  `,
    show:false
  },

  {
    quest: `Can I store the videos I created in the app on my device?`,
    ans: `Videos will be saved on your device at the time of upload.`,
    show:false
  },

  {
    quest: `How can I remove videos I don’t want anymore?`,
    ans: `To remove the videos you uploaded go to your user profile page and hold your finger on the video you want to remove. A pop-up will confirm the request to remove it.`,
    show:false
  },
]

export default () => {
  const[faqData, setFaqData] = useState(faqs);

  const faqSow = (idx) => {
    let newFaq = [];
    
    faqs.forEach((element, index) => {
      if(idx === index){
        element.show = !element.show;
      }
      newFaq.push(element);
    });

    setFaqData(newFaq); 
  }

  return (
  <React.Fragment>
  <Helmet>
    <meta name="robots" content="noindex,nofollow" />
    <meta name="googlebot" content="noindex,nofollow" />
    <title>Carets | Create, share, and combine video content</title>
  </Helmet>

    <div className="customContainer">
        <div>
        <Header isCenter={true}/>
        </div>
    </div>

    <div className="customContainer">
      <h1 className="text-4xl mt-16 text-center px-5 font-semibold">Frequently Asked Questions</h1>

      <div className="faqs">
        {
          faqData &&
          faqData.map((elem, index) =>(
            <div key={index} className="faq mt-6">
              <div onClick={() => faqSow(index)} className="quest text-2xl cursor-pointer px-6 py-4 bg-gray-100 hover:bg-gray-200 transition-all ease-in-out duration-500 border border-gray-100 flex items-center justify-between">
                <div>{elem.quest}</div>
                <div className={(elem.show ? "faqToggle open" : "faqToggle")}><span></span><span></span></div>
              </div>
              {
                elem.show &&
                <div className={"ans px-6 py-4 text-xl leading-relaxed border border-gray-100 animFaq " + (elem.show ? "block" : "hidden")}>{elem.ans}</div>
              }
            </div>
          ))
        }
      </div>
    </div>
  </React.Fragment>
  )
}