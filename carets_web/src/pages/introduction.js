import React from "react";
import { Helmet } from "react-helmet";
import Header from "../components/header";
import User from "../components/user";
import video from "../assets/video.mp4";

const userData = {
    username: "@danialLavez",
    tags: ["#party", "^happy", "@fiesta"],
    music: "Justin Bieber - Yummy",
    videoUrl: video,
};

export default () => (
    <React.Fragment>
        <Helmet>
            <meta name="robots" content="noindex,nofollow" />
            <meta name="googlebot" content="noindex,nofollow" />
            <title>Carets | Create, share, and combine video content</title>
        </Helmet>

        <div className="customContainer1">
            <div>
                <Header isCenter={true} />
            </div>

            <div className="mt-14">
                <User data={userData} />
            </div>
        </div>
    </React.Fragment>
);
