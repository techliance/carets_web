export const basePath =
    process.env.NODE_ENV === "development"
        ? "https://api.carets.tv/api/v1/"
        : "https://api.carets.tv/api/v1/";
