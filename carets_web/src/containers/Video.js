import React, {useState, useEffect} from 'react';
import {Helmet} from 'react-helmet';
import axios from 'axios'
import Header from '../components/header';
import Userdata from '../components/userData';
import video from '../assets/video.mp4';
import {basePath} from '../helpers/basePath';
import loader from '../assets/imgs/ajax-loader.gif'

export default (props) => {
const [videoData, setData]  = useState(null);
const [dataLoader, setLoader]  = useState(loader);

useEffect(()=>{
  let queryParam = new URLSearchParams(props.location.search).get('id');

    queryParam &&
    axios.get(`${basePath}videos/showWeb/${queryParam}`, {
      headers: {
        'Content-Type': null,
        'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI3IiwianRpIjoiY2IyYmE1NThhZTAyNDgwZjc5OTJiZDQ5NGQ4ZjFhMWI3ZjI2ODkwNDQ5ZWMxNWY4NmJhNTRiMDY5MjVjNjJiNTcxMTQ4OTkwNTZjMWRhOTUiLCJpYXQiOjE2MDQ5MTc0NTMsIm5iZiI6MTYwNDkxNzQ1MywiZXhwIjoxNjM2NDUzNDUzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.heVKvWhwzR797mMh4qxy34kP1njHXdaOo-5mKqJvdS4INR1vn_y3-xtZeZYPwqNGBtKPVZiGePmIiqftbC7ch4h9aazQCPlNhlDDoVFzIqwvxvI9Qa-HXdsTfLUj18onMj2ZFemyOM6vXxpJsUKOSSduRSWDbwlkHBZUU1__5CwZ9-i-Er8oRU_rYEo9sVgcP7naAxE57Cf9zjp-R2C5CR6yHECIMsVjvSYNnTPvs_OKhhn-fpHKndxJrKchsVgN6aE75yrs3-ySYYCg3CGzrOn52gMs9zO8h3fsCCJhqLmlFXwN09n5ODGtBHnrHPuAeiK7pWSSI1jNB6IrZT7P9tKdLupoosfmclFQ9PHEkxkCRKoGimCWZDRloKs_qf-R6AZdjEOXvSVebJHmjsW02dksJW7kOjKiwl8dIN1-cPB2feurv0WYx8l6zP02mxgy_OCxQbEGhhYo_IzN-h9jzy_szsevdzt1baD-MjV5JRqO7RxSQ5neYxTJKX2SaRLmsVGEITjU4w01TXHgLHdwryO9egLRCMfq-wYdqNo34oUfUFtAlraugPQ41sGrFw4ri0VxUQR-P-QintUM8SaicT9s3w98noBkHraUqX2JF2FxXgexQzwvGa34CgQ8V31lfqaV-1x0_FTenrXIxv-o4sho82VRRJqp23gllaePd8A',
      },
    })
    .then(function (response) {
        // handle success
        setData(response.data)
        setLoader(false);
    })
    .catch(function (error) {
        // handle error
        console.log(error);
        setLoader(false);
    })
    .then(function () {
        // always executed
        setLoader(false);
    });
},[]);

return(    
  <React.Fragment>
  <Helmet>
    <meta name="robots" content="noindex,nofollow" />
    <meta name="googlebot" content="noindex,nofollow" />
    <title>Carets | Create, share, and combine video content</title>
  </Helmet>

  <div className="customContainer">
    <div>
      <Header isCenter={true}/>
    </div>

    <div className="mt-14">
      {
        dataLoader ?
        <div className="text-center"><img className="inline-block" src={dataLoader} alt="Loading..." /></div>
        :
        (
          videoData &&
          <Userdata dataloader={dataLoader} dummy={video} data={videoData} />
        )
      }
    </div>
  </div>
  </React.Fragment>
)
}