module.exports = {
    module: {
      rules: [
        {
            test: /\.(png|jpe?g|gif|svg)$/i,
            loader: 'file-loader',
            options: {
            name: '[path][name].[ext]',
            exclude: /\.inline.svg$/,
            },
        },
        {
            test: /\.(png|jpe?g|gif|svg)$/i,
            loader: 'url-loader',
            options: {
              limit: 25000,
            },
        },
      ],
    },
  };