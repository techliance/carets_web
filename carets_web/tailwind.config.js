module.exports = {
    purge: {
      content: [
        "./src/pages/**/*.js",
        "./src/components/**/*.js",
        "./src/plugins/**/*.js",
        "./src/static/**/*.js",
        "./src/store/**/*.js"
      ],
      options: {
        whitelist: ['bg-color-500']
      }
    },
    theme: {
      extend: {},
    },
    variants: {
      extend: {
        // ...
       animation: ['hover', 'focus'],
      }
    },
    plugins: [],
  
  }