(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(global, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded chunks
/******/ 	// "0" means "already loaded"
/******/ 	var installedChunks = {
/******/ 		0: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// The chunk loading function for additional chunks
/******/ 	// Since all referenced chunks are already included
/******/ 	// in this file, this function is empty here.
/******/ 	__webpack_require__.e = function requireEnsure() {
/******/ 		return Promise.resolve();
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// uncaught error handler for webpack runtime
/******/ 	__webpack_require__.oe = function(err) {
/******/ 		process.nextTick(function() {
/******/ 			throw err; // catch this error by using import().catch()
/******/ 		});
/******/ 	};
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 66);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("babel-plugin-universal-import/universalImport");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("react-helmet");

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "@babel/runtime/helpers/slicedToArray"
var slicedToArray_ = __webpack_require__(5);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray_);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(0);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/components/Router.js
var Router = __webpack_require__(6);

// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/components/util/Download.js



var Download_Download = function Download(props) {
  var data = props.data;
  return props.link ? /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: data.extraClasses ? data.extraClasses + " inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100" : "inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100",
    style: {
      backgroundColor: data.bgColor ? data.bgColor : '#444'
    }
  }, /*#__PURE__*/external_react_default.a.createElement("span", null, /*#__PURE__*/external_react_default.a.createElement("img", {
    style: {
      height: '2rem'
    },
    src: data.icon,
    alt: data.alt
  })), /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "ml-2"
  }, /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "block text-xs"
  }, data.smallText), /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "block text-lg font-semibold leading-none"
  }, data.largeText))) : /*#__PURE__*/external_react_default.a.createElement("a", {
    href: props.extLink ? props.extLink : "#!",
    target: props.extLink && '_blank',
    className: data.extraClasses ? data.extraClasses + " inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100" : "inline-flex text-white py-3 px-6 rounded-full hover:scale-105 transition ease transform scale-100",
    style: {
      backgroundColor: data.bgColor ? data.bgColor : '#444'
    }
  }, /*#__PURE__*/external_react_default.a.createElement("span", null, /*#__PURE__*/external_react_default.a.createElement("img", {
    style: {
      height: '2rem'
    },
    src: data.icon,
    alt: data.alt
  })), /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "ml-2"
  }, /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "block text-xs"
  }, data.smallText), /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "block text-lg font-semibold leading-none"
  }, data.largeText)));
};

/* harmony default export */ var util_Download = (Download_Download);
// EXTERNAL MODULE: external "react-modal"
var external_react_modal_ = __webpack_require__(54);
var external_react_modal_default = /*#__PURE__*/__webpack_require__.n(external_react_modal_);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/caretLogo.svg
var caretLogo = __webpack_require__(55);
var caretLogo_default = /*#__PURE__*/__webpack_require__.n(caretLogo);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/apple.svg
var apple = __webpack_require__(56);
var apple_default = /*#__PURE__*/__webpack_require__.n(apple);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/playstore.svg
var playstore = __webpack_require__(57);
var playstore_default = /*#__PURE__*/__webpack_require__.n(playstore);

// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/components/header.js








var appleBtn = {
  bgColor: "#050038",
  icon: apple_default.a,
  smallText: "Download from",
  largeText: "App Store",
  extraClasses: "mx-2.5 mt-5"
};
var playBtn = {
  bgColor: "#050038",
  icon: playstore_default.a,
  smallText: "Download from",
  largeText: "Google Play",
  extraClasses: "mx-2.5 mt-5"
};

var header_header = function header(props) {
  var customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)"
    }
  };

  var _React$useState = external_react_default.a.useState(false),
      _React$useState2 = slicedToArray_default()(_React$useState, 2),
      modalIsOpen = _React$useState2[0],
      setIsOpen = _React$useState2[1];

  var _React$useState3 = external_react_default.a.useState(false),
      _React$useState4 = slicedToArray_default()(_React$useState3, 2),
      modalData = _React$useState4[0],
      setModalData = _React$useState4[1];

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);

    if (window) {
      window.history.pushState({}, document.title, "/");
    }
  }

  Object(external_react_["useEffect"])(function () {
    if (window) {
      var urlParams = new URLSearchParams(window.location.search);
      var reset = urlParams.get("reset");
      var success = urlParams.get("success");

      if (reset == "true") {
        setModalData({
          icon: true,
          msg: "Password Reset Successfully"
        });
        openModal();
      } else if (success == "true") {
        setModalData({
          icon: true,
          msg: "Email Verified Successfully"
        });
        openModal();
      } else if (success == "false") {
        setModalData({
          icon: false,
          msg: "Error! Invalid Data"
        });
        openModal();
      }
    }
  }, []);
  return /*#__PURE__*/external_react_default.a.createElement(external_react_default.a.Fragment, null, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "w-full mx-auto pb-4 border-b border-gray-300 mb-8"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "flex flex-wrap md:flex-nowrap gap-4 items-center justify-center md:justify-between w-full max-w-5xl mx-auto"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    to: "/",
    className: "pt-4"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    src: caretLogo_default.a,
    className: "w-56",
    alt: "Carets"
  })), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: props.isCenter ? "text-center downloadBtns" : "downloadBtns flex items-center justify-center sm:text-left lg:ml-10 w-full lg:w-auto whitespace-nowrap"
  }, /*#__PURE__*/external_react_default.a.createElement(util_Download, {
    extLink: "https://apps.apple.com/us/app/carets/id1571154682",
    data: appleBtn
  }), /*#__PURE__*/external_react_default.a.createElement(util_Download, {
    extLink: "https://play.google.com/store/apps/details?id=com.carets.tv",
    data: playBtn
  })))), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "flex flex-col gap-2 items-center justify-center w-full max-w-5xl mx-auto"
  }, /*#__PURE__*/external_react_default.a.createElement("h1", {
    className: props.isCenter ? "text-3xl mt-8 font-semibold text-center" : "text-3xl mt-8 font-semibold text-center sm:text-left"
  }, "Create, share, and combine video content."), /*#__PURE__*/external_react_default.a.createElement("h2", {
    className: props.isCenter ? "text-2xl text-center md:text-left" : "text-2xl text-center md:text-left"
  }, "What we see with ^Carets is extraordinary.")), /*#__PURE__*/external_react_default.a.createElement(external_react_modal_default.a, {
    isOpen: modalIsOpen,
    style: customStyles,
    onRequestClose: closeModal,
    ariaHideApp: false,
    contentLabel: "Password Reset Successfull"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "f-modal-alert"
  }, modalData.icon ? /*#__PURE__*/external_react_default.a.createElement("div", {
    "class": "f-modal-icon f-modal-success animate"
  }, /*#__PURE__*/external_react_default.a.createElement("span", {
    "class": "f-modal-line f-modal-tip animateSuccessTip"
  }), /*#__PURE__*/external_react_default.a.createElement("span", {
    "class": "f-modal-line f-modal-long animateSuccessLong"
  }), /*#__PURE__*/external_react_default.a.createElement("div", {
    "class": "f-modal-placeholder"
  }), /*#__PURE__*/external_react_default.a.createElement("div", {
    "class": "f-modal-fix"
  })) : /*#__PURE__*/external_react_default.a.createElement("div", {
    "class": "f-modal-icon f-modal-error animate"
  }, /*#__PURE__*/external_react_default.a.createElement("span", {
    "class": "f-modal-x-mark"
  }, /*#__PURE__*/external_react_default.a.createElement("span", {
    "class": "f-modal-line f-modal-left animateXLeft"
  }), /*#__PURE__*/external_react_default.a.createElement("span", {
    "class": "f-modal-line f-modal-right animateXRight"
  })), /*#__PURE__*/external_react_default.a.createElement("div", {
    "class": "f-modal-placeholder"
  }), /*#__PURE__*/external_react_default.a.createElement("div", {
    "class": "f-modal-fix"
  }))), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: modalData.icon ? "reset_msg success" : "reset_msg error"
  }, modalData.msg), /*#__PURE__*/external_react_default.a.createElement("div", {
    style: {
      textAlign: "center",
      marginTop: "2em"
    }
  }, /*#__PURE__*/external_react_default.a.createElement("button", {
    className: "modal_close",
    onClick: closeModal
  }, "OK"))));
};

/* harmony default export */ var components_header = __webpack_exports__["a"] = (header_header);

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/slicedToArray");

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _reach_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);
/* harmony import */ var _reach_router__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reach_router__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _reach_router__WEBPACK_IMPORTED_MODULE_0__["Link"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _reach_router__WEBPACK_IMPORTED_MODULE_0__["Router"]; });



/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {

var _typeof = __webpack_require__(20);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setHasBabelPlugin = exports.ReportChunks = exports.MODULE_IDS = exports.CHUNK_NAMES = undefined;

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _requireUniversalModule = __webpack_require__(72);

Object.defineProperty(exports, 'CHUNK_NAMES', {
  enumerable: true,
  get: function get() {
    return _requireUniversalModule.CHUNK_NAMES;
  }
});
Object.defineProperty(exports, 'MODULE_IDS', {
  enumerable: true,
  get: function get() {
    return _requireUniversalModule.MODULE_IDS;
  }
});

var _reportChunks = __webpack_require__(74);

Object.defineProperty(exports, 'ReportChunks', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_reportChunks)["default"];
  }
});
exports["default"] = universal;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(28);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _hoistNonReactStatics = __webpack_require__(30);

var _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics);

var _requireUniversalModule2 = _interopRequireDefault(_requireUniversalModule);

var _context = __webpack_require__(29);

var _context2 = _interopRequireDefault(_context);

var _utils = __webpack_require__(21);

var _helpers = __webpack_require__(75);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (_typeof(call) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + _typeof(superClass));
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

var hasBabelPlugin = false;

var isHMR = function isHMR() {
  return (// $FlowIgnore
    module.hot && (false)
  );
};

var setHasBabelPlugin = exports.setHasBabelPlugin = function setHasBabelPlugin() {
  hasBabelPlugin = true;
};

function universal(asyncModule) {
  var _class, _temp;

  var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var userRender = opts.render,
      _opts$loading = opts.loading,
      Loading = _opts$loading === undefined ? _utils.DefaultLoading : _opts$loading,
      _opts$error = opts.error,
      Err = _opts$error === undefined ? _utils.DefaultError : _opts$error,
      _opts$minDelay = opts.minDelay,
      minDelay = _opts$minDelay === undefined ? 0 : _opts$minDelay,
      _opts$alwaysDelay = opts.alwaysDelay,
      alwaysDelay = _opts$alwaysDelay === undefined ? false : _opts$alwaysDelay,
      _opts$testBabelPlugin = opts.testBabelPlugin,
      testBabelPlugin = _opts$testBabelPlugin === undefined ? false : _opts$testBabelPlugin,
      _opts$loadingTransiti = opts.loadingTransition,
      loadingTransition = _opts$loadingTransiti === undefined ? true : _opts$loadingTransiti,
      options = _objectWithoutProperties(opts, ['render', 'loading', 'error', 'minDelay', 'alwaysDelay', 'testBabelPlugin', 'loadingTransition']);

  var renderFunc = userRender || (0, _utils.createDefaultRender)(Loading, Err);
  var isDynamic = hasBabelPlugin || testBabelPlugin;
  options.isDynamic = isDynamic;
  options.usesBabelPlugin = hasBabelPlugin;
  options.modCache = {};
  options.promCache = {};
  return _temp = _class = function (_React$Component) {
    _inherits(UniversalComponent, _React$Component);

    _createClass(UniversalComponent, [{
      key: 'requireAsyncInner',
      value: function requireAsyncInner(requireAsync, props, state, isMount) {
        var _this2 = this;

        if (!state.mod && loadingTransition) {
          this.update({
            mod: null,
            props: props
          }); // display `loading` during componentWillReceiveProps
        }

        var time = new Date();
        requireAsync(props).then(function (mod) {
          var state = {
            mod: mod,
            props: props
          };
          var timeLapsed = new Date() - time;

          if (timeLapsed < minDelay) {
            var extraDelay = minDelay - timeLapsed;
            return setTimeout(function () {
              return _this2.update(state, isMount);
            }, extraDelay);
          }

          _this2.update(state, isMount);
        })["catch"](function (error) {
          return _this2.update({
            error: error,
            props: props
          });
        });
      }
    }, {
      key: 'handleBefore',
      value: function handleBefore(isMount, isSync) {
        var isServer = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

        if (this.props.onBefore) {
          var onBefore = this.props.onBefore;
          var info = {
            isMount: isMount,
            isSync: isSync,
            isServer: isServer
          };
          onBefore(info);
        }
      }
    }, {
      key: 'handleAfter',
      value: function handleAfter(state, isMount, isSync, isServer) {
        var mod = state.mod,
            error = state.error;

        if (mod && !error) {
          (0, _hoistNonReactStatics2["default"])(UniversalComponent, mod, {
            preload: true,
            preloadWeak: true
          });

          if (this.props.onAfter) {
            var onAfter = this.props.onAfter;
            var info = {
              isMount: isMount,
              isSync: isSync,
              isServer: isServer
            };
            onAfter(info, mod);
          }
        } else if (error && this.props.onError) {
          this.props.onError(error);
        }

        this.setState(state);
      } // $FlowFixMe

    }, {
      key: 'init',
      value: function init(props) {
        var _req = (0, _requireUniversalModule2["default"])(asyncModule, options, props),
            addModule = _req.addModule,
            requireSync = _req.requireSync,
            requireAsync = _req.requireAsync,
            asyncOnly = _req.asyncOnly;

        var mod = void 0;

        try {
          mod = requireSync(props);
        } catch (error) {
          return (0, _helpers.__update)(props, {
            error: error,
            props: props
          }, this._initialized);
        }

        this._asyncOnly = asyncOnly;
        var chunkName = addModule(props); // record the module for SSR flushing :)

        if (this.context && this.context.report) {
          this.context.report(chunkName);
        }

        if (mod || _utils.isServer) {
          this.handleBefore(true, true, _utils.isServer);
          return (0, _helpers.__update)(props, {
            asyncOnly: asyncOnly,
            props: props,
            mod: mod
          }, this._initialized, true, true, _utils.isServer);
        }

        this.handleBefore(true, false);
        this.requireAsyncInner(requireAsync, props, {
          props: props,
          asyncOnly: asyncOnly,
          mod: mod
        }, true);
        return {
          mod: mod,
          asyncOnly: asyncOnly,
          props: props
        };
      }
    }], [{
      key: 'preload',
      value: function preload(props) {
        props = props || {};

        var _req2 = (0, _requireUniversalModule2["default"])(asyncModule, options, props),
            requireAsync = _req2.requireAsync,
            requireSync = _req2.requireSync;

        var mod = void 0;

        try {
          mod = requireSync(props);
        } catch (error) {
          return Promise.reject(error);
        }

        return Promise.resolve().then(function () {
          if (mod) return mod;
          return requireAsync(props);
        }).then(function (mod) {
          (0, _hoistNonReactStatics2["default"])(UniversalComponent, mod, {
            preload: true,
            preloadWeak: true
          });
          return mod;
        });
      }
      /* eslint-enable react/sort-comp */

      /* eslint-disable react/sort-comp */

    }, {
      key: 'preloadWeak',
      value: function preloadWeak(props) {
        props = props || {};

        var _req3 = (0, _requireUniversalModule2["default"])(asyncModule, options, props),
            requireSync = _req3.requireSync;

        var mod = requireSync(props);

        if (mod) {
          (0, _hoistNonReactStatics2["default"])(UniversalComponent, mod, {
            preload: true,
            preloadWeak: true
          });
        }

        return mod;
      }
    }]);

    function UniversalComponent(props, context) {
      _classCallCheck(this, UniversalComponent);

      var _this = _possibleConstructorReturn(this, (UniversalComponent.__proto__ || Object.getPrototypeOf(UniversalComponent)).call(this, props, context));

      _this.update = function (state) {
        var isMount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var isSync = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        var isServer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
        if (!_this._initialized) return;
        if (!state.error) state.error = null;

        _this.handleAfter(state, isMount, isSync, isServer);
      };

      _this.state = _this.init(_this.props); // $FlowFixMe

      _this.state.error = null;
      return _this;
    }

    _createClass(UniversalComponent, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this._initialized = true;
      }
    }, {
      key: 'componentDidUpdate',
      value: function componentDidUpdate(prevProps) {
        var _this3 = this;

        if (isDynamic || this._asyncOnly) {
          var _req4 = (0, _requireUniversalModule2["default"])(asyncModule, options, this.props, prevProps),
              requireSync = _req4.requireSync,
              requireAsync = _req4.requireAsync,
              shouldUpdate = _req4.shouldUpdate;

          if (shouldUpdate(this.props, prevProps)) {
            var mod = void 0;

            try {
              mod = requireSync(this.props);
            } catch (error) {
              return this.update({
                error: error
              });
            }

            this.handleBefore(false, !!mod);

            if (!mod) {
              return this.requireAsyncInner(requireAsync, this.props, {
                mod: mod
              });
            }

            var state = {
              mod: mod
            };

            if (alwaysDelay) {
              if (loadingTransition) this.update({
                mod: null
              }); // display `loading` during componentWillReceiveProps

              setTimeout(function () {
                return _this3.update(state, false, true);
              }, minDelay);
              return;
            }

            this.update(state, false, true);
          }
        }
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        this._initialized = false;
      }
    }, {
      key: 'render',
      value: function render() {
        var _props = this.props,
            isLoading = _props.isLoading,
            userError = _props.error,
            props = _objectWithoutProperties(_props, ['isLoading', 'error']);

        var _state = this.state,
            mod = _state.mod,
            error = _state.error;
        return renderFunc(props, mod, isLoading, userError || error);
      }
    }], [{
      key: 'getDerivedStateFromProps',
      value: function getDerivedStateFromProps(nextProps, currentState) {
        var _req5 = (0, _requireUniversalModule2["default"])(asyncModule, options, nextProps, currentState.props),
            requireSync = _req5.requireSync,
            shouldUpdate = _req5.shouldUpdate;

        if (isHMR() && shouldUpdate(currentState.props, nextProps)) {
          var mod = requireSync(nextProps);
          return _extends({}, currentState, {
            mod: mod
          });
        }

        return null;
      }
    }]);

    return UniversalComponent;
  }(_react2["default"].Component), _class.contextType = _context2["default"], _temp;
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(71)(module)))

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("react-custom-scrollbars");

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _utils = __webpack_require__(21);

var requireById = function requireById(id) {
  if (!(0, _utils.isWebpack)() && typeof id === 'string') {
    return __webpack_require__(73)("" + id);
  }

  return __webpack_require__('' + id);
};

exports["default"] = requireById;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("react-static");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGZpbGw9IiNmZmZmZmYiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgMzg0IDM4NCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzg0IDM4NDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0wLDIxLjMzM2wxOTIsMzQxLjMzM0wzODQsMjEuMzMzSDB6IE03Miw2NGgyNDBMMTkyLDI3Ny4zMzNMNzIsNjR6Ii8+DQoJPC9nPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("@reach/router");

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader

module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return '@media ' + item[2] + '{' + content + '}';
      } else {
        return content;
      }
    }).join('');
  }; // import a list of modules into the list


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (i = 0; i < modules.length; i++) {
      var item = modules[i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || '';
  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
  return '/*# ' + data + ' */';
}

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/video.a9fffb23.mp4";

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return basePath; });
var basePath =  false ? undefined : "https://api.carets.tv/api/v1/";

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/ajax-loader.5fe0e55f.gif";

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_imgs_triangle_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(11);
/* harmony import */ var _assets_imgs_triangle_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_triangle_svg__WEBPACK_IMPORTED_MODULE_2__);



/* harmony default export */ __webpack_exports__["a"] = (function (props) {
  var videoEl = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      isPlaying = _useState2[0],
      setPlaying = _useState2[1];

  var playVideo = function playVideo() {
    videoEl.current.play();
    videoEl.current.addEventListener("ended", myHandler, false);
    setPlaying(true);
  };

  var myHandler = function myHandler() {
    setPlaying(false);
  };

  var data = props.data;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-3xl font-semibold"
  }, data.username), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, data.tags && data.tags.map(function (elem, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "inline-flex mx-3 mt-1",
      key: index
    }, elem);
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "inline-flex items-center mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-lg font-bold"
  }, "\u266C"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-1"
  }, data.music)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative inline-block w-full max-w-5xl mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("video", {
    playsInline: true,
    className: "w-full",
    ref: videoEl // src={data.videoUrl}
    ,
    src: "https://caretsffmpeg.s3.amazonaws.com/promosvideos/caretsintrodesktop.mp4"
  }), !isPlaying && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "playBtn",
    onClick: playVideo
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: _assets_imgs_triangle_svg__WEBPACK_IMPORTED_MODULE_2___default.a,
    alt: "Play"
  })))));
});

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(0);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "react-static"
var external_react_static_ = __webpack_require__(10);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/components/Router.js
var Router = __webpack_require__(6);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/facebook.svg
var facebook = __webpack_require__(23);
var facebook_default = /*#__PURE__*/__webpack_require__.n(facebook);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/twitter.svg
var twitter = __webpack_require__(24);
var twitter_default = /*#__PURE__*/__webpack_require__.n(twitter);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/instagram.svg
var instagram = __webpack_require__(25);
var instagram_default = /*#__PURE__*/__webpack_require__.n(instagram);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/youtube.svg
var youtube = __webpack_require__(26);
var youtube_default = /*#__PURE__*/__webpack_require__.n(youtube);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/footer_logo.svg
var footer_logo = __webpack_require__(53);
var footer_logo_default = /*#__PURE__*/__webpack_require__.n(footer_logo);

// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/components/footer.js








var footer_footer = function footer() {
  return /*#__PURE__*/external_react_default.a.createElement("footer", null, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "bg-gray-200 w-full mt-16",
    style: {
      height: "1px"
    }
  }), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "customContainer py-7"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 md:text-left text-center"
  }, /*#__PURE__*/external_react_default.a.createElement("div", null, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "text-xl font-semibold themeDarkBlue mt-5"
  }, "^Carets"), /*#__PURE__*/external_react_default.a.createElement("ul", {
    className: "list-none p-0 mt-5"
  }, /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/introduction"
  }, "Introduction")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/community-guidelines"
  }, "Community Guidelines")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/legal"
  }, "Legal")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/help"
  }, "Help")))), /*#__PURE__*/external_react_default.a.createElement("div", null, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "text-xl font-semibold themeDarkBlue mt-5"
  }, "Company"), /*#__PURE__*/external_react_default.a.createElement("ul", {
    className: "list-none p-0 mt-5"
  }, /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/about"
  }, "About Carets")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/terms"
  }, "Advertising T&C")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/clmTerms"
  }, "CLM T&C")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement("a", {
    className: "hover:text-blue-800 hover:underline",
    href: "https://admin.carets.tv/",
    target: "_blank"
  }, "Advertise with Us")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    className: "hover:text-blue-800 hover:underline",
    to: "/clmTutorials"
  }, "CLM Tutorials")), /*#__PURE__*/external_react_default.a.createElement("li", {
    className: "mt-3 text-lg 2xl:text-base"
  }, /*#__PURE__*/external_react_default.a.createElement("a", {
    className: "hover:text-blue-800 hover:underline",
    href: "/deleteInstructions",
    target: "_blank"
  }, "Delete Account")))), /*#__PURE__*/external_react_default.a.createElement("div", null, "\xA0"), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "justify-self-center"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "text-xl font-semibold themeDarkBlue mt-5"
  }, "Follow us"), /*#__PURE__*/external_react_default.a.createElement("ul", {
    className: "socialIcons list-none p-0 mt-5 inline-grid grid-cols-4 gap-4 grid-flow-col-dense"
  }, /*#__PURE__*/external_react_default.a.createElement("li", null, /*#__PURE__*/external_react_default.a.createElement("a", {
    target: "_blank",
    rel: "noreferrer",
    href: "https://www.facebook.com/profile.php?id=100069312018877"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6 absolute hidden",
    src: facebook_default.a,
    alt: "Facebook"
  }), /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6",
    src: facebook_default.a,
    alt: "Facebook"
  }))), /*#__PURE__*/external_react_default.a.createElement("li", null, /*#__PURE__*/external_react_default.a.createElement("a", {
    target: "_blank",
    rel: "noreferrer",
    href: "https://twitter.com/CaretsApp"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6 absolute hidden",
    src: twitter_default.a,
    alt: "Twitter"
  }), /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6",
    src: twitter_default.a,
    alt: "Twitter"
  }))), /*#__PURE__*/external_react_default.a.createElement("li", null, /*#__PURE__*/external_react_default.a.createElement("a", {
    target: "_blank",
    rel: "noreferrer",
    href: "https://www.instagram.com/caretsapp/"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6 absolute hidden",
    src: instagram_default.a,
    alt: "Instagram"
  }), /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6",
    src: instagram_default.a,
    alt: "Instagram"
  }))), /*#__PURE__*/external_react_default.a.createElement("li", null, /*#__PURE__*/external_react_default.a.createElement("a", {
    target: "_blank",
    rel: "noreferrer",
    href: "https://www.youtube.com/channel/UCBb6TaWtn3BScytshsQksmw"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6 absolute hidden",
    src: youtube_default.a,
    alt: "Youtube"
  }), /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "h-6",
    src: youtube_default.a,
    alt: "Youtube"
  }))))))), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "bg-gray-200 w-full mt-16",
    style: {
      height: "1px"
    }
  }), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "customContainer py-5"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "text-sm"
  }, "Copyright \xA9 ", new Date().getFullYear(), ", Carets Corporation, All Rights Reserved."), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "ml-auto"
  }, /*#__PURE__*/external_react_default.a.createElement(Router["a" /* Link */], {
    to: "/"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    src: footer_logo_default.a,
    alt: "Carets Logo",
    className: "h-10"
  }))))));
};

/* harmony default export */ var components_footer = (footer_footer);
// EXTERNAL MODULE: external "@babel/runtime/helpers/slicedToArray"
var slicedToArray_ = __webpack_require__(5);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray_);

// EXTERNAL MODULE: external "react-helmet"
var external_react_helmet_ = __webpack_require__(3);

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(14);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/components/header.js + 1 modules
var header = __webpack_require__(4);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/triangle.svg
var triangle = __webpack_require__(11);
var triangle_default = /*#__PURE__*/__webpack_require__.n(triangle);

// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/components/userData.js



/* harmony default export */ var userData = (function (props) {
  var videoEl = Object(external_react_["useRef"])(null);

  var _useState = Object(external_react_["useState"])(false),
      _useState2 = slicedToArray_default()(_useState, 2),
      isPlaying = _useState2[0],
      setPlaying = _useState2[1];

  var playVideo = function playVideo() {
    videoEl.current.play();
    videoEl.current.addEventListener("ended", myHandler, false);
    setPlaying(true);
  };

  var myHandler = function myHandler() {
    setPlaying(false);
  };

  var data = props.data.data;
  return /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "text-center",
    clg: console.log(data)
  }, data ? /*#__PURE__*/external_react_default.a.createElement(external_react_default.a.Fragment, null, data.users && data.users.map(function (usr, idx) {
    return /*#__PURE__*/external_react_default.a.createElement("div", {
      className: "text-3xl font-semibold",
      key: idx
    }, "@", usr.username);
  }), /*#__PURE__*/external_react_default.a.createElement("div", null, data.hashtags && data.hashtags.map(function (elem, index) {
    return /*#__PURE__*/external_react_default.a.createElement("span", {
      className: "inline-flex mx-3 mt-1",
      key: index
    }, elem ? elem : "Loading...");
  })), data.music && /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "inline-flex items-center mt-2"
  }, /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "text-lg font-bold"
  }, "\u266C"), /*#__PURE__*/external_react_default.a.createElement("span", {
    className: "ml-1"
  }, data.music ? data.music : /*#__PURE__*/external_react_default.a.createElement("div", null, "Loading..."))), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "mt-5"
  }, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "relative inline-block"
  }, data.video_url && false ? /*#__PURE__*/external_react_default.a.createElement(external_react_default.a.Fragment, null, /*#__PURE__*/external_react_default.a.createElement("video", {
    playsInline: true,
    className: "max-w-screen-lg w-full inline-flex",
    ref: videoEl,
    src: data.video_url,
    poster: data.image_url ? data.image_url : null
  }), !isPlaying && /*#__PURE__*/external_react_default.a.createElement("button", {
    className: "playBtn",
    onClick: playVideo
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    src: triangle_default.a,
    alt: "Play"
  }))) : /*#__PURE__*/external_react_default.a.createElement(external_react_default.a.Fragment, null, /*#__PURE__*/external_react_default.a.createElement("video", {
    playsInline: true,
    className: "max-w-screen-lg w-full inline-flex",
    ref: videoEl,
    src: props.dummy,
    poster: data.image_url ? data.image_url : null
  }), !isPlaying && /*#__PURE__*/external_react_default.a.createElement("button", {
    className: "playBtn",
    onClick: playVideo
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    src: triangle_default.a,
    alt: "Play"
  })))))) : /*#__PURE__*/external_react_default.a.createElement("div", null, "No video found"));
});
// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/video.mp4
var video = __webpack_require__(15);
var video_default = /*#__PURE__*/__webpack_require__.n(video);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/helpers/basePath.js
var basePath = __webpack_require__(16);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/imgs/ajax-loader.gif
var ajax_loader = __webpack_require__(17);
var ajax_loader_default = /*#__PURE__*/__webpack_require__.n(ajax_loader);

// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/containers/Video.js









/* harmony default export */ var Video = (function (props) {
  var _useState = Object(external_react_["useState"])(null),
      _useState2 = slicedToArray_default()(_useState, 2),
      videoData = _useState2[0],
      setData = _useState2[1];

  var _useState3 = Object(external_react_["useState"])(ajax_loader_default.a),
      _useState4 = slicedToArray_default()(_useState3, 2),
      dataLoader = _useState4[0],
      setLoader = _useState4[1];

  Object(external_react_["useEffect"])(function () {
    var queryParam = new URLSearchParams(props.location.search).get('id');
    queryParam && external_axios_default.a.get("".concat(basePath["a" /* basePath */], "videos/showWeb/").concat(queryParam), {
      headers: {
        'Content-Type': null,
        'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI3IiwianRpIjoiY2IyYmE1NThhZTAyNDgwZjc5OTJiZDQ5NGQ4ZjFhMWI3ZjI2ODkwNDQ5ZWMxNWY4NmJhNTRiMDY5MjVjNjJiNTcxMTQ4OTkwNTZjMWRhOTUiLCJpYXQiOjE2MDQ5MTc0NTMsIm5iZiI6MTYwNDkxNzQ1MywiZXhwIjoxNjM2NDUzNDUzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.heVKvWhwzR797mMh4qxy34kP1njHXdaOo-5mKqJvdS4INR1vn_y3-xtZeZYPwqNGBtKPVZiGePmIiqftbC7ch4h9aazQCPlNhlDDoVFzIqwvxvI9Qa-HXdsTfLUj18onMj2ZFemyOM6vXxpJsUKOSSduRSWDbwlkHBZUU1__5CwZ9-i-Er8oRU_rYEo9sVgcP7naAxE57Cf9zjp-R2C5CR6yHECIMsVjvSYNnTPvs_OKhhn-fpHKndxJrKchsVgN6aE75yrs3-ySYYCg3CGzrOn52gMs9zO8h3fsCCJhqLmlFXwN09n5ODGtBHnrHPuAeiK7pWSSI1jNB6IrZT7P9tKdLupoosfmclFQ9PHEkxkCRKoGimCWZDRloKs_qf-R6AZdjEOXvSVebJHmjsW02dksJW7kOjKiwl8dIN1-cPB2feurv0WYx8l6zP02mxgy_OCxQbEGhhYo_IzN-h9jzy_szsevdzt1baD-MjV5JRqO7RxSQ5neYxTJKX2SaRLmsVGEITjU4w01TXHgLHdwryO9egLRCMfq-wYdqNo34oUfUFtAlraugPQ41sGrFw4ri0VxUQR-P-QintUM8SaicT9s3w98noBkHraUqX2JF2FxXgexQzwvGa34CgQ8V31lfqaV-1x0_FTenrXIxv-o4sho82VRRJqp23gllaePd8A'
      }
    }).then(function (response) {
      // handle success
      setData(response.data);
      setLoader(false);
    })["catch"](function (error) {
      // handle error
      console.log(error);
      setLoader(false);
    }).then(function () {
      // always executed
      setLoader(false);
    });
  }, []);
  return /*#__PURE__*/external_react_default.a.createElement(external_react_default.a.Fragment, null, /*#__PURE__*/external_react_default.a.createElement(external_react_helmet_["Helmet"], null, /*#__PURE__*/external_react_default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/external_react_default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/external_react_default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/external_react_default.a.createElement("div", null, /*#__PURE__*/external_react_default.a.createElement(header["a" /* default */], {
    isCenter: true
  })), /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "mt-14"
  }, dataLoader ? /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "text-center"
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    className: "inline-block",
    src: dataLoader,
    alt: "Loading..."
  })) : videoData && /*#__PURE__*/external_react_default.a.createElement(userData, {
    dataloader: dataLoader,
    dummy: video_default.a,
    data: videoData
  }))));
});
// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/components/util/loader.js



var loader_loader = function loader() {
  return /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "flex justify-center items-center",
    style: {
      height: '100vh',
      width: '100vw'
    }
  }, /*#__PURE__*/external_react_default.a.createElement("img", {
    src: ajax_loader_default.a,
    alt: "Loading..."
  }));
};

/* harmony default export */ var util_loader = (loader_loader);
// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/tw.css
var tw = __webpack_require__(83);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/assets/fonts/fonts.css
var fonts = __webpack_require__(84);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/app.css
var app = __webpack_require__(105);

// EXTERNAL MODULE: D:/laragon/www/carets_web/carets_web/src/styleCustom.css
var styleCustom = __webpack_require__(106);

// CONCATENATED MODULE: D:/laragon/www/carets_web/carets_web/src/App.js


 //







 // Any routes that start with 'video' will be treated as non-static routes

Object(external_react_static_["addPrefetchExcludes"])(["video"]);

function App() {
  return /*#__PURE__*/external_react_default.a.createElement(external_react_static_["Root"], null, /*#__PURE__*/external_react_default.a.createElement("div", {
    className: "content"
  }, /*#__PURE__*/external_react_default.a.createElement(external_react_default.a.Suspense, {
    fallback: /*#__PURE__*/external_react_default.a.createElement(util_loader, null)
  }, /*#__PURE__*/external_react_default.a.createElement("header", {
    className: "topBar"
  }), /*#__PURE__*/external_react_default.a.createElement(Router["b" /* Router */], null, /*#__PURE__*/external_react_default.a.createElement(Video, {
    path: "video"
  }), /*#__PURE__*/external_react_default.a.createElement(external_react_static_["Routes"], {
    path: "*"
  })), /*#__PURE__*/external_react_default.a.createElement(components_footer, null))));
}

/* harmony default export */ var src_App = __webpack_exports__["a"] = (App);

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/typeof");

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof2 = __webpack_require__(20);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cacheProm = exports.loadFromPromiseCache = exports.cacheExport = exports.loadFromCache = exports.callForString = exports.createDefaultRender = exports.createElement = exports.findExport = exports.resolveExport = exports.tryRequire = exports.DefaultError = exports.DefaultLoading = exports.babelInterop = exports.isWebpack = exports.isServer = exports.isTest = undefined;

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
  return _typeof2(obj);
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
};

var _react = __webpack_require__(0);

var React = _interopRequireWildcard(_react);

var _requireById = __webpack_require__(9);

var _requireById2 = _interopRequireDefault(_requireById);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj["default"] = obj;
    return newObj;
  }
}

var isTest = exports.isTest = "production" === 'test';
var isServer = exports.isServer = !(typeof window !== 'undefined' && window.document && window.document.createElement);

var isWebpack = exports.isWebpack = function isWebpack() {
  return typeof __webpack_require__ !== 'undefined';
};

var babelInterop = exports.babelInterop = function babelInterop(mod) {
  return mod && (typeof mod === 'undefined' ? 'undefined' : _typeof(mod)) === 'object' && mod.__esModule ? mod["default"] : mod;
};

var DefaultLoading = exports.DefaultLoading = function DefaultLoading() {
  return React.createElement('div', null, 'Loading...');
};

var DefaultError = exports.DefaultError = function DefaultError(_ref) {
  var error = _ref.error;
  return React.createElement('div', null, 'Error: ', error && error.message);
};

var tryRequire = exports.tryRequire = function tryRequire(id) {
  try {
    return (0, _requireById2["default"])(id);
  } catch (err) {
    // warn if there was an error while requiring the chunk during development
    // this can sometimes lead the server to render the loading component.
    if (false) {}
  }

  return null;
};

var resolveExport = exports.resolveExport = function resolveExport(mod, key, onLoad, chunkName, props, modCache) {
  var isSync = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : false;
  var exp = findExport(mod, key);

  if (onLoad && mod) {
    var _isServer = typeof window === 'undefined';

    var info = {
      isServer: _isServer,
      isSync: isSync
    };
    onLoad(mod, info, props);
  }

  if (chunkName && exp) cacheExport(exp, chunkName, props, modCache);
  return exp;
};

var findExport = exports.findExport = function findExport(mod, key) {
  if (typeof key === 'function') {
    return key(mod);
  } else if (key === null) {
    return mod;
  }

  return mod && (typeof mod === 'undefined' ? 'undefined' : _typeof(mod)) === 'object' && key ? mod[key] : babelInterop(mod);
};

var createElement = exports.createElement = function createElement(Component, props) {
  return React.isValidElement(Component) ? React.cloneElement(Component, props) : React.createElement(Component, props);
};

var createDefaultRender = exports.createDefaultRender = function createDefaultRender(Loading, Err) {
  return function (props, mod, isLoading, error) {
    if (isLoading) {
      return createElement(Loading, props);
    } else if (error) {
      return createElement(Err, _extends({}, props, {
        error: error
      }));
    } else if (mod) {
      // primary usage (for async import loading + errors):
      return createElement(mod, props);
    }

    return createElement(Loading, props);
  };
};

var callForString = exports.callForString = function callForString(strFun, props) {
  return typeof strFun === 'function' ? strFun(props) : strFun;
};

var loadFromCache = exports.loadFromCache = function loadFromCache(chunkName, props, modCache) {
  return !isServer && modCache[callForString(chunkName, props)];
};

var cacheExport = exports.cacheExport = function cacheExport(exp, chunkName, props, modCache) {
  return modCache[callForString(chunkName, props)] = exp;
};

var loadFromPromiseCache = exports.loadFromPromiseCache = function loadFromPromiseCache(chunkName, props, promisecache) {
  return promisecache[callForString(chunkName, props)];
};

var cacheProm = exports.cacheProm = function cacheProm(pr, chunkName, props, promisecache) {
  return promisecache[callForString(chunkName, props)] = pr;
};

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSItMTEwIDEgNTExIDUxMS45OTk5NiIgZmlsbD0iIzA1MDAzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJtMTgwIDUxMmgtODEuOTkyMTg4Yy0xMy42OTUzMTIgMC0yNC44MzU5MzctMTEuMTQwNjI1LTI0LjgzNTkzNy0yNC44MzU5Mzh2LTE4NC45Mzc1aC00Ny44MzU5MzdjLTEzLjY5NTMxMyAwLTI0LjgzNTkzOC0xMS4xNDQ1MzEtMjQuODM1OTM4LTI0LjgzNTkzN3YtNzkuMjQ2MDk0YzAtMTMuNjk1MzEyIDExLjE0MDYyNS0yNC44MzU5MzcgMjQuODM1OTM4LTI0LjgzNTkzN2g0Ny44MzU5Mzd2LTM5LjY4MzU5NGMwLTM5LjM0NzY1NiAxMi4zNTU0NjktNzIuODI0MjE5IDM1LjcyNjU2My05Ni44MDQ2ODggMjMuNDc2NTYyLTI0LjA4OTg0MyA1Ni4yODUxNTYtMzYuODIwMzEyIDk0Ljg3ODkwNi0zNi44MjAzMTJsNjIuNTMxMjUuMTAxNTYyYzEzLjY3MTg3NS4wMjM0MzggMjQuNzkyOTY4IDExLjE2NDA2MyAyNC43OTI5NjggMjQuODM1OTM4djczLjU3ODEyNWMwIDEzLjY5NTMxMy0xMS4xMzY3MTggMjQuODM1OTM3LTI0LjgyODEyNCAyNC44MzU5MzdsLTQyLjEwMTU2My4wMTU2MjZjLTEyLjgzOTg0NCAwLTE2LjEwOTM3NSAyLjU3NDIxOC0xNi44MDg1OTQgMy4zNjMyODEtMS4xNTIzNDMgMS4zMDg1OTMtMi41MjM0MzcgNS4wMDc4MTItMi41MjM0MzcgMTUuMjIyNjU2djMxLjM1MTU2M2g1OC4yNjk1MzFjNC4zODY3MTkgMCA4LjYzNjcxOSAxLjA4MjAzMSAxMi4yODkwNjMgMy4xMjEwOTMgNy44Nzg5MDYgNC40MDIzNDQgMTIuNzc3MzQzIDEyLjcyNjU2MyAxMi43NzczNDMgMjEuNzIyNjU3bC0uMDMxMjUgNzkuMjQ2MDkzYzAgMTMuNjg3NS0xMS4xNDA2MjUgMjQuODI4MTI1LTI0LjgzNTkzNyAyNC44MjgxMjVoLTU4LjQ2ODc1djE4NC45NDE0MDZjMCAxMy42OTUzMTMtMTEuMTQ0NTMyIDI0LjgzNTkzOC0yNC44Mzk4NDQgMjQuODM1OTM4em0tNzYuODEyNS0zMC4wMTU2MjVoNzEuNjMyODEydi0xOTMuMTk1MzEzYzAtOS4xNDQ1MzEgNy40NDE0MDctMTYuNTgyMDMxIDE2LjU4MjAzMi0xNi41ODIwMzFoNjYuNzI2NTYybC4wMjczNDQtNjguODgyODEyaC02Ni43NTc4MTJjLTkuMTQwNjI2IDAtMTYuNTc4MTI2LTcuNDM3NS0xNi41NzgxMjYtMTYuNTgyMDMxdi00NC43ODkwNjNjMC0xMS43MjY1NjMgMS4xOTE0MDctMjUuMDYyNSAxMC4wNDI5NjktMzUuMDg1OTM3IDEwLjY5NTMxMy0xMi4xMTcxODggMjcuNTUwNzgxLTEzLjUxNTYyNiAzOS4zMDA3ODEtMTMuNTE1NjI2bDM2LjkyMTg3Ni0uMDE1NjI0di02My4yMjY1NjNsLTU3LjMzMjAzMi0uMDkzNzVjLTYyLjAyMzQzNyAwLTEwMC41NjY0MDYgMzkuNzAzMTI1LTEwMC41NjY0MDYgMTAzLjYwOTM3NXY1My4xMTcxODhjMCA5LjE0MDYyNC03LjQzNzUgMTYuNTgyMDMxLTE2LjU3ODEyNSAxNi41ODIwMzFoLTU2LjA5Mzc1djY4Ljg4MjgxMmg1Ni4wOTM3NWM5LjE0MDYyNSAwIDE2LjU3ODEyNSA3LjQzNzUgMTYuNTc4MTI1IDE2LjU4MjAzMXptMTYzLjA2MjUtNDUxLjg2NzE4N2guMDAzOTA2em0wIDAiLz48L3N2Zz4="

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PHN2ZyBpZD0icmVndWxhciIgZmlsbD0iIzA1MDAzOCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMjQgMjQiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgMjQgMjQiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibS40NzMgMTkuNTk1YzIuMjIyIDEuNDEgNC44MDggMi4xNTUgNy40NzggMi4xNTUgMy45MSAwIDcuNDkzLTEuNTAyIDEwLjA5LTQuMjI5IDIuNDg1LTIuNjEgMy44NTItNi4xMTcgMy43ODQtOS42NzYuOTQyLS44MDYgMi4wNS0yLjM0NSAyLjA1LTMuODQ1IDAtLjU3NS0uNjI0LS45NC0xLjEzLS42NDctLjg4NS41Mi0xLjY5Mi42NTYtMi41MjIuNDIzLTEuNjk1LTEuNjUyLTQuMjE4LTItNi4zNDQtLjg1NC0xLjg1OCAxLTIuODkxIDIuODMtMi43OTggNC44My0zLjEzOS0uMzgzLTYuMDM5LTEuOTU3LTguMDYxLTQuNDAzLS4zMzItLjM5OS0uOTYyLS4zNTItMS4yMjYuMS0uOTc0IDEuNjY4LS45NjQgMy42MDEtLjExNyA1LjE2Mi0uNDAzLjA3MS0uNjUyLjQxLS42NTIuNzc3IDAgMS41NjkuNzA2IDMuMDExIDEuODQzIDMuOTk1LS4yMTIuMjA0LS4yODIuNTA3LS4xOTIuNzc3LjUgMS41MDIgMS42MzIgMi42NzYgMy4wNDcgMy4yNjQtMS41MzkuNzM1LTMuMjQxLjk4LTQuNzU2Ljc5NC0uNzg0LS4xMDYtMS4xNzEuOTQ4LS40OTQgMS4zNzd6bTcuNjgzLTEuOTE0Yy41NjEtLjQzMS4yNjMtMS4zMjktLjQ0MS0xLjM0NC0xLjI0LS4wMjYtMi4zNjktLjYzNy0zLjA3Mi0xLjU5OC4zMzktLjAyMi42OS0uMDc0IDEuMDI0LS4xNjQuNzYxLS4yMDYuNzI1LTEuMzA0LS4wNDgtMS40NTktMS40MDMtLjI4Mi0yLjUwNC0xLjMwNC0yLjkxNy0yLjYyLjM3Ny4wOTMuNzYxLjE0NSAxLjE0NC4xNTIuNzU5LjAwNCAxLjA0Ni0uOTY5LjQyNy0xLjM3Ni0xLjM5NS0uOTE5LTEuOTktMi41NDItMS41OTYtNC4wNjggMi40MzYgMi40NjggNS43NDEgMy45NTUgOS4yMzcgNC4xMjMuNTAxLjAzMS44NzctLjQ0Ljc2Ny0uOTE3LS40NzUtMi4wNTkuNjc1LTMuNTAyIDEuOTEtNC4xNjcgMS4yMjItLjY2IDMuMTg0LS44NjYgNC42ODguNzEyLjQ0Ny40NzEgMS45NTUuNDg5IDIuNzIyLjMxLS4zNDQuNjQ4LS44NzMgMS4yNjMtMS4zNjggMS42MDktLjIxMS4xNDgtLjMzMi4zOTQtLjMxOS42NTEuMTYxIDMuMjg1LTEuMDYzIDYuNTUxLTMuMzU4IDguOTYtMi4zMTIgMi40MjctNS41MDkgMy43NjQtOS4wMDQgMy43NjQtMS4zOSAwLTIuNzUzLS4yMjYtNC4wNDEtLjY2MiAxLjU0LS4yOTggMy4wMDMtLjk1IDQuMjQ1LTEuOTA2eiIvPjwvc3ZnPg=="

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUxMnB0IiBmaWxsPSIjMDUwMDM4IiB2aWV3Qm94PSIwIDAgNTEyIDUxMi4wMDAwNiIgd2lkdGg9IjUxMnB0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Im0yNjEuMDM5MDYyIDUxMmMtMS42OTUzMTIgMC0zLjM5MDYyNCAwLTUuMDk3NjU2LS4wMDc4MTItNDAuMTMyODEyLjA5NzY1Ni03Ny4yMTQ4NDQtLjkyMTg3Ni0xMTMuMjc3MzQ0LTMuMTE3MTg4LTMzLjA2MjUtMi4wMTE3MTktNjMuMjQyMTg3LTEzLjQzNzUtODcuMjgxMjUtMzMuMDM5MDYyLTIzLjE5NTMxMi0xOC45MTQwNjMtMzkuMDM1MTU2LTQ0LjQ4ODI4Mi00Ny4wNzgxMjQtNzYuMDAzOTA3LTctMjcuNDM3NS03LjM3MTA5NC01NC4zNzEwOTMtNy43MjY1NjMtODAuNDIxODc1LS4yNTc4MTMtMTguNjkxNDA2LS41MjM0Mzc1LTQwLjgzOTg0NC0uNTc4MTI1LTYzLjM2MzI4MS4wNTQ2ODc1LTIyLjYxNzE4Ny4zMjAzMTItNDQuNzY1NjI1LjU3ODEyNS02My40NTcwMzEuMzU1NDY5LTI2LjA0Njg3NS43MjY1NjMtNTIuOTgwNDY5IDcuNzI2NTYzLTgwLjQyMTg3NSA4LjA0Mjk2OC0zMS41MTU2MjUgMjMuODgyODEyLTU3LjA4OTg0NCA0Ny4wNzgxMjQtNzYuMDAzOTA3IDI0LjAzOTA2My0xOS42MDE1NjIgNTQuMjE4NzUtMzEuMDI3MzQzIDg3LjI4NTE1Ny0zMy4wMzkwNjIgMzYuMDYyNS0yLjE5MTQwNiA3My4xNTIzNDMtMy4yMTQ4NDM4IDExMy4zNzEwOTMtMy4xMTcxODc1IDQwLjE0NDUzMi0uMDg1OTM3NSA3Ny4yMTQ4NDQuOTI1NzgxNSAxMTMuMjc3MzQ0IDMuMTE3MTg3NSAzMy4wNjI1IDIuMDExNzE5IDYzLjI0MjE4OCAxMy40Mzc1IDg3LjI4MTI1IDMzLjAzOTA2MiAyMy4xOTkyMTkgMTguOTE0MDYzIDM5LjAzNTE1NiA0NC40ODgyODIgNDcuMDc4MTI1IDc2LjAwMzkwNyA3IDI3LjQzNzUgNy4zNzEwOTQgNTQuMzc1IDcuNzI2NTYzIDgwLjQyMTg3NS4yNTc4MTIgMTguNjkxNDA2LjUyNzM0NCA0MC44Mzk4NDQuNTc4MTI1IDYzLjM2MzI4MXYuMDkzNzVjLS4wNTA3ODEgMjIuNTIzNDM3LS4zMjAzMTMgNDQuNjcxODc1LS41NzgxMjUgNjMuMzYzMjgxLS4zNTU0NjkgMjYuMDQ2ODc1LS43MjI2NTYgNTIuOTgwNDY5LTcuNzI2NTYzIDgwLjQyMTg3NS04LjA0Mjk2OSAzMS41MTU2MjUtMjMuODc4OTA2IDU3LjA4OTg0NC00Ny4wNzgxMjUgNzYuMDAzOTA3LTI0LjAzOTA2MiAxOS42MDE1NjItNTQuMjE4NzUgMzEuMDI3MzQzLTg3LjI4MTI1IDMzLjAzOTA2Mi0zNC41MzUxNTYgMi4xMDE1NjItNzAuMDExNzE4IDMuMTI1LTEwOC4yNzczNDQgMy4xMjV6bS01LjA5NzY1Ni00MC4wMDc4MTJjMzkuNDgwNDY5LjA5Mzc1IDc1LjczMDQ2OS0uOTAyMzQ0IDExMC45NDUzMTMtMy4wNDI5NjkgMjUtMS41MTk1MzEgNDYuNjc1NzgxLTkuNjMyODEzIDY0LjQzMzU5My0yNC4xMTMyODEgMTYuNDE0MDYzLTEzLjM4NjcxOSAyNy43MTg3NS0zMS44NTU0NjkgMzMuNTk3NjU3LTU0Ljg5NDUzMiA1LjgyODEyNS0yMi44Mzk4NDQgNi4xNjQwNjItNDcuMzYzMjgxIDYuNDg4MjgxLTcxLjA3ODEyNS4yNTM5MDYtMTguNTY2NDA2LjUxOTUzMS00MC41NTg1OTMuNTc0MjE5LTYyLjg2MzI4MS0uMDU0Njg4LTIyLjMwODU5NC0uMzIwMzEzLTQ0LjI5Njg3NS0uNTc0MjE5LTYyLjg2MzI4MS0uMzI0MjE5LTIzLjcxNDg0NC0uNjYwMTU2LTQ4LjIzODI4MS02LjQ4ODI4MS03MS4wODIwMzEtNS44Nzg5MDctMjMuMDM5MDYzLTE3LjE4MzU5NC00MS41MDc4MTMtMzMuNTk3NjU3LTU0Ljg5NDUzMi0xNy43NTc4MTItMTQuNDc2NTYyLTM5LjQzMzU5My0yMi41ODk4NDQtNjQuNDMzNTkzLTI0LjEwOTM3NS0zNS4yMTQ4NDQtMi4xNDQ1MzEtNzEuNDY0ODQ0LTMuMTMyODEyLTExMC44NTE1NjMtMy4wNDY4NzUtMzkuNDcyNjU2LS4wOTM3NS03NS43MjY1NjIuOTAyMzQ0LTExMC45NDE0MDYgMy4wNDY4NzUtMjUgMS41MTk1MzEtNDYuNjc1NzgxIDkuNjMyODEzLTY0LjQzMzU5NCAyNC4xMDkzNzUtMTYuNDE0MDYyIDEzLjM4NjcxOS0yNy43MTg3NSAzMS44NTU0NjktMzMuNTk3NjU2IDU0Ljg5NDUzMi01LjgyODEyNSAyMi44NDM3NS02LjE2NDA2MiA0Ny4zNjMyODEtNi40ODgyODEgNzEuMDgyMDMxLS4yNTM5MDcgMTguNTgyMDMxLS41MTk1MzEgNDAuNTg1OTM3LS41NzQyMTkgNjIuOTEwMTU2LjA1NDY4OCAyMi4yMjY1NjMuMzIwMzEyIDQ0LjIzNDM3NS41NzQyMTkgNjIuODE2NDA2LjMyNDIxOSAyMy43MTQ4NDQuNjYwMTU2IDQ4LjIzODI4MSA2LjQ4ODI4MSA3MS4wNzgxMjUgNS44Nzg5MDYgMjMuMDM5MDYzIDE3LjE4MzU5NCA0MS41MDc4MTMgMzMuNTk3NjU2IDU0Ljg5NDUzMiAxNy43NTc4MTMgMTQuNDc2NTYyIDM5LjQzMzU5NCAyMi41ODk4NDMgNjQuNDMzNTk0IDI0LjEwOTM3NCAzNS4yMTQ4NDQgMi4xNDQ1MzIgNzEuNDc2NTYyIDMuMTQ0NTMyIDExMC44NDc2NTYgMy4wNDY4NzZ6bS0uOTUzMTI1LTkwLjk5MjE4OGMtNjguOTIxODc1IDAtMTI1LTU2LjA3NDIxOS0xMjUtMTI1czU2LjA3ODEyNS0xMjUgMTI1LTEyNWM2OC45MjU3ODEgMCAxMjUgNTYuMDc0MjE5IDEyNSAxMjVzLTU2LjA3NDIxOSAxMjUtMTI1IDEyNXptMC0yMTBjLTQ2Ljg2NzE4NyAwLTg1IDM4LjEzMjgxMi04NSA4NXMzOC4xMzI4MTMgODUgODUgODVjNDYuODcxMDk0IDAgODUtMzguMTMyODEyIDg1LTg1cy0zOC4xMjg5MDYtODUtODUtODV6bTEzOS04MGMtMTYuNTY2NDA2IDAtMzAgMTMuNDI5Njg4LTMwIDMwczEzLjQzMzU5NCAzMCAzMCAzMGMxNi41NzAzMTMgMCAzMC0xMy40Mjk2ODggMzAtMzBzLTEzLjQyOTY4Ny0zMC0zMC0zMHptMCAwIi8+PC9zdmc+"

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZmlsbD0iIzA1MDAzOCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTExLjk4MiA1MTEuOTgyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMS45ODIgNTExLjk4MiIgd2lkdGg9IjUxMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJtMzQxLjg4NSAyMzguMzYtMTMwLTc0Yy02LjE4OC0zLjUyNC0xMy43ODctMy40ODgtMTkuOTQ2LjA5MS02LjE1OCAzLjU4MS05Ljk0NyAxMC4xNjctOS45NDcgMTcuMjl2MTQ4YzAgNy4xMjMgMy43ODkgMTMuNzA5IDkuOTQ3IDE3LjI5IDYuMTU5IDMuNTggMTMuNzU3IDMuNjE2IDE5Ljk0Ni4wOTFsMTMwLTc0YzEzLjM5Ni03LjQ3OCAxMy4zOTItMjcuMjg3IDAtMzQuNzYyem0tMTE5Ljg5NCA1Ni45ODR2LTc5LjIwNWw2OS41NzEgMzkuNjAzem0yODIuMzA2LTE0NS44NTVjLTYuNTAzLTQ1LjkxOC00My4zMDYtODEuMTQ2LTg5LjUtODUuNjY4LTM2LjQ4OS0zLjU3Mi05My42NjEtNy44My0xNTguODA2LTcuODNzLTEyMi4zMTYgNC4yNTgtMTU4LjgwNiA3LjgzYy00Ni4xOTQgNC41MjEtODIuOTk3IDM5Ljc1LTg5LjUgODUuNjY3LTEwLjI0NyA3Mi43My0xMC4yNDcgMTQwLjI3NiAwIDIxMy4wMDUgNi41MDMgNDUuOTE4IDQzLjMwNiA4MS4xNDYgODkuNSA4NS42NjggMzYuNDg5IDMuNTcyIDkzLjY2MSA3LjgzIDE1OC44MDYgNy44M3MxMjIuMzE2LTQuMjU4IDE1OC44MDYtNy44M2M0Ni4xOTQtNC41MjEgODIuOTk3LTM5Ljc1IDg5LjUtODUuNjY3IDEwLjI0Ny03Mi43MyAxMC4yNDctMTQwLjI3NiAwLTIxMy4wMDV6bS0zOS42MDYgMjA3LjM5N2MtMy45MDYgMjcuNTg0LTI2LjAyNSA0OC43NDctNTMuNzkxIDUxLjQ2NS0zNS42MDQgMy40ODYtOTEuMzc5IDcuNjQxLTE1NC45MDkgNy42NDFzLTExOS4zMDYtNC4xNTQtMTU0LjkwOS03LjY0MWMtMjcuNzY2LTIuNzE4LTQ5Ljg4NS0yMy44ODEtNTMuNzkxLTUxLjQ2Ni00Ljg0NC0zNC4yMDMtNy4zLTY4LjE0OC03LjMtMTAwLjg5NHMyLjQ1Ni02Ni42OSA3LjMtMTAwLjg5NWMzLjkwNi0yNy41ODQgMjYuMDI1LTQ4Ljc0NyA1My43OTEtNTEuNDY1IDM1LjYwNC0zLjQ4NiA5MS4zNzktNy42NDEgMTU0LjkwOS03LjY0MXMxMTkuMzA2IDQuMTU0IDE1NC45MDkgNy42NDFjMjcuNzY2IDIuNzE4IDQ5Ljg4NSAyMy44ODEgNTMuNzkxIDUxLjQ2NiA5LjcyMiA2OS4wMTggOS43MjIgMTMyLjc3MSAwIDIwMS43ODl6Ii8+PC9zdmc+"

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = require("D:\\laragon\\www\\carets_web\\carets_web\\node_modules\\react-static\\lib\\browser");

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var ReportContext = _react2["default"].createContext({
  report: function report() {}
});

exports["default"] = ReportContext;

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = require("hoist-non-react-statics");

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/interopRequireDefault");

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/interopRequireWildcard");

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function escape(url, needQuotes) {
  if (typeof url !== 'string') {
    return url;
  } // If url is already wrapped in quotes, remove them


  if (/^['"].*['"]$/.test(url)) {
    url = url.slice(1, -1);
  } // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls


  if (/["'() \t\n]/.test(url) || needQuotes) {
    return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"';
  }

  return url;
};

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Regular.8ebb4b89.eot";

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Medium.bfd92eef.eot";

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Light.2b35c59a.eot";

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Bold.51767ae0.eot";

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/delete-hero-bg.6c437ad1.png";

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, "404 - Oh no's! We couldn't find that page :("));
});

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);




/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-gray-100 py-12 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-4xl text-center px-5 font-semibold mb-6"
  }, "About ^Carets"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer border-t border-b py-7 px-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__["Scrollbars"], {
    style: {
      width: '100%'
    },
    className: "mt-3",
    autoHeight: true,
    autoHeightMax: 400
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-lg 2xl:text-base"
  }, "Carets is fun, family friendly app designed to enable users to integrate multiple videos from multiple users into a single interface.   We have created an our video interface based on our patented media player technology showing consolidated content. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-5 text-lg 2xl:text-base"
  }, "Its simple to use. You can create a video, tag it with a ^Caret designation just like how you use a #hashtag or @user symbol, and your videos will be consolidated automatically with content from other users with the same ^Caret designation. Its fun, it\u2019s easy, and it is pretty amazing to think of the possibilities."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-5 text-lg 2xl:text-base"
  }, "Image, the power of multiple cameras generating videos from their own perspective and being presented with other content in a single medium; you may not even know who the other people are who creating concurrent content of the same event but here you can see it.   "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-5 text-lg 2xl:text-base"
  }, "We invite you to download the ^Carets app, create your own content, and share your view of the world.  Invite your friends and family to participate and then you can all start to see what we see.")))));
});

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);




/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-gray-100 py-12 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-4xl text-center px-5 font-semibold mb-6"
  }, "CLM \u2013 Terms and Conditions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer border-t border-b py-7 px-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__["Scrollbars"], {
    style: {
      width: '100%'
    },
    className: "mt-3",
    autoHeight: true,
    autoHeightMax: 400
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-lg 2xl:text-base"
  }, "Welcome to Carets. These Terms and Conditions (\"Terms\") govern your placement of advertisements on our platform. By submitting an ad or advertising campaign, you (\"Advertiser\") agree to be bound by these Terms.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ol", {
    className: "listNumber mt-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "General Provisions", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must be at least 18 years old and have the legal authority to enter into this agreement."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are solely responsible for the content of your ads and any claims arising from them."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Your ads must comply with all applicable laws and regulations, including advertising, intellectual property, data privacy, and consumer protection laws."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must not submit ads that are:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplha"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "False, misleading, or deceptive."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Offensive, harmful, or discriminatory."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Infringing on any intellectual property rights."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Illegal or in violation of the Carets user guidelines."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You will provide us with all necessary information and materials for verifying your identity and compliance with these Terms."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We reserve the right to reject or remove any ad at any time and for any reason, without notice."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "If you are using Carets ad services as an agent for another individual or entity, then you represent and warrant that:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) you are authorized to, and do, bind that individual or entity to these terms and conditions.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) all of your actions in connection with these Terms are and will be within the scope of the agency relationship between you and that individual or entity, and in accordance with any applicable legal and fiduciary duties.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "H. Your use of Carets ad services will constitute acceptance of these Terms.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Advertising Formats and Specifications", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We offer various ad formats such as video, banner, and native ads. Refer to our advertising specifications for detailed requirements."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are responsible for ensuring that your ads meet all technical specifications including content, links, narrative, etc."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may modify ad formats and specifications at any time, with reasonable notice."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Licensing", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "If your ad content contains copyrighted auditable or visual works then you agree that you have obtained all rights, licenses and permissions as may be necessary for such content to be incorporated within your ad content.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "If your ad content contains musical works, compositions or sound recordings not sourced from Carets, then you agree that you have obtained all rights, licenses and permissions as may be necessary for such music to be incorporated within your ad content. For example, ensuring that such music can remain on Carets for a specified period after the campaign end date for certain ad and campaign products."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You grant license to Carets use ad content to:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) perform its obligations under these Terms, such as providing the ad services you have purchased;", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) to comply with any legal or regulatory obligations to which Carets or its affiliates are subject or to assist with a lawful investigation;", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(c) for testing and internal research and development purposes;", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(d) with your prior consent, for external marketing purposes or external research and development; and", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(e) to provide the transparency tools as described below."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Carets may remove or restrict access to your ad content, if we have reason to believe", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(i) you are in breach of these Terms; or", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(ii) you cause, or may cause, harm to Carets, its users, or other third parties. If we remove or restrict access to your ad content, we will notify you without undue delay, unless it is not appropriate or legally prevented from doing so.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "By submitting your ad content, you grant Carets a non-exclusive, royalty-free, worldwide, transferable, sublicensable license to access, use, host, cache, store, display, publish, distribute, modify, and adapt ad content in order to develop, research, provide, promote, and improve Carets products and services.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Content Creators and Influencers", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You may choose to engage individuals, such as bloggers, influencers (directly or through third parties) or other content creators, such as creative agencies or production companies to edit, create, produce and/or distribute ad content either", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) independently of Carets, or", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) through a program or initiative operated by Carets in connection with its third party creative partners. In each case, you remain solely responsible for any and all content and messaging that you suggest, request, and/or require creators to include, or that you otherwise approve to be included, in an ad or campaign, which will form part of your advertising content. Creators own their submitted content unless negotiated separately between you and the relevant creators. Carets is an independent contractor and will not be deemed an agent of either creators or you, nor will Carets be liable for the acts or omissions of creators."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Targeting and Delivery", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We offer various targeting options based on demographics, interests, and user behavior."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are responsible for selecting the appropriate targeting options for your ads and campaigns."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may refuse to display your ads on certain categories of content or to certain audiences."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We cannot guarantee the delivery of your ads to a specific number of users or at a specific time."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may refuse to display ads and campaigns that do not meet the requirements outlined in General Provisions.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Billing and Payment", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You will be responsible for all charges associated with your ad campaigns."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We offer a fixed rate pricing model."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must provide a valid payment method and keep it updated."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will bill you according to your chosen pricing model upon approval of your submitted ad and advertising campaign."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are responsible for any taxes applicable to your ad campaigns."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Payment triggers the activation of the ads and advertiser campaigns. Once payment is processed it is non-refundable.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must have legal authority to make payment on your entity\u2019s behalf.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Data Privacy and Security", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must comply with all applicable data privacy and security laws and regulations."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You will not collect or use any user data from our platform without explicit written consent."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will not share any user data with you except as necessary to deliver your ads and conduct analytics."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We have implemented appropriate security measures to protect user data."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Intellectual Property", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You own all intellectual property rights in your ads and the materials used in them."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You grant us a non-exclusive, worldwide license to use, reproduce, promote, and display your ads on our platform."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We own all intellectual property rights in our platform and brand. You will not infringe on our intellectual property rights."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Advertiser acknowledges and agrees to all rights to Carets intellectual property, including the patented use of a ^Caret, like a #Hashtag, and agrees to the terms of use of the ^Caret in ads and campaigns. Advertiser may purchase a license agreement for designated and dedicated ^Carets for promotional and licensing purposes of content for promotion within and outside the Carets platform.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Disclaimer of Warranties", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We provide our platform and advertising services \"as is\" and without any warranties, express or implied."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We do not warrant that our platform or advertising services will be uninterrupted, error-free, or secure."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will not be liable for any loss or damage arising from your use of our platform or advertising services."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We do guarantee a minimum number of ad impressions or ad click through rate.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Indemnification", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You agree to indemnify and hold us harmless from any and all claims, losses, damages, liabilities, costs, and expenses (including attorney's fees) arising from or in connection with your ads or your breach of these Terms."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Limitation of Liability", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Our total liability to you for any claim arising from or in connection with these Terms will be limited to the amount you paid for advertising services during the preceding month."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will not be liable for any indirect, incidental, consequential, special, or punitive damages."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Nothing in these terms will exclude or limit either party's liability for losses which may not be lawfully excluded or limited."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Carets and its affiliates will not be liable to you for any:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) indirect, incidental, special, consequential, or punitive damages; or", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) any loss of profits or revenues (whether incurred directly or indirectly), loss of data, use, goodwill, or intangible losses, even if Carets has been advised or should be aware of the possibility of any such losses arising."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "The maximum liability of Carets and its affiliates to you will not in aggregate exceed the greater of $250 usd or the amounts paid by you to Carets, if any, within the 2 months before the claim arose."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Term and Termination", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "These Terms will commence upon your first submission of an ad and will continue until terminated by either party."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You may terminate these Terms at any time by removing your ads and ceasing to use our platform."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may terminate these Terms at any time and for any reason, with or without notice."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Governing Law and Dispute Resolution", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "These Terms will be governed by and construed in accordance with the laws of the State of Utah, without regard to its conflict of laws principles."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "The laws of the state of Utah will govern these Terms and any claims and disputes (whether contractual or otherwise) arising out of or relating to these Terms or their subject matter."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Entire Agreement", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "These Terms constitute the entire agreement between Advertiser and Carets with respect to advertising on the Carets platform."))))));
});

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);



 // useEffect(() => {
//     window.scrollTo({ top: 0, behavior: "smooth" });
// }, []);

/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "CLM Tutorials")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer",
    tabIndex: -1
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-gray-100 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "LMmainSection"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flexRowF"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "content container py-16 mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "LMheroSection"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-white text-2xl md:text-5xl font-bold text-center"
  }, "Licensing Module Tutorial"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-white text-center text-xl md:text-3xl mt-5"
  }, "Learn how to build new videos", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "with your licensed content.")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "container py-16 mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    "class": "text-center mb-5 mt-3 text-xl md:text-4xl font-semibold"
  }, "When app users tag their videos with your ^Caret you", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "can use that video to create new content."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full max-w-sm mx-auto grid grid-cols-2 px-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "appuser-videoBanner w-full",
    src: "/video1.png",
    alt: "Video 1"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "appuser-videoBanner w-full",
    src: "/video2.png",
    alt: "Video 2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "appuser-videoBanner w-full",
    src: "/video3.png",
    alt: "Video 3"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "appuser-videoBanner w-full",
    src: "/video4.png",
    alt: "Video 4"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "licensedCarets py-16 px-4 mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-center mb-5 mt-3 text-xl md:text-4xl font-semibold"
  }, "Get started in three simple steps."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "mb-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "stepOne relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-center text-xl md:text-3xl font-semibold mt-10 mb-8"
  }, "Upload custom content"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "currentStep"
  }, "Step: 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "BLGridStep"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig1.png",
    alt: "Figure One",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "pt-2"
  }, "Splash"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig2.png",
    alt: "Figure Two",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Videos"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetVideo"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/play-icon.svg",
    alt: "Play",
    width: 18
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig3.png",
    alt: "Figure Three",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Watermark"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetWatermark"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/caretFavicon.svg",
    alt: "Audio",
    width: 48
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig4.png",
    alt: "Figure Four",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Custom Audio"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetAudio"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/Audio.svg",
    alt: "Audio",
    width: 40
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig5.png",
    alt: "Figure Five",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Promotions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetPromo"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/promo.png",
    alt: "Audio",
    width: 72
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "stepTitle"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Splash"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Videos"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Watermark"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Custom Audio"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Promotions"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "mb-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "stepOne stepTwo relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-center text-xl md:text-3xl font-semibold mt-10 mb-8"
  }, "Assemble a new licensed ^Carets video."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "currentStep"
  }, "Step: 2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "BLGridStep"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig1.png",
    alt: "Figure One",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "mt-1 btn btnPurple text-white",
    style: {
      maxWidth: 110
    }
  }, "Selected"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "pt-2"
  }, "Splash"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig2.png",
    alt: "Figure Two",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "mt-1 btn btnPurple text-white",
    style: {
      maxWidth: 110
    }
  }, "Selected"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Videos"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetVideo"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/play-icon.svg",
    alt: "Play",
    width: 18
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig3.png",
    alt: "Figure Three",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "mt-1 btn btnPurple text-white",
    style: {
      maxWidth: 110
    }
  }, "Selected"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Watermark"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetWatermark"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/caretFavicon.svg",
    alt: "Audio",
    width: 48
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig4.png",
    alt: "Figure Four",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "mt-1 btn btnPurple text-white",
    style: {
      maxWidth: 110
    }
  }, "Selected"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Custom Audio"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetAudio"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/Audio.svg",
    alt: "Audio",
    width: 40
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig5.png",
    alt: "Figure Five",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "mt-1 btn btnPurple text-white",
    style: {
      maxWidth: 110
    }
  }, "Selected"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-2"
  }, "Promotions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "assetPromo"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/promo.png",
    alt: "Audio",
    width: 72
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "stepTitle"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Select Splash"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Select Videos"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Select Watermark"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Select Audio"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Select Promotion"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "stepOne stepThree relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-center text-xl md:text-3xl font-semibold mt-10 mb-8"
  }, "Use your new licensed ^Caret videos."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "currentStep"
  }, "Step: 3"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "BLGridStep"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig1.png",
    alt: "Figure One",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "pt-2"
  }, "Splash"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "publishApp"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/publish-icon.png",
    alt: "publishIcon",
    width: 60
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figure", {
    className: "text-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/fig2.png",
    alt: "Figure Two",
    className: "mx-auto"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Videos"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "downloadApp"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/video-play-download-icon.svg",
    alt: "video Download Icon",
    width: 60
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "stepTitle"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Publish in the Carets App"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("figcaption", {
    className: "relative pt-8"
  }, "Download for your use"))))));
});

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4);





var faqs = [{
  quest: "How does Carets work?",
  ans: "The concept of Carets is simple. Videos are created through the Carets app, content is uploaded to the cloud via a wireless connection, and your videos, along with others matching location or tagging criteria, is available to view through our patented video player.",
  show: false
}, {
  quest: "Do I require a user name?",
  ans: "To create video content you are required to set up a user account which requires a user name.  For viewing content on a handheld device the media player is designed to work in the app. ",
  show: false
}, {
  quest: "I forgot my username and password; how do I retrieve this information?",
  ans: "Username and password retrieval can be accomplished through the app.",
  show: false
}, {
  quest: "Can I upload video content that isn\u2019t created through the app?",
  ans: "Yes, video stored on your mobile device can be uploaded through the app however the syncing features are limited.  ",
  show: false
}, {
  quest: "What kind of devices can I use with Carets?",
  ans: "Carets is designed for Apple iOS and Android devices.  ",
  show: false
}, {
  quest: "What kind of content can I create?",
  ans: "This platform has been designed for our users to create whatever they want as long it isn\u2019t offensive.  We encourage our users to get creative and come up with new and exciting ways to implement the technology for everyone\u2019s entertainment. You can learn more about acceptable content through the Carets Community Guidelines.",
  show: false
}, {
  quest: "What do I do if I see a video that does not meet the community guidelines?",
  ans: "There is a reporting feature in the app that allows users to flag content. Once flagged the Carets team is notified to review content related to that video for content.  Violators of the Carets Community Guidelines or any of the Terms and Conditions may result in account termination as outlined. Final determination is made by Carets.",
  show: false
}, {
  quest: "I have created a video but I\u2019m having a difficult time uploading it; what do ,I do?",
  ans: "The app is designed to upload directly to the server.  In the event there are network connectivity problems between your device and the network the upload may fail resulting in video loss.  ",
  show: false
}, {
  quest: "Can I store the videos I created in the app on my device?",
  ans: "Yes, there is an option to store the user\u2019s videos at the time of upload.",
  show: false
}, {
  quest: "How can I remove videos I don\u2019t want anymore?",
  ans: "To remove videos you must use the app to retrieve and manage user content.",
  show: false
}];
/* harmony default export */ __webpack_exports__["default"] = (function () {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(faqs),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      faqData = _useState2[0],
      setFaqData = _useState2[1];

  var faqSow = function faqSow(idx) {
    var newFaq = [];
    faqs.forEach(function (element, index) {
      if (idx === index) {
        element.show = !element.show;
      }

      newFaq.push(element);
    });
    setFaqData(newFaq);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-gray-100 py-12 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "text-4xl text-center px-5 font-semibold mb-6"
  }, "^Carets \u2013 Community Guidelines"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer border-t border-b py-7 px-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3__["Scrollbars"], {
    style: {
      width: '100%'
    },
    className: "mt-3",
    autoHeight: true,
    autoHeightMax: 400
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-lg 2xl:text-base"
  }, "The ^Carets\u2122 platform is a family friendly service model designed to enable our users to easily generate video content using handheld devices and posting this content to the app. The app is designed to aggregate videos from multiple users for concurrent and simultaneous viewing.  The content you have generated may be viewed with other user\u2019s content enabling a different perspective; this combined video is what we call a ^Carets Video\u2122.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "text-3xl mt-16 font-semibold"
  }, "^Carets Community Guidelines"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "The Carets community is intended to enable user to be creative in developing content and sharing with fellow users of the platform.  Carets potential is only gated by the creativity and imagination of its users. The platform is intended to be a family friendly and we ask that you help us accomplish this vision by creating and posting content that aligns with this intent.  "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "As with all good plans there are usually groups and individuals who have a different set of values and will attempt to abuse the system. We ask for your help to notify us if content appears to be out of line by reporting issues through the website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "text-2xl font-semibold mt-10"
  }, "Common Sense and General Guidelines"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "This is a family friend site; the following are ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("u", null, "examples of unacceptable content"), " and violation of these common sense rules may result in removal of your account and content. In cases where videos containing illegal activities are uploaded to the site and it is reported by other users this content and associated user account information will be issued to the appropriate authorities."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "styledList mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Nudity or Sexual Content: This includes pornography or sexually explicit content."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Harmful or Dangerous Content: Any content that encourages others to do things that may be harmful to themselves or to others. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Violent or Graphic Content: There are two types of content; anything that is intended to sensational or disrespectful is inappropriate. Content that could be found on the news or presented in a manner consistent with a documentary may be deemed acceptable.  In the latter case please provide audible narrative providing context as you create content. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Hateful Content: This includes derogatory or inflammatory content that is intended to degrade or condones violence towards individuals or groups based on race, ethnicity, religion, or sexual orientation."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Threats: This includes anything that could be represented as threatening to individuals or groups.  The Carets platform is not intended to be used for bullying, stalking, harassment, invading privacy, or predatory behaviors.  "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Misrepresentations, Spam, and Trolling: Please refrain from misrepresentation, spamming, and trolling various users. This also includes creating content and misleading users with headlines and titles that do not align with the intentions. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Copyright: Please respect copyrights and be mindful that pre-recorded content and images may be copyrighted and that redistribution may not be allowed or licensed."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-5 text-lg 2xl:text-base"
  }, "We review reports from the website carefully. In the event that content is considered inappropriate, and Carets reserves the right to be the final judge in such decisions, that content may be removed from viewers and accounts may be permanently deactivated."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "text-3xl mt-16 font-semibold"
  }, "Carets Privacy Notice"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "There are many different ways you can use Carets \u2013 to search for and share video content created on User Account\u2019s mobile devices.  We\u2019ve attempted to keep our Privacy Notice simple. Your privacy matters so whether you are new or a long-time user, please do take the time to get to know our practices \u2013 and if you have any questions contact us."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-5 text-xl 2xl:text-base font-medium"
  }, "We collect information to provide better services to all of our users. We collect information in the following ways:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "styledList"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Information you give us. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Information we get from your use of our services. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Device information"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Log-in information"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Location information"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Unique application numbers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Local storage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Cookies and similar technologies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "User name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Comments posted on the app")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "Transparency and choice"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "People have different privacy concerns. Our goal is to be clear about what information we collect, so that you can make meaningful choices about how it is used. For example, you can:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "styledList mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Review and update your Carets controls to decide what types of data, such as videos you\u2019ve watched on Carets or past searches, you would like saved with your account when you use our services."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "Adjust how the Profile associated with your Account appears to others. Our default is to use your user name on published video content. ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "Information you share"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "Our service is intended to allow you to share information with others. Remember that when you share information publicly it may be indexable by search engines.  You agree to share your User Name with your published content. You also agree to share GPS and time data with video content that is uploaded. You may adjust your settings to limit data provided to Carets. "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "Information we share"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "We do not share personal information with companies, organizations and individuals outside of Carets unless one of the following circumstances applies:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "styledList mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "With your consent"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "With domain administrators ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "If your Account is managed for you by a domain administrator then your domain administrator and resellers who provide user support to your organization will have access to your User Account information (including your email and other data). Your domain administrator may be able to:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "capitalLetter"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "view statistics regarding your account, like statistics regarding applications you install."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "change your account password."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "suspend or terminate your account access."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "access or retain information stored as part of your account."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "receive your account information in order to satisfy applicable law, regulation, legal process or enforceable governmental request."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "restrict your ability to delete or edit information or privacy settings."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "For legal reasons ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "We will share personal information with companies, organizations or individuals outside of Carets if we have a good-faith belief that access, use, preservation or disclosure of the information is reasonably necessary to:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "capitalLetter"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "meet any applicable law, regulation, legal process or enforceable governmental request."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "enforce applicable Terms of Service, including investigation of potential violations."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "detect, prevent, or otherwise address fraud, security or technical issues."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "protect against harm to the rights, property or safety of Carets, our users or the public as required or permitted by law.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-5 text-lg 2xl:text-base"
  }, "We may share non-personally identifiable information publicly and with our partners \u2013 like publishers, advertisers or connected sites. For example, we may share information publicly to show trends about the general use of our services. This may include a user\u2019s name displayed with associated content."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "If Carets is involved in a merger, acquisition or asset sale, we will continue to ensure the confidentiality of any personal information and give affected users notice before personal information is transferred or becomes subject to a different privacy policy."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "Information security"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "We work hard to protect Carets and our users from unauthorized access to or unauthorized alteration, disclosure or destruction of information we hold. In particular:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "styledList mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "We encrypt many of our services using SSL."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "We review our information collection, storage and processing practices, including physical security measures, to guard against unauthorized access to systems."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, "We restrict access to personal information to Carets employees, contractors and agents who need to know that information in order to process it for us, and who are subject to strict contractual confidentiality obligations and may be disciplined or terminated if they fail to meet these obligations.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "When this Privacy Notice applies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "Our Privacy Notice applies to all of the services offered by Carets and its affiliates. Our Privacy Notice does not apply to services offered by other companies or individuals, including products or sites that may be displayed to you in search results, sites that may include services, or other sites linked from our services. Our Privacy Notice does not cover the information practices of other companies and organizations who advertise our services, and who may use cookies, pixel tags and other technologies to serve and offer relevant ads."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "Compliance and cooperation with regulatory authorities"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "We regularly review our compliance with our Privacy Notice. When we receive formal written complaints, we will contact the person who made the complaint to follow up. We work with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of personal data that we cannot resolve with our users directly."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-2xl font-semibold mt-10"
  }, "Changes"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-3 text-lg 2xl:text-base"
  }, "Our Privacy Notice may change from time to time. We will post any Privacy Notice changes on this page and, if the changes are significant, we will provide a more prominent notice (including, for certain services, email notification of Privacy Notice changes). ")))));
});

/***/ }),
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);
/* harmony import */ var _assets_imgs_instruct_1_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(58);
/* harmony import */ var _assets_imgs_instruct_1_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_instruct_1_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_imgs_instruct_2_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(59);
/* harmony import */ var _assets_imgs_instruct_2_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_instruct_2_png__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _assets_imgs_instruct_3_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(60);
/* harmony import */ var _assets_imgs_instruct_3_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_instruct_3_png__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _assets_imgs_delete_icon_svg__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(61);
/* harmony import */ var _assets_imgs_delete_icon_svg__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_delete_icon_svg__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _assets_imgs_delete_hero_bg_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(38);
/* harmony import */ var _assets_imgs_delete_hero_bg_png__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_delete_hero_bg_png__WEBPACK_IMPORTED_MODULE_8__);









/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "grid md:grid-cols-2 gap-10 bgDelete py-16 px-8 items-center mt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    style: {
      textAlign: "center"
    },
    className: "text-2xl md:text-5xl font-bold text-white leading-normal text-center md:text-left"
  }, "Want To Delete Your Account?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_delete_icon_svg__WEBPACK_IMPORTED_MODULE_7___default.a,
    alt: "delete icon",
    className: "max-h-96 mx-auto"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-gray-1001 py-12 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-4xl text-center px-5 font-bold mb-6 text-gray-800"
  }, "Delete Account Instructions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "grid md:grid-cols-3 gap-8 px-5 py-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center px-8 md:px-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_instruct_1_png__WEBPACK_IMPORTED_MODULE_4___default.a,
    alt: "instruct 1",
    className: "mx-auto -mr-4 md:-mr-8"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-lg 2xl:text-base mt-4"
  }, "Login into the app.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center px-8 md:px-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_instruct_2_png__WEBPACK_IMPORTED_MODULE_5___default.a,
    alt: "instruct 2",
    className: "mx-auto -mr-4 md:-mr-8"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-4 text-lg 2xl:text-base"
  }, "After successfully authorizing, you will able to see your video feed list screen. You need to click on Profile icon from bottom bar.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center px-8 md:px-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_instruct_3_png__WEBPACK_IMPORTED_MODULE_6___default.a,
    alt: "instruct 3",
    className: "mx-auto -mr-4 md:-mr-8"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-4 text-lg 2xl:text-base"
  }, "On Profile Screen you will see the", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-red-600 font-semibold"
  }, "DELETE ACCOUNT", " "), "button. Click on it and you will see a confirmation dialog on the screen. Press", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-red-600 font-semibold"
  }, "YES"), " ", "to delete the account.")))));
});

/***/ }),
/* 45 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);




var faqs = [{
  quest: "How does Carets work?",
  ans: "The concept of Carets is simple. Videos are created through the Carets app, content is uploaded to the cloud via a wireless connection, and your videos, along with others matching location or tagging criteria, is available to view through our patented video player.",
  show: false
}, {
  quest: "Do I require a user name?",
  ans: "To create video content you are required to set up a user account which requires a user name.  For viewing content on a handheld device the media player is designed to work in the app. ",
  show: false
}, {
  quest: "I forgot my username and password; how do I retrieve this information?",
  ans: "Username and password retrieval can be accomplished through the app.",
  show: false
}, {
  quest: "Can I upload video content that isn\u2019t created through the app?",
  ans: "Yes, video stored on your mobile device can be uploaded through the app however the syncing features are limited.  ",
  show: false
}, {
  quest: "What kind of devices can I use with Carets?",
  ans: "Carets is designed for Apple iOS and Android devices.  ",
  show: false
}, {
  quest: "What kind of content can I create?",
  ans: "This platform has been designed for our users to create whatever they want as long it isn\u2019t offensive.  We encourage our users to get creative and come up with new and exciting ways to implement the technology for everyone\u2019s entertainment. You can learn more about acceptable content through the Carets Community Guidelines.",
  show: false
}, {
  quest: "What do I do if I see a video that does not meet the community guidelines?",
  ans: "There is a reporting feature in the app that allows users to flag content. Once flagged the Carets team is notified to review content related to that video for content.  Violators of the Carets Community Guidelines or any of the Terms and Conditions may result in account termination as outlined. Final determination is made by Carets.",
  show: false
}, {
  quest: "I have created a video but I\u2019m having a difficult time uploading it; what do ,I do?",
  ans: "The app is designed to upload directly to the server.  In the event there are network connectivity problems between your device and the network the upload may fail resulting in video loss.  ",
  show: false
}, {
  quest: "Can I store the videos I created in the app on my device?",
  ans: "Videos will be saved on your device at the time of upload.",
  show: false
}, {
  quest: "How can I remove videos I don\u2019t want anymore?",
  ans: "To remove the videos you uploaded go to your user profile page and hold your finger on the video you want to remove. A pop-up will confirm the request to remove it.",
  show: false
}];
/* harmony default export */ __webpack_exports__["default"] = (function () {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(faqs),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      faqData = _useState2[0],
      setFaqData = _useState2[1];

  var faqSow = function faqSow(idx) {
    var newFaq = [];
    faqs.forEach(function (element, index) {
      if (idx === index) {
        element.show = !element.show;
      }

      newFaq.push(element);
    });
    setFaqData(newFaq);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "text-4xl mt-16 text-center px-5 font-semibold"
  }, "Frequently Asked Questions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "faqs"
  }, faqData && faqData.map(function (elem, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "faq mt-6"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      onClick: function onClick() {
        return faqSow(index);
      },
      className: "quest text-2xl cursor-pointer px-6 py-4 bg-gray-100 hover:bg-gray-200 transition-all ease-in-out duration-500 border border-gray-100 flex items-center justify-between"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, elem.quest), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: elem.show ? "faqToggle open" : "faqToggle"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null))), elem.show && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "ans px-6 py-4 text-xl leading-relaxed border border-gray-100 animFaq " + (elem.show ? "block" : "hidden")
    }, elem.ans));
  }))));
});

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_imgs_11_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(62);
/* harmony import */ var _assets_imgs_11_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_11_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_imgs_22_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(63);
/* harmony import */ var _assets_imgs_22_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_22_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_imgs_33_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(64);
/* harmony import */ var _assets_imgs_33_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_33_png__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _assets_imgs_44_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(65);
/* harmony import */ var _assets_imgs_44_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_imgs_44_png__WEBPACK_IMPORTED_MODULE_6__);






 // import video from "../assets/intro.mp4";
// import videoDesktop from "../assets/caretsintrodesktop.mp4";

/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "homePage"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container1 mx-auto mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col container w-full max-w-5xl mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    // style={{ backgroundColor: "#E8E9EE" }}
    className: "flex overflow-hidden justify-center items-end mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("video", {
    playsInline: true,
    src: "https://caretsffmpeg.s3.amazonaws.com/promosvideos/caretsintrodesktop.mp4",
    autoPlay: true,
    muted: false,
    loop: true,
    preload: "auto",
    controls: true,
    style: {
      marginBottom: "-1px"
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "grid grid-cols-2 md:grid-cols-4 gap-4 mt-auto w-full max-w-5xl mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-9"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_11_png__WEBPACK_IMPORTED_MODULE_3___default.a,
    className: "w-full",
    alt: "Video 1"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-9"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_22_png__WEBPACK_IMPORTED_MODULE_4___default.a,
    className: "w-full",
    alt: "Video 2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-9"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_33_png__WEBPACK_IMPORTED_MODULE_5___default.a,
    className: "w-full",
    alt: "Video 3"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-9"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_imgs_44_png__WEBPACK_IMPORTED_MODULE_6___default.a,
    className: "w-full h-full object-cover",
    alt: "Video 4"
  }))))));
});

/***/ }),
/* 47 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
/* harmony import */ var _components_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(18);
/* harmony import */ var _assets_video_mp4__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15);
/* harmony import */ var _assets_video_mp4__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_video_mp4__WEBPACK_IMPORTED_MODULE_4__);





var userData = {
  username: "@danialLavez",
  tags: ["#party", "^happy", "@fiesta"],
  music: "Justin Bieber - Yummy",
  videoUrl: _assets_video_mp4__WEBPACK_IMPORTED_MODULE_4___default.a
};
/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    isCenter: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-14"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_user__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    data: userData
  }))));
});

/***/ }),
/* 48 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4);





var faqs = [{
  quest: "How does Carets work?",
  ans: "The concept of Carets is simple. Videos are created through the Carets app, content is uploaded to the cloud via a wireless connection, and your videos, along with others matching location or tagging criteria, is available to view through our patented video player.",
  show: false
}, {
  quest: "Do I require a user name?",
  ans: "To create video content you are required to set up a user account which requires a user name.  For viewing content on a handheld device the media player is designed to work in the app. ",
  show: false
}, {
  quest: "I forgot my username and password; how do I retrieve this information?",
  ans: "Username and password retrieval can be accomplished through the app.",
  show: false
}, {
  quest: "Can I upload video content that isn\u2019t created through the app?",
  ans: "Yes, video stored on your mobile device can be uploaded through the app however the syncing features are limited.  ",
  show: false
}, {
  quest: "What kind of devices can I use with Carets?",
  ans: "Carets is designed for Apple iOS and Android devices.  ",
  show: false
}, {
  quest: "What kind of content can I create?",
  ans: "This platform has been designed for our users to create whatever they want as long it isn\u2019t offensive.  We encourage our users to get creative and come up with new and exciting ways to implement the technology for everyone\u2019s entertainment. You can learn more about acceptable content through the Carets Community Guidelines.",
  show: false
}, {
  quest: "What do I do if I see a video that does not meet the community guidelines?",
  ans: "There is a reporting feature in the app that allows users to flag content. Once flagged the Carets team is notified to review content related to that video for content.  Violators of the Carets Community Guidelines or any of the Terms and Conditions may result in account termination as outlined. Final determination is made by Carets.",
  show: false
}, {
  quest: "I have created a video but I\u2019m having a difficult time uploading it; what do ,I do?",
  ans: "The app is designed to upload directly to the server.  In the event there are network connectivity problems between your device and the network the upload may fail resulting in video loss.  ",
  show: false
}, {
  quest: "Can I store the videos I created in the app on my device?",
  ans: "Yes, there is an option to store the user\u2019s videos at the time of upload.",
  show: false
}, {
  quest: "How can I remove videos I don\u2019t want anymore?",
  ans: "To remove videos you must use the app to retrieve and manage user content.",
  show: false
}];
/* harmony default export */ __webpack_exports__["default"] = (function () {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(faqs),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      faqData = _useState2[0],
      setFaqData = _useState2[1];

  var faqSow = function faqSow(idx) {
    var newFaq = [];
    faqs.forEach(function (element, index) {
      if (idx === index) {
        element.show = !element.show;
      }

      newFaq.push(element);
    });
    setFaqData(newFaq);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-gray-100 py-12 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "text-4xl text-center px-5 font-semibold mb-6"
  }, "^Carets \u2013 Terms and Conditions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer border-t border-b py-7 px-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_3__["Scrollbars"], {
    style: {
      width: "100%"
    },
    className: "mt-3",
    autoHeight: true,
    autoHeightMax: 400
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-lg 2xl:text-base"
  }, "The ^Carets\u2122 platform is a family friendly service model designed to enable our users to easily generate video content using handheld devices and posting this content to the app. The app is designed to aggregate videos from multiple users for concurrent and simultaneous viewing. The content you have generated may be viewed with other user\u2019s content enabling a different perspective; this combined video is what we call a ^Carets Video."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "mt-5 text-lg 2xl:text-base"
  }, "The following is intended to provide our customers with the best user experience we can provide; this includes ^Carets (Carets) Terms and Conditions:")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "text-3xl mt-16 font-semibold"
  }, "^Carets Terms and Conditions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ol", {
    className: "termsList"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Your Acceptance"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "By using or visiting the Carets website or any ^Carets products, software, data feeds, and services provided to you on, from, or through the ^Carets website or mobile application (collectively the \"Service\") you signify your agreement to ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (1) "), " ", "These terms and conditions (the \"Terms and Conditions\"),", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (2) "), " Carets's Privacy Notice and incorporated herein by reference, and ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (3) "), " ", "Carets's Community Guidelines\xA0and also incorporated herein by reference. If you do not agree to any of these terms, the Carets Privacy Notice, or ^ Community Guidelines, please do not use the Service.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Although we may attempt to notify you when major changes are made to these Terms and Conditions, you should periodically review the most up-to-date version. Carets may, in its sole discretion, modify or revise these Terms and Conditions and policies at any time, and you agree to be bound by such modifications or revisions. Nothing in these Terms and Conditions shall be deemed to confer any third-party rights or benefits.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Service"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "These Terms and Conditions apply to all users of the Service, including users who are also contributors of Content on the Service. \u201CContent\u201D includes the video, audio, audiovisual combinations, text, software, scripts, graphics, sounds, interactive features and other materials you may view on, access through, or contribute to the Service. The Service includes all aspects of Carets, including but not limited to all products, software and services offered via the Carets website and mobile applications.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "The Service may contain links to third party websites that are not owned or controlled by Carets. Carets has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party websites. In addition, Carets will not and cannot censor or edit the content of any third-party site. By using the Service, you expressly relieve Carets from any and all liability arising from your use of any third-party website.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "The Service is designed to allow your video content to be aggregated and combined with other content published through Carets. By publishing videos users agree to all aggregation and publication with content created by other users in the community.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Accordingly, we encourage you to be aware when you leave the Service and to read the terms and conditions and privacy policy of each other website that you visit.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Carets Accounts"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "In order to create content or access some features of the Service, you will have to create a Carets or Carets User Account. You may also be required to download the app associated with your handheld device. You may never use another's account without permission. When creating your account, you must provide accurate and complete information. You are solely responsible for the activity that occurs on your account, and you must keep your account password secure. You must notify Carets immediately of any breach of security or unauthorized use of your account.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Although Carets will not be liable for your losses caused by any unauthorized use of your account, you may be liable for losses to Carets or others due to such unauthorized use.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "General Use of the Service\u2014Permissions and Restrictions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Carets hereby grants you permission to access and use the Service as set forth in these Terms and Conditions, provided that:")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree not to distribute in any medium any part of the Service or the Content without Carets' prior written authorization, unless Carets makes available the means for such distribution through functionality offered by the Service.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree not to alter or modify any part of the Service.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree to allow your video content to be aggregated and shown with other content shared through Carets including audio, video, GPS and timing data.\xA0")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree Carets may aggregate your user content with user content created by other users to create new videos.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree and provide express permission for your individual videos to be aggregated with other user content for the use and view of the Carets community.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree Carets may place a watermark on all user generated content.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree Carets may integrate Carets authorized content into Caret videos.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree Carets may use user generated content for both primary and third-party advertising; this may include inclusion of externally generated content that may be incorporated into Caret Videos.\xA0")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree not to access Content through any technology or means other than the video playback through the app or the Caret\u2019s webpage of the Service itself or other explicitly authorized means Carets may designate. \xA0")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree not to use the Service for any of the following commercial uses unless you obtain Carets' prior written approval:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "the sale of access to the Service;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "the sale of advertising, sponsorships, or promotions placed on or within the Service or Content; or")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "the sale of advertising, sponsorships, or promotions on any page of an ad-enabled blog or website containing Content delivered via the Service, unless other material not obtained from Carets appears on the same page and is of sufficient value to be the basis for such sales.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Prohibited commercial uses do not include:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "uploading an original video to Carets to promote your business or artistic enterprise;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Showing Carets videos an ad-enabled blog or website subject to the advertising restrictions set forth above; or")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "any use that Carets expressly authorizes in writing.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree not to use or launch any automated system, including without limitation, \"robots,\" \"bots,\" \"spiders,\" or \"offline readers,\" that accesses the Service in a manner that sends more request messages to the Carets servers in a given period of time than a human can reasonably produce in the same period by using a conventional on-line web browser. Notwithstanding the foregoing, Carets grants the operators of public search engines permission to use spiders to copy materials from the site for the sole purpose of and solely to the extent necessary for creating publicly available searchable indices of the materials, but not caches or archives of such materials. Carets reserves the right to revoke these exceptions either generally or in specific cases. You agree not to collect or harvest any personally identifiable information, including items such as account holder names, usernames, from the Service, nor to use the communication systems provided by the Service (e.g., comments, email) for any commercial solicitation purposes. You agree not to solicit, for commercial purposes, any users of the Service with respect to their Content.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "In your use of the Service, you will comply with all applicable laws.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Carets reserves the right to discontinue any aspect of the Service at any time.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree through these terms and conditions that Carets has developed proprietary technology as part of our service model and that appropriate documentation has been filed with the US patent office claiming this Intellectual Property. Carets uses proprietary patented technology for video aggregation. Additional provision patents and trademarks are pending including the integration of the \"^\" symbol used to trigger video aggregation. You also agree as a user of any part of the Carets service that you are not authorized to duplicate, copy, transfer or in any way mimic proprietary technology or applications within the Carets platform without express written authorization and/or license from Carets.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree and provide express permission your uploaded ^Caret videos may be used by ^Caret license holders for their use, aggregation, modification, and distribution.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Your Use of Content"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "In addition to the general restrictions above, the following restrictions and conditions apply specifically to your use of Content.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "The Content on the Service, and the trademarks, service marks and logos (\"Marks\") on the Service, are owned by or licensed to Carets, subject to copyright and other intellectual property rights under the law.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Content is provided to you AS IS. You may access Content for your information and personal use solely as intended through the provided functionality of the Service and as permitted under these Terms and Conditions. You shall not download any Content unless you see a \u201Cdownload\u201D or similar link displayed by Carets on the Service for that Content. You shall not copy, reproduce, distribute, transmit, broadcast, display, sell, license, or otherwise exploit any Content for any other purposes without the prior written consent of Carets or the respective licensors of the Content. Carets and its licensors reserve all rights not expressly granted in and to the Service and the Content.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree not to circumvent, disable or otherwise interfere with security-related features of the Service or features that prevent or restrict use or copying of any Content or enforce limitations on use of the Service or the Content therein.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree your video content will be aggregated and shown concurrently with other video content uploaded and stored through Carets including audio, video, GPS and timing data.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You understand that when using the Service, you will be exposed to Content from a variety of sources, and that Carets is not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such Content. You further understand and acknowledge that you may be exposed to Content that is inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against Carets with respect thereto, and, to the extent permitted by applicable law, agree to indemnify and hold harmless Carets, its owners, operators, affiliates, licensors, and licensees to the fullest extent allowed by law regarding all matters related to your use of the Service.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Your Content and Conduct"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "As a Carets account holder you may submit Content to the Service, including videos and user comments. You understand that Carets does not guarantee any confidentiality with respect to any Content you submit.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You shall be solely responsible for your own Content and the consequences of submitting and publishing your Content on the Service. You affirm, represent, and warrant that you own or have the necessary licenses, rights, consents, and permissions to publish Content you submit; and you license to Carets all patent, trademark, trade secret, copyright or other proprietary rights in and to such Content for publication on the Service pursuant to these Terms and Conditions.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "For clarity, you retain all of your ownership rights in your Content. However, by submitting Content to Carets, you hereby grant Carets a worldwide, non-exclusive, royalty-free, sublicenseable and transferable license to use, reproduce, combine and publish with other user\u2019s content, distribute, prepare derivative works of, display, and perform the Content in connection with the Service and Carets's (and its successors' and affiliates') business, including without limitation for promoting and redistributing part or all of the Service (and derivative works thereof) in any media formats and through any media channels. You also hereby grant each user of the Service a non-exclusive license to access your Content through the Service, and to use, reproduce, distribute, display and perform such Content as permitted through the functionality of the Service and under these Terms and Conditions. The above licenses granted by you in video Content you submit to the Service terminate within a commercially reasonable time after you remove or delete your videos from the Service. You understand and agree, however, that Carets may retain, but not display, distribute, or perform, server copies of your videos that have been removed or deleted. The above licenses granted by you in user comments you submit are perpetual and irrevocable. You also agree that Carets is not intended to be a permanent holder of Account holder\u2019s content and that data may be deleted for any reason solely at the discretion of Carets. \xA0")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You further agree that Content you submit to the Service will not contain third party copyrighted material, or material that is subject to other third party proprietary rights, unless you have permission from the rightful owner of the material or you are otherwise legally entitled to post the material and to grant Carets all of the license rights granted herein.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You further agree that you will not submit to the Service any Content or other material that is contrary to the Carets Community Guidelines which may be updated from time to time, or contrary to applicable local, national, and international laws and regulations.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Carets does not endorse any Content submitted to the Service by any user or other licensor, or any opinion, recommendation, or advice expressed therein, and Carets expressly disclaims any and all liability in connection with Content. Carets does not permit copyright infringing activities and infringement of intellectual property rights on the Service, and Carets will remove all Content if properly notified that such Content infringes on another's intellectual property rights. Carets reserves the right to remove Content without prior notice.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Users may block app content and communications from other users at any time and for any reason. Carets does not monitor blocking of users. Carets will not reverse blocking requests by individuals who have been blocked by other users.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Account Termination Policy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Carets will terminate a user's access to the Service if, under appropriate circumstances, the user is determined to be a repeat infringer.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Carets reserves the right to decide whether Content violates these Terms and Conditions for reasons other than copyright infringement, such as, but not limited to, pornography, obscenity, or excessive length. Carets may at any time, without prior notice and in its sole discretion, remove such Content and/or terminate a user's account for submitting such material in violation of these Terms and Conditions.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Users may report abusive behaviors and/or violations of the terms and conditions of other users. Reporting initiates account review by Carets within 24 hours. In the event of violation of terms and conditions, or behaviors deemed unacceptable by Carets, may result in both content removal and/or account termination. Carets does not have an appeals process to reinstate an account following termination.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Digital Millennium Copyright Act"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "If you are a copyright owner or an agent thereof and believe that any Content infringes upon your copyrights, you may submit a notification pursuant to the Digital Millennium Copyright Act (\"DMCA\") by providing our Copyright Agent with the following information in writing (see 17 U.S.C 512(c)(3) for further detail):"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit the service provider to locate the material;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Information reasonably sufficient to permit the service provider to contact you, such as an address, telephone number, and, if available, an electronic mail;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You may direct copyright infringement notifications to our email:\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("u", null, "support@carets.tv"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, ". For clarity, only DMCA notices should go to the Copyright Agent; any other feedback, comments, requests for technical support, and other communications should be directed to Carets customer service through ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("u", null, "support@carets.tv"), ". You acknowledge that if you fail to comply with all of the requirements of this Section 5(D), your DMCA notice may not be valid."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Counter-Notice. If you believe that your Content that was removed (or to which access was disabled) is not infringing, or that you have the authorization from the copyright owner, the copyright owner's agent, or pursuant to the law, to post and use the material in your Content, you may send a counter-notice containing the following information to the Copyright Agent:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Your physical or electronic signature;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Identification of the Content that has been removed or to which access has been disabled and the location at which the Content appeared before it was removed or disabled;")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "A statement that you have a good faith belief that the Content was removed or disabled as a result of mistake or a misidentification of the Content; and")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Your name, address, telephone number, and e-mail address, a statement that you consent to the jurisdiction of the federal court in San Francisco, California, and a statement that you will accept service of process from the person who provided notification of the alleged infringement.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "If a counter-notice is received by the Copyright Agent, Carets may send a copy of the counter-notice to the original complaining party informing that person that it may replace the removed Content or cease disabling it in 10 business days. Unless the copyright owner files an action seeking a court order against the Content provider, member or user, the removed Content may be replaced, or access to it restored, in 10 to 14 business days or more after receipt of the counter-notice, at Carets's sole discretion.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Warranty Disclaimer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "YOU AGREE THAT YOUR USE OF THE SERVICES SHALL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, CARETS, ITS OFFICERS, DIRECTORS, EMPLOYEES, AND AGENTS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SERVICES AND YOUR USE THEREOF. CARETS MAKES NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SITE'S CONTENT OR THE CONTENT OF ANY SITES LINKED TO THIS SITE AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (I)"), " ERRORS, MISTAKES, OR INACCURACIES OF CONTENT.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (II)"), " PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (III)"), " ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (IV)"), " ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (IV)"), " ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (V)"), " ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICES. CARETS DOE S NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICES OR ANY HYPERLINKED SERVICES OR FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND CARETS WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Limitation of Liability"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "IN NO EVENT SHALL CARETS, ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS, BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES WHATSOEVER RESULTING FROM ANY", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (I)"), " ERRORS, MISTAKES, OR INACCURACIES OF CONTENT", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (II)"), " PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (III)"), " ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (IV)"), " ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (IV)"), " ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (V)"), " ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICES, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT THE COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "YOU SPECIFICALLY ACKNOWLEDGE THAT CARETS SHALL NOT BE LIABLE FOR CONTENT OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE FOREGOING RESTS ENTIRELY WITH YOU.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "The Service is controlled and offered by Carets from its facilities in the United States of America. Carets makes no representations that the Service is appropriate or available for use in other regions or countries. Those who access or use the Service from other jurisdictions do so at their own volition and are responsible for compliance with local law."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Indemnity"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "To the extent permitted by applicable law, you agree to defend, indemnify and hold harmless Carets, its parent company Carets Corporation, officers, directors, employees and agents, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees) arising from:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (i)"), " your use of and access to the Service; ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, "(ii) "), " your violation of any term of these Terms and Conditions; ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, "(iii)"), " your violation of any third party right, including without limitation any copyright, property, or privacy right; or", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, "(iv) "), " any claim that your Content caused damage to a third party. This defense and indemnification obligation will survive these Terms and Conditions and your use of the Service."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Ability to Accept Terms and Conditions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You affirm that you are either more than 18 years of age, or an emancipated minor, or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms and Conditions, and to abide by and comply with these Terms and Conditions. In any case, you affirm that you are over the age of 13, as the Service is not intended for children under 13. If you are under 13 years of age, then please do not use the Service. There are lots of other great web sites for you. Talk to your parents about what sites are appropriate for you."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Assignment"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "These Terms and Conditions, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by Carets without restriction."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "General"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "You agree that: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (i) "), "the Service shall be deemed solely based in Utah; and ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("strong", null, " (ii) "), " the Service shall be deemed a passive website that does not give rise to personal jurisdiction over Carets, either specific or general, in jurisdictions other than Utah. These Terms and Conditions shall be governed by the internal substantive laws of the State of Utah, without respect to its conflict of laws principles. Any claim or dispute between you and Carets that arises in whole or in part from the Service shall be decided exclusively by a court of competent jurisdiction located in Salt Lake County, Utah. These Terms and Conditions, together with the Privacy Notice and any other legal notices published by Carets on the Service, shall constitute the entire agreement between you and Carets concerning the Service. If any provision of these Terms and Conditions is deemed invalid by a court of competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms and Conditions, which shall remain in full force and effect. No waiver of any term of this these Terms and Conditions shall be deemed a further or continuing waiver of such term or any other term, and Carets's failure to assert any right or provision under these Terms and Conditions shall not constitute a waiver of such right or provision. Carets reserves the right to amend these Terms and Conditions at any time and without notice, and it is your responsibility to review these Terms and Conditions for any changes. Your use of the Service following any amendment of these Terms and Conditions will signify your assent to and acceptance of its revised terms.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "YOU AND CARETS AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF OR RELATED TO THE SERVICES MUST COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES. OTHERWISE, SUCH CAUSE OF ACTION IS PERMANENTLY BARRED.")))))));
});

/***/ }),
/* 49 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);




/* harmony default export */ __webpack_exports__["default"] = (function () {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "googlebot",
    content: "noindex,nofollow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, "Carets | Create, share, and combine video content")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-gray-100 py-12 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-4xl text-center px-5 font-semibold mb-6"
  }, "Advertising \u2013 Terms and Conditions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer border-t border-b py-7 px-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_custom_scrollbars__WEBPACK_IMPORTED_MODULE_2__["Scrollbars"], {
    style: {
      width: '100%'
    },
    className: "mt-3",
    autoHeight: true,
    autoHeightMax: 400
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-lg 2xl:text-base"
  }, "Welcome to Carets. These Terms and Conditions (\"Terms\") govern your placement of advertisements on our platform. By submitting an ad or advertising campaign, you (\"Advertiser\") agree to be bound by these Terms.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ol", {
    className: "listNumber mt-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "General Provisions", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must be at least 18 years old and have the legal authority to enter into this agreement."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are solely responsible for the content of your ads and any claims arising from them."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Your ads must comply with all applicable laws and regulations, including advertising, intellectual property, data privacy, and consumer protection laws."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must not submit ads that are:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplha"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "False, misleading, or deceptive."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Offensive, harmful, or discriminatory."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Infringing on any intellectual property rights."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Illegal or in violation of the Carets user guidelines."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You will provide us with all necessary information and materials for verifying your identity and compliance with these Terms."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We reserve the right to reject or remove any ad at any time and for any reason, without notice."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "If you are using Carets ad services as an agent for another individual or entity, then you represent and warrant that:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) you are authorized to, and do, bind that individual or entity to these terms and conditions.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) all of your actions in connection with these Terms are and will be within the scope of the agency relationship between you and that individual or entity, and in accordance with any applicable legal and fiduciary duties.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "H. Your use of Carets ad services will constitute acceptance of these Terms.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Advertising Formats and Specifications", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We offer various ad formats such as video, banner, and native ads. Refer to our advertising specifications for detailed requirements."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are responsible for ensuring that your ads meet all technical specifications including content, links, narrative, etc."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may modify ad formats and specifications at any time, with reasonable notice."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Licensing", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "If your ad content contains copyrighted auditable or visual works then you agree that you have obtained all rights, licenses and permissions as may be necessary for such content to be incorporated within your ad content.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "If your ad content contains musical works, compositions or sound recordings not sourced from Carets, then you agree that you have obtained all rights, licenses and permissions as may be necessary for such music to be incorporated within your ad content. For example, ensuring that such music can remain on Carets for a specified period after the campaign end date for certain ad and campaign products."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You grant license to Carets use ad content to:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) perform its obligations under these Terms, such as providing the ad services you have purchased;", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) to comply with any legal or regulatory obligations to which Carets or its affiliates are subject or to assist with a lawful investigation;", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(c) for testing and internal research and development purposes;", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(d) with your prior consent, for external marketing purposes or external research and development; and", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(e) to provide the transparency tools as described below."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Carets may remove or restrict access to your ad content, if we have reason to believe", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(i) you are in breach of these Terms; or", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(ii) you cause, or may cause, harm to Carets, its users, or other third parties. If we remove or restrict access to your ad content, we will notify you without undue delay, unless it is not appropriate or legally prevented from doing so.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "By submitting your ad content, you grant Carets a non-exclusive, royalty-free, worldwide, transferable, sublicensable license to access, use, host, cache, store, display, publish, distribute, modify, and adapt ad content in order to develop, research, provide, promote, and improve Carets products and services.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Content Creators and Influencers", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You may choose to engage individuals, such as bloggers, influencers (directly or through third parties) or other content creators, such as creative agencies or production companies to edit, create, produce and/or distribute ad content either", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) independently of Carets, or", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) through a program or initiative operated by Carets in connection with its third party creative partners. In each case, you remain solely responsible for any and all content and messaging that you suggest, request, and/or require creators to include, or that you otherwise approve to be included, in an ad or campaign, which will form part of your advertising content. Creators own their submitted content unless negotiated separately between you and the relevant creators. Carets is an independent contractor and will not be deemed an agent of either creators or you, nor will Carets be liable for the acts or omissions of creators."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Targeting and Delivery", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We offer various targeting options based on demographics, interests, and user behavior."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are responsible for selecting the appropriate targeting options for your ads and campaigns."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may refuse to display your ads on certain categories of content or to certain audiences."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We cannot guarantee the delivery of your ads to a specific number of users or at a specific time."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may refuse to display ads and campaigns that do not meet the requirements outlined in General Provisions.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Billing and Payment", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You will be responsible for all charges associated with your ad campaigns."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We offer a fixed rate pricing model."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must provide a valid payment method and keep it updated."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will bill you according to your chosen pricing model upon approval of your submitted ad and advertising campaign."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You are responsible for any taxes applicable to your ad campaigns."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Payment triggers the activation of the ads and advertiser campaigns. Once payment is processed it is non-refundable.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must have legal authority to make payment on your entity\u2019s behalf.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Data Privacy and Security", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You must comply with all applicable data privacy and security laws and regulations."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You will not collect or use any user data from our platform without explicit written consent."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will not share any user data with you except as necessary to deliver your ads and conduct analytics."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We have implemented appropriate security measures to protect user data."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Intellectual Property", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You own all intellectual property rights in your ads and the materials used in them."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You grant us a non-exclusive, worldwide license to use, reproduce, promote, and display your ads on our platform."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We own all intellectual property rights in our platform and brand. You will not infringe on our intellectual property rights."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Advertiser acknowledges and agrees to all rights to Carets intellectual property, including the patented use of a ^Caret, like a #Hashtag, and agrees to the terms of use of the ^Caret in ads and campaigns. Advertiser may purchase a license agreement for designated and dedicated ^Carets for promotional and licensing purposes of content for promotion within and outside the Carets platform.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Disclaimer of Warranties", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We provide our platform and advertising services \"as is\" and without any warranties, express or implied."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We do not warrant that our platform or advertising services will be uninterrupted, error-free, or secure."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will not be liable for any loss or damage arising from your use of our platform or advertising services."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We do guarantee a minimum number of ad impressions or ad click through rate.", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Indemnification", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You agree to indemnify and hold us harmless from any and all claims, losses, damages, liabilities, costs, and expenses (including attorney's fees) arising from or in connection with your ads or your breach of these Terms."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Limitation of Liability", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Our total liability to you for any claim arising from or in connection with these Terms will be limited to the amount you paid for advertising services during the preceding month."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We will not be liable for any indirect, incidental, consequential, special, or punitive damages."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Nothing in these terms will exclude or limit either party's liability for losses which may not be lawfully excluded or limited."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "Carets and its affiliates will not be liable to you for any:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(a) indirect, incidental, special, consequential, or punitive damages; or", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "(b) any loss of profits or revenues (whether incurred directly or indirectly), loss of data, use, goodwill, or intangible losses, even if Carets has been advised or should be aware of the possibility of any such losses arising."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "The maximum liability of Carets and its affiliates to you will not in aggregate exceed the greater of $250 usd or the amounts paid by you to Carets, if any, within the 2 months before the claim arose."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Term and Termination", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "These Terms will commence upon your first submission of an ad and will continue until terminated by either party."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "You may terminate these Terms at any time by removing your ads and ceasing to use our platform."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "We may terminate these Terms at any time and for any reason, with or without notice."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Governing Law and Dispute Resolution", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "These Terms will be governed by and construed in accordance with the laws of the State of Utah, without regard to its conflict of laws principles."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "The laws of the state of Utah will govern these Terms and any claims and disputes (whether contractual or otherwise) arising out of or relating to these Terms or their subject matter."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "mt-1"
  }, "Entire Agreement", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "listAplhaCap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, "These Terms constitute the entire agreement between Advertiser and Carets with respect to advertising on the Carets platform."))))));
});

/***/ }),
/* 50 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);
/* harmony import */ var _components_user__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(18);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(14);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _helpers_basePath__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(16);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(108);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_7__);






 //import video from 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4';

var baseURL = _helpers_basePath__WEBPACK_IMPORTED_MODULE_6__[/* basePath */ "a"]; //"http://carets_web.test/api/v1/videos/singleWeb/";

 // Get ID from URL

/* harmony default export */ __webpack_exports__["default"] = (function () {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    username: "",
    tags: [],
    music: "",
    videoUrl: "",
    imageUrl: ""
  }),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      videoData = _useState2[0],
      setVideoData = _useState2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var video_id = new URLSearchParams(location.search).get("video_id");
    console.log(video_id);
    var newurl = baseURL + "videos/singleWeb/" + video_id;
    axios__WEBPACK_IMPORTED_MODULE_5___default.a.get(newurl).then(function (response) {
      setVideoData(response.data);
    });
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, videoData.username), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:image",
    content: videoData.imageUrl
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:image:secure_url",
    content: videoData.imageUrl
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:image:type",
    content: "image/gif"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:image:width",
    content: "400"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:image:height",
    content: "300"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:video",
    content: videoData.videoUrl
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:video:secure_url",
    content: videoData.videoUrl
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:video:type",
    content: "video/mp4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:video:width",
    content: "1200"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:video:height",
    content: "630"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:type",
    content: "video"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:title",
    content: videoData.username
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:description",
    content: videoData.tags[0]
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "customContainer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    isCenter: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-14"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_user__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    data: videoData
  }))));
});

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(0));

var _reactStatic = __webpack_require__(10);

var _router = __webpack_require__(12);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var _default = function _default(_ref) {
  var _ref$RouterProps = _ref.RouterProps,
      userRouterProps = _ref$RouterProps === void 0 ? {} : _ref$RouterProps;
  return {
    Root: function Root(PreviousRoot) {
      return function (_ref2) {
        var children = _ref2.children,
            rest = _objectWithoutProperties(_ref2, ["children"]);

        var basepath = (0, _reactStatic.useBasepath)();
        var staticInfo = (0, _reactStatic.useStaticInfo)();

        var RouteHandler = function RouteHandler(props) {
          return /*#__PURE__*/_react["default"].createElement(PreviousRoot, _extends({}, rest, props), children);
        };

        var renderedChildren =
        /*#__PURE__*/
        // Place a top-level router inside the root
        // This will give proper context to Link and other reach components
        _react["default"].createElement(_router.Router, _extends({}, basepath ? {
          basepath: basepath
        } : {}, userRouterProps), /*#__PURE__*/_react["default"].createElement(RouteHandler, {
          path: "/*"
        })); // If we're in SSR, use a manual server location


        return typeof document === 'undefined' ? /*#__PURE__*/_react["default"].createElement(_router.ServerLocation, {
          url: (0, _reactStatic.makePathAbsolute)("".concat(basepath, "/").concat(staticInfo.path))
        }, renderedChildren) : renderedChildren;
      };
    },
    Routes: function Routes(PreviousRoutes) {
      return function (props) {
        return (
          /*#__PURE__*/
          // Wrap Routes in location
          _react["default"].createElement(_router.Location, null, function (location) {
            return /*#__PURE__*/_react["default"].createElement(PreviousRoutes, _extends({
              path: "/*"
            }, props, {
              location: location
            }));
          })
        );
      };
    }
  };
};

exports["default"] = _default;

/***/ }),
/* 52 */
/***/ (function(module, exports) {

module.exports = require("react-hot-loader");

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/footer_logo.8e47b3e5.svg";

/***/ }),
/* 54 */
/***/ (function(module, exports) {

module.exports = require("react-modal");

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/caretLogo.7fe17bbb.svg";

/***/ }),
/* 56 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sbnM6c3ZnanM9Imh0dHA6Ly9zdmdqcy5jb20vc3ZnanMiIHZlcnNpb249IjEuMSIgd2lkdGg9IjUxMiIgaGVpZ2h0PSI1MTIiIHg9IjAiIHk9IjAiIHZpZXdCb3g9IjAgMCA1MTIuMDAzIDUxMi4wMDMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KICA8Zz4NCjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0zNTEuOTgsMGMtMjcuMjk2LDEuODg4LTU5LjIsMTkuMzYtNzcuNzkyLDQyLjExMmMtMTYuOTYsMjAuNjQtMzAuOTEyLDUxLjI5Ni0yNS40NzIsODEuMDg4ICAgIGMyOS44MjQsMC45MjgsNjAuNjQtMTYuOTYsNzguNDk2LTQwLjA5NkMzNDMuOTE2LDYxLjU2OCwzNTYuNTU2LDMxLjEwNCwzNTEuOTgsMHoiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiLz4NCgk8L2c+DQo8L2c+DQo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPg0KCTxnPg0KCQk8cGF0aCBkPSJNNDU5Ljg1MiwxNzEuNzc2Yy0yNi4yMDgtMzIuODY0LTYzLjA0LTUxLjkzNi05Ny44MjQtNTEuOTM2Yy00NS45MiwwLTY1LjM0NCwyMS45ODQtOTcuMjQ4LDIxLjk4NCAgICBjLTMyLjg5NiwwLTU3Ljg4OC0yMS45Mi05Ny42LTIxLjkyYy0zOS4wMDgsMC04MC41NDQsMjMuODQtMTA2Ljg4LDY0LjYwOGMtMzcuMDI0LDU3LjQwOC0zMC42ODgsMTY1LjM0NCwyOS4zMTIsMjU3LjI4ICAgIGMyMS40NzIsMzIuODk2LDUwLjE0NCw2OS44ODgsODcuNjQ4LDcwLjIwOGMzMy4zNzYsMC4zMiw0Mi43ODQtMjEuNDA4LDg4LTIxLjYzMmM0NS4yMTYtMC4yNTYsNTMuNzkyLDIxLjkyLDg3LjEwNCwyMS41NjggICAgYzM3LjUzNi0wLjI4OCw2Ny43NzYtNDEuMjgsODkuMjQ4LTc0LjE3NmMxNS4zOTItMjMuNTg0LDIxLjEyLTM1LjQ1NiwzMy4wNTYtNjIuMDggICAgQzM4Ny44NTIsMzQyLjYyNCwzNzMuOTMyLDIxOS4xNjgsNDU5Ljg1MiwxNzEuNzc2eiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiIvPg0KCTwvZz4NCjwvZz4NCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCjwvZz4NCjwvc3ZnPg0K"

/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDUxMS45OTkgNTExLjk5OSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTExLjk5OSA1MTEuOTk5OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojMzJCQkZGOyIgZD0iTTM4Mi4zNjksMTc1LjYyM0MzMjIuODkxLDE0Mi4zNTYsMjI3LjQyNyw4OC45MzcsNzkuMzU1LDYuMDI4DQoJCUM2OS4zNzItMC41NjUsNTcuODg2LTEuNDI5LDQ3Ljk2MiwxLjkzbDI1NC4wNSwyNTQuMDVMMzgyLjM2OSwxNzUuNjIzeiIvPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiMzMkJCRkY7IiBkPSJNNDcuOTYyLDEuOTNjLTEuODYsMC42My0zLjY3LDEuMzktNS40MDEsMi4zMDhDMzEuNjAyLDEwLjE2NiwyMy41NDksMjEuNTczLDIzLjU0OSwzNnY0MzkuOTYNCgkJYzAsMTQuNDI3LDguMDUyLDI1LjgzNCwxOS4wMTIsMzEuNzYxYzEuNzI4LDAuOTE3LDMuNTM3LDEuNjgsNS4zOTUsMi4zMTRMMzAyLjAxMiwyNTUuOThMNDcuOTYyLDEuOTN6Ii8+DQoJPHBhdGggc3R5bGU9ImZpbGw6IzMyQkJGRjsiIGQ9Ik0zMDIuMDEyLDI1NS45OEw0Ny45NTYsNTEwLjAzNWM5LjkyNywzLjM4NCwyMS40MTMsMi41ODYsMzEuMzk5LTQuMTAzDQoJCWMxNDMuNTk4LTgwLjQxLDIzNy45ODYtMTMzLjE5NiwyOTguMTUyLTE2Ni43NDZjMS42NzUtMC45NDEsMy4zMTYtMS44NjEsNC45MzgtMi43NzJMMzAyLjAxMiwyNTUuOTh6Ii8+DQo8L2c+DQo8cGF0aCBzdHlsZT0iZmlsbDojMkM5RkQ5OyIgZD0iTTIzLjU0OSwyNTUuOTh2MjE5Ljk4YzAsMTQuNDI3LDguMDUyLDI1LjgzNCwxOS4wMTIsMzEuNzYxYzEuNzI4LDAuOTE3LDMuNTM3LDEuNjgsNS4zOTUsMi4zMTQNCglMMzAyLjAxMiwyNTUuOThIMjMuNTQ5eiIvPg0KPHBhdGggc3R5bGU9ImZpbGw6IzI5Q0M1RTsiIGQ9Ik03OS4zNTUsNi4wMjhDNjcuNS0xLjgsNTMuNTItMS41NzcsNDIuNTYxLDQuMjM5bDI1NS41OTUsMjU1LjU5Nmw4NC4yMTItODQuMjEyDQoJQzMyMi44OTEsMTQyLjM1NiwyMjcuNDI3LDg4LjkzNyw3OS4zNTUsNi4wMjh6Ii8+DQo8cGF0aCBzdHlsZT0iZmlsbDojRDkzRjIxOyIgZD0iTTI5OC4xNTgsMjUyLjEyNkw0Mi41NjEsNTA3LjcyMWMxMC45Niw1LjgxNSwyNC45MzksNi4xNTEsMzYuNzk0LTEuNzg5DQoJYzE0My41OTgtODAuNDEsMjM3Ljk4Ni0xMzMuMTk2LDI5OC4xNTItMTY2Ljc0NmMxLjY3NS0wLjk0MSwzLjMxNi0xLjg2MSw0LjkzOC0yLjc3MkwyOTguMTU4LDI1Mi4xMjZ6Ii8+DQo8cGF0aCBzdHlsZT0iZmlsbDojRkZENTAwOyIgZD0iTTQ4OC40NSwyNTUuOThjMC0xMi4xOS02LjE1MS0yNC40OTItMTguMzQyLTMxLjMxNGMwLDAtMjIuNzk5LTEyLjcyMS05Mi42ODItNTEuODA5bC04My4xMjMsODMuMTIzDQoJbDgzLjIwNCw4My4yMDVjNjkuMTE2LTM4LjgwNyw5Mi42LTUxLjg5Miw5Mi42LTUxLjg5MkM0ODIuMjk5LDI4MC40NzIsNDg4LjQ1LDI2OC4xNyw0ODguNDUsMjU1Ljk4eiIvPg0KPHBhdGggc3R5bGU9ImZpbGw6I0ZGQUEwMDsiIGQ9Ik00NzAuMTA4LDI4Ny4yOTRjMTIuMTkxLTYuODIyLDE4LjM0Mi0xOS4xMjQsMTguMzQyLTMxLjMxNEgyOTQuMzAzbDgzLjIwNCw4My4yMDUNCglDNDQ2LjYyNCwzMDAuMzc5LDQ3MC4xMDgsMjg3LjI5NCw0NzAuMTA4LDI4Ny4yOTR6Ii8+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/instruct-1.3ded7d55.png";

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/instruct-2.f4b7eca9.png";

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/instruct-3.a513e9f3.png";

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/delete-icon.da4c31fc.svg";

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/11.ff9bac8d.png";

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/22.441459e6.png";

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/33.8839b90f.png";

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/44.2ce99a99.png";

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(67);
__webpack_require__(69);
module.exports = __webpack_require__(76);


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* eslint-disable import/no-dynamic-require */

var plugins = __webpack_require__(68)["default"];

var _require = __webpack_require__(27),
    registerPlugins = _require.registerPlugins;

registerPlugins(plugins);

if (false) {}

/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _react_static_root_node_modules_react_static_plugin_reach_router_browser_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(51);
/* harmony import */ var _react_static_root_node_modules_react_static_plugin_reach_router_browser_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_react_static_root_node_modules_react_static_plugin_reach_router_browser_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports
 // Plugins

var plugins = [{
  location: "__react_static_root__/node_modules/react-static-plugin-tailwindcss",
  plugins: [],
  hooks: {}
}, {
  location: "__react_static_root__/node_modules/react-static-plugin-source-filesystem",
  plugins: [],
  hooks: {}
}, {
  location: "__react_static_root__/node_modules/react-static-plugin-reach-router",
  plugins: [],
  hooks: _react_static_root_node_modules_react_static_plugin_reach_router_browser_api_js__WEBPACK_IMPORTED_MODULE_0___default()({})
}, {
  location: "__react_static_root__/node_modules/react-static-plugin-sitemap/dist",
  plugins: [],
  hooks: {}
}, {
  location: "__react_static_root__/",
  plugins: [],
  hooks: {}
}]; // Export em!

/* harmony default export */ __webpack_exports__["default"] = (plugins);

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/* eslint-disable import/no-dynamic-require */

var _require = __webpack_require__(27),
    registerTemplates = _require.registerTemplates;

var _require2 = __webpack_require__(70),
    templates = _require2["default"],
    notFoundTemplate = _require2.notFoundTemplate;

registerTemplates(templates, notFoundTemplate);

if (false) {}

/***/ }),
/* 70 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__dirname) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notFoundTemplate", function() { return notFoundTemplate; });
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_universal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7);
/* harmony import */ var react_universal_component__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_universal_component__WEBPACK_IMPORTED_MODULE_3__);


























Object(react_universal_component__WEBPACK_IMPORTED_MODULE_3__["setHasBabelPlugin"])();
var universalOptions = {
  loading: function loading() {
    return null;
  },
  error: function error(props) {
    console.error(props.error);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", null, "An error occurred loading this page's template. More information is available in the console.");
  },
  ignoreBabelRename: true
};
var t_0 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/404.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/404 */).then(__webpack_require__.bind(null, 39))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/404.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(39);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/404";
  }
}), universalOptions);
t_0.template = '__react_static_root__/src/pages/404.js';
var t_1 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/about.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/about */).then(__webpack_require__.bind(null, 40))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/about.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(40);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/about";
  }
}), universalOptions);
t_1.template = '__react_static_root__/src/pages/about.js';
var t_2 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/clmTerms.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/clmTerms */).then(__webpack_require__.bind(null, 41))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/clmTerms.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(41);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/clmTerms";
  }
}), universalOptions);
t_2.template = '__react_static_root__/src/pages/clmTerms.js';
var t_3 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/clmTutorials.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/clmTutorials */).then(__webpack_require__.bind(null, 42))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/clmTutorials.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(42);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/clmTutorials";
  }
}), universalOptions);
t_3.template = '__react_static_root__/src/pages/clmTutorials.js';
var t_4 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/community-guidelines.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/community-guidelines */).then(__webpack_require__.bind(null, 43))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/community-guidelines.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(43);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/community-guidelines";
  }
}), universalOptions);
t_4.template = '__react_static_root__/src/pages/community-guidelines.js';
var t_5 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/deleteInstructions.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/deleteInstructions */).then(__webpack_require__.bind(null, 44))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/deleteInstructions.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(44);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/deleteInstructions";
  }
}), universalOptions);
t_5.template = '__react_static_root__/src/pages/deleteInstructions.js';
var t_6 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/help.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/help */).then(__webpack_require__.bind(null, 45))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/help.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(45);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/help";
  }
}), universalOptions);
t_6.template = '__react_static_root__/src/pages/help.js';
var t_7 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/index.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/index */).then(__webpack_require__.bind(null, 46))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/index.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(46);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/index";
  }
}), universalOptions);
t_7.template = '__react_static_root__/src/pages/index.js';
var t_8 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/introduction.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/introduction */).then(__webpack_require__.bind(null, 47))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/introduction.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(47);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/introduction";
  }
}), universalOptions);
t_8.template = '__react_static_root__/src/pages/introduction.js';
var t_9 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/legal.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/legal */).then(__webpack_require__.bind(null, 48))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/legal.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(48);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/legal";
  }
}), universalOptions);
t_9.template = '__react_static_root__/src/pages/legal.js';
var t_10 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/terms.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/terms */).then(__webpack_require__.bind(null, 49))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/terms.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(49);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/terms";
  }
}), universalOptions);
t_10.template = '__react_static_root__/src/pages/terms.js';
var t_11 = react_universal_component__WEBPACK_IMPORTED_MODULE_3___default()(babel_plugin_universal_import_universalImport__WEBPACK_IMPORTED_MODULE_1___default()({
  id: "__react_static_root__/src/pages/videos.js",
  load: function load() {
    return Promise.all([Promise.resolve(/* import() | __react_static_root__/src/pages/videos */).then(__webpack_require__.bind(null, 50))]).then(function (proms) {
      return proms[0];
    });
  },
  path: function path() {
    return path__WEBPACK_IMPORTED_MODULE_0___default.a.join(__dirname, '__react_static_root__/src/pages/videos.js');
  },
  resolve: function resolve() {
    return /*require.resolve*/(50);
  },
  chunkName: function chunkName() {
    return "__react_static_root__/src/pages/videos";
  }
}), universalOptions);
t_11.template = '__react_static_root__/src/pages/videos.js'; // Template Map

/* harmony default export */ __webpack_exports__["default"] = ({
  '__react_static_root__/src/pages/404.js': t_0,
  '__react_static_root__/src/pages/about.js': t_1,
  '__react_static_root__/src/pages/clmTerms.js': t_2,
  '__react_static_root__/src/pages/clmTutorials.js': t_3,
  '__react_static_root__/src/pages/community-guidelines.js': t_4,
  '__react_static_root__/src/pages/deleteInstructions.js': t_5,
  '__react_static_root__/src/pages/help.js': t_6,
  '__react_static_root__/src/pages/index.js': t_7,
  '__react_static_root__/src/pages/introduction.js': t_8,
  '__react_static_root__/src/pages/legal.js': t_9,
  '__react_static_root__/src/pages/terms.js': t_10,
  '__react_static_root__/src/pages/videos.js': t_11
}); // Not Found Template

var notFoundTemplate = "__react_static_root__/src/pages/404.js";
/* WEBPACK VAR INJECTION */}.call(this, "/"))

/***/ }),
/* 71 */
/***/ (function(module, exports) {

module.exports = function (module) {
  if (!module.webpackPolyfill) {
    module.deprecate = function () {};

    module.paths = []; // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function get() {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function get() {
        return module.i;
      }
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.clearChunks = exports.flushModuleIds = exports.flushChunkNames = exports.MODULE_IDS = exports.CHUNK_NAMES = undefined;

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

exports["default"] = requireUniversalModule;

var _utils = __webpack_require__(21);

var CHUNK_NAMES = exports.CHUNK_NAMES = new Set();
var MODULE_IDS = exports.MODULE_IDS = new Set();

function requireUniversalModule(universalConfig, options, props, prevProps) {
  var key = options.key,
      _options$timeout = options.timeout,
      timeout = _options$timeout === undefined ? 15000 : _options$timeout,
      onLoad = options.onLoad,
      onError = options.onError,
      isDynamic = options.isDynamic,
      modCache = options.modCache,
      promCache = options.promCache,
      usesBabelPlugin = options.usesBabelPlugin;
  var config = getConfig(isDynamic, universalConfig, options, props);
  var chunkName = config.chunkName,
      path = config.path,
      resolve = config.resolve,
      load = config.load;
  var asyncOnly = !path && !resolve || typeof chunkName === 'function';

  var requireSync = function requireSync(props) {
    var exp = (0, _utils.loadFromCache)(chunkName, props, modCache);

    if (!exp) {
      var mod = void 0;

      if (!(0, _utils.isWebpack)() && path) {
        var modulePath = (0, _utils.callForString)(path, props) || '';
        mod = (0, _utils.tryRequire)(modulePath);
      } else if ((0, _utils.isWebpack)() && resolve) {
        var weakId = (0, _utils.callForString)(resolve, props);

        if (__webpack_require__.m[weakId]) {
          mod = (0, _utils.tryRequire)(weakId);
        }
      }

      if (mod) {
        exp = (0, _utils.resolveExport)(mod, key, onLoad, chunkName, props, modCache, true);
      }
    }

    return exp;
  };

  var requireAsync = function requireAsync(props) {
    var exp = (0, _utils.loadFromCache)(chunkName, props, modCache);
    if (exp) return Promise.resolve(exp);
    var cachedPromise = (0, _utils.loadFromPromiseCache)(chunkName, props, promCache);
    if (cachedPromise) return cachedPromise;
    var prom = new Promise(function (res, rej) {
      var reject = function reject(error) {
        error = error || new Error('timeout exceeded');
        clearTimeout(timer);

        if (onError) {
          var _isServer = typeof window === 'undefined';

          var info = {
            isServer: _isServer
          };
          onError(error, info);
        }

        rej(error);
      }; // const timer = timeout && setTimeout(reject, timeout)


      var timer = timeout && setTimeout(reject, timeout);

      var resolve = function resolve(mod) {
        clearTimeout(timer);
        var exp = (0, _utils.resolveExport)(mod, key, onLoad, chunkName, props, modCache);
        if (exp) return res(exp);
        reject(new Error('export not found'));
      };

      var request = load(props, {
        resolve: resolve,
        reject: reject
      }); // if load doesn't return a promise, it must call resolveImport
      // itself. Most common is the promise implementation below.

      if (!request || typeof request.then !== 'function') return;
      request.then(resolve)["catch"](reject);
    });
    (0, _utils.cacheProm)(prom, chunkName, props, promCache);
    return prom;
  };

  var addModule = function addModule(props) {
    if (_utils.isServer || _utils.isTest) {
      if (chunkName) {
        var name = (0, _utils.callForString)(chunkName, props);

        if (usesBabelPlugin) {
          // if ignoreBabelRename is true, don't apply regex
          var shouldKeepName = options && !!options.ignoreBabelRename;

          if (!shouldKeepName) {
            name = name.replace(/\//g, '-');
          }
        }

        if (name) CHUNK_NAMES.add(name);
        if (!_utils.isTest) return name; // makes tests way smaller to run both kinds
      }

      if ((0, _utils.isWebpack)()) {
        var weakId = (0, _utils.callForString)(resolve, props);
        if (weakId) MODULE_IDS.add(weakId);
        return weakId;
      }

      if (!(0, _utils.isWebpack)()) {
        var modulePath = (0, _utils.callForString)(path, props);
        if (modulePath) MODULE_IDS.add(modulePath);
        return modulePath;
      }
    }
  };

  var shouldUpdate = function shouldUpdate(next, prev) {
    var cacheKey = (0, _utils.callForString)(chunkName, next);
    var config = getConfig(isDynamic, universalConfig, options, prev);
    var prevCacheKey = (0, _utils.callForString)(config.chunkName, prev);
    return cacheKey !== prevCacheKey;
  };

  return {
    requireSync: requireSync,
    requireAsync: requireAsync,
    addModule: addModule,
    shouldUpdate: shouldUpdate,
    asyncOnly: asyncOnly
  };
}

var flushChunkNames = exports.flushChunkNames = function flushChunkNames() {
  var chunks = Array.from(CHUNK_NAMES);
  CHUNK_NAMES.clear();
  return chunks;
};

var flushModuleIds = exports.flushModuleIds = function flushModuleIds() {
  var ids = Array.from(MODULE_IDS);
  MODULE_IDS.clear();
  return ids;
};

var clearChunks = exports.clearChunks = function clearChunks() {
  CHUNK_NAMES.clear();
  MODULE_IDS.clear();
};

var getConfig = function getConfig(isDynamic, universalConfig, options, props) {
  if (isDynamic) {
    var resultingConfig = typeof universalConfig === 'function' ? universalConfig(props) : universalConfig;

    if (options) {
      resultingConfig = _extends({}, resultingConfig, options);
    }

    return resultingConfig;
  }

  var load = typeof universalConfig === 'function' ? universalConfig : // $FlowIssue
  function () {
    return universalConfig;
  };
  return {
    file: 'default',
    id: options.id || 'default',
    chunkName: options.chunkName || 'default',
    resolve: options.resolve || '',
    path: options.path || '',
    load: load,
    ignoreBabelRename: true
  };
};

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	".": 9,
	"./": 9,
	"./index": 9,
	"./index.js": 9
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 73;

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = __webpack_require__(20);

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(28);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _context = __webpack_require__(29);

var _context2 = _interopRequireDefault(_context);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (_typeof(call) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + _typeof(superClass));
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var ReportChunks = function (_React$Component) {
  _inherits(ReportChunks, _React$Component);

  function ReportChunks(props) {
    _classCallCheck(this, ReportChunks);

    var _this = _possibleConstructorReturn(this, (ReportChunks.__proto__ || Object.getPrototypeOf(ReportChunks)).call(this, props));

    _this.state = {
      report: props.report
    };
    return _this;
  }

  _createClass(ReportChunks, [{
    key: 'render',
    value: function render() {
      return _react2["default"].createElement(_context2["default"].Provider, {
        value: this.state
      }, this.props.children);
    }
  }]);

  return ReportChunks;
}(_react2["default"].Component);

ReportChunks.propTypes = {
  report: _propTypes2["default"].func.isRequired
};
exports["default"] = ReportChunks;

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.__handleAfter = exports.__update = undefined;

var _hoistNonReactStatics = __webpack_require__(30);

var _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics);

var _index = __webpack_require__(7);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var __update = exports.__update = function __update(props, state, isInitialized) {
  var isMount = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var isSync = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  var isServer = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
  if (!isInitialized) return state;

  if (!state.error) {
    state.error = null;
  }

  return __handleAfter(props, state, isMount, isSync, isServer);
};
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["__handleAfter"] }] */


var __handleAfter = exports.__handleAfter = function __handleAfter(props, state, isMount, isSync, isServer) {
  var mod = state.mod,
      error = state.error;

  if (mod && !error) {
    (0, _hoistNonReactStatics2["default"])(_index2["default"], mod, {
      preload: true,
      preloadWeak: true
    });

    if (props.onAfter) {
      var onAfter = props.onAfter;
      var info = {
        isMount: isMount,
        isSync: isSync,
        isServer: isServer
      };
      onAfter(info, mod);
    }
  } else if (error && props.onError) {
    props.onError(error);
  }

  return state;
};

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(31);

var _interopRequireWildcard = __webpack_require__(32);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var React = _interopRequireWildcard(__webpack_require__(0));

var _useStaticInfo = __webpack_require__(77);

var _Suspense = _interopRequireDefault(__webpack_require__(78));
/* eslint-disable import/no-dynamic-require */
// Override the suspense module to be our own
// This is expected to break when using preact
// In order to make it work with preact 10, use `patch-package` to remove those 2 lines
// Reference: https://github.com/react-static/react-static/pull/1500


React.Suspense = _Suspense["default"];
React["default"].Suspense = _Suspense["default"];

var App = __webpack_require__(81)["default"];

var _default = function _default(staticInfo) {
  return function (props) {
    return /*#__PURE__*/React.createElement(_useStaticInfo.staticInfoContext.Provider, {
      value: staticInfo
    }, /*#__PURE__*/React.createElement(App, props));
  };
};

exports["default"] = _default;

/***/ }),
/* 77 */
/***/ (function(module, exports) {

module.exports = require("D:\\laragon\\www\\carets_web\\carets_web\\node_modules\\react-static\\lib\\browser\\hooks\\useStaticInfo");

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(32);

var _interopRequireDefault = __webpack_require__(31);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(79));

var _objectWithoutProperties2 = _interopRequireDefault(__webpack_require__(80));

var React = _interopRequireWildcard(__webpack_require__(0));

var OriginalSuspense = React.Suspense;

function Suspense(_ref) {
  var key = _ref.key,
      children = _ref.children,
      rest = (0, _objectWithoutProperties2["default"])(_ref, ["key", "children"]);
  return typeof document !== 'undefined' ? /*#__PURE__*/React.createElement(OriginalSuspense, (0, _extends2["default"])({
    key: key
  }, rest), children) : /*#__PURE__*/React.createElement(React.Fragment, {
    key: key
  }, children);
}

var _default = Suspense;
exports["default"] = _default;

/***/ }),
/* 79 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/extends");

/***/ }),
/* 80 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/objectWithoutProperties");

/***/ }),
/* 81 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(22);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_hot_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(52);
/* harmony import */ var react_hot_loader__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_hot_loader__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(19);


 // Your top level component

 // Export your top level component as JSX (for static rendering)

/* harmony default export */ __webpack_exports__["default"] = (_App__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"]); // Render your app

if (typeof document !== 'undefined') {
  var target = document.getElementById('root');
  var renderMethod = target.hasChildNodes() ? react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.hydrate : react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render;

  var render = function render(Comp) {
    renderMethod( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_hot_loader__WEBPACK_IMPORTED_MODULE_2__["AppContainer"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Comp, null)), target);
  }; // Render!


  render(_App__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"]); // Hot Module Replacement

  if (module && module.hot) {
    module.hot.accept('./App', function () {
      render(_App__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"]);
    });
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(82)(module)))

/***/ }),
/* 82 */
/***/ (function(module, exports) {

module.exports = function (originalModule) {
  if (!originalModule.webpackPolyfill) {
    var module = Object.create(originalModule); // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function get() {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function get() {
        return module.i;
      }
    });
    Object.defineProperty(module, "exports", {
      enumerable: true
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// Module
exports.push([module.i, "/*! tailwindcss v2.2.17 | MIT License | https://tailwindcss.com */\n\n/*! modern-normalize v1.1.0 | MIT License | https://github.com/sindresorhus/modern-normalize */\n\n/*\nDocument\n========\n*/\n\n/**\nUse a better box model (opinionated).\n*/\n\n*,\n::before,\n::after {\n  box-sizing: border-box;\n}\n\n/**\nUse a more readable tab size (opinionated).\n*/\n\nhtml {\n  -moz-tab-size: 4;\n  -o-tab-size: 4;\n     tab-size: 4;\n}\n\n/**\n1. Correct the line height in all browsers.\n2. Prevent adjustments of font size after orientation changes in iOS.\n*/\n\nhtml {\n  line-height: 1.15; /* 1 */\n  -webkit-text-size-adjust: 100%; /* 2 */\n}\n\n/*\nSections\n========\n*/\n\n/**\nRemove the margin in all browsers.\n*/\n\nbody {\n  margin: 0;\n}\n\n/**\nImprove consistency of default fonts in all browsers. (https://github.com/sindresorhus/modern-normalize/issues/3)\n*/\n\nbody {\n  font-family:\n\t\tsystem-ui,\n\t\t-apple-system, /* Firefox supports this but not yet `system-ui` */\n\t\t'Segoe UI',\n\t\tRoboto,\n\t\tHelvetica,\n\t\tArial,\n\t\tsans-serif,\n\t\t'Apple Color Emoji',\n\t\t'Segoe UI Emoji';\n}\n\n/*\nGrouping content\n================\n*/\n\n/**\n1. Add the correct height in Firefox.\n2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)\n*/\n\nhr {\n  height: 0; /* 1 */\n  color: inherit; /* 2 */\n}\n\n/*\nText-level semantics\n====================\n*/\n\n/**\nAdd the correct text decoration in Chrome, Edge, and Safari.\n*/\n\nabbr[title] {\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n}\n\n/**\nAdd the correct font weight in Edge and Safari.\n*/\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\n/**\n1. Improve consistency of default fonts in all browsers. (https://github.com/sindresorhus/modern-normalize/issues/3)\n2. Correct the odd 'em' font sizing in all browsers.\n*/\n\ncode,\nkbd,\nsamp,\npre {\n  font-family:\n\t\tui-monospace,\n\t\tSFMono-Regular,\n\t\tConsolas,\n\t\t'Liberation Mono',\n\t\tMenlo,\n\t\tmonospace; /* 1 */\n  font-size: 1em; /* 2 */\n}\n\n/**\nAdd the correct font size in all browsers.\n*/\n\nsmall {\n  font-size: 80%;\n}\n\n/**\nPrevent 'sub' and 'sup' elements from affecting the line height in all browsers.\n*/\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n/*\nTabular data\n============\n*/\n\n/**\n1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)\n2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)\n*/\n\ntable {\n  text-indent: 0; /* 1 */\n  border-color: inherit; /* 2 */\n}\n\n/*\nForms\n=====\n*/\n\n/**\n1. Change the font styles in all browsers.\n2. Remove the margin in Firefox and Safari.\n*/\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit; /* 1 */\n  font-size: 100%; /* 1 */\n  line-height: 1.15; /* 1 */\n  margin: 0; /* 2 */\n}\n\n/**\nRemove the inheritance of text transform in Edge and Firefox.\n1. Remove the inheritance of text transform in Firefox.\n*/\n\nbutton,\nselect { /* 1 */\n  text-transform: none;\n}\n\n/**\nCorrect the inability to style clickable types in iOS and Safari.\n*/\n\nbutton {\n  -webkit-appearance: button;\n}\n\n/**\nRemove the inner border and padding in Firefox.\n*/\n\n/**\nRestore the focus styles unset by the previous rule.\n*/\n\n/**\nRemove the additional ':invalid' styles in Firefox.\nSee: https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737\n*/\n\n/**\nRemove the padding so developers are not caught out when they zero out 'fieldset' elements in all browsers.\n*/\n\nlegend {\n  padding: 0;\n}\n\n/**\nAdd the correct vertical alignment in Chrome and Firefox.\n*/\n\nprogress {\n  vertical-align: baseline;\n}\n\n/**\nCorrect the cursor style of increment and decrement buttons in Safari.\n*/\n\n/**\n1. Correct the odd appearance in Chrome and Safari.\n2. Correct the outline style in Safari.\n*/\n\n/**\nRemove the inner padding in Chrome and Safari on macOS.\n*/\n\n/**\n1. Correct the inability to style clickable types in iOS and Safari.\n2. Change font properties to 'inherit' in Safari.\n*/\n\n/*\nInteractive\n===========\n*/\n\n/*\nAdd the correct display in Chrome and Safari.\n*/\n\nsummary {\n  display: list-item;\n}\n\n/**\n * Manually forked from SUIT CSS Base: https://github.com/suitcss/base\n * A thin layer on top of normalize.css that provides a starting point more\n * suitable for web applications.\n */\n\n/**\n * Removes the default spacing and border for appropriate elements.\n */\n\nblockquote,\ndl,\ndd,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\nhr,\nfigure,\np,\npre {\n  margin: 0;\n}\n\nbutton {\n  background-color: transparent;\n  background-image: none;\n}\n\nfieldset {\n  margin: 0;\n  padding: 0;\n}\n\nol,\nul {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n}\n\n/**\n * Tailwind custom reset styles\n */\n\n/**\n * 1. Use the user's configured `sans` font-family (with Tailwind's default\n *    sans-serif font stack as a fallback) as a sane default.\n * 2. Use Tailwind's default \"normal\" line-height so the user isn't forced\n *    to override it to ensure consistency even when using the default theme.\n */\n\nhtml {\n  font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; /* 1 */\n  line-height: 1.5; /* 2 */\n}\n\n/**\n * Inherit font-family and line-height from `html` so users can set them as\n * a class directly on the `html` element.\n */\n\nbody {\n  font-family: inherit;\n  line-height: inherit;\n}\n\n/**\n * 1. Prevent padding and border from affecting element width.\n *\n *    We used to set this in the html element and inherit from\n *    the parent element for everything else. This caused issues\n *    in shadow-dom-enhanced elements like <details> where the content\n *    is wrapped by a div with box-sizing set to `content-box`.\n *\n *    https://github.com/mozdevs/cssremedy/issues/4\n *\n *\n * 2. Allow adding a border to an element by just adding a border-width.\n *\n *    By default, the way the browser specifies that an element should have no\n *    border is by setting it's border-style to `none` in the user-agent\n *    stylesheet.\n *\n *    In order to easily add borders to elements by just setting the `border-width`\n *    property, we change the default border-style for all elements to `solid`, and\n *    use border-width to hide them instead. This way our `border` utilities only\n *    need to set the `border-width` property instead of the entire `border`\n *    shorthand, making our border utilities much more straightforward to compose.\n *\n *    https://github.com/tailwindcss/tailwindcss/pull/116\n */\n\n*,\n::before,\n::after {\n  box-sizing: border-box; /* 1 */\n  border-width: 0; /* 2 */\n  border-style: solid; /* 2 */\n  border-color: currentColor; /* 2 */\n}\n\n/*\n * Ensure horizontal rules are visible by default\n */\n\nhr {\n  border-top-width: 1px;\n}\n\n/**\n * Undo the `border-style: none` reset that Normalize applies to images so that\n * our `border-{width}` utilities have the expected effect.\n *\n * The Normalize reset is unnecessary for us since we default the border-width\n * to 0 on all elements.\n *\n * https://github.com/tailwindcss/tailwindcss/issues/362\n */\n\nimg {\n  border-style: solid;\n}\n\ntextarea {\n  resize: vertical;\n}\n\ninput::-moz-placeholder, textarea::-moz-placeholder {\n  opacity: 1;\n  color: #9ca3af;\n}\n\ninput:-ms-input-placeholder, textarea:-ms-input-placeholder {\n  opacity: 1;\n  color: #9ca3af;\n}\n\ninput::placeholder,\ntextarea::placeholder {\n  opacity: 1;\n  color: #9ca3af;\n}\n\nbutton {\n  cursor: pointer;\n}\n\n/**\n * Override legacy focus reset from Normalize with modern Firefox focus styles.\n *\n * This is actually an improvement over the new defaults in Firefox in our testing,\n * as it triggers the better focus styles even for links, which still use a dotted\n * outline in Firefox by default.\n */\n\ntable {\n  border-collapse: collapse;\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-size: inherit;\n  font-weight: inherit;\n}\n\n/**\n * Reset links to optimize for opt-in styling instead of\n * opt-out.\n */\n\na {\n  color: inherit;\n  text-decoration: inherit;\n}\n\n/**\n * Reset form element properties that are easy to forget to\n * style explicitly so you don't inadvertently introduce\n * styles that deviate from your design system. These styles\n * supplement a partial reset that is already applied by\n * normalize.css.\n */\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  padding: 0;\n  line-height: inherit;\n  color: inherit;\n}\n\n/**\n * Use the configured 'mono' font family for elements that\n * are expected to be rendered with a monospace font, falling\n * back to the system monospace stack if there is no configured\n * 'mono' font family.\n */\n\npre,\ncode,\nkbd,\nsamp {\n  font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;\n}\n\n/**\n * 1. Make replaced elements `display: block` by default as that's\n *    the behavior you want almost all of the time. Inspired by\n *    CSS Remedy, with `svg` added as well.\n *\n *    https://github.com/mozdevs/cssremedy/issues/14\n * \n * 2. Add `vertical-align: middle` to align replaced elements more\n *    sensibly by default when overriding `display` by adding a\n *    utility like `inline`.\n *\n *    This can trigger a poorly considered linting error in some\n *    tools but is included by design.\n * \n *    https://github.com/jensimmons/cssremedy/issues/14#issuecomment-634934210\n */\n\nimg,\nsvg,\nvideo,\ncanvas,\naudio,\niframe,\nembed,\nobject {\n  display: block; /* 1 */\n  vertical-align: middle; /* 2 */\n}\n\n/**\n * Constrain images and videos to the parent width and preserve\n * their intrinsic aspect ratio.\n *\n * https://github.com/mozdevs/cssremedy/issues/14\n */\n\nimg,\nvideo {\n  max-width: 100%;\n  height: auto;\n}\n\n/**\n * Ensure the default browser behavior of the `hidden` attribute.\n */\n\n[hidden] {\n  display: none;\n}\n\n*, ::before, ::after {\n  --tw-border-opacity: 1;\n  border-color: rgba(229, 231, 235, var(--tw-border-opacity));\n}\n\n.container {\n  width: 100%;\n}\n\n@media (min-width: 640px) {\n  .container {\n    max-width: 640px;\n  }\n}\n\n@media (min-width: 768px) {\n  .container {\n    max-width: 768px;\n  }\n}\n\n@media (min-width: 1024px) {\n  .container {\n    max-width: 1024px;\n  }\n}\n\n@media (min-width: 1280px) {\n  .container {\n    max-width: 1280px;\n  }\n}\n\n@media (min-width: 1536px) {\n  .container {\n    max-width: 1536px;\n  }\n}\n\n.fixed {\n  position: fixed;\n}\n\n.absolute {\n  position: absolute;\n}\n\n.relative {\n  position: relative;\n}\n\n.mx-2 {\n  margin-left: 0.5rem;\n  margin-right: 0.5rem;\n}\n\n.mx-3 {\n  margin-left: 0.75rem;\n  margin-right: 0.75rem;\n}\n\n.mx-auto {\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.mx-2\\.5 {\n  margin-left: 0.625rem;\n  margin-right: 0.625rem;\n}\n\n.mt-1 {\n  margin-top: 0.25rem;\n}\n\n.mt-2 {\n  margin-top: 0.5rem;\n}\n\n.mt-3 {\n  margin-top: 0.75rem;\n}\n\n.mt-4 {\n  margin-top: 1rem;\n}\n\n.mt-5 {\n  margin-top: 1.25rem;\n}\n\n.mt-6 {\n  margin-top: 1.5rem;\n}\n\n.mt-8 {\n  margin-top: 2rem;\n}\n\n.mt-9 {\n  margin-top: 2.25rem;\n}\n\n.mt-10 {\n  margin-top: 2.5rem;\n}\n\n.mt-14 {\n  margin-top: 3.5rem;\n}\n\n.mt-16 {\n  margin-top: 4rem;\n}\n\n.mt-auto {\n  margin-top: auto;\n}\n\n.-mr-4 {\n  margin-right: -1rem;\n}\n\n.mb-5 {\n  margin-bottom: 1.25rem;\n}\n\n.mb-6 {\n  margin-bottom: 1.5rem;\n}\n\n.mb-8 {\n  margin-bottom: 2rem;\n}\n\n.mb-16 {\n  margin-bottom: 4rem;\n}\n\n.ml-0 {\n  margin-left: 0px;\n}\n\n.ml-1 {\n  margin-left: 0.25rem;\n}\n\n.ml-2 {\n  margin-left: 0.5rem;\n}\n\n.ml-auto {\n  margin-left: auto;\n}\n\n.block {\n  display: block;\n}\n\n.inline-block {\n  display: inline-block;\n}\n\n.flex {\n  display: flex;\n}\n\n.inline-flex {\n  display: inline-flex;\n}\n\n.table {\n  display: table;\n}\n\n.grid {\n  display: grid;\n}\n\n.inline-grid {\n  display: inline-grid;\n}\n\n.hidden {\n  display: none;\n}\n\n.h-6 {\n  height: 1.5rem;\n}\n\n.h-10 {\n  height: 2.5rem;\n}\n\n.h-full {\n  height: 100%;\n}\n\n.max-h-96 {\n  max-height: 24rem;\n}\n\n.w-32 {\n  width: 8rem;\n}\n\n.w-56 {\n  width: 14rem;\n}\n\n.w-full {\n  width: 100%;\n}\n\n.max-w-sm {\n  max-width: 24rem;\n}\n\n.max-w-5xl {\n  max-width: 64rem;\n}\n\n.max-w-screen-lg {\n  max-width: 1024px;\n}\n\n.transform {\n  --tw-translate-x: 0;\n  --tw-translate-y: 0;\n  --tw-rotate: 0;\n  --tw-skew-x: 0;\n  --tw-skew-y: 0;\n  --tw-scale-x: 1;\n  --tw-scale-y: 1;\n  transform: translateX(var(--tw-translate-x)) translateY(var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));\n}\n\n.scale-100 {\n  --tw-scale-x: 1;\n  --tw-scale-y: 1;\n}\n\n.hover\\:scale-105:hover {\n  --tw-scale-x: 1.05;\n  --tw-scale-y: 1.05;\n}\n\n@-webkit-keyframes spin {\n  to {\n    transform: rotate(360deg);\n  }\n}\n\n@keyframes spin {\n  to {\n    transform: rotate(360deg);\n  }\n}\n\n@-webkit-keyframes ping {\n  75%, 100% {\n    transform: scale(2);\n    opacity: 0;\n  }\n}\n\n@keyframes ping {\n  75%, 100% {\n    transform: scale(2);\n    opacity: 0;\n  }\n}\n\n@-webkit-keyframes pulse {\n  50% {\n    opacity: .5;\n  }\n}\n\n@keyframes pulse {\n  50% {\n    opacity: .5;\n  }\n}\n\n@-webkit-keyframes bounce {\n  0%, 100% {\n    transform: translateY(-25%);\n    -webkit-animation-timing-function: cubic-bezier(0.8,0,1,1);\n            animation-timing-function: cubic-bezier(0.8,0,1,1);\n  }\n\n  50% {\n    transform: none;\n    -webkit-animation-timing-function: cubic-bezier(0,0,0.2,1);\n            animation-timing-function: cubic-bezier(0,0,0.2,1);\n  }\n}\n\n@keyframes bounce {\n  0%, 100% {\n    transform: translateY(-25%);\n    -webkit-animation-timing-function: cubic-bezier(0.8,0,1,1);\n            animation-timing-function: cubic-bezier(0.8,0,1,1);\n  }\n\n  50% {\n    transform: none;\n    -webkit-animation-timing-function: cubic-bezier(0,0,0.2,1);\n            animation-timing-function: cubic-bezier(0,0,0.2,1);\n  }\n}\n\n.cursor-pointer {\n  cursor: pointer;\n}\n\n.list-none {\n  list-style-type: none;\n}\n\n.grid-flow-col-dense {\n  grid-auto-flow: column dense;\n}\n\n.grid-cols-1 {\n  grid-template-columns: repeat(1, minmax(0, 1fr));\n}\n\n.grid-cols-2 {\n  grid-template-columns: repeat(2, minmax(0, 1fr));\n}\n\n.grid-cols-3 {\n  grid-template-columns: repeat(3, minmax(0, 1fr));\n}\n\n.grid-cols-4 {\n  grid-template-columns: repeat(4, minmax(0, 1fr));\n}\n\n.flex-col {\n  flex-direction: column;\n}\n\n.flex-wrap {\n  flex-wrap: wrap;\n}\n\n.items-end {\n  align-items: flex-end;\n}\n\n.items-center {\n  align-items: center;\n}\n\n.justify-center {\n  justify-content: center;\n}\n\n.justify-between {\n  justify-content: space-between;\n}\n\n.gap-2 {\n  gap: 0.5rem;\n}\n\n.gap-4 {\n  gap: 1rem;\n}\n\n.gap-6 {\n  gap: 1.5rem;\n}\n\n.gap-8 {\n  gap: 2rem;\n}\n\n.gap-10 {\n  gap: 2.5rem;\n}\n\n.justify-self-center {\n  justify-self: center;\n}\n\n.overflow-hidden {\n  overflow: hidden;\n}\n\n.whitespace-nowrap {\n  white-space: nowrap;\n}\n\n.rounded-full {\n  border-radius: 9999px;\n}\n\n.border {\n  border-width: 1px;\n}\n\n.border-t {\n  border-top-width: 1px;\n}\n\n.border-b {\n  border-bottom-width: 1px;\n}\n\n.border-gray-100 {\n  --tw-border-opacity: 1;\n  border-color: rgba(243, 244, 246, var(--tw-border-opacity));\n}\n\n.border-gray-300 {\n  --tw-border-opacity: 1;\n  border-color: rgba(209, 213, 219, var(--tw-border-opacity));\n}\n\n.bg-gray-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgba(243, 244, 246, var(--tw-bg-opacity));\n}\n\n.bg-gray-200 {\n  --tw-bg-opacity: 1;\n  background-color: rgba(229, 231, 235, var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-200:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgba(229, 231, 235, var(--tw-bg-opacity));\n}\n\n.object-cover {\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.p-0 {\n  padding: 0px;\n}\n\n.px-4 {\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n\n.px-5 {\n  padding-left: 1.25rem;\n  padding-right: 1.25rem;\n}\n\n.px-6 {\n  padding-left: 1.5rem;\n  padding-right: 1.5rem;\n}\n\n.px-7 {\n  padding-left: 1.75rem;\n  padding-right: 1.75rem;\n}\n\n.px-8 {\n  padding-left: 2rem;\n  padding-right: 2rem;\n}\n\n.py-3 {\n  padding-top: 0.75rem;\n  padding-bottom: 0.75rem;\n}\n\n.py-4 {\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n}\n\n.py-5 {\n  padding-top: 1.25rem;\n  padding-bottom: 1.25rem;\n}\n\n.py-7 {\n  padding-top: 1.75rem;\n  padding-bottom: 1.75rem;\n}\n\n.py-8 {\n  padding-top: 2rem;\n  padding-bottom: 2rem;\n}\n\n.py-12 {\n  padding-top: 3rem;\n  padding-bottom: 3rem;\n}\n\n.py-16 {\n  padding-top: 4rem;\n  padding-bottom: 4rem;\n}\n\n.pt-2 {\n  padding-top: 0.5rem;\n}\n\n.pt-4 {\n  padding-top: 1rem;\n}\n\n.pt-8 {\n  padding-top: 2rem;\n}\n\n.pb-4 {\n  padding-bottom: 1rem;\n}\n\n.text-center {\n  text-align: center;\n}\n\n.text-xs {\n  font-size: 0.75rem;\n  line-height: 1rem;\n}\n\n.text-sm {\n  font-size: 0.875rem;\n  line-height: 1.25rem;\n}\n\n.text-lg {\n  font-size: 1.125rem;\n  line-height: 1.75rem;\n}\n\n.text-xl {\n  font-size: 1.25rem;\n  line-height: 1.75rem;\n}\n\n.text-2xl {\n  font-size: 1.5rem;\n  line-height: 2rem;\n}\n\n.text-3xl {\n  font-size: 1.875rem;\n  line-height: 2.25rem;\n}\n\n.text-4xl {\n  font-size: 2.25rem;\n  line-height: 2.5rem;\n}\n\n.font-medium {\n  font-weight: 500;\n}\n\n.font-semibold {\n  font-weight: 600;\n}\n\n.font-bold {\n  font-weight: 700;\n}\n\n.leading-none {\n  line-height: 1;\n}\n\n.leading-normal {\n  line-height: 1.5;\n}\n\n.leading-relaxed {\n  line-height: 1.625;\n}\n\n.text-white {\n  --tw-text-opacity: 1;\n  color: rgba(255, 255, 255, var(--tw-text-opacity));\n}\n\n.text-gray-800 {\n  --tw-text-opacity: 1;\n  color: rgba(31, 41, 55, var(--tw-text-opacity));\n}\n\n.text-red-600 {\n  --tw-text-opacity: 1;\n  color: rgba(220, 38, 38, var(--tw-text-opacity));\n}\n\n.hover\\:text-blue-800:hover {\n  --tw-text-opacity: 1;\n  color: rgba(30, 64, 175, var(--tw-text-opacity));\n}\n\n.hover\\:underline:hover {\n  text-decoration: underline;\n}\n\n*, ::before, ::after {\n  --tw-shadow: 0 0 #0000;\n}\n\n*, ::before, ::after {\n  --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);\n  --tw-ring-offset-width: 0px;\n  --tw-ring-offset-color: #fff;\n  --tw-ring-color: rgba(59, 130, 246, 0.5);\n  --tw-ring-offset-shadow: 0 0 #0000;\n  --tw-ring-shadow: 0 0 #0000;\n}\n\n.transition-all {\n  transition-property: all;\n  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n  transition-duration: 150ms;\n}\n\n.transition {\n  transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform, filter, -webkit-backdrop-filter;\n  transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter;\n  transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter, -webkit-backdrop-filter;\n  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n  transition-duration: 150ms;\n}\n\n.duration-500 {\n  transition-duration: 500ms;\n}\n\n.ease-in-out {\n  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n}\n\n@media (min-width: 640px) {\n  .sm\\:text-left {\n    text-align: left;\n  }\n}\n\n@media (min-width: 768px) {\n  .md\\:-mr-8 {\n    margin-right: -2rem;\n  }\n\n  .md\\:grid-cols-2 {\n    grid-template-columns: repeat(2, minmax(0, 1fr));\n  }\n\n  .md\\:grid-cols-3 {\n    grid-template-columns: repeat(3, minmax(0, 1fr));\n  }\n\n  .md\\:grid-cols-4 {\n    grid-template-columns: repeat(4, minmax(0, 1fr));\n  }\n\n  .md\\:flex-nowrap {\n    flex-wrap: nowrap;\n  }\n\n  .md\\:justify-end {\n    justify-content: flex-end;\n  }\n\n  .md\\:justify-between {\n    justify-content: space-between;\n  }\n\n  .md\\:px-24 {\n    padding-left: 6rem;\n    padding-right: 6rem;\n  }\n\n  .md\\:text-left {\n    text-align: left;\n  }\n\n  .md\\:text-3xl {\n    font-size: 1.875rem;\n    line-height: 2.25rem;\n  }\n\n  .md\\:text-4xl {\n    font-size: 2.25rem;\n    line-height: 2.5rem;\n  }\n\n  .md\\:text-5xl {\n    font-size: 3rem;\n    line-height: 1;\n  }\n}\n\n@media (min-width: 1024px) {\n  .lg\\:ml-10 {\n    margin-left: 2.5rem;\n  }\n\n  .lg\\:ml-24 {\n    margin-left: 6rem;\n  }\n\n  .lg\\:w-auto {\n    width: auto;\n  }\n\n  .lg\\:grid-cols-2 {\n    grid-template-columns: repeat(2, minmax(0, 1fr));\n  }\n\n  .lg\\:grid-cols-4 {\n    grid-template-columns: repeat(4, minmax(0, 1fr));\n  }\n\n  .lg\\:items-start {\n    align-items: flex-start;\n  }\n\n  .lg\\:px-0 {\n    padding-left: 0px;\n    padding-right: 0px;\n  }\n}\n\n@media (min-width: 1280px) {\n}\n\n@media (min-width: 1536px) {\n  .\\32xl\\:text-base {\n    font-size: 1rem;\n    line-height: 1.5rem;\n  }\n}", ""]);



/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// Imports
var urlEscape = __webpack_require__(33);
var ___CSS_LOADER_URL___0___ = urlEscape(__webpack_require__(34));
var ___CSS_LOADER_URL___1___ = urlEscape(__webpack_require__(34) + "?#iefix");
var ___CSS_LOADER_URL___2___ = urlEscape(__webpack_require__(85));
var ___CSS_LOADER_URL___3___ = urlEscape(__webpack_require__(86));
var ___CSS_LOADER_URL___4___ = urlEscape(__webpack_require__(87));
var ___CSS_LOADER_URL___5___ = urlEscape(__webpack_require__(88));
var ___CSS_LOADER_URL___6___ = urlEscape(__webpack_require__(89));
var ___CSS_LOADER_URL___7___ = urlEscape(__webpack_require__(35));
var ___CSS_LOADER_URL___8___ = urlEscape(__webpack_require__(35) + "?#iefix");
var ___CSS_LOADER_URL___9___ = urlEscape(__webpack_require__(90));
var ___CSS_LOADER_URL___10___ = urlEscape(__webpack_require__(91));
var ___CSS_LOADER_URL___11___ = urlEscape(__webpack_require__(92));
var ___CSS_LOADER_URL___12___ = urlEscape(__webpack_require__(93));
var ___CSS_LOADER_URL___13___ = urlEscape(__webpack_require__(94));
var ___CSS_LOADER_URL___14___ = urlEscape(__webpack_require__(36));
var ___CSS_LOADER_URL___15___ = urlEscape(__webpack_require__(36) + "?#iefix");
var ___CSS_LOADER_URL___16___ = urlEscape(__webpack_require__(95));
var ___CSS_LOADER_URL___17___ = urlEscape(__webpack_require__(96));
var ___CSS_LOADER_URL___18___ = urlEscape(__webpack_require__(97));
var ___CSS_LOADER_URL___19___ = urlEscape(__webpack_require__(98));
var ___CSS_LOADER_URL___20___ = urlEscape(__webpack_require__(99));
var ___CSS_LOADER_URL___21___ = urlEscape(__webpack_require__(37));
var ___CSS_LOADER_URL___22___ = urlEscape(__webpack_require__(37) + "?#iefix");
var ___CSS_LOADER_URL___23___ = urlEscape(__webpack_require__(100));
var ___CSS_LOADER_URL___24___ = urlEscape(__webpack_require__(101));
var ___CSS_LOADER_URL___25___ = urlEscape(__webpack_require__(102));
var ___CSS_LOADER_URL___26___ = urlEscape(__webpack_require__(103));
var ___CSS_LOADER_URL___27___ = urlEscape(__webpack_require__(104));

// Module
exports.push([module.i, "/* purgecss start ignore */\n\n@font-face{\n  font-family:\"Orkney\";\n\n  src:url(" + ___CSS_LOADER_URL___0___ + ");\n\n  src:url(" + ___CSS_LOADER_URL___1___ + ") format(\"embedded-opentype\"),url(" + ___CSS_LOADER_URL___2___ + ") format(\"opentype\"),url(" + ___CSS_LOADER_URL___3___ + ") format(\"svg\"),url(" + ___CSS_LOADER_URL___4___ + ") format(\"truetype\"),url(" + ___CSS_LOADER_URL___5___ + ") format(\"woff\"),url(" + ___CSS_LOADER_URL___6___ + ") format(\"woff2\");\n\n  font-weight:normal;\n\n  font-style:normal;\n\n  font-display:swap\n}\n\n@font-face{\n  font-family:\"Orkney\";\n\n  src:url(" + ___CSS_LOADER_URL___7___ + ");\n\n  src:url(" + ___CSS_LOADER_URL___8___ + ") format(\"embedded-opentype\"),url(" + ___CSS_LOADER_URL___9___ + ") format(\"opentype\"),url(" + ___CSS_LOADER_URL___10___ + ") format(\"svg\"),url(" + ___CSS_LOADER_URL___11___ + ") format(\"truetype\"),url(" + ___CSS_LOADER_URL___12___ + ") format(\"woff\"),url(" + ___CSS_LOADER_URL___13___ + ") format(\"woff2\");\n\n  font-weight:600;\n\n  font-style:normal;\n\n  font-display:swap\n}\n\n@font-face{\n  font-family:\"Orkney\";\n\n  src:url(" + ___CSS_LOADER_URL___14___ + ");\n\n  src:url(" + ___CSS_LOADER_URL___15___ + ") format(\"embedded-opentype\"),url(" + ___CSS_LOADER_URL___16___ + ") format(\"opentype\"),url(" + ___CSS_LOADER_URL___17___ + ") format(\"svg\"),url(" + ___CSS_LOADER_URL___18___ + ") format(\"truetype\"),url(" + ___CSS_LOADER_URL___19___ + ") format(\"woff\"),url(" + ___CSS_LOADER_URL___20___ + ") format(\"woff2\");\n\n  font-weight:300;\n\n  font-style:normal;\n\n  font-display:swap\n}\n\n@font-face{\n  font-family:\"Orkney\";\n\n  src:url(" + ___CSS_LOADER_URL___21___ + ");\n\n  src:url(" + ___CSS_LOADER_URL___22___ + ") format(\"embedded-opentype\"),url(" + ___CSS_LOADER_URL___23___ + ") format(\"opentype\"),url(" + ___CSS_LOADER_URL___24___ + ") format(\"svg\"),url(" + ___CSS_LOADER_URL___25___ + ") format(\"truetype\"),url(" + ___CSS_LOADER_URL___26___ + ") format(\"woff\"),url(" + ___CSS_LOADER_URL___27___ + ") format(\"woff2\");\n\n  font-weight:700;\n\n  font-style:normal;\n\n  font-display:swap\n}\n\n/* purgecss end ignore */\r\n", ""]);



/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Regular.8b8f662d.otf";

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Regular.84bf429d.svg";

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Regular.b17f6d20.ttf";

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Regular.09b97bbe.woff";

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Regular.57cc320c.woff2";

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Medium.fb2c6273.otf";

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Medium.0dc7b370.svg";

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Medium.a4ce302f.ttf";

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Medium.150faef5.woff";

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Medium.77332fd4.woff2";

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Light.b08414c4.otf";

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Light.1f61e924.svg";

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Light.41772214.ttf";

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Light.b0049f84.woff";

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Light.1aaf27fc.woff2";

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Bold.21e13c23.otf";

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Bold.4ebecf05.svg";

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Bold.11a5ccc4.ttf";

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Bold.7c89d20e.woff";

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/Orkney-Bold.e033e3cc.woff2";

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// Module
exports.push([module.i, "/* purgecss start ignore */\n\n.themeOrange{\n  color:#ffc90e\n}\n\n.themeDarkBlue{\n  color:#050038\n}\n\n.bodyColor{\n  color:#444646\n}\n\n*{\n  scroll-behavior:smooth\n}\n\nbody,html{\n  font-family:\"Orkney\",\"HelveticaNeue-Light\",\"Helvetica Neue Light\",\"Helvetica Neue\",Helvetica,Arial,\"Lucida Grande\",sans-serif;\n  font-weight:300;\n  font-size:1vw;\n  margin:0;\n  padding:0;\n  color:#444646\n}\n\n@media(max-width: 1280px){\n  body,html{\n    font-size:14px\n  }\n}\n\n@media(max-width: 1024px){\n  body,html{\n    font-size:16px\n  }\n}\n\nimg{\n  max-width:100%\n}\n\n.customContainer{\n  width:100;\n  max-width:80vw;\n  margin:0 auto\n}\n\n@media(max-width: 767px){\n  .customContainer{\n    max-width:90vw\n  }\n}\n\n@media(max-width: 639px){\n  .customContainer{\n    max-width:95vw\n  }\n}\n\n.topBar{\n  background-color:#ffc90e;\n  min-height:.7rem\n}\n\n.socialIcons li a:hover img:first-child{\n  display:block;\n  -webkit-animation:hoverEfffect .3s ease-out;\n          animation:hoverEfffect .3s ease-out\n}\n\n@-webkit-keyframes hoverEfffect{\n  from{\n    opacity:.7;\n    transform:scale(1)\n  }\n\n  to{\n    opacity:0;\n    transform:scale(2)\n  }\n}\n\n@keyframes hoverEfffect{\n  from{\n    opacity:.7;\n    transform:scale(1)\n  }\n\n  to{\n    opacity:0;\n    transform:scale(2)\n  }\n}\n\n.playBtn{\n  background-color:rgba(255,201,14,.5);\n  transform:rotate(-90deg);\n  outline:none !important;\n  padding:2rem 1rem;\n  position:absolute;\n  width:5rem;\n  height:7rem;\n  top:0;\n  bottom:0;\n  left:0;\n  right:0;\n  margin:auto\n}\n\n.playBtn img{\n  max-width:100%;\n  height:100%\n}\n\n.playBtn:hover{\n  background-color:rgba(255,201,14,.7)\n}\n\ndiv.faqToggle{\n  color:#ecf0f1;\n  background:#ffc90e;\n  width:2rem;\n  height:2rem;\n  border:0;\n  font-size:1.5em;\n  position:relative\n}\n\ndiv.faqToggle span{\n  position:absolute;\n  transition:.3s;\n  background:#fff;\n  border-radius:2px\n}\n\ndiv.faqToggle span:first-of-type{\n  top:25%;\n  bottom:25%;\n  width:10%;\n  left:45%\n}\n\ndiv.faqToggle span:last-of-type{\n  left:25%;\n  right:25%;\n  height:10%;\n  top:45%\n}\n\ndiv.faqToggle.open span:first-of-type,div.faqToggle.open span:last-of-type{\n  transform:rotate(90deg)\n}\n\ndiv.faqToggle.open span:last-of-type{\n  left:50%;\n  right:50%\n}\n\n.animFaq{\n  -webkit-animation:animFaq .4s ease;\n          animation:animFaq .4s ease\n}\n\n@-webkit-keyframes animFaq{\n  from{\n    max-height:0\n  }\n\n  to{\n    max-height:500px\n  }\n}\n\n@keyframes animFaq{\n  from{\n    max-height:0\n  }\n\n  to{\n    max-height:500px\n  }\n}\n\n.termsList{\n  font-size:1.1rem;\n  counter-reset:section;\n  line-height:1.7\n}\n\n.termsList>li{\n  position:relative;\n  counter-increment:section;\n  padding-left:2.5rem;\n  margin-top:2.5rem\n}\n\n.termsList>li>span{\n  font-size:1.5rem;\n  font-weight:500;\n  color:#050038\n}\n\n.termsList>li::before{\n  content:counter(section);\n  position:absolute;\n  top:0;\n  left:0;\n  display:block;\n  text-align:center;\n  height:1.7rem;\n  width:1.7rem;\n  color:#fff;\n  background-color:#050038;\n  line-height:1.85em;\n  border-radius:100%;\n  font-weight:600;\n  margin-top:3px\n}\n\n.termsList>li>ul li{\n  padding-left:1.5rem;\n  position:relative;\n  margin-top:.5rem\n}\n\n.termsList>li>ul li+li{\n  margin-top:1rem\n}\n\n.termsList>li>ul li::before{\n  content:\"\";\n  margin-top:6px;\n  position:absolute;\n  display:block;\n  top:0;\n  left:0;\n  background:#1e5799;\n  background:linear-gradient(135deg, #1e5799 0%, #2989d8 50%, #207cca 51%, #7db9e8 100%);\n  filter:progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#1e5799\", endColorstr=\"#7db9e8\",GradientType=1 );\n  height:.7rem;\n  width:.7rem;\n  transform:rotate(45deg);\n  border-radius:30%\n}\n\n.termsList>li>ul li>ul li::before{\n  background:#b7deed;\n  background:linear-gradient(135deg, #b7deed 0%, #71ceef 50%, #21b4e2 51%, #b7deed 100%);\n  filter:progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#b7deed\", endColorstr=\"#b7deed\",GradientType=1 );\n  border-radius:100%\n}\n\n.termsList u{\n  color:#ffc90e\n}\n\nul.styledList li{\n  padding-left:1.5rem;\n  position:relative;\n  margin-top:.5rem\n}\n\nul.styledList li+li{\n  margin-top:1rem\n}\n\nul.styledList li::before{\n  content:\"\";\n  margin-top:6px;\n  position:absolute;\n  display:block;\n  top:0;\n  left:0;\n  background:#1e5799;\n  background:linear-gradient(135deg, #1e5799 0%, #2989d8 50%, #207cca 51%, #7db9e8 100%);\n  filter:progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#1e5799\", endColorstr=\"#7db9e8\",GradientType=1 );\n  height:.7rem;\n  width:.7rem;\n  transform:rotate(45deg);\n  border-radius:30%\n}\n\nul.styledList li>ul li::before{\n  background:#b7deed;\n  background:linear-gradient(135deg, #b7deed 0%, #71ceef 50%, #21b4e2 51%, #b7deed 100%);\n  filter:progid:DXImageTransform.Microsoft.gradient( startColorstr=\"#b7deed\", endColorstr=\"#b7deed\",GradientType=1 );\n  border-radius:100%\n}\n\n.capitalLetter li::first-letter{\n  text-transform:uppercase\n}\n\n.modal_close{\n  background-color:#e42a61;\n  border-radius:5px;\n  outline:none !important;\n  color:#fff;\n  padding:7px 10px;\n  padding-top:12px;\n  font-size:1.2em;\n  width:200px;\n  transition:.2s all\n}\n\n.modal_close:hover{\n  background-color:#cc426b\n}\n\n.reset_msg{\n  font-size:2em;\n  text-align:center;\n  font-weight:500\n}\n\n.reset_msg.success{\n  color:#a5dc86\n}\n\n.reset_msg.error{\n  color:#f27474\n}\n\n.f-modal-alert .f-modal-icon{\n  border-radius:50%;\n  border:4px solid gray;\n  box-sizing:content-box;\n  height:80px;\n  margin:20px auto;\n  padding:0;\n  position:relative;\n  width:80px\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success,.f-modal-alert .f-modal-icon.f-modal-error{\n  border-color:#a5dc86\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success:after,.f-modal-alert .f-modal-icon.f-modal-success:before,.f-modal-alert .f-modal-icon.f-modal-error:after,.f-modal-alert .f-modal-icon.f-modal-error:before{\n  background:#fff;\n  content:\"\";\n  height:120px;\n  position:absolute;\n  transform:rotate(45deg);\n  width:60px\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success:before,.f-modal-alert .f-modal-icon.f-modal-error:before{\n  border-radius:120px 0 0 120px;\n  left:-33px;\n  top:-7px;\n  transform-origin:60px 60px;\n  transform:rotate(-45deg)\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success:after,.f-modal-alert .f-modal-icon.f-modal-error:after{\n  border-radius:0 120px 120px 0;\n  left:30px;\n  top:-11px;\n  transform-origin:0 60px;\n  transform:rotate(-45deg)\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success .f-modal-placeholder,.f-modal-alert .f-modal-icon.f-modal-error .f-modal-placeholder{\n  border-radius:50%;\n  border:4px solid rgba(165,220,134,.2);\n  box-sizing:content-box;\n  height:80px;\n  left:-4px;\n  position:absolute;\n  top:-4px;\n  width:80px;\n  z-index:2\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success .f-modal-fix,.f-modal-alert .f-modal-icon.f-modal-error .f-modal-fix{\n  background-color:#fff;\n  height:90px;\n  left:28px;\n  position:absolute;\n  top:8px;\n  transform:rotate(-45deg);\n  width:5px;\n  z-index:1\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success .f-modal-line,.f-modal-alert .f-modal-icon.f-modal-error .f-modal-line{\n  background-color:#a5dc86;\n  border-radius:2px;\n  display:block;\n  height:5px;\n  position:absolute;\n  z-index:2\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success .f-modal-line.f-modal-tip,.f-modal-alert .f-modal-icon.f-modal-error .f-modal-line.f-modal-tip{\n  left:14px;\n  top:46px;\n  transform:rotate(45deg);\n  width:25px\n}\n\n.f-modal-alert .f-modal-icon.f-modal-success .f-modal-line.f-modal-long,.f-modal-alert .f-modal-icon.f-modal-error .f-modal-line.f-modal-long{\n  right:8px;\n  top:38px;\n  transform:rotate(-45deg);\n  width:47px\n}\n\n.f-modal-alert .f-modal-icon.f-modal-error{\n  border-color:#f27474\n}\n\n.f-modal-alert .f-modal-icon.f-modal-error .f-modal-x-mark{\n  display:block;\n  position:relative;\n  z-index:2\n}\n\n.f-modal-alert .f-modal-icon.f-modal-error .f-modal-placeholder{\n  border:4px solid rgba(200,0,0,.2)\n}\n\n.f-modal-alert .f-modal-icon.f-modal-error .f-modal-line{\n  background-color:#f27474;\n  top:37px;\n  width:47px\n}\n\n.f-modal-alert .f-modal-icon.f-modal-error .f-modal-line.f-modal-left{\n  left:17px;\n  transform:rotate(45deg)\n}\n\n.f-modal-alert .f-modal-icon.f-modal-error .f-modal-line.f-modal-right{\n  right:16px;\n  transform:rotate(-45deg)\n}\n\n.f-modal-alert .f-modal-icon.f-modal-warning{\n  border-color:#f8bb86\n}\n\n.f-modal-alert .f-modal-icon.f-modal-warning:before{\n  -webkit-animation:pulseWarning 2s linear infinite;\n          animation:pulseWarning 2s linear infinite;\n  background-color:#fff;\n  border-radius:50%;\n  content:\"\";\n  display:inline-block;\n  height:100%;\n  opacity:0;\n  position:absolute;\n  width:100%\n}\n\n.f-modal-alert .f-modal-icon.f-modal-warning:after{\n  background-color:#fff;\n  border-radius:50%;\n  content:\"\";\n  display:block;\n  height:100%;\n  position:absolute;\n  width:100%;\n  z-index:1\n}\n\n.f-modal-alert .f-modal-icon.f-modal-warning .f-modal-body{\n  background-color:#f8bb86;\n  border-radius:2px;\n  height:47px;\n  left:50%;\n  margin-left:-2px;\n  position:absolute;\n  top:10px;\n  width:5px;\n  z-index:2\n}\n\n.f-modal-alert .f-modal-icon.f-modal-warning .f-modal-dot{\n  background-color:#f8bb86;\n  border-radius:50%;\n  bottom:10px;\n  height:7px;\n  left:50%;\n  margin-left:-3px;\n  position:absolute;\n  width:7px;\n  z-index:2\n}\n\n.f-modal-alert .f-modal-icon+.f-modal-icon{\n  margin-top:50px\n}\n\n.animateSuccessTip{\n  -webkit-animation:animateSuccessTip .75s;\n          animation:animateSuccessTip .75s\n}\n\n.animateSuccessLong{\n  -webkit-animation:animateSuccessLong .75s;\n          animation:animateSuccessLong .75s\n}\n\n.f-modal-icon.f-modal-success.animate:after{\n  -webkit-animation:rotatePlaceholder 4.25s ease-in;\n          animation:rotatePlaceholder 4.25s ease-in\n}\n\n.f-modal-icon.f-modal-error.animate:after{\n  -webkit-animation:rotatePlaceholder 4.25s ease-in;\n          animation:rotatePlaceholder 4.25s ease-in\n}\n\n.animateErrorIcon{\n  -webkit-animation:animateErrorIcon .5s;\n          animation:animateErrorIcon .5s\n}\n\n.animateXLeft{\n  -webkit-animation:animateXLeft .75s;\n          animation:animateXLeft .75s\n}\n\n.animateXRight{\n  -webkit-animation:animateXRight .75s;\n          animation:animateXRight .75s\n}\n\n.scaleWarning{\n  -webkit-animation:scaleWarning .75s infinite alternate;\n          animation:scaleWarning .75s infinite alternate\n}\n\n.pulseWarningIns{\n  -webkit-animation:pulseWarningIns .75s infinite alternate;\n          animation:pulseWarningIns .75s infinite alternate\n}\n\n@-webkit-keyframes animateSuccessTip{\n  0%,54%{\n    width:0;\n    left:1px;\n    top:19px\n  }\n\n  70%{\n    width:50px;\n    left:-8px;\n    top:37px\n  }\n\n  84%{\n    width:17px;\n    left:21px;\n    top:48px\n  }\n\n  100%{\n    width:25px;\n    left:14px;\n    top:45px\n  }\n}\n\n@keyframes animateSuccessTip{\n  0%,54%{\n    width:0;\n    left:1px;\n    top:19px\n  }\n\n  70%{\n    width:50px;\n    left:-8px;\n    top:37px\n  }\n\n  84%{\n    width:17px;\n    left:21px;\n    top:48px\n  }\n\n  100%{\n    width:25px;\n    left:14px;\n    top:45px\n  }\n}\n\n@-webkit-keyframes animateSuccessLong{\n  0%,65%{\n    width:0;\n    right:46px;\n    top:54px\n  }\n\n  84%{\n    width:55px;\n    right:0;\n    top:35px\n  }\n\n  100%{\n    width:47px;\n    right:8px;\n    top:38px\n  }\n}\n\n@keyframes animateSuccessLong{\n  0%,65%{\n    width:0;\n    right:46px;\n    top:54px\n  }\n\n  84%{\n    width:55px;\n    right:0;\n    top:35px\n  }\n\n  100%{\n    width:47px;\n    right:8px;\n    top:38px\n  }\n}\n\n@-webkit-keyframes rotatePlaceholder{\n  0%,5%{\n    transform:rotate(-45deg)\n  }\n\n  100%,12%{\n    transform:rotate(-405deg)\n  }\n}\n\n@keyframes rotatePlaceholder{\n  0%,5%{\n    transform:rotate(-45deg)\n  }\n\n  100%,12%{\n    transform:rotate(-405deg)\n  }\n}\n\n@-webkit-keyframes animateErrorIcon{\n  0%{\n    transform:rotateX(100deg);\n    opacity:0\n  }\n\n  100%{\n    transform:rotateX(0deg);\n    opacity:1\n  }\n}\n\n@keyframes animateErrorIcon{\n  0%{\n    transform:rotateX(100deg);\n    opacity:0\n  }\n\n  100%{\n    transform:rotateX(0deg);\n    opacity:1\n  }\n}\n\n@-webkit-keyframes animateXLeft{\n  0%,65%{\n    left:82px;\n    top:95px;\n    width:0\n  }\n\n  84%{\n    left:14px;\n    top:33px;\n    width:47px\n  }\n\n  100%{\n    left:17px;\n    top:37px;\n    width:47px\n  }\n}\n\n@keyframes animateXLeft{\n  0%,65%{\n    left:82px;\n    top:95px;\n    width:0\n  }\n\n  84%{\n    left:14px;\n    top:33px;\n    width:47px\n  }\n\n  100%{\n    left:17px;\n    top:37px;\n    width:47px\n  }\n}\n\n@-webkit-keyframes animateXRight{\n  0%,65%{\n    right:82px;\n    top:95px;\n    width:0\n  }\n\n  84%{\n    right:14px;\n    top:33px;\n    width:47px\n  }\n\n  100%{\n    right:16px;\n    top:37px;\n    width:47px\n  }\n}\n\n@keyframes animateXRight{\n  0%,65%{\n    right:82px;\n    top:95px;\n    width:0\n  }\n\n  84%{\n    right:14px;\n    top:33px;\n    width:47px\n  }\n\n  100%{\n    right:16px;\n    top:37px;\n    width:47px\n  }\n}\n\n@-webkit-keyframes scaleWarning{\n  0%{\n    transform:scale(1)\n  }\n\n  30%{\n    transform:scale(1.02)\n  }\n\n  100%{\n    transform:scale(1)\n  }\n}\n\n@keyframes scaleWarning{\n  0%{\n    transform:scale(1)\n  }\n\n  30%{\n    transform:scale(1.02)\n  }\n\n  100%{\n    transform:scale(1)\n  }\n}\n\n@-webkit-keyframes pulseWarning{\n  0%{\n    background-color:#fff;\n    transform:scale(1);\n    opacity:.5\n  }\n\n  30%{\n    background-color:#fff;\n    transform:scale(1);\n    opacity:.5\n  }\n\n  100%{\n    background-color:#f8bb86;\n    transform:scale(2);\n    opacity:0\n  }\n}\n\n@keyframes pulseWarning{\n  0%{\n    background-color:#fff;\n    transform:scale(1);\n    opacity:.5\n  }\n\n  30%{\n    background-color:#fff;\n    transform:scale(1);\n    opacity:.5\n  }\n\n  100%{\n    background-color:#f8bb86;\n    transform:scale(2);\n    opacity:0\n  }\n}\n\n@-webkit-keyframes pulseWarningIns{\n  0%{\n    background-color:#f8d486\n  }\n\n  100%{\n    background-color:#f8bb86\n  }\n}\n\n@keyframes pulseWarningIns{\n  0%{\n    background-color:#f8d486\n  }\n\n  100%{\n    background-color:#f8bb86\n  }\n}\n\n.ReactModal__Content{\n  box-shadow:0 0 20px rgba(0,0,0,.2)\n}\n\n.listNumber {\n  padding-left: 20px;\n  list-style: numaric;\n  margin-top: 25px;\n}\n\n.listNumber > li {\n  font-weight: 600;\n}\n\n.listAplhaCap {\n  list-style: upper-alpha;\n  padding-left: 16px;\n}\n\n.listAplha {\n  list-style: lower-alpha;\n  padding-left: 16px;\n}\n\n.listAplhaCap li,\r\n.listAplha li {\n  margin-top: 10px;\n  font-weight: normal;\n}\n\n.loginAdWrapper {\n  width: 250px;\n  height: 420px;\n}\n\n/* purgecss end ignore */\r\n", ""]);



/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// Imports
var urlEscape = __webpack_require__(33);
var ___CSS_LOADER_URL___0___ = urlEscape(__webpack_require__(107));
var ___CSS_LOADER_URL___1___ = urlEscape(__webpack_require__(38));

// Module
exports.push([module.i, "/* purgecss start ignore */\n\n.LMmainSection {\n  /* background: url(../img/licensing-module/LMT-bg.png) no-repeat center / cover; */\n  background: url(" + ___CSS_LOADER_URL___0___ + ") no-repeat center / cover;\n  padding: 2.5rem 0 !important;\n}\n\n.stepOne {\n  border: 1px dashed #707070;\n  border-radius: 1rem;\n  padding: 0.5rem 0 2rem;\n  margin-top: 3rem;\n}\n\n.stepOne.stepThree .BLGridStep figure > img {\n  width: 240px;\n  height: 280px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n}\n\n.stepOne.stepThree + .stepTitle,\r\n.stepOne.stepThree .BLGridStep {\n  display: grid;\n  grid-template-columns: repeat(2, 1fr);\n  grid-gap: 2rem;\n  gap: 2rem;\n  text-align: center;\n}\n\n.currentStep {\n  position: absolute;\n  top: -24px;\n  left: 50%;\n  transform: translateX(-50%);\n  background: #806bff;\n  color: #fff;\n  padding: 0.75rem 2.25rem;\n  border-radius: 6px;\n  font-size: 18px;\n}\n\n.BLGridStep,\r\n.stepTitle {\n  display: grid;\n  grid-template-columns: repeat(5, 1fr);\n  grid-gap: 2rem;\n  gap: 2rem;\n  text-align: center;\n}\n\n.stepTitle figcaption:before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 50%;\n  width: 1px;\n  height: 24px;\n  background: url(/linesV.svg) repeat-y 50% / contain;\n  /* background: url(/static/media/linesV.93f1ed0d.svg) repeat-y 50% / contain; */\n}\n\n.BLGridStep figure > img {\n  width: 110px;\n  height: 150px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n  padding: 0.6rem;\n  border: 1px solid #0080ff;\n  border-radius: 6px 6px 0 0;\n}\n\n.downloadApp,\r\n.publishApp,\r\n.stepOne .BLGridStep .assetVideo {\n  width: 48px;\n  background: #ffc90e;\n  height: 48px;\n  border-radius: 10rem;\n}\n\n.assetAudio,\r\n.assetPromo,\r\n.assetVideo,\r\n.assetWatermark,\r\n.downloadApp,\r\n.publishApp {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.downloadApp,\r\n.publishApp {\n  width: 100px;\n  height: 100px;\n}\n\n.btnPurple {\n  background-color: #806bff;\n  color: #fff;\n  border-radius: 6px !important;\n  width: 100%;\n  border-color: #806bff;\n  transition: 0.4s cubic-bezier(0, 0.68, 1, 0.56);\n  opacity: 1;\n  padding: 10px 0.4rem;\n}\n\n.bgDelete {\n  background: url(" + ___CSS_LOADER_URL___1___ + ") no-repeat center center /\r\n        cover;\n}\n\n@media all and (max-width: 767.98px) {\n  .BLGridStep {\n    grid-template-columns: repeat(3, 1fr);\n    row-gap: 3rem;\n    -moz-column-gap: 0.5rem;\n         column-gap: 0.5rem;\n  }\n\n  .stepOne {\n    border: none;\n    row-gap: 3rem;\n    -moz-column-gap: 1rem;\n         column-gap: 1rem;\n  }\n\n  .BLGridStep figure figcaption {\n    display: block;\n  }\n\n  .stepTitle figcaption::before {\n    content: none;\n  }\n\n  .stepTitle {\n    display: none;\n  }\n\n  .stepOne.stepThree .BLGridStep figure > img {\n    width: 160px;\n    height: 200px;\n  }\n\n  .assetVideo,\r\n    .assetWatermark,\r\n    .assetAudio,\r\n    .assetPromo {\n    top: 70px;\n  }\n\n  .downloadApp,\r\n    .publishApp {\n    top: 100px;\n  }\n}\n\n/* purgecss end ignore */\r\n", ""]);



/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/LMT-bg.f4ab1731.png";

/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ })
/******/ ]);
});