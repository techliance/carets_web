

ALTER TABLE `ad_views`
	ADD COLUMN `position` VARCHAR(255) NULL DEFAULT NULL AFTER `campaign_id`,
	ADD COLUMN `age` VARCHAR(255) NULL DEFAULT NULL AFTER `position`,
	ADD COLUMN `gender` VARCHAR(255) NULL DEFAULT NULL AFTER `age`,
	ADD COLUMN `location` VARCHAR(255) NULL DEFAULT NULL AFTER `gender`,
	ADD COLUMN `interests` VARCHAR(255) NULL DEFAULT NULL AFTER `location`,
	ADD COLUMN `complete` TINYINT(1) ZEROFILL NOT NULL AFTER `interests`,
	ADD COLUMN `clicked` TINYINT(1) ZEROFILL NOT NULL AFTER `complete`;

ALTER TABLE `user_profiles`
	ADD COLUMN `gender` VARCHAR(250) NULL DEFAULT NULL AFTER `user_video`;


    ALTER TABLE `ads`
	ADD COLUMN `ad_button_text` VARCHAR(255) NULL DEFAULT NULL AFTER `ad_share_link`,
	ADD COLUMN `ad_button_link` VARCHAR(255) NULL DEFAULT NULL AFTER `ad_button_text`;

    ALTER TABLE `ad_views`
	CHANGE COLUMN `complete` `complete` TINYINT(1) UNSIGNED ZEROFILL NULL AFTER `interests`,
	CHANGE COLUMN `clicked` `clicked` TINYINT(1) UNSIGNED ZEROFILL NULL AFTER `complete`;


##################################################################################
CREATE TABLE `genders` (
	`id` INT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;
INSERT INTO `caretsback`.`genders` (`title`, `created_at`, `updated_at`) VALUES ('Male', '2023-04-13 16:29:12', '2023-04-13 16:29:12');
INSERT INTO `caretsback`.`genders` (`title`, `created_at`, `updated_at`) VALUES ('Female', '2023-04-13 16:29:12', '2023-04-13 16:29:12');


CREATE TABLE `ad_compaigns` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NULL DEFAULT '0',
	`ad_id` BIGINT(20) UNSIGNED NULL DEFAULT '0',
	`plan` INT(11) UNSIGNED NULL DEFAULT NULL,
	`ages` INT(5) UNSIGNED NOT NULL DEFAULT '0',
	`gender` INT(5) UNSIGNED NOT NULL DEFAULT '0',
	`location` VARCHAR(255) NULL DEFAULT NULL,
	`compaign_title` VARCHAR(255) NULL DEFAULT NULL,
	`is_active` TINYINT(1) NULL DEFAULT '0',
	`is_blocked` TINYINT(1) NULL DEFAULT '0',
	`watch_count` INT(10) NULL DEFAULT '0',
	`activeDate` TIMESTAMP NULL DEFAULT NULL,
	`nextDate` TIMESTAMP NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_ad_compaigns_users` (`user_id`),
	INDEX `FK_ad_compaigns_ads` (`ad_id`),
	INDEX `FK_ad_compaigns_ad_subscription_plans` (`plan`),
	INDEX `FK_ad_compaigns_ages` (`ages`),
	INDEX `FK_ad_compaigns_genders` (`gender`),
	CONSTRAINT `FK_ad_compaigns_ad_subscription_plans` FOREIGN KEY (`plan`) REFERENCES `ad_subscription_plans` (`id`),
	CONSTRAINT `FK_ad_compaigns_ads` FOREIGN KEY (`ad_id`) REFERENCES `ads` (`id`),
	CONSTRAINT `FK_ad_compaigns_ages` FOREIGN KEY (`ages`) REFERENCES `ages` (`id`),
	CONSTRAINT `FK_ad_compaigns_genders` FOREIGN KEY (`gender`) REFERENCES `genders` (`id`),
	CONSTRAINT `FK_ad_compaigns_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=4
;

ALTER TABLE `ad_plans`
	CHANGE COLUMN `duration` `duration` VARCHAR(50) NULL DEFAULT NULL AFTER `position`;

    ALTER TABLE `ad_cards`
	ADD COLUMN `set_default` TINYINT(1) ZEROFILL NOT NULL AFTER `card_expiry`;

    ALTER TABLE `user_profiles`
	ADD COLUMN `industry_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `user_id`,
	ADD COLUMN `business_name` VARCHAR(250) NOT NULL DEFAULT '0'  AFTER `industry_id`,
	ADD COLUMN `phone_number` VARCHAR(250) NOT NULL DEFAULT '0'  AFTER `business_name`;

    ALTER TABLE `user_profiles`
	CHANGE COLUMN `industry_id` `industry_id` INT(10) UNSIGNED NULL DEFAULT '0' AFTER `user_id`,
	ADD CONSTRAINT `FK_user_profiles_ad_indusrty` FOREIGN KEY (`industry_id`) REFERENCES `ad_indusrty` (`id`);


    ALTER TABLE `user_profiles`
	CHANGE COLUMN `business_name` `business_name` VARCHAR(250) NULL DEFAULT '0' AFTER `industry_id`,
	CHANGE COLUMN `phone_number` `phone_number` VARCHAR(250) NULL DEFAULT '0' AFTER `business_name`;

###########

ALTER TABLE `ad_payments`
	ADD COLUMN `transaction_type` VARCHAR(255) NULL DEFAULT NULL AFTER `transaction_id`;

ALTER TABLE `ad_payments`
	ADD COLUMN `latest_invoice` VARCHAR(255) NULL DEFAULT NULL AFTER `transaction_id`;

