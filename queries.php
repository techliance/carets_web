ALTER TABLE videos
	ADD COLUMN is_m3u8 TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER is_allow_caretCreation;
ALTER TABLE `clm_requests`
	ADD COLUMN `order_item_id` INT(10) NULL AFTER `order_id`;

ALTER TABLE `clm_requests`
	ADD COLUMN `order_id` INT(10) NOT NULL AFTER `user_id`;

ALTER TABLE `cart_items`
	ADD COLUMN `interval_count` INT(10) NULL DEFAULT NULL AFTER `subscription_pricing_id`;
    
ALTER TABLE `clm_order_items`
	ADD COLUMN `interval_count` INT(10) NOT NULL AFTER `subscription_pricing_id`;

ALTER TABLE `clm_order_items`
	ADD COLUMN `subscription_status` VARCHAR(255) NULL DEFAULT NULL AFTER `subscription_amount`,
	ADD COLUMN `cancel_at_period_end` VARCHAR(255) NULL DEFAULT NULL AFTER `subscription_status`,
	ADD COLUMN `current_period_end` VARCHAR(255) NULL DEFAULT NULL AFTER `cancel_at_period_end`;

CREATE TABLE `clm_order_payments` (
    `id` INT(10) NOT NULL AUTO_INCREMENT,
    `order_id` INT(10) NOT NULL, -- Reference to the orders table
    `payment_method` ENUM('stripe', 'paypal', 'bank_transfer') NOT NULL, -- Payment method used
    `transaction_id` VARCHAR(255) NOT NULL, -- Unique transaction identifier from the payment gateway
    `amount` DECIMAL(20,2) NOT NULL, -- Amount paid
    `status` ENUM('pending', 'completed', 'failed', 'refunded') NOT NULL DEFAULT 'pending', -- Payment status
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

CREATE TABLE `clm_order_items` (
    `id` INT(10) NOT NULL AUTO_INCREMENT,
    `order_id` INT(10) NOT NULL, -- Reference to the orders table
    `caret_title` VARCHAR(255) NOT NULL, -- Title of the caret
    `subscription_stripe_id` VARCHAR(255) NOT NULL, -- Stripe subscription ID
    `subscription_pricing_id` INT(10) NOT NULL, -- Reference to the subscription pricing
    `subscription_amount` DECIMAL(20,2) NOT NULL, -- Amount for the subscription
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

CREATE TABLE `clm_orders` (
    `id` INT(10) NOT NULL AUTO_INCREMENT,
    `user_id` INT(10) NOT NULL, -- Reference to the user who placed the order
    `order_number` VARCHAR(50) NOT NULL UNIQUE, -- Unique order identifier
    `total_items` TINYINT(3) NOT NULL, -- Total items in the order
    `total_amount` DECIMAL(20,2) NOT NULL, -- Total amount for the order
    `order_type` ENUM('order', 'request') NOT NULL DEFAULT 'order', -- Order status
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

ALTER TABLE `videos`
	ADD COLUMN `video_export_url` VARCHAR(255) NULL DEFAULT NULL AFTER `video_url`;

CREATE TABLE `cart_items` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`cart_id` INT(11) NOT NULL,
	`caret_title` VARCHAR(255) NULL DEFAULT NULL,
	`subscription_stripe_id` VARCHAR(255) NULL DEFAULT NULL,
	`subscription_pricing_id` INT(10) NULL DEFAULT NULL,
	`subscription_amount` DECIMAL(20,2) NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;;

CREATE TABLE `carts` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NULL DEFAULT NULL,
	`guest_token` VARCHAR(255) NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `guest_token` (`guest_token`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

ALTER TABLE `clm_requests`
	ADD COLUMN `status_id` INT(10) NULL DEFAULT NULL AFTER `caret_id`;

ALTER TABLE `clm_plans`
	ADD COLUMN `interval_count` INT(10) NULL DEFAULT NULL AFTER `duration`;
  
ALTER TABLE `sounds`
	ADD COLUMN `license_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `user_id`;

ALTER TABLE `clm_splash`
	ADD COLUMN `license_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `user_id`;

ALTER TABLE `ads`
	ADD COLUMN `license_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `user_id`;

ALTER TABLE `caret_pricing`
	ADD COLUMN `contact_amount` DECIMAL(10,2) UNSIGNED NULL DEFAULT NULL AFTER `contact_sales`;

ALTER TABLE `caret_pricing`
	ADD COLUMN `contact_sales` TINYINT(1) ZEROFILL NULL DEFAULT NULL AFTER `special_instructions`;

ALTER TABLE `clm_licenses`
	ADD COLUMN `subscription_stripe_id` VARCHAR(255) NULL DEFAULT NULL AFTER `subscription_amount`;

ALTER TABLE `caret_pricing`
	ADD COLUMN `stripe_id_one_year` VARCHAR(255) NULL DEFAULT NULL AFTER `stripe_id`,
	ADD COLUMN `stripe_id_two_year` VARCHAR(255) NULL DEFAULT NULL AFTER `stripe_id_one_year`,
	ADD COLUMN `stripe_id_three_year` VARCHAR(255) NULL DEFAULT NULL AFTER `stripe_id_two_year`;

ALTER TABLE `caret_pricing`
	ADD COLUMN `stripe_subscription_id` VARCHAR(255) NOT NULL DEFAULT '0' AFTER `stripe_id`;

ALTER TABLE `caret_pricing`
	ADD COLUMN `stripe_id` VARCHAR(255) NOT NULL DEFAULT '0' AFTER `plan_id`,

ALTER TABLE `caret_pricing`
	ADD COLUMN `plan_id` BIGINT(20) NULL DEFAULT 0 AFTER `id`;

CREATE TABLE caret_pricing (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,  -- To store title for each row
    networth VARCHAR(255),
    instagram_followers VARCHAR(255),
    twitter_followers VARCHAR(255),
    google_searches VARCHAR(255),
    one_year_license DECIMAL(10,2),
    two_year_license DECIMAL(10,2),
    three_year_license DECIMAL(10,2),
    special_instructions TEXT,
    `is_active` TINYINT(1) UNSIGNED ZEROFILL NULL DEFAULT NULL,
    sales_price DECIMAL(10,2),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,  -- Timestamp for record creation
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  -- Timestamp for record update
    deleted_at TIMESTAMP NULL DEFAULT NULL  -- NULL by default, used for soft deletes
);

CREATE TABLE `clm_keywords` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NULL DEFAULT NULL,
	`type` VARCHAR(50) NULL DEFAULT NULL,
	`is_active` TINYINT(1) UNSIGNED ZEROFILL NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

ALTER TABLE `videos`
	ADD COLUMN `caret_atempts` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `is_manual`;

ALTER TABLE `clm_requests`
	ADD COLUMN `status` TINYINT(1) NULL DEFAULT '0' AFTER `company_description`,
	ADD COLUMN `approval_date` TIMESTAMP NULL DEFAULT NULL AFTER `status`;



CREATE TABLE `caret_search_data` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `keyword` VARCHAR(255) NULL DEFAULT NULL,
    `classification` VARCHAR(255) NULL DEFAULT NULL,
    `net_worth` VARCHAR(255 NULL DEFAULT NULL,
    `google_search_volume` VARCHAR(255) NULL DEFAULT NULL,
    `instagram` VARCHAR(255) NULL DEFAULT NULL,
    `twitter` VARCHAR(255) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `clm_requests`
   ADD COLUMN `name` VARCHAR(255) NULL DEFAULT NULL AFTER `subscription_amount`,
   ADD COLUMN `designation` VARCHAR(255) NULL DEFAULT NULL AFTER `name`,
   ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL AFTER `designation`,
   ADD COLUMN `phone` VARCHAR(15) NULL DEFAULT NULL AFTER `email`;

ALTER TABLE `clm_requests`
	ADD COLUMN `subscription_plan` INT(5) NULL DEFAULT NULL AFTER `caret_title`,
	ADD COLUMN `subscription_amount` DECIMAL(20,6) NULL DEFAULT NULL AFTER `subscription_plan`;


ALTER TABLE `video_splashes`
	ADD COLUMN `finish` TINYINT(1) UNSIGNED NULL DEFAULT 0 AFTER `splash_id`;

CREATE TABLE `clm_requests` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id` BIGINT(20) NULL DEFAULT NULL,
    `caret_id` INT(11) NULL DEFAULT NULL,
    `caret_title` VARCHAR(255) NULL DEFAULT NULL,
    `company_name` VARCHAR(255) NULL DEFAULT NULL,
    `company_address` VARCHAR(255) NULL DEFAULT NULL,
    `company_description` VARCHAR(255) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `clm_licenses`
	ADD COLUMN `default_sound` INT(13) NULL DEFAULT NULL AFTER `default_finish`;

ALTER TABLE `sounds`
	ADD COLUMN `user_id` BIGINT(20) NULL DEFAULT NULL AFTER `id`;

CREATE TABLE `video_splashes` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`video_id` BIGINT(20) UNSIGNED NOT NULL,
	`splash_id` BIGINT(20) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=4
;

CREATE TABLE `video_ads` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`video_id` BIGINT(20) UNSIGNED NOT NULL,
	`ad_id` BIGINT(20) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=4
;

ALTER TABLE `videos`
	ADD COLUMN `caret_processing` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `is_manual`;


ALTER TABLE `videos`
	ADD COLUMN `license_id` INT(5) UNSIGNED NULL DEFAULT NULL AFTER `age`,
	ADD COLUMN `is_manual` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `license_id`;


ALTER TABLE `clm_licenses`
	ADD COLUMN `default_ad` INT(13) NULL DEFAULT NULL AFTER `status_id`,
	ADD COLUMN `default_intro` INT(13) NULL DEFAULT NULL AFTER `default_ad`,
	ADD COLUMN `default_finish` INT(13) NULL DEFAULT NULL AFTER `default_intro`;


ALTER TABLE `clm_licenses`
	ADD COLUMN `status_id` TINYINT(1) NULL DEFAULT '0' AFTER `is_active`;

CREATE TABLE `user_interests` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`cat_id` INT(11) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;



    ALTER TABLE `videos`
	ADD COLUMN `video_raw_url` VARCHAR(255) NULL DEFAULT NULL AFTER `distance_retry_count`;

    ALTER TABLE `clm_licenses`
	ADD COLUMN `is_active` TINYINT(1) NULL DEFAULT '0' AFTER `caret_id`;

    ALTER TABLE `clm_licenses`
	ADD COLUMN `subscription_plan` INT(5) NULL DEFAULT NULL AFTER `is_active`,
	ADD COLUMN `subscription_amount` DECIMAL(10,2) NULL DEFAULT NULL AFTER `subscription_plan`;








DROP TABLE IF EXISTS `clm_licenses`;
CREATE TABLE IF NOT EXISTS `clm_licenses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `caret_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `caret_title` varchar(255) DEFAULT NULL,
  `caret_logo` varchar(255) DEFAULT NULL,
  `caret_certificate` varchar(255) DEFAULT NULL,
  `subscription_status` varchar(255) DEFAULT NULL,
  `cancel_at_period_end` varchar(255) DEFAULT NULL,
  `current_period_end` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table caretsback.clm_licenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `clm_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `clm_licenses` ENABLE KEYS */;

-- Dumping structure for table caretsback.clm_payments
DROP TABLE IF EXISTS `clm_payments`;
CREATE TABLE IF NOT EXISTS `clm_payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `license_id` bigint(20) DEFAULT NULL,
  `card_id` int(13) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT '0.00',
  `transaction_id` varchar(255) DEFAULT NULL,
  `latest_invoice` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `cancel_at` varchar(255) DEFAULT NULL,
  `cancel_at_period_end` varchar(255) DEFAULT NULL,
  `canceled_at` varchar(255) DEFAULT NULL,
  `collection_method` varchar(255) DEFAULT NULL,
  `created` varchar(255) DEFAULT NULL,
  `current_period_end` varchar(255) DEFAULT NULL,
  `current_period_start` varchar(255) DEFAULT NULL,
  `days_until_due` varchar(255) DEFAULT NULL,
  `default_payment_method` varchar(255) DEFAULT NULL,
  `plan_id` varchar(255) DEFAULT NULL,
  `plan_currency` varchar(255) DEFAULT NULL,
  `plan_interval` varchar(255) DEFAULT NULL,
  `plan_interval_count` varchar(255) DEFAULT NULL,
  `plan_product` varchar(255) DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_clm_payments_users` (`user_id`),
  CONSTRAINT `FK_clm_payments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table caretsback.clm_payments: ~0 rows (approximately)
/*!40000 ALTER TABLE `clm_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `clm_payments` ENABLE KEYS */;

-- Dumping structure for table caretsback.clm_plans
DROP TABLE IF EXISTS `clm_plans`;
CREATE TABLE IF NOT EXISTS `clm_plans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stripe_id` varchar(255) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `duration` varchar(50) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `is_active` tinyint(1) unsigned zerofill DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table caretsback.clm_plans: ~0 rows (approximately)
/*!40000 ALTER TABLE `clm_plans` DISABLE KEYS */;
/*!40000 ALTER TABLE `clm_plans` ENABLE KEYS */;

-- Dumping structure for table caretsback.clm_splash
DROP TABLE IF EXISTS `clm_splash`;
CREATE TABLE IF NOT EXISTS `clm_splash` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT '0',
  `splash_title` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `start_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `end_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- Dumping data for table caretsback.clm_splash: ~0 rows (approximately)
/*!40000 ALTER TABLE `clm_splash` DISABLE KEYS */;
/*!40000 ALTER TABLE `clm_splash` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
